;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; and now for something completely different...
;; UrForth/C Forth Engine!
;; Copyright (C) 2023 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

HERE  \ for size reports


VOCABULARY zxdis
ALSO zxdis DEFINITIONS


<public-words>

0 value pc
1 value hex?


<hidden-words>

0 value (ixiy)  \ contains 0, [char] x or [char] y
0 value (disp)
0 value (dptr)
0 value (dofs)
0 value (opcode)

create (ixydisp-table)
  0x00 C, 0x00 C, 0x00 C, 0x00 C,
  0x00 C, 0x00 C, 0x70 C, 0x00 C,
  0x40 C, 0x40 C, 0x40 C, 0x40 C,
  0x40 C, 0x40 C, 0xBF C, 0x40 C,
  0x40 C, 0x40 C, 0x40 C, 0x40 C,
  0x40 C, 0x40 C, 0x40 C, 0x40 C,
  0x00 C, 0x08 C, 0x00 C, 0x00 C,
  0x00 C, 0x00 C, 0x00 C, 0x00 C,
create;


\ ////////////////////////////////////////////////////////////////////////// //
\ : (draw-str)  ( -- ) (dptr) 1+ (dofs) xtype cr ;

: (put-char)  ( ch -- )
  (dptr) (dofs) + 1+ C!
  (dofs) 1+ to (dofs)
;

: (put-str)  ( addr len -- )
  dup 0> if
    over + swap do
      i c@ (put-char)
    loop
  else
    2drop
  endif
;

: (put-space)  ( -- ) bl (put-char) ;
: (put-comma)  ( -- ) [char] , (put-char) ;
: (put-lpar)  ( -- ) [char] ( (put-char) ;
: (put-rpar)  ( -- ) [char] ) (put-char) ;


: (put-str-2)  ( addr -- ) 2 (put-str) ;
: (put-str-4)  ( addr -- ) drop + 4 (put-str) ;
: (put-str-4x)  ( addr -- ) dup 3 + c@ bl = if 3 else 4 endif (put-str) ;


\ ////////////////////////////////////////////////////////////////////////// //
: (put-n8)  ( n -- )
  lo-byte
  base @ >r
  hex? if
    hex <# # # [char] # hold
  else
    decimal <# #s
  endif
  #>
  (put-str) r> base !
;

: (put-n16)  ( n -- )
  lo-word
  base @ >r
  hex? if
    hex <# # # # # [char] # hold
  else
    decimal <# #s
  endif
  #>
  (put-str) r> base !
;

: (put-disp)  ( -- )
  (disp) if
    (disp) 0< if [char] - else [char] + endif (put-char)
    (disp) abs
    base @ >r decimal <# #s #>
    (put-str) r> base !
  endif
;


\ ////////////////////////////////////////////////////////////////////////// //
\ advances PC
defer (zx-c@)  ( pc -- byte )
' (notimpl) to (zx-c@)

: (get-byte)  ( -- b )
  pc (zx-c@)
  pc 1+ lo-word to pc
;


\ advances PC
: (get-word)  ( -- b )
  (get-byte) (get-byte) 8 lshift or
;


: (byte-to-signed)  ( b -- n )  dup 0x80 >= if 0x100 - endif ;


\ ////////////////////////////////////////////////////////////////////////// //
: (put-ixy-mem) ( -- )
  " (i" (put-str)
  (ixiy) (put-char)
  (put-disp)
  (put-rpar)
;


: (put-r8)  ( r8 -- )
  7 and  " bcdehl.a" drop + c@
  dup [char] . = if
    \ (hl)
    drop
    (ixiy) if
      (put-ixy-mem)
    else
      " (hl)" (put-str)
    endif
  else
    \ undocumented IX/IY 8-bit part access
    (ixiy) if
      dup [char] h = over [char] l = or if
        \ [char] i (put-char)
        (ixiy) (put-char)
      endif
    endif
    (put-char)
  endif
;


: (put-r16-hl-ixy)  ( -- )
  \ hl
  (ixiy) if
    " i" (put-str)
    (ixiy) (put-char)
  else
    " hl" (put-str)
  endif
;

: (put-v16)  ( -- ) (get-word) (put-n16) ;
: (put-m16)  ( -- )  (put-lpar) (put-v16) (put-rpar) ;

: (put-r16-common)  ( r16 addr count -- )
  drop swap 3 and 2* +
  dup c@ [char] h = if
    drop (put-r16-hl-ixy)
  else
    (put-str-2)
  endif
;

: (put-r16-sp)  ( r16 -- )
  (opcode) 4 rshift
  " bcdehlsp" (put-r16-common)
;


: (put-r16-af)  ( r16 -- )
  (opcode) 4 rshift
  " bcdehlaf" (put-r16-common)
;


: (put-cc)  ( cc -- )
  7 and 2* " nzz ncc popep m " drop +
  dup c@ (put-char)
  1+ c@ dup 32 <> if (put-char) else drop endif
;

\ ////////////////////////////////////////////////////////////////////////// //
: (decode-cb-unixy)  ( -- )
  \ special undocumented thing
  (ixiy) if
    \ `bit` doesn't need undoc ixy
    (opcode) 0x80 and if
      (opcode) 7 and 6 <> if
        (put-comma)
        (put-ixy-mem)
      endif
    endif
  endif
;

: (decode-cb)  ( -- )
  (opcode) 0xc0 and
  if
    (opcode) 4 rshift 0x0c and 4- " bit res set " (put-str-4)
    (put-space)
    (opcode) 3 rshift 7 and [char] 0 + (put-char)
    (put-comma)
  else
    (opcode) 2u/ 0x1c and
    " rlc rrc rl  rr  sla sra sll srl " (put-str-4)
    (put-space)
  endif
  (opcode) (put-r8)
  (decode-cb-unixy)
;


\ ////////////////////////////////////////////////////////////////////////// //
: (decode-ed-xrep)  ( -- )
  \ two instructions with the wrong mnemonic length
  (opcode) 0xa3 = if " outi" (put-str) exit endif
  (opcode) 0xab = if " outd" (put-str) exit endif
  \ common code
  (opcode) 3 and 2* " ldcpinot" drop + (put-str-2)
  (opcode) 0x08 and if [char] d else [char] i endif (put-char)
  (opcode) 0x10 and if [char] r (put-char) endif
;

: (decode-ed)  ( -- )
  (opcode) 0xa4 and 0xa0 = if (decode-ed-xrep) exit endif
  (opcode) 0xc0 and 0x40 <> if " nope" (put-str) exit endif
  (opcode) 0x04 and if
    (opcode) 7 and case
      0x04 of " neg" (put-str) endof
      0x05 of " ret" (put-str) (opcode) 0x08 and if [char] i else [char] n endif (put-char) endof
      0x06 of \ im
        " im   " (put-str)
        (opcode) 0x47 = if " 0/1" (put-str) exit endif
        (opcode) 0x10 and if
          (opcode) 0x08 and if [char] 2 else [char] 1 endif
        else
          [char] 0
        endif
        (put-char)
      endof
      0x07 of
        (opcode) case
          0x47 of " ld   i,a" endof
          0x4f of " ld   r,a" endof
          0x57 of " ld   a,i" endof
          0x5f of " ld   a,r" endof
          0x67 of " rrd" endof
          0x6f of " rld" endof
          otherwise drop " nope"
        endcase
        (put-str)
      endof
      otherwise drop " nope" (put-str)
    endcase
  else
    (opcode) 0x02 and if
      \ r16
      (opcode) 0x01 and if
        " ld   " (put-str)
        \ direction
        (opcode) 0x08 and if
          \ to rr
          (put-r16-sp)
          (put-comma)
          (put-m16)
        else
          \ to mem
          (put-m16)
          (put-comma)
          (put-r16-sp)
        endif
      else
        (opcode) 2u/ 4 and " sbc adc " (put-str-4)
        "  hl," (put-str)
        (put-r16-sp)
      endif
    else
      (opcode) 0x01 and if
        " out  (c)," (put-str)
        (opcode) 3 rshift
        \ check for `(hl)`, it is special here
        dup 7 and 6 = if
          drop [char] 0 (put-char)
        else
          (put-r8)
        endif
      else
        " in   " (put-str)
        (opcode) 3 rshift
        \ check for `(hl)`, it is special here
        dup 7 and 6 <> if
          (put-r8)
          (put-comma)
        else
          drop
        endif
        " (c)" (put-str)
      endif
    endif
  endif
;


\ ////////////////////////////////////////////////////////////////////////// //
\ ld r8,r8 (and halt)
: (decode-norm-grp1)  ( -- )
  (opcode) 0x76 = if " halt" (put-str) exit endif
  " ld   " (put-str)
  (ixiy) 0<>  (opcode) 3 rshift 7 and 6 =  (opcode) 7 and 6 = or and if
    ;; this is for "ld l,(ix)" and such
    (ixiy) >r
    (opcode) 3 rshift 7 and 6 <> if 0 to (ixiy) endif
    (opcode) 3 rshift (put-r8)
    (put-comma)
    (opcode) 7 and 6 <> if rdrop 0 else r> endif to (ixiy)
    (opcode) (put-r8)
  else
    (opcode) 3 rshift (put-r8)
    (put-comma)
    (opcode) (put-r8)
  endif
;

: (put-alu-str)  ( -- )
  (opcode) 2u/ 0x1c and " add adc sub sbc and xor or  cp  " (put-str-4)
  (put-space)
  \ two special opcodes
  (opcode) 0x38 and dup 0x08 = over 0x18 = or swap 0x00 = or if " a," (put-str) endif
;


\ call,ret,push,pop,etc.
: (decode-norm-grp3)  ( -- )
  (opcode) 7 and case
    0x00 of " ret  " (put-str)  (opcode) 3 rshift (put-cc) endof
    0x01 of
      (opcode) 0x08 and if
        (opcode) 0x30 and case
          0x00 of " ret" (put-str) endof
          0x10 of " exx" (put-str) endof
          0x20 of " jp   (" (put-str) (put-r16-hl-ixy) (put-rpar) endof
          0x30 of " ld   sp," (put-str) (put-r16-hl-ixy) endof
        endcase
      else
        " pop  " (put-str)
        (put-r16-af)
      endif
    endof
    0x02 of " jp   " (put-str)  (opcode) 3 rshift (put-cc) (put-comma) (put-v16) endof
    0x03 of
      (opcode) 0x38 and case
        0x00 of " jp   " (put-str) (put-v16) endof
        \ CB:0x08 of endof
        0x10 of " out  (" (put-str) (get-byte) (put-n8) " ),a" (put-str) endof
        0x18 of " in   a,(" (put-str) (get-byte) (put-n8) (put-rpar) endof
        0x20 of " ex   (sp)," (put-str) (put-r16-hl-ixy)  endof
        0x28 of " ex   de,hl" (put-str) endof
        0x30 of " di" (put-str) endof
        0x38 of " ei" (put-str) endof
      endcase
    endof
    0x04 of " call " (put-str)  (opcode) 3 rshift (put-cc) (put-comma) (put-v16) endof
    0x05 of
      (opcode) 0x08 and if
        \ prefixes already done, so only call is left
        " call " (put-str)
        (put-v16)
      else
        " push " (put-str)
        (put-r16-af)
      endif
    endof
    0x06 of
      (put-alu-str)
      (get-byte) (put-n8)
    endof
    0x07 of
      " rst  " (put-str)
      (opcode) 0x38 and (put-n8)
    endof
  endcase
;

: (decode-norm-grp0)  ( -- )
  (opcode) 0x06 and case
    0x00 of
      (opcode) 1 and if
        (opcode) 0x08 and if
          " add  " (put-str)
          (put-r16-hl-ixy)
          (put-comma)
          (put-r16-sp)
        else
          " ld   " (put-str)
          (put-r16-sp)
          (put-comma)
          (put-v16)
        endif
      else
        (opcode) 0x20 and if
          " jr   " (put-str)
          (opcode) 3 rshift 3 and (put-cc)
          (put-comma)
          (get-byte) dup 0x80 >= if 0x100 - endif
          pc + (put-n16)
          exit
        endif
        (opcode) 0x10 and if
          (opcode) 2u/ 4 and " djnzjr  " (put-str-4)
          (put-space)
          (get-byte) dup 0x80 >= if 0x100 - endif
          pc + (put-n16)
          exit
        endif
        (opcode) 0x08 and if " ex   af,af'" else " nop" endif (put-str)
      endif
    endof
    0x02 of
      (opcode) 1 and if
        (opcode) 2u/ 4 and " inc dec " (put-str-4)
        (put-space)
        (put-r16-sp)
      else
        " ld   " (put-str)
        (opcode) 0x3c and case
          0x00 of " (bc),a" (put-str) endof
          0x08 of " a,(bc)" (put-str) endof
          0x10 of " (de),a" (put-str) endof
          0x18 of " a,(de)" (put-str) endof
          0x20 of (put-m16) (put-comma) (put-r16-hl-ixy) endof
          0x28 of (put-r16-hl-ixy) (put-comma) (put-m16) endof
          0x30 of (put-m16) " ,a" (put-str) endof
          0x38 of " a," (put-str) (put-m16) endof
        endcase
      endif
    endof
    0x04 of
      (opcode) 0x01 and 2 lshift " inc dec " (put-str-4)
      (put-space)
      (opcode) 3 rshift (put-r8)
    endof
    0x06 of
      (opcode) 1 and if
        (opcode) 2u/ 0x1c and " rlcarrcarla rra daa cpl scf ccf " drop + (put-str-4x)
      else
        " ld   " (put-str)
        (opcode) 3 rshift (put-r8)
        (put-comma)
        (get-byte) (put-n8)
      endif
    endof
  endcase
;

: (decode-norm)  ( -- )
  (opcode) 0xc0 and case
    0x00 of (decode-norm-grp0) endof
    0x40 of (decode-norm-grp1) endof
    0x80 of (put-alu-str) (opcode) (put-r8) endof \ alu a,r8
    otherwise drop (decode-norm-grp3)
  endcase
;


\ ////////////////////////////////////////////////////////////////////////// //
<public-words>

\ returns disassembled text (only command, no address or bytes )
\ uses PAD
: disasm-one ( -- saddr slen )
  \ to pc
  0 to (ixiy)
  PAD 420 + to (dptr)
  0 to (dofs)
  0 to (disp)
  (get-byte)

  \ check if I<X|Y> prefix
  dup 0xdd = if [char] x to (ixiy) endif
  dup 0xfd = if [char] y to (ixiy) endif
  (ixiy) if
    drop (get-byte) dup to (opcode)
    dup 0xdd = over 0xfd = or if
      drop " nopx" (put-str)
      \ one byte back
      pc 1- lo-word to pc
      exit
    endif
    \ check if we have disp here
    dup 3 rshift (ixydisp-table) + c@
    1 rot 7 and lshift and
    if
      \ has disp
      (get-byte) (byte-to-signed) to (disp)
    endif
  else
    to (opcode)
  endif

  (opcode) case
    0xcb of (get-byte) to (opcode) (decode-cb) endof
    0xed of (get-byte) to (opcode) (decode-ed) endof
    otherwise drop (decode-norm)
  endcase
  (dofs) (dptr) C!
  (dptr) 1+ (dofs)
;

previous definitions

here swap -
 dup .( Z80 disasm size: ) . .( bytes\n)
drop

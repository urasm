;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; and now for something completely different...
;; UrForth/C Forth Engine!
;; Copyright (C) 2023 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; superinstruction optimiser: simple patterns
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; instruction pattern matcher
;;

$define SINOPT-DUMP-HIT-MATCH

0x8000_0000 constant mt-any

(*
  match format:
    count, data
  data:
    0x8000_0001 means "anything", otherwise value to compare
*)

0 value match-blockid

: match?  ( staddr mtaddr -- match? )
  a>r >a @+4>a  ( staddr count / A: mtaddr )
  over @ @+4>a = if
    over block-info@ to match-blockid
    true swap 1- for ( staddr flag / A: mtaddr )
      drop cell+
      ;; check block id
      dup block-info@ match-blockid = ifnot false break endif
      @+4>a dup mt-any = if drop true
      else ( staddr value )
        over @ = dup ifnot break endif
      endif
    endfor nip
  else 2drop false endif
  r>a
;


0 value curr-match
0 value curr-repl

: optim-replace  ( addr -- )
  a>r >a
  curr-repl count for  ( rpaddr )
    dup @ dup mt-any = if drop +4>a
    else !+4>a endif
  cell+ endfor drop r>a
;

defer first-dump
' drop to first-dump

: optim-match  ( mtaddr -- )
  dup to curr-match
  count +cells to curr-repl
  word-start begin dup word-end u< while  ( addr )
    dup curr-match match? if
      dup first-dump
        \ dup ." optim at " . cr \ debug:dump-stack
      \ dup curr-match @ curr-repl @ - dup +to instr-removed remove-instrs
      \ dup curr-repl count cells rot swap move
      dup optim-replace
      curr-match @ curr-repl @ - dup +to instr-removed  ( addr instr-remove )
      over curr-match @ +cells ( addr i-rem eaddr ) over -cells swap remove-instrs
      +1-to optim-count
    endif
  skip-instruction repeat drop
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; patterns
;;

create mt-(0|+)
\ words:
3 forth:,
  ' forth:(lit) ,
  0 ,
  ' + ,
\ replace:
0 forth:,
create;

create mt-(1|+)
\ words:
3 forth:,
  ' forth:(lit) ,
  1 ,
  ' + ,
\ replace:
1 forth:,
  ' 1+ ,
create;

create mt-(2|+)
\ words:
3 forth:,
  ' forth:(lit) ,
  2 ,
  ' + ,
\ replace:
1 forth:,
  ' 2+ ,
create;

create mt-(4|+)
\ words:
3 forth:,
  ' forth:(lit) ,
  4 ,
  ' + ,
\ replace:
1 forth:,
  ' 4+ ,
create;

create mt-(8|+)
\ words:
3 forth:,
  ' forth:(lit) ,
  8 ,
  ' + ,
\ replace:
1 forth:,
  ' 8+ ,
create;


create mt-(0|-)
\ words:
3 forth:,
  ' forth:(lit) ,
  0 ,
  ' - ,
\ replace:
0 forth:,
create;

create mt-(1|-)
\ words:
3 forth:,
  ' forth:(lit) ,
  1 ,
  ' - ,
\ replace:
1 forth:,
  ' 1- ,
create;

create mt-(2|-)
\ words:
3 forth:,
  ' forth:(lit) ,
  2 ,
  ' - ,
\ replace:
1 forth:,
  ' 2- ,
create;

create mt-(4|-)
\ words:
3 forth:,
  ' forth:(lit) ,
  4 ,
  ' - ,
\ replace:
1 forth:,
  ' 4- ,
create;

create mt-(8|-)
\ words:
3 forth:,
  ' forth:(lit) ,
  8 ,
  ' - ,
\ replace:
1 forth:,
  ' 8- ,
create;


create mt-(1|lsh)
\ words:
3 forth:,
  ' forth:(lit) ,
  1 ,
  ' lsh ,
\ replace:
1 forth:,
  ' 2* ,
create;

create mt-(2|lsh)
\ words:
3 forth:,
  ' forth:(lit) ,
  2 ,
  ' lsh ,
\ replace:
1 forth:,
  ' 4* ,
create;

create mt-(3|lsh)
\ words:
3 forth:,
  ' forth:(lit) ,
  3 ,
  ' lsh ,
\ replace:
1 forth:,
  ' 8* ,
create;

create mt-(-1|lsh)
\ words:
3 forth:,
  ' forth:(lit) ,
  -1 ,
  ' lsh ,
\ replace:
1 forth:,
  ' 2U/ ,
create;

create mt-(-2|lsh)
\ words:
3 forth:,
  ' forth:(lit) ,
  -2 ,
  ' lsh ,
\ replace:
1 forth:,
  ' 4U/ ,
create;

create mt-(-3|lsh)
\ words:
3 forth:,
  ' forth:(lit) ,
  -3 ,
  ' lsh ,
\ replace:
1 forth:,
  ' 8U/ ,
create;

create mt-(-1|ash)
\ words:
3 forth:,
  ' forth:(lit) ,
  -1 ,
  ' ash ,
\ replace:
1 forth:,
  ' 2/ ,
create;

create mt-(-2|ash)
\ words:
3 forth:,
  ' forth:(lit) ,
  -2 ,
  ' ash ,
\ replace:
1 forth:,
  ' 4/ ,
create;

create mt-(-3|ash)
\ words:
3 forth:,
  ' forth:(lit) ,
  -3 ,
  ' ash ,
\ replace:
1 forth:,
  ' 8/ ,
create;


create mt-(1|*)
\ words:
3 forth:,
  ' forth:(lit) ,
  1 ,
  ' * ,
\ replace:
0 forth:,
create;

create mt-(1|U*)
\ words:
3 forth:,
  ' forth:(lit) ,
  1 ,
  ' u* ,
\ replace:
0 forth:,
create;

create mt-(2|*)
\ words:
3 forth:,
  ' forth:(lit) ,
  2 ,
  ' * ,
\ replace:
1 forth:,
  ' 2* ,
create;

create mt-(2|U*)
\ words:
3 forth:,
  ' forth:(lit) ,
  2 ,
  ' U* ,
\ replace:
1 forth:,
  ' 2* ,
create;

create mt-(4|*)
\ words:
3 forth:,
  ' forth:(lit) ,
  4 ,
  ' * ,
\ replace:
1 forth:,
  ' 4* ,
create;

create mt-(4|U*)
\ words:
3 forth:,
  ' forth:(lit) ,
  4 ,
  ' U* ,
\ replace:
1 forth:,
  ' 4* ,
create;

create mt-(8|*)
\ words:
3 forth:,
  ' forth:(lit) ,
  8 ,
  ' * ,
\ replace:
1 forth:,
  ' 8* ,
create;

create mt-(8|U*)
\ words:
3 forth:,
  ' forth:(lit) ,
  8 ,
  ' U* ,
\ replace:
1 forth:,
  ' 8* ,
create;


create mt-(swap|1+|swap)
\ words:
3 forth:,
  ' forth:swap ,
  ' 1+ ,
  ' forth:swap ,
\ replace:
1 forth:,
  ' (swap:1+:swap) ,
create;


create mt-(cells|+)
\ words:
2 forth:,
  ' cells ,
  ' + ,
\ replace:
1 forth:,
  ' +cells ,
create;

create mt-(cells|-)
\ words:
2 forth:,
  ' cells ,
  ' - ,
\ replace:
1 forth:,
  ' -cells ,
create;


create mt-(dp0|-)
\ words:
3 forth:,
  ' forth:(lit) ,
  0 ,
  ' forth:(direct:!) ,
\ replace:
1 forth:,
  ' forth:(direct:0:!) ,
create;

create mt-(dp1|-)
\ words:
3 forth:,
  ' forth:(lit) ,
  1 ,
  ' forth:(direct:!) ,
\ replace:
1 forth:,
  ' forth:(direct:1:!) ,
create;

create mt-(dp-1|-)
\ words:
3 forth:,
  ' forth:(lit) ,
  -1 ,
  ' forth:(direct:!) ,
\ replace:
1 forth:,
  ' forth:(direct:-1:!) ,
create;


create mt-(dpfalse|-)
\ words:
2 forth:,
  ' forth:false ,
  ' forth:(direct:!) ,
\ replace:
1 forth:,
  ' forth:(direct:0:!) ,
create;


create mt-(dptrue|-)
\ words:
2 forth:,
  ' forth:true ,
  ' forth:(direct:!) ,
\ replace:
1 forth:,
true -1 = [IF]
  ' forth:(direct:-1:!) ,
[ELSE] true 1 <> " wut?!" ?error
  ' forth:(direct:1:!) ,
[ENDIF]
create;


create mt-(dp0+|-)
\ words:
3 forth:,
  ' forth:(lit) ,
  0 ,
  ' forth:(direct:+!) ,
\ replace:
1 forth:,
  ' forth:(direct:!) ,
create;

create mt-(dp0-|-)
\ words:
3 forth:,
  ' forth:(lit) ,
  0 ,
  ' forth:(direct:-!) ,
\ replace:
1 forth:,
  ' forth:(direct:!) ,
create;

create mt-(dp0|+|!)
\ words:
3 forth:,
  ' forth:(lit) ,
  0 ,
  ' forth:(direct:+:!) ,
\ replace:
1 forth:,
  ' forth:! ,
create;

create mt-(dp0|+|@)
\ words:
3 forth:,
  ' forth:(lit) ,
  0 ,
  ' forth:(direct:+:@) ,
\ replace:
1 forth:,
  ' forth:@ ,
create;


create mt-(lit-and)
\ words:
3 forth:,
  ' forth:(lit) ,
  mt-any ,
  ' forth:and ,
\ replace:
2 forth:,
  ' forth:(lit-and) ,
  mt-any ,
create;

create mt-(lit-~and)
\ words:
3 forth:,
  ' forth:(lit) ,
  mt-any ,
  ' forth:~and ,
\ replace:
2 forth:,
  ' forth:(lit-~and) ,
  mt-any ,
create;

create mt-(lit-or)
\ words:
3 forth:,
  ' forth:(lit) ,
  mt-any ,
  ' forth:or ,
\ replace:
2 forth:,
  ' forth:(lit-or) ,
  mt-any ,
create;

create mt-(lit-xor)
\ words:
3 forth:,
  ' forth:(lit) ,
  mt-any ,
  ' forth:xor ,
\ replace:
2 forth:,
  ' forth:(lit-xor) ,
  mt-any ,
create;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; pattern list
;;

create match-list
  ' mt-(cells|+) cfa->pfa ,
  ' mt-(cells|-) cfa->pfa ,

  ' mt-(swap|1+|swap) cfa->pfa ,

  ' mt-(0|+) cfa->pfa ,
  ' mt-(1|+) cfa->pfa ,
  ' mt-(2|+) cfa->pfa ,
  ' mt-(4|+) cfa->pfa ,
  ' mt-(8|+) cfa->pfa ,

  ' mt-(0|-) cfa->pfa ,
  ' mt-(1|-) cfa->pfa ,
  ' mt-(2|-) cfa->pfa ,
  ' mt-(4|-) cfa->pfa ,
  ' mt-(8|-) cfa->pfa ,

  ' mt-(1|*) cfa->pfa ,
  ' mt-(1|U*) cfa->pfa ,
  ' mt-(2|*) cfa->pfa ,
  ' mt-(2|U*) cfa->pfa ,
  ' mt-(4|*) cfa->pfa ,
  ' mt-(4|U*) cfa->pfa ,
  ' mt-(8|*) cfa->pfa ,
  ' mt-(8|U*) cfa->pfa ,

  ' mt-(1|lsh) cfa->pfa ,
  ' mt-(2|lsh) cfa->pfa ,
  ' mt-(3|lsh) cfa->pfa ,
  ' mt-(-1|lsh) cfa->pfa ,
  ' mt-(-2|lsh) cfa->pfa ,
  ' mt-(-3|lsh) cfa->pfa ,
  ' mt-(-1|ash) cfa->pfa ,
  ' mt-(-2|ash) cfa->pfa ,
  ' mt-(-3|ash) cfa->pfa ,

  ' mt-(dp0|-) cfa->pfa ,
  ' mt-(dp1|-) cfa->pfa ,
  ' mt-(dp-1|-) cfa->pfa ,
  ' mt-(dpfalse|-) cfa->pfa ,
  ' mt-(dptrue|-) cfa->pfa ,

  ' mt-(dp0+|-) cfa->pfa ,
  ' mt-(dp0-|-) cfa->pfa ,

  ' mt-(dp0|+|!) cfa->pfa ,
  ' mt-(dp0|+|@) cfa->pfa ,

  ' mt-(lit-and) cfa->pfa ,
  ' mt-(lit-~and) cfa->pfa ,
  ' mt-(lit-or) cfa->pfa ,
  ' mt-(lit-xor) cfa->pfa ,

  0 ,
create;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; high-level code
;;

defer intermid-dump

' drop to intermid-dump

: run-match-optims  ( -- )
  match-list begin
    dup @ ?dup while ( mtl match ) optim-match
    dup intermid-dump
  cell+ repeat drop
;

: optim-matches  ( -- )
  optim-count begin run-match-optims optim-count <> while optim-count repeat
;

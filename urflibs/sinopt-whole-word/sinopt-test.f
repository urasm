;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; and now for something completely different...
;; UrForth/C Forth Engine!
;; Copyright (C) 2023 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; superinstruction optimiser
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

$include <sinopt/sinopt.f>

.( optimising system words...\n)
(sinopt):optimise-system

.( setting optimiser...\n)
(sinopt):activate

also (sinopt)

: xxtest  ( -- )
  42 " boo" 2drop drop
;

: test-000  ( a n -- )  cells + . cr ;
: test-001  ( a n -- )  ?dup if cells + dup if noop endif 0 - endif . cr 7 and -2 ash ;

bye

\ prepare-word: interpret
\ debug:decompile xxtest
\ prepare-word: xxtest
\ debug:decompile collect-branches
\ prepare-word: test-001

\ 0 value prev-optim-count
\ 0 value first-dumped

: dbg-first-dump  ( optaddr -- )
  ." optim at " 0 u.r cr \ debug:dump-stack
  first-dumped ifnot
    true to first-dumped
    ." === BEFORE: " word-start pfa->cfa cfa->lfa lfa->nfa id-count xtype
    ."  ===" cr disasm-word
  endif
;

' dbg-first-dump to first-dump


: dbg-intermid-dump  ( match-tbl-addr -- )
  optim-count prev-optim-count <> if
    ." MATCH HIT: " @ pfa->nfa id-count xtype cr
    ." === intermid ===" cr
    disasm-word
    $IF $DEBUG-SINOPT-BRANCHES
    dump-branch-addrs
    $ENDIF
    optim-count to prev-optim-count
  else drop endif
;

' dbg-intermid-dump to intermid-dump

previous

\ debug:decompile mark-blocks
$if 1
(sinopt):prepare-word: (sinopt):mark-blocks (sinopt):optim-run
(sinopt):prepare-word: test-001 (sinopt):optim-run
$endif

\ 48176 lfa->cfa debug:(decompile-cfa)

$if 0
' forth:ufo-run-repl dup cfa->pfa to word-start
cfa->wend to word-end
." word end: " word-end 0 u.r cr
guess-word-end
." word end: " word-end 0 u.r cr
\ debug:decompile forth:ufo-run-repl
disasm-word

bye
$endif


$if 1
.( ==============================================\n)
0 value words-optimised
0 value optim-total
0 value instr-total

: optim-vocab  ( vocid -- )
  0 to words-optimised
  0 to optim-total
  0 to instr-total
  voc-latest ( vocid )
  begin ?dup while
      \ ." word at: " dup 0 u.r cr
    dup
    lfa->nfa
    @
    compiler:(wflag-smudge) (* compiler:(wflag-hidden) or *) and
    ifnot
      dup lfa->cfa compiler:forth-word? if
        dup lfa->cfa compiler:cfa-scolon? ifnot
            \ dup lfa->nfa id-count ." <<< " xtype ."  >>>" cr
          dup lfa->cfa (sinopt):prepare-word-cfa
          (sinopt):optim-run
          (sinopt):optim-count if
            (sinopt):optim-count +to optim-total
            (sinopt):instr-removed +to instr-total
            +1-to words-optimised
          endif
        endif
      endif
    endif
      \ ." word at: " dup 0 u.r cr
  @ repeat

  ." words optimised: " words-optimised 0 .r cr
  ." optimisations done: " optim-total 0 .r cr
  ." instructions removed: " instr-total 0 .r cr
;

\ vocid: forth vocid-words
vocid: forth optim-vocab
\ vocid: compiler optim-vocab
\ vocid: linore optim-vocab
$endif

debug:dump-stack

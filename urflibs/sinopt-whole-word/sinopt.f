;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; and now for something completely different...
;; UrForth/C Forth Engine!
;; Copyright (C) 2023 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; superinstruction optimiser
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(*
optimiser process basic blocks. to do this, it records all branch
instruction addresses, and all branch destinations. then it processes
blocks between those things. if it needs to remove an instruction,
it fixes all branches.

it also fixes branch to branch to the final destination.

as our words are small, we can simply build a map with block ids.
each instruction is aligned at 4 bytes, which is very handy.

superinstructions are:
  constant
    replaced with literal (this is longer, but faster)
  variable
    replaced with a pfa-literal (this adds one more instruction, but is faster)
  variable @
    replaced with direct-@
  variable !
    replaced with direct-!
  value
    replaced with direct-@
  to value
    replaced with direct-!
*)


vocabulary (sinopt) (hidden)
also-defs: (sinopt)

$define DEBUG-SINOPT-SYSTIME-STATS
\ $define DEBUG-SINOPT-STATS
\ $define DEBUG-SINOPT-OPTHIT
\ $define DEBUG-SINOPT-VERBOSE
\ $define DEBUG-SINOPT-BRANCHES


0 value word-start
0 value word-end
0 value branch-map  ;; holds branch argument addrs; ends with 0
0 value branch-count
0 value block-map
;; statistics
0 value instr-removed
0 value optim-count

: align-dword  ( addr -- addr )
  3 + [ 3 bitnot ] imm-literal and
;

: branch-addr-addr  ( idx -- addr )
  dup 0 branch-count within " invalid index" ?not-error
  cells branch-map +
;

: branch-addr  ( idx -- addr )
  branch-addr-addr @
;

: block-info  ( addr -- info-addr )
  dup word-start word-end cell+ bounds? " invalid block address" ?not-error
  word-start - [ 3 bitnot ] imm-literal and block-map +
;

: block-info@  ( addr -- value )  block-info @ ;
: block-info!  ( addr -- value )  block-info ! ;

;; get VM instruction argument type
: get-instruction-arg-type  ( addr -- atype )
  @ cfa->nfa @ compiler:(warg-mask) and
;

;; skip VM instruction
: skip-instruction  ( addr -- next-addr )
  dup cell+ swap get-instruction-arg-type
  case
    compiler:(warg-branch) of cell+ endof
    compiler:(warg-lit) of cell+ endof
    compiler:(warg-c4strz) of abort endof  ;; we don't have such words yet
    compiler:(warg-cfa) of cell+ endof
    compiler:(warg-pfa) of cell+ endof
    compiler:(warg-cblock) of abort endof  ;; we don't have such words yet
    compiler:(warg-vocid) of cell+ endof
    compiler:(warg-c1strz) of bcount + 1+ ( trailing zero ) align-dword endof
    compiler:(warg-dataskip) of count + align-dword endof
  endcase
;

;; VM instruction length in bytes (including opcode)
: instruction-length  ( addr -- len )
  dup skip-instruction swap -
;


: good-branch-addr?  ( brdest -- )
  word-start word-end cell- bounds?
;

;; this expects `word-start` and `word-end` to be set
;; collect all branches into branch map
: collect-branches  ( -- )
  forth:(addr-temp-bit) 32768 + to branch-map
  0 to branch-count branch-map
  word-start begin dup word-end u< while  ( brdest addr )
    dup get-instruction-arg-type compiler:(warg-branch) = if
      2dup cell+ swap!  swap cell+ swap
      +1-to branch-count
    endif
  skip-instruction repeat drop
  0!  ;; final zero
;

: dump-branch-addrs  ( -- )
  branch-count for i branch-addr 0 .r cr endfor
;


;; mark all branch destinations with non-zero values in block map
;; this is used in block marker
: temp-mark-brdests  ( -- )
  branch-count for
    i branch-addr compiler:(branch-addr@)
    dup good-branch-addr? if block-info 1! else drop endif
    ;; also, always mark instruction after the branch as a new block
    i branch-addr cell+ block-info 1!
  endfor
  ;; always mark first instruction, to bump block id
  word-start block-info 1!
;

;; branches must be collected
: mark-blocks  ( -- )
  branch-map branch-count 1+ cells + to block-map
  block-map word-end word-start - 4 +cells erase  ;; slightly too much, why not
  temp-mark-brdests
  $IF 0
    word-start begin dup word-end u< while
      dup 5 .r ." : " dup block-info@ 0 .r cr
    cell+ repeat
  $ENDIF
    \ ." word end: " word-end 0 u.r cr
  ;; start with block id #0, because first instruction is always marked as branch dest
  0 word-start begin dup word-end u< while  ( block-id addr )
    dup block-info@ if  ;; we have a branch to here, new block
      swap 1+ swap
    endif
     \ debug:dump-stack
     \ 2dup block-info!  ;; set block id
    dup instruction-length
    dup 3 and " invalid instruction length" ?error -- assertion
      \ ." ***len: " dup . cr
      \ dup . cr
    /cells for 2dup block-info! cell+ endfor
  repeat
  \ over . ." blocks found" cr
  2drop
;


0 value (adj-staddr)
0 value (adj-dbytes)

;; adjust branches after staddr by delta *cells*
;; subtracts delta
: adjust-branches  ( -- )
  (adj-staddr) ( staddr )
  branch-count for  ( staddr )
    i branch-addr compiler:(branch-addr@)
    dup good-branch-addr? if
      2dup u< if  ( staddr braddr )
        (adj-dbytes) - i branch-addr compiler:(branch-addr!)
      else drop endif
    else drop endif
  endfor drop
;

;; subtracts delta
: adjust-branch-addrs  ( -- )
  (adj-staddr) cell- ( staddr )
  branch-count for
    i branch-addr
    over u> if  ( staddr )
      (adj-dbytes) i branch-addr-addr -!
    endif
  endfor drop
;

: (move-instrs)  ( -- )
  (adj-staddr) dup (adj-dbytes) +  ;; from
  swap dup                         ;; to
  word-end swap - (adj-dbytes) -   ;; count
  move
;

: (move-blockids)  ( -- )
  (adj-staddr) block-info dup (adj-dbytes) +         ;; from
  swap                                               ;; to
  (adj-staddr) word-end swap - (adj-dbytes) - cell+  ;; count, and one more cell
  move
;

;; remove `instcount` instructions at staddr
: remove-instrs  ( staddr instcount -- )
  dup 0< " invalid instcount" ?error
  ?dup if
    cells to (adj-dbytes) to (adj-staddr)
    adjust-branches
    adjust-branch-addrs
    (move-instrs)
    (move-blockids)
    (adj-dbytes) -to word-end
  else drop endif
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; instruction pattern matcher
;;
$include <sinopt-pats.f>

0 value prev-optim-count
0 value first-dumped


: .hex8  ( n -- )  base @ hex swap <# # # # # # # # # #> type base ! ;

: disasm-word-arg  ( addr -- )
  dup get-instruction-arg-type
  case
    compiler:(warg-branch) of cell+ @ space 0 u.r endof
    compiler:(warg-lit) of cell+ @ space 0 .r endof
    compiler:(warg-c4strz) of abort endof  ;; we don't have such words yet
    compiler:(warg-cfa) of cell+ @ space 0 u.r endof
    compiler:(warg-pfa) of cell+ @ space 0 u.r endof
    compiler:(warg-cblock) of abort endof  ;; we don't have such words yet
    compiler:(warg-vocid) of cell+ @ space u. endof
    compiler:(warg-c1strz) of drop ( dup bcount + align-dword ) endof
    compiler:(warg-dataskip) of drop ( dup @ + cell+ align-dword ) endof
    otherwise 2drop
  endcase
;

: disasm-word  ( -- )
  ." start: " word-start 0 .r cr
  ."   end: " word-end 0 .r cr
  word-start begin dup word-end u< while  ( addr )
    dup 6 u.r ." ("
    dup block-info@ 4 .r ." ): "
    dup @ .hex8 ." : "
    dup @ cfa->nfa id-count xtype
    dup disasm-word-arg
    cr
  skip-instruction repeat drop
;


;; guess current word end
;; we need this because some words may appear longer than they are
;; scanner remembers the furthest branch, and stops on "FORTH:(EXIT)"

0 value furthest-branch

: branch-back?  ( addr -- bool )
  dup @ ['] forth:(branch) = if
    \ cell+ dup compiler:(branch-addr@) swap cell+ ( braddr addr ) u<
    cell+ compiler:(branch-addr@) good-branch-addr? not
  else drop false endif
;

: stop-word?  ( addr -- bool )
  dup branch-back? ?dup ifnot
    @ case
      ['] forth:(exit) of true endof
      ['] forth:abort of true endof
      ['] forth:error of true endof
      ['] forth:fatal-error of true endof
      ['] forth:@execute-tail of true endof
      ['] forth:execute-tail of true endof
      ['] forth:bye of true endof
      \ ['] forth:nbye of true endof
    otherwise drop false endcase
  else nip
  endif
;

: guess-word-end  ( -- )
  word-start to furthest-branch  ;; nothing yet
  word-start begin dup word-end u< while  ( addr )
    dup get-instruction-arg-type
    compiler:(warg-branch) = if
      dup cell+ compiler:(branch-addr@)
      dup word-end u< if furthest-branch umax to furthest-branch else drop endif
        \ ." furthest branch: " furthest-branch 0 u.r cr
    endif
    dup stop-word? if
        \ ." possible EXIT: " dup 0 u.r cr
        \ ." fbranch: " furthest-branch 0 u.r cr
      dup furthest-branch u>= if
        skip-instruction to word-end exit
      endif
    endif
  skip-instruction repeat drop
;

: prepare-word-cfa  ( cfa -- )
  dup cfa->pfa to word-start
  cfa->wend to word-end
  0 to instr-removed
  0 to optim-count
    \ ." system word end at: " word-end 0 u.r cr
  guess-word-end
    \ ." guessed word end at: " word-end 0 u.r cr
;

: prepare-word:  ( -- )  \ name
  -find-required prepare-word-cfa
;

: optim-run  ( -- )
  collect-branches
  $IF $DEBUG-SINOPT-BRANCHES
  branch-count . ." branches found." cr
  $ENDIF
  mark-blocks
  \ ." === BEFORE ===" cr disasm-word
  $IF $DEBUG-SINOPT-BRANCHES
  dump-branch-addrs
  $ENDIF
  \ optim-cells+
  \ (cells_+) optim-match

  0 to prev-optim-count
  false to first-dumped
  optim-matches

  $IF $DEBUG-SINOPT-STATS
  optim-count if
    \ ." === AFTER ===" cr
    \ disasm-word
    \ dump-branch-addrs
    word-start pfa->nfa id-count ." OPTIMISED: WORD: " xtype cr
    2 spaces optim-count . ." optimisation(s) done, "
    2 spaces instr-removed . ." instruction(s) removed." cr
  endif
  $ENDIF
;

0 value words-optimised
0 value optim-total
0 value instr-total

: optim-vocab  ( vocid -- )
  0 to words-optimised
  0 to optim-total
  0 to instr-total
  voc-latest ( vocid )
  begin ?dup while
      \ ." word at: " dup 0 u.r cr
    dup
    lfa->nfa
    @
    compiler:(wflag-smudge) (* compiler:(wflag-hidden) or *) and
    ifnot
      dup lfa->cfa compiler:forth-word? if
        dup lfa->cfa compiler:cfa-scolon? ifnot
            \ dup lfa->nfa id-count ." <<< " xtype ."  >>>" cr
          dup lfa->cfa (sinopt):prepare-word-cfa
          (sinopt):optim-run
          (sinopt):optim-count if
            (sinopt):optim-count +to optim-total
            (sinopt):instr-removed +to instr-total
            +1-to words-optimised
          endif
        endif
      endif
    endif
      \ ." word at: " dup 0 u.r cr
  @ repeat

  $IF $DEBUG-SINOPT-STATS
  words-optimised if
    ." words optimised: " words-optimised 0 .r cr
    ." optimisations done: " optim-total 0 .r cr
    ." instructions removed: " instr-total 0 .r cr
  endif
  $ENDIF
;


$IF $DEBUG-SINOPT-VERBOSE
: dbg-first-dump  ( optaddr -- )
  ." optim at " 0 u.r cr \ debug:dump-stack
  first-dumped ifnot
    true to first-dumped
    ." === BEFORE: " word-start pfa->cfa cfa->lfa lfa->nfa id-count xtype
    ."  ===" cr disasm-word
  endif
;

' dbg-first-dump to first-dump


: dbg-intermid-dump  ( match-tbl-addr -- )
  optim-count prev-optim-count <> if
    ." MATCH HIT: " @ pfa->nfa id-count xtype cr
    ." === intermid ===" cr
    disasm-word
    $IF $DEBUG-SINOPT-BRANCHES
    dump-branch-addrs
    $ENDIF
    optim-count to prev-optim-count
  else drop endif
;

' dbg-intermid-dump to intermid-dump
$ELSIF $DEBUG-SINOPT-OPTHIT
: dbg-intermid-mthit  ( match-tbl-addr -- )
  optim-count prev-optim-count <> if
    ." ...MATCH HIT: " @ pfa->nfa id-count xtype cr
    optim-count to prev-optim-count
  else drop endif
;

' dbg-intermid-mthit to intermid-dump
$ENDIF


: optimise-system  ( -- )
  $IF $DEBUG-SINOPT-SYSTIME-STATS
  get-msecs negate
  $ENDIF
  vocid: forth optim-vocab
  vocid: compiler optim-vocab
  vocid: string optim-vocab
  $IF $DEBUG-SINOPT-SYSTIME-STATS
  get-msecs + ." System optimisation time: " 0 .r cr
  $ENDIF
  \ vocid: linore optim-vocab
;


: semicolon-optim  ( -- )
  \ ." optimiser invoked for " latest-nfa id-count xtype cr
  $IF $DEBUG-SINOPT-STATS
  get-msecs negate
  $ENDIF
  latest-cfa prepare-word-cfa optim-run
  $IF $DEBUG-SINOPT-STATS
  optim-count if
    get-msecs + ." word optimisation time: " 0 .r cr
  else drop endif
  $ENDIF
;

: activate  ( -- )
  ['] semicolon-optim to compiler:(;-optimise)
;

: deactivate  ( -- )
  ['] forth:noop to compiler:(;-optimise)
;


prev-defs

\ debug:dump-stack

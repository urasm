;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; and now for something completely different...
;; UrForth/C Forth Engine!
;; Copyright (C) 2023 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; very simple and limited support for double numbers, with "DPL"
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


-1 uservar DPL

vocabulary (dbl-dpl-parser)
also-defs: (dbl-dpl-parser)

;; convert the ASCII text beginning at addr with regard to BASE.
;; the new value is accumulated into unsigned double number ud0, being left as ud1.
;; addr1 and count1 are unparsed part of the string
;; will never read more than count bytes
;; doesn't skip any spaces, doesn't parse prefixes and signs
;; but skips '_'
: dnumber-parse-simple  ( addr count ud0lo ud0hi -- addr1 count1 ud1lo ud1hi )
  2over nip 0> if
    2>r  ( addr count | ud )
    ;; first must be a digit
    over c@ base @ string:digit? if
      ;; main loop
      begin dup while  ( addr count | u )
        over c@
        dup [char] _ = if drop  ;; skip '_'
        else  ;; try digit
          base @ string:digit ifnot break endif
          2r> base @ uds* rot u>d d+ 2>r
        endif
        string:/char
      repeat
    endif
    2r>
  endif
;

: >number  ( ud1 c-addr1 count -- ud2 c-addr2 count )
  2swap dnumber-parse-simple 2swap
;

;; convert a character string left at addr to a signed number, using the current numeric base
: number-dbl-dpl  ( addr count -- ud 1 // d -1 // addr count false )
  ;; check length
  dup 0> ifnot false exit endif
  2dup 2>r  ;; for failure exit
  ;; ok, we have at least one char; check for a sign
  over c@ case
    [char] - of string:/char true endof
    [char] + of string:/char false endof
    otherwise drop false
  endcase nrot  ;; ( negflag addr count )
  ;; should have at least one char to work with
  dup 0> ifnot 2drop drop 2r> false exit endif
  0 u>d 2swap >number  ;; ( negflag ud addr count )
  dup ifnot  ;; no more chars
    2drop drop  ;; drop str, and convert double to single
    swap if negate endif
    -1 dpl !  2rdrop true
  else  ;; have some more chars
    over c@ [char] . = ifnot 2drop 2drop 2r> false exit endif
    string:/char dup >r >number  ;; ( negflag ud addr count | oldcount )
    if rdrop 2drop 2drop 2r> false exit endif  ;; too many chars
    r> dpl ! 2rdrop drop rot if dnegate endif
    -1
  endif
;

..: forth:(interpret-word-not-found)  ( addr count FALSE -- addr count FALSE / TRUE )
  ifnot
    number-dbl-dpl dup if
      compiler:comp? if
        0< if swap literal endif literal
      else drop endif
      true
    endif
  else false
  endif
;..

prev-defs

$IF 0
1.4 . . cr
$ENDIF

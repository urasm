;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; and now for something completely different...
;; UrForth/C Forth Engine!
;; Copyright (C) 2023 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; interactive debugger
;; optional; not included by default
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

debug:single-step@ [IF]

$include-once <linore.f>

vocabulary debugger
also-defs: debugger

;; debugger task id
0 value debugger-task
;; task the we're debugging now
0 value debuggee-stid

;; source file
0 value source-stx
;; source file name
0 value source-name-stx

false value active?

;; are we on the alt (debugger) screen now?
false value alt-scr?
true value alt-clear

;; show keycodes?
false value debug-show-keys


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; TTY helpers
;;

;; switch to alternative screen
: to-alt-scr  ( -- )
  alt-scr? ifnot
    flush-emit
    alt-clear if " \e[?1049h"
    else " \e[?1048h\e[?47h" endif
    tty:raw-type \ tty:raw-flush
    true to alt-scr?
  endif
;

;; switch to main screen
: to-main-scr  ( -- )
  alt-scr? if
    alt-clear if " \e[?1049l"
    else " \e[?47l\e[?1048l" endif
    tty:raw-type tty:raw-flush
    false to alt-scr?
  endif
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; replace standard emit
;;

: (new-norm-emit-char)  ( ch -- ch )
  lo-byte alt-scr? ifnot
    dup bl < if
      case
        9 of 9 endof
        10 of 10 endof
        13 of 13 endof
        otherwise drop [char] ?
      endcase
    else dup 127 = if drop [char] ? endif
    endif
  endif
;

debug:replace (norm-emit-char) (new-norm-emit-char)


: (debug-emit)  ( ch -- )
  ;; for alt-screen, prepend CR to LF
  alt-scr? if
    dup nl = if 13 tty:raw-emit endif
  endif
  dup tty:raw-emit
  dup nl = lastcr!
  ;; for main screen, flush on CR or LF
  alt-scr? ifnot
    dup nl = swap 10 = or if tty:raw-flush endif
  else drop
  endif
;

debug:replace (emit) (debug-emit)


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; load source code
;;

: same-source?  ( addr count -- bool )
  0 max
  source-name-stx if source-name-stx count string:=
  else 2drop false endif
;

: free-source  ( -- )
  source-stx handle:free 0-to source-stx
  source-name-stx handle:free 0-to source-name-stx
;

: load-source-file  ( addr count -- )
  free-source
  2dup handle:load-file dup to source-stx
  if
    0 handle:new to source-name-stx
    dup 8 + source-name-stx handle:size!
    dup source-name-stx !  ;; counter
    source-name-stx cell+ swap move
  else 2drop endif
;

: load-source  ( addr count -- )
  2dup same-source? if 2drop
  else dup 0> if load-source-file else 2drop endif
  endif
;


: source-c@  ( ofs -- char )
  source-stx if
    dup source-stx handle:size@ u< if
      source-stx handle:c@
      ?dup ifnot bl endif
    else drop 0
    endif
  else drop 0
  endif
;


: norm-source-ofs  ( ofs -- ofs )
  source-stx if 0 max source-stx handle:size@ min
  else drop 0 endif
;

: source-to-bol  ( ofs -- ofs )
  begin dup 1- + source-c@ dup 0= swap nl = or not-while 1- repeat
;

: source-to-eol  ( ofs -- ofs )
  begin dup source-c@ dup 0= swap nl = or not-while 1+ repeat
;

;; `ofs` is BOL
: source-prev-line  ( ofs -- ofs )
  1- source-to-bol norm-source-ofs
;

;; `ofs` is BOL
: source-next-line  ( ofs -- ofs )
  source-to-eol 1+ norm-source-ofs
;

: find-source-line  ( line -- ofs )
  0 swap 1- for source-next-line endfor
;

: .-source-line  ( ofs -- next-line-ofs )
  dup source-c@ if
    begin dup source-c@ dup 0= over nl = or not-while xemit 1+ repeat drop
    1+ cr
  endif
;

: show-source-line  ( addr count line -- )
  >r load-source
  source-stx if
    r@ 8 - find-source-line
    8 for .-source-line endfor
    ." \e[0;1m" .-source-line ." \e[0m"
    8 for .-source-line endfor
    drop
  endif
  rdrop
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; M-F5 to show main screen
;;

also-defs: linore:osc-actions

// M-F5
: 15;3~  ( -- )
  alt-scr? if to-main-scr else to-alt-scr endif
;

// F12
// switch "show keys" mode
: 24~  ( -- )
  debug-show-keys not to debug-show-keys
;

prev-defs


defer (linore-draw-prompt)

;; return TRUE if key should be eaten
: (check-key-alt)  ( flag -- FALSE / TRUE )
  drop alt-scr? ifnot to-alt-scr true else false endif
;

: (show-keycode)  ( ch -- )
  dup 0< if
    debug-show-keys if
      cr ." KEY: code: " dup .
      dup 33 127 within if ."  char: " dup xemit endif
      cr
    endif
  endif
  drop
;

: (show-osc)  ( addr count -- )
  debug-show-keys if
    cr ." OSC-KEY(" dup 0 u.r ." ): "
    xtype cr
  else 2drop endif
;

..: linore:key-processor  ( ch FALSE -- ch FALSE / TRUE )
  active? if
    dup ifnot
      (check-key-alt) if drop true else false endif
      dup ifnot >r dup (show-keycode) r> endif
    endif
  endif
;..

..: linore:osc-key-processor  ( addr count FALSE -- addr count FALSE / TRUE )
  active? if
    dup ifnot
      (check-key-alt) if 2drop true else false endif
      dup ifnot >r 2dup (show-osc) r> endif
    endif
  endif
;..

..: linore:on-before-draw  ( FALSE -- FALSE / TRUE )
  active? if
    dup ifnot
      drop alt-scr? not
      dup ifnot (linore-draw-prompt) endif
    endif
  endif
;..


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; debugger helpers
;;

: do-resume  ( -- )
  false to active?
  debuggee-stid mtask:debugger-resume
;

: do-sstep  ( -- )
  false to active?
  debuggee-stid mtask:debugger-single-step
;


: show-current-word  ( -- )
  ." \r*** CURRENT WORD: "
  debuggee-stid mtask:state-ip@ debug:ip->nfa id-count xtype cr
  debuggee-stid mtask:state-ip@ 6 u.r ." : "
  debuggee-stid mtask:state-ip@ @ ." [" 4 u.r ." ]: "
  debuggee-stid mtask:state-ip@ @ cfa->nfa id-count xtype cr

  debuggee-stid mtask:state-ip@ debug:ip->file/line
  ( ip -- addr count line TRUE / FALSE )
  if
    >r 2dup r@ show-source-line r>
    ." line " . ."  in `" xtype ." `" cr
  endif
;

: process-task-switch  ( action old-stid -- )
  to debuggee-stid drop  ;; drop action id
  flush-emit
  true to active?
  (*
  case
    -1 of ." yay, a breakpoint!" cr endof
    -2 of ." yay, a single-step!" cr endof
    otherwise " wutafuck?!" error
  endcase
  *)
  base @ 10 <> " ass!" ?error
  compiler:comp? " hole!" ?error
  true to alt-clear to-alt-scr false to alt-clear
  show-current-word
;


;; used in step-line
0 value step-from-line
0 value step-from-file-hash
0 value step-from-file-nlen
;; used in step-l-over
0 value step-curr-nfa
0 value step-over-loops


;; call before various dumps
: (newline)  ( -- )
  flush-emit cr tty:raw-flush
;

: (.push-one)  ( n -- )
  debuggee-stid mtask:state-sp@ 1+ debuggee-stid mtask:state-sp!
  0 debuggee-stid mtask:state-ds!
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; debugger commands
;;

simple-vocabulary commands
also-defs: commands

: .source  ( -- )
  show-current-word
;
alias-for .source is ?

;; dump data stack
: .stack  ( -- )
  debuggee-stid debug:dump-stack-task
;
alias-for .stack is .s

;; dump data stack
: .rstack  ( -- )
  debuggee-stid debug:dump-rstack-task
;
alias-for .rstack is .r

;; backtrace
: bt  ( -- )
   debuggee-stid debug:backtrace-task
;

;; drop value from the debuggee stack
: .drop  ( -- )
  debuggee-stid mtask:state-sp@ dup if
    1- debuggee-stid mtask:state-sp!
    debuggee-stid debug:dump-stack-task
  else drop ." ERROR: debuggee stack is empty!" cr
  endif
;

;; push value(s) to the debuggee stack
: .push  ( ... -- )
  depth if
    ;; reserve room
    depth debuggee-stid mtask:state-sp@ + debuggee-stid mtask:state-sp!
    ;; move values
    depth for i debuggee-stid mtask:state-ds! endfor
    debuggee-stid debug:dump-stack-task
  else ." ERROR: push what?" cr
  endif
;

;; clear debugger data stack
: .dclear  ( -- )
  sp0!
;

;; debugger data stack depth
: .depth  ( -- )
  ." debugger stack depth: " depth . cr
;

: continue  ( -- )
  to-main-scr tty:raw-flush
  do-resume
  process-task-switch
;
alias-for continue is c

: step  ( -- )
  to-main-scr tty:raw-flush
  do-sstep
  process-task-switch
;
alias-for step is s

: quit  ( -- )
  to-main-scr tty:raw-flush
  abort
;
alias-for quit is q

;; run current line, stop when it changed
: step-line  ( -- )
  debuggee-stid mtask:state-ip@ debug:ip->file-hash/line
  // ( ip -- len hash line TRUE / FALSE )
  if
    to step-from-line
    to step-from-file-hash
    to step-from-file-nlen
    ;; now single-step until everything is the same
    to-main-scr tty:raw-flush
    tty:set-cooked drop
    (*
      ." line: " step-from-line .
      ." hash: " step-from-file-hash .
      ." nlen: " step-from-file-nlen .
      ."   ip: " debuggee-stid mtask:state-ip@ .
      cr
    *)
    false to active?
    begin
      debuggee-stid mtask:debugger-single-step
      to debuggee-stid drop  ;; drop action id
      debuggee-stid mtask:state-ip@ debug:ip->file-hash/line
      if
        (*
          >r 2dup r@
          ." new-line: " .
          ." new-hash: " .
          ." new-nlen: " .
          ."   new-ip: " debuggee-stid mtask:state-ip@ .
          cr r>
        *)
        step-from-line =
        swap step-from-file-hash = and
        swap step-from-file-nlen = and
        ifnot break endif
      endif
    again
    flush-emit
    -3 debuggee-stid  ;; for PROCESS-TASK-SWITCH
    tty:set-raw drop
    process-task-switch
  endif
;

;; run current line, stop when we returned to the current word
: step-l-over  ( over-loops -- )
  to step-over-loops
  ;; HACK: check for FORTH:(EXIT)
  debuggee-stid mtask:state-ip@ @
  ['] forth:(exit) = if step exit endif
  debuggee-stid mtask:state-ip@ debug:ip->file-hash/line
  // ( ip -- len hash line TRUE / FALSE )
  if
    to step-from-line
    to step-from-file-hash
    to step-from-file-nlen
    debuggee-stid mtask:state-ip@ debug:ip->nfa to step-curr-nfa
    ;; now single-step until everything is the same
    to-main-scr tty:raw-flush
    tty:set-cooked drop
    false to active?
    begin
      debuggee-stid mtask:debugger-single-step
      to debuggee-stid drop  ;; drop action id
      debuggee-stid mtask:state-ip@ debug:ip->nfa
      step-curr-nfa =
      if
        debuggee-stid mtask:state-ip@ debug:ip->file-hash/line
        if
          step-over-loops if step-from-line <=
          else step-from-line = endif
          swap step-from-file-hash = and
          swap step-from-file-nlen = and
          ifnot break endif
        endif
      endif
    again
    flush-emit
    -3 debuggee-stid  ;; for PROCESS-TASK-SWITCH
    tty:set-raw drop
    process-task-switch
  endif
;

;; run one forth instruction, step when it changed
: step-over  ( -- )
  ;; HACK: check for FORTH:(EXIT)
  debuggee-stid mtask:state-ip@ @
  ['] forth:(exit) = if step exit endif
  debuggee-stid mtask:state-ip@ debug:ip->nfa to step-curr-nfa
  ;; now single-step
  to-main-scr tty:raw-flush
  tty:set-cooked drop
  false to active?
  begin
    debuggee-stid mtask:debugger-single-step
    to debuggee-stid drop  ;; drop action id
    debuggee-stid mtask:state-ip@ debug:ip->nfa
    step-curr-nfa =
  until
  flush-emit
  -3 debuggee-stid  ;; for PROCESS-TASK-SWITCH
  tty:set-raw drop
  process-task-switch
;
alias-for step-over is o

: .help  ( -- )
  ."  ? .source     -- show source code (also enter)" CR
  ." .s .stack      -- dump debuggee data stack" CR
  ." .r .rstack     -- dump debuggee return stack" CR
  ."    bt          -- show backtrace" CR
  ."    .drop       -- drop value from debuggee data stack" CR
  ."    .push       -- push values to debuggee data stack (use `:` to exec Forth in dbg)" CR
  ."    .dclear     -- clear *debugger* data stack" CR
  ."    .depth      -- print *debugger* data stack depth" CR
  ." c  continue    -- leave debugger, continue program execution" CR
  ." s  step        -- single-step (execute once Forth instruction)" CR
  ." o  step-over   -- execute over once forth instruction (rstack check)" CR
  ." q  quit        -- quit application" CR
  ."    step-line   -- step over the current line (enter subroutines)" CR
  ."    step-l-over -- ( over-loops ) step over the current line (skip subroutines)" CR
;
alias-for .help is .h

prev-defs


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; more LINORE hotkeys
;;

also-defs: linore:osc-actions

// F7
: 18~  ( -- )  commands:step-line ;
// F8
: 19~  ( -- )  false commands:step-over ;
// S-F8
: 32~  ( -- )  true commands:step-over ;

prev-defs


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; working with breakpoints
;; UNFINISHED!
;;

;; breakpoint info in user buffer
16384 forth:(addr-temp-bit) + constant bp-area
0 value bp-used
(*
  breakpoint format:
    dd  address    ; 0: unused slot
    dd  old-value  ; original value  (~0: BP not set)
*)

: (bp-rec)  ( idx -- addr )
  2 cells bp-area +
;


: bp-addr@  ( bpinfo -- addr )  @ ;
: bp-addr!  ( value bpinfo -- )  ! ;

: bp-oldval@  ( bpinfo -- oldval )  cell+ @ ;
: bp-oldval!  ( value bpinfo -- )  cell+ ! ;


: bp-alloc  ( -- bpinfo )
  bp-used for
    i (bp-rec) @ ifnot i (bp-rec) (unloop) exit endif
  endfor
  bp-used (bp-rec) +1-to bp-used
;

: bp-find  ( addr -- bpinfo / FALSE )
  bp-used for
    dup i (bp-rec) @ = if drop i (bp-rec) (unloop) exit endif
  endfor drop false
;

;; add new breakpoint
: bp-set  ( addr -- )
  dup bp-find ifnot  ;; create new breakpoint
    bp-alloc 2dup bp-addr!  0 bitnot over bp-oldval!
  else nip
  endif  ( bpinfo )
  dup bp-oldval@ 0 bitnot = ifnot  ;; not set yet
    dup bp-addr@ @ over bp-oldval! ;; save old value
    ['] debug:(bp) swap bp-addr@ ! ;; and patch the code
  else drop
  endif
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; main loop
;;

: debugger-draw-prompt  ( -- )
  " \r\e[0;1;5m>\e[0m" tty:raw-type
;

' debugger-draw-prompt to (linore-draw-prompt)

0 value run-forth-ret-addr

: run-forth-done  ( -- )
  \ ." FORTH DONE!" cr
  endcr tty:raw-flush
  compiler:exec!
  linore:reset
  rp0! run-forth-ret-addr >r
;

;; do not allow debugger to abort on bad forth word
..: forth:(interpret-word-not-found-post)  ( addr count -- addr count )
  active? if
    space xtype ." ? -- wut?!" cr
    run-forth-done
  endif
;..


;; run Forth code in debugger context.
;; this is used to execute all debugger commands, yay!
: run-forth  ( ... )
  ;; set TIB and run one line
  0 linore:line + c!  ;; finish tib with 0 byte
  linore:line drop tib ! >in 0!  ;; setup tib
  ['] run-forth-done (user-interpret-next-line) !
  r@ to run-forth-ret-addr
  (newline)
  $IF 0
    tib @ >a begin c@a while c@a xemit +1>a repeat cr
    ." ---" cr
  $ENDIF
  tty:raw-flush
  interpret
  run-forth-done
;


: debugger-main-loop ( -- )
  process-task-switch
  sp0! rp0!
  begin
    1 to linore:start-x
    tty:size drop to linore:draw-width
      \ linore:visible-amount 2* to linore:draw-width
    flush-emit linore:edit-line
    linore:accept-result 0<
    if
      linore:accept-result linore:result-esc <> if
        commands:continue
      endif
    else
      linore:line nip 0= if " .source" linore:line! endif
      only forth definitions also commands
      run-forth
    endif
  again
;

: debugger-startup  \ multitask entry point
  \ ." yay, we are in debugger!" cr
  \ ." debugger stid: " mtask:active-state . cr
  ( ... argc old-stid )
  swap " wuta?!" ?error   ;; there should be no args
  mtask:set-self-as-debugger
  0 swap mtask:yield-to   ;; return to the caller
  ['] debugger-main-loop execute-tail
;


: setup-debugger  ( -- )
  ['] debugger-startup mtask:new-state to debugger-task
  0 debugger-task mtask:yield-to  ;; init it
  debugger-task <> " shit!" ?error
  " fuck!" ?error  ;; check number of args
;


prev-defs

debugger:setup-debugger

[ENDIF]

;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; and now for something completely different...
;; UrAsm Forth Engine!
;; GPLv3 ONLY
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Z80 Assembler: label manager
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(*
  we'll do one pass, so we need to record info for all forward references.
  we could as well record *each* label reference to create cross-ref reports.
  so let's do it.
*)


vocab-if-none (asm-labels)

vocab-if-none asm-labels
also-defs: asm-labels

$DEFINE URASM-LABEL-MAN-CACHE-LAST-FILE-INFO


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; label types
enum{
  def: ltype-unknown  ;; this label was used, but not defined yet
  def: ltype-var      ;; "= value" label, variable
  def: ltype-equ      ;; "equ value" label, constant
  def: ltype-stofs    ;; structure field offset
  def: ltype-code     ;; code label
  def: ltype-data     ;; data label
}


;; fills with zeroes
: z-allot  ( size -- addr )
  dup n-allot  ( size addr )
  dup rot erase
;

;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; file info, to avoid allocating file name for each line
;;

define-accessors file-info
  def: next
  def: fname  ;; handle to file name string
  def: id
end-accessors

0 value file-info-head
$IF $URASM-LABEL-MAN-CACHE-LAST-FILE-INFO
;; cached "last used" file info, because most of the time that's what we'll hit
0 value file-info-last
$ENDIF

: file-info-init-append  ( finfo -- )
  0 (include-file-id) over file-info->id!
  0 (include-file-name) 0 handle:new-init over file-info->fname!
  file-info-head over file-info->next!
  to file-info-head
;

: file-info-name-str  ( finfo -- addr count )
  file-info->fname@ dup handle:size@
;

;; return FileInfo for the current source file
;; will reuse an existing one, or create a new one if required
: curr-file-info  ( -- file-info )
  $IF $URASM-LABEL-MAN-CACHE-LAST-FILE-INFO
  file-info-last ?dup if
    dup file-info->id@ 0 (include-file-id) = if exit endif
  drop endif
  $ENDIF
  file-info-head begin ?dup while
    dup file-info->id@ 0 (include-file-id) = if
      $IF $URASM-LABEL-MAN-CACHE-LAST-FILE-INFO
      dup to file-info-last
      $ENDIF
      exit
    endif
  file-info->next@ repeat
  ;; not found, want a new struct
  file-info-size n-allot dup file-info-init-append
  $IF $URASM-LABEL-MAN-CACHE-LAST-FILE-INFO
  dup to file-info-last
  $ENDIF
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; file reference: info and line
;;

define-accessors file-ref
  def: file   ;; reference file-info
  def: line   ;; reference file line
end-accessors

: file-ref-name-str  ( fref -- addr count )
  file-ref->file@ file-info-name-str
;

;; print file and line
: file-ref-print  ( fref -- )
  dup file-ref-name-str xtype [char] : emit
  file-ref->line@ <# #S #> xtype
;

: file-ref-init  ( fref -- )
  curr-file-info over file-ref->file!
  0 (include-file-line) swap file-ref->line!
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; main label record
;;

define-accessors label
  def: info-head  ;; label-ref
  def: nfa        ;; word nfa, to get a name
  def: type       ;; see above
  def: value      ;; label value
  def: set-at     ;; last initialised at (file-ref)
  def: exprs      ;; expressions where this label was used, but not defined
end-accessors

: label-init  ( nfa label -- ) dup >r label->nfa!  ltype-unknown r> label->type! ;

: label-defined-here  ( label -- )
  file-ref-size n-allot 2dup swap label->set-at!
  nip file-ref-init
;

: label-name  ( label -- addr count )  label->nfa@ id-count ;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; each label reference will be recorded
;; it is used both to create reference file, and to show
;; diagnostics on undefined labels
define-accessors label-ref
  def: prev       ;; next info struct addr, or 0 (label-ref)
  def: zx-pc$     ;; pc$ from asm-emit
  def: zx-pc      ;; pc from asm-emit
  def: zx-emit-pc ;; disp from asm-emit
  def: ref        ;; file reference (file-ref)
end-accessors

: label-ref-init  ( prev lref -- )
  dup >r label-ref->prev!
  file-ref-size n-allot dup r@ label-ref->ref! file-ref-init
  asm-emit:pc$ r@ label-ref->zx-pc$!
  asm-emit:pc r@ label-ref->zx-pc!
  asm-emit:emit-pc r> label-ref->zx-emit-pc!
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; now we can define the method
: label-new-ref  ( label -- )
  label-ref-size z-allot >r
  dup label->info-head@ r@ label-ref-init
  r> swap label->info-head!
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; postponed expressions (expressions with undefined labels)
;;

;; globals for fixers
0 value fix-pc
0 value fix-emit-pc
0 value fix-value

;; for temp labels
0 value postpone-expr-list

define-accessors rv-expr
  def: prev
  def: expr-cfa
  def: fixer-cfa
  def: fixer-emit-pc
  def: fixer-pc
end-accessors


: label-register-expr  ( expr-rec label -- )
  dup >r label->exprs@ over rv-expr->prev!
  r> label->exprs!
;

;; set in expression evaluator, used to attach expression to label
;; if failed with 0 here, it was temp label access
0 value expr-label-failure

;; for "expr-label-failure"
: (register-expr)  ( rv-expr -- )
  expr-label-failure if  ;; known label
    expr-label-failure label-register-expr
  else  ;; temp label
    postpone-expr-list over rv-expr->prev!
    to postpone-expr-list
  endif
;

;; for "expr-label-failure"
: remember-expr  ( fix-addr fix-emit-addr fixer-cfa expr-cfa -- )
  \ ." *** new postponed expression!" cr
  rv-expr-size n-allot >r
  r@ rv-expr->expr-cfa!
  r@ rv-expr->fixer-cfa!
  r@ rv-expr->fixer-emit-pc!
  r@ rv-expr->fixer-pc!
  r> (register-expr)
;


defer (eval-expr-cfa)  ( cfa -- res TRUE / error-code FALSE )
defer (err-undefined?)  ( error -- bool )
defer (err-message)  ( error -- addr count )

;; if set, we are resolving "@@"
;; in this calse, "get" should return current "@@" value instead of failing
false value resolve-nameless

: (count-exprs)  ( -- n )
  0 postpone-expr-list
  begin ?dup while swap 1+ swap @ repeat
;


: (resolve-list)  ( rv-expr -- )
  begin ?dup while
    dup rv-expr->prev@ >r
    dup rv-expr->expr-cfa@ (eval-expr-cfa) if  ;; resolved
      to fix-value
      dup rv-expr->fixer-emit-pc@ to fix-emit-pc
      dup rv-expr->fixer-pc@ to fix-pc
      rv-expr->fixer-cfa@ execute
    else (register-expr) endif  ;; postpone
  r> repeat
;

: label-resolve-exprs  ( label -- )
  dup label->exprs@ ?dup if
    ;; wipe list, evaluate everything, and put failed exprs to the respective labels
    0 rot label->exprs! (resolve-list)
  else drop endif
;

: resolve-nameless-exprs  ( -- )
  postpone-expr-list ?dup if
    true to resolve-nameless
    0 to postpone-expr-list
    (resolve-list)
    false to resolve-nameless
  endif
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; some globals
;;

;; last "@@"/"$$" label value
-1 value last-f/b-label

;; local labels (starting with dot) will be prefixed with this one
0 value global-prefix


create temp-name-buf 256 cell+ allot create;

: >temp-buf  ( addr count -- )
  dup 1 255 within " label name too long" ?not-error
  dup temp-name-buf ! temp-name-buf cell+ swap move
;

: temp-buf+cc  ( addr count -- )
  dup 1 255 within " label name too long" ?not-error
  dup temp-name-buf @ + dup 1 255 within " label name too long" ?not-error
  >r temp-name-buf count + swap move
  r> temp-name-buf !
;

: temp-buf-cc@  ( -- addr count )
  temp-name-buf count
;


;; "@@" or "$$"?
: f/b-label?  ( addr count -- bool )
  2 = if
    w@ dup [ [char] @ dup 8 lsh or ] imm-literal =
    swap [ [char] $ dup 8 lsh or ] imm-literal = or
  else drop false endif
;

;; "@f", "$f", "@b", "$b"?
;; 0: nope; -1: back; 1: forward
: fref-label?  ( addr count -- flag )
  2 = if
    dup c@ dup [char] @ = swap [char] $ = or if
      1+ c@ string:char-upper case
        [char] B of -1 endof
        [char] F of 1 endof
        otherwise drop 0
      endcase
    else drop 0 endif
  else drop 0 endif
;

: fref-f/b-label?  ( addr count -- bool )
  2dup f/b-label? nrot fref-label? logor
;

: fix-prefix  ( addr count -- addr count )
  dup " empty label name, wtf?" ?not-error
  2dup fref-f/b-label? ifnot
    over c@ case
      [char] . of  ;; local label (starts with a dot)
          global-prefix " local label without a global" ?not-error
          global-prefix label-name >temp-buf temp-buf+cc temp-buf-cc@
        endof
      [char] @ of  ;; "@" means "do not reset global prefix
          string:/char
          dup " invalid label name" ?not-error
          over c@ [char] @ = if
            string:/char
            dup " invalid label name" ?not-error
          endif
        endof
    endcase
  endif
;

: prefix-set?  ( addr count -- bool )
  drop c@ dup [char] @ <> swap [char] . <> and
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; find label
: find  ( addr count -- pfa TRUE / FALSE )
  vocid: (asm-labels) find-word-in-voc
  if cfa->pfa true else false endif
;


: (new-label-ref)  ( addr count -- Label )
  (create)
  here >r
  latest-nfa label-size z-allot label-init
  create;
  r>
;

;; note label reference
;; this introduces new label record if there wasn't one, but
;; will do nothing for already existing label records
: (ref)  ( addr count -- Label )
  2dup find if nrot 2drop
  else  ;; create new label word
    ;; change current vocab to labels
    current @ >r  vocid: (asm-labels) current !
    (new-label-ref)
    ;; restore old current vocab
    r> current !
  endif
;

: ref-res  ( addr count -- label TRUE / FALSE )
  2dup fref-f/b-label? ifnot (ref) dup label-new-ref true
  else 2drop false endif
;

: ref-res-nocheck  ( addr count -- label )
  (ref) dup label-new-ref
;

: ref  ( addr count -- )  ref-res if drop endif ;


: defined?  ( addr count -- bool )
  find dup if drop label->type@ ltype-unknown <> endif
;


0 value last-defined-code-label

: clear-last-defined-label  ( -- )
  0 to last-defined-code-label
;

: ltype-code-or-data?  ( ltype -- bool )  dup ltype-code = swap ltype-data = or ;

: set-last-label-as-data  ( -- )
  last-defined-code-label if
    last-defined-code-label label->type@
    ltype-code-or-data? if
      \ last-defined-code-label name xtype cr
      ltype-data last-defined-code-label label->type!
    endif
  endif
;


;; doesn't perform any checks or fixes
;; also, doesn't reset global prefix
: (define-label-simple)  ( addr count value type -- Label )
  swap >r >r (ref) ( Label | value type )
  dup label->type@
  dup ltype-unknown <> if r@ <> r@ ltype-var <> or " label redefinition" ?error
  else drop
  endif
  r@ ltype-code = if dup to last-defined-code-label endif
  r> over label->type!
  r> over label->value!
  \ dup oof:invoke: Label name xtype ." =" dup oof:invoke: Label value . cr
  dup label-defined-here
  dup label-resolve-exprs
;

: (define-label)  ( addr count value type -- )
  swap >r >r
  2dup f/b-label? if
    r@ ltype-code <> r> ltype-data <> and " invalid nameless label def" ?error
    r@ 0 65536 within " invalid nameless label value" ?not-error
    r> to last-f/b-label
    2drop
    resolve-nameless-exprs
  else
    2dup fref-label? " invalid label name" ?error
    r@ ltype-code >= if 2dup prefix-set? else false endif >r
    fix-prefix  ( addr count | value type needfix )
    r> r> r> swap rot >r (define-label-simple)
    r> if to global-prefix else drop endif
  endif
;

: define-var  ( addr count value -- )  ltype-var (define-label) ;
: define-equ  ( addr count value -- )  ltype-equ (define-label) ;
: define-code ( addr count value -- )  ltype-code (define-label) ;
: define-data ( addr count value -- )  ltype-data (define-label) ;

: define-var-simple   ( addr count value -- )  ltype-var (define-label-simple) drop ;
: define-equ-simple   ( addr count value -- )  ltype-equ (define-label-simple) drop ;
: define-code-simple  ( addr count value -- )  ltype-code (define-label-simple) drop ;
: define-data-simple  ( addr count value -- )  ltype-data (define-label-simple) drop ;
: define-stofs-simple ( addr count value -- )  ltype-stofs (define-label-simple) drop ;

: define-var-simple-res  ( addr count value -- label )  ltype-var (define-label-simple) ;


: get-value  ( lbl -- value TRUE / FALSE )
  dup if
    dup label->type@ ltype-unknown = if drop false
    else label->value@ true endif
  endif
;

;; create new label info struct, return label value (not truncated)
;; doesn't record label access
: get  ( addr count -- value TRUE / FALSE )
\ 2dup xtype cr
  2dup f/b-label? if 2drop false exit endif
  2dup fref-label? ?dup if nrot 2drop  ;; "@b" or "@f"
    resolve-nameless if ;; resolving "@@"
      0< " internal nameless resolver error" ?error -- the thing that should not be
      last-f/b-label
      dup 0 65536 within " internal nameless resolver error (2)" ?not-error -- assertion
      true
    else
      -if last-f/b-label +0if last-f/b-label true else false endif  ;; backref
      else false endif
    endif
    exit
  endif
  \ fix-prefix 2dup xtype cr
  2dup find ifnot
    ." shit: " xtype cr
    " label not found, wtf?!" error
  endif nrot 2drop
  dup label->type@ ltype-unknown = if drop false
  else label->value@ true endif
;


: dump-label-refs  ( LabelRef -- )
  begin ?dup while
    ."   ref: " dup label-ref->ref@ file-ref-print cr
    label-ref->prev@
  repeat
;

: dump-labels  ( -- )
  vocid: (asm-labels) voc-latest
  begin ?dup while
    ." === LABEL: " dup lfa->nfa id-count xtype ."  ===" cr
    dup lfa->pfa
    dup label->type@ ltype-unknown <> if
      dup label->set-at@
      ." defined at: " file-ref-print cr
    endif
    dup label->type@ ."  type: " . cr
    dup label->value@ ."  value: " . cr
    dup label->info-head@ dump-label-refs
    drop
  @ repeat
;


0 value label-count

;; check if all labels are defined
: check-labels  ( -- )
  0 to label-count
  false >r  ;; errors?
  vocid: (asm-labels) voc-latest
  begin ?dup while +1-to label-count
    dup lfa->pfa
    dup label->type@ ltype-unknown = if
      ." UNDEFINED LABEL: " over lfa->nfa id-count xtype cr
      dup label->info-head@ dump-label-refs
      rdrop true >r
    endif
    drop
  @ repeat
  label-count . ." label" label-count 1 <> if ." s" endif ."  checked." cr
  r> " undefined labels" ?error
;

prev-defs

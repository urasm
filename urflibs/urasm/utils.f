;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; and now for something completely different...
;; UrAsm Forth Engine!
;; GPLv3 ONLY
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Z80 Assembler: utilities
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


vocab-if-none asm-helpers

also asm-lexer
also-defs: asm-helpers


: dispatcher:  ( -- )  \ name
  create
;

: dsp:  ( -- )  \ checker doer
  -find-required , -find-required ,
;

: end-dispatcher ( -- )
  0 ,
 does> ( pfa )
  begin dup @ ?dup while
    execute if cell+ @ execute-tail endif 2 +cells
  repeat drop
  " invalid instruction" error
;


: (ixy-prefix,)  ( iyflag -- )
  if 0xfd else 0xdd endif asm-emit:byte
;

\ (IX+n), (IY+n) -- actually, "(i[xy][+-]"
;; emit prefix, then opcode, then displacement
;; return opcode position to patch
: (common-ixy)  ( opcode -- patch-addr )
  tok-kind 1 and (ixy-prefix,)
  asm-emit:emit-pc >r asm-emit:byte  ;; opcode will be patched later
  tok-kind asm-resw:kind-ix-add < if  ;; no displacement
    0x00 asm-emit:byte next-token
  else
    tok-kind >r next-token
    r> 2 and asm-expr:expression-disp,
    expect-`)`
  endif
  r>
;

previous prev-defs

;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; and now for something completely different...
;; UrForth/C Forth Engine!
;; Copyright (C) 2023 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


false value asm-verbose-loading
true to asm-verbose-loading


: inc<  ( -- here )  here ;
: >inc  ( here -- )  [char] ) parse " wtf?" ?not-error
                     type ." : " here swap - . " bytes" cr ;

: (inc-counted-save)  ( -- )
  r>
  context @ >r current @ >r depth >r (vsp@) >r
  here >r
  [char] : parse " wtf?" ?not-error
  0 handle:new-init >r
  >r
;

: inc-counted  ( -- )
  r> (inc-counted-save) >r
;

: inc-counted-once  ( -- )
  r> (inc-counted-save) >r
  [compile] $include-once
;

: inc-counted-done  ( -- )
  r>
  asm-verbose-loading if
    r@ dup handle:size@ xtype
    r> handle:free ." : "
    here r> - . ." bytes" cr
  else rdrop rdrop endif
  (vsp@) r> <> " unbalanced wordlist stack" ?error
  r> depth <> " unbalanced data stack" ?error
  current @ r> <> " unbalanced current vocabulary" ?error
  context @ r> <> " unbalanced context vocabulary" ?error
  >r
;

defer do-assemble-line
' (notimpl) to do-assemble-line

$IF 0
  inc-counted-once DEBUGGER: <debugger.f>
  inc-counted-done
$ENDIF
inc-counted-once THROW/CATCH: <exceptions.f>
inc-counted-done
\ inc-counted-once UROOF: <uroof.f>
\ inc-counted-done

$include-once <emit-mem.f>

get-msecs
asm-verbose-loading [IF]
.( loading UrAsm Z80 assembler...\n)
[ENDIF]
inc-counted  total:
  inc-counted-once   emitter: <emit.f>
  inc-counted-done
  inc-counted-once   reserved words: <res-words.f>
  inc-counted-done
  inc-counted-once   lexer: <lexer.f>
  inc-counted-done
  inc-counted-once   labels: <labels.f>
  inc-counted-done
  inc-counted-once   expressions: <expr.f>
  inc-counted-done
  inc-counted-once   utils: <utils.f>
  inc-counted-done
  inc-counted-once   opcodes: <opc>
  inc-counted-done
  inc-counted-once   directives: <ext>
  inc-counted-done
  inc-counted-once   conditionals: <cond-comp.f>
  inc-counted-done
  inc-counted-once   writers: <writers>
  inc-counted-done
inc-counted-done
get-msecs swap -
.( UrAsm load time: ) . .( msecs\n)

asm-emit:init-memory


$include-once <main.f>

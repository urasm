;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; and now for something completely different...
;; UrAsm Forth Engine!
;; GPLv3 ONLY
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Z80 Assembler: expression parser
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(*
  expression evaluator is done as a compiler to Forth code.
  any math expression is compiled to executable Forth code. on executing,
  this code creates label references, and THROWs on undefined label.
*)


vocab-if-none asm-expr

also asm-lexer
also-defs: asm-expr

vocab-if-none asm-funcs


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; error codes for "throw"
;;

0 value err-label-c1str

1 enum-from{
  def: error-undefined-label
  def: error-division-by-0
  def: error-invalid-shift
}

: error-undefined-label? ( code -- bool )  error-undefined-label = ;

: error-message ( code -- addr count )
  case
    error-undefined-label of " undefined label" endof
    error-division-by-0 of " division by 0" endof
    error-invalid-shift of " invalid shift" endof
    otherwise drop " unknown error"
  endcase
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; generate code for expression evaluation (low level)
;;

;; currently, nameless labels cannot be used together with named ones.
;; this is because label resoultion may fail several times, and nameless
;; label could be redefined between tries. so to play safe, i simply
;; forbid mixing different label types in one expression.
;;    "0" means "nope"
;;    "1" means "nameless label was seen"
;;   "-1" means "named label was seen"
false value was-nameless-label-ref

;; we need to wrap it this way due to how EXECUTE in CATCH works
: (run-forth-at-addr)  ( ... addr -- ... )
  forth:(forth-tail-call)
;

\ debug:decompile (run-forth-at-addr)

-1 value bc-buffer-start
-1 value bc-buffer-end

;; compiled to generated expression code
;; arg is label object address or 0
: (get-label)  ( addr count -- value )
  r> dup @ ?dup if  ( addr count ip label )
    $IF 0
      ." KNOWN: " 2over xtype cr
    $ENDIF
    2swap 2drop dup to asm-labels:expr-label-failure
    asm-labels:get-value ifnot error-undefined-label throw endif
  else nrot ( ip addr count )
    2dup asm-labels:fref-f/b-label? ifnot  ;; real label
      $IF 0
        ." REAL:" 2dup xtype cr
      $endif
      asm-labels:ref-res-nocheck ( ip lbl )
      2dup swap!  ;; save lavel info object as our argument
      dup to asm-labels:expr-label-failure
      asm-labels:get-value
      ifnot
        \ debug:backtrace
        error-undefined-label throw
      endif
    else ( ip addr count )  ;; pseudolabel, should be fully processed each time
      $IF 0
        ." PSEUDO:" 2dup xtype cr
      $endif
      0 to asm-labels:expr-label-failure
      asm-labels:get ifnot error-undefined-label throw endif
    endif
  endif
  ( ip value )
  swap cell+ >r
; compiler:(warg-lit) compiler:set-warg

: (div)  ( a b -- )  dup ifnot error-division-by-0 throw endif / ;
: (mod)  ( a b -- )  dup ifnot error-division-by-0 throw endif mod ;

: (sal)  ( a count -- )
  dup 0 32 within ifnot error-invalid-shift throw endif
  ash
;

: (sar)  ( a count -- )
  dup 0 32 within ifnot error-invalid-shift throw endif
  negate ash
;


: bc-init  ( -- )
  $IF HAS-WORD("sinopt-temp-disable") AND HAS-WORD("sinopt-temp-restore")
    ;; peephole optimiser slows down everything a lot, disable it
    sinopt-temp-disable
  $ENDIF
  here to bc-buffer-start  state 1!
;

: bc-done  ( -- )
  here to bc-buffer-end  state 0!
  $IF HAS-WORD("sinopt-temp-disable") AND HAS-WORD("sinopt-temp-restore")
    sinopt-temp-restore
  $ENDIF
;

;; throw on error
: bc-run  ( addr -- result TRUE / errorcode FALSE )
  ['] (run-forth-at-addr) catch ?dup if nip ( drop `addr` ) false
  else true endif
;

alias-for , is bc-,
alias-for compile, is bc-compile,
alias-for compile is bc-compile
alias-for literal is bc-emit-lit
alias-for strliteral is bc-emit-strlit

: bc-emit-label  ( addr count -- )
  2dup 1 = swap c@ [char] $ = and if
    2drop asm-emit:pc$ bc-emit-lit
  else
    2dup asm-labels:fref-label? if
      was-nameless-label-ref 0< " cannot mix named and nameless labels in one expression" ?error
      1 to was-nameless-label-ref
    else
      was-nameless-label-ref 0> " cannot mix named and nameless labels in one expression" ?error
      -1 to was-nameless-label-ref
    endif
    ;; we need to resolve local labels here, otherwise
    ;; postponed evaluation could pick a wrong local
    asm-labels:fix-prefix bc-emit-strlit
    bc-compile (get-label) false bc-,
  endif
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; generate code for expression evaluation (parser)
;;

defer (parse-expr)


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; terminal and unaries
;;

: term  ( -- )
  tok-type case
    tk-num of tok-num bc-emit-lit next-token endof
    tk-id of  ;; label or function
        token vocid: asm-funcs find-word-in-voc  ( cfa TRUE / FALSE )
        if next-token execute
        else token bc-emit-label next-token
        endif
      endof
    tk-str of
        tok-num 0< " unexpected string" ?error
        tok-num bc-emit-lit next-token
      endof
    (*
    tk-punct of  ;; check for "=id"
        tk-num [char] = = if
          next-token tok-type tk-id <> " macro argument name expected" ?error
          token bc-emit-macarg next-token
        endif
      endof
    *)
    otherwise
      endcr space token xtype ." ? -- wut?" cr " unknown term" error
  endcase
;

: unary  ( -- )
  tok-type tk-punct = if
    tok-num case
      [char] ( of  ;; )
          next-token (parse-expr)
          tok-type tk-punct =  tok-num [char] ) = and " unbalanced parens" ?not-error
          next-token
        endof
      [char] [ of
          next-token (parse-expr)
          tok-type tk-punct =  tok-num [char] ] = and " unbalanced parens" ?not-error
          next-token
        endof
      [char] ! of
          next-token recurse
          bc-compile not
        endof
      [char] ~ of
          next-token recurse
          bc-compile bitnot
        endof
      [char] + of
          next-token recurse
        endof
      [char] - of
          next-token recurse
          bc-compile negate
        endof
      otherwise " invalid expression (term expected)" error
    endcase
  else term
  endif
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; define binary operators table
;;

: def-op  ( prio -- prio )  \ token doer
  dup ,  ;; priority
  parse-name dup 1 2 bounds? " invalid token" ?not-error
  1 = if c@ else w@ endif ,  ;; put token code
  -find-required ,  ;; doer CFA
;

: -next-prio-  ( prio -- prio+1 )  1+ ;

1  ;; current priority
create operators
 -next-prio- dup
  def-op  * forth:*
  def-op  / (div)
  def-op  % (mod)
 -next-prio-
  def-op  + forth:+
  def-op  - forth:-
 -next-prio-
  def-op << (sal)
  def-op >> (sar)
 -next-prio-
  def-op  < forth:<
  def-op <= forth:<=
  def-op  > forth:>
  def-op >= forth:>=
 -next-prio-
  def-op  = forth:=
  def-op == forth:=
  def-op <> forth:<>
  def-op != forth:<>
 -next-prio-
  def-op  & forth:and
 -next-prio-
  def-op  ^ forth:xor
 -next-prio-
  def-op  | forth:or
 -next-prio-
  def-op && forth:(and-branch)
 -next-prio-
  def-op || forth:(or-branch)
  0 ,  ;; no more
create;
constant max-prio
constant min-binary-prio
\ min-binary-prio 2 <> " shit!" ?error


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; table-driven expression parser
;;

: (xbranch?)  ( opc -- bool )
  dup ['] forth:(and-branch) =  swap ['] forth:(or-branch) = or
;

;; find operator using the given priority and the current token
: (find-operator)  ( prio -- opcode TRUE / FALSE )
  tok-type tk-punct = if
    >r operators
    begin
      dup @ r@ = if
        dup cell+ @ tok-num = if
          rdrop 2 +cells @ true exit
        endif
      endif
      3 +cells
      dup @
    not-until rdrop
  endif
  drop false
;

: (expr)  ( prio -- )
  dup min-binary-prio < if drop unary
  else  ( prio )
    dup 1- recurse  ;; left operand
    ( prio )
    0 >r ;; save jump chain start to rstack for short-circuit logicals
    begin
      dup (find-operator)
    while
      next-token  ;; skip operator
      dup (xbranch?) if
        ;; logic op: jump over the following code
        \ bc-compile dup
        bc-compile, r> compiler:(chain-j>) >r
        \ bc-compile drop
        dup 1- recurse
      else
        over 1- recurse bc-compile,
      endif
    repeat drop
    r> compiler:(resolve-j>)
  endif
;

:noname  ( -- )
  max-prio (expr)
; to (parse-expr)


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; high-level API
;;

: (setup-lexer)  ( -- )
  \ r> asm-lexer:expand-macro-args >r >r
  \ false to asm-lexer:expand-macro-args
  false to asm-lexer:line-start
  false to was-nameless-label-ref
;

: (restore-lexer)  ( -- )
  \ r> r> to asm-lexer:expand-macro-args >r
;

;; reset expression code buffer to HERE, parse expression, generate code
: expression  ( -- )
  (setup-lexer)
  bc-init (parse-expr) bc-compile forth:(exit) bc-done
  (restore-lexer)
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; expression range checkers
;;

: (expr-check-range)  ( val lo hi addr count -- )
  >r 2over drop swap >r >r within ifnot
    r> r> r> endcr ." ERROR: " 2dup type ." : " rot . cr
    error
  else rdrop rdrop rdrop endif
;

: (expr-check-byte)  ( value -- )  -128 256 " 8-bit value out of range" (expr-check-range) ;
: (expr-check-addr)  ( value -- )  0 65536 " address out of range" (expr-check-range) ;
: (expr-check-ubyte) ( value -- )  0 256 " unsigned 8-bit value out of range" (expr-check-range) ;
: (expr-check-word)  ( value -- )  -32768 65536 " 16-bit value out of range" (expr-check-range) ;
: (expr-check-im)    ( value -- )  0 3 " invalid IM number" (expr-check-range) ;
: (expr-check-rel8)  ( value -- )  -128 128 " relative jump too long" (expr-check-range) ;
: (expr-check-bit)   ( value -- )  0 8 " invalid bit number" (expr-check-range) ;
: (expr-check-disp+) ( value -- )  -128 128 " displacement out of range" (expr-check-range) ;
: (expr-check-disp-) ( value -- )  negate -128 128 " displacement out of range" (expr-check-range) ;
: (expr-check-rst)   ( value -- )  dup 0 0x38 bounds?  swap 0x03 and 0=  and  " invalid RST address" ?not-error ;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; fixers for postponed expressions
;;

also asm-labels

: fix-addr  ( -- )
  fix-value dup (expr-check-addr)
  fix-emit-pc asm-emit:w!
;

: fix-word  ( -- )
  fix-value dup (expr-check-word)
  fix-emit-pc asm-emit:w!
;

: fix-rword  ( -- )
  fix-value dup (expr-check-word)
  fix-emit-pc asm-emit:rw!
;

: fix-byte  ( -- )
  fix-value dup (expr-check-byte)
  fix-emit-pc asm-emit:c!
;

: fix-ubyte  ( -- )
  fix-value dup (expr-check-ubyte)
  fix-emit-pc asm-emit:c!
;

: fix-disp+  ( -- )
  fix-value dup (expr-check-disp+)
  fix-emit-pc asm-emit:c!
;

: fix-disp-  ( -- )
  fix-value negate dup (expr-check-disp+)
  fix-emit-pc asm-emit:c!
;

: fix-rel8  ( -- )
  fix-value fix-pc 1+ - dup (expr-check-rel8)
  fix-emit-pc asm-emit:c!
;

: fix-im  ( -- )
  fix-value dup (expr-check-im)
  asm-emit:(im) fix-emit-pc asm-emit:c!
;

: fix-rst  ( -- )
  fix-value dup (expr-check-rst)
  fix-emit-pc asm-emit:rst-c!
;

: fix-bit  ( -- )
  fix-value dup (expr-check-bit)
  fix-emit-pc asm-emit:bit-c!
;

: put-bit  ( value -- )
  asm-emit:emit-pc asm-emit:bit-c!
;

: put-ent  ( value -- )  to asm-emit:ent ;
: fix-ent  ( -- )  fix-value dup (expr-check-addr) put-ent ;

: put-clr  ( value -- )  to asm-emit:clr ;
: fix-clr  ( -- )  fix-value dup (expr-check-addr) put-clr ;


previous


: (save-expr)  ( fixer-cfa -- )
  ( fix-addr fix-emit-addr fixer-cfa expr-cfa -- )
  asm-emit:pc asm-emit:emit-pc rot bc-buffer-start
  asm-labels:remember-expr
;

: (expr-do,)  ( cfa-check cfa-emit cfa-fix -- )
  $IF 0
    ." ===" cr
    asm-expr:bc-buffer-start asm-expr:bc-buffer-end debug:(decompile-mem)
    ." ===" cr
  $ENDIF
  bc-buffer-start bc-run if  ( cfa-check cfa-emit cfa-fix value )
    >r rot r@ swap execute
    drop r> swap execute-tail
  else  ( cfa-check cfa-emit cfa-fix error )
    dup error-undefined-label? if drop (save-expr) 0 swap execute drop
    else error-message error
    endif
  endif
;

;; expression already parsed
: expression-addr-postponed,  ( -- )
  ['] (expr-check-addr)  ['] asm-emit:addr  ['] fix-addr
  (expr-do,)
;

: expression-addr,  ( -- )
  expression expression-addr-postponed,
;

: expression-addr-rel8,  ( -- )
  expression
  bc-buffer-start bc-run if  ( value )
    asm-emit:pc 1+ -
    dup (expr-check-rel8)
    asm-emit:byte
  else
    dup error-undefined-label? if drop
      ['] fix-rel8 (save-expr)
      0 asm-emit:byte
    else error-message error
    endif
  endif
;


: expression-byte,  ( -- )  expression
  ['] (expr-check-byte)  ['] asm-emit:byte  ['] fix-byte
  (expr-do,)
;

: expression-ubyte,  ( -- )  expression
  ['] (expr-check-byte)  ['] asm-emit:byte  ['] fix-ubyte
  (expr-do,)
;

: expression-word,  ( -- )  expression
  ['] (expr-check-word)  ['] asm-emit:word  ['] fix-word
  (expr-do,)
;

: expression-rword,  ( -- )  expression
  ['] (expr-check-word)  ['] asm-emit:rword  ['] fix-rword
  (expr-do,)
;

: expression-disp+,  ( -- )  expression
  ['] (expr-check-disp+)  ['] asm-emit:byte  ['] fix-disp+
  (expr-do,)
;

: expression-disp-,  ( -- )  expression
  ['] (expr-check-disp-)  ['] asm-emit:neg-byte  ['] fix-disp-
  (expr-do,)
;

: expression-disp,  ( add-flag -- )
  if expression-disp+, else expression-disp-, endif
;

: expression-rst,  ( -- addr )  expression
  ['] (expr-check-rst)  ['] asm-emit:rst  ['] fix-rst
  (expr-do,)
;


: expression-im,  ( -- idx )  expression
  ['] (expr-check-im)  ['] asm-emit:im  ['] fix-im
  (expr-do,)
;


0 value bit-expr-addr

: expression-bit-postponed,  ( opc-base -- )
  asm-emit:byte
  bit-expr-addr to bc-buffer-start
  ['] (expr-check-bit)  ['] put-bit  ['] fix-bit
  (expr-do,)
;

: expression-bit  ( opc-base -- )  expression bc-buffer-start to bit-expr-addr ;


: expression-ent  ( -- )  expression
  ['] (expr-check-addr)  ['] put-ent  ['] fix-ent
  (expr-do,)
;

: expression-clr  ( -- )  expression
  ['] (expr-check-addr)  ['] put-clr  ['] fix-clr
  (expr-do,)
;


' bc-run to asm-labels:(eval-expr-cfa)
' error-undefined-label? to asm-labels:(err-undefined?)
' error-message to asm-labels:(err-message)


;; expression value must be defined here
: expression-const  ( -- value )
  expression
  bc-buffer-start bc-run ifnot error-message error endif
;


previous prev-defs

\ debug:dump-stack

$IF 0
: expr-test ( -- )  \ expr
  depth >r
  false to asm-lexer:line-start
  asm-lexer:next-token
  asm-expr:expression
  asm-expr:bc-buffer-start asm-expr:bc-buffer-end debug:(decompile-mem)
  asm-expr:bc-buffer-start asm-expr:bc-run
  asm-expr:bc-undo-all
  if
    ." result: " . cr
  else
    ." error: " dup . asm-expr:error-message ." (" type ." )" cr
  endif
  depth r> <> if
    debug:dump-stack
    " stack disbalance!" error
  endif
;


;; expr-test 3+4*2 || 5+label && (9+1)*2
;; expr-test 3 || 2 + $
;; expr-test 5 / 0
;; expr-test 5 > 0 && 3 < 5
;; debug:dump-stack
;; expr-test 3 + (1<<XTE_PRFLAGS_TRANS_BIT)

expr-test @b

debug:dump-stack
bye
$ENDIF

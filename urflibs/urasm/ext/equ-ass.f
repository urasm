;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; and now for something completely different...
;; UrAsm Forth Engine!
;; GPLv3 ONLY
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Z80 Assembler: EQU and "="
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


create saved-label-name 256 cell+ allot create; (hidden)

: >saved-name  ( addr count -- )
  dup saved-label-name !
  saved-label-name cell+ swap move
; (hidden)

: saved-name>  ( -- addr count )  saved-label-name count ;

: asm-equ  ( addr count -- )
  \ ." LBL-EQU: " 2dup xtype cr
  >saved-name
  allow-continuations >r true to allow-continuations
  next-token asm-expr:expression-const
  r> to allow-continuations
  saved-name> rot
  \ ." EQU: " >r 2dup xtype r> ." =" dup . cr
  asm-labels:define-equ
;

: asm-ass  ( addr count -- )
  >saved-name
  allow-continuations >r true to allow-continuations
  next-token asm-expr:expression-const
  r> to allow-continuations
  saved-name> rot
  asm-labels:define-var
;

;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; and now for something completely different...
;; UrAsm Forth Engine!
;; GPLv3 ONLY
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Z80 Assembler: DUP and INCLUDE instructions
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


also-defs: asm-helpers

false value verbose-include?

: .curr-file  ( -- )
  0 (include-file-name) xtype
;


define-accessors dup
  def: prev
  def: count
  def: line
  def: fofs
  def: clabel
end-accessors

0 value dup-curr


define-accessors inc
  def: prev
  def: dupr
end-accessors

0 value inc-curr


: (end-include)  ( -- eof-flag )
  inc-curr if
    dup-curr " unfinished DUP" ?error
    inc-curr inc->dupr@ to dup-curr
    inc-curr inc->prev@ to inc-curr
    next-token expect-eol
    refill drop
    true to line-start next-token
    false
    verbose-include? if ." continue >: " .curr-file cr endif
  else true endif
;

: do-include  ( -- )
  next-token tok-type tk-str <> " quoted file name expected" ?error
  token string:>pad next-token expect-eol
  inc-size n-allot
  inc-curr over inc->prev! to inc-curr
  dup-curr inc-curr inc->dupr!
  0 to dup-curr
  string:pad-cc@ 0 0 (include-no-refill)
  verbose-include? if ." included <: " .curr-file cr endif
  next-token expect-eol
;


: inc-dup-label  ( -- )
  dup-curr dup->clabel@ ?dup if
    dup asm-labels:label->value@
    1+ swap asm-labels:label->value!
  endif
;

: do-edup  ( -- )
  next-token expect-eol
  dup-curr " EDUP without DUP" ?not-error
  dup-curr dup->count@ 1- dup +if
    dup-curr dup->count!
    dup-curr dup->line@ 1- dup-curr dup->fofs@ (INCLUDE-LINE-SEEK)
    refill-nocross " wuta?" ?not-error parse-skip-line  ;; skip EDUP
    next-token expect-eol
    inc-dup-label
  else drop dup-curr dup->prev@ to dup-curr endif
;

: do-dup  ( -- )
  next-token asm-expr:expression-const
  ;; check for label
  token-`,`? if next-token
    token-id? " label name expected" ?not-error
    token 0 asm-labels:define-var-simple-res next-token
  else  0
  endif expect-eol
  swap
  dup 1 65536 within " invalid DUP counter" ?not-error
  dup-size n-allot
  dup-curr over dup->prev! to dup-curr
  dup-curr dup->count!
  0 (INCLUDE-FILE-LINE) dup-curr dup->line!
  (INCLUDE-LINE-FOFS) dup-curr dup->fofs!
  dup-curr dup->clabel!
;

prev-defs


also-defs: asm-instr

: INCLUDE  ( -- )  asm-helpers:do-include ;
ALIAS-FOR INCLUDE IS $INCLUDE

: EDUP  ( -- )  asm-helpers:do-edup ;

;;WARNING! THIS MUST BE THE LAST ONE!
: $DUP  ( -- )  asm-helpers:do-dup ;

prev-defs

;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; and now for something completely different...
;; UrAsm Forth Engine!
;; GPLv3 ONLY
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Z80 Assembler: ZXEmuT specific macros
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


also-defs: asm-helpers

: (ZXEmuT-opcode)  ( opc -- )
  0xed asm-emit:byte
  0xfe asm-emit:byte
  0x18 asm-emit:byte
  1 asm-emit:byte
  asm-emit:byte
  next-token expect-eop
;

prev-defs


also-defs: asm-instr

: ZXEmuT_maxspeed_on   ( -- )  0xfe asm-helpers:(ZXEmuT-opcode) ;
: ZXEmuT_maxspeed_off  ( -- )  0xff asm-helpers:(ZXEmuT-opcode) ;
: ZXEmuT_tsmark        ( -- )  0 asm-helpers:(ZXEmuT-opcode) ;
: ZXEmuT_tsdiff        ( -- )  1 asm-helpers:(ZXEmuT-opcode) ;
: ZXEmuT_bp            ( -- )  2 asm-helpers:(ZXEmuT-opcode) ;
: ZXEmuT_pause         ( -- )  3 asm-helpers:(ZXEmuT-opcode) ;

prev-defs

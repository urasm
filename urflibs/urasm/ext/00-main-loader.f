;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; and now for something completely different...
;; UrForth/C Forth Engine!
;; Copyright (C) 2023 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

also asm-lexer

$include-once <funcs.f>

$include-once <org.f>
$include-once <equ-ass.f>
$include-once <defx.f>
$include-once <incbin.f>

$include-once <printf.f>
$include-once <structs.f>

$include-once <dup-include.f>
$include-once <macro.f>

$include-once <zxemut.f>

prev-defs

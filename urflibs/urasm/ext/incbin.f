;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; and now for something completely different...
;; UrAsm Forth Engine!
;; GPLv3 ONLY
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Z80 Assembler: INCBIN instruction
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


also-defs: asm-instr

\ INCBIN name [,maxlen]
\ include binary file "as-is", possibly with max length limit.
: INCBIN  ( -- )
  next-token tok-type tk-str <> " quoted file name expected" ?error
  token false false (include-build-name) 2drop ;; already at PAD
  \ string:pad-cc@ xtype cr abort
  string:pad-cc@ handle:load-file ?dup ifnot
    ." incbin file '" string:pad-cc@ xtype ." ' not found!" cr
    " incbin file not found" error
  endif
  next-token
  token-eol? ifnot expect-`,` asm-expr:expression-const expect-eol
  else dup handle:size@
  endif
  ( stx maxlen )
  dup 0 65536 within " incbin length too big" ?not-error
  for i over handle:c@ asm-emit:byte endfor
  handle:free
;

ALIAS-FOR INCBIN IS $INCBIN


prev-defs

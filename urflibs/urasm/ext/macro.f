;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; and now for something completely different...
;; UrAsm Forth Engine!
;; GPLv3 ONLY
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Z80 Assembler: Forth macros
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(*
usage:

$FORTH
macro: xte_rstack_put_imm16  ( -- )
  mc-arg-count 1 <> " one argument expected" ?error
  mc" ld (hl),low($0)"
  mc" inc hl"
  mc" ld (hl),high($0)"
  mc" inc hl"
;
$END

xte_rstack_put_imm16 0x29a

it is ok to use "EXIT" in macro word (but not other return stack tricks).
*)


vocabulary asm-macro-helpers
also-defs: asm-macro-helpers


define-accessors marg
  def: start
  def: end
end-accessors

0 value (mac-argv)  ;; header, then margs
0 value (mac-argc)  ;; argument count
0 value (mc-buffer) ;; string buffer

;; at exit, token is comma or eol
: (skip-arg)  ( -- )
  0  ;; current paren level
  begin token-eol? not-while
    case
      token-`,`? if-of
          dup ifnot break endif
        endof
      token-`(`? if-of 1+ endof
      token-`[`? if-of 1+ endof
      token-`)`? if-of 1- 0 max endof
      token-`]`? if-of 1- 0 max endof
      token-`:`? if-of " macro cannot be combined with other instructions" error endof
      otherwise  ;; suppress "DROP"
    endcase
  next-token repeat drop
;

;; build arglist
;; token is at the first arg
: (build-args)  ( -- )
  forth:(max-handle-ofs) 1+ 0 handle:new-alloc to (mc-buffer)
  0 to (mac-argc)
  forth:(max-handle-ofs) 1+ 0 handle:new-alloc
  dup to (mac-argv)
  a>r >a
  begin token-eol? not-while
    (mac-argc) dup 255 >= " too many macro arguments" ?error
    1+ to (mac-argc)
    (tok->in) >in ! parse-skip-blanks next-token
    tib @ (tok->in) + !+4>a  ;; start
    (skip-arg)
    tib @ (tok->in) + !+4>a  ;; end
    token-eol? ifnot expect-`,` endif
  repeat r>a
;

: (free-args)  ( -- )
  (mac-argv) dup " FREE-ARGS without BUILD-ARGS" ?not-error
  handle:free 0 to (mac-argv)
  (mc-buffer) handle:free 0 to (mc-buffer)
;

: mc-arg-count  ( -- count )
  (mac-argv) " call BUILD-ARGS first" ?not-error
  (mac-argc)
;
alias-for mc-arg-count is arg#

: mc-arg-str  ( idx -- addr count )
  (mac-argv) " call BUILD-ARGS first" ?not-error
  dup 0 (mac-argc) within " invalid argument index" ?not-error
  marg-size * (mac-argv) +
  dup marg->start@ swap marg->end@ over -
;
alias-for mc-arg-str is arg@


: mc-start  ( -- )
  0 (mc-buffer) handle:used!
;

: mc+char  ( ch -- )
  (mc-buffer) " MACRO-START is not called" ?not-error
  (mc-buffer) handle:used@
  dup (mc-buffer) handle:size@ cell- u>= " macro string too long" ?error
  swap over (mc-buffer) + c!
  1+ (mc-buffer) handle:used!
;

: mc+str  ( addr count -- )
  (mc-buffer) " MACRO-START is not called" ?not-error
  dup 0< " invalid macro string" ?error
  ?dup if
    (mc-buffer) handle:used@ over +
    0 (mc-buffer) handle:size@ cell- within " macro string too long" ?not-error
    dup >r  ;; save counter
    (mc-buffer) dup handle:used@ + swap cmove
    (mc-buffer) handle:used@ r> + (mc-buffer) handle:used!
  else drop endif
;

: mc+arg  ( aidx -- )
  mc-arg-str mc+str
;

: mc-compile  ( -- )
  (mc-buffer) " MACRO-START is not called" ?not-error
  0 (mc-buffer) dup handle:used@ + c!
  (mc-buffer) tib ! >in 0!
  $IF 0
    ." <" (tib-in) 0 begin dup tib-peekch-ofs while 1+ repeat xtype ." >" cr
  $ENDIF
  next-token do-assemble-line
;

: mc-compile-str  ( addr count -- )
  mc-start mc+str mc-compile
;

;; A is string address
: (mc-parse-number)  ( count -- num count )
  >r 0 begin r@ while c@a 10 string:digit while swap 10 U* + +1>a r> 1- >r repeat
  dup 0< " numeric overflow" ?error
  r>
;

;; A is string address, at '['
: (mc-char-indexed)  ( idx count -- count )
  +1>a 1-
  c@a 10 string:digit? " number expected" ?not-error
  (mc-parse-number)  ( idx str-idx count )
  dup 0<= " `]` expected" ?error
  c@a [char] ] <> " `]` expected" ?error
  +1>a 1-
  >r >r
  mc-arg-str r@ <= " invalid string index" ?error
  r> + c@ mc+char
  r>
;

;; A is string address
: (mc-indexed)  ( idx count -- )
  dup if c@a [char] [ = if (mc-char-indexed) exit endif endif
  swap mc-arg-str mc+str
;

: (mc-")  ( addr count -- )
  dup if
    mc-start swap a>r >a
    begin ( count ) dup +while
      c@+1>a dup [char] $ <> if mc+char 1-
      else drop c@a 10 string:digit? if (mc-parse-number) (mc-indexed)
      else [char] $ mc+char 1-
      endif endif
    repeat drop r>a
    mc-compile
  else 2drop endif
;

;; compile asm string
;; use "%idx" for arg substitution
: mc"  ( -- )  ;; "
  compiler:?comp
  34 parse " string expected" ?not-error
  strliteral compile (mc-")
; immediate


: label@  ( addr count -- value )
  2dup asm-labels:get ifnot
    endcr space ." label '" xtype ." ' not defined" cr
    " label not defined" error
  else nrot 2drop endif
;

: eval-const  ( addr count -- value )
  mc-start mc+str
  0 (mc-buffer) dup handle:used@ + c!
  (mc-buffer) tib ! >in 0!
  $IF 0
    ." <" (tib-in) 0 begin dup tib-peekch-ofs while 1+ repeat xtype ." >" cr
  $ENDIF
  next-token asm-expr:expression-const
;

: eval-arg  ( idx -- value )
  mc-arg-str eval-const
;

prev-defs


vocabulary (asm-in-macro)
also-defs: (asm-in-macro)

0 value saved-current

: ;  -- end macro definition
  compiler:?comp compiler:(ctlid-colon) 2- compiler:?pairs
  saved-current " wut?!" ?not-error
  compiler:(ctlid-colon) [compile] ;
  previous previous previous saved-current current !
  0 to saved-current
; immediate

prev-defs


also-defs: forth

: MACRO:  ( -- )  \ name
  parse-name dup " macro name expected" ?not-error
  (asm-in-macro):saved-current " already defining a macro" ?error
  current @ to (asm-in-macro):saved-current
  vocid: asm-instr current !
  \ compiler:(create-forth-header)
  (create)
  also asm-lexer
  also asm-macro-helpers
  also (asm-in-macro)
  compiler:(ctlid-colon) 2- [compile] ]
 does>  ( pfa )
  tib @ >r >in @ >r
  asm-lexer:next-token  ;; skip macro name
  asm-macro-helpers:(mac-argv) >r
  asm-macro-helpers:(mac-argc) >r
  asm-macro-helpers:(mc-buffer) >r
  asm-macro-helpers:(build-args)
  forth:(forth-call)
  asm-macro-helpers:(free-args)
  r> to asm-macro-helpers:(mc-buffer)
  r> to asm-macro-helpers:(mac-argc)
  r> to asm-macro-helpers:(mac-argv)
  \ r> >in ! r> tib ! next-token  ;; restore last token
  r> >in ! r> tib ! parse-skip-line next-token
;

prev-defs

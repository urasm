;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; and now for something completely different...
;; UrForth/C Forth Engine!
;; Copyright (C) 2023 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


current @  ;; save
also asm-expr
also-defs: asm-funcs

: (one-arg)  ( -- )  expect-`(` (parse-expr) expect-`)` ; (hidden)

: (scrattr8x8)  ( x y base -- addr )
  dup 0 [ 65536 6912 - ] imm-literal within " invalid base in `scrattr8x8`" ?not-error
  6144 +
  rot dup 0 32 within " invalid x in `scrattr8x8`" ?not-error
  rot dup 0 24 within " invalid y in `scrattr8x8`" ?not-error
  32 * + +
; (hidden)

: (scraddr8x1)  ( x y base -- addr )
  dup 0 [ 65536 6144 - ] imm-literal within " invalid base in `scraddr8x1`" ?not-error
  rot dup 0 32 within " invalid x in `scraddr8x1`" ?not-error
  rot dup 0 192 within " invalid y in `scraddr8x1`" ?not-error
  dup -5 lsh 11 lsh   ;; (y/64)*2048
  over 7 and 8 lsh +  ;; (y%8)*256
  swap 63 and -3 lsh 5 lsh +  ;; ((y%64)/8)*32
  + +
; (hidden)

: (scraddr8x8)  ( x y base -- addr )
  >r 3 lsh r> (scraddr8x1)
; (hidden)

: (compile-scr-xxx-args)  ( -- )
  expect-`(` (parse-expr)
  expect-`,` (parse-expr)
  token-`,`? if next-token (parse-expr) else 0x4000 bc-emit-lit endif
  expect-`)`
; (hidden)


: low  ( -- )  (one-arg)
  0xff bc-emit-lit bc-compile forth:and
;

: high  ( -- )  (one-arg)
  -8 bc-emit-lit bc-compile lsh
  0xff bc-emit-lit bc-compile forth:and
;

: word  ( -- )  (one-arg)
  0xffff bc-emit-lit bc-compile forth:and
;

: bswap  ( -- )  (one-arg)
  bc-compile bswap16
;

: bswap32  ( -- )  (one-arg)
  bc-compile bswap32
;

: defined  ( -- )
  expect-`(`
  tok-type tk-id <> " identifier expected" ?error
  token bc-emit-strlit bc-compile asm-labels:defined?
  next-token expect-`)`
;

: scraddr8x8  ( -- )  (compile-scr-xxx-args)  bc-compile (scraddr8x8) ;
: scraddr8x1  ( -- )  (compile-scr-xxx-args)  bc-compile (scraddr8x1) ;
: scrattr8x8  ( -- )  (compile-scr-xxx-args)  bc-compile (scrattr8x8) ;

previous previous current !

;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; and now for something completely different...
;; UrAsm Forth Engine!
;; GPLv3 ONLY
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Z80 Assembler: structures
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(*
WARNING! "init_ofs" is not implemented yet!

structures
==========
STRUCT structname <flags> [,init_ofs]
...
ENDS
STRUCT extends structname [,init_ofs]
...
ENDS
STRUCT structname <flags> extends parentstructname [,init_ofs]
...
ENDS

you can extend existing struct with new fields using the second form.
this will not create a new struct, but simply append fields to the existing
one (and fix `._sizeof`).

the third form is used to "subclass" existing structure. i.e. to create a
new structure with a new name, and with all fields from the parent, followed
by new fields.

optional `init_ofs` can be used to set initial offset of the declared fields.
for extensions, it sets the initial offset of the first extension field.

field definitions:
  fldname <type>

types are:
  byte
  word
  dword  (4 bytes)
  address  (2 bytes, same as "word")
  label  (create field label, do not adjust offset)
  defs <size>

for non-label and non-ds fields it is possible to specify an initial value.
  field word = 0x29a

each struct will automatically get "structname._sizeof" label.

<flags> is a comma-separated list of flags in square brackets. optional.
currently defined flags:
  `zero_fill`: fill uinitialised struct fields with zeroes.


create anonymous struct
  structname [init]

create named struct
  label: structname [init]

for named structs, labels with fields addresses will be automatically
created. i.e. "boo: goo" will create labels "boo.fldname", etc.
note that label must be on the same line in this case.

struct initialisation consists of "name = value" pairs, delimited by
commas. all pairs must be on the same line. to create multiline inits,
use culry brackets:

structname {
  field0 = 0x29a, field1 = 666
  field1 = 69
}

opening bracket must be last at the line, and closing bracket must be
the only non-blank char in the line.
*)

;; struct info will be put into this vocab
vocab-if-none asm-structs

vocabulary asm-structs-internal
also-defs: asm-structs-internal

bitmask-enum{
  def: sflag-zero-fill
}

0x8000_0000 constant struct-no-init

;; field words pfa: offset, size, init-value

define-accessors field
  def: offset
  def: size
  def: init-val
end-accessors

define-accessors struct
  def: size
  def: flags
  def: fields-vocid
end-accessors

0 value curr-struct
0 value last-field-pfa

: next-line  ( -- )
  begin
    expect-eol
    refill-nocross " unfinished struct definition" ?not-error
    next-token
  token-eol? not-until
;

;; token is at "extends"
: find-struct  ( addr count -- stct TRUE / FALSE )
  vocid: asm-structs find-word-in-voc if cfa->pfa true
  else false endif
;

;; token is at "extends"
: find-token-struct  ( -- stct )
  next-token token-id? " struct name expected" ?not-error
  token vocid: asm-structs find-word-in-voc " extend what?" ?not-error cfa->pfa
;

: struct-extend?  ( -- bool )
  token-id? if
    token " extend" string:=ci
    token " extends" string:=ci or
  else false endif
;

: end-struct?  ( -- bool )
  token-id? if
    token " ends" string:=ci
    token " endstruct" string:=ci or
    token " end-struct" string:=ci or
  else false endif
;

: parse-field-size  ( -- size )
  token-id? " field size expected" ?not-error
  token " byte" string:=ci if 1 next-token exit endif
  token " word" string:=ci if 2 next-token exit endif
  token " dword" string:=ci if 4 next-token exit endif
  token " address" string:=ci if -2 next-token exit endif
  token " label" string:=ci if 0 next-token exit endif
  token " defs" string:=ci if
    next-token asm-expr:expression-const
    dup 0 32768 within " invalid field size" ?not-error
    exit
  endif
  " invalid field size declaration" error
;

: parse-field-init-value  ( -- )
  token-`=`? if next-token
    asm-expr:expression-const
    dup struct-no-init = " invalid field init value" ?error
    last-field-pfa field->init-val!
  endif
;

: create-fields-wordlist  ( stct -- )
  false false forth:(new-wordlist)  ;; without a hash table
  swap struct->fields-vocid!
;

;; token is at struct name
: create-new-struct-word  ( -- stct )
  token-id? " struct name expected" ?not-error
  token vocid: asm-structs find-word-in-voc " structure redefinition" ?error
  current @ vocid: asm-structs current !
  token (create) struct-size n-allot create;
  swap current !
  dup create-fields-wordlist
;

: build-size-label-name  ( -- addr count )
  curr-struct pfa->nfa id-count string:>pad " ._sizeof" string:pad+cc
  string:pad-cc@
;

: create-size-label  ( -- )
  build-size-label-name curr-struct struct->size@ asm-labels:define-stofs-simple
;

: update-size-label  ( -- )
  build-size-label-name asm-labels:find " wtf?!" ?not-error
  curr-struct struct->size@ swap asm-labels:label->value!
;

;; token is at field name
: create-field-word  ( addr count offset -- )
  current @ >r curr-struct struct->fields-vocid@ current ! >r  ( addr count | old-current offset )
  2dup current @ find-word-in-voc " structure field redefinition" ?error
  (create) here to last-field-pfa
  r> , ( ofs ) 0 , ( size ) struct-no-init , ( init-value )
  create;
  r> current !
;

;; token is at field name
: create-field-label  ( addr count offset -- )
  curr-struct pfa->nfa id-count string:>pad
  [char] . string:pad+char nrot string:pad+cc
  string:pad-cc@ rot asm-labels:define-stofs-simple
;

: set-field-size  ( size -- )
  last-field-pfa field->size!
;

: adjust-struct-size  ( delta -- )
  curr-struct struct->size +!
;

: parse-field-defs  ( -- )
  begin end-struct? not-while
    tok-type tk-label = if tk-id to tok-type endif
    token-id? " field name expected" ?not-error
    token curr-struct struct->size@ create-field-word
    token curr-struct struct->size@ create-field-label
    next-token parse-field-size
    dup set-field-size
    dup abs adjust-struct-size
    dup 1 = over 2 = or swap -2 = or if parse-field-init-value endif
    next-line
  repeat
  next-token expect-eol
;

: parse-struct-flags  ( -- )
  token-`[`? if next-token
    token-id? " flag name expected" ?not-error
    token " zero_fill" string:=ci
    token " zerofill" string:=ci or " unknown struct flag" ?not-error
    sflag-zero-fill curr-struct struct->flags or!
    next-token expect-`]`
  endif
;

: inherit-fix-size  ( stct -- )
  struct->size@ curr-struct struct->size!
;

: inherit-copy-field-data  ( field-pfa -- )
  dup field->size@ last-field-pfa field->size!
  field->init-val@ last-field-pfa field->init-val!
;

: inherit-copy-fields  ( stct -- )
  struct->fields-vocid@ voc-latest
  begin ?dup while  ( field-lfa )
    dup lfa->nfa        ( field-lfa field-nfa )
    over lfa->pfa @ >r  ( field-lfa field-nfa | ofs )
    dup id-count r@ create-field-word
    id-count r> create-field-label
    dup lfa->pfa inherit-copy-field-data
  @ repeat
;

;; token is at "extends"
: struct-inherit  ( -- )
  find-token-struct dup curr-struct = " struct cannot extend itself" ?error
  dup inherit-fix-size
  inherit-copy-fields
  next-token
;

;; current token is at <flags>
: new-struct  ( -- )
  struct-extend? if find-token-struct to curr-struct next-token
  else  ;; definitely a new one
    create-new-struct-word to curr-struct next-token
    parse-struct-flags struct-extend? if struct-inherit endif
    create-size-label
  endif
  next-line
  parse-field-defs
  update-size-label
  $IF 0
    curr-struct pfa->nfa id-count xtype cr
    curr-struct struct->fields-vocid@ vocid-words
  $ENDIF
  0 to curr-struct
;


0 value curr-struct-pc
0 value curr-struct-emit-pc

: alloc-struct  ( stct -- )
  asm-emit:emit-pc to curr-struct-emit-pc
  asm-emit:pc to curr-struct-pc
  dup struct->flags@ sflag-zero-fill and if
    struct->size@ for 0 asm-emit:byte endfor
  else
    struct->size@ asm-emit:+>pc
  endif
;


: save-set-pc  ( fofs -- | curr-emit-pc curr-pc )
  r> asm-emit:emit-pc >r asm-emit:pc >r >r
  curr-struct-pc to asm-emit:pc
  curr-struct-emit-pc to asm-emit:emit-pc
  asm-emit:+>pc
;

: restore-pc  ( | curr-emit-pc curr-pc -- )
  r> r> to asm-emit:pc r> to asm-emit:emit-pc >r
;

: fval-c!  ( value fofs -- )
  over -128 256 within " field init value out of bounds" ?not-error
  save-set-pc asm-emit:emit-pc asm-emit:c!-ok restore-pc
;

: fval-w!  ( value fofs -- )
  over -32768 65536 within " field init value out of bounds" ?not-error
  save-set-pc asm-emit:emit-pc asm-emit:w!-ok restore-pc
;

: fval-dw!  ( value fofs -- )
  save-set-pc asm-emit:emit-pc asm-emit:dw!-ok restore-pc
;

: fval!  ( value ofs size -- )
  case
    1 of fval-c! endof
    2 of fval-w! endof
   -2 of fval-w! endof -- address
    4 of fval-dw! endof
    otherwise " cannot set non-standard field value" error
  endcase
;

: fval-blank  ( size -- )
  0 asm-emit:emit-pc rot case
    1 of asm-emit:c!-ok endof
    2 of asm-emit:w!-ok endof
   -2 of asm-emit:w!-ok endof -- address
    4 of asm-emit:dw!-ok endof
    otherwise " cannot set non-standard field value" error
  endcase
;

: set-default-fvals  ( -- )
  curr-struct struct->fields-vocid@ voc-latest
  begin ?dup while
    dup lfa->pfa field->init-val@ dup struct-no-init <> if  ( lfa initval )
      over lfa->pfa dup field->offset@ swap field->size@  ( lfa initval ofs size )
      fval!
    else drop endif
  @ repeat
;

;; "curr-struct"
: parse-uinit-line  ( -- )
  begin token-eol? not-while
    token-`,`? if next-token
    else
      tok-type tk-id <> " field name expected" ?error
      token curr-struct struct->fields-vocid@ find-word-in-voc ifnot
        endcr space token xtype ." ? -- unknown field in struct '"
        curr-struct pfa->nfa id-count xtype ." '" cr
        $IF 0
          curr-struct struct->fields-vocid@ vocid-words
        $ENDIF
        " unknown field" error
      endif
      cfa->pfa
      next-token expect-`=`
      dup field->size@ swap field->offset@  ( size ofs )
      save-set-pc
      dup fval-blank
      case
        1 of asm-expr:expression-byte, endof
        2 of asm-expr:expression-word, endof
       -2 of asm-expr:expression-addr, endof
        \ 4 of fval-dw! endof -- TODO
        otherwise " cannot set non-standard field value" error
      endcase
      restore-pc
      token-eol? ifnot expect-`,` endif
    endif
  repeat
;

: parse-uinit  ( -- )
  token-`{`? if next-token
    begin next-line token-`}`? not-while parse-uinit-line repeat
    expect-`}` expect-eol \ next-line
  else parse-uinit-line endif
;

: declare-anon-struct  ( stct -- )
  dup to curr-struct
  $IF 0
    ." ***ANON: " curr-struct pfa->nfa id-count xtype cr
  $ENDIF
  alloc-struct
  set-default-fvals
  next-token token-eol? ifnot parse-uinit else expect-eol endif
;

: declare-named-struct-labels  ( stct -- )
  dup >r  ;; for "._sizeof"
  string:pad-cc@ asm-emit:pc asm-labels:define-data-simple  ;; main label
  struct->fields-vocid@ voc-latest
  begin ?dup while
    dup lfa->pfa field->offset@ asm-emit:emit-pc +  ( lfa ofs )
    pad @ >r  ;; save length
    [char] . string:pad+char over lfa->nfa id-count string:pad+cc
    string:pad-cc@ rot asm-labels:define-stofs-simple  ;; field label
    r> pad !  ;; restore length
  @ repeat
  ;; "._sizeof" label
  " ._sizeof" string:pad+cc string:pad-cc@ r> struct->size@ asm-labels:define-data-simple
;

: declare-labeled-struct  ( stct -- )
  dup declare-named-struct-labels
  declare-anon-struct
;

prev-defs


also-defs: asm-instr

: STRUCT   ( -- )  next-token asm-structs-internal:new-struct ;

prev-defs

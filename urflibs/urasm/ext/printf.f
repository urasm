;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; and now for something completely different...
;; UrAsm Forth Engine!
;; GPLv3 ONLY
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Z80 Assembler: $PRINTF and company (formatted printing)
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


also-defs: asm-helpers

true value printf-enabled?

pad 23552 + constant fmt-str-buf
0 value fmt-addr
0 value fmt-count
0 value fmt-numarg
create fmt-char-str 1 allot create;

: fmt-peekch  ( -- char )
  fmt-count +if fmt-addr c@ else 0 endif
;

: fmt-getch  ( -- char )
  fmt-count +if fmt-addr c@ +1-to fmt-addr -1-to fmt-count
  else 0 endif
;

: fmt-collect-numarg  ( char -- next-char )
  0 >r begin fmt-getch dup 10 string:digit while r> 10 U* + >r drop repeat
  r> to fmt-numarg
;

: fmt-get-num-arg  ( -- num )
  token-str? " expect numeric or label arg" ?error
  asm-expr:expression-const
  (*
  token-num? if tok-num true
  else
    token-id? " invalid %s arg" ?not-error
    ;; label value
    token asm-labels:get
  endif
  next-token
  *)
;


simple-vocabulary fmt-commands
also-defs: fmt-commands

: s  ( -- )
  expect-`,`
  token-str? if token xtype next-token exit endif
  fmt-get-num-arg 0 .r
;

: d  ( -- )
  expect-`,`
  fmt-get-num-arg fmt-numarg .r
;

: x  ( -- )
  expect-`,`
  fmt-get-num-arg
  base @ >r hex
  <# fmt-numarg 1 max 32 min for # endfor
  dup if #S endif #> xtype
  r> base !
;


prev-defs

;; we cannot store format string at PAD, because PAD may be used by other words.
;; use PAD+23552 instead.
;; lexer shoult be at the formatting string.
: (process-printf)  ( -- )
  token-str? " format string expected" ?not-error
  token to fmt-count drop fmt-str-buf to fmt-addr
  token fmt-addr swap move
  allow-continuations >r true to allow-continuations
  next-token
  begin fmt-getch ?dup while
    dup [char] % = ifnot xemit
    else
      drop fmt-collect-numarg
      fmt-char-str c!
      fmt-char-str 1 vocid: fmt-commands find-word-in-voc if execute
      else " invalid formatting command" error
      endif
    endif
  repeat
  next-token expect-eop
  r> to allow-continuations
  cr
;

prev-defs


also-defs: asm-instr

: $PRINTF   ( -- )
  asm-helpers:printf-enabled? if
    next-token asm-helpers:(process-printf)
  else parse-skip-line next-token endif
;

: $WARNING  ( -- )
  ." <" (tib-in) 0 begin dup tib-peekch-ofs while 1+ repeat xtype ." >" cr
  next-token asm-helpers:(process-printf)
  " user warning" asm-lexer:warning
;

: $ERROR  ( -- )
  ." <" (tib-in) 0 begin dup tib-peekch-ofs while 1+ repeat xtype ." >" cr
  next-token asm-helpers:(process-printf)
  abort
;


prev-defs

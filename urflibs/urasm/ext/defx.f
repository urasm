;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; and now for something completely different...
;; UrAsm Forth Engine!
;; GPLv3 ONLY
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Z80 Assembler: DEFx instructions
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


also-defs: asm-helpers

: (emit-str)  ( -- )
  token for dup c@ asm-emit:byte 1+ endfor drop
;

: (emit-strz)  ( -- )
  (emit-str) 0 asm-emit:byte
;

: (emit-strc)  ( -- )
  tok-len dup 255 > " string too long" ?error asm-emit:byte
  (emit-str)
;

: (emit-strx)  ( -- )
  tok-len " string cannot be empty" ?not-error
  (emit-str) 0x80 asm-emit:emit-pc 1- asm-emit:or-c!
;

: (defb-x) ( stremit -- )
  >r asm-labels:set-last-label-as-data
  begin
    next-token
    tok-type tk-str = if r@ execute next-token
    else asm-expr:expression-byte, endif
  token-`,`? not-until rdrop
;

: (defb) ( -- )  ['] (emit-str) (defb-x) ;
: (defz) ( -- )  ['] (emit-strz) (defb-x) ;
: (defc) ( -- )  ['] (emit-strc) (defb-x) ;
: (defx) ( -- )  ['] (emit-strx) (defb-x) ;

: (defw) ( -- )
  asm-labels:set-last-label-as-data
  begin next-token asm-expr:expression-word,
  token-`,`? not-until
;

: (defr) ( -- )
  asm-labels:set-last-label-as-data
  begin next-token asm-expr:expression-rword,
  token-`,`? not-until
;

: (defs) ( -- )
  asm-labels:set-last-label-as-data
  begin
    next-token asm-expr:expression-const
    dup 0 65536 within " invalid DEFS counter" ?not-error
    token-`,`? if  ;; fill with byte
      expect-`,` asm-expr:expression-const
      swap for dup asm-emit:byte endfor drop
    else  ;; skip counter
      asm-emit:+>pc
    endif
  token-`,`? not-until
;


previous


also-defs: asm-instr

: DEFB  ( -- )  asm-helpers:(defb) ;
: DB    ( -- )  asm-helpers:(defb) ;
: DEFW  ( -- )  asm-helpers:(defw) ;
: DW    ( -- )  asm-helpers:(defw) ;
: DEFR  ( -- )  asm-helpers:(defr) ;
: DEFS  ( -- )  asm-helpers:(defs) ;
: DEFM  ( -- )  asm-helpers:(defb) ;
: DEFZ  ( -- )  asm-helpers:(defz) ;
: DEFC  ( -- )  asm-helpers:(defc) ;
: DEFX  ( -- )  asm-helpers:(defx) ;

prev-defs

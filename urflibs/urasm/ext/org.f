;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; and now for something completely different...
;; UrAsm Forth Engine!
;; GPLv3 ONLY
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Z80 Assembler: ORG and DISP instructions
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


also-defs: asm-helpers

: (fix-ent-clr)  ( -- )
  asm-emit:ent -if asm-emit:pc to asm-emit:ent endif
  asm-emit:clr -if asm-emit:pc 1- to asm-emit:clr endif
;

previous


also-defs: asm-instr

: ENT  ( -- )  next-token asm-expr:expression-ent ;
: CLR  ( -- )  next-token asm-expr:expression-clr ;
\ ALIAS-FOR CLR IS CLEAR

: ORG  ( -- )
  next-token asm-expr:expression-const
  dup 0 65536 within " invalid ORG address" ?not-error
  dup to asm-emit:pc to asm-emit:emit-pc
  asm-helpers:(fix-ent-clr)
;

: DISP  ( -- )
  next-token asm-expr:expression-const
  dup 0 65536 within " invalid DISP address" ?not-error
  to asm-emit:pc
  asm-helpers:(fix-ent-clr)
;
ALIAS-FOR DISP IS PHASE

: ENDDISP  ( -- )
  asm-emit:emit-pc to asm-emit:pc
  next-token
;
ALIAS-FOR ENDDISP IS ENDPHASE

: ALIGN  ( -- )
  next-token asm-expr:expression-const
  dup 0 32769 within " invalid ALIGN value" ?not-error
  ?dup if
    asm-emit:pc over 1- + over / *
    asm-emit:pc - asm-emit:+>pc
    asm-helpers:(fix-ent-clr)
  endif
;


prev-defs

;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; and now for something completely different...
;; UrAsm Forth Engine!
;; GPLv3 ONLY
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Z80 Assembler: main module
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


\ $DEFINE DEBUG-URASM-DUMP-READ-LINE

enum{
  def: out-none
  def: out-sna48
  def: out-sna128
  def: out-raw
}

out-sna48 value out-format
false value out-locked

false value do-org-100

0 value out-base-name  ;; handle


also asm-lexer
also-defs: asm-instr

0 value ret-forth (hidden)
0 value ret-forth-rp (hidden)

: (done-forth)  ( -- )
  ret-forth-rp 0 mtask:state-rp! rdrop
  ret-forth >r
; (hidden)

..: forth:(interpret-check-word)  ( addr count FALSE -- addr count FALSE / TRUE )
  ret-forth if
    dup ifnot drop
      \ 2dup " $END-FORTH" string:=ci if (done-forth) endif
      2dup " $END_FORTH" string:=ci if (done-forth) endif
      2dup " $END" string:=ci if (done-forth) endif
      false
    endif
  endif
;..

: (do-forth)  ( -- )
  r@ to ret-forth
  0 mtask:state-rp@ to ret-forth-rp
  interpret
; (hidden)

: $START_FORTH  ( -- )
  (do-forth) sp0!
  parse-skip-line next-token
;

: $FORTH  ( -- )
  (do-forth) sp0!
  parse-skip-line next-token
;


: DEFFMT  ( -- )
  next-token tok-type tk-id <> " identifier expected" ?error
  out-locked ifnot
    token " sna" string:=ci if out-sna48 to out-format next-token exit endif
    token " sna48" string:=ci if out-sna48 to out-format next-token exit endif
    token " sna128" string:=ci if out-sna128 to out-format next-token exit endif
    token " raw" string:=ci if out-raw to out-format next-token exit endif
    token " bin" string:=ci if out-raw to out-format next-token exit endif
    token " none" string:=ci if out-none to out-format next-token exit endif
    token " null" string:=ci if out-none to out-format next-token exit endif
    endcr ." unknown output format: " token xtype cr
    " unknown output format" error
  else next-token endif
;
ALIAS-FOR DEFFMT IS $DEFFMT


previous prev-defs



;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; main assembler loop
;;

also asm-lexer

: assemble-token-punct  ( -- )
  token-`:`? ifnot dump-token " bad syntax" error endif
  next-token
  asm-labels:clear-last-defined-label
;

: assemble-token-label  ( as-label? -- want-end-of-instr? )
  >r  ;; save "as-label?"
  token asm-structs-internal:find-struct if
    r@ if tok-label-colon? " declare structs without a colon, please" ?error endif
    asm-structs-internal:declare-anon-struct
    true
  else
    token string:>pad next-token
    case
      token-equ? if-of string:pad-cc@ asm-equ true endof
      token-`=`? if-of string:pad-cc@ asm-ass true endof
      token-id? if-of
          token asm-structs-internal:find-struct if
            asm-structs-internal:declare-labeled-struct
            true
          else
            string:pad-cc@ r@ ifnot
              endcr space xtype ." ? wut?" cr
              " dunno what to do" error
            endif
            asm-emit:pc$ asm-labels:define-code
            false
          endif
        endof
      otherwise
        string:pad-cc@ r@ ifnot
          endcr space xtype ." ? wut?" cr
          " dunno what to do" error
        endif
        asm-emit:pc$ asm-labels:define-code
        false
    endcase
  endif rdrop
;

: assemble-token-mnemo  ( -- )
  tok-mnemo-cfa execute
  token-colon-eol? " end of instruction expected" ?not-error
  asm-labels:clear-last-defined-label
;

;; first token must be ready
: assemble-line  ( -- )
  true to line-start
  begin token-eol? not-while
    asm-emit:instruction-start
    $IF 0
      ." 000: " dump-token
    $ENDIF
    depth >r
    tok-type case
      tk-punct of assemble-token-punct false endof
      tk-label of true assemble-token-label endof
      tk-id of false assemble-token-label endof  ;; this can be " lbl = ..." or " lbl equ ..."
      tk-mnemo of assemble-token-mnemo true endof
      otherwise dump-token " wutafuck?" error
    endcase
    depth 1- r> <> " ASSEMBLER: unbalanced stack" ?error
    $IF 0
      ." 001: " dump-token
    $ENDIF
    \ token-eol? if ." !!!\n" drop break endif
    if token-`:`? token-eol? or " end of instruction expected" ?not-error next-token endif
  repeat
;

' assemble-line to do-assemble-line


(*
current @
also-defs: asm-helpers

: asm-line  ( addr count -- )
  dup 0< " invalid line length in ASM-LINE" ?error
  ?dup if
    tib @ >r (tok->in) >r
    0 handle:new-strz tib ! >in 0!
    next-token
    assemble-line
    tib @ handle:free
    r> >in ! r> tib ! next-token  ;; restore token
  else drop endif
;

previous current !
*)


: assemble-file  ( addr count -- )
  dup +if false false (include) else 2drop endif
  true to line-start next-token
  ." assembling: " asm-helpers:.curr-file cr
  out-base-name ifnot
    0 (include-file-name) 2drop
    string:pad-remove-ext
    \ string:pad-cc@ xtype cr
    string:pad-cc@ 0 handle:new-init to out-base-name
    \ out-base-name dup handle:size@ xtype cr
  endif
  get-msecs >r
  begin
    \ true to expand-macro-args  \ just in case
    begin
      token-eol?
    while
      \ ." *** NEWLINE ***: " 0 (INCLUDE-FILE-NAME) xtype 0 (INCLUDE-FILE-LINE) space . cr
      refill-nocross ifnot break endif
      true to line-start
      $IF $DEBUG-URASM-DUMP-READ-LINE
        ." <" (tib-in) 0 begin dup tib-peekch-ofs while 1+ repeat xtype ." >" cr
      $ENDIF
      true to line-start next-token
    repeat
    asm-emit:instruction-start
    $IF 0
      dump-token
    $ENDIF
    token-eol? if
      asm-labels:clear-last-defined-label
      asm-helpers:(end-include)
      if break endif
    else assemble-line endif
  again
  get-msecs r> - ." assembled in " . ." msecs." cr
;

previous


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; utilities
;;

: $LABEL-DATA:  ( -- )  \ name
  parse-name dup " label name expected" ?not-error
  asm-emit:pc asm-labels:define-data
;

: $LABEL-CODE:  ( -- )  \ name
  parse-name dup " label name expected" ?not-error
  asm-emit:pc asm-labels:define-code
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; utilities
;;

: write-sna  ( addr count wr-cfa -- )
  >r
  string:>pad string:pad-remove-ext " .sna" string:pad+cc
  ." OUTPUT: " string:pad-cc@ xtype cr
  string:pad-cc@ files:create " cannot create output .SNA" ?not-error
  dup r> execute
  files:close drop
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; main entry point
;;

0 value arg-idx
-1 value arg-fname-idx

: next-arg  ( -- )  +1-to arg-idx ;

vocabulary cli-args
also-defs: cli-args

: --raw  ( -- )  out-raw to out-format true to out-locked next-arg ;
: --sna  ( -- )  out-sna48 to out-format true to out-locked next-arg ;
alias-for --sna is --sna48
: --sna128  ( -- )  out-sna128 to out-format true to out-locked next-arg ;
: --none  ( -- )  out-none to out-format true to out-locked next-arg ;
: --org100h  ( -- )  true to do-org-100 next-arg ;

: --disable-printf  ( -- )  false to asm-helpers:printf-enabled? next-arg ;

: --out-base-name  ( -- )
  out-base-name handle:free
  next-arg
  arg-idx < argv " where's name?" ?not-error
  arg-idx argv 0 handle:new-init to out-base-name
;

: --reffile  ( -- )  next-arg ;
: --vinc  ( -- )  true to asm-helpers:verbose-include? next-arg ;

prev-defs

: parse-arg  ( -- )
  arg-idx argv dup if
    vocid: cli-args find-word-in-voc if
      execute
    else
      arg-fname-idx 0>= " too many file names" ?error
      arg-idx to arg-fname-idx
      next-arg
    endif
  else 2drop next-arg endif
;


: RUN-URASM  ( -- )
  begin arg-idx argc < while parse-arg repeat
  do-org-100 if 0x100
    dup to asm-emit:pc dup to asm-emit:emit-pc
    dup to asm-emit:ent 1- to asm-emit:clr
  endif
  arg-fname-idx 0< if
    " ." false false (include)
    \ " assemble what?" ?error
    0 0
  else
    arg-fname-idx argv 2drop
    string:pad-remove-ext
    \ string:pad-cc@ xtype cr
    string:pad-cc@ 0 handle:new-init to out-base-name
    \ out-base-name dup handle:size@ xtype cr
    arg-fname-idx argv
  endif
  ;; do not optimise macros, optimisator is fairly slow
  $IF HAS-WORD("(sinopt)")
    (sinopt):deactivate
  $ENDIF
  assemble-file
  \ asm-labels:dump-labels
  asm-labels:check-labels
  out-format case
    out-none of endof
    out-sna48 of out-base-name dup handle:size@ ['] asm-writers:write-sna-48 write-sna endof
    out-sna128 of out-base-name dup handle:size@ ['] asm-writers:write-sna-128 write-sna endof
    out-raw of out-base-name dup handle:size@ asm-writers:write-raw endof
    otherwise " internal error: bad output format" error
  endcase
;


also-defs: asm-instr
;;WARNING! THIS MUST BE THE LAST ONE!
ALIAS-FOR $DUP IS DUP

;;WARNING! THOSE MUST BE THE LATEST ONES!
: else  " please, use $ELSE" error ;
: if  " please, use $IF" error ;
: elsif  " please, use $ELSIF" error ;
: elif  " please, use $ELSIF" error ;
: endif  " please, use $ENDIF" error ;

prev-defs

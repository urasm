;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; and now for something completely different...
;; UrAsm Forth Engine!
;; GPLv3 ONLY
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Z80 Assembler: CALL instruction
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


also-defs: asm-helpers

\ CALL addr
: (call-addr)  ( -- )
  0o315 asm-emit:byte
  asm-expr:expression-addr,
;

\ CALL cc,addr
: (call-cond)  ( -- )
  expect-cond expect-`,`
  3 lsh 0o304 or asm-emit:byte
  asm-expr:expression-addr,
;

dispatcher: (dsp-call)
  dsp: token-cond?   (call-cond)
  dsp: true          (call-addr)
end-dispatcher


prev-defs


also-defs: asm-instr

: CALL ( -- )  asm-lexer:next-token asm-helpers:(dsp-call) ;

prev-defs

;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; and now for something completely different...
;; UrAsm Forth Engine!
;; GPLv3 ONLY
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Z80 Assembler: ALU instructions
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ALU helpers
;; lexer is at the first operand
;;

also asm-resw
also-defs: asm-helpers

0 value alu-opcode


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ALU r8
;;

\ ALU [A,] r8
: (alu-a-r8)  ( -- )
  alu-opcode tok-kind or asm-emit:byte
  next-token
;

\ ALU [A,] imm8
: (alu-a-imm8)  ( alu-code -- )
  alu-opcode 0o306 or asm-emit:byte
  asm-expr:expression-byte,
;

\ ALU [A,] <x|y><l|h>
: (alu-a-xylh)  ( -- )
  tok-kind 2 and (ixy-prefix,)
  tok-kind 1 and kind-h + alu-opcode or asm-emit:byte
  next-token
;

\ ALU [A,] (i<xy>+...)
: (alu-a-(ixy))  ( -- )
  kind-(hl) alu-opcode or (common-ixy) drop
;

dispatcher: (dsp-alu-r8)
  dsp: token-r8?     (alu-a-r8)
  dsp: token-(ixy)?  (alu-a-(ixy))
  dsp: token-rx8?    (alu-a-xylh)
  dsp: true          (alu-a-imm8)
end-dispatcher

: (alu-r8-common)  ( alu-code -- )
  to alu-opcode
  ;; skip "A,", it is optional
  token-regA? if
    next-token token-`,`? ifnot  ;; alu A,something
      kind-a alu-opcode or asm-emit:byte
      next-token exit
    endif
    expect-`,`
  endif
  (dsp-alu-r8)
;

: (alu-r8-common-drop)  ( alu-code pfx<<8+opcode -- )  drop (alu-r8-common) ;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ALU r16
;;

: (alu-lhx-r16-common)  ( alu-code pfx<<8+opcode -- )  nip
  dup -8 lsh ?dup if asm-emit:byte endif
  next-token expect-`,`
;

: (alu-r16-finish)  ( pfx<<8+opcode -- )
  expect-r16
  dup kind-sp > " invalid instruction" ?error
  4 lsh or asm-emit:byte
;

\ ALU HL,r16
: (alu-hl-r16)  ( alu-code pfx<<8+opcode -- )
  (alu-lhx-r16-common) (alu-r16-finish)
;

: (alu-ixy-r16) ( alu-code pfx<<8+opcode -- )
  dup 0xff > " invalid instruction" ?error
  tok-kind dup >r
  1 and (ixy-prefix,) (alu-lhx-r16-common)
  token-ixy? if
    r> tok-kind <> " invalid instruction" ?error
    kind-hl 4 lsh or asm-emit:byte
    next-token
  else rdrop (alu-r16-finish)
  endif
;

dispatcher: (dsp-adcx)  ( simple-opcode pfx<<8+opcode -- )
  dsp: token-hl?  (alu-hl-r16)
  dsp: token-ixy? (alu-ixy-r16)
  dsp: true       (alu-r8-common-drop)
end-dispatcher

previous prev-defs


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; LD main
;;

also-defs: asm-instr

: ADD  ( -- )  next-token 0o200 0o011 asm-helpers:(dsp-adcx) ;
: ADC  ( -- )  next-token 0o210 [ 0xed00 0o112 or ] imm-literal asm-helpers:(dsp-adcx) ;
: SUB  ( -- )  next-token 0o220 asm-helpers:(alu-r8-common) ;
: SBC  ( -- )  next-token 0o230 [ 0xed00 0o102 or ] imm-literal asm-helpers:(dsp-adcx) ;
: AND  ( -- )  next-token 0o240 asm-helpers:(alu-r8-common) ;
: XOR  ( -- )  next-token 0o250 asm-helpers:(alu-r8-common) ;
: OR   ( -- )  next-token 0o260 asm-helpers:(alu-r8-common) ;
: CP   ( -- )  next-token 0o270 asm-helpers:(alu-r8-common) ;

prev-defs

;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; and now for something completely different...
;; UrAsm Forth Engine!
;; GPLv3 ONLY
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Z80 Assembler: BIT, RES, SET, RLx instructions
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


also-defs: asm-helpers

\ rlx r8
: (rlx-r8)  ( opcode-base -- )
  0xcb asm-emit:byte
  tok-kind or asm-emit:byte
  next-token
;

\ rlx xylh
: (rlx-xylh)  ( opcode-base -- )
  tok-kind 1 and (ixy-prefix,)
  0xcb asm-emit:byte
  tok-kind asm-resw:kind-h + or asm-emit:byte
  next-token
;

\ rlx (ixy)
: (rlx-(ixy))  ( opcode-base -- )
  0xcb (common-ixy) drop
  asm-resw:kind-(hl) or asm-emit:byte
;

dispatcher: (dsp-rlx)
  dsp: token-r8?     (rlx-r8)
  dsp: token-rx8?    (rlx-xylh)
  dsp: token-(ixy)?  (rlx-(ixy))
end-dispatcher

: (rlx)  ( opcode-base -- )  (dsp-rlx) ;


\ brs r8
: (brs-r8)  ( opcode-base -- )
  0xcb asm-emit:byte
  tok-kind or asm-expr:expression-bit-postponed,
  next-token
;

\ brs xylh
: (brs-xylh)  ( opcode-base -- )
  tok-kind 1 and (ixy-prefix,)
  0xcb asm-emit:byte
  tok-kind asm-resw:kind-h + or asm-expr:expression-bit-postponed,
  next-token
;

\ brs (ixy)
: (brs-(ixy))  ( opcode-base -- )
  0xcb (common-ixy) drop
  asm-resw:kind-(hl) or asm-expr:expression-bit-postponed,
;

dispatcher: (dsp-brs)
  dsp: token-r8?     (brs-r8)
  dsp: token-rx8?    (brs-xylh)
  dsp: token-(ixy)?  (brs-(ixy))
end-dispatcher


: (brs)  ( opcode-base -- )
  asm-expr:expression-bit
  expect-`,` (dsp-brs)
;

prev-defs


also-defs: asm-instr

: BIT ( -- )  asm-lexer:next-token 0o100 asm-helpers:(brs) ;
: RES ( -- )  asm-lexer:next-token 0o200 asm-helpers:(brs) ;
: SET ( -- )  asm-lexer:next-token 0o300 asm-helpers:(brs) ;

: RLC ( -- )  asm-lexer:next-token 0o000 asm-helpers:(rlx) ;
: RRC ( -- )  asm-lexer:next-token 0o010 asm-helpers:(rlx) ;
: RL  ( -- )  asm-lexer:next-token 0o020 asm-helpers:(rlx) ;
: RR  ( -- )  asm-lexer:next-token 0o030 asm-helpers:(rlx) ;
: SLA ( -- )  asm-lexer:next-token 0o040 asm-helpers:(rlx) ;
: SRA ( -- )  asm-lexer:next-token 0o050 asm-helpers:(rlx) ;
: SLL ( -- )  asm-lexer:next-token 0o060 asm-helpers:(rlx) ;
: SRL ( -- )  asm-lexer:next-token 0o070 asm-helpers:(rlx) ;

prev-defs

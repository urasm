;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; and now for something completely different...
;; UrAsm Forth Engine!
;; GPLv3 ONLY
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Z80 Assembler: LD instruction
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; LD helpers
;; lexer is at the first operand
;;

also asm-resw
also-defs: asm-helpers

0 value ld-dest-reg
0 value ld-patch-addr  ;; used in IX/IY with displacements


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; LD r8,<smth>
;;

\ LD r8,r8
: (ld-r8-r8)  ( -- )
  ;; (hl),(hl) is forbidden
  ld-dest-reg kind-(hl) = tok-kind kind-(hl) = and " invalid instruction" ?error
  ld-dest-reg 3 lsh tok-kind 0x40 or or asm-emit:byte
  next-token
;

\ LD r8,<X|Y><H|L>
: (ld-r8-xylh)  ( -- )
  ;; H/L/(HL) are forbidden
  ld-dest-reg kind-h kind-(hl) bounds? " invalid instruction" ?error
  tok-kind kind-yh >= (ixy-prefix,)
  tok-kind 1 and kind-h + to tok-kind
  (ld-r8-r8)
;

\ LD A,<I|R>
: (ld-r8-ir)  ( -- )
  ld-dest-reg kind-a <> " invalid instruction" ?error
  0xed asm-emit:byte
  tok-kind kind-i = if 0o127 else 0o137 endif asm-emit:byte
  next-token
;

\ LD r8,imm8
: (ld-r8-imm8)  ( -- )
  0x06 ld-dest-reg 3 lsh or asm-emit:byte
  asm-expr:expression-byte,
;

\ LD A,(nn)
: (ld-r8-m16)  ( -- )
  ld-dest-reg kind-a <> " invalid instruction" ?error
  0o072 asm-emit:byte
  expect-`(` asm-expr:expression-addr, expect-`)`
;

\ LD r8,(i<x|y>+...)
: (ld-r8-(ixy))  ( -- )
  ;; (hl) is forbidden
  ld-dest-reg kind-(hl) = " invalid instruction" ?error
  ld-dest-reg 3 lsh 0o106 or (common-ixy) ( patch-addr ) drop
;

\ LD A,(r16)
: (ld-r8-mr16)  ( -- )
  ld-dest-reg kind-a = token-mr16-x? and " invalid instruction" ?not-error
  0o012 tok-kind 4 lsh or asm-emit:byte
  next-token
;

dispatcher: (dsp-ld-r8)
  dsp: token-r8?     (ld-r8-r8)
  dsp: token-rx8?    (ld-r8-xylh)
  dsp: token-(ixy)?  (ld-r8-(ixy))
  dsp: token-mr16?   (ld-r8-mr16)
  dsp: token-regIR?  (ld-r8-ir)
  dsp: token-`(`?    (ld-r8-m16)
  dsp: true          (ld-r8-imm8)
end-dispatcher

\ B,C,D,E,H,L,(HL),A
: (ld-r8)  ( -- )
  tok-kind to ld-dest-reg
  next-token expect-`,`
  (dsp-ld-r8)
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; LD <x|y><l|h>,<smth>
;;


\ LD xn,r8
: (ld-xylh-r8)  ( -- )
  tok-kind kind-(hl) = " invalid instruction" ?error
  ld-dest-reg 0o100 or tok-kind or asm-emit:byte
  next-token
;

\ LD xn,xn
: (ld-xylh-xylh)  ( -- )
  \ tok-kind 1 and kind-h + 3 lsh ld-dest-reg <> " invalid instruction" ?error
  ld-dest-reg 0o100 or tok-kind 1 and kind-h + or asm-emit:byte
  next-token
;

\ LD xn,imm8
: (ld-xylh-imm8)  ( -- )
  ld-dest-reg 0o006 or asm-emit:byte
  asm-expr:expression-byte,
;

dispatcher: (dsp-ld-xylh)
  dsp: token-r8?     (ld-xylh-r8)
  dsp: token-rx8?    (ld-xylh-xylh)
  dsp: true          (ld-xylh-imm8)
end-dispatcher

\ XH,XL,YH,YL
: (ld-xylh)  ( -- )
  tok-kind kind-yh >= (ixy-prefix,)
  tok-kind 1 and kind-h + 3 lsh to ld-dest-reg
  next-token expect-`,`
  (dsp-ld-xylh)
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; LD <i|r>,<smth>
;;

\ LD <I|R>,A
: (ld-ir)  ( -- )
  tok-kind >r  ;; save register
  next-token expect-`,` expect-regA
  0xed asm-emit:byte
  r> kind-i = if 0o107 else 0o117 endif asm-emit:byte
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; LD r16,<smth>
;;

\ LD rr,(nn)
: (ld-r16-mem)  ( -- )
  ld-dest-reg asm-resw:kind-hl = if
    0o052 asm-emit:byte
  else ld-dest-reg asm-resw:kind-sp > " invalid instruction" ?error
    0xed asm-emit:byte
    0o113 ld-dest-reg 4 lsh or asm-emit:byte
  endif
  next-token asm-expr:expression-addr, expect-`)`
;

\ LD HL,SP
: (ld-hl-sp)  ( -- )  " invalid instruction" error ;

: (ld-r16-ixy)  ( -- )
  ld-dest-reg asm-resw:kind-sp <> " invalid instruction" ?error
  tok-kind 1 and (ixy-prefix,)
  0o371 asm-emit:byte
  next-token
;

\ LD r16,r16
\ synthethic 16-bit move
: (ld-r16-r16)  ( -- )
  ld-dest-reg asm-resw:kind-sp = if
    token-hl? " invalid instruction" ?not-error
    0o371 asm-emit:byte
  else
    ld-dest-reg kind-sp >= " invalid instruction" ?error
    ld-dest-reg 4 lsh tok-kind 2* or 0o100 or  ( opcode+dest+src )
    dup asm-emit:byte     ;; high parts
    0o11 + asm-emit:byte  ;; low parts
  endif
  next-token
;

\ LD r16,imm16
: (ld-r16-imm16) ( -- )
  ld-dest-reg kind-af = " LD AF,imm is not a valid instruction" ?error
  ld-dest-reg 4 lsh 0o001 or asm-emit:byte
  asm-expr:expression-word,
;

dispatcher: (dsp-ld-r16)
  dsp: token-`(`?    (ld-r16-mem)
  dsp: token-sp?     (ld-hl-sp)
  dsp: token-r16-x?  (ld-r16-r16)
  dsp: token-ixy?    (ld-r16-ixy)
  dsp: true          (ld-r16-imm16)
end-dispatcher

\ BC,DE,HL,SP,AF
: (ld-r16)  ( -- )
  tok-kind to ld-dest-reg
  next-token expect-`,`
  (dsp-ld-r16)
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; LD (r16),<smth>
;;

\ LD (r16),r8
\ (HL) never comes here
: (ld-mr16-r8) ( -- )
  expect-regA ld-dest-reg dup asm-resw:kind-hl > " invalid instruction" ?error
  4 lsh 0o002 or asm-emit:byte
  next-token
;

dispatcher: (dsp-ld-mr16)
  dsp: token-r8?  (ld-mr16-r8)
end-dispatcher

\ (BC),(DE),(HL),(SP)
: (ld-mr16)  ( -- )
  tok-kind to ld-dest-reg
  next-token expect-`,`
  (dsp-ld-mr16)
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; LD (i<x|y)+...),<smth>
;;

\ LD (i<x|y)+...),r8
: (ld-mrx16-r8)  ( -- )
  ;; (HL) is forbidden
  token-(hl)? " invalid instruction" ?error
  0o100 tok-kind or kind-(hl) 3 lsh or
  ld-patch-addr asm-emit:or-c!
  next-token
;

\ LD (i<x|y)+...),imm8
: (ld-mrx16-imm8)  ( -- )
  token-(hl)? " invalid instruction" ?error
  0o066 ld-patch-addr asm-emit:or-c!
  asm-expr:expression-byte,
;

dispatcher: (dsp-ld-mrx16)
  dsp: token-r8? (ld-mrx16-r8)
  dsp: true      (ld-mrx16-imm8)
end-dispatcher

\ (IX+n), (IY+n)
: (ld-mrx16)  ( -- )
  0o000 (common-ixy) to ld-patch-addr
  expect-`,`
  (dsp-ld-mrx16)
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; LD (nn),<smth>
;;

\ LD (nn),r8
: (ld-mem-r8)  ( -- )
  expect-regA
  0o062 asm-emit:byte
  asm-expr:expression-addr-postponed,
  next-token
;

\ LD (nn),HL
: (ld-mem-hl)  ( -- )
  0o042 asm-emit:byte
  asm-expr:expression-addr-postponed,
  next-token
;

\ LD (nn),I<X|Y>
: (ld-mem-ixy)  ( -- )
  tok-kind 1 and (ixy-prefix,) (ld-mem-hl)
;

\ LD (nn),<BC|DE|SP>
: (ld-mem-r16)  ( -- )
  0xed asm-emit:byte
  tok-kind 4 lsh 0o103 or asm-emit:byte
  asm-expr:expression-addr-postponed,
  next-token
;

dispatcher: (dsp-ld-mem)
  dsp: token-r8?    (ld-mem-r8)
  dsp: token-hl?    (ld-mem-hl)
  dsp: token-ixy?   (ld-mem-ixy)
  dsp: token-r16-n? (ld-mem-r16)
end-dispatcher

\ (... -- memory reference
: (ld-mem)  ( -- )
  next-token
  asm-expr:expression  ;; evaluation will be done later
  expect-`)` expect-`,`
  (dsp-ld-mem)
;


\ LD i<x|y>,(nn)
: (ld-ixy-mem)  ( -- )
  0o052 asm-emit:byte
  next-token
  asm-expr:expression-addr,
  expect-`)`
;

\ LD i<x|y>,nn
: (ld-ixy-imm16)  ( -- )
  0o041 asm-emit:byte
  asm-expr:expression-word,
;


dispatcher: (dsp-ld-ixy)
  dsp: token-`(`? (ld-ixy-mem)
  dsp: true       (ld-ixy-imm16)
end-dispatcher

\ LD i<x|y>,smth
: (ld-ixy)  ( -- )
  tok-kind 1 and (ixy-prefix,)
  next-token expect-`,`
  (dsp-ld-ixy)
;


dispatcher: (dsp-ld-main)
  dsp: token-`(`?     (ld-mem)
  dsp: token-r8?      (ld-r8)
  dsp: token-r16?     (ld-r16)
  dsp: token-mr16?    (ld-mr16)
  dsp: token-rx8?     (ld-xylh)
  dsp: token-regIR?   (ld-ir)
  dsp: token-(ixy)?   (ld-mrx16)
  dsp: token-ixy?     (ld-ixy)
end-dispatcher

previous prev-defs


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; LD main
;;

also-defs: asm-instr

: LD  ( -- ) next-token asm-helpers:(dsp-ld-main) ;

prev-defs

;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; and now for something completely different...
;; UrAsm Forth Engine!
;; GPLv3 ONLY
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Z80 Assembler: JR and DJNZ instructions
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


also-defs: asm-helpers

\ JR addr
: (jr/djnz-addr)  ( opc -- )
  asm-emit:byte
  asm-expr:expression-addr-rel8,
;

\ DJNZ addr
: (djnz-addr)  ( -- ) 0o020 (jr/djnz-addr) ;

\ JR addr
: (jr-addr)  ( -- ) 0o030 (jr/djnz-addr) ;

\ JR cc,addr
: (jr-cond)  ( -- )
  expect-cond dup asm-resw:kind-cc > " invalid JR condition" ?error
  expect-`,`
  3 lsh 0o040 or (jr/djnz-addr)
;

dispatcher: (dsp-jr)
  dsp: token-cond?   (jr-cond)
  dsp: true          (jr-addr)
end-dispatcher

prev-defs


also-defs: asm-instr

: JR   ( -- )  asm-lexer:next-token asm-helpers:(dsp-jr) ;
: DJNZ ( -- )  asm-lexer:next-token asm-helpers:(djnz-addr) ;

prev-defs

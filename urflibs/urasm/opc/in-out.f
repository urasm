;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; and now for something completely different...
;; UrAsm Forth Engine!
;; GPLv3 ONLY
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Z80 Assembler: PUSH, POP, INC, DEC
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


also-defs: asm-helpers

\ OUT (C),r8
: (out-(c))  ( -- )
  next-token expect-`,` expect-r8
  dup asm-resw:kind-(hl) = " invalid instruction" ?error
  0xed asm-emit:byte
  3 lsh 0o101 or asm-emit:byte
;

\ OUT (n),A
: (out-(n))  ( -- )
  0o323 asm-emit:byte
  expect-`(` asm-expr:expression-ubyte, expect-`)`
  expect-`,` expect-regA
;

dispatcher: (dsp-out)
  dsp: token-(c)/(bc)? (out-(c))
  dsp: token-`(`?      (out-(n))
end-dispatcher

: (out)  ( -- )  next-token (dsp-out) ;


: (in-r8-(c)-gen)  ( r8 -- )
  dup asm-resw:kind-(hl) = " invalid instruction" ?error
  0xed asm-emit:byte
  3 lsh 0o100 or asm-emit:byte
;

\ IN r8,(C)
: (in-r8)  ( -- )
  tok-kind dup asm-resw:kind-(hl) = " invalid instruction" ?error
  (in-r8-(c)-gen)
  expect-r8 drop expect-`,` expect-(c)/(bc)
;

\ IN A,(n)
: (in-a)  ( -- )
  expect-regA expect-`,`
  token-(c)/(bc)? if asm-resw:kind-a (in-r8-(c)-gen) next-token
  else
    0o333 asm-emit:byte
    expect-`(` asm-expr:expression-ubyte, expect-`)`
  endif
;

dispatcher: (dsp-in)
  dsp: token-regA?   (in-a)
  dsp: token-r8?     (in-r8)
end-dispatcher

: (in)  ( r8 -- )  next-token (dsp-in) ;

prev-defs


also-defs: asm-instr

: OUT  ( -- )  asm-helpers:(out) ;
: IN   ( -- )  asm-helpers:(in) ;

prev-defs

;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; and now for something completely different...
;; UrAsm Forth Engine!
;; GPLv3 ONLY
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Z80 Assembler: instructions without operands
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


also-defs: asm-helpers

;; 1 or 2 bytes
: simple-opc ( n -- )
  create ,
 does>  ( pfa )
  @ dup 256 < if asm-emit:byte else asm-emit:word endif
  asm-lexer:next-token
;

prev-defs


also asm-helpers
also-defs: asm-instr

\ simple opcodes
0x00 simple-opc NOP
0x07 simple-opc RLCA
0x0F simple-opc RRCA
0x17 simple-opc RLA
0x1F simple-opc RRA
0x27 simple-opc DAA
0x2F simple-opc CPL
0x37 simple-opc SCF
0x3F simple-opc CCF
0x76 simple-opc HALT
\ 0xC9 simple-opc RET
0xD9 simple-opc EXX
0xF3 simple-opc DI
0xFB simple-opc EI

\ string opcodes
0xA0ED simple-opc LDI
0xB0ED simple-opc LDIR
0xA1ED simple-opc CPI
0xB1ED simple-opc CPIR
0xA2ED simple-opc INI
0xB2ED simple-opc INIR
0xA3ED simple-opc OUTI
0xB3ED simple-opc OTIR
0xA8ED simple-opc LDD
0xB8ED simple-opc LDDR
0xA9ED simple-opc CPD
0xB9ED simple-opc CPDR
0xAAED simple-opc IND
0xBAED simple-opc INDR
0xABED simple-opc OUTD
0xBBED simple-opc OTDR

0x67ED simple-opc RRD
0x6FED simple-opc RLD

0x45ED simple-opc RETN
0x4DED simple-opc RETI

0x44ED simple-opc NEG

previous prev-defs

;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; and now for something completely different...
;; UrForth/C Forth Engine!
;; Copyright (C) 2023 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

vocab-if-none asm-instr
vocab-if-none asm-helpers

also asm-lexer

$include-once <simple.f>
$include-once <ret.f>
$include-once <rst.f>
$include-once <ld.f>
$include-once <ex.f>
$include-once <alu.f>
$include-once <in-out.f>
$include-once <push-pop.f>
$include-once <inc-dec.f>
$include-once <jp.f>
$include-once <call.f>
$include-once <jr.f>
$include-once <im.f>
$include-once <bit.f>

prev-defs

;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; and now for something completely different...
;; UrAsm Forth Engine!
;; GPLv3 ONLY
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Z80 Assembler: PUSH, POP, INC, DEC
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


also-defs: asm-helpers


: (inc/dec-r8)  ( opc-r8 opc-r16 -- )  drop
  expect-r8 3 lsh or asm-emit:byte
;

: (inc/dec-xylh)  ( opc-r8 opc-r16 -- )  drop
  tok-kind 2 and (ixy-prefix,)
  tok-kind 1 and asm-resw:kind-h + 3 lsh or asm-emit:byte
  next-token
;

: (inc/dec-r16)  ( opc-r8 opc-r16 -- )  nip
  expect-r16 4 lsh or asm-emit:byte
;

: (inc/dec-ixy)  ( opc-r8 opc-r16 -- )  nip
  tok-kind 1 and (ixy-prefix,)
  asm-resw:kind-hl 4 lsh or asm-emit:byte
  next-token
;

: (inc/dec-(ixy))  ( opc-r8 opc-r16 -- )  drop
  asm-resw:kind-(hl) 3 lsh or (common-ixy) ( patch-addr ) drop
;

dispatcher: (dsp-inc/dec)  ( opc-r8 opc-r16 )
  dsp: token-r8?     (inc/dec-r8)
  dsp: token-r16-n?  (inc/dec-r16)
  dsp: token-ixy?    (inc/dec-ixy)
  dsp: token-(ixy)?  (inc/dec-(ixy))
  dsp: token-rx8?    (inc/dec-xylh)
end-dispatcher

prev-defs


also-defs: asm-instr

: INC  ( -- ) next-token 0o04 0o03 asm-helpers:(dsp-inc/dec) ;
: DEC  ( -- ) next-token 0o05 0o13 asm-helpers:(dsp-inc/dec) ;

prev-defs

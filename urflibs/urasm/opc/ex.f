;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; and now for something completely different...
;; UrAsm Forth Engine!
;; GPLv3 ONLY
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Z80 Assembler: EX
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


also-defs: asm-helpers

\ EX AF,AFx
: (ex-af)  ( -- )
  next-token expect-`,`
  token-af? token-afx? or " invalid instruction" ?not-error
  0o010 asm-emit:byte
  next-token
;

\ EX HL,<DE|(SP)>
: (ex-hl)  ( -- )
  next-token expect-`,`
  token-de? if 0o353
  else token-(sp)? " invalid instruction" ?not-error 0o343
  endif
  asm-emit:byte
  next-token
;

\ EX DE,HL
: (ex-de)  ( -- )
  next-token expect-`,`
  token-hl? " invalid instruction" ?not-error
  0o353 asm-emit:byte
  next-token
;

\ EX (SP),HL
: (ex-(sp))  ( -- )
  next-token expect-`,`
  token-ixy? if tok-kind 1 and (ixy-prefix,)
  else token-hl? " invalid instruction" ?not-error
  endif
  0o343 asm-emit:byte
  next-token
;

dispatcher: (dsp-ex)
  dsp: token-af?    (ex-af)
  dsp: token-afx?   (ex-af)
  dsp: token-hl?    (ex-hl)
  dsp: token-de?    (ex-de)
  dsp: token-(sp)?  (ex-(sp))
end-dispatcher

prev-defs


also-defs: asm-instr

: EX  ( -- )  next-token asm-helpers:(dsp-ex) ;

prev-defs

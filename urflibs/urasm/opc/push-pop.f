;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; and now for something completely different...
;; UrAsm Forth Engine!
;; GPLv3 ONLY
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Z80 Assembler: PUSH, POP
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


also-defs: asm-helpers

: (push/pop-r16)  ( opcode-base -- )
  tok-kind dup asm-resw:kind-sp = " invalid instruction" ?error
  dup asm-resw:kind-af = if 1- endif
  4 lsh or asm-emit:byte
  next-token
;

: (push/pop-ixy)  ( opcode-base -- )
  tok-kind 1 and (ixy-prefix,)
  asm-resw:kind-hl 4 lsh or asm-emit:byte
  next-token
;

dispatcher: (dsp-push/pop)  ( opcode-base -- )
  dsp: token-r16?  (push/pop-r16)
  dsp: token-ixy?  (push/pop-ixy)
end-dispatcher

: (push-pop-one)  ( opcode-base -- opcode-base )
  next-token dup (dsp-push/pop)
;

: (push-pop)  ( opcode-base -- )
  (push-pop-one) begin token-`,`? while (push-pop-one) repeat drop
;

prev-defs


also-defs: asm-instr

: PUSH ( -- ) 0xc5 asm-helpers:(push-pop) ;
: POP  ( -- ) 0xc1 asm-helpers:(push-pop) ;

prev-defs

;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; and now for something completely different...
;; UrAsm Forth Engine!
;; GPLv3 ONLY
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Z80 Assembler: JP instruction
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


also-defs: asm-helpers

\ JP (HL)
: (jp-(hl))  ( -- )
  0o351 asm-emit:byte
  next-token
;

\ JP (I<X|Y>)
: (jp-(ixy))  ( -- )
  tok-kind asm-resw:kind-iy-none > " invalid instruction" ?error
  tok-kind 1 and (ixy-prefix,)
  (jp-(hl))
;

\ JP addr
: (jp-addr)  ( -- )
  0o303 asm-emit:byte
  asm-expr:expression-addr,
;

\ JP cc,addr
: (jp-cond)  ( -- )
  expect-cond
  3 lsh 0o302 or asm-emit:byte
  expect-`,` asm-expr:expression-addr,
;

dispatcher: (dsp-jp)
  dsp: token-hl?     (jp-(hl))
  dsp: token-(hl)?   (jp-(hl))
  dsp: token-(ixy)?  (jp-(ixy))
  dsp: token-cond?   (jp-cond)
  dsp: true          (jp-addr)
end-dispatcher

prev-defs


also-defs: asm-instr

: JP  ( -- )  asm-lexer:next-token asm-helpers:(dsp-jp) ;

prev-defs

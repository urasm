;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; and now for something completely different...
;; UrAsm Forth Engine!
;; GPLv3 ONLY
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Z80 Assembler: RET instruction
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


also-defs: asm-helpers

\ RET cc
: (ret-cond)  ( -- )
  0o300 expect-cond 3 lsh or asm-emit:byte
;

\ RET
: (ret-simple)  ( -- )
  0o311 asm-emit:byte
;

dispatcher: (dsp-ret)
  dsp: token-cond?   (ret-cond)
  dsp: true          (ret-simple)
end-dispatcher

: (ret)  ( -- )  next-token (dsp-ret) ;

prev-defs


also-defs: asm-instr

: RET  ( -- )  asm-lexer:next-token asm-helpers:(dsp-ret) ;

prev-defs

;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; and now for something completely different...
;; UrForth/C Forth Engine!
;; Copyright (C) 2023 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; 48K/128K .SNA writer
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


$include-once <sna-data.f>


: rle-unpack-to-zx ( src-addr zx-dest-addr src-len -- )
  a>r rot >a  ;; copy src to A
  begin over 0x1_0000 u< over 2 >= and while  ( zx-dest src-len )
    2- >r  ( zx-dest | len )
    c@a +1>a c@a +1>a  ( zx-dest cnt byte | len )
    over ifnot  ( zx-dest 0 byte | len ) -- copy
      nip 1+ r> over - >r for  ( zx-dest | len )
        c@a +1>a over asm-emit:c!-if-free 1+
      endfor
    else  ( zx-dest cnt byte | len ) -- fill
      nrot for 2dup asm-emit:c!-if-free 1+ endfor nip
    endif
    r>
  repeat 2drop r>a
;


: (prepare-sna-header)  ( -- )
  ursna48 27 +  16384  ursna48-size 27 -  rle-unpack-to-zx
  ;; patch CLEAR
  asm-emit:clr dup +0if
    base @ decimal swap <# # # # # # #> 23762 swap asm-emit:cmove-from-normal base !
  else drop endif
  ;; patch USR
  asm-emit:ent dup -if drop 0 endif
  base @ decimal swap <# # # # # # #> 23773 swap asm-emit:cmove-from-normal base !
;

: write-sna-48  ( fd -- )
  ursna48 23 + w@
  dup asm-emit:used? swap 1+ asm-emit:used? or " cannot write .SNA (stack overlap)" ?error
  (prepare-sna-header)
  >r  ( | fd )
  ;; write registers
  ursna48 27 r@ files:write " error writing .SNA file header" ?not-error
  ;; write memory
  asm-emit:zx-mem 16384 + 49152 r@ files:write " error writing .SNA file memory" ?not-error
  rdrop
;

: write-sna-128  ( fd -- )
  dup >r write-sna-48
  ;; start address
  ursna48 23 + 2 r@ files:write " error writing .SNA file 128K start address" ?not-error
  ;; flags: 0x7FFD state, "TR-DOS active" flag
  0x10 asm-emit:zx-mem c!
  asm-emit:zx-mem 1+ 0!
  asm-emit:zx-mem 2 r@ files:write " error writing .SNA file 12K flags" ?not-error
  asm-emit:zx-mem 16384 erase  ;; empty page
  8 for
    i 1 = i 4 = or ifnot
      asm-emit:zx-mem 16384 r@ files:write " error writing .SNA file 128K page" ?not-error
    endif
  endfor
  rdrop
;

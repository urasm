;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; and now for something completely different...
;; UrForth/C Forth Engine!
;; Copyright (C) 2023 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; RAW chunks writer
;; trashes PAD
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


: (write-raw-chunk)  ( staddr fh -- )
  >r asm-emit:find-used if
    dup asm-emit:find-unused " wtf?!" ?not-error  ( staddr endaddr )
    2dup swap -  ( staddr endaddr len )
    rot asm-emit:zx-mem + swap r@ files:write " error writing raw chunk" ?not-error
    drop
  endif rdrop
;


;; pass base file name with extension
: write-raw  ( addr count -- )
  string:>pad
  string:pad-remove-ext string:pad-len@ >r  ( | base-name-len )
  0 begin asm-emit:find-used while
    r@ string:pad-len!  ;; restore base name length
    base @ hex over <# # # # # [char] _ hold #> string:pad+cc base !
    " .bin" string:pad+cc
    ." OUTPUT: " string:pad-cc@ xtype cr
    string:pad-cc@ files:create ifnot
      ." cannot create file '" pad count xtype ." '" cr
      " file writing error" error
    endif
    2dup (write-raw-chunk) files:close drop
    asm-emit:find-unused
  not-until rdrop
;

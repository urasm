;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; and now for something completely different...
;; UrAsm Forth Engine!
;; GPLv3 ONLY
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Z80 Assembler: conditional compilation
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


also asm-lexer
vocabulary asm-cc
also-defs: asm-cc

0 value if-count


: expr  ( -- )
  allow-continuations >R
  true to allow-continuations
  next-token asm-expr:expression-const
  r> to allow-continuations
;

\ 0 VALUE ($SKIP-FROM-LINE)

: skip-conds  ( toelse -- )
  \ ." SKIP: LINE=" 0 (include-file-line) .
  \ ." FILE: " 0 (include-file-name) xtype cr
  0 (include-file-line) >r
  0 >r  ( toelse | stline level )
  begin
    refill-nocross ifnot
      rdrop
      ." unfinished conditional started at: "
      0 (include-file-name) xtype ." :" r> 0.r cr
      " unexpected end of file" error
    endif
    next-token
    \ token xtype cr
    tok-type tk-str = if 0 to tok-len endif
    $IF 0
      token 1 > swap c@ [char] $ = and if
        +1-to tok-addr -1-to tok-len
      endif
    $ENDIF
    token ifnot drop continue endif
    c@ [char] $ <> if continue endif
    false  ( toelse done? | stline level )
    token " $IF" string:=ci if drop r> 1+ >r false endif
    token " $ENDIF" string:=ci if drop
      ;; in nested ifs, look only for $ENDIF
      r@ if r> 1- >r false
      else true endif ;; it doesn't matter which part we're skipping, it ends here anyway
    endif
    token " $ELSE" string:=ci if drop
      ;; if we're skipping "true" part, go on
      dup if
        +1-to if-count
        r@ 0=  \ FALSE
      else
        ;; we're skipping "false" part, there should be no else
        r@ " unexpected $ELSE" ?not-error
        false
      endif
    endif
    ;; only for level 0
    r@ ifnot
      token " $ELSIF" string:=ci if drop
        ;; if we're skipping "true" part, go on
        dup if
          ;; process the conditional
          expr expect-eol
          ;; either resume normal execution, or keep searching for $ELSE
          if +1-to if-count true
          else false endif
        else
          ;; we're skipping "false" part, there should be no else
          " unexpected $ELSIF" error
        endif
      endif
    endif
  until
  drop  ;; drop `toelse`
  r> 0<> " oops?" ?error rdrop
  \ REFILL " unexpected end of file" ?NOT-ERROR
  parse-skip-line next-token
;

: process-cond  ( -- )
  \ ." $IF-000: depth=" depth .
  expr expect-eol \ ." $IF: " dup . cr
  \ ." $IF-001: depth=" depth .
  if +1-to if-count
  else true skip-conds endif
;


previous prev-defs


also-defs: asm-instr

: $IF  asm-lexer:expect-first-token
  asm-cc:process-cond
;

: $ELSE  asm-lexer:expect-first-token
  asm-cc:if-count " unexpected $ELSE" ?not-error
  asm-lexer:next-token asm-lexer:expect-eol
  false asm-cc:skip-conds
;

: $ELSIF  asm-lexer:expect-first-token
  asm-cc:if-count " unexpected $ELSIF" ?not-error
  \ -1-to asm-cc:if-count
  false asm-cc:skip-conds
;

: $ENDIF  asm-lexer:expect-first-token
  asm-cc:if-count " unexpected $ENDIF" ?not-error
  asm-lexer:next-token asm-lexer:expect-eol
  -1-to asm-cc:if-count
;

: $ELIF  " please, use $ELSIF" error ;
: $ELSEIF  " please, use $ELSIF" error ;

prev-defs

;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; and now for something completely different...
;; UrAsm Forth Engine!
;; GPLv3 ONLY
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Z80 Assembler: reserved words
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


vocab-if-none asm-resw
vocab-if-none asm-helpers


also-defs: asm-resw

;; list of reserved word types
1 enum-from{
\ 8 bit registers (bits 0-2 of opcode)
  def: uo-r8      \ B,C,D,E,H,L,A
  def: uo-rx8     \ XH,XL,YH,YL
  def: uo-sr8     \ R,I
\ 16 bit registers (bits 4-5 of opcode)
  def: uo-r16     \ BC,DE,HL,SP,AF
  def: uo-afx     \ AFx
  def: uo-rx16    \ IX,IY
\ conditions (bits 3-5 of opcode)
  def: uo-cond    \ NZ,Z,NC,C,PO,PE,P,M
  def: uo-portc   \ (C), no value
  def: uo-mr16    \ (BC),(DE),(HL),(SP)
  def: uo-mrx16   \ (IX+n), (IY+n) -- actually, "(i[xy][+-]"
  def: uo-mem     \ (... -- memory reference
  def: uo-equ
}

;; list of kinds
\ UO-R8
enum{
  def: kind-b     \ register: B
  def: kind-c     \ register: C
  def: kind-d     \ register: D
  def: kind-e     \ register: E
  def: kind-h     \ register: H
  def: kind-l     \ register: L
  def: kind-(hl)  \ register: (HL)
  def: kind-a     \ register: A
}

\ UO-SR8
enum{
  def: kind-r     \ register: R
  def: kind-i     \ register: I
}

\ UO-RX8
enum{
  def: kind-xh    \ register: XH
  def: kind-xl    \ register: XL
  def: kind-yh    \ register: YH
  def: kind-yl    \ register: YL
}

\ UO-R16, UO-MR16
enum{
  def: kind-bc    \ register: BC
  def: kind-de    \ register: DE
  def: kind-hl    \ register: HL
  def: kind-sp    \ register: SP
  def: kind-af    \ register: AF
}

\ UO-RX16
enum{
  def: kind-ix    \ register: IX
  def: kind-iy    \ register: IY
}

\ UO-MRX16
\ WARNING! this MUST be in this order, and with this values!
\          the lexer expects it this way
\ bit 0: ix/iy
\ bit 1: set for "add", reset for "none/sub"
enum{
  def: kind-ix-none   \ (IX)
  def: kind-iy-none   \ (IY)
  def: kind-ix-add    \ (IX+
  def: kind-iy-add    \ (IY+
  def: kind-ix-sub    \ (IX-
  def: kind-iy-sub    \ (IY-
}

\ UO-COND
enum{
  def: kind-nz    \ condition: NZ
  def: kind-z     \ condition: Z
  def: kind-nc    \ condition: NC
  def: kind-cc    \ condition: C
  def: kind-po    \ condition: PO
  def: kind-pe    \ condition: PE
  def: kind-p     \ condition: P
  def: kind-m     \ condition: M
}

\ UO-AFx
enum{
  def: kind-afx
}

prev-defs


also-defs: asm-helpers

: reserved-word ( uo kind-- )
  create swap , , create;
;

prev-defs

also asm-helpers
also-defs: asm-resw

UO-R8 KIND-B     reserved-word B
UO-R8 KIND-C     reserved-word C
UO-R8 KIND-D     reserved-word D
UO-R8 KIND-E     reserved-word E
UO-R8 KIND-H     reserved-word H
UO-R8 KIND-L     reserved-word L
\ UO-R8 KIND-(HL)  reserved-word (HL)
UO-R8 KIND-A     reserved-word A

UO-SR8 KIND-R  reserved-word R
UO-SR8 KIND-I  reserved-word I

UO-RX8 KIND-XH  reserved-word XH
UO-RX8 KIND-XL  reserved-word XL
UO-RX8 KIND-YH  reserved-word YH
UO-RX8 KIND-YL  reserved-word YL

UO-RX8 KIND-XH  reserved-word HX
UO-RX8 KIND-XL  reserved-word LX
UO-RX8 KIND-YH  reserved-word HY
UO-RX8 KIND-YL  reserved-word LY

UO-RX8 KIND-XH  reserved-word IXH
UO-RX8 KIND-XL  reserved-word IXL
UO-RX8 KIND-YH  reserved-word IYH
UO-RX8 KIND-YL  reserved-word IYL

UO-R16 KIND-BC  reserved-word BC
UO-R16 KIND-DE  reserved-word DE
UO-R16 KIND-HL  reserved-word HL
UO-R16 KIND-SP  reserved-word SP
UO-R16 KIND-AF  reserved-word AF

\ UO-MR16 KIND-BC  RESERVED-WORD (BC)
\ UO-MR16 KIND-DE  RESERVED-WORD (DE)
\ UO-MR16 KIND-HL  RESERVED-WORD (HL)
\ UO-MR16 KIND-SP  RESERVED-WORD (SP)

UO-RX16 KIND-IX  reserved-word IX
UO-RX16 KIND-IY  reserved-word IY

\ UO-MRX16 KIND-IX  RESERVED-WORD (IX
\ UO-MRX16 KIND-IY  RESERVED-WORD (IY

UO-COND KIND-NZ  reserved-word NZ
UO-COND KIND-Z   reserved-word Z
UO-COND KIND-NC  reserved-word NC
\ C is processed by the lexer
UO-COND KIND-PO  reserved-word PO
UO-COND KIND-PE  reserved-word PE
UO-COND KIND-P   reserved-word P
UO-COND KIND-M   reserved-word M

UO-AFx  KIND-AFx  reserved-word AFx

UO-EQU  0  reserved-word EQU


previous prev-defs

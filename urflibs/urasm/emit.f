;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; and now for something completely different...
;; UrAsm Forth Engine!
;; GPLv3 ONLY
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Z80 Assembler: code emitter
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


vocab-if-none asm-emit
also-defs: asm-emit

bitmask-enum{
  def: zx-mflag-used
  def: zx-mflag-reserved
}


: init-memory  ( -- )
  zx-mem 65536 erase
  zx-mflags 65536 erase
;


: used?  ( addr -- bool )
  dup 0x1_0000 u< swap zx-mflags + c@ zx-mflag-used and logand
;


: find-used  ( from -- addr TRUE / FALSE )
  begin dup 0x1_0000 u< over used? not and while 1+ repeat
  dup 0x1_0000 u< if true else drop false endif
;

: find-unused  ( from -- addr TRUE / FALSE )
  begin dup 0x1_0000 u< over used? and while 1+ repeat
  dup 0x1_0000 u< if true else drop false endif
;


;; this is PC for "$"
;; main driver sets it before calling instruction words
0 value pc$
;; logical PC
0 value pc
;; physical PC
0 value emit-pc
;; "ENT" value
-1 value ent
;; "CLR" value
-1 value clr


: instruction-start  ( -- )
  pc to pc$
;


: .hex2    ( n -- )  base @ hex swap <# # # #> type base ! ;
: .hex4    ( n -- )  base @ hex swap <# # # # # #> type base ! ;
: .hex4-r  ( n -- )  dup .hex2 space -8 lsh .hex2 ;

;; used in various unpackers (RLE, for example)
;; doesn't change flags
: c!-if-free  ( value addr -- )
  dup 0x1_0000 u< if
    dup zx-mflags + c@ zx-mflag-used and ifnot zx-mem + c!
    else 2drop endif
  else 2drop endif
;

;; doesn't change flags
: cmove-from-normal  ( src zx-dest count -- )
  for
    dup 0x1_0000 u< if
      over c@ over zx-mem + c!
      1+ swap 1+ swap
    endif
  endfor 2drop
;

\ : here  ( -- addr )  emit-pc ;

: or-c!  ( value addr -- )
  dup 65535 u> " invalid zx address" ?error
  dup zx-mflags + forth:c@ zx-mflag-used <> " trying to patch undefined zx memory address" ?error
  zx-mem + forth:or-c!
;

: c!  ( value addr -- )
  dup 65535 u> " invalid zx address" ?error
  dup zx-mflags + forth:c@ zx-mflag-used <> " trying to patch undefined zx memory address" ?error
  zx-mem + forth:c!
;

: w!  ( value addr -- )  2dup c! 1+ swap hi-byte swap c! ;
: rw! ( value addr -- )  bswap16 w! ;
: dw! ( value addr -- )  2dup w! 2+ swap hi-word swap w! ;

: c@  ( addr -- byte )  lo-word zx-mem + forth:c@ ;
: w@  ( addr -- byte )  dup c@ swap 1+ c@ 8 lsh or ;

: mark-used  ( addr count -- )
  for
    dup 65535 u> " invalid zx address" ?error
    dup zx-mflags + dup forth:c@ zx-mflag-used or swap forth:c!
  1+ endfor drop
;

: c!-ok  ( value addr -- )  dup 1 mark-used c! ;
: w!-ok  ( value addr -- )  dup 2 mark-used w! ;
: dw!-ok ( value addr -- )  dup 4 mark-used dw! ;


: +>pc  ( n -- )
  dup 0< " invalid pc offset" ?error
  ?dup if
    dup +to pc
    +to emit-pc
    emit-pc 65536 u> " invalid PC offset" ?error
  endif
;

: +1>pc  ( -- )
  +1-to pc
  +1-to emit-pc
;

: .pc  ( -- ) ." #" pc .hex4 ." : " ;

: (emit-byte)  ( byte -- )
  pc 65536 u> " zx memory overflow (pc)" ?error
  emit-pc 65536 u> " zx memory overflow (emit-pc)" ?error
  emit-pc zx-mflags + forth:c@ zx-mflag-reserved and " trying to write to reserved memory" ?error
  emit-pc zx-mem + forth:c!
  emit-pc zx-mflags + dup forth:c@ zx-mflag-used or swap forth:c!
  +1>pc
;

: byte  ( byte -- )
  \ .pc dup .hex2 cr
  (emit-byte)
;

: neg-byte  ( byte -- )
  negate lo-byte
  \ .pc dup .hex2 cr
  (emit-byte)
;

: word  ( word -- )
  \ .pc dup .hex4-r cr
  dup (emit-byte) -8 lsh (emit-byte)
;

: rword  ( word -- )
  \ .pc dup .hex4-r cr
  dup -8 lsh (emit-byte) (emit-byte)
;

: addr  ( word -- )
  \ .pc dup .hex4-r cr
  word
;


: (im)  ( byte -- opcode )
  case
    0 of 0x46 endof
    1 of 0x56 endof
    2 of 0x5e endof
    otherwise " wtf?!" error
  endcase
;

: im  ( byte -- )  (im) byte ;

: rst  ( rst-addr -- )  0o307 or byte ;
: rst-c!  ( rst-addr addr -- )  swap 0o307 or swap c! ;

: bit-c!  ( bitnum addr+1 -- )  swap 3 lsh swap 1- or-c! ;


prev-defs

;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; and now for something completely different...
;; UrForth/C Forth Engine!
;; Copyright (C) 2023 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; pseudostructs
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(*
usage:
  define-accessors rec
    acc: field0
    acc: field1
  end-accessors  -- defines constant "rec-size"

now, "rec-size" is record size in bytes. each field is a cell, and
have "rec->field" (offset), "rec->field@" and "rec->field!" accessors.
*)


simple-vocabulary (struct-accs-internal)
also-defs: (struct-accs-internal)

;; create "record->name"
: (create-ofs-accessor)  ( addr count ofs-in-bytes -- ofs-in-bytes )
  dup >r
  pad @ >r nrot " ->" string:pad+cc string:pad+cc
  string:pad-cc@ (create) ,
  r> pad ! r>
  immediate
 does> ( info-addr pfa -- info-addr-with-offset )
  @  ;; offset
  ?dup if
    compiler:comp? if literal compile +
    else + endif
  endif
;

;; create "record->nameX"
: (create-accessor)  ( addr count ofs do-write? char -- )
  pad @ >r " ->" string:pad+cc >r 2swap string:pad+cc r> string:pad+char
  string:pad-cc@ (create)
  ;; pfa: ofs cfa-r/w-compile ofs cfa-r/w-exec
  if , ['] forth:(direct:+:!) , ['] forth:! ,
  else , ['] forth:(direct:+:@) , ['] forth:@ ,
  endif
  r> pad !
  immediate
 does> ( base pfa )
  compiler:comp? if
    dup @ ?dup if swap cell+ @ <compile, , compile>
    else 2 +cells @ compile, endif
  else dup @ rot + swap 2 +cells @execute-tail
  endif
;

: (create-peek-accessor)  ( addr count ofs -- )  false [char] @ (create-accessor) ;
: (create-poke-accessor)  ( addr count ofs -- )  true [char] ! (create-accessor) ;

prev-defs


simple-vocabulary (struct-accs-defs)
also-defs: (struct-accs-defs)

: def:  ( ofs -- ofs+4 )  \ name
  >r  ;; save offset
  parse-name dup " accessor name expected" ?not-error
  2dup r@ (struct-accs-internal):(create-ofs-accessor) >r
  2dup r@ (struct-accs-internal):(create-peek-accessor)
  r> (struct-accs-internal):(create-poke-accessor)
  r> cell+
;

: end-accessors  ( ofs -- )
  previous
  ;; create constant
  " -size" string:pad+cc
  string:pad-cc@ compiler:(create-header)
  compiler:(cfaidx-do-const) compiler:cfa, ,
  compiler:smudge
;

prev-defs


;; create accessors
;; trashes PAD
: define-accessors  ( -- 0 )  \ name
  parse-name string:>pad
  0 also (struct-accs-defs)
;

(*
define-accessors boo
  def: first
  def: second
end-accessors

boo-size . cr
debug:decompile boo->second!
*)

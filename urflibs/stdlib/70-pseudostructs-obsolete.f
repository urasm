;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; and now for something completely different...
;; UrForth/C Forth Engine!
;; Copyright (C) 2023 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; pseudostructs
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


simple-vocabulary (pseudostructs-internal)
also-defs: (pseudostructs-internal)

;; create "record->name"
: (create-ofs-accessor)  ( ofs-in-bytes addr count -- ofs-acc-cfa )
  (create) latest-cfa swap
  , does> ( info-addr pfa -- info-addr-with-offset ) @ +
;

;; create "class->nameX"
: (create-accessor)  ( addr count ofs-acc-cfa cfa-r/w char -- )
  >r 2swap string:>pad r> string:pad+char
  string:pad-cc@ compiler:(create-forth-header)
  swap compile, compile, compile forth:(exit) compiler:smudge
;

: (create-peek-accessor)  ( addr count ofs-acc-cfa -- )  ['] @ [char] @ (create-accessor) ;
: (create-poke-accessor)  ( addr count ofs-acc-cfa -- )  ['] ! [char] ! (create-accessor) ;

prev-defs

(*
usage:
  0
  new-accessors rec->field0
  new-accessors rec->field1
  constant rec-size

now, "rec-size" is record size in bytes. each field is a cell, and
have "rec->field" (offset), "rec->field@" and "rec->field!" accessors.

*)
;; create accessors
;; trashes PAD
: new-accessors  ( ofs -- ofs+4 )  \ name
  parse-name 2>r  ( ofs | addr count )
  dup 2r@ (pseudostructs-internal):(create-ofs-accessor)  ( ofs ofs-acc-cfa | addr count )
  dup 2r@ rot (pseudostructs-internal):(create-peek-accessor)
      2r> rot (pseudostructs-internal):(create-poke-accessor)
  cell+
;

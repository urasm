;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; and now for something completely different...
;; UrForth/C Forth Engine!
;; Copyright (C) 2023 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; enums
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(*
enum{
  def: name
  def: name
}

"n enum-from{" -- start from `n`.

"bitmask-enum{" create bitmasks. after defining a name, enum value
is shifted left by one bit.

inside enum definition:
  n set -- set new current value
  n +set -- increment current value
  n -set -- decrement current value
  n set-bit -- set current value to 1<<n
*)


simple-vocabulary (enum-internal) (hidden)
also-defs: (enum-internal)

;; etypes
0 constant (bit) (hidden)
1 constant (inc) (hidden)

: (advance)  ( etype evalue -- etype enextvalue )
  over (bit) = if ?dup if 2* else 1 endif
  else 1+
  endif
; (hidden)

: def:  ( etype evalue -- etype enextvalue )  \ name
  dup constant (advance)
;

: }  ( etype evalue -- )
  2drop previous
;

: set      ( etype evalue newvalue -- etype newvalue )  nip ;
: set-bit  ( etype evalue newbit -- etype 1<<newbit )  nip 1 swap lsh ;
: -set     ( etype evalue delta -- etype evalue-delta ) - ;
: +set     ( etype evalue delta -- etype evalue+delta ) + ;

prev-defs

: enum{  ( -- etype enextvalue )  forth:(enum-internal):(inc) 0 also (enum-internal) ;
: enum-from{  ( start-value -- etype enextvalue )  forth:(enum-internal):(inc) swap also (enum-internal) ;
: bitmask-enum{  ( -- etype enextvalue )  forth:(enum-internal):(bit) 1 also (enum-internal) ;

;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; and now for something completely different...
;; UrForth/C Forth Engine!
;; Copyright (C) 2023 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; working with double (64-bit) numbers
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


;; convert single number do double number
: s>d  ( n -- nl nh )  dup 0< if -1 else 0 endif ;
: u>d  ( n -- nl nh )  0 ;
: d>s  ( lo hi -- lo )  drop ;

: dnegate  ( lo hi -- lo hi )  bitnot swap bitnot swap 1 0 d+ ;

: d<>  ( lo0 hi0 lo1 hi1 -- bool )  d= not ;
: d>=  ( lo0 hi0 lo1 hi1 -- bool )  d< not ;
: d>   ( lo0 hi0 lo1 hi1 -- bool )  d<= not ;

: du>=  ( lo0 hi0 lo1 hi1 -- bool )  du< not ;
: du>   ( lo0 hi0 lo1 hi1 -- bool )  du<= not ;

: d0<  ( lo hi -- bool )  nip 0< ;
: d0>  ( lo hi -- bool )  nip 0> ;
: d0=  ( lo hi -- bool )  or 0= ;
: d0<>  ( lo hi -- bool )  or 0<> ;

: d2*  ( lo hi -- lo hi )  2dup d+ ;
: d2/  ( lo hi -- lo hi )  dup >r 2/  swap 2u/ r> 1 and if 0x8000_0000 or endif swap ;

: dabs ( lo hi -- lo hi )  dup 0< if dnegate endif ;

: dmax  ( d1 d2 -- max[d1,d2] )  2over 2over d< if 2swap endif 2drop ;
: dmin  ( d1 d2 -- min[d1,d2] )  2over 2over d> if 2swap endif 2drop ;
: 2rot  ( x1 x2 x3 x4 x5 x6 -- x3 x4 x5 x6 x1 x2 )  2>r 2swap 2r> 2swap ;
: 2nrot ( x1 x2 x3 x4 x5 x6 -- x5 x6 x1 x2 x3 x4 )  2swap 2>r 2swap 2r> ;

: m+  ( d1|ud1 n -- d2|ud2 )  s>d d+ ;

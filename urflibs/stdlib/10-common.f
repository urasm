;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; and now for something completely different...
;; UrForth/C Forth Engine!
;; Copyright (C) 2023 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


: uservar  ( val -- )  \ name
  (user-var-used) @
  dup cell+  (user-var-addr) (user-var-size) + u>= " too many user vars" ?error
  dup rot swap!
  dup cell+ (user-var-used) !
  create , does> @
;

;; moved to C kernel
(*
: 2>r  ( lo hi -- | lo hi )  r> rot >r swap >r >r ;
: 2r>  ( | lo hi -- lo hi )  r> r> r> swap rot >r ;
: 2r@  ( | lo hi -- lo hi | lo hi )  2 rpick 1 rpick ;
: 2rdrop  r> rdrop rdrop >r ;


;; store byte via A, advance A by 1
: c!+1>a ( byte -- )  c!a +1>a ;
;; store cell via A, advance A by 1
: !+4>a  ( value -- )  !a +4>a ;

: c@+1>a ( byte -- )  c@a +1>a ;
: @+4>a  ( value -- ) @a +4>a ;
*)


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; handle utils
;;

also-defs: handle

: new-alloc  ( size typeid -- stx )
  handle:new swap over handle:size!
;

: new-init  ( addr size typeid -- stx )
  over 0< " invalid handle size" ?error
  handle:new  ( addr size stx )
  2dup handle:size!
  dup >r swap cmove r>
;

: new-strz  ( addr size typeid -- stx )
  over 0< " invalid handle size" ?error
  handle:new  ( addr size stx )
  2dup 1+ handle:size!  ;; this also fills handle data with zeroes
  dup >r swap cmove r>
;

prev-defs


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; some standard words
;;

;; this is The Word that should be used for vocabulary searches
;; this does namespace resolution
;; if "a:b" is not a known word, try to search "b" in dictionary "a"
;; things like "a:b:c" are allowed too
;; returns `1` if cfa is immediate, or `-1` if it is a normal word
: wfind  ( addr count -- cfa -1 // cfa 1 // false )
  find-word if dup compiler:immediate-word? if 1 else -1 endif
  else false endif
;


;; +- ( n1 n2 -- n3 )
;; Apply the sign of n2 to n1, which is left as n3.
: +-
  swap 0< if abs negate else abs endif
;

;; moved to C kernel
(*
;; -1, 0 or 1
: memcmp  ( addr1 addr2 size -- n )
  a>r  >r 0 nrot r>  ( 0 a1 a2 sz | rega )
  for  ( 0 a1 a2 | rega )
    over >a c@a over >a c@a -
    ?dup if  ( 0 a1 a2 sgn )  >r drop r> nrot break  endif
    1+ swap 1+ swap
  endfor 2drop
  r>a  sign?
;

;; -1, 0 or 1
: memcmp-ci  ( addr1 addr2 size -- n )
  a>r  >r 0 nrot r>  ( 0 a1 a2 sz | rega )
  for  ( 0 a1 a2 | rega )
    over >a c@a string:char-upper over >a c@a string:char-upper -
    ?dup if  ( 0 a1 a2 sgn )  >r drop r> nrot break  endif
    1+ swap 1+ swap
  endfor 2drop
  r>a  sign?
;
*)

: ucmp  ( a b -- -1|0|1 )
  2dup u< if 2drop -1 else u> if 1 else 0 endif endif
;

: compare  ( c-addr1 u1 c-addr2 u2 -- n )
  rot 2dup 2>r umin memcmp ?dup ifnot 2r> swap ucmp else 2rdrop endif
;

: compare-ci  ( c-addr1 u1 c-addr2 u2 -- n )
  rot 2dup 2>r umin memcmp-ci ?dup ifnot 2r> swap ucmp else 2rdrop endif
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; wordlist utilities
;;

: (.-vocid-name)  ( vocid -- )
  compiler:(vocofs-header) + @
  ?dup if id-count xtype else ." <unnamed>" endif
; (hidden)

: show-wordlists  ( -- )
  ." === WORDLISTS ===\n"
  (vsp@) for
    2 spaces i 0 .r ." : " i (vsp-at@) (.-vocid-name) cr
  endfor
  ." CONTEXT: " context @ (.-vocid-name) cr
  ." CURRENT: " current @ (.-vocid-name) cr
;


80 value words-width

$IF HAS-WORD("LOCALS:")
: vocid-words ( vocid -- )
  locals: iter cols
  voc-latest
  to :iter  0 to :cols
  begin :iter while
    :iter lfa->nfa @ compiler:(wflag-smudge) compiler:(wflag-hidden) or  and
    ifnot
      :iter lfa->nfa id-count  ( addr count )
      :cols over + 1+ dup to :cols  ( addr count cols )
      words-width > if dup 1+ to :cols cr endif space xtype
    endif
    :iter @ to :iter
  repeat cr
;
$ELSE
: vocid-words ( vocid -- )
  voc-latest >r 0  ( cols | vocid )
  begin r@ while
    r@ lfa->nfa @ compiler:(wflag-smudge) compiler:(wflag-hidden) or  and
    ifnot
      r@ lfa->nfa id-count    ( cols addr count )
      rot over + 1+           ( addr count newcols )
      dup words-width > if drop dup 1+ cr endif
      nrot space xtype
    endif
    r> @ >r  ;; move to the previous LFA
  repeat rdrop drop endcr
;
$ENDIF

: WORDS ( -- )  CONTEXT @ VOCID-WORDS ;


: 2@  ( n -- lo hi )  dup @ swap 4+ @ swap ;
: 2!  ( lo hi n -- )  2dup ! nip 4+ ! ;

;; moved to C kernel
\ : @execute  ( addr )  @ execute-tail ;
\ : @execute-tail  ( addr )  rdrop @ execute-tail ;
\ : (self@)  ( -- self-value )  (self) @ ;
\ : (self!)  ( self-value -- )  (self) ! ;

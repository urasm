;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; and now for something completely different...
;; UrForth/C Forth Engine!
;; Copyright (C) 2023 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; additional string operations
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


also-defs: string

;; moved to C kernel
(*
: char-upper  ( ch -- ch )
  lo-byte dup [char] a [char] z bounds? if 32 - endif
;

: upper  ( addr count -- )
  for dup c@ [char] a [char] z bounds? if dup c@ 32 - over c! endif 1+ endfor drop
;
*)

: -trailing  ( addr count -- addr count )
  a>r over >a
  begin
    dup 0>
  while  ( addr count )
    dup 1- c@a+ bl <= if 1- false else true endif
  until
  r>a
;

;; adjust the character string at c-addr1 by n characters.
;; the resulting character string, specified by c-addr2 u2,
;; begins at c-addr1 plus n characters and is u1 minus n characters long.
;; doesn't check length, allows negative n.
: /string  ( c-addr1 count n -- c-addr2 count )
  dup >r - swap r> + swap
;

;; checks length, doesn't strip anything from an empty string
: /char  ( c-addr1 u1 -- c-addr+1 u1-1 )
  1- dup -if drop 0 else swap 1+ swap endif
;

;; checks length, doesn't strip anything from an empty string
: /2chars  ( c-addr1 u1 -- c-addr+2 u1-2 )  /char /char ;

;; moved to C kernel
(*
: (char-digit)  ( ch -- digit true // false )
  dup case
    [char] 0 [char] 9 bounds-of [char] 0 - true endof
    [char] A [char] Z bounds-of [char] A - 10 + true endof
    [char] a [char] z bounds-of [char] a - 10 + true endof
    otherwise 2drop false
  endcase
;

: digit  ( char base -- digit TRUE / FALSE )
  swap (char-digit) if  ( base digit )
    over 1 36 bounds? if  ( base digit )
      dup rot u< if true else drop false endif
    else ( base digit ) 2drop false
    endif
  else  ( base )  drop false
  endif
;

: digit?  ( ch base -- flag )  digit dup if nip endif ;
*)

prev-defs


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; additional string operations
;;

also-defs: string

: cc-cat-char  ( cc-str char -- )
  >r dup count + r> swap c! 1+!
;

: cc-cat  ( cc-str addr count -- )
  dup 0> if
    rot dup >r  ( addr count cc-str | cc-str )
    over >r     ( addr count cc-str | cc-str count )
    count + swap move  ( | cc-str count )
    r> r> +!
  else 2drop
  endif
;

: pad+char  ( ch -- )  pad swap cc-cat-char ;
: pad+cc  ( addr count -- )  pad nrot cc-cat ;

;; copy string to pad as cell-counted string
: >pad  ( addr count -- )
  dup pad !
  pad 4+ swap move
;

: pad-cc@  ( -- addr count )  pad count ;
: pad-len@ ( -- count )  pad @ ;
: pad-len! ( count -- )  pad ! ;

: pad-char@  ( idx -- ch )  pad 4+ + c@ ;

: path-delimiter?  ( ch -- )
  $IF $SHITDOZE
    dup [char] / forth:= over [char] \ forth:= or swap [char] : forth:= or
  $ELSE
    [char] / forth:=
  $ENDIF
;

;; leaves only path (or empty string)
;; leaves final path delimiter
;; UNTESTED!
: pad-remove-name  ( -- )
  pad-len@ 1+
  begin 1- dup +while
    dup 1- pad-char@ path-delimiter?
  until 0 max pad-len!
;

: pad-remove-ext  ( -- )
  pad-len@
  begin 1- dup +while
    dup pad-char@
    dup [char] . forth:=
    if swap pad-len! true
    else path-delimiter? endif
  until drop
;

prev-defs

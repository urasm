;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; and now for something completely different...
;; UrForth/C Forth Engine!
;; Copyright (C) 2023 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; simple superinstruction optimiser
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(*
this is very simple peephole optimiser. it is called by "INTERPRET" after
compiling a literal or a word. also, compiler words which create branches
take care of resetting the optimiser, to avoid optimising things like
"7 begin and" to "(lit-and)" (to avoid breaking branches).

compiler words to mark/resolve branches are aware of the optimiser. you
should use those words in your code instead of direct poking.

we are using the ring buffer to record recent commands, because it is cheap.
reseting the buffer is cheap too: just write "0" to the current ring buffer
entry. it works because all optimisers check buffer validity first.
*)


vocabulary (sinopt-peephole) (hidden)
also-defs: (sinopt-peephole)


\ $DEFINE DEBUG-SINOPT-DUMP-PUSHES

\ $DEFINE DEBUG-SINOPT

$DEFINE DEBUG-SINOPT-SIMPLE-BITWISE
$DEFINE DEBUG-SINOPT-SIMPLE-SWAP-INC-SWAP
$DEFINE DEBUG-SINOPT-SIMPLE-INC-DEC
$DEFINE DEBUG-SINOPT-SIMPLE-CELLS
$DEFINE DEBUG-SINOPT-SIMPLE-CELLS-INC-DEC
$DEFINE DEBUG-SINOPT-SIMPLE-DIRECT-POKES
$DEFINE DEBUG-SINOPT-SIMPLE-DIRECT-POKES-INCS
$DEFINE DEBUG-SINOPT-SIMPLE-DIRECT-POKE-INCS
$DEFINE DEBUG-SINOPT-SIMPLE-SHIFTS
$DEFINE DEBUG-SINOPT-SIMPLE-MULS
$DEFINE DEBUG-SINOPT-SIMPLE-DIVS
$DEFINE DEBUG-SINOPT-VARS
$DEFINE DEBUG-SINOPT-STACK-OPS


false value enabled?


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; utils
;;

;; last compiled word address
: prev-cw   ( -- addr )  postpone here postpone 8- ; immediate
: prev-lit-cw   ( -- addr )  postpone here postpone 12 postpone - ; immediate
\ debug:decompile prev-lit-cw

: last-cw   ( -- addr )  postpone here postpone cell- ; immediate

: 2cells  ( n -- n-8 )  compiler:?comp [ 2 cells ] imm-literal literal ; immediate
: 3cells  ( n -- n-8 )  compiler:?comp [ 3 cells ] imm-literal literal ; immediate
: 2:-cells  ( n -- n-8 )  postpone 8- ; immediate
: 3:-cells  ( n -- n-8 )  compiler:?comp [ 3 cells ] imm-literal literal postpone - ; immediate

;; for clarity
: compile!  ( val addr -- )  compiler:?comp compile ! ; immediate


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ring buffer with instruction addresses
;;

;; ring buffer for 8 instructions.
;; WARNING! should be power of 2!
8 constant buffer-size
buffer-size cells 1- constant buffer-pos-mask

create buffer buffer-size cells allot create;
buffer buffer-size cells erase

;; positions are byte offsets!
0 value buffer-lit-pos  ;; position of the last compiled (LIT); -1 means "not there"
0 value buffer-tail     ;; last used buffer entry

: buffer-reset  ( -- )
  -1 to buffer-lit-pos
  ;; put zeroes to the last ring buffer element.
  ;; this way any check will definitely fail.
  buffer-tail buffer + 0!
;

;; rollback one instruction
: buffer-rollback-one  ( -- )  buffer-tail cell- buffer-pos-mask and to buffer-tail ;

: buffer+@  ( ofs -- )  buffer + @ ;

: buffer-tail@  ( -- )  buffer-tail buffer+@ ;
: buffer-prev@  ( -- )  buffer-tail cell- buffer-pos-mask and buffer+@ ;
: buffer-third@ ( -- )  buffer-tail 8- buffer-pos-mask and buffer+@ ;

: (put-addr)  ( addr newofs -- )  buffer + ! ;
: (advance-buf)  ( -- newofs ) buffer-tail cell+ buffer-pos-mask and dup to buffer-tail ;
: lit-reset  ( -- )  -1 to buffer-lit-pos ;

;; called when new literal compiled
: push-literal  ( addr -- )
  (advance-buf) dup to buffer-lit-pos (put-addr)
  $IF $DEBUG-SINOPT-DUMP-PUSHES
    ." 3LIT(" buffer-tail 0 u.r ." ): " buffer-tail@ 0 u.r space buffer-prev@ 0 u.r space buffer-third@ 0 u.r cr
  $ENDIF
;

;; push last compiled word address to the ring buffer.
;; this will reset "lit-pos" if necessary.
: push-last-instr  ( addr -- )
  (advance-buf) dup buffer-lit-pos = if lit-reset endif (put-addr)
  $IF $DEBUG-SINOPT-DUMP-PUSHES
    ." 3BUF(" buffer-tail 0 u.r ." ): " buffer-tail@ 0 u.r space buffer-prev@ 0 u.r space buffer-third@ 0 u.r cr
  $ENDIF
;

;; set by checker, for speed
0 value lit-instr-addr
0 value lit-arg-addr
0 value lit-value
0 value last-instr-addr
0 value last-instr-cfa

: prepare-globals  ( lit-instr-addr last-instr-addr -- )
  dup to last-instr-addr @ to last-instr-cfa
  dup to lit-instr-addr cell+ dup to lit-arg-addr @ to lit-value
;

;; check if we have a valid "(LIT)" and instructions without arguments.
;; they must be two latest instructions at HERE.
;; explicit "exit" used for slight speedup. sorry.
: lit-and-instr?  ( -- bool )
  buffer-lit-pos dup -if drop false exit endif
  buffer+@ dup prev-lit-cw = ifnot drop false exit endif  ( lit-instr-addr )
  buffer-tail@ dup last-cw = ifnot 2drop false exit endif   ( lit-instr-addr last-instr-addr )
  prepare-globals true
;

: dp!  ( addr -- ) forth:(dp-here) @ ! ;

;; replace last "(LIT)" value, remove other instructions.
: set-lit-value-only  ( value -- )
  lit-arg-addr !
  buffer-lit-pos to buffer-tail
  lit-arg-addr cell+ dp!
;

;; replace last "(LIT)" value, replace last instruction.
: set-lit-value-instr  ( value cfa -- )
  ;; fix those, because the only place where this is used is "+cells" optimisation,
  ;; and that optimisation rely on further optimisers.
  over to lit-value
  dup to last-instr-cfa
  last-instr-addr compile!
  lit-arg-addr !
;

;; replace last "(LIT)" with lit-arg instruction.
;; remove last instruction.
: replace-lit-with-arginstr  ( cfa -- )
  lit-instr-addr compile!
  last-instr-addr dp! lit-reset buffer-rollback-one
;

;; replace last "(LIT)" with simple instruction without operands.
: replace-lit  ( cfa -- )
  $IF 0
    ." REPLACE LIT! "
    buffer-tail@ 0 u.r space buffer-prev@ 0 u.r space buffer-third@ 0 u.r
    4 spaces buffer-lit-pos 0 u.r space buffer-tail 0 u.r
    cr
  $ENDIF
  lit-instr-addr compile!
  buffer-lit-pos to buffer-tail lit-reset
  lit-arg-addr dp!
;

;; remove last "(LIT)", and everything after it.
: remove-lit  ( cfa -- )
  lit-instr-addr dp!
  buffer-lit-pos cell- buffer-pos-mask and to buffer-tail lit-reset
;


;; check if we have a valid "(LIT)" and instruction with one argument.
;; they must be two latest instructions at HERE.
;; explicit "exit" used for slight speedup. sorry.
: lit-and-one-arg?  ( -- bool )
  buffer-lit-pos dup -if drop false exit endif
  buffer+@ dup here [ 4 cells ] imm-literal - = ifnot drop false exit endif  ( lit-instr-addr )
  buffer-tail@ dup here 8- = ifnot 2drop false exit endif   ( lit-instr-addr last-instr-addr )
  prepare-globals true
;

;; remove last "(LIT)". set direct poke instead.
;; note that "(LIT)" must be followed by direct poke.
: remove-lit-set-direct  ( cfa -- )
  lit-instr-addr compile!
  last-instr-addr cell+ @ lit-arg-addr !  ;; copy argument
  last-instr-addr dp! lit-reset buffer-rollback-one
;


0 value prev-instr-addr
0 value t/f-value

;; check if we have a valid "(LIT)" and instruction with one argument.
;; they must be two latest instructions at HERE.
;; explicit "exit" used for slight speedup. sorry.
: true/false-and-one-arg?  ( -- bool )
  buffer-tail@ dup here 8- = ifnot drop false exit endif   ( last-instr-addr )
  buffer-prev@ dup here [ 3 cells ] imm-literal - = ifnot 2drop false exit endif  ( lia pia )
  dup @ case
    ['] true of true to t/f-value endof
    ['] false of false to t/f-value endof
  otherwise drop 2drop false exit endcase
  to prev-instr-addr
  dup to last-instr-addr @ to last-instr-cfa true
;

;; replace previous instruction with direct poke.
;; last instruction must be a direct poke.
: replace-prev-with-direct  ( cfa -- )
  prev-instr-addr compile!
  ;; copy argument
  last-instr-addr cell+ @ prev-instr-addr cell+ dup cell+ dp! !
  \ buffer-reset  ;; don't bother
  buffer-rollback-one
;

: remove-prev-and-direct  ( -- )
  prev-instr-addr dp!
  buffer-reset  ;; don't bother
;


0 value v/c-value   ;; var/const value

;; check if we have a variable/constant as previous instruction,
;; folowed by simple instruction.
;; they must be two latest instructions at HERE.
: prev-var/const-and-instr?  ( -- bool )
  buffer-tail@ dup here 4- = ifnot drop false exit endif   ( last-instr-addr )
  buffer-prev@ dup here 8- = ifnot 2drop false exit endif  ( lia pia )
  dup @ dup compiler:variable-word? if drop false
  else compiler:constant-word? if true
  else 2drop false exit endif endif
  over to prev-instr-addr
  swap @ cfa->pfa swap if @ endif to v/c-value
  dup to last-instr-addr @ to last-instr-cfa true
;

;; use v/c-value as address
: replace-v/c-with-direct  ( cfa -- )
  prev-instr-addr compile!
  prev-instr-addr cell+ v/c-value over ! cell+ dp!
  buffer-rollback-one
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; debug
;;
$IF $DEBUG-SINOPT
: .curr-word   ( -- )  latest-nfa id-count xtype ;
: .last-instr  ( -- )  last-instr-cfa cfa->nfa id-count xtype ;
: .optim  ( addr count -- )  ." ***SINOPT(" here 0 u.r ." ) at `" .curr-word ." `:" type ." : " ;
$ENDIF


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; {lit} <and|or|xor|~and>  -->  (lit-...)
;;

: optimise-direct-bitwise  ( -- done? )
  last-instr-cfa case
    ['] forth:and of
        lit-value dup 0xff = if
          $IF $DEBUG-SINOPT AND $DEBUG-SINOPT-SIMPLE-BITWISE
            " direct-bitwise" .optim ." LO-BYTE" cr
          $ENDIF
          drop ['] forth:lo-byte replace-lit true exit
        endif
        0xffff = if
          $IF $DEBUG-SINOPT AND $DEBUG-SINOPT-SIMPLE-BITWISE
            " direct-bitwise" .optim ." LO-WORD" cr
          $ENDIF
          ['] forth:lo-word replace-lit true exit
        endif
        ['] forth:(lit-and)
      endof
    ['] forth:~and of ['] forth:(lit-~and) endof
    ['] forth:or of ['] forth:(lit-or) endof
    ['] forth:xor of ['] forth:(lit-xor) endof
  otherwise drop false exit endcase
  $IF $DEBUG-SINOPT AND $DEBUG-SINOPT-SIMPLE-BITWISE
    " direct-bitwise" .optim lit-value 0 .r cr
  $ENDIF
  replace-lit-with-arginstr true
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; table utilities
;;

: tw:  ( -- )  \ name
  -find-required dup compiler:constant-word? if cfa->pfa @ endif ,
;

;; special table values
-2 constant tbl-remove
-1 constant tbl-keep
 0 constant tbl-lit-0
 1 constant tbl-lit-1

: process-table  ( tbl index repl-cfa -- did-repl? )
  nrot +cells @ case
    tbl-remove of drop remove-lit true exit endof
    tbl-keep of drop false exit endof
    tbl-lit-0 of drop 0 set-lit-value-only true exit endof
    tbl-lit-1 of drop 1 set-lit-value-only true exit endof
  otherwise swap execute true exit endcase
;

: simple-replace-from-table  ( tbl index -- did-repl? )
  ['] replace-lit process-table
;

: direct-replace-from-table  ( tbl index -- did-repl? )
  ['] remove-lit-set-direct process-table
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; {lit} +
;; {lit} -
;;

;; starts with -8
create inc-cfa-tbl
  tw: forth:8-   ;; -8
  tw: tbl-keep   ;; -7
  tw: tbl-keep   ;; -6
  tw: tbl-keep   ;; -5
  tw: forth:4-   ;; -4
  tw: tbl-keep   ;; -6
  tw: forth:2-   ;; -2
  tw: forth:1-   ;; -1
  tw: tbl-remove ;;  0
  tw: forth:1+   ;; +1
  tw: forth:2+   ;; +2
  tw: tbl-keep   ;; +3
  tw: forth:4+   ;; +4
  tw: tbl-keep   ;; +5
  tw: tbl-keep   ;; +6
  tw: tbl-keep   ;; +7
  tw: forth:8+   ;; +8
create;

: optimise-inc-dec  ( -- done? )
  lit-value -8 9 within ifnot false exit endif
  inc-cfa-tbl last-instr-cfa case
    ['] forth:+ of lit-value endof
    ['] forth:- of lit-value negate endof
  otherwise 2drop false exit endcase
  8+ simple-replace-from-table
  $IF $DEBUG-SINOPT AND $DEBUG-SINOPT-SIMPLE-INC-DEC
    dup if
      " inc-dec" .optim lit-value . cr
    endif
  $ENDIF
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; {lit} *
;; {lit} u*
;;

;; starts with -1
create lit-mul-tbl
  tw: forth:negate ;; -1
  tw: tbl-lit-0    ;;  0
  tw: tbl-remove   ;; +1
  tw: forth:2*     ;; +2
  tw: tbl-keep     ;; +3
  tw: forth:4*     ;; +4
  tw: tbl-keep     ;; +5
  tw: tbl-keep     ;; +6
  tw: tbl-keep     ;; +7
  tw: forth:8*     ;; +8
create;

: optimise-mul  ( -- done? )
  lit-value -1 9 within ifnot false exit endif
  last-instr-cfa case
    ['] * of lit-mul-tbl endof
    ['] u* of lit-mul-tbl endof
  otherwise drop false exit endcase
  lit-value 1+ simple-replace-from-table
  $IF $DEBUG-SINOPT AND $DEBUG-SINOPT-SIMPLE-MULS
    dup if
      " mul" .optim lit-value 0 .r cr
    endif
  $ENDIF
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; {lit} /
;; {lit} u/
;;

;; starts with -1
create lit-div-tbl
  tw: forth:negate ;; -1
  tw: tbl-keep     ;;  0
  tw: tbl-remove   ;; +1
  tw: forth:2/     ;; +2
  tw: tbl-keep     ;; +3
  tw: forth:4/     ;; +4
  tw: tbl-keep     ;; +5
  tw: tbl-keep     ;; +6
  tw: tbl-keep     ;; +7
  tw: forth:8/     ;; +8
create;

;; starts with -1
create lit-udiv-tbl
  tw: tbl-keep     ;; -1
  tw: tbl-keep     ;;  0
  tw: tbl-remove   ;; +1
  tw: forth:2u/    ;; +2
  tw: tbl-keep     ;; +3
  tw: forth:4u/    ;; +4
  tw: tbl-keep     ;; +5
  tw: tbl-keep     ;; +6
  tw: tbl-keep     ;; +7
  tw: forth:8u/    ;; +8
create;

: optimise-div  ( -- done? )
  lit-value -1 9 within ifnot false exit endif
  last-instr-cfa case
    ['] / of lit-div-tbl endof
    ['] u/ of lit-udiv-tbl endof
  otherwise drop false exit endcase
  lit-value 1+ simple-replace-from-table
  $IF $DEBUG-SINOPT AND $DEBUG-SINOPT-SIMPLE-MULS
    dup if
      " div" .optim lit-value 0 .r cr
    endif
  $ENDIF
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; {lit} lsh
;; {lit} ash
;; {lit} lshift
;; {lit} rshift
;; {lit} arshift
;;

;; starts with -3
create lit-lsh-tbl
  tw: forth:8u/  ;; -3
  tw: forth:4u/  ;; -2
  tw: forth:2u/  ;; -1
  tw: tbl-remove ;;  0
  tw: forth:2*   ;; +1
  tw: forth:4*   ;; +2
  tw: forth:8*   ;; +3
create;

;; starts with -3
create lit-ash-tbl
  tw: forth:8/   ;; -3
  tw: forth:4/   ;; -2
  tw: forth:2/   ;; -1
  tw: tbl-remove ;;  0
  tw: forth:2*   ;; +1
  tw: forth:4*   ;; +2
  tw: forth:8*   ;; +3
create;

: optimise-shifts  ( -- done? )
  lit-value -3 4 within ifnot false exit endif
  lit-value last-instr-cfa case
    ['] forth:lsh of 3 + lit-lsh-tbl endof
    ['] forth:ash of 3 + lit-ash-tbl endof
    ['] forth:lshift of dup -if drop false exit endif 3 + lit-lsh-tbl endof
    ['] forth:rshift of dup -if drop false exit endif negate 3 + lit-lsh-tbl endof
    ['] forth:arshift of dup -if drop false exit endif negate 3 + lit-ash-tbl endof
  otherwise 2drop false exit endcase
  swap simple-replace-from-table
  $IF $DEBUG-SINOPT AND $DEBUG-SINOPT-SIMPLE-MULS
    dup if
      " shift" .optim lit-value 0 .r cr
    endif
  $ENDIF
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; {lit} cells
;;

: optimise-cells  ( -- done? )
  last-instr-cfa ['] cells = ifnot false exit endif
  $IF $DEBUG-SINOPT AND $DEBUG-SINOPT-SIMPLE-CELLS
    " cells" .optim lit-value 0 .r cr
  $ENDIF
  lit-value cells set-lit-value-only true
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; {lit} +cells
;; {lit} -cells
;;

: optimise-cells-inc-dec  ( -- done? )
  last-instr-cfa case
    ['] +cells of ['] + endof
    ['] -cells of ['] - endof
  otherwise drop false exit endcase
  $IF $DEBUG-SINOPT AND $DEBUG-SINOPT-SIMPLE-CELLS-INC-DEC
    " cells-inc-dec " .optim lit-value 0 .r cr
  $ENDIF
  lit-value cells swap set-lit-value-instr true
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; {lit} (direct:!)
;;

: optimise-direct-poke  ( -- done? )
  last-instr-cfa ['] (direct:!) = ifnot false exit endif
  lit-value case
    -1 of ['] forth:(direct:-1:!) endof
     0 of ['] forth:(direct:0:!) endof
     1 of ['] forth:(direct:1:!) endof
  otherwise drop false exit endcase
  $IF $DEBUG-SINOPT AND $DEBUG-SINOPT-SIMPLE-DIRECT-POKES
    " direct-poke" .optim lit-value 0 .r cr
  $ENDIF
  remove-lit-set-direct true
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; {lit} (direct:+!)
;; {lit} (direct:-!)
;;

;; starts with -8
create direct-inc-tbl
  tw: (direct:8-!) ;; -8
  tw: tbl-keep     ;; -7
  tw: tbl-keep     ;; -6
  tw: tbl-keep     ;; -5
  tw: (direct:4-!) ;; -4
  tw: tbl-keep     ;; -3
  tw: (direct:2-!) ;; -2
  tw: (direct:1-!) ;; -1
  tw: tbl-remove   ;;  0
  tw: (direct:1+!) ;; +1
  tw: (direct:2+!) ;; +2
  tw: tbl-keep     ;; +3
  tw: (direct:4+!) ;; +4
  tw: tbl-keep     ;; +5
  tw: tbl-keep     ;; +6
  tw: tbl-keep     ;; +7
  tw: (direct:8+!) ;; +8
create;

: optimise-direct-poke-inc  ( -- done? )
  lit-value -8 9 within ifnot false exit endif
  last-instr-cfa case
    ['] (direct:+!) of lit-value endof
    ['] (direct:-!) of lit-value negate endof
  otherwise drop false exit endcase
  8+ direct-inc-tbl swap direct-replace-from-table
  $IF $DEBUG-SINOPT AND $DEBUG-SINOPT-SIMPLE-DIRECT-POKE-INCS
    dup if
      " direct-poke-inc" .optim lit-value 0 .r cr
    endif
  $ENDIF
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;  false (direct:!)
;;  true (direct:!)
;;

: optimise-direct-poke-bool  ( -- done? )
  last-instr-cfa ['] (direct:!) = ifnot false exit endif
  $IF $DEBUG-SINOPT AND $DEBUG-SINOPT-SIMPLE-DIRECT-POKES
    " direct-poke-bool" .optim lit-value 0 .r cr
  $ENDIF
  t/f-value case
    -1 of ['] forth:(direct:-1:!) endof
     0 of ['] forth:(direct:0:!) endof
     1 of ['] forth:(direct:1:!) endof
  otherwise " wuta?!" error endcase -- assertion
  replace-prev-with-direct true
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;  false (direct:+!)  -->  none
;;  false (direct:-!)  -->  none
;;  true (direct:+!)  -->  (direct:1-!)
;;  true (direct:-!)  -->  (direct:1+!)
;;

: optimise-direct-poke-inc-bool  ( -- done? )
  last-instr-cfa case
    ['] (direct:+!) of
        t/f-value ifnot remove-prev-and-direct true exit endif
        t/f-value
      endof
    ['] (direct:-!) of
        t/f-value ifnot remove-prev-and-direct true exit endif
        t/f-value negate
      endof
  otherwise drop false exit endcase
  $IF $DEBUG-SINOPT AND $DEBUG-SINOPT-SIMPLE-DIRECT-POKES
    " direct-poke-inc-bool" .optim dup 0 .r cr
  $ENDIF
  case
    -1 of ['] forth:(direct:1-!) endof
     1 of ['] forth:(direct:1+!) endof
  otherwise " wuta?!" error endcase -- assertion
  replace-prev-with-direct true
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; var @
;; var !
;; var +!
;; var -!
;; with this optimisation, vars are as fast as direct memory access
;;

: optimise-vars  ( -- done? )
  last-instr-cfa case
    ['] @ of ['] forth:(direct:@) endof
    ['] ! of ['] forth:(direct:!) endof
    ['] +! of ['] forth:(direct:+!) endof
    ['] -! of ['] forth:(direct:-!) endof
  otherwise drop false exit endcase
  $IF $DEBUG-SINOPT AND $DEBUG-SINOPT-VARS
    " var-access" .optim
    ." var:`" prev-instr-addr @ cfa->nfa id-count xtype
    ." `; op:" last-instr-cfa cfa->nfa id-count xtype
    \ ."  addr: " v/c-value 0 u.r
    cr
  $ENDIF
  replace-v/c-with-direct true
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; swap drop  -->  nip
;; rot rot    -->  nrot
;; swap !     -->  swap!
;;

: optimise-stack-ops  ( -- done? )
  buffer-tail@ dup last-cw = ifnot drop false exit endif
  buffer-prev@ dup prev-cw = ifnot 2drop false exit endif
  @ swap @ case  ;; prev tail
    ['] drop of
        ['] swap = ifnot false exit endif
        ['] nip
      endof
    ['] rot of
        ['] rot = ifnot false exit endif
        ['] nrot
      endof
    ['] ! of
        ['] swap = ifnot false exit endif
        ['] swap!
      endof
    ['] w! of
        ['] swap = ifnot false exit endif
        ['] swap-w!
      endof
    ['] c! of
        ['] swap = ifnot false exit endif
        ['] swap-c!
      endof
  otherwise 2drop false exit endcase
  $IF $DEBUG-SINOPT AND $DEBUG-SINOPT-STACK-OPS
    " stack-ops" .optim
    dup cfa->nfa id-count xtype
    cr
  $ENDIF
  buffer-prev@ swap over compile! cell+ dp!
  buffer-reset true  ;; don't bother restoring the buffer for now
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; swap 1+ swap  -->  (swap:1+:swap)
;;

: optimise-swap-1+-swap  ( -- done? )
  ;; last swap
  buffer-tail@ dup last-cw = ifnot drop false exit endif
  @ ['] swap = ifnot false exit endif
  ;; 1+
  buffer-prev@ dup prev-cw = ifnot drop false exit endif
  @ ['] 1+ = ifnot false exit endif
  ;; first swap
  buffer-third@ dup here 12 - = ifnot drop false exit endif
  @ ['] swap = ifnot false exit endif
  $IF $DEBUG-SINOPT AND $DEBUG-SINOPT-SIMPLE-SWAP-INC-SWAP
    " swap-1+-swap" .optim cr
  $ENDIF
  buffer-third@ ['] forth:(swap:1+:swap) over compile! cell+ dp!
  buffer-reset true  ;; don't bother restoring the buffer for now
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; main dispatcher
;;

: instr-compiled  ( addr -- )
  push-last-instr
  lit-and-instr? if  ;; we just compiled a literal
    optimise-direct-bitwise
    dup ifnot drop optimise-cells endif
    dup ifnot drop optimise-cells-inc-dec drop false endif -- keep optimising, inc/dec might do more
    dup ifnot drop optimise-inc-dec endif
    dup ifnot drop optimise-mul endif
    dup ifnot drop optimise-div endif
    dup ifnot drop optimise-shifts endif
    drop
  endif
  prev-var/const-and-instr? if
    optimise-vars drop \ if exit endif -- direct optimisers below will do more this way
  endif
  ;; direct pokes
  lit-and-one-arg? if
    optimise-direct-poke if exit endif
    optimise-direct-poke-inc if exit endif
  endif
  true/false-and-one-arg? if
    optimise-direct-poke-bool if exit endif
    optimise-direct-poke-inc-bool if exit endif
  endif
  optimise-swap-1+-swap if exit endif
  optimise-stack-ops if exit endif
;
buffer-reset


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; reset
;;

: install  ( -- )
  enabled? ifnot
    true to enabled?
    buffer-reset
    ;; called by "compiler:(<j-mark)"
    ;; erase literal address, so "lit begin and" will not be optimised to shit
    ['] buffer-reset to compiler:(jump-here-marked)
    ['] buffer-reset to compiler:(reset-sinopt)
    ['] push-literal to compiler:(after-compile-lit)
    ['] instr-compiled to compiler:(after-compile-word)
  endif
;

: uninstall  ( -- )
  enabled? if
    false to enabled?
    buffer-reset
    ;; called by "compiler:(<j-mark)"
    ;; erase literal address, so "lit begin and" will not be optimised to shit
    ['] forth:noop to compiler:(jump-here-marked)
    ['] forth:noop to compiler:(reset-sinopt)
    ['] forth:drop to compiler:(after-compile-lit)
    ['] forth:drop to compiler:(after-compile-word)
  endif
;

install

: temp-disable  ( -- )
  enabled? if
    ['] forth:drop to compiler:(after-compile-lit)
    ['] forth:drop to compiler:(after-compile-word)
  endif
;

: temp-restore  ( -- )
  enabled? if
    ['] push-literal to compiler:(after-compile-lit)
    ['] instr-compiled to compiler:(after-compile-word)
  endif
;

prev-defs


: sinopt-enable  ( -- )  forth:(sinopt-peephole):install ;
: sinopt-disable  ( -- )  forth:(sinopt-peephole):uninstall ;
: sinopt-enabled?  ( -- bool )  forth:(sinopt-peephole):enabled? ;
: sinopt-enabled!  ( bool -- )  if forth:(sinopt-peephole):install else forth:(sinopt-peephole):uninstall endif ;

;; optimising compiled UrAsm code slows everything down a lot
: sinopt-temp-restore  ( -- )  forth:(sinopt-peephole):temp-restore ;
: sinopt-temp-disable  ( -- )  forth:(sinopt-peephole):temp-disable ;

;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; and now for something completely different...
;; UrForth/C Forth Engine!
;; Copyright (C) 2023 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; statically and dynamically allocated structures
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(*
  each struct is a vocabulary (without a hash table, i don't expect
  structs to have enough fields to varrant using a hash table).

  that vocabulary contains words with field names. such word does
  access to struct field (via "DOES>"), and also contains field
  info in pfa:
    offset
    size    ; this is unused yet, and always 4
    owner struct vocid

  special predefined fields:
    @size -- returns total struct size, in bytes: MyStruct:@size
    @parent -- returns parent vocid or 0
    @self   -- returns self vocid

  internally, each struct has 3 those fields preallocated as first 3 cells.

  declaring structs:
    STRUCT:DEFINE MyStruct [ EXTENDS OtherStruct ]
      FIELD: a
      FIELD: b
    END-STRUCT MyStruct

  using structs:

    MyStruct STRUCT:NEW value stx -- dynamically allocated, creates dynamic memory handle
    ." orig a: " stx MyStruct:a . cr
    666 stx MyStruct:a!
    ." new a: " stx MyStruct:a . cr

    ." size of a: " MyStruct size@ . cr

    stx 69 MyStruct:a!
    ." new a: " stx MyStruct:a . cr

    : stx-set-test  ( stx -- )  dup  666 MyStruct:b!  MyStruct:b . cr ;
    stx stx-set-test

    MyStruct STRUCT:ALLOT TO stx

    CREATE sst MyStruct:@size ALLOT CREATE; -- creates static struct
*)

;; create main vocab
simple-vocabulary struct
also struct definitions

;; create vocab with hidden internal words
simple-nested-vocabulary struct-internals
also struct-internals definitions

;; wordlist typeid for structs
;; this should use some central repository in the future
0x029a_0069 constant (wl-struct-typeid)

;; we'll save "CURRENT" in "DEFINE" here, to restore it in "END-STRUCT"
0 value (saved-current)

;; struct size offset in wordlist
;; -4 is typeid
-2 cells constant (sofs-vocid-size)

;; internal field offsets in *allocated* struct
0 cells constant (sofs-size)
1 cells constant (sofs-parent)
2 cells constant (sofs-self)
;; size of all internal fields
3 cells constant (sofs-internal-size)

;; offsets from field word PFA
0 cells constant (fofs-ofs)
1 cells constant (fofs-size)
2 cells constant (fofs-owner)


;; doer for main struct word
;; we don't need it to change context vocab
: (struct-doer)  ( pfa -- vocid )  @ ;

;; vocid utils
: (vocid-struct-size@)  ( vocid -- size ) (sofs-vocid-size) + @ ;
: (vocid-struct-size!)  ( size vocid -- ) (sofs-vocid-size) + ! ;
: (vocid-struct?)  ( vocid -- bool )  (vocid-typeid@) (wl-struct-typeid) = ;
: (word-struct?)  ( cfa -- bool )  @ ['] (struct-doer) cfa->pfa = ;
: (addr-struct?)  ( addr/stx -- bool )
  dup (handle-addr?) if handle:typeid@
  else (sofs-self) + @ (vocid-typeid@) endif
  (wl-struct-typeid) =
;


;; create struct wordlist
: (create-wlist)  ( -- vocid )
  ;; allocate some struct helper data
  ;;   -8: size-in-bytes
  ;; calculate our initial size
  (sofs-internal-size) forth:, ;; store initial size
  0 false forth:(new-wordlist)   ;; create wordlist without a hash table
  ;; setup type id
  (wl-struct-typeid) over (vocid-typeid!)
;


;; create new field in the current vocabulary
;; "CURRENT" must be a struct vocab
: (create-field)  ( fofs doer-cfa addr count -- )
  forth:(new-word-flags) dup @ >r compiler:(wflag-protected) swap or!
  (create)
  swap forth:, ;; offset
  cell ,       ;; size
  current @ ,  ;; owner vocid
  create;
  cfa->pfa latest-cfa compiler:(set-doer)
  r> forth:(new-word-flags) !
;


;; check if the handle/address of the allocated struct is of a good type
: (check-type)  ( stx vocid -- )
  over (addr-struct?) " allocated struct expected" ?not-error
  ;; get stx-self
  a>r swap >a (sofs-self) @a+ r>a  ( vocid parent-vocid )
  begin
    dup " invalid struct type" ?not-error
    2dup <>
  while
    (vocid-parent@)
  repeat
  2drop
;

;; doers for user fields

;; address register contains struct base
: (@a-field-doer)  ( pfa -- value )
  a> over (fofs-owner) + @ (check-type)
  (fofs-ofs) + @ @a+
;

: (!a-field-doer)  ( value pfa -- value )
  a> over (fofs-owner) + @ (check-type)
  (fofs-ofs) + @ !a+
;

: (@field-doer)  ( stx pfa -- value )
  a>r swap >a (@a-field-doer) r>a
;

: (!field-doer)  ( value stx pfa -- value )
  a>r swap >a (!a-field-doer) r>a
;

;; check if we aren't defining a struct
: (?normal)  ( -- )
  compiler:?exec
  current @ (vocid-typeid@)  " already defining some data structure" ?error
;

;; check if we are defining a struct
: (?struct)  ( -- )
  compiler:?exec
  current @ (vocid-struct?) " not defining a struct" ?not-error
;


;; CURRENT must be the empty struct wordlist
: (create-default-fields)  ( -- )
  ;; temporarily remove parent, to disable "word redefined" warnings
  current @ dup (vocid-parent@) >r >r  ( | parent vocid )
  0 r@ (vocid-parent!)

  <public-words> \ <protected-words>

  (sofs-size) ['] (@field-doer) " @size" (create-field)
  (sofs-parent) ['] (@field-doer) " @parent" (create-field)
  (sofs-self) ['] (@field-doer) " @self" (create-field)

  ;; restore parent
  r> r> swap (vocid-parent!)
;

previous definitions
also struct-internals


;; high-level API
: define  ( -- )  \ name [ EXTENDS OtherStruct ]
  (?normal)
  parse-skip-line-comments parse-name dup " struct name expected" ?not-error
  (create-wlist)
  dup  >r nrot forth:(create-named-vocab)  ;; ( | vocid )
  ;; HACK! set DOER for non-CREATEd word
  ;; it works due to how doers are implemented in VM
  ['] (struct-doer) cfa->pfa latest-cfa compiler:(set-doer-no-checks)
  ;; define new words in this dictionary (because why not)
  current @ to (saved-current)
  also r> current ! vocid: struct context !
  (create-default-fields)
;

: extends  ( -- )  \ name
  (?struct)
  ;; we can extend only structs without parents, and
  ;; the struct cannot have any user fields defined yet
  current @  ( vocid )
  ;; check parent
  dup (vocid-parent@) " cannot extend already extended struct" ?error
  ;; check size
  dup (vocid-struct-size@) (sofs-internal-size) <> " cannot extend struct with defined fields" ?error
  ;; ok, we can do it, parse parent struct name
  parse-skip-line-comments -find-required  ( vocid cfa )
  ;; check if the given name is a struct
  dup (word-struct?) " vocabulary name expected" ?not-error
  (word->vocid)  ( vocid parent-vocid )
  ;; check type
  dup (vocid-struct?) " struct name expected" ?not-error
  ;; do not try to extend ourself
  2dup = " struct cannot extend itself" ?error
  ;; ok, we can set a parent
  2dup swap (vocid-parent!)   ( vocid parent-vocid )
  ;; we have to fix current struct size too
  (vocid-struct-size@)       ;; get parent struct size
  swap (vocid-struct-size!)  ;; and store it as our own
;

;; define struct field
: field:  ( -- )  \ name
  (?struct)
  ;; build it manually
  parse-skip-line-comments parse-name dup " field name expected" ?not-error
  current @ (vocid-struct-size@) dup >r  ;; current struct size is field offset
  ['] (@field-doer)    ;; doer ( addr count ofs doer-cfa | ofs )
  2over (create-field) ;; ( addr count | ofs )
  ;; save original char (it is safe to assume that we have one free byte in TIB)
  2dup + c@ >r rswap   ;; ( addr count | oldch ofs )
  ;; create "name!"
  2dup + [char] ! swap c!
  r> ['] (!field-doer) 2over 1+ (create-field)
  ;; restore original char
  + r> swap c!
  ;; now fix struct size
  cell current @ (sofs-vocid-size) + +!
;

;; finish structure definition
: end-struct  ( -- )  \ [structname]
  (?struct)
  parse-skip-line-comments parse-name dup if
    find-word " wut?" ?not-error
    dup (word-struct?) " invalid struct name" ?not-error
    (word->vocid) current @ <> " invalid struct name" ?error
  else 2drop endif
  previous
  (saved-current) current !
;

;; for vocids
: size-of  ( vocid -- size )
  dup (vocid-struct?) " struct vocid expected" ?not-error
  (vocid-struct-size@)
;

: parent-of  ( vocid -- parent-vocid / 0 )
  dup (vocid-struct?) " struct vocid expected" ?not-error
  (vocid-parent@)
;

;; get name of the structure pointed to by the given vocid
: name-of  ( vocid -- addr count )
  dup (vocid-struct?) " struct vocid expected" ?not-error
  compiler:(vocofs-header) + @
  ?dup if id-count else 0 0 endif
;

;; for allocated structs
: @size-of  ( stx -- size )
  dup (addr-struct?) " allocated struct expected" ?not-error
  a>r >a (sofs-size) @a+ r>a
;

: @parent-of  ( stx -- parent-vocid / 0 )
  dup (addr-struct?) " allocated struct expected" ?not-error
  a>r >a (sofs-parent) @a+ r>a
;

: @id-of  ( stx -- vocid )
  dup (addr-struct?) " allocated struct expected" ?not-error
  a>r >a (sofs-self) @a+ r>a
;

;; get name of the structure pointed to by the given vocid
: @name-of  ( vocid -- addr count )
  dup (addr-struct?) " allocated struct expected" ?not-error
  a>r >a (sofs-self) @a+ r>a
  dup (vocid-struct?) " struct vocid expected" ?not-error
  compiler:(vocofs-header) + @
  ?dup if id-count else 0 0 endif
;


;; allocate dynamic struct
: new  ( vocid -- stx )
  (?normal)  ;; just in case
  dup (vocid-struct?) " struct expected" ?not-error  ( vocid )
  >r (wl-struct-typeid) handle:new  ( stx | vocid )
  ;; allocate memory
  r@ (vocid-struct-size@) over handle:size!
  ;; setup internal struct fields
  ;; size
  r@ (vocid-struct-size@) over (sofs-size) swap handle:!
  ;; parent
  r@ (vocid-parent@) over (sofs-parent) swap handle:!
  ;; self
  r> over (sofs-self) swap handle:!
;

;; allocate dynamic struct
: allot  ( vocid -- dict-addr )
  (?normal)  ;; just in case
  dup (vocid-struct?) " struct expected" ?not-error  ( vocid )
  ;; allocate memory
  dup >r (vocid-struct-size@) n-allot  ( addr | vocid )
  ;; setup internal struct fields
  ;; size
  r@ (vocid-struct-size@) over (sofs-size) + !
  ;; parent
  r@ (vocid-parent@) over (sofs-parent) + !
  ;; self
  r> over (sofs-self) + !
  ;; return address
;

alias-for vocid: is id:


previous previous definitions

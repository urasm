;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; and now for something completely different...
;; UrForth/C Forth Engine!
;; Copyright (C) 2023 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; slightly more complicated OO system than mini-oof
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(*
  each class has inheritance table (16 items; more than enough).
  tbl[level] points to self.
  maximum number of methods is 128.

  internal class layout:
    typeid-class   (signature)
    instance-size  (bytes)
    level          (inheritance level)
    class-word-pfa (created class word)
    vocid          (class vocid)
    <inheritance-table>
    <vmt-table>

  VMT layout:
    method-count (number of methods in VMT)
    method-cfas

  internal object layout:
    typeid-instance (signature)
    class-info-addr (pointer to class info above)

  class vocabulary contains all class variables and methods.

  class word pfa contains:
    typeid-class-word (signature)
    info-addr         (address of class info table)

  field word pfa contains:
    typeid-field-word (signature)
    info-addr         (address of class info table)
    offset            (field offset in instance, in bytes)

  method word pfa contains:
    typeid-method-word (signature)
    info-addr          (address of class info table)
    offset             (method offset in VMT, in bytes)

  usage:
    oof:class: MyClass [ extends: OtherClass ]
      field: fld
      method: mth
    end-class [ MyClass ]

    MyClass oof:method: ... ;

    field/method read inside a method: simply using the name.
    field write inside a method: "TO name"
    field address inside a method: "-> name"

    special predefined fields:
      @class -- get info-addr

    call inherited methods (inside a method, of course):
      inherited -- with the same name

    special fields inside a method:
      self -- pointer to instance or to a class info

    checks:
      cls-or-inst cls-or-inst oof:isa?
        check if the first arg is a second or its subclass
      cls-or-inst oof:invoke-dyn <mtname>
        invoke method
      cls-or-inst addr count oof:invoke-str

    special object values:
      classname oof:obj-value <name> -- immediate
      <name> method
    using such values performs type checking on assignment, and
    method calling is much faster (dispatched by index, not by name string).
    to get the contents (including 0, which is NULL), use: "oof:value-of: <name>".
    to get the bound class, use: "oof:class-of: <name>".
    to rebind to the new class, use: "<class> oof:bind: <name>". note that
    rebinding will fail with fatal error if the value already bound.

    obj-value fields:
      classname obj-value: <field>
    here we have one problem: defining a reference to the class which is
    not defined yet. it is done by using "0" instead of class name, and
    later bind it with:
      bind-class classname oof:bind-field: <field>
*)

vocabulary oof
also-defs: oof

 16 constant max-inheritance-level
128 constant max-vmt-methods

vocabulary oof-internal
also-defs: oof-internal

;; various typeids
666_666 enum-from{
  def: typeid-class           ;; first cell of class definition holds this
  def: typeid-instance        ;; first cell of instance holds this
  def: typeid-class-word      ;; first pfa cell of class name word holds this
  def: typeid-field-word      ;; first pfa cell of class field word holds this
  def: typeid-method-word     ;; first pfa cell of class method word holds this
  def: typeid-objfield-word   ;; first pfa cell of class obj-value field word holds this
  def: typeid-@class-word     ;; first pfa cell of class @class word holds this
  def: typeid-objval-word     ;; first pfa cell of oof:obj-value word holds this
}

;; field or objval-field?
: field-typeid?  ( typeid -- bool )
  dup typeid-field-word = swap typeid-objfield-word = or
;

;; are we defining class method? (holds info-addr)
0 value curr-method-def-class
0 value curr-method-def-vmtofs
0 value current-class-def


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; create class info accessor words
;;

define-accessors class
  def: typeid
  def: inst-size
  def: level
  def: def-pfa
  def: vocid
end-accessors

: class->iht  ( info-addr -- iht-addr )  class-size + ;
: class->vmt  ( info-addr -- vmt-addr )
  [ class-size max-inheritance-level +cells ] imm-literal +
;

: class->name@  ( class -- addr count )  class->def-pfa@ pfa->nfa id-count ;

: inst->class  ( instance -- info-addr-addr )  cell+ ;
: inst->class@ ( instance -- info-addr )  inst->class @ ;
: inst->class! ( info-addr instance -- )  inst->class ! ;

2 cells constant initial-inst-size

;; field/method word offsets
define-accessors fword
  def: typeid
  def: info-addr
  def: ofs
  def: doer-cfa
  def: objval-info-addr
end-accessors

;; for obj-values
alias-for fword->ofs is fword->value


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; class info creation
;;

: ?class-info  ( info-addr -- info-addr )
  dup " class id expected" ?not-error
  dup class->typeid@ typeid-class <> " class id expected" ?error
;

: ?inst-addr  ( addr -- addr )
  dup " object expected" ?not-error
  dup @ typeid-instance <> " object expected" ?error
;

: ?in-class-def  ( -- )
  current-class-def " not defining a class" ?not-error
;

;; convert instance or class-info address to class-info address
: (inst->info-addr)  ( instance/info -- info-addr )
  dup @ dup typeid-instance = if drop inst->class@ dup @ endif
  typeid-class <> " not instance/class" ?error
;

;; create new class info record
: create-class-info  ( vocid -- info-addr )
 $IF 0
  ;; create in dictionary
  here swap  ( info-addr vocid )
  ;; create the header struct
  typeid-class ,      ;; signature
  initial-inst-size , ;; initial instance size
  0 ,                 ;; inheritance level
  0 ,                 ;; def-pfa
  ,                   ;; vocid
  ;; create inheritance table
  dup , max-inheritance-level 1- for 0 , endfor
  ;; create VMT
  0 , max-vmt-methods 1- for ['] (notimpl) , endfor
 $ELSE
  ;; create in dynamic memory
  class-size max-inheritance-level max-vmt-methods + cells + over handle:new-alloc
  swap  ( info-addr vocid )
  a>r over >a
  ;; create the header struct
  typeid-class !+4>a      ;; signature
  initial-inst-size !+4>a ;; initial instance size
  0 !+4>a                 ;; inheritance level
  0 !+4>a                 ;; def-pfa
  !+4>a                   ;; vocid
  ;; create inheritance table
  dup !+4>a max-inheritance-level 1- for 0 !+4>a endfor
  ;; create VMT
  0 !+4>a max-vmt-methods 1- for ['] (notimpl) !+4>a endfor
  r>a
 $ENDIF
;

: class-link-wordlist-to-parent  ( my-info-addr -- )
  dup class->level@ " bad inherit-from call" ?not-error  ;; "my" should not be at level 0
  dup class->vocid@  ( my myvocid )
  over class->level@ 1-  ( my myvocid parent-level )
  cells rot class->iht + @  ( myvocid inh-ofs parent-info-addr )
  class->vocid@  ( myvocid parentvocid )
  swap (vocid-parent!)
;

;; use only on newly created classes
;; make "my" subclass of "super"
: class-inherit-from  ( super-info-addr my-info-addr -- )
  dup class->level@ " bad inherit-from call" ?error  ;; "my" should be at level 0
  ;; check instance size
  dup class->inst-size@ initial-inst-size <> " cannot inherit to non-empty class" ?error
  ;; copy instance size
  over class->inst-size@ over class->inst-size!
  >r  ( super-info-addr | my-info-addr )
  ;; check maximum inheritance level
  dup class->level@ 1+ max-inheritance-level = " inheritance level too deep" ?error
  ;; copy tables (they're all consecutive, use this fact)
  dup  class->iht  r@ class->iht
  max-inheritance-level max-vmt-methods + cmove-cells
  ;; now fix inheritance level
  class->level@ 1+ dup r@ class->level!  ( level | my )
  ;; and put ourself into inheritance table
  cells r@ class->iht + r@ swap!
  r> class-link-wordlist-to-parent
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; field accessor words
;;

: (isa?)  ( my-instance/info other-instance/info -- my-isa-other? )
  (inst->info-addr) swap (inst->info-addr) swap
  ;; easy check: our inheritance table should have other-info-addr at its level
  ( my other )
  dup class->level@  ( my other other-level )
  cells rot class->iht + @ =
;

: (find-field-word)  ( addr count info-addr -- pfa )
  ?class-info  ( addr count info-addr )
  >r 2dup r>  ( addr count addr count info-addr )
  class->vocid@ find-word-in-voc-and-parents ifnot ( addr count )
    endcr space xtype ." ? -- method/field not found" cr " method/field not found" error
  else ( addr count cfa ) nrot 2drop
  endif
  cfa->pfa
;

: (find-method-vmtofs)  ( addr count info-addr -- vmtofs )
  (find-field-word)
  dup fword->typeid @ typeid-method-word <> " trying to invoke non-method" ?error
  fword->ofs @
;

: (find-method)  ( addr count info-addr -- cfa )
  dup >r (find-method-vmtofs)
  r> class->vmt + @  ( cfa )
;

: (bind-execute)  ( inst-addr method-cfa )
  (self@) >r swap (self!) execute r> (self!)
;

: (vmt-call)  ( inst-addr vmtofs -- )
  over " trying to use NULL object" ?not-error
  over inst->class@ class->vmt + @ ( inst-addr method-cfa ) (bind-execute)
;

: (field-read)  ( inst-addr instofs -- )
  over " trying to use NULL object" ?not-error
  + @
;

: (obj-value-!)  ( value pfa+4 -- )
  over if
    over @ typeid-instance <> " trying to assign non-object" ?error
    2dup @ (isa?) " invalid assignment (type)" ?not-error
    cell+ !
  else  ;; 0 is always allowed
    cell+ 0! drop
  endif
;

;; moved to C kernel
\ : (self@+)  ( ofs -- @self+ofs )  (self@) + ;

;; accessor for "->field"
: (field-acc-addr)  ( pfa -- addr )
  ;; (self@) ofs +
  ;; do not optimise for 0 offset: first, it is rare case,
  ;; and second, let the peephole optimiser take care of it
  fword->ofs @ literal compile (self@+)
;

;; accessor for "field"
: (field-acc-@)  ( pfa -- value )
  ;; (self@) ofs + @
  ;; again, let the peephole optimiser do its work
  (field-acc-addr) compile @
;

;; accessor for "field!"
: (field-acc-!)  ( value pfa -- )
  ;; (self@) ofs + !
  ;; again, let the peephole optimiser do its work
  (field-acc-addr) compile !
;

;; accessor for obj-value "field"
: (objval-acc-run)  ( pfa -- )  \ name
  ;; first check if it is bound
  dup fword->objval-info-addr @ dup " obj-value field is not bound to anything" ?not-error
  >r  ( pfa | info-addr )
  parse-name dup " method name expected" ?not-error
  ( pfa addr count | info-addr )
  r> (find-field-word)  ( pfa word-pfa )
  ;; check type
  dup fword->typeid @  ( pfa word-pfa typeid )
  dup typeid-method-word <> over field-typeid? not and " trying to access something wrong" ?error
  ( pfa word-pfa typeid )
  swap fword->ofs @ rot   ( typeid ofs pfa )
  (field-acc-@) literal ( typeid )
  typeid-method-word = if compile (vmt-call) else compile (field-read) endif
;

;; this is what compiled to the method body
: (objval-do-!)  ( value info-addr inst-ofs -- )
  >r over if 2dup (isa?) " invalid type for obj-value field" ?not-error endif drop
  r> (self@+) !
;

;; accessor for obj-value "field!"
: (objval-acc-!)  ( pfa -- )
  ;; first check if it is bound
  dup fword->objval-info-addr @ dup " obj-value field is not bound to anything" ?not-error ( pfa info-addr )
  literal fword->ofs @ literal compile (objval-do-!)
;

: create-field-accessor  ( addr count doer-cfa -- )
  ;; field word layout:
  ;;   typeid-field-word (signature)
  ;;   info-addr         (address of owner class info table)
  ;;   ofs               (offset in the instance)
  ;;   doer-cfa          (compile this doer if not 0)
  nrot (create) immediate
  typeid-field-word ,  ;; signature
  current-class-def ,  ;; info-addr
  current-class-def class->inst-size@ ,  ;; offset
  ,                    ;; doer-cfa
 does>  ( pfa )
  compiler:?comp
  curr-method-def-class " not in method definition" ?not-error
  dup fword->doer-cfa @execute-tail
;

: (finish-objval-field-accessor)  ( info-addr -- )
  ,   ;; compile bound class to field word
  typeid-objfield-word latest-pfa fword->typeid !  ;; patch word type
;

: bump-inst-size  ( delta -- )
  current-class-def class->inst-size@ +
  current-class-def class->inst-size!
;

: (self-@class)  ( -- info-addr )  (self@) inst->class@ ;
: (self-vmtidx@)  ( idx -- cfa ) (self-@class) class->vmt + @ ;

;; call inherited method for "self"
: (self-call-inh-vmtofs)  ( vmtofs -- ... )
  (self-@class) dup class->level@ 1- dup 0< " inherited without parent" ?error
  ( vmtofs info-addr level-1 )
  cells swap class->iht + @  ( vmtofs parent-info-addr )
  class->vmt + @execute-tail
;

: create-method  ( addr count -- )
  ;; method word layout:
  ;;   typeid-method-word (signature)
  ;;   info-addr          (address of owner class info table)
  ;;   ofs                (offset in VMT, in bytes)
  ;; fix method counter first, and use as offset in VMT later
  current-class-def class->vmt 1+!
  (create) immediate
  typeid-method-word ,  ;; signature
  current-class-def ,   ;; info-addr
  current-class-def class->vmt @ cells ,  ;; offset
 does>  ( pfa )
  compiler:?comp
  curr-method-def-class " not in method definition" ?not-error
  ;; no need to compile any checks, "self" should be valid here
  fword->ofs @ literal compile (self-vmtidx@) compile execute
;

;; save some state on rstack, and setup new
: start-class-compiling  ( -- )
  r>
  current @ >r  ;; save old CURRENT
  forth:(new-word-flags) @ dup >r  ;; save old flags
  compiler:word-redefine-warn-mode @ >r
  compiler:(wflag-protected) or forth:(new-word-flags) !
  current-class-def class->vocid@ current !
  compiler:(redefine-warn-parents) compiler:word-redefine-warn-mode !
  >r
;

;; restore some state from rstack
: finish-class-compiling  ( -- )
  r>
  r> compiler:word-redefine-warn-mode !
  r> forth:(new-word-flags) !
  r> current !
  >r
;

: create-@class-accessor ( -- )
  " @class" (create) immediate
  typeid-@class-word , ;; signature
  current-class-def ,  ;; info-addr
 does>  ( pfa )
  compiler:?comp
  curr-method-def-class " not in method definition" ?not-error
  drop compile (self-@class)
;

;; invoke method by name
: (invoke-str)  ( inst-addr addr count -- ... )
  rot ?inst-addr  dup >r inst->class@ (find-method)  ( cfa | inst-addr )
  \ (self@) r> (self!) >r execute r> (self!)
  r> swap (bind-execute)
;

: (obj-value-rebind)  ( info-addr pfa -- )
  dup fword->value @ " cannot rebind non-empty obj-value" ?error
  dup fword->info-addr @ " cannot rebind already bound obj-value" ?error
  fword->info-addr !
;

;; with class binding
: (class-invoke) ( inst-addr vmtofs info-addr -- )
  ?class-info rot ?inst-addr  ( vmtofs info-addr inst-addr )
  dup >r swap (isa?) " invalid object type" ?not-error  ( vmtofs | inst-addr )
  r@ inst->class@ class->vmt + @  ( cfa | inst-addr )
  \ (self@) r> (self!) >r execute r> (self!)
  r> swap (bind-execute)
;

;; with class binding
: (field-invoke) ( inst-addr instofs info-addr -- )
  ?class-info rot ?inst-addr  ( instofs info-addr inst-addr )
  dup >r swap (isa?) " invalid object type" ?not-error  ( instofs | inst-addr )
  r> + @
;

: (clx-invoke)  ( inst-addr ofs info-addr invoker-cfa )
  compiler:comp? if rot literal swap literal compile,
  else execute-tail
  endif
;


;; "TO" for obj-values
..: FORTH:(TO-EXTENDER-FOUND)  ( cfa FALSE -- cfa FALSE / TRUE )
  ?dup ifnot
    dup cfa->pfa fword->typeid @ typeid-objval-word = if
      cfa->pfa cell+
      compiler:comp? if literal compile (obj-value-!)
      else (obj-value-!)
      endif
      true
    else false
    endif
  endif
;.. (hidden)

;; "TO" for simple fields
..: FORTH:(TO-EXTENDER-FOUND)  ( cfa FALSE -- cfa FALSE / TRUE )
  ?dup ifnot
    dup cfa->pfa fword->typeid @ typeid-field-word = if
      compiler:?comp
      curr-method-def-class " not in method definition" ?not-error
      cfa->pfa (field-acc-!)
      true
    else false
    endif
  endif
;.. (hidden)

;; "TO" for obj-value fields
..: FORTH:(TO-EXTENDER-FOUND)  ( cfa FALSE -- cfa FALSE / TRUE )
  ?dup ifnot
    dup cfa->pfa fword->typeid @ typeid-objfield-word = if
      compiler:?comp
      curr-method-def-class " not in method definition" ?not-error
      cfa->pfa (objval-acc-!)
      true
    else false
    endif
  endif
;.. (hidden)


prev-defs  ;; at OOF
also oof-internal


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; vocabulary used in class definition
;;

simple-vocabulary oof-creatori
also-defs: oof-creatori

;; finish class definition
: end-class  ( -- )
  ?in-class-def
  ;; if we are at 0th level, create "self" word
  current-class-def class->level@ ifnot
    start-class-compiling
    create-@class-accessor
    finish-class-compiling
  endif
  previous  ;; remove "oof-creatori"
  0 to current-class-def
;

;; extend class definition
: extends:  ( -- )  \ parent-name
  ?in-class-def -find-required cfa->pfa  ( other-pfa )
  dup fword->typeid @ typeid-class-word <> " extend what?" ?error
  dup current-class-def class->def-pfa@ = " cannot extend self" ?error
  fword->info-addr @
  current-class-def class-inherit-from
;
alias-for extends: is extend:


;; create field access word (immediate)
;;   "name" -- get field value
: field:  ( -- )  \ name
  ?in-class-def
  parse-name dup " field name expected" ?not-error
  start-class-compiling
  ['] (field-acc-@) create-field-accessor
  cell bump-inst-size
  finish-class-compiling
;

;; this trashes PAD
;; create field access word (immediate)
;;   "name" -- use as obj-value
: obj-value:  ( info-addr -- )  \ name
  ?in-class-def
  dup if ?class-info endif
  parse-name dup " field name expected" ?not-error
  start-class-compiling rot >r
  ;; "field"
  ['] (objval-acc-run) create-field-accessor
  r> (finish-objval-field-accessor)
  cell bump-inst-size
  finish-class-compiling
;

;; create method call words (immediate)
;;   "name" -- execute method
: method:  ( -- )  \ name
  ?in-class-def
  parse-name dup " method name expected" ?not-error
  current-class-def class->vmt @ 1+ max-vmt-methods = " too many methods" ?error
  start-class-compiling
  create-method
  finish-class-compiling
;

previous prev-defs  ;; at OOF


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; vocabulary used in method definition
;;

simple-vocabulary oof-mtdef
also-defs: oof-mtdef
also oof-internal

: self  ( -- )
  compiler:?comp
  curr-method-def-class " not defining a method" ?not-error
  compile (self@)
; immediate

: @class  ( -- )
  compiler:?comp
  curr-method-def-class " not defining a method" ?not-error
  compile (self-@class)
; immediate

;; call inherited method
;; dynamic binding
: inherited  ( -- )
  compiler:?comp
  curr-method-def-class " not defining a method" ?not-error
  curr-method-def-class class->level@ " inherited without parent" ?not-error
  curr-method-def-vmtofs literal compile (self-call-inh-vmtofs)
; immediate

;; field address
: ->  ( -- addr ) \ name
  compiler:?comp
  curr-method-def-class " not defining a method" ?not-error
  -find-required cfa->pfa dup fword->typeid @ field-typeid? " address of what?" ?not-error
  (field-acc-addr)
; immediate

;; must be the last one in this vocabulary
: ;
  compiler:?comp
  curr-method-def-class " not defining a method" ?not-error
  false to curr-method-def-class
  previous previous [compile] forth:;
; immediate


previous prev-defs  ;; at OOF
also oof-internal


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; high-level class creation words
;;

;; this trashes PAD
: class:  ( -- )  \ name
  ;; class word layout:
  ;;   typeid-class-word  (signature)
  ;;   info-addr           (address of class info table)
  compiler:?exec
  current-class-def " previous class definition is not finished" ?error
  parse-name dup " class name expected" ?not-error
  ;; create class wordlist (without a hashtable)
  0 FALSE forth:(new-wordlist)
  create-class-info  ( addr count info-addr )
  nrot (create)  ( info-addr )
  here over class->def-pfa!
  typeid-class-word ,  ;; signature
  dup ,                ;; info-addr
  ;; we're defining this class
  to current-class-def
  ;; activate definition dictionary
  also oof-creatori
 does> ( pfa )
  fword->info-addr @
;

: method:  ( info-addr -- )  \ name
  compiler:?exec
  current-class-def " finish class definition first" ?error
  curr-method-def-class " already defining a method" ?error
  ?class-info
  ;; checks complete
  >r parse-name dup " method name expected" ?not-error
  ( addr count | info-addr )
  r@ class->vocid@ find-word-in-voc-and-parents " method not found" ?not-error
  ( cfa | info-addr )
  cfa->pfa dup fword->typeid @ typeid-method-word <> " trying to define non-method" ?error
  fword->ofs @
  dup to curr-method-def-vmtofs
  >r  ( | info-addr vmt-ofs )
  :noname swap  ( colon-id cfa | info-addr vmt-ofs )
  r> r@ class->vmt + ! r>  ( info-addr )
  to curr-method-def-class
  also oof-mtdef
  also curr-method-def-class class->vocid@ context !
;

: method-cfa:  ( info-addr -- cfa )  \ name
  ?class-info
  ;; checks complete
  >r parse-name dup " method name expected" ?not-error
  r> (find-method)
  compiler:comp? if cfaliteral endif
; immediate


: invoke-str  ( inst-addr addr count -- )
  compiler:comp? if compile (invoke-str)
  else (invoke-str)
  endif
; immediate

: invoke-dyn:  ( inst-addr -- )  \ name
  parse-name dup " method name expected" ?not-error
  compiler:comp? if strliteral endif
  [compile] invoke-str
; immediate

: invoke:  ( inst-addr -- )  \ classname name
  -find-required cfa->pfa dup fword->typeid @ typeid-class-word <> " class name expected" ?error
  fword->info-addr @ >r  ( inst-addr | info-addr )
  parse-name dup " method/field name expected" ?not-error
  r@ (find-field-word)  ( inst-addr pfa | info-addr )
  dup fword->ofs @ swap fword->typeid @  ( inst ofs typeid | info )
  r> swap ( inst ofs info typeid )
  dup typeid-method-word = if drop ['] (class-invoke)
  else field-typeid? " invoke what?" ?not-error ['] (field-invoke)
  endif
  (clx-invoke)
; immediate

\ bind-class classname oof:bind-field: <field>
: bind-field:  ( bind-class info-addr -- )  \ name
  compiler:?exec
  current-class-def " finish class definition first" ?error
  curr-method-def-class " already defining a method" ?error
  ?class-info  over ?class-info drop
  parse-name dup " field name expected" ?not-error ( bc ia addr count )
  rot (find-field-word)  ( bc pfa )
  dup fword->typeid @ typeid-objfield-word <> " trying to bind non-objval field" ?error
  fword->objval-info-addr dup @ " already bound" ?error
  !
; immediate

: size-of  ( info-addr -- instance-size )
  ?class-info class->inst-size@
;

: name-of  ( info-addr -- addr count )
  ?class-info class->name@
;

: parent-of  ( info-addr -- info-addr / 0 )
  ?class-info
  dup class->level@ dup if  ( info-addr level )
    1- cells swap class->iht + @
  else nip
  endif
;

;; init internal fields, zero other
: emplace  ( info-addr addr -- )
  >r dup size-of  ( info-addr inst-size | addr )
  r@ swap erase
  typeid-instance r@ !  ;; signatire
  r> cell+ !            ;; info-addr
;

: allot  ( info-addr -- addr )
  dup size-of n-allot  ( info-addr addr )
  dup >r emplace r>
;

: new  ( info-addr -- addr )
  dup size-of swap dup >r handle:new-alloc
  r> over emplace
;

;; check if instance/class-0 is a valid child of instance/class-1.
;; equal classes are valid children too.
: isa?  ( i/c-0 i/c-1 -- bool )  oof-internal:(isa?) ;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; obj-value
;;

: obj-value  ( info-addr -- )  \ name
  dup if ?class-info endif
  create immediate
  ;; info-addr is second to keep it in line with other words
  typeid-objval-word ,  ;; signature
  ,                     ;; info-addr
  0 ,                   ;; value
 does> ( pfa )  \ name
  >r parse-name dup " method name expected" ?not-error
  r@ fword->info-addr @ (find-field-word)  ( word-pfa | pfa )
  ;; check type
  dup fword->typeid @  ( word-pfa typeid | pfa )
  dup typeid-method-word <> over field-typeid? not and " trying to access something wrong" ?error
  ( word-pfa typeid | pfa )
  swap fword->ofs @ r> fword->value  ( typeid ofs value-addr )
  compiler:comp? if
    literal compile @  ;; get value
    literal            ;; vmtofs
    typeid-method-word = if compile (vmt-call) else compile (field-read) endif
  else
    @ swap rot typeid-method-word = if ['] (vmt-call) else ['] (field-read) endif
    execute-tail
  endif
;

\ classname oof:bind: <obj-value>
: bind:  ( info-addr -- )  \ name
  ?class-info -find-required ( ia cfa )
  cfa->pfa dup fword->typeid @ typeid-objval-word <> " trying to bind non-obj-value" ?error
  compiler:comp? if literal compile (obj-value-rebind)
  else (obj-value-rebind)
  endif
; immediate

: value-of:  ( -- value )  \ name
  -find-required ( ia cfa )
  cfa->pfa dup fword->typeid @ typeid-objval-word <> " trying to query non-obj-value" ?error
  fword->value compiler:comp? if literal compile @ else @ endif
; immediate

: class-of:  ( -- value )  \ name
  -find-required ( ia cfa )
  cfa->pfa dup fword->typeid @ typeid-objval-word <> " trying to query non-obj-value" ?error
  fword->info-addr compiler:comp? if literal compile @ else @ endif
; immediate


previous prev-defs  ;; at original

;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; and now for something completely different...
;; UrForth/C Forth Engine!
;; Copyright (C) 2023 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; THROW and CATCH
;; note that you cannot CATCH system errors (produced with "ERROR")
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


;; reset all exception frames
: (exc0!)  ( -- )  ... (exc-frame-ptr) 0! ; (hidden)

;; this should save all necessary data to the return stack
;; the format is: ( ...data restorecfa )
;;   restorecfa:  ( restoreflag -- )
;;   if "restoreflag" is 0, drop the data
: (catch-saver)  ( -- )  ... ; (hidden)


: CATCH  ( i * x xt -- j * x 0 | i * x n )
  ;; this is using return stack to hold previous catch frame
  ;; of course, this prevents Very Smart return stack manipulation, but idc (for now)
  ;; exception frame consists of:
  ;;   return-to-catch EIP (return stack TOS)
  ;;   sp (frame points here)
  ;;   prev_self
  ;;   prev_lp
  ;;   prev_lbp
  ;;   prev_a
  ;;   prev
  ;;   return-to-catch-caller EIP
  ;; create exception frame
  (exc-frame-ptr) @ >r
  ;; our local data
  (self@) >r             ;; self
  0 mtask:state-lp@ >r   ;; lp
  0 mtask:state-lbp@ >r  ;; lbp
  a>r                    ;; a
  ;; section to save various custom data
  0x8bad_c0de >r (catch-saver)
  ;; custom data section end
  sp@ >r                 ;; sp
  rp@ (exc-frame-ptr) !  ;; update exception frame pointer
  execute  ;; and execute
  ;; we will return here only if no exception was thrown
  rdrop begin r> 0x8bad_c0de <> while false swap execute repeat
  rdrop rdrop rdrop rdrop  ;; drop our saved local data
  r> (exc-frame-ptr) !     ;; restore previous exception frame
  0  ;; exception code (none)
;


: THROW  ( k * x n -- k * x | i * x n )
  ?dup if
    ;; check if we have exception frame set
    (exc-frame-ptr) @ ?dup ifnot
      ;; panic!
      (exc0!)
      compiler:exec!  ;; just in case
      \ fatal-error  ;; err-throw-without-catch (error)
      " throw without catch" fatal-error
      \ 1 n-bye ;; just in case
    endif
    ;; check if return stack is not exhausted
    ;; note that UrForth/C stacks grow from 0 to higher numbers!
    rp@ over u<= if
      ;; panic!
      (exc0!)
      compiler:exec!  ;; just in case
      \ err-throw-chain-corrupted fatal-error
      " throw chain corrupted" fatal-error
      \ 1 n-bye ;; just in case
    endif
    rp!  ;; restore return stack
    r> swap >r  ;; exchange return stack top and data stack top (save exception code, and pop sp to data stack)
    ;; blindly restore data stack (let's hope it is not too badly trashed)
    sp! drop  ;; drop the thing that was CFA
    r>  ;; restore exception code
    ;; restore custom data
    begin r> 0x8bad_c0de <> while true swap execute repeat
    ;; restore our custom data
    r>a
    r> 0 mtask:state-lbp!
    r> 0 mtask:state-lp!
    r> (self!)
    ;; restore previous exception frame
    r> (exc-frame-ptr) !
    ;; now EXIT will return to CATCH caller
  endif
; compiler:(wflag-noreturn) compiler:or-wflags

;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; and now for something completely different...
;; UrForth/C Forth Engine!
;; Copyright (C) 2023 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; simple line-oriented editor
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(*
  usage:
    LINORE:RESET
      reset input line (make it empty)

    LINORE:EDIT-LINE
      edit line without redrawing the prompt.
      result is in LINORE:ACCEPT-RESULT.
      default is:
        1 for enter, -1 on esc/^C/^D

    LINORE:LINE  ( -- addr count )
      get edited line.

    " abc" LINORE:LINE!
      set line to edit.
*)

vocabulary linore

;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; helper public API
;;
also-defs: linore

;; it will be filled later
simple-vocabulary osc-actions

-3 constant result-^D
-2 constant result-^C
-1 constant result-esc
 1 constant result-enter

;; if set, LINORE will switch to raw mode on entering
;; edit loop, and then switch back.
true value tty-set/reset

;; set this to non-zero in key handler to exit edit loop.
;; can be checked after editing.
0 value accept-result

;; start x position for line drawing.
0 value start-x
;; total line width.
80 value draw-width
;; when cursor is out of visible line part, make at
;; least this number of chars visible before/after the cursor.
;; do not set to zero or negative.
10 value visible-amount

;; reset input buffer (i.e. make it empty).
;; defined here, because we need it in the code.
defer reset


;; use this from key processors to exit with this code.
: finish-it!  ( code -- )  to accept-result ;

;; called for normal keys (non-OSC).
;; drop `ch` and return TRUE if you handled it.
;; you can replace char code here.
;; negative `ch` means that this is "ESC something" sequence (not OSC).
: key-processor  ( ch FALSE -- ch FALSE / TRUE )  ... ;

;; called for OSC keys. string is OSC without "\e[".
;; drop string and return TRUE if you handled it.
;; you can replace string here.
: osc-key-processor  ( addr count FALSE -- addr count FALSE / TRUE )  ... ;

;; called before redrawing the input line.
;; return TRUE to prevent redraw.
: on-before-draw  ( FALSE -- FALSE / TRUE )  ... ;

;; called after redrawing the input line.
: on-after-draw  ( -- )  ... ;

;; this is called after screen switching
: on-start-editing  ( -- ) ... ;

;; this is called before screen switching
: on-end-editing  ( -- ) ... ;

prev-defs


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; LINORE internals
;;

also-defs: linore
vocabulary linore-internal
also-defs: linore-internal

;; input buffer
(user-tib) value buf
(user-tib-size) 1- value buf-size
0 value buf-len
0 value draw-ofs
0 value cur-ofs

;; if set, first input will erase the line
0 value first-input


;; OSC buffer (without first 2 chars)
32 constant keybuf-size
0 value keybuf-len
create keybuf keybuf-size allot create;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; working with cursor position
;;

: norm-cursor-pos  ( -- )  cur-ofs buf-len min 0 max to cur-ofs ;

: curvis-at-left  ( -- )
  cur-ofs visible-amount - 0 max to draw-ofs
;

: curvis-at-right  ( -- )
  \ " goo!" error
  cur-ofs draw-width - visible-amount + 0 max
  buf-len visible-amount - 0 max
  min to draw-ofs
;

: make-cursor-visible  ( -- )
  norm-cursor-pos
  case
    cur-ofs draw-ofs < if-of curvis-at-left endof
    cur-ofs draw-ofs - draw-width >= if-of curvis-at-right endof
  otherwise endcase
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; drawing the edit line
;;

: .osc-1  ( n cmd-char -- )
  " \e[" tty:raw-type
  swap <# #s #> tty:raw-type
  tty:raw-emit
;

: goto-x  ( x -- )
  1+ [char] g .osc-1
;

: to-start-x  ( -- )
  start-x goto-x
;

: higlight-first-input  ( -- )
  first-input if " \e[0;4m" else " \e[0m" endif
  tty:raw-type
;

: draw-line  ( -- )
  higlight-first-input
  buf draw-ofs +  buf-len draw-ofs - draw-width min  tty:raw-type
  " \e[0m\e[K" tty:raw-type
;

: pos-cursor  ( -- )
  start-x cur-ofs draw-ofs - + goto-x
;

: draw  ( -- )
  make-cursor-visible
  to-start-x draw-line pos-cursor
  tty:raw-flush
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; line editing helper words
;;

;; -1 when out of bounds
: CUR-C@  ( -- ch )
  cur-ofs 0 buf-len within if buf cur-ofs + c@
  else -1 endif
;

: (go-left-right)  ( mdir cmp-cfa -- mdir )
  >r BEGIN
    cur-c@ 0>=
  while
    cur-c@ bl r@ execute
  while
    dup +to cur-ofs
  repeat rdrop
;

: (go-word-left-right)  ( mdir -- )
  norm-cursor-pos
  dup 0< if -1-to cur-ofs endif
  cur-c@ 0>= if
    cur-c@ bl <= if ['] <= else ['] > endif
    (go-left-right)
    0< if +1-to cur-ofs endif
  else 0< if +1-to cur-ofs endif
  endif
;

prev-defs   ;; in LINORE again


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; line editing words
;;

simple-vocabulary linore-edit-api
also linore-internal
also-defs: linore-edit-api

: reset-first-input  ( -- )  false to first-input ;

: left  ( -- )  reset-first-input -1-to cur-ofs ;
: right  ( -- )  reset-first-input +1-to cur-ofs ;
: home  ( -- )  reset-first-input 0-to cur-ofs ;
: end  ( -- )  reset-first-input buf-len to cur-ofs ;

: word-left  ( -- )  reset-first-input -1 (go-word-left-right) ;
: word-right  ( -- )  reset-first-input +1 (go-word-left-right) ;

: delete  ( -- )
  reset-first-input
  norm-cursor-pos
  cur-ofs buf-len < if
    buf cur-ofs + dup 1+ swap
    buf-len cur-ofs - move
    -1-to buf-len
  endif
;

: backspace  ( -- )
  reset-first-input
  norm-cursor-pos
  cur-ofs if left delete endif
;

: ins-char  ( ch -- )
  first-input if linore:reset reset-first-input endif
  norm-cursor-pos
  buf cur-ofs +  dup 1+
  buf-len cur-ofs - move
  buf cur-ofs + c!
  +1-to buf-len
  right
;

prev-defs   ;; in LINORE-INTERNAL again


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; input handling
;;

;; [0..31]
create (ctrl-keys-handlers) 32 cells allot create;
;; default is "do nothing"
(ctrl-keys-handlers) 32 cells erase

: set-ctrl-handler  ( cfa code -- )
  dup 0 32 within " invalid ctrl code" ?not-error
  cells (ctrl-keys-handlers) + !
;

:noname  ( -- )  result-^C linore:finish-it! ; 3 set-ctrl-handler  ;; ^C
:noname  ( -- )  result-^D linore:finish-it! ; 4 set-ctrl-handler  ;; ^D

:noname  ( -- )  result-esc linore:finish-it! ; 27 set-ctrl-handler  ;; ESC

:noname  ( -- )  result-enter linore:finish-it! ;
dup 10 set-ctrl-handler
dup 13 set-ctrl-handler
drop

' linore-edit-api:backspace 8 set-ctrl-handler  ;; ^H
' linore:reset 25 set-ctrl-handler  ;; ^Y


;; collect OSC (without first 2 chars)
: (collect-osc)  ( -- )
  0-to keybuf-len 0
  begin
    drop tty:raw-readch dup bl >
  while
    keybuf-len keybuf-size = if break endif
    dup keybuf keybuf-len + c!  +1-to keybuf-len
    dup [char] A >=
  until drop
;

: (edit-process-osc)  ( -- )
  (collect-osc)
  keybuf keybuf-len false linore:osc-key-processor
  ifnot
    vocid: linore:osc-actions find-word-in-voc
    if execute endif
  endif
;

: (edit-process-other)  ( ch -- )
  false linore:key-processor ifnot
    dup 0 32 within if
      cells (ctrl-keys-handlers) + @ ?dup if execute endif
    else
      dup 127 = if drop linore-edit-api:backspace
      else dup bl >= if linore-edit-api:ins-char else drop endif
      endif
    endif
  endif
;

: (edit-process-esc)  ( ch -- )
  drop ;; it is always 27
  tty:raw-readch dup [char] [ = if  ;; OSC start
    drop (edit-process-osc)
  else  ;; esc+something; negate keycode
    dup 27 <> if ( not esc-esc ) negate endif
    (edit-process-other)
  endif
;

prev-defs   ;; in LINORE again



;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; default OSC handlers
;;

also-defs: osc-actions

: D  linore-edit-api:left ;
: C  linore-edit-api:right ;
: 7~  linore-edit-api:home ;
: 8~  linore-edit-api:end ;
: 3~  linore-edit-api:delete ;
: 1;5D  linore-edit-api:word-left ;
: 1;5C  linore-edit-api:word-right ;

prev-defs   ;; in LINORE again


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; public API
;;

also linore-internal
also linore-edit-api

;; set "RESET" API
:noname  ( -- )  0-to buf-len  0-to draw-ofs  0-to cur-ofs ;
to linore:reset

: line  ( -- addr count )  buf buf-len ;

: line!  ( addr count -- )
  buf-size min dup to buf-len buf swap move
  buf-len to cur-ofs
;

: edit-line  ( -- )
  tty-set/reset if tty:set-raw " fuck!" ?not-error endif
  0 to accept-result  true to first-input
  on-start-editing
  begin
    false on-before-draw ifnot draw endif on-after-draw
    tty:raw-readch dup 0>=
  while
    dup 27 = if (edit-process-esc)
    else (edit-process-other)
    endif
  accept-result until
  on-end-editing
  tty-set/reset if tty:set-cooked drop endif
;


;; we're done
previous previous prev-defs

;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; and now for something completely different...
;; UrForth/C Forth Engine!
;; Copyright (C) 2023 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
$include-once <bootstrap>

;; ////////////////////////////////////////////////////////////////////////// //
;; big and slow word optimiser
;; totally doesn't worth it, but was fun to write
$IF 0
<public-words>
<protected-words>
$include-once <?sinopt-whole-word>
\ .( optimising system words...\n) (sinopt):optimise-system
\ .( activating optimiser...\n) (sinopt):activate
<public-words>
<unprotected-words>
$ENDIF


;; ////////////////////////////////////////////////////////////////////////// //
;; simple superinstruction optimiser
;; fast, and sometimes is able to do very simple optimisations
$IF 1
<public-words>
<protected-words>
$include-once <../sinopt-peephole>
<public-words>
<unprotected-words>
$ENDIF


;; ////////////////////////////////////////////////////////////////////////// //
$include-once <../stdlib>


;; ////////////////////////////////////////////////////////////////////////// //
$IF $URASM-NEW
  $include-once <?../urasm>
$ENDIF

$IF $DEBUGGER
  $include-once <debugger.f>
$ENDIF

;;$IF $STANDALONE OR $URASM-NEW
$IF $STANDALONE AND NOT $SKIP-REPL
  $include-once <?../repl.f>
$ENDIF

;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; and now for something completely different...
;; UrForth/C Forth Engine!
;; Copyright (C) 2023 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


\ vocabulary ($cc-eval)
also ($cc-eval) definitions

0 value $if-forth-count


: parse-next-word  ( -- )
  begin parse-skip-comments bl parse
  NOT-WHILE refill " unexpected end of file" ?not-error
  repeat
  to curr-word-len to curr-word-addr
  \ ." |" curr-word xtype ." |\n"
;

: $skip-conds-forth  ( toelse -- )
  0 >r  ( toelse | level )
  begin
    parse-next-word
    false  ( toelse done? | level )
    curr-word " [IF]" string:=ci if drop r> 1+ >r false endif
    curr-word " [ENDIF]" string:=ci
    curr-word " [THEN]" string:=ci or
    if drop
      ;; in nested ifs, look only for $ENDIF
      r@ if r> 1- >r false
      else
        ;; it doesn't matter which part we're skipping, it ends here anyway
        true
      endif
    endif
    curr-word " [ELSE]" string:=ci if drop
      ;; if we're skipping "true" part, go on
      dup if
        +1-to $if-forth-count
        r@ 0=  \ false
      else
        ;; we're skipping "false" part, there should be no else
        r@ " unexpected [ELSE]" ?not-error
        false
      endif
    endif
  until
  drop  ;; drop `toelse`
  r> 0<> " oops?" ?error
;

: $process-cond-forth  ( cond -- )
  if +1-to $if-forth-count
  else true $skip-conds-forth endif
;

previous definitions


: [IF]
  ($cc-eval):$process-cond-forth
; immediate

: [ELSE]
  ($cc-eval):$if-forth-count " unexpected [ELSE]" ?not-error
  false ($cc-eval):$skip-conds-forth
; immediate

: [ENDIF]
  ($cc-eval):$if-forth-count " unexpected [ENDIF]" ?not-error
  -1-to ($cc-eval):$if-forth-count
; immediate

: [THEN]
  ($cc-eval):$if-forth-count " unexpected [THEN]" ?not-error
  -1-to ($cc-eval):$if-forth-count
; immediate

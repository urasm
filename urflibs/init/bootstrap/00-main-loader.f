;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; and now for something completely different...
;; UrForth/C Forth Engine!
;; Copyright (C) 2023 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; define some standard words
;; note that UFO starts without colon, conditionals and loops!
;;
get-msecs
$include-once <01-colon-semicolon.f>
$include-once <10-mem-utils.f>
$include-once <15-defer_value.f>
$include-once <20-base-creatori.f>
$include-once <25-scolon.f>
$include-once <30-ifthen.f>
$include-once <35-num-print.f>
$include-once <40-voc-creatori.f>
$include-once <50-to.f>
$include-once <70-replace.f>
\ $include-once <80-locals.f>
$include-once <90-cond-comp.f>
$include-once <92-cond-if.f>
<public-words>
<unprotected-words>
$IF $UFO-DEBUG-STARTUP-TIMES
  get-msecs swap - .( urforth bootstrap time: ) . .( msecs\n)
$ELSE
  drop
$ENDIF


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
$IF $UFO-BOOTSTRAP-TESTS
.( >>> CASE TEST <<<\n)
: BBB
  CASE
     8 OF ." This is 8" CR ENDOF
    12 OF ." This is 12" CR ENDOF
    99 OF ." This is 99" CR ENDOF
    ( note that you *HAVE* to DUP it )
    ( ENDCASE will call DROP )
    ( so if you want to leave something on the stack, )
    ( make sure that TOS is garbage to be dropped )
    \ DUP
    OTHERWISE
    ." The number is: " . CR
  ENDCASE
;
\ DEBUG:DECOMPILE BBB

42
1 BBB
12 BBB
8 BBB
666 BBB
. CR
.( >>> CASE TEST COMPLETE <<<\n)

.( >>> FOR TEST <<<\n)
: testfor
  10 FOR
    I 2 = IF CONTINUE ENDIF
    I . cr
    I 5 = IF BREAK ENDIF
  ENDFOR
;
DEBUG:DECOMPILE testfor
testfor
.( >>> FOR TEST COMPLETE <<<\n)

;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
.( testing CELLS\n)
: aa  3 cells " 3 cells: " TYPE . cr ;
\ DEBUG:DECOMPILE aa
aa

: test02 ( -- )
  ENDCR
  0 10 DO I . CR -1 +LOOP CR
;
\ DEBUG:DECOMPILE test02
test02

: test01 ( -- )
  ENDCR
  11 1 DO I . CR LOOP CR
  -11 -1 DO I . CR -1 +LOOP CR
  CR ." ---" CR
  0 -10 DO I . CR LOOP CR
  CR ." ---" CR
;
\ DEBUG:DECOMPILE test01
test01

: e2  ." E2!\n" 69 ;
: e1  ." E1!\n" ['] e2 execute ." after EXEC! num: " . cr ;
: e0  ." E0!\n" ['] e2 execute-tail ." ERROR: after EXEC! num: " . cr ;
\ DEBUG:DECOMPILE e1
.( === E1 ===\n)
e1
.( === E0 ===\n)
e0 .( NUM: ) . cr
.( === EXEC NATIVE ===\n)
60 9 ' + EXECUTE . cr
: e3  60 9 ['] + EXECUTE-TAIL ." ERROR: after EXEC! num: " . cr ;
.( === E3 ===\n)
e3 .( NUM: ) . cr

$ENDIF

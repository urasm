;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; and now for something completely different...
;; UrForth/C Forth Engine!
;; Copyright (C) 2023 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DEFER, VALUE
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


: VALUE  ( value -- )  compiler:(cfaidx-do-value) compiler:(mk-const-var) ;
: VALUE?  ( cfa -- bool )  @ compiler:(cfaidx-do-value) = ;
: VALUE@  ( cfa -- value )  dup value? " non-value CFA" ?not-error  cfa->pfa @ ;
: VALUE!  ( value cfa -- )  dup value? " non-value CFA" ?not-error  cfa->pfa ! ;

: DEFER  ( -- )
  [ ' forth:(litcfa) compile, ' noop , ]
  compiler:(cfaidx-do-defer) compiler:(mk-const-var) ;

: DEFER?  ( cfa -- bool )  @ compiler:(cfaidx-do-defer) = ;
: DEFER@  ( cfa -- value )  dup defer? " non-defer CFA" ?not-error  cfa->pfa @ ;
: DEFER!  ( value cfa -- )  dup defer? " non-defer CFA" ?not-error  cfa->pfa ! ;


: (to-value)  ( n cfa -- )  cfa->pfa ! ; (hidden)
: (+to-value)  ( n cfa -- )  cfa->pfa +! ; (hidden)
\ alias-for forth:(to-defer) is (to-value)

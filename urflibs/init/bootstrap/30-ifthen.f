;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; and now for something completely different...
;; UrForth/C Forth Engine!
;; Copyright (C) 2023 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; IF/ELSE/ENDIF
;;
also compiler definitions

  1 constant PAIR-BEGIN
  2 constant PAIR-IF
  3 constant PAIR-IFELSE
  4 constant PAIR-DO
  5 constant PAIR-CASE
  6 constant PAIR-OF
  7 constant PAIR-OTHER
  8 constant PAIR-WHILE
  9 constant PAIR-FOR
 10 constant PAIR-QDO
666 constant PAIR-CBLOCK

0 variable (chain-break)
0 variable (chain-cont)
0 variable (loop-pair)


;; WHILE
;; ( saved-break saved-cont saved-lastpair begin-id branchcfa -- saved-break saved-cont saved-lastpair begin-id )
;; all failed whiles will terminate the loop
: (COMPILE-WHILE)
  ?comp swap pair-begin ?pairs
  compile, (chain-break) @ (chain-j>) (chain-break) !
  pair-begin
;

;; AGAIN
;; ( saved-break saved-cont saved-lastpair begin-id branchcfa -- )
;; ( -- saved-break saved-cont saved-lastpair begin-id )
: (COMPILE-UNTIL)
  ?comp swap pair-begin ?pairs
  compile, (chain-cont) @ (<j-resolve)
  (chain-break) @ (resolve-j>)
  (loop-pair) !  (chain-cont) !  (chain-break) !
;

: (COUNTED-LOOP?)  ( n -- bool )
  dup pair-do =  over pair-qdo = or  swap pair-for = or
;


previous definitions also compiler

: (IF-COMMON)  ( branch-cfa -- )
  ?comp compile, (mark-j>)
  pair-if
;

: IF    ( -- addr id )  ['] forth:(0branch) (if-common) ; immediate
: IFNOT ( -- addr id )  ['] forth:(tbranch) (if-common) ; immediate
: -IF   ( -- addr id )  ['] forth:(+0branch) (if-common) ; immediate  -- if negative
: +IF   ( -- addr id )  ['] forth:(-0branch) (if-common) ; immediate  -- if positive (but not zero)
: -0IF  ( -- addr id )  ['] forth:(+branch) (if-common) ; immediate  -- if negative or 0
: +0IF  ( -- addr id )  ['] forth:(-branch) (if-common) ; immediate  -- if positive or 0

: ENDIF
  ?comp pair-if pair-ifelse ?2pairs
  (resolve-j>)
; immediate

: THEN
  [compile] endif
; immediate

: ELSE
  ?comp pair-if ?pairs
  compile forth:(branch) (mark-j>)
  swap (resolve-j>)
  pair-ifelse
; immediate


;; ( -- saved-break saved-cont saved-lastpair begin-jmark begin-id )
: BEGIN
  ?comp
  (chain-break) @  (chain-cont) @  (loop-pair) @
  (chain-break) 0!  (<j-mark) (chain-cont) !
  pair-begin dup (loop-pair) !
; immediate

;; all failed whiles will terminate the loop
: WHILE      ['] forth:(0branch) (compile-while) ; immediate
: NOT-WHILE  ['] forth:(tbranch) (compile-while) ; immediate
: -WHILE     ['] forth:(+0branch) (compile-while) ; immediate
: +WHILE     ['] forth:(-0branch) (compile-while) ; immediate
: -0WHILE    ['] forth:(+branch) (compile-while) ; immediate
: +0WHILE    ['] forth:(-branch) (compile-while) ; immediate

: AGAIN  ['] forth:(branch) (compile-until) ; immediate
: REPEAT [compile] again ;  immediate

: UNTIL      ['] forth:(0branch) (compile-until) ; immediate
: NOT-UNTIL  ['] forth:(tbranch) (compile-until) ; immediate
: -UNTIL     ['] forth:(+0branch) (compile-until) ; immediate
: +UNTIL     ['] forth:(-0branch) (compile-until) ; immediate
: -0UNTIL    ['] forth:(+branch) (compile-until) ; immediate
: +0UNTIL    ['] forth:(-branch) (compile-until) ; immediate


: (DO)  ( limit start -- | limit counter )
  ;; index = 0x8000_0000 - to + from
  ;; limit = 0x8000_0000 - to
  r> nrot   ( ret to from )
  0x8000_0000 rot -   ( ret from 0x8000_0000-to )
  dup >r + >r >r
; (hidden)

: (?DO)  ( limit start -- | limit counter )
  2dup <> if  ;; do it
    ;; index = 0x8000_0000 - to + from
    ;; limit = 0x8000_0000 - to
    r> nrot   ( ret to from )
    0x8000_0000 rot -   ( ret from 0x8000_0000-to )
    dup >r + >r
    4+  ( skip branch address )
  else  ;; skip it
    2drop r> compiler:(branch-addr@)
  endif
  >r
; (hidden)  compiler:(warg-branch) compiler:set-warg

;; exit when counter crosses a limit
: (+LOOP)  ( add -- | limit index )
  r> swap
  r@ +  ( ret add+index )
  r@ over xor 0< if  ( ret add+index )
    ;; break
    drop rdrop rdrop 4+
  else  ( ret add+index )
    ;; continue
    rdrop >r compiler:(branch-addr@)
  endif
  >r
; (hidden)  compiler:(warg-branch) compiler:set-warg
\ debug:decompile forth:(+loop)

: (I)  ( | limit index -- real-index | limit index )
  1 rpick 2 rpick -
;

: (J)  ( | limit0 index0 limit1 index1 -- real-index0 | limit0 index0 limit1 index1 )
  3 rpick 4 rpick -
;

: (UNLOOP)
  r> rdrop rdrop >r
;

: DO
  ?comp
  compile forth:(do)
  (chain-break) @  (chain-cont) @  (loop-pair) @
  (chain-break) 0!  (<j-mark) (chain-cont) !
  pair-do dup (loop-pair) !
; immediate

: ?DO
  ?comp
  compile forth:(?do)
  (chain-break) @  (chain-cont) @  (loop-pair) @
  (mark-j>) (chain-break) !  (<j-mark) (chain-cont) !
  pair-qdo dup (loop-pair) !
; immediate

: (LOOP-FINISH)  ( loop-id -- )
  drop compile forth:(+loop)
  (chain-cont) @ (<j-resolve)
  (chain-break) @ (resolve-j>)
  (loop-pair) !  (chain-cont) !  (chain-break) !
; (hidden)

: LOOP
  ?comp dup pair-do pair-qdo ?2pairs
  1 literal forth:(loop-finish)
; immediate

: +LOOP
  ?comp dup pair-do pair-qdo ?2pairs
  forth:(loop-finish)
; immediate

: I
  ?comp (loop-pair) @ (counted-loop?) " 'I' out of a loop" ?not-error
  compile (i)
; immediate

: J
  ?comp (loop-pair) @ (counted-loop?) " 'J' out of a loop" ?not-error
  compile (j)
; immediate


: (FOR)  ( limit -- | limit counter )
  dup 0> if  ;; do it
    ;; index = 0x8000_0000 - to + from
    ;; limit = 0x8000_0000 - to
    0 r> nrot   ( ret to from )
    0x8000_0000 rot -   ( ret from 0x8000_0000-to )
    dup >r + >r
    4+  ( skip branch address )
  else  ;; skip it
    drop r> compiler:(branch-addr@)
  endif
  >r
; (hidden)  compiler:(warg-branch) compiler:set-warg

: (ENDFOR)  ( -- | limit counter )
  r>
  r@ 1+  ( ret add+index )
  r@ over xor 0< if  ( ret add+index )
    ;; break
    drop rdrop rdrop 4+
  else  ( ret add+index )
    ;; continue
    rdrop >r compiler:(branch-addr@)
  endif
  >r
; (hidden)  compiler:(warg-branch) compiler:set-warg

: FOR
  ?comp
  compile forth:(for)
  (chain-break) @  (chain-cont) @  (loop-pair) @
  (mark-j>) (chain-break) !   (chain-cont) 0!
  (<j-mark) pair-for dup (loop-pair) !
; immediate

: ENDFOR
  ?comp pair-for ?pairs
  ;; continue points here
  (chain-cont) @ (resolve-j>)
  compile (endfor) (<j-resolve)
  ;; break points here
  (chain-break) @ (resolve-j>)
  (loop-pair) !  (chain-cont) !  (chain-break) !
; immediate


: CASE
  ?comp 0 pair-case
; immediate

: ENDCASE
  ?comp
  dup pair-other = if
    drop  ;; pair-id
  else pair-case ?pairs
    compile drop
  endif
  ;; jump out of the cases
  compiler:(resolve-j>)
; immediate

definitions

: (COMPILE-IF-OF)  ( brcfa -- )
  ?comp swap pair-case ?pairs
  compile, compiler:(mark-j>)
  pair-of
; (hidden)

: (COMPILE-OF)  ( cmpcfa -- )
  ?comp swap pair-case ?pairs
  ?dup if compile, endif
  compile forth:(case-branch) compiler:(mark-j>)
  pair-of
; (hidden)

: (of=)  over = ; (hidden)
: (of<>)  over <> ; (hidden)
: (of<)  over swap < ; (hidden)
: (of<=)  over swap <= ; (hidden)
: (of>)  over swap > ; (hidden)
: (of>=)  over swap >= ; (hidden)
: (of-u<)  over swap < ; (hidden)
: (of-u<=)  over swap <= ; (hidden)
: (of-u>)  over swap > ; (hidden)
: (of-u>=)  over swap >= ; (hidden)
: (of-and)  over and ; (hidden)
: (of-~and)  over swap bitnot and ; (hidden)
: (of-within)  >r >r dup r> r> within ; (hidden)
: (of-uwithin)  >r >r dup r> r> uwithin ; (hidden)
: (of-bounds)  >r >r dup r> r> bounds? ; (hidden)

previous definitions also compiler

;; "?OF" consumes the boolean which is already at TOS.
;; it can be used like this: "dup 3 = ?OF".
;; DO NOT FORGET "DUP"! case value should be on the stack.
: ?OF        0 compiler:(compile-of) ; immediate
: IF-OF      ['] forth:(0branch) compiler:(compile-if-of) ; immediate
: IFNOT-OF   ['] forth:(tbranch) compiler:(compile-if-of) ; immediate
: OF         ['] compiler:(of=) compiler:(compile-of) ; immediate
: NOT-OF     ['] compiler:(of<>) compiler:(compile-of) ; immediate
: <OF        ['] compiler:(of<) compiler:(compile-of) ; immediate
: <=OF       ['] compiler:(of<=) compiler:(compile-of) ; immediate
: >OF        ['] compiler:(of>) compiler:(compile-of) ; immediate
: >=OF       ['] compiler:(of>=) compiler:(compile-of) ; immediate
: U<OF       ['] compiler:(of-u<) compiler:(compile-of) ; immediate
: U<=OF      ['] compiler:(of-u<=) compiler:(compile-of) ; immediate
: U>OF       ['] compiler:(of-u>) compiler:(compile-of) ; immediate
: U>=OF      ['] compiler:(of-u>=) compiler:(compile-of) ; immediate
: &OF        ['] compiler:(of-and) compiler:(compile-of) ; immediate
: AND-OF     ['] compiler:(of-and) compiler:(compile-of) ; immediate
: ~AND-OF    ['] compiler:(of-~and) compiler:(compile-of) ; immediate
: WITHIN-OF  ['] compiler:(of-within) compiler:(compile-of) ; immediate
: UWITHIN-OF ['] compiler:(of-uwithin) compiler:(compile-of) ; immediate
: BOUNDS-OF  ['] compiler:(of-bounds) compiler:(compile-of) ; immediate

: ENDOF
  ?comp pair-of ?pairs
  ;; jump out of the case
  swap compile forth:(branch) compiler:(chain-j>)
  ;; fix previous of jump
  swap compiler:(resolve-j>)
  pair-case
; immediate

: OTHERWISE
  ?comp pair-case ?pairs
  pair-other
; immediate


: BREAK
  ?comp
  (loop-pair) @ pair-begin = if
    compile forth:(branch) (chain-break) @ (chain-j>) (chain-break) !
  else (loop-pair) @ (counted-loop?) if
    compile forth:(unloop)
    compile forth:(branch) (chain-break) @ (chain-j>) (chain-break) !
  else " 'BREAK' out of loop" error
  endif endif
; immediate

: CONTINUE
  ?comp
  (loop-pair) @ pair-begin = if
    compile forth:(branch) (chain-cont) @ (<j-resolve)
  else (loop-pair) @ pair-for = if
    ;; in "FOR", "CONTINUE" is a forward jump to "ENDFOR"
    compile forth:(branch) (chain-cont) @ (chain-j>) (chain-cont) !
  else (loop-pair) @ (counted-loop?) " 'CONTINUE' out of loop" ?not-error
    1 literal compile forth:(+loop) (chain-cont) @ (<j-resolve)
    compile forth:(branch) (chain-break) @ (chain-j>) (chain-break) !
  endif endif
; immediate


previous

;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; and now for something completely different...
;; UrForth/C Forth Engine!
;; Copyright (C) 2023 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; word utils and creation helper
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


also compiler definitions


: FORTH-WORD?  ( cfa -- bool )
  @ compiler:(cfaidx-do-forth) =
;

: VARIABLE-WORD?  ( cfa -- bool )
  @ compiler:(cfaidx-do-var) =
;

: CONSTANT-WORD?  ( cfa -- bool )
  @ compiler:(cfaidx-do-const) =
;

: IMMEDIATE-WORD?  ( cfa -- bool )
  cfa->nfa @ compiler:(wflag-immediate) and 0<>
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; compiler helpers
;;

;; usage:
;;  compile (0branch)
;;  (mark-j>)
;;  ...
;;  (resolve-j>)
;;
;;  (<j-mark)
;;  ...
;;  compile (branch)
;;  (<j-resolve)

;; moved to C kernel, to be used before this file
;; (BRANCH-ADDR!)  ( destaddr addr -- )
;; write "branch to destaddr" address to addr
\ : (BRANCH-ADDR!)  ( destaddr addr -- )  ! ;

;; moved to C kernel, to be used before this file
;; (BRANCH-ADDR@)  ( addr -- dest )
;; read branch address
\ : (BRANCH-ADDR@)  ( addr -- dest )  @ ;


;; (<J-MARK)  ( -- addr )
;; return addr suitable for "(<J-RESOLVE)"
: (<J-MARK)  ( -- addr )  here (jump-here-marked) ;

;; (<J-CHAIN)  ( addr -- addr )
;; use after "(<J-MARK)" to reserve jump and append it to jump chain
: (<J-CHAIN)  ( addr -- addr )  dup here 0 , (branch-addr!) ;

;; (<J-RESOLVE)  ( addr -- )
;; patch "forward jump" address to HERE
;; addr is the result of "(<J-MARK)"
: (<J-RESOLVE)  ( addr -- )  here 0 , (branch-addr!) ;


;; (MARK-J>)  ( -- addr )
;; reserve room for branch address, return addr suitable for "(RESOLVE-J>)"
: (MARK-J>)  ( -- addr )  here 0 , ;

;; (CHAIN-J>)  ( addr -- addr )
;; use after "(MARK-J>)" to reserve jump and append it to jump chain
: (CHAIN-J>)  ( addr -- addr )  here swap , ;

;; (RESOLVE-J>)  ( addr -- )
;; compile "forward jump" (possibly chain) from address to HERE
;; addr is the result of "(MARK-J>)"
: (RESOLVE-J>)  ( addr -- )  \ HERE SWAP (BRANCH-ADDR!) ; (HIDDEN)
  [ (<j-mark) ]  ;; begin
    ?dup
  forth:(0branch) [ (mark-j>) ]  ;; while
    dup @   ( addr prevaddr )
    here rot (branch-addr!)  ( prevaddr )
  forth:(branch) [ here 4+ ( cell+ ) swap (branch-addr!)  (<j-resolve) ]  ;; repeat
;
\ debug:decompile (resolve-j>)


: ?PAIRS  ( a b -- )  <> " unbalanced constructs" ?error ;
: ?2PAIRS  ( a b c -- )  >r over <>  swap r> <> and " unbalanced constructs" ?error ;
: ?3PAIRS  ( a b c d -- )
  >r rot r> swap >r  ( b c d | a )
  r@ <> ( b c flag )
  swap r@ <> and  ( b flag )
  swap r> <> and " unbalanced constructs" ?error
;

previous definitions \ also compiler


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; higher-level compiler helpers
;;

also compiler definitions  ;; compiler

;; compiles CFA to be compiled
: (COMPILE-CFA-LITERAL)  ( cfa -- )  cfaliteral  ['] compile, compile, ;

previous definitions


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; compilation helpers, in FORTH vocabulary
;;
: COMPILE  ( -- )  \ word
  compiler:?comp -find-required compiler:(compile-cfa-literal)
; immediate

: [CHAR]  ( -- ch )
  parse-name 1 <> " [CHAR] expects a char" ?error
  c@ literal
; immediate


: RECURSE       latest-cfa compile, ; immediate
: RECURSE-TAIL  compile forth:(branch) latest-pfa here 0 , compiler:(branch-addr!) ; immediate


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; word creation
;;
: CONSTANT  ( value -- )  compiler:(cfaidx-do-const) compiler:(mk-const-var) ;
: VARIABLE  ( value -- )  compiler:(cfaidx-do-var) compiler:(mk-const-var) ;

: (CREATE)  ( addr count -- )
  compiler:(create-header)
  compiler:(cfaidx-do-create) compiler:cfa,
  compiler:reset-smudge
;

: CREATE  ( -- )  \ name
  parse-name (create)
;

: CREATE;  ( -- )
  latest-cfa @ compiler:(cfaidx-do-create) <> " finish what?" ?error
;


also compiler definitions

;; patch CREATEd word
: (SET-DOER-NO-CHECKS)  ( doer-pfa cfa -- )
  compiler:(cfaidx-do-does) over !
  cfa->does-cfa !
;

;; patch CREATEd word
: (SET-DOER)  ( doer-pfa cfa -- )
  dup @
    dup compiler:(cfaidx-do-create) =
    swap dup compiler:(cfaidx-do-does) =
    swap 0x1000_0000 u< or or
  " trying to set DOER on non-CREATEd word" ?not-error
  (set-doer-no-checks)
;

;; put doer address to its CFA, VM will do the rest
: (DOES>)  ( doer-addr -- )
  latest-cfa (set-doer)
;

previous definitions

: DOES>  ( -- )  ( pfa )
  compiler:(check-semi)
  ;; end current word definition
  compile forth:(lit)
  here 0 ,  ;; DOES PFA -- will be patched later
  compile compiler:(does>)
  compiler:(compile-exit) compiler:(finish)
  ;; current address is DOER
  here swap!
  compiler:(start-semi)
; immediate

: :NONAME  ( -- cfa )
  compiler:(create-nameless) compiler:(start-semi)
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ANS idiocity
;;
;; moved to C kernel
(*
4 constant CELL
alias-for 4* is CELLS  ( n -- n*cells )
alias-for 4+ is CELL+  ( n -- n+cell )
alias-for 4- is CELL-  ( n -- n-cell )
: +CELLS  ( a n -- a+n*cells )  cells + ;
: -CELLS  ( a n -- a+n*cells )  cells - ;
*)


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; compiled and immediate printer
;;
: .(  ( -- )
  41 parse " string expected" ?not-error compiler:(unescape) type
; immediate

: ."
  compiler:comp? forth:(0branch) [ compiler:(mark-j>) ]
    [compile] " compile type  ;; "
  forth:(branch) [ compiler:(mark-j>) swap compiler:(resolve-j>) ]
    34 parse " string expected" ?not-error compiler:(unescape) type
  [ compiler:(resolve-j>) ]
; immediate
\ debug:decompile ."

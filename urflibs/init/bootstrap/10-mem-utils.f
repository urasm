;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; and now for something completely different...
;; UrForth/C Forth Engine!
;; Copyright (C) 2023 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

: (HANDLE-ADDR?)  ( addr -- bool )  forth:(addr-handle-bit) and notnot ;

: HEX  ( -- )  16 base ! ;
: DECIMAL  ( -- )  10 base ! ;
: OCTAL  ( -- )  8 base ! ;
: BINARY  ( -- )  2 base ! ;

;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; some standard words
;;

: SP@  ( -- n ) 0 mtask:state-sp@ ;
: SP!  ( n -- ) 0 mtask:state-sp! ;

: RP@  ( -- n ) r> 0 mtask:state-rp@ swap >r ;
: RP!  ( n -- ) r> swap 0 mtask:state-rp! >r ;

: DEPTH  ( -- n ) SP@  ;


;; moved to C kernel
(*
: FILL  ( addr count byte -- )
  a>r rot >a
  swap for dup c!a +1>a endfor drop
  r>a
;

: FILL-CELLS  ( addr count u32 -- )
  a>r rot >a
  swap for dup !a +4>a endfor drop
  r>a
;

: CMOVE-CELLS  ( source dest count -- )
  a>r
  for  ( source dest )
    swap dup >a cell+ swap     ( source+1 dest ) -- source in a
    @a swap dup >a cell+ swap  ( source+1 dest+1 c ) -- dest in a
    !a
  endfor 2drop
  r>a
;

: CMOVE  ( source dest count -- )
  \ ." CMOVE: from=" ROT DUP U. NROT
  \ ." to=" OVER U.
  \ ." count=" DUP U.
  \ CR
  \ FOR
  \   OVER C@ OVER C! 1+ SWAP 1+ SWAP
  \ ENDFOR 2DROP
  a>r
  for  ( source dest )
    swap dup >a 1+ swap      ( source+1 dest ) -- source in a
    c@a swap dup >a 1+ swap  ( source+1 dest+1 c ) -- dest in a
    c!a
  endfor 2drop
  r>a
;

: CMOVE>  ( source dest count -- )
  \ ." CMOVE>: from=" ROT DUP U. NROT
  \ ." to=" OVER U.
  \ ." count=" DUP U.
  \ CR
  a>r
  >r swap r@ + swap r@ + r>
  for  ( source+count dest+count )
    swap 1- dup >a swap      ( source-1 dest ) -- source-1 in a
    c@a swap 1- dup >a swap  ( source-1 dest-1 c ) -- dest-1 in a
    c!a
  endfor 2drop
  r>a
;

;; uses CMOVE or CMOVE> (i.e. works like libc `memmove`)
;; negative length does nothing (i.e. you cannot MOVE more that 2GB of data)
: MOVE  ( from to len -- )
  dup 0> if
    >r
    2dup u< if  ;; from < to: may need to use CMOVE> (copy backwards)
      r> cmove>
    else  ;; from > to: use CMOVE (normal forward copy)
      r> cmove
    endif
  else drop 2drop
  endif
;
*)

: BLANKS  ( addr count -- )  bl fill ;
: ERASE  ( addr count -- )  0 fill ;
: ERASE-CELLS  ( addr count -- )  0 fill-cells ;


;; everything else moved to C kernel

;; yay, ABS without branches! because i can, lol
\ a + (a>>31) ^ (a>>31)
(*
: ABS  ( a -- |a| )  dup -31 ash dup rot + xor ;
: SIGN?  ( n -- -1|0|1 )  dup -31 ash swap 0> 1 and + ;
: NEGATE  ( n -- -n )  bitnot 1+ ;

: LSHIFT  ( n count -- n )  dup 0< " invalid shift amount" ?error lsh ;
: RSHIFT  ( n count -- n )  dup 0< " invalid shift amount" ?error negate lsh ;
: ARSHIFT ( n count -- n )  dup 0< " invalid shift amount" ?error negate ash ;

alias-for LSHIFT is SHL
alias-for RSHIFT is SHR
alias-for LSHIFT is SAL
alias-for ARSHIFT is SAR

: LO-WORD  ( a -- a&0xffff )  0xffff and ;
: HI-WORD  ( a -- [a>>16]&0xffff ) -16 lsh lo-word ;

: LO-BYTE  ( a -- a&0xff )  0xff and ;
: HI-BYTE  ( a -- [a>>8]&0xff )  -8 lsh lo-byte ;

: 0!     ( addr -- )  0 swap! ;
: 1!     ( addr -- )  1 swap! ;
: +!     ( n addr -- )  dup @ rot + swap! ;
: -!     ( n addr -- )  dup @ rot - swap! ;
: 1+!    ( addr -- )  1 swap +! ;
: 1-!    ( addr -- )  1 swap -! ;

: BCOUNT  ( addr -- addr+1 count )  dup 1+ swap c@ ;
: COUNT   ( addr -- addr+4 count )  dup 4+ swap @ ;

: WITHIN  ( value a b -- value>=a&&value<b )
  over - >r - r> u<
;

: UWITHIN  ( value a b -- value>=a&&value<b )
  rot dup >r swap u< r> rot u>= and
;

;; unsigned
: BOUNDS?  ( value a b -- value>=a&&value<=b )
  rot dup >r swap u<= r> rot u>= and
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; additional stack ops
;;
: NIP  ( a b -- b )  swap drop ;
: TUCK  ( a b -- b a b )  swap over ;
*)

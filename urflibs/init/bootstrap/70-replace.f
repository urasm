;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; and now for something completely different...
;; UrAsm Forth Engine!
;; GPLv3 ONLY
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


also debug definitions

: (replace)  ( what-cfa with-cfa -- )
  (*
  compiler:(create-nameless) cfa->pfa swap
  state @ >r [compile] ]
  compile drop [compile] cfaliteral compile execute-tail
  r> state !
  swap!
  *)
  over compiler:(cfaidx-do-redirect) swap!
  swap cfa->does-cfa !
;

: REPLACE  ( -- )  \ what with
  -find-required
  parse-name " with" string:=ci " `WITH` expected" ?not-error
  -find-required (replace)
;

previous definitions

;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; and now for something completely different...
;; UrForth/C Forth Engine!
;; Copyright (C) 2023 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


;; $DEFINE defname
: $DEFINE
  parse-name dup " define name expected" ?not-error
  parse-skip-line-comments
  tib-peekch " $DEFINE doesn't accept extra args yet" ?error
  ($define)
;

;; $UNDEF defname
: $UNDEF
  parse-name dup " define name expected" ?not-error
  parse-skip-line-comments
  tib-peekch " $DEFINE doesn't accept extra args yet" ?error
  ($undef)
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; simple expression evaluator
;;

vocabulary ($cc-eval)
also ($cc-eval) definitions

0 value curr-word-addr
0 value curr-word-len
0 value $if-count
0 value allow-continuations

0 value ($cc-trace)

defer $skip-conds
defer $cc-expr  ( doeval? -- value )


: curr-word  ( -- addr count )  curr-word-addr curr-word-len ;

: one-char-word  ( -- )
  (tib-in) to curr-word-addr  1 to curr-word-len  tib-skipch
;

;; one or two same chars
: one-two-char-word  ( -- )
  (tib-in) to curr-word-addr
  tib-getch tib-peekch = if tib-skipch 2 to curr-word-len
  else 1 to curr-word-len endif
;

: is-special-char  ( ch -- bool )
  case
    [char] ! of true endof
    [char] ( of true endof
    [char] \ of true endof
    [char] ) of true endof
    [char] ~ of true endof
    [char] & of true endof
    [char] | of true endof
    [char] ; of true endof
    otherwise drop false
  endcase
;

: cc-parse-special  ( -- )
  tib-peekch dup [char] & = swap [char] | = or
  if one-two-char-word else one-char-word endif
;

: cc-parse-id  ( -- )
  (tib-in) to curr-word-addr
  begin tib-peekch dup bl > swap is-special-char not and
  while tib-skipch
  repeat
  (tib-in) curr-word-addr - to curr-word-len
;

: next-word  ( -- )
  begin
    tib-peekch case
      0 of (tib-in) to curr-word-addr  0 to curr-word-len  true endof
      [char] ; of one-char-word parse-skip-line  0 to curr-word-len  true endof
      [char] \ of  ;; possible continuation
          allow-continuations if
            tib-skipch parse-skip-blanks
            tib-peekch " invalid continuation" ?error
            refill-nocross " unexpected end of file" ?not-error
            false
          else one-char-word true
          endif
        endof
      bl <=of tib-skipch false endof
      otherwise
        is-special-char if cc-parse-special else cc-parse-id endif
        true
    endcase
  until
  \ ." |" curr-word xtype ." | -- " tib-peekch xemit cr
;

;; parse required argument in parens
: next-word-arg  ( -- )
  next-word curr-word " (" string:= " '(' expected" ?not-error
  parse-skip-blanks
  ;; quoted?
  tib-peekch dup 34 = over 96 = or swap 39 = or if
    tib-getch parse " argument expected" ?not-error
    to curr-word-len to curr-word-addr
  else
    ;; parse up to blank or ")"
    parse-skip-blanks
    (tib-in) to curr-word-addr  0 to curr-word-len
    begin
      tib-peekch dup bl <= swap 41 = or
    not-while
      tib-skipch
      curr-word-len 1+ to curr-word-len
    repeat
  endif
  \ ." |" curr-word xtype ." | -- " tib-peekch xemit cr
  curr-word-len 1 = curr-word-addr c@ is-special-char and " identifier required" ?error
  curr-word  ;; save address
  ;; check final ")"
  next-word curr-word " )" string:= " ')' expected" ?not-error
  ;; restore word
  to curr-word-len to curr-word-addr
  \ ." ||" curr-word xtype ." | -- " tib-peekch xemit cr
;


vocabulary ($cc-eval-terms)
also ($cc-eval-terms) definitions

: defined    ( -- val )
  next-word-arg curr-word forth:($defined?)
  ?dup ifnot  ;; try without a leading dollar
    curr-word-len 2 >  curr-word-addr c@ [char] $ =  and if
      curr-word-addr 1+ curr-word-len 1- forth:($defined?)
    else false
    endif
  endif
;
: undefined  ( -- val )  defined not ;
: has-word   ( -- val )  next-word-arg curr-word find-word if drop true else false endif ;
: no-word    ( -- val )  has-word not ;
alias-for undefined is not-defined

previous definitions


: $cc-term  ( -- value )
  \ ." $term: " curr-word xtype cr
  curr-word vocid: ($cc-eval-terms) find-word-in-voc  ( cfa true / false )
  if execute
  \ ." $term:   res: " dup . cr
  else
    ;; special syntax: $defvarname
    curr-word-len 2 > curr-word-addr c@ 36 = and if
      curr-word-addr 1+ curr-word-len 1- forth:($defined?)
    else
      curr-word true base @ forth:(based-number)  ( num true / false )
      ifnot endcr space curr-word xtype ." ? -- wut?!\n" " unknown term" error endif
    endif
  endif
  next-word
;

: $cc-unary  ( -- value )
  curr-word " (" string:= if
    next-word $cc-expr
    curr-word " )" string:= " unbalanced parens" ?not-error
    next-word
  else
    curr-word " NOT" string:=ci
    curr-word " !" string:= or
    curr-word " ~" string:= or
    if next-word recurse not
    else $cc-term
    endif
  endif
;

: $is-logand  ( -- bool )
  curr-word " AND" string:=ci
  curr-word " &&" string:= or
  curr-word " &" string:= or
;

: $is-logor  ( -- bool )
  curr-word " OR" string:=ci
  curr-word " ||" string:= or
  curr-word " |" string:= or
;

: ($cc-log-create)  ( cfacheck cfalogop cfanext -- value )
  create
  ;; pfa: cfanext cfacheck cfaop
  -find-required ,  ;; cfanext
  -find-required ,  ;; cfacheck
  -find-required ,  ;; cfaop
 does>  ( pfa -- value )
  dup >r @ execute
  begin r@ cell+ @ execute
  while
    next-word r@ @ execute
    r@ 2 +cells @ execute
  repeat
  rdrop
;

($cc-log-create) $cc-and  $cc-unary $is-logand logand
($cc-log-create) $cc-or   $cc-and   $is-logor  logor

: ($cc-expr)  ( -- )
  allow-continuations >r
  1 to allow-continuations $cc-or
  r> to allow-continuations
;

' ($cc-expr) to $cc-expr


: ensure-eol
  parse-skip-blanks
  tib-peekch ?dup if
    [char] ; <> " invalid expression" ?error
    parse-skip-line
  endif
;

: $process-cond
  next-word $cc-expr ensure-eol
  if $if-count 1+ to $if-count
  else true $skip-conds endif
;

\ 0 value ($skip-from-line)

: ($skip-conds)  ( toelse -- )
  (*
    ." skip: line=" 0 (include-file-line) .
    ." file: " 0 (include-file-name) xtype cr
  *)
  0 >r  ( toelse | level )
  begin
    refill-nocross " unexpected end of file" ?not-error
    next-word
    false  ( toelse done? | level )
    curr-word " $IF" string:=ci if drop r> 1+ >r false endif
    curr-word " $ENDIF" string:=ci if drop
      ;; in nested ifs, look only for $ENDIF
      r@ if r> 1- >r false
      else
        ;; it doesn't matter which part we're skipping, it ends here anyway
        true
      endif
    endif
    curr-word " $ELSE" string:=ci if drop
      ;; if we're skipping "true" part, go on
      dup if
        $if-count 1+ to $if-count
        r@ 0=  \ false
      else
        ;; we're skipping "false" part, there should be no else
        r@ " unexpected $ELSE" ?not-error
        false
      endif
    endif
    ;; only for level 0
    r@ ifnot
      curr-word " $ELSIF" string:=ci if drop
        ;; if we're skipping "true" part, go on
        dup if
          ;; process the conditional
          next-word $cc-expr ensure-eol
          ;; either resume normal execution, or keep searching for $ELSE
          if $if-count 1+ to $if-count true
          else false endif
        else
          ;; we're skipping "false" part, there should be no else
          " unexpected $ELSIF" error
        endif
      endif
    endif
  until
  drop  ;; drop `toelse`
  r> 0<> " oops?" ?error
  \ refill " unexpected end of file" ?not-error
  parse-skip-line
;

' ($skip-conds) to $skip-conds

previous definitions


: $IF
  ($cc-eval):$process-cond
; immediate

: $ELSE
  ($cc-eval):$if-count " unexpected $ELSE" ?not-error
  ($cc-eval):ensure-eol
  false ($cc-eval):$skip-conds
; immediate

: $ELSIF
  ($cc-eval):$if-count " unexpected $ELSIF" ?not-error
  \ ($cc-eval):$if-count 1- to ($cc-eval):$if-count
  false ($cc-eval):$skip-conds
; immediate

: $ENDIF
  ($cc-eval):$if-count " unexpected $ENDIF" ?not-error
  ($cc-eval):ensure-eol
  ($cc-eval):$if-count 1- to ($cc-eval):$if-count
; immediate


also ($cc-eval):($cc-eval-terms) definitions
also ($cc-eval)

$IF HAS-WORD(URASM:HAS-LABEL?)
: HAS-LABEL  ( -- val )  next-word-arg curr-word urasm:has-label? ;
: NO-LABEL   ( -- val )  has-label not ;
$ENDIF

$IF HAS-WORD(URASM:PASS@)
: PASS0      ( -- val )  urasm:pass@ 0= ;
: PASS1      ( -- val )  urasm:pass@ 0<> ;
alias-for PASS0 is PASS-0
alias-for PASS1 is PASS-1
$ENDIF

previous previous definitions

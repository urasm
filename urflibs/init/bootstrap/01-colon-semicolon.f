;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; and now for something completely different...
;; UrForth/C Forth Engine!
;; Copyright (C) 2023 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; colon and semicolon
;;

;; define tick (we'll need it later)
;; ( -- cfa )  \ name
" '" compiler:(wflag-protected) compiler:(create-word-header)
compiler:(cfaidx-do-forth) compiler:cfa, ]
  parse-name 2dup find-word forth:(tbranch) [ here 0 , ]
    endcr space type " ? -- wut?\n" type  " word not found" error
  [ here swap compiler:(branch-addr!) ]
  nrot 2drop
  forth:(exit)
[  ;; return to interpreter mode (we don't have a semicolon yet)
\ ' ' debug:(decompile-cfa)

;; some compiler helpers

;; define in COMPILER
compiler forth:context forth:@ forth:current forth:!

forth:" (CTLID-COLON)" compiler:(wflag-protected) compiler:(create-word-header)
compiler:(cfaidx-do-const) compiler:cfa, 0xdeadfeed forth:,

forth:" (;-OPTIMISE)" compiler:(wflag-protected) compiler:(create-word-header)
compiler:(cfaidx-do-defer) compiler:cfa, forth:' forth:noop forth:,

;; turn on the compiler
forth:" (START)" compiler:(wflag-protected) compiler:(create-word-header)
compiler:(cfaidx-do-forth) compiler:cfa, forth:]
  compiler:?exec
  compiler:(reset-sinopt) forth:]
  forth:(exit)
forth:[  ;; return to interpreter mode (we don't have a semicolon yet)

;; compile final "exit"
forth:" (COMPILE-EXIT)" compiler:(wflag-protected) compiler:(create-word-header)
compiler:(cfaidx-do-forth) compiler:cfa, forth:]
  compiler:?comp
  ;; call "exit"
  forth:[ forth:' forth:exit forth:compile, forth:]  ;; because it is immediate
  forth:(exit)
forth:[  ;; return to interpreter mode (we don't have a semicolon yet)

;; turn off the compiler, finish current word
;; will NOT automatically compile "exit"
forth:" (FINISH)" compiler:(wflag-protected) compiler:(create-word-header)
compiler:(cfaidx-do-forth) compiler:cfa, forth:]
  compiler:?comp
  forth:[ forth:' forth:[ forth:compile, forth:]  ;; compile "[" (it is immediate)
  compiler:(reset-sinopt)  ;; just in case
  compiler:(;-optimise)    ;; call optional optimiser
  forth:(exit)
forth:[  ;; return to interpreter mode (we don't have a semicolon yet)

;; start semicolon definition (push flag, start the compiler)
forth:" (START-SEMI)" compiler:(wflag-protected) compiler:(create-word-header)
compiler:(cfaidx-do-forth) compiler:cfa, forth:]
  compiler:(ctlid-colon)  ;; push flag for semicolon
  compiler:(start)
  forth:(exit)
forth:[  ;; return to interpreter mode (we don't have a semicolon yet)

;; check semicolon flag
forth:" (CHECK-SEMI)" compiler:(wflag-protected) compiler:(create-word-header)
compiler:(cfaidx-do-forth) compiler:cfa, forth:]
  compiler:?comp compiler:(ctlid-colon) forth:<> forth:" primary imbalance" forth:?error
  forth:(exit)
forth:[  ;; return to interpreter mode (we don't have a semicolon yet)


;; define in FORTH again
forth:forth forth:context forth:@ forth:current forth:!


;; LATEST-CFA
;; ( -- latest-lfa )
" LATEST-LFA" compiler:(wflag-protected) compiler:(create-word-header)
compiler:(cfaidx-do-forth) compiler:cfa, ]
  current @ compiler:(vocofs-latest) + @ forth:(exit)
[  ;; return to interpreter mode (we don't have a semicolon yet)

;; define colon
" :" compiler:(wflag-protected) compiler:(create-word-header)
compiler:(cfaidx-do-forth) compiler:cfa, ]
  compiler:?exec parse-name
  ;; sanitise default word flags
  forth:(new-word-flags) @  compiler:(wflag-hidden) compiler:(wflag-protected) or and
  ;; create smudged words
  compiler:(wflag-smudge) or  compiler:(create-word-header)
  compiler:(cfaidx-do-forth) compiler:cfa,
  compiler:(start-semi)
  forth:(exit)
[  ;; return to interpreter mode (we don't have a semicolon yet)

;; define semicolon
" ;" compiler:(wflag-protected) compiler:(wflag-immediate) or compiler:(create-word-header)
compiler:(cfaidx-do-forth) compiler:cfa, ]
  compiler:(check-semi)
  compiler:(compile-exit)
  ;; fix smudge
  latest-lfa lfa->nfa  dup @ compiler:(wflag-smudge) bitnot and swap!
  compiler:(finish)
  forth:(exit)
[  ;; return to interpreter mode (we don't have a semicolon yet)

;; <protected-words>
(new-word-flags) @ compiler:(wflag-protected) or (new-word-flags) !

\ : LATEST-LFA  ( -- lfa )  current @ compiler:(vocofs-latest) + @ ;
: LATEST-CFA  ( -- cfa )  latest-lfa lfa->cfa ;
: LATEST-PFA  ( -- pfa )  latest-lfa lfa->pfa ;
: LATEST-NFA  ( -- nfa )  latest-lfa lfa->nfa ;

: IMMEDIATE ( -- )  compiler:(wflag-immediate) latest-nfa xor! ;
: (HIDDEN)  ( -- )  compiler:(wflag-hidden) latest-nfa or! ;
: (PUBLIC)  ( -- )  compiler:(wflag-hidden) latest-nfa ~and! ;

: [COMPILE]  compiler:?comp ' compile, ; immediate
: [']  compiler:?comp ' cfaliteral ; immediate

: (ALIAS-FOR)  ( addr count orig-cfa -- )  \ newword
  dup >r cfa->nfa @ 0xffff_00_00 and  ;; get word flags
  compiler:(create-word-header)
  compiler:(cfaidx-do-redirect) compiler:cfa,
  r> latest-cfa cfa->does-cfa !
;

: ALIAS-FOR  ( -- )  \ oldword newword
  forth:' >r  ;; we'll need cfa later
  parse-name 2 = swap w@ 0x20_20 or 0x73_69 = and " `IS` expected" ?not-error
  parse-name r> (alias-for)
;

alias-for ' is -FIND-REQUIRED

: PARSE-SKIP-COMMENTS  ( -- )  true (parse-skip-comments) ;
;; only single-line
: PARSE-SKIP-LINE-COMMENTS  ( -- )  false (parse-skip-comments) ;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; wordlist utils
;;

: (SET-DEF-WORD-FLAGS)   ( flg -- )  (new-word-flags) @ or (new-word-flags) ! ; (hidden)
: (RESET-DEF-WORD-FLAGS) ( flg -- )  (new-word-flags) @ swap bitnot and (new-word-flags) ! ; (hidden)

: <PUBLIC-WORDS>  ( -- )  compiler:(wflag-hidden) forth:(reset-def-word-flags) ;
: <HIDDEN-WORDS>  ( -- )  compiler:(wflag-hidden) forth:(set-def-word-flags) ;
: <UNPROTECTED-WORDS>  ( -- )  compiler:(wflag-protected) forth:(reset-def-word-flags) ;
: <PROTECTED-WORDS>  ( -- )  compiler:(wflag-protected) forth:(set-def-word-flags) ;

: ONLY  ( -- )  0 (vsp!) ;
: ALSO  ( -- )  context @ (vsp@) (vsp-at!) (vsp@) 1+ (vsp!) ;
: PREVIOUS  ( -- )  (vsp@) 1- dup (vsp!)  (vsp-at@) context ! ;
: DEFINITIONS  ( -- )  context @ current ! <public-words> ;


also debug definitions

: DECOMPILE  ( -- )  \ wordname
  -find-required (decompile-cfa)
;

previous definitions

\ debug:decompile debug:decompile


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; we need a way to compile an immediate string literal
;;

: IMM-STRLITERAL  strliteral ; immediate

;; moved to C kernel
;; for NFAs
\ : ID-COUNT ( addr -- addr count )  dup c@ swap 4 + swap  ;

;; compiles CFA to be executed
;; defined in the kernel
\ : COMPILE,  ( cfa -- )  , ;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; some compiler helpers
;;

also compiler definitions

: SET-SMUDGE    ( -- )  (wflag-smudge) latest-nfa or! ;
: RESET-SMUDGE  ( -- )  (wflag-smudge) latest-nfa ~and! ;

: SET-WARG  ( warg -- )  latest-nfa @ (warg-mask) ~and or latest-nfa ! ;
: OR-WFLAGS  ( flags -- )  latest-nfa or! ;

: (GET-NEW-WORD-FLAGS)  ( -- flags )
  forth:(new-word-flags) @ (wflag-hidden) (wflag-protected) or and
;

;; smudged
: (CREATE-HEADER)  ( addr count -- )
  (get-new-word-flags) (wflag-smudge) or (create-word-header)
;

;; smudged
: (CREATE-FORTH-HEADER)  ( addr count -- )
  (create-header)
  compiler:(cfaidx-do-forth) compiler:cfa,
;

;; toggle smudge
: SMUDGE  ( -- )
  latest-nfa dup @ compiler:(wflag-smudge) xor swap!
;

;; smudged, hidden
: (CREATE-NAMELESS)  ( -- cfa )
  (get-new-word-flags) (wflag-smudge) or (wflag-hidden) or (create-nameless-word-header)
  here  compiler:(cfaidx-do-forth) compiler:cfa,
;

: (MK-CONST-VAR)  ( value cfaidx -- )
  parse-name (create-header)
  \  " LFA: " type forth:latest-lfa . cr
  \  " CFA: " type forth:latest-cfa . cr
  \  " NFA: " type forth:latest-nfa . cr
  \  " N# : " type forth:latest-nfa c@ . cr
  \  " NME: " type forth:latest-nfa dup c@ swap 4+ swap xtype cr
  compiler:cfa, ( cfa )
  , ( value )
  reset-smudge
;

previous definitions

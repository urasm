;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; and now for something completely different...
;; UrForth/C Forth Engine!
;; Copyright (C) 2023 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; additional math
;;

;; moved to C kernel
(*
: MIN  ( a b -- max-of-a-b )  2dup > if swap endif drop ;
: MAX  ( a b -- max-of-a-b )  2dup < if swap endif drop ;

: UMIN  ( a b -- max-of-a-b )  2dup u> if swap endif drop ;
: UMAX  ( a b -- max-of-a-b )  2dup u< if swap endif drop ;
*)


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; print number to buffer
;;

<hidden-words>
;; the buffer is right before PAD
;; allocate number translation buffer
\ 0x3464_97af handle:new constant (#buf)  ;; arbitrary typeid
\ forth:(max-handle-ofs) 1+ constant (#buf-size)
\ (#buf-size) (#buf) handle:size!
(#buf) (#buf-size) + constant (#buf-end)
\ 0 variable (#buf-ofs)
(#buf) 4- constant (#buf-ofs)
<public-words>


: HOLD  ( ch -- )
  (#buf-end)
  (#buf-ofs) @  dup (#buf-size) u>= " number too long" ?error
  1+ dup (#buf-ofs) !
  - c!
;

: HOLDS  ( addr u -- )  begin dup while 1- 2dup + c@ hold repeat 2drop ;

;; add '-'
: SIGN  ( n -- )
  0< if [char] - hold endif
;

: <#  ( n -- n )
  (#buf-ofs) 0!
;

: #>  ( n -- addr count )
  drop
  (#buf-ofs) @ (#buf-end) over - swap
;

: #  ( n -- n )
  base @ u/mod  ( n/base n%base )
  48 + dup 57 > if 7 + endif
  hold
;

: #S  ( n -- 0 )
  begin # dup not-until
;

: #SIGNED  ( n -- 0 )
  dup 0< dup >r if negate endif
  #s r> if [char] - hold endif
;

: .  ( n -- )  <# #signed #> type space ;

: .R  ( n width -- )
  >r <# #signed #>   ( addr count | width )
  dup r> swap - spaces type
;

: U.  ( n -- )  <# #s #> type space ;

: U.R  ( n width -- )
  >r <# #s #>   ( addr count | width )
  dup r> swap - spaces type
;

: 0.R  ( n -- )  <# #signed #> type ;
: 0U.R  ( n -- )  <# #s #> type ;

\ " AAAAAAAAAAAAAAAAAAAAAAAAAAAA\n" TYPE
\ 669 . cr
\ " AAAAAAAAAAAAAAAAAAAAAAAAAAAA\n" TYPE

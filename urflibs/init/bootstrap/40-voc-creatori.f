;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; and now for something completely different...
;; UrForth/C Forth Engine!
;; Copyright (C) 2023 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; vocabulary creation
;;

;; worker for the following words
: (DO-ALLOT)  ( n -- )  forth:(dp-here) @ +! ; (hidden)

;; contrary to other systems, alloting negative number of bytes is prohibited.
;; use "UNALLOT" to "deallot" dictionary space.
: N-ALLOT  ( n -- start-addr )  dup 0< " negative allot" ?error  here >r (do-allot) r> ;

;; contrary to other systems, alloting negative number of bytes is prohibited.
;; use "UNALLOT" to "deallot" dictionary space.
: ALLOT  ( n -- )  n-allot drop ;

;; "deallot" given number of bytes.
: UNALLOT  ( n -- )  dup 0< " negative unallot" ?error  negate (do-allot) ;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; POSTPONE
;;

;; this does "[COMPILE]" for immediates, and "COMPILE" for non-immediates.
;; i extended it to numeric literals.
: POSTPONE  ( -- )  \ name
  compiler:?comp
  parse-name 2dup find-word if
    dup compiler:immediate-word? if compile, else compiler:(compile-cfa-literal) endif
  else ;; try number
    2dup true base @ (based-number)  ( addr count allowsign? base -- num TRUE / FALSE )
    ifnot endcr space xtype ." ? -- postpone what?" cr  " word not found" error endif
    [compile] literal compile literal
  endif 2drop
; immediate


also compiler


;; create new headerless wordlist
: (new-wordlist)  ( parentvocid need-hashtable? -- vocid )
  ;; typeid, used by Forth code (structs and such)
  0 ,                   ;; typeid
  here >r               ;; result -- vocid
  ;; create wordlist struct
  0 ,                   ;; latest
  here >r               ;; for voclink
  forth:(voc-link) @ ,  ;; voclink
  swap ,                ;; parent
  0 ,                   ;; header NFA
  ;; create hashtable
  if (voc-htable-size) for 0 , endfor
  else (voc-htable-noflag) ,
  endif
  ;; link to voclink
  r@ (addr-temp-bit) and if rdrop  \ ." NO-LINK!\n"
  else r> forth:(voc-link) ! endif
  ;; done
  r>
; (hidden)

;; create vocabulary word
: (create-named-vocab)  ( vocid addr count -- )
  ;; name flags
  (get-new-word-flags)
  (wflag-smudge) or (wflag-vocab) or
  (create-word-header)
  ;; fill CFA and PFA
  (cfaidx-do-voc) compiler:cfa,
  dup , ;; wordlist address
  reset-smudge
  (vocofs-header) + latest-nfa swap!
; (hidden)

;; create vocabulary word
: (create-vocab)  ( vocid -- )  \ vocname
  parse-name (create-named-vocab)
; (hidden)

;; check if the given CFA defined as a vocabulary header
: (is-voc-word?)  ( cfa -- bool )
  dup @ (cfaidx-do-voc) =
  swap cfa->nfa @ (wflag-vocab) (wflag-smudge) or and  logor
;

;; doesn't check arguments
: (word->vocid)  ( cfa -- vocid )
  cfa->pfa @
;

;; doesn't check arguments
: (vocid-parent@)  ( vocid -- vocid )  (vocofs-parent) + @ ;
: (vocid-parent!)  ( parent-vocid vocid -- )  (vocofs-parent) + ! ;

: (vocid-typeid@)  ( vocid -- typeid )  cell- @ ;
: (vocid-typeid!)  ( typeid vocid -- )  cell- ! ;

previous

: VOCID-HEADNFA@  ( vocid -- head-nfa / FALSE )
  compiler:(vocofs-header) + @
;

;; useful low-level words to create vocabs with already parsed names
: (vocabulary-ex)  ( addr count parent need-hashtbl? -- )
  (new-wordlist) nrot (create-named-vocab)
;

: (vocabulary)  ( addr count -- )  0 true  (vocabulary-ex) ;
: (simple-vocabulary)  ( addr count -- )  0 false  (vocabulary-ex) ;
: (nested-vocabulary)  ( addr count -- )  current @ true  (vocabulary-ex) ;
: (simple-nested-vocabulary)  ( addr count -- )  current @ false  (vocabulary-ex) ;


: VOCABULARY  ( -- )  \ vocname
  0 true (new-wordlist)  (create-vocab)
;

;; vocabulary without a hash table
: SIMPLE-VOCABULARY  ( -- )  \ vocname
  0 false (new-wordlist)  (create-vocab)
;

: NESTED-VOCABULARY  ( -- )  \ vocname
  current @ true (new-wordlist)  (create-vocab)
;

: SIMPLE-NESTED-VOCABULARY  ( -- )  \ vocname
  current @ true (new-wordlist)  (create-vocab)
;

: VOCID:  ( -- vocid )  \ vocname
  -find-required dup (is-voc-word?) " vocabulary name expected" ?not-error
  ;; vocid is in PFA
  cfa->pfa @
  compiler:comp? if
    ;; compile vocid literal
    compile forth:(litvocid) ,
  endif
; immediate

: VOC-LATEST  ( vocid -- latest )
  compiler:(vocofs-latest) + @
;


;; ALSO-DEFS: vocname
;; does "ALSO vocname DEFINITIONS"
: ALSO-DEFS:  ( -- )  \ vocname
  -find-required dup (is-voc-word?) " vocabulary name expected" ?not-error
  also (word->vocid) context ! definitions
;

;; does "PREVIOUS DEFINITIONS"
: PREV-DEFS  ( -- )
  previous definitions
;


;; create vocabulary if we don't have such word yet
: VOCAB-IF-NONE  ( -- )  \ name
  parse-name 2dup find-word if 2drop drop
  else (vocabulary) endif
;

;; create vocabulary if we don't have such word yet
: SIMPLE-VOCAB-IF-NONE  ( -- )  \ name
  parse-name 2dup find-word if 2drop drop
  else (simple-vocabulary) endif
;

;; create nested vocabulary if we don't have such word yet
: NESTED-VOCAB-IF-NONE  ( -- )  \ name
  parse-name 2dup find-word if 2drop drop
  else (nested-vocabulary) endif
;

;; create nested vocabulary if we don't have such word yet
: SIMPLE-NESTED-VOCAB-IF-NONE  ( -- )  \ name
  parse-name 2dup find-word if 2drop drop
  else (simple-nested-vocabulary) endif
;


also-defs: compiler

;; because in scolon we doesn't have "IF" yet
: cfa-scolon?  ( cfa -- bool )
  dup forth-word? if
    cfa->pfa
    dup @ ['] forth:(branch) = if
      2 +cells @ ['] forth:(branch) =
    else drop false endif
  else false endif
;

prev-defs

;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; and now for something completely different...
;; UrForth/C Forth Engine!
;; Copyright (C) 2023 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; TO, +TO
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(*
it is possible to extend "TO" for your own words. there are two scattered
colon definitions for that.

  ;; ( addr count FALSE -- addr count FALSE / TRUE )
  : (TO-EXTENDER) ... ; (HIDDEN)

this will be called before "TO" tried to find a word. if you processed
everything by yourself, remove `addr` and `count`, and return TRUE.
otherwise don't touch anything.

;; ( cfa FALSE -- cfa FALSE / TRUE )
: (TO-EXTENDER-FOUND) ... ; (HIDDEN)

this will be called after "TO" succesfully found a word.


please, note that you have to start your extensions with code like this:

  ?DUP IFNOT
    your code here
  ENDIF

i.e. don't do anything if some other extension already processed the
request.

*)


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; extenders
;;

false value (+to-negated)  ;; set for "-to"

;; called before trying to find a word
;; ( addr count FALSE -- addr count FALSE / TRUE )
: (to-extender) ... ; (hidden)

;; called before trying to find a word
;; ( addr count FALSE -- addr count FALSE / TRUE )
: (+to-extender) ... ; (hidden)

;; called after the word was found
;; ( cfa FALSE -- cfa FALSE / TRUE )
: (to-extender-found) ... ; (hidden)

;; called after the word was found
;; ( cfa FALSE -- cfa FALSE / TRUE )
: (+to-extender-found) ... ; (hidden)


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; main words
;;

;; "TO" for values and defers
..: (to-extender-found)  ( cfa FALSE -- cfa FALSE / TRUE )
  ?dup ifnot
    dup value? over defer? or if
      compiler:comp? if ['] forth:(direct:!) <compile, cfa->pfa , compile>
      else forth:(to-value)
      endif
      true
    else false
    endif
  endif
;..

;; "+TO" for values
..: (+to-extender-found)  ( cfa FALSE -- cfa FALSE / TRUE )
  ?dup ifnot
    dup value? if
      compiler:comp? if
        (+to-negated) if ['] forth:(direct:-!) else ['] forth:(direct:+!) endif
        <compile, cfa->pfa , compile>
      else (+to-negated) ifnot negate endif forth:(+to-value)
      endif
      true
    else false
    endif
  endif
;..


: TO  ( n -- )
  parse-skip-comments parse-name  dup " word name expected" ?not-error
  false (to-extender)
  ifnot
    2dup find-word  ( addr count cfa TRUE / addr count FALSE )
    ifnot  ;; not found
      endcr space type ." ? -- wut?\n" " word not found" error
    endif nrot 2drop
    false (to-extender-found) " trying to use 'TO' with something strange" ?not-error
  endif
; immediate

: (+TO-COMPILER)  ( n -- )
  parse-skip-comments parse-name  dup " word name expected" ?not-error
  false (+to-extender)
  ifnot
    2dup find-word  ( addr count cfa TRUE / addr count FALSE )
    ifnot  ;; not found
      endcr space type ." ? -- wut?\n" " word not found" error
    endif nrot 2drop
    false (+to-extender-found) " trying to use '+TO' with something strange" ?not-error
  endif
; (hidden)

: +TO  ( n -- )  false to (+to-negated) (+to-compiler) ; immediate
: -TO  ( n -- )  true to (+to-negated) (+to-compiler) ; immediate

: (+imm-TO)  ( imm -- )
  compiler:comp? if literal endif
  [compile] +to
;

: +1-TO  ( -- )  1 (+imm-TO) ; immediate
: -1-TO  ( -- )  -1 (+imm-TO) ; immediate

: 0-TO  ( -- )
  0 compiler:comp? if literal endif
  [compile] to
; immediate


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; 2012 crap
;;

: IS ( defer-cfa -- )  \ name
  compiler:comp? if  [compile] ['] compile defer!
  else ' defer! endif
; immediate

: ACTION-OF ( -- cfa )  \ name
  compiler:comp? if  [compile] ['] compile defer@
  else ' defer@ endif
; immediate

;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; and now for something completely different...
;; UrForth/C Forth Engine!
;; Copyright (C) 2023 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; scattered colon implementation
;; based on the idea by M.L.Gassanenko ( mlg@forth.org )
;; written from scratch by Ketmar Dark
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(*
  : word  ... other-forth-code ;

  ..: word  more forth code  ;..
  ..: word  more forth code  ;..

  each "..:" will be executed when "word" is executed, starting
  from the last defined. to append as the last one instead the
  first one, use "..: word  code  <;..".

  note that "..." must always be the first word in a definition.
  you cannot use local variables with such words.
*)

also compiler definitions
0xcafef00d constant (ctlid-sc-colon)

: ?cfa-scolon  ( cfa -- )
  dup forth-word? " forth word expected" ?not-error
  dup cfa->nfa @ compiler:(wflag-scolon) and " s-colon word expected" ?not-error
  cfa->pfa
  dup @ ['] forth:(branch) <> " s-colon word expected" ?error
  2 +cells @ ['] forth:(branch) <> " s-colon word expected" ?error
;

previous definitions
also compiler


;; placeholder for scattered colon
;; it will compile two branches:
;; the first branch will jump to the first "..:" word (or over the two branches)
;; the second branch is never taken, and works as a pointer to the latest branch addr in the list
;; this way, each extension word will simply fix the last branch address, and update list tail
;; at the creation time, second branch points to the first branch
: ...  ( -- )
  ?comp
  latest-cfa forth-word? " forth word expected" ?not-error
  latest-pfa here <> " scattered colon must be first" ?error
  \ compiler:colon-word
  compile forth:(branch) (<j-mark) (mark-j>)
  compile forth:(branch) swap (<j-resolve)
  (resolve-j>)
  (wflag-scolon) or-wflags  \ (set-scolon)
; immediate


;; start scattered colon extension code
;; TODO: better error checking!
;; this does very simple sanity check, and remembers the address of the tail pointer
: ..:  ( -- pptp ourpfa compid )  \ word
  ?exec
  -find-required dup ?cfa-scolon ;; sanity check
  cfa->pfa 3 +cells  ;; pointer to the tail pointer
  (create-nameless)
  cfa->pfa ;; :noname leaves our cfa
  compiler:(start-semi)  ;; pttp ourpfa flag
;

;; s-colon words starts with:
;; BRANCH xaddr
;; BRANCH taddr
;; pptp points to the second branch argument
;; tadds initially points to the first branch argument

;; this ends the extension code
;; it patches jump at which list tail points to jump to our pfa, then
;; it compiles jump right after the list tail, and then
;; it updates the tail to point at that jump address
: ;..  ( pptp ourpfa compid -- )
  compiler:(check-semi)
  ( pptp ourpfa )  \ over forth:(dp-protected?) >r
  ;; first: patch first s-colon jump to point to our pfa
  over (branch-addr@) (branch-addr!)
  ;; second: compile jump to the original word
  >r compile forth:(branch) here 0 , ;; branch and its argument
  r@ cell+ swap (branch-addr!) ;; patch branch we just compiled
  ;; update tail pointer
  here cell- r> (branch-addr!)
  ;; we're done here
  compiler:(finish)
  \ r> if here forth:(dp-protect) endif
; immediate


;; this ends the extension code
;; makes the code first in the jump list
;; jumps to the destination of the first jump
;; patches the first jump so it points to our nonamed code
;; patches tail pointer so it points to our jump
: <;..  ( pptp ourpfa compid -- )
  compiler:(check-semi)
  ( pttp ourpfa )  \ over forth:(dp-protected?) >r
  >r  ( pttp | ourpfa )
  ;; get first jump destination
  compile (branch) here 0 ,  ( pttp jpatchaddr )
  over 2 -cells (branch-addr@) over (branch-addr!)  ;; fix our jump
  over 2 -cells r> swap (branch-addr!)  ;; fix first jump
  ;; patch original jump if there are no items in scattered chain yet
  over dup (branch-addr@) 2 +cells =  forth:(0branch) [ (mark-j>) ] ;; if
    swap (branch-addr!)
  forth:(branch) [ (mark-j>) swap (resolve-j>) ] ;; else
    2drop
  [ (resolve-j>) ] ;; endif
  ;; we're done here
  compiler:(finish)
  \ r> if here forth:(dp-protect) endif
; immediate

previous

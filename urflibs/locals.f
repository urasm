;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; and now for something completely different...
;; UrForth/C Forth Engine!
;; Copyright (C) 2023 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; local variables support
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(*
  : myword  ( a b -- c )
    args: a b
    locals: loc1 loc2

    :a TO :loc1
    :b TO :loc2
    :loc1 :loc2 +
  ;
here, "args:" will declare local variables, and automatically fill them
with arguments from the data stack. "locals:" declare "free" locals.
there can be more than one "locals:" section, but all such sections should
come before anything else. to use local, you must prepend its name with a
colon. the usual "TO" word is used to set local values.
*)

vocabulary (locals-support)
also (locals-support) definitions

;; offset in temp (user) dict
;; should be enough for now ;-)
\ 65536 constant locals-temp-voc-offset

0 value locals-dp-temp-saved
0 value locals-vocid

: new-locals-wordlist  ( -- )
  forth:(dp-here) @ dup forth:(dp-main) <> " invalid (DP-HERE)" ?error
  forth:(dp-temp) dup @ to locals-dp-temp-saved forth:(dp-here) !
  0 false forth:(new-wordlist) to locals-vocid
  forth:(dp-here) !
;

;; (L-ENTER) arg:
;; low byte of loccount is total number of locals
;; high byte is the number of args

: latest-has-locals?  ( -- bool )
  latest-pfa dup here = if drop false
  else @ ['] forth:(l-enter) = endif
;

: start-locals  ( -- )
  latest-pfa here <> if
    latest-has-locals? " \`locals:\` or \`args:\` should be the first word" ?not-error
  else  ;; init locals
    new-locals-wordlist compile forth:(l-enter) 0 ,
  endif
;

: add-local  ( addr count -- )
  start-locals
  latest-pfa cell+  dup c@ 255 = " too many locals" ?error
  1 over +! c@ >r  ;; save current local number to rstack
  ;; now create access word
  forth:(dp-here) @  current @
  forth:(dp-temp) forth:(dp-here) !  locals-vocid current !
  \ ." DP: " forth:(dp) @ . cr
  \ ." DP-TEMP: " forth:(dp-temp) @ . cr
  \ ." HERE: " here . cr
  2swap
  \ 2dup xtype cr
  compiler:(create-header)
  compiler:(cfaidx-do-const) compiler:cfa,  ;; why not
  r> ,  ;; local index
  compiler:reset-smudge
  forth:(dp-temp) @ here <> " wtf?!" ?error  ;; assertion
  ;; restore voc and DP
  current !  forth:(dp-here) !
;

;; create local access words, adjust "(L-ENTER)"
: add-arg  ( addr count -- )
  add-local 0x01_00 latest-pfa cell+ +!
;

: parse-loc-list  ( regcfa -- )
  >r begin
    parse-skip-line-comments
    parse-name ?dup
  while r@ execute repeat
  drop rdrop
;


..: forth:(exit-extender)  ( -- )
  latest-has-locals? if
    compile forth:(l-leave)
    locals-dp-temp-saved forth:(dp-temp) !
  endif
;..


: find-local  ( addr count -- addr count FALSE / idx TRUE )
  false >r
  2dup  1 >  swap c@ [char] : =  and if
    latest-has-locals? if  ( addr count | ret-flag )
      over 1+ over 1- locals-vocid find-word-in-voc
      if  ;; i found her!
        >r 2drop r> cfa->pfa @
        rdrop true >r
      endif
    endif
  endif
  r>
;

..: forth:(to-extender)  ( addr count FALSE -- addr count FALSE / TRUE )
  dup ifnot
    compiler:comp? if drop
      find-local if literal compile forth:(local!) true
      else false
      endif
    endif
  endif
;..


..: forth:(interpret-check-word)  ( addr count FALSE -- addr count FALSE / TRUE )
  dup ifnot
    compiler:comp? if drop
      find-local if literal compile forth:(local@) true
      else false
      endif
    endif
  endif
;..


previous definitions


: args:    ['] (locals-support):add-arg (locals-support):parse-loc-list ; immediate
: locals:  ['] (locals-support):add-local (locals-support):parse-loc-list ; immediate

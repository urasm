;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth/C
;; Copyright (C) 2023 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; slightly more complicated OO system than mini-oof
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

$INCLUDE-ONCE <uroof.f>


oof:class: MyList
  0 obj-value: item-ptr
  method: item@
  method: item!
  method: doit
end-class

oof:class: MyItem
  field: fld
  method: mth1
  method: mth2
end-class

MyItem MyList oof:bind-field: item-ptr

MyList oof:method: item@  ( -- addr )
  -> item-ptr @
;
\ MyList oof:method-cfa: item debug:(decompile-cfa)

MyList oof:method: item!  ( addr -- )
  to item-ptr
;

MyList oof:method: doit  ( -- )
  item-ptr mth1
  item-ptr mth2
  item-ptr mth1
  item-ptr mth2
;
\ MyList oof:method-cfa: doit debug:(decompile-cfa)


MyItem oof:method: mth1  ( -- )
  \ ." fld: " fld . cr
  ." === MTH1 ===" cr
  ."   self: " self . cr
  ."   fld: " fld . cr
  ."   fld: " -> fld @ . cr
  69 to fld
;

MyItem oof:method: mth2  ( -- )
  \ ." fld: " fld . cr
  ." === MTH2 ===" cr
  ."   self: " self . cr
  ."   fld: " fld . cr
  ."   fld: " -> fld @ . cr
  ."   class-name: " @class oof:name-of type cr
  ."   size-of: " @class oof:size-of . cr
  @class oof:parent-of ?dup if
  ."   parent: " oof:name-of type cr
  endif
  666 to fld
;

MyList oof:obj-value head
MyList oof:new to head

MyItem oof:new dup head item!
head item@ <> " fucka? (0)" ?error
head doit
0 head item!
\ head doit

MyItem oof:new dup head item!
head item@ <> " fucka? (1)" ?error
head doit

abort

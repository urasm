0 variable var

: test-0  ( n -- n )  noop 0 var +! noop ;
.( 0 var +!\n)
debug:decompile test-0

: test-1  ( n -- n )  noop 0 var -! noop ;
.( 0 var -!\n)
debug:decompile test-1

: test-2  ( n -- n )  noop 0 var ! noop ;
.( 0 var !\n)
debug:decompile test-2

: test-3  ( n -- n )  noop false var ! noop ;
.( false var !\n)
debug:decompile test-3

: test-4  ( n -- n )  noop true var ! noop ;
.( true var !\n)
debug:decompile test-4

: test-3.1  ( n -- n )  noop false var +! noop ;
.( false var +!\n)
debug:decompile test-3.1

: test-4.1  ( n -- n )  noop true var +! noop ;
.( true var +!\n)
debug:decompile test-4.1

: test-4.2  ( n -- n )  noop true var -! noop ;
.( true var -!\n)
debug:decompile test-4.2

: test-5  ( n -- n )  noop 3 var +! noop ;
.( 3 var +!\n)
debug:decompile test-5

: test-6  ( n -- n )  noop 4 var +! noop ;
.( 4 var +!\n)
debug:decompile test-6

: test-7  ( n -- n )  noop 4 var -! noop ;
.( 4 var -!\n)
debug:decompile test-7

: test-8  ( n -- n )  noop -4 var -! noop ;
.( -4 var -!\n)
debug:decompile test-8

: test-9  ( n -- n )  noop -4 var +! noop ;
.( -4 var +!\n)
debug:decompile test-9

: test-a  ( n -- n )  noop var @ noop ;
.( var @\n)
debug:decompile test-a

debug:dump-stack

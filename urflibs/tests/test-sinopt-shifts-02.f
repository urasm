0 value val

: test-0  ( n -- n )  -4 lshift ;
.( -4 lshift\n)
debug:decompile test-0

: test-1  ( n -- n )  -3 lshift ;
.( -3 lshift\n)
debug:decompile test-1

: test-2  ( n -- n )  -2 lshift ;
.( -2 lshift\n)
debug:decompile test-2

: test-3  ( n -- n )  -1 lshift ;
.( -1 lshift\n)
debug:decompile test-3

: test-4  ( n -- n )  0 lshift ;
.( 0 lshift\n)
debug:decompile test-4

: test-5  ( n -- n )  1 lshift ;
.( 1 lshift\n)
debug:decompile test-5

: test-6  ( n -- n )  2 lshift ;
.( 2 lshift\n)
debug:decompile test-6

: test-7  ( n -- n )  3 lshift ;
.( 3 lshift\n)
debug:decompile test-7

: test-8  ( n -- n )  4 lshift ;
.( 4 lshift\n)
debug:decompile test-8


debug:dump-stack

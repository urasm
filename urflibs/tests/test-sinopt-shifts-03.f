0 value val

: test-0  ( n -- n )  -4 rshift ;
.( -4 rshift\n)
debug:decompile test-0

: test-1  ( n -- n )  -3 rshift ;
.( -3 rshift\n)
debug:decompile test-1

: test-2  ( n -- n )  -2 rshift ;
.( -2 rshift\n)
debug:decompile test-2

: test-3  ( n -- n )  -1 rshift ;
.( -1 rshift\n)
debug:decompile test-3

: test-4  ( n -- n )  0 rshift ;
.( 0 rshift\n)
debug:decompile test-4

: test-5  ( n -- n )  1 rshift ;
.( 1 rshift\n)
debug:decompile test-5

: test-6  ( n -- n )  2 rshift ;
.( 2 rshift\n)
debug:decompile test-6

: test-7  ( n -- n )  3 rshift ;
.( 3 rshift\n)
debug:decompile test-7

: test-8  ( n -- n )  4 rshift ;
.( 4 rshift\n)
debug:decompile test-8


debug:dump-stack

0 value val

: testto-0  ( n -- n )  0 to val ;
.( 0 to val\n)
debug:decompile testto-0

: testto-1  ( n -- n )  1 to val ;
.( 1 to val\n)
debug:decompile testto-1

: testto-2  ( n -- n )  2 to val ;
.( 2 to val\n)
debug:decompile testto-2

: testto-3  ( n -- n )  -1 to val ;
.( -1 to val\n)
debug:decompile testto-3

: testto-4  ( n -- n )  false to val ;
.( false to val\n)
debug:decompile testto-4

: testto-5  ( n -- n )  true to val ;
.( true to val\n)
debug:decompile testto-5

: testto-6  ( n -- n )  0-to val ;
.( 0-to to val\n)
debug:decompile testto-6

: testto-7  ( n -- n )  +1-to val ;
.( +1-to to val\n)
debug:decompile testto-7

: testto-8  ( n -- n )  -1-to val ;
.( -1-to to val\n)
debug:decompile testto-8

: testto-9  ( n -- n )  1 +to val ;
.( 1 +to to val\n)
debug:decompile testto-9

: testto-a  ( n -- n )  1 -to val ;
.( 1 -to to val\n)
debug:decompile testto-a

: testto-b  ( n -- n )  2 +to val ;
.( 2 +to to val\n)
debug:decompile testto-b

: testto-c  ( n -- n )  2 -to val ;
.( 2 -to to val\n)
debug:decompile testto-c

: testto-d  ( n -- n )  3 +to val ;
.( 3 +to to val\n)
debug:decompile testto-d

: testto-e  ( n -- n )  3 -to val ;
.( 3 -to to val\n)
debug:decompile testto-e

: testto-f  ( n -- n )  3 +to val ;
.( 3 +to to val\n)
debug:decompile testto-f

: testto-g  ( n -- n )  3 -to val ;
.( 3 -to to val\n)
debug:decompile testto-g

: testto-h  ( n -- n )  4 +to val ;
.( 4 +to to val\n)
debug:decompile testto-h

: testto-i  ( n -- n )  4 -to val ;
.( 4 -to to val\n)
debug:decompile testto-i

: testto-j  ( n -- n )  4 +to val ;
.( 4 +to to val\n)
debug:decompile testto-j

: testto-k  ( n -- n )  4 -to val ;
.( 4 -to to val\n)
debug:decompile testto-k


debug:dump-stack

0 value val

: testto-0  ( n -- n )  false to val ;
.( false to val\n)
debug:decompile testto-0

: testto-1  ( n -- n )  true to val ;
.( true to val\n)
debug:decompile testto-1

: testto-2  ( n -- n )  false +to val ;
.( false +to val\n)
debug:decompile testto-2

: testto-3  ( n -- n )  true +to val ;
.( true +to val\n)
debug:decompile testto-3

: testto-4  ( n -- n )  false -to val ;
.( false -to val\n)
debug:decompile testto-4

: testto-5  ( n -- n )  true -to val ;
.( true -to val\n)
debug:decompile testto-5


debug:dump-stack

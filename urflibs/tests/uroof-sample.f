;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth/C
;; Copyright (C) 2023 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; slightly more complicated OO system than mini-oof
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

$INCLUDE-ONCE <uroof.f>


oof:class: MyClass \ [ extends: OtherClass ]
  field: fld
  method: mth1
  method: mth2
end-class \ [ MyClass ]

MyClass ." XCLS: " . cr

MyClass oof:method: mth1  ( -- )
  \ ." fld: " fld . cr
  ." === MTH1 ===" cr
  ." self: " self . cr
  ." fld: " fld . cr
  69 to fld
  \ inherited
;

MyClass oof:method: mth2  ( -- )
  \ ." fld: " fld . cr
  ." === MTH2 ===" cr
  ."   self: " self . cr
  ."   fld: " fld . cr
  ."   class-name: " @class oof:name-of type cr
  ."   size-of: " @class oof:size-of . cr
  @class oof:parent-of ?dup if
  ."   parent: " oof:name-of type cr
  endif
  666 to fld
  \ inherited
;

\ MyClass oof:method-cfa: mth debug:(decompile-cfa)

\ MyClass oof-internal:(class-dump-iht)
\ MyClass oof-internal:(class-dump-vmt)

MyClass oof:allot value inst

oof:class: MyClass2 extends: MyClass
  field: fld2
\  method: mth1
\  method: mth2
end-class \ [ MyClass ]

MyClass2 MyClass2 oof:isa? " fail0" ?not-error
MyClass2 MyClass oof:isa? " fail1" ?not-error
MyClass MyClass2 oof:isa? " fail2" ?error

\ MyClass oof-internal:(class-dump-iht)
\ MyClass2 oof-internal:(class-dump-iht)
\ MyClass oof-internal:(class-dump-vmt)
\ MyClass2 oof-internal:(class-dump-vmt)
\ abort

." inst: " inst . cr
inst oof:invoke-dyn: mth1
inst oof:invoke-dyn: mth2
inst oof:invoke-dyn: mth1
inst oof:invoke-dyn: mth2

MyClass2 oof:method: mth2  ( -- )
debug:dump-stack
  ." MC2: MTH2: ENTER!" cr
  ."  fld2: " fld2 . cr
  fld2 ?dup if 1+ else 42 endif to fld2
  inherited
  ."  fld2: " fld2 . cr
  ." MC2: MTH2: EXIT!" cr
;

\ MyClass2 oof:method-cfa: mth2  debug:(decompile-cfa)

MyClass2 oof:allot value inst2

.( ###############\n)
." inst2: " inst2 . cr
inst2 oof:invoke-dyn: mth1
inst2 oof:invoke-dyn: mth2
inst2 oof:invoke-dyn: mth1
inst2 oof:invoke-dyn: mth2

MyClass oof:obj-value object

.( ---------------------\n)
inst to object
object mth2

inst2 to object
object mth2

." inst2 fld: " object fld . cr

: goo  ( -- )
  inst2 oof:invoke-dyn: mth2
;
debug:decompile goo
goo


: boo  ( -- )
  object mth2
;
debug:decompile boo
boo


\ abort
bye

: test-inc-2  ( n -- n )  noop swap 2 + swap noop ;
debug:decompile test-inc-2

: test-inc-4  ( n -- n )  noop swap 4 + swap noop ;
debug:decompile test-inc-4

: test-inc-8  ( n -- n )  noop swap 8 + swap noop ;
debug:decompile test-inc-8

: test-dec-1  ( n -- n )  noop swap 1 - swap noop ;
debug:decompile test-dec-1

: test-dec-2  ( n -- n )  noop swap 2 - swap noop ;
debug:decompile test-dec-2

: test-dec-4  ( n -- n )  noop swap 4 - swap noop ;
debug:decompile test-dec-4

: test-dec-8  ( n -- n )  noop swap 8 - swap noop ;
debug:decompile test-dec-8

debug:dump-stack

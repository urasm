;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; and now for something completely different...
;; UrForth/C Forth Engine!
;; Copyright (C) 2023 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; simple REPL
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

$include-once <linore.f>
$IF $DEBUGGER
$include-once <debugger.f>
$ENDIF


vocabulary ufo-repl-support
also-defs: ufo-repl-support

false value editor-active?
0 value restart-word-cfa

: .ok  ( -- )
  compiler:exec? if
    ." ok"
    depth ?dup if ."  (" 0 .r ." )" endif
  else ."  more..."
  endif cr
  flush-emit tty:raw-flush
;

: interp-done  ( -- )
  .ok
  rp0! restart-word-cfa execute-tail
;

: draw-prompt  ( -- )
  " \r\e[0;1;5m>\e[0m" tty:raw-type
;


..: linore:on-before-draw  ( FALSE -- FALSE / TRUE )
  editor-active? if
    dup ifnot draw-prompt endif
  endif
;..


;; do not allow debugger to abort on bad forth word
..: forth:(interpret-word-not-found-post)  ( addr count -- addr count )
  space xtype ." ? -- wut?!" cr
  compiler:exec! sp0!
  interp-done
;..


: repl-loop
  tty:tty? " cannot run REPL on non-TTY" ?not-error
  begin
    linore:reset
    true to ufo-repl-support:editor-active?
    1 to linore:start-x
    tty:size drop to linore:draw-width
    flush-emit linore:edit-line
    false to ufo-repl-support:editor-active?
    linore:accept-result linore:result-^d = if break endif
    linore:accept-result +if
      ['] ufo-repl-support:interp-done (user-interpret-next-line) !
      0 linore:line + c!  ;; finish TIB with 0 byte
      linore:line drop tib ! >in 0!  ;; setup TIB
      cr tty:raw-flush
      interpret
      ufo-repl-support:interp-done
    endif
  again
;

' repl-loop to ufo-repl-support:restart-word-cfa

prev-defs


: ufo-run-repl  ( -- )
  sp0! rp0! ['] ufo-repl-support:repl-loop execute-tail
;

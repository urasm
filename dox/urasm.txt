unsorted notes on UrAsm commands and quirks


EQU vs '='
==========
EQU defines unique label (which cannot be redefined)

'=' assigns value to label (creating new label if necessary, or
redefining the existing one)


labels
======
if label starts with '@@', it doesn't reset local labels.

if label starts with '.', it is a local label; all local
labels will be forgotten after new global label.


displaying expressions while assembling
=======================================

DISPLAY ...
  pring args on the second pass; can print strings and perform math
DISPLAY0 ...
  pring args on the first pass; can print strings and perform math
DISPLAYA ...
  pring args on each pass; can print strings and perform math
DISPHEX ...
DISPHEX0 ...
DISPHEXA ...
  the same as "DISPLAY*", but prins all numbers as hex values


define output format
====================
DEFFMT fmt
  SNA -- .sna, no autorun
  RUNSNA -- .sna, with autorun
  TAP, TAPE -- .tap, no autorun
  RUNTAP, RUNTAPE -- .tap, with autorun (i.e. with BASIC loader)
  BIN, RAW -- just a raw binary (or several binaries, one for each ORG)
  DMB, RUNDMB -- DMB, DMB with autorun
  NONE, NOTHING -- no output file

for tapes with autorun you can use "CLR" command to define BASIC "CLEAR".


macros
======
 MACRO downxx  regpair, asret=0, asjp=0
  inc  =regpair[0]
  ld   a,=regpair[0]
  and  #07
 IF =asret
  ret  nz
 ELSEIF =asjp
  jp   nz,..done
 ELSE
  jr   nz,..done
 ENDIF
  ld   a,=regpair[1]
  add  a,32
  ld   =regpair[1],a
 IF =asret
  ret  c
 ELSEIF =asjp
  jp   c,..done
 ELSE
  jr   c,..done
 ENDIF
  ld   a,=regpair[0]
  sub  8
  ld   =regpair[0],a
 IF =asret
  ret
 ELSE
..done:
 ENDIF
 ENDM


  downxx de
  nop
  downxx hl,1
  nop
  downxx bc,,1
  nop

"..label:" is label local to macro.

to use `regpair` as full register name: `inc =regpair[]`.

default arg can be empty, like `MACRO xxx arg=`. to test it, use:
  IF marg2str(=arg) != ""
    ...
  ENDIF


ORG
===
ORG addr
  set current logical and physical destination address


DISPlacements
=============
DISP addr
ENDDISP
 or
PHASE addr
DEPHASE | UNPHASE
  physical code location is not changed, but logical code location set to "addr"


ALIGN
=====
ALIGN size
  aligns PC.
  cannot be used in DISP


DISPlacement ALIGN
==================
DISPALIGN size
 or
PHASEALIGN size
  ALIGN, but for "DISP" blocks.


code ENTry point
================
ENT addr
  sets code entry point (for formats with autostart).


BASIC CLEAR
===========
CLR addr
  sets CLEAR addess for BASIC tap loader.
  default value: 24999


RESERVE address space
===========================================
RESERVE addr, size
  will automaticaly skip the given area (and insert all necessary
  jumps) if assembled code will overlap.

  WARNING! this is not properly tested, and it will happily try
           to insert jumps even in def* data!


INCLUDE source file
===================
INCLUDE "name"
INCLUDE <name>
  includes source file. for simple quotes, searching starts from the
  directory with the file we are processing right now (same as in C).
  angle brackets means "search in system library dir".
  if file name starts with "?", this is "soft include". if file could
  not be found, the include will be silently ignored. you can separate
  "?" from the file name with spaces.


INClude BINary file
===================
INCBIN name [,maxlen]
  include binary file "as-is", possibly with max length limit.


MODULEs
=======
MODULE mname
ENDMODULE [mname]
  all non-local labels between those will be "exported" to the other
  code as "mname.labelname".


code DUPlication
================
DUP cnt [,itervarname [,initval [,increment]]]
 ...
EDUP
  duplicates given code fragment "cnt" times. nested DUPs are allowed.
  if `itervarname` is specified, it will contain iteration counter.
  note that this var is local for DUP block, and will be destroyed
  when DUP complete (i.e. it won't go to ref, and can shadow other
  labels).

  you can specily initial value (default is 0), and increment
  (default is 1) for iteration variable.

  you can use REPT/ENDR as synonyms.


conditional compilation
=======================
IF <condexpr>
IFX <condexpr>
ELSE
ELSEIF <condexpr>
ELSEIFX <condexpr>
ENDIF
  for "IF", all labels in "condexpr" must be defined.
  for "IFX", any undefined label in "condexpr" turns it to 0 (false).


add value to data
=================
DEFINCR val
  will add "val" to all data defined via:
    DB, DW, DR, DS,
    and to *NUMBERS* (NOT strings) in DM, DZ, DX � DC
  it is possible to use complex expression to modify the data, by using string.
  all operations are done on the properly sized value.
  operators:
    shr, <count>
    shl, <count>
    sar, <count>  -- arith shift, extend bit 7
    sal, <count>  -- arith shift, extend bit 0
    ror, <count>
    rol, <count>
    and, <byte>
    xor, <byte>
    or, <byte>
    add, <byte>
    sub, <byte>
  example:
    DEFINCR "expr", "shl",3, "xor",0x80, "or",1


DEFine Bytes
============
DEFB or DB:
  define bytes

DEFine Words
============
DEFW or DW:
  define 16-bit little-endian words

DEFine Reversed Words
=====================
DEFR:
  define 16-bit big-endian words


DEFine repeated values
======================
DEFS:
DEFS <rep[,value]>+
  define repeated values


DEFine strings
==============
DEFM:
  define string data


DEFine asciiZ strings
=====================
DEFZ:
  define string data, automatically adding zero byte after each one


DEFine high-bit-terminated string
=================================
DEFX:
  define string data, automatically set highest bit on the last char of each one


DEFine byte-Counted strings
===========================
DEFC:
  define string data, automatically insert byte length before each one


known functions in math expressions
===================================
  defined(xxx) -- is label "xxx" value defined?
  known(xxx) -- is label "xxx" seen? (its value may be still undefined)
  aligned256(xxx) -- xxx%256 == 0
  align(xxx[, what]) -- align xxx by what, default what == 256
  sameseg(x0, x1) -- x0/256 == x1/256
  low(xxx) -- low byte of xxx
  high(xxx) -- high byte of xxx
  abs(xxx) -- absolute value of xxx
  bswap(xxx) -- swap bytes in 16-bit word

  scraddr8x8(x, y, base=#4000) -- calculate screen address; coords are in chars
  scraddr8x1(x, y, base=#4000) -- calculate screen address; x coord is char, y coord is pixel
  scrattr8x8(x, y, base=#4000) -- calculate attribute address; coords are in chars

  marg2str(argname) -- get macro argument name as string value; always lowercased
    this is to use macro arguments in comparisons, like this:
      IF marg2str(=regpair) == "hl"
        ...
      ENDIF
    note the '=' prefix. result of `marg2str()` will be lowercased.

  strlen(str) -- string length


you can use "[]" instead of "()" in math expressions.


numeric literals
================
start numeric literal with "0" to get octal.
you can end hex number with "h" instead of using prefix (but this is not recommended).

allowed number prefixes for hex numbers:
  0xNN
  #NN
  $NN
  &HNN

allowed number prefixes for bin numbers:
  0bNN
  %nn
  &BNN

allowed number prefixes for octal numbers:
  0NN
  0oNN
  &ONN

allowed number prefixes for decimal numbers:
  [1-9]NN
  0dNN
  &DNN


two-char string literals as numbers
===================================
two-char string literals (in single or double quotes) can be used
as numeric literals. the difference is:

  "ab" -- 'a' will be in low (first) byte
  'ab' -- 'a' will be in high (second) byte


EX AF,AF'
=========
you can use either "AF'" or "AFX" form for this.


colons for commands
===================
you can separate multiple commands with colons:

  ld a,42 : ld b,69


math priorities (WARNING! NOT C!)
=================================
   ! (logical not)
   ~ (bitwise not)
  ------------
   <<
   >> (signed shift)
   >>> (unsigned shift)
  ------------
   &
  ------------
   |
   ^
  ------------
   *
   /
   %
  ------------
   +
   -
  ------------
   <
   >
   = (equality test, the same as '==')
   ==
   !=
   <> (inequality test, the same as '!=')
   <=
   >=
  ------------
   &&
  ------------
   ||
  ------------

  logical '&&' � '||' are short-circuited.


$MATHMODE
=========
$MATHMODE <old|new>

new is more C-like, and the current default.
directive is here for compatibility with the old code.


$CASTRATES
==========
$CASTRATES <yes|no>

allow hex numbers like "&abc"? note that turning this on
will turn off "&b" binary prefix.


$REFOPT
=======
$REFOPT <default|alllabels|noupcase>

what to output to label reference file? default is "alllabels".
"noupcase" means "omit all-uppercase labels".


$PRINTF
$PRINTF0
$PRINTFA
========
$PRINTF "fmt" [,args...]

very simple printf. automatically advances the line.
"printf" works only on the second pass.
"printf0" works only on the first pass.
"printfa" works on all passes.


$ERROR
======
$ERROR "fmt" [,args...]

prints "user error" message, and aborts compilation.


$WARNING
========
$WARNING "fmt" [,args...]

prints "user warning" message, but doesn't abort compilation.


adding external data files to the resulting image
=================================================
DATABIN "name"[,len[,offset[,codeaddr]]]
DATABIN "name|dfname"[,len[,offset[,codeaddr]]]

load external file with the given name, and write it to the
resulting image (tap, scl, etc.). loader will NOT automatically
load it. to autoload it, use CODEBIN.

WARNING! all such blocks will be appended (and loaded) *AFTER*
         the normal memory blocks.


writing part of the code
========================
$SAVECODE "filename",addr,size [,ldaddr [,options...]]

options:
  wipe -- wipe saved area (mark it as unused)
  data -- save as data (do not autoload it)

this can be used to assemble and write code for additional 128K
pages.

WARNING! all used code labels must be known, because the code will
be copied "as is", and unknown labels won't be patched!

WARNING! all such blocks will be appended (and loaded) *AFTER*
         the normal memory blocks.


structures
==========
STRUCT <flags> structname [,init_ofs]
...
ENDS
STRUCT <flags> extend structname [,init_ofs]
...
ENDS
STRUCT <flags> structname extend parentstructname [,init_ofs]
...
ENDS

you can extend existing struct with new fields using the second form.
this will not create a new struct, but simply append fields to the existing
one (and fix `._sizeof`).

the third form is used to "subclass" existing structure. i.e. to create a
new structure with a new name, and with all fields from the parent, followed
by new fields.

optional `init_ofs` can be used to set initial offset of the declared fields.
for extensions, it sets the initial offset of the first extension field.

field definitions:
  fldname <type>

types are:
  byte
  word
  dword  (4 bytes)
  address  (2 bytes, same as "word")
  label
  defs <size>
  ds <size>

for non-label and non-ds fields it is possible to specify an initial value.
  field word = 0x29a

each struct will automatically get "structname._sizeof" label.

<flags> is a comma-separated list of flags in square brackets. optional.
currently defined flags:
  `zero_fill`: fill uinitialised struct fields with zeroes.


create anonymous struct
  structname [init]

create named struct
  label: structname [init]

for named structs, labels with fields addresses will be automatically
created. i.e. "boo: goo" will create labels "boo.fldname", etc.
note that label must be on the same line in this case.

struct initialisation consists of "name = value" pairs, delimited by
commas. all pairs must be on the same line. to create multiline inits,
use culry brackets:

structname {
  field0 = 0x29a, field1 = 666
  field1 = 69
}

opening bracket must be last at the line, and closing bracket must be
the only non-blank char in the line.

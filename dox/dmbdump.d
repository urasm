#!/usr/bin/env rdmd
import std.stdio;

import iv.stream;


void dumpDMB (File fl) {
  auto sign = fl.readNum!uint();
  if (sign != 0x30424D44u && sign != 0x31424D44u) throw new Exception("invalid signature");
  bool v1 = (((sign>>24)&0xff) == 0x31);
  auto pc = fl.readNum!ushort();
  if (pc) writefln("run from: #%04x (%s)", pc, pc);
  if (v1) {
    auto clr = fl.readNum!ushort();
    if (clr) writefln("CLEAR #%04x (%s)", clr, clr);
  }
  auto count = fl.readNum!ushort();
  while (count--) {
    auto size = fl.readNum!ushort();
    auto addr = fl.readNum!ushort();
    if (addr != 0) {
      writefln("#%04x %s  (%s)", addr, size, addr);
    } else {
      addr = fl.readNum!ushort();
      auto page = fl.readNum!ubyte();
      writefln("%02s #%04x %s  (%s)", page, addr, size, addr);
    }
    fl.seek(size, SEEK_CUR);
  }
}


void main (string[] args) {
  foreach (auto fn; args[1..$]) {
    writeln("=== ", fn, " ===");
    dumpDMB(File(fn));
  }
}

#ifndef _CVT_H_
#define _CVT_H_

#include <stdio.h>
#include <stdlib.h>

#include "libhobeta/hobeta.h"

#ifdef __cplusplus
extern "C" {
#endif


extern int locaseTokens; // default: yet


typedef struct CvtMethods {
  struct CvtMethods *next;
  const char *name;
  const char *fmtname;
  // 'fi' must me HoBeta file
  int (*extract) (FILE *fo, FILE *fi);
  const int (*detect) (FILE *fi, const char *fname, const HOBHeader *hdr); // !0: ok
} CvtMethods;


extern void cvtAdd (CvtMethods *mt);
extern void *loadWholeFile (FILE *fl, int *size);


#ifdef __cplusplus
}
#endif
#endif

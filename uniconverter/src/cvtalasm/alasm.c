#include "alasm.h"

#include <ctype.h>
#include <string.h>


static const char *mnemtkn[]  = {
  "INCLUDE", "INCBIN", "MACRO", "LOCAL", "RLCA", "RRCA", "HALT", "CALL", "PUSH", "RETN", "RETI", "DJNZ", "OUTI",
  "OUTD", "LDIR", "CPIR", "INIR", "OTIR", "LDDR", "CPDR", "INDR", "OTDR", "DD", "DEFB", "DEFW", "DEFS", "DISP",
  "ENDM", "EDUP", "ENDL", "MAIN", "ELSE", "DISPLAY", "EX AF,AFX", "DB", "DW", "DS", "NOP", "INC", "DEC", "RLA", "RRA",
  "DAA", "CPL", "SCF", "CCF", "ADD", "ADC", "SUB", "SBC", "AND", "XOR", "RET", "POP", "RST", "EXX", "RLC", "RRC",
  "SLA", "SRA", "SLI", "SRL", "BIT", "RES", "SET", "OUT", "NEG", "RRD", "RLD", "LDI", "CPI", "INI", "LDD", "CPD",
  "IND", "ORG", "EQU", "ENT", "INF", "DUP", "IFN", "REPEAT", "UNTIL0", "IF0", "LD", "JR", "JP", "OR", "CP", "EX",
  "DI", "EI", "IN", "RL", "RR", "IM", "ENDIF", "EXD", "JNZ", "JZ", "JNC", "JC", "RUN"
};
static const char *regstkn1[] = {"(BC)", "(DE)", "(HL)", "(SP)", "(IX)", "(IY)"};
static const char *regstkn2[] = {"(C)", "(IX", "(IY", "AFX"};
static const char *regstkn3[] = {
  "BC", "DE", "HL", "AF", "IX", "IY", "SP", "NZ", "NC", "PO", "PE", "HX", "LX", "HY", "LY", "B", "C", "D", "E", "H",
  "L", "A", "P", "M", "Z", "R", "I"
};


#define GETBYTE(_dst)  do { \
  if (--buflen < 0) { fprintf(stderr, "OUT of data...\n"); /*free(buf); return -1;*/ (_dst) = 0; break; } \
  (_dst) = *p++; \
} while (0)


#define PUTCH(_ch)  do { \
  uint8_t _xch = (uint8_t)(_ch); \
  if (fwrite(&_xch, 1, 1, fo) != 1) { fprintf(stderr, "ERROR writing...\n"); free(buf); return -1; } \
} while (0)


#define PUTCH_TK(_ch)  do { \
  uint8_t _xch = (uint8_t)(_ch); \
  if (locaseTokens && _xch >= 'A' && _xch <= 'Z') _xch = _xch-'A'+'a'; \
  if (fwrite(&_xch, 1, 1, fo) != 1) { fprintf(stderr, "ERROR writing...\n"); free(buf); return -1; } \
} while (0)


static int extract (FILE *fo, FILE *fi) {
  int buflen;
  uint8_t *buf;
  const uint8_t *p;
  //
  if ((buf = loadWholeFile(fi, &buflen)) == NULL) {
    fprintf(stderr, "ERROR loading...\n");
    return -1;
  }
  if ((buflen -= 0x39+8+16) < 0) {
    free(buf);
    fprintf(stderr, "BAD buffer size...\n");
    return -1;
  }
  p = buf+0x39+8+16;
  //
  for (;;) {
    uint8_t len;
    int comment = 0;
    int string = 0;
    int russian = 0;
    int tab_used = 0;
    int first_token = 1;
    size_t pos = 0;
    //
    GETBYTE(len);
    if (len == 0) break;
    for (int j = 0; j < len-1; ++j) {
      uint8_t b;
      //
      GETBYTE(b);
      if (b == 0xff) continue;
      if (comment || russian) {
        if (b < 0x20) continue; // skip tabs
        PUTCH(b);
        ++pos;
        continue;
      }
      if (string) {
        if (b < 0x20) continue; // skip tabs
        if (b == '"') string = 0;
        PUTCH(b);
        ++pos;
        continue;
      }
      if (b == ';') comment = 1;  // comment by EOL
      else if (b == '"') string = 1; // sting by '"'
      else if (b == 0x10) {
        // russian by EOL
        russian = 1;
        continue;
      }
      if (b < 0x10) {
        // tabs
        for (int k = 0; k < b; ++k) PUTCH(' ');
        pos += b;
        tab_used = 1;
        continue;
      }
      if (b >= 0x80) {
        if (first_token) {
          first_token = 0;
          if (pos < 8 && !tab_used) {
            for(size_t k = 0; k < 8-pos; ++k) PUTCH(' ');
          }
          pos += 8-pos;
          if (b <= 0xe6) {
            const char *from = mnemtkn[b-0x80];
            int len = strlen(from);
            pos += len;
            while (*from) PUTCH_TK(*from++);
            while (len++ < 5) PUTCH(' ');
          }
        } else {
          if (b >= 0x9f && b <= 0xa4) {
            const char *from = regstkn1[b-0x9f];
            pos += strlen(from);
            while (*from) PUTCH_TK(*from++);
          } else if (b >= 0xd0 && b <= 0xd3) {
            const char *from = regstkn2[b-0xd0];
            pos += strlen(from);
            while (*from) PUTCH_TK(*from++);
          } else if (b >= 0xe0 && b <= 0xfa) {
            const char *from = regstkn3[b-0xe0];
            pos += strlen(from);
            while (*from) PUTCH_TK(*from++);
          }
        }
        continue;
      }
      PUTCH(b);
      ++pos;
    }
    PUTCH('\n');
  }
  free(buf);
  return 0;
}


static const int detect (FILE *fi, const char *fname, const HOBHeader *hdr) {
  static const uint8_t sign[8] = {0xF3,0x76,0xC7,0xDD,0xFD,0xED,0xB0,0xD9};
  uint8_t buf[8];
  //
  fseek(fi, 0x39, SEEK_SET);
  if (fread(buf, sizeof(sign), 1, fi) != 1) return 0;
  if (memcmp(buf, sign, sizeof(sign)) != 0) return 0;
  return 1;
}


////////////////////////////////////////////////////////////////////////////////
static CvtMethods mtlist;


CvtMethods *cvt_alasm (void) {
  mtlist.name = "Alasm assembler";
  mtlist.fmtname = "alasm";
  mtlist.extract = extract;
  mtlist.detect = detect;
  return &mtlist;
};

#include "zxasm.h"

#include <ctype.h>
#include <string.h>


static const char *token[] = {
"ld",    "ex",      "im",     "rst",     "ret",    "add",     "adc",     "sub",   "sbc",  "and",  "xor",   "or",    "cp",      "push",   "pop",     "inc",
"dec",   "in",      "out",    "jp",      "call",   "jr",      "djnz",    "rlc",   "rrc",  "rl",   "rr",    "sla",   "sra",     "sli",    "srl",     "bit",
"res",   "set",     "nop",    "halt",    "di",     "ei",      "rlca",    "rla",   "rrca", "rra",  "exx",   "daa",   "cpl",     "ccf",    "scf",     "ldi",
"ldir",  "ldd",     "lddr",   "cpi",     "cpir",   "cpd",     "cpdr",    "neg",   "inf",  "ini",  "inir",  "ind",   "indr",    "outi",   "otir",    "outd",
"otdr",  "reti",    "retn",   "rld",     "rrd",    "org",     "equ",     "db",    "dw",   "ds",   "defb",  "defw",  "defs",    "insert", "include", "if",
"ifdef", "ifndef",  "ifused", "ifnused", "else",   "endif",   "make",    "b",     "c",    "d",    "e",     "h",     "l",       "(hl)",   "a",       "xh",
"xl",    "yh",      "yl",     "(ix",     "(iy",    "(bc)",    "(de)",    "i",     "r",    "af",   "bc",    "de",    "hl",      "ix",     "iy",      "sp",
"(sp)",  "af'",     "(c)",    "nz",      "z",      "nc",      "c",       "po",    "pe",   "p",    "m",     "phase", "unphase", "dc",     "ent",     "repeat",
"endr",  "loadtab", "macro",  "endm",    "create", "makelab", "saveobj", "exitm", "ifp",  "exa",  "retz",  "retnz", "retc",    "retnc",  "retm",    "retp",
"retpo", "retpe",   "jpz",    "jpnz",    "jpc",    "jpnc",    "jpm",     "jpp",   "jppo", "jppe", "callz", "callc", "callm",   "callpe", "callnz",  "callnc",
"callp", "callpo",  "jrz",    "jrnz",    "jrc",    "jrnc"
};


#define GETBYTE(_dst)  do { \
  if (--buflen < 0) { free(buf); return -1; } \
  (_dst) = *from++; \
} while (0)


#define PUTCH(_ch)  do { \
  uint8_t _xch = (uint8_t)(_ch); \
  if (fwrite(&_xch, 1, 1, fo) != 1) { free(buf); return -1; } \
} while (0)


#define PUTCH_TK(_ch)  do { \
  uint8_t _xch = (uint8_t)(_ch); \
  if (!locaseTokens && _xch >= 'a' && _xch <= 'z') _xch = _xch-'a'+'A'; \
  if (fwrite(&_xch, 1, 1, fo) != 1) { free(buf); return -1; } \
} while (0)


static int extract (FILE *fo, FILE *fi) {
  int buflen;
  uint8_t *buf;
  const uint8_t *from;
  //
  if ((buf = loadWholeFile(fi, &buflen)) == NULL) return -1;
  if ((buflen -= sizeof(HOBHeader)) < 1) { free(buf); return -1; }
  //
  from = buf+sizeof(HOBHeader);
  while (buflen-- > 0) {
    uint8_t code;
    //
    if (*from == 0x0D) {
      PUTCH('\n');
      ++from;
      continue;
    }
    if (*from < 2 || *from > 6 || buflen < 1) {
      PUTCH(*from++);
      continue;
    }
    code = *(from+1);
    if (*from == 0x06) {
      code &= 0x7F;
      for (int j=0; j < code; ++j) PUTCH(0x20);
    } else {
      if (code < 0x20 || code > 0xC5) {
        PUTCH(*from++);
        continue;
      } else {
        const char *ptr = token[code-0x20];
        if ((*from)&1) PUTCH_TK(toupper(*ptr++)); else PUTCH_TK(*ptr++);
        while (*ptr) PUTCH_TK(*ptr++);
        if (!((*from)&2)) PUTCH(' ');
      }
    }
    --buflen;
    from += 2;
  }
  free(buf);
  return 0;
}


static const int detect (FILE *fi, const char *fname, const HOBHeader *hdr) {
  if (!(hdr->type == 'a' && hdr->type1 == 's' && hdr->type2 == 'm') &&
      !(hdr->type == 'a' && hdr->type1 == ' ' && hdr->type2 == ' ') &&
      !(hdr->type == 'z' && hdr->type1 == 'a' && hdr->type2 == 's') &&
      !(hdr->type == 'C' && (hdr->start == 35151)) &&
      !(hdr->type == 'a' && (hdr->start == 28001))) return 0;
  return 1;
}


////////////////////////////////////////////////////////////////////////////////
static CvtMethods mtlist;


CvtMethods *cvt_zxasm (void) {
  mtlist.name = "ZXAsm assembler";
  mtlist.fmtname = "zxasm";
  mtlist.extract = extract;
  mtlist.detect = detect;
  return &mtlist;
};

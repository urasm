#include "masm.h"

#include <ctype.h>
#include <string.h>


static const char *token[] = {
"A",      "B" ,      "C",    "D",     "E",    "H",      "L",       "I",    "R",    "XH",     "XL",   "YH",      "YL",       "IX",   "IY",    "AF'",
"AF",     "HL",      "DE",   "BC",    "M",    "NC",     "NV",      "NZ",   "P",    "PE",     "PO",   "V",       "Z",        "SP",   "{ ",    "ORG ",
"PHASE ", "UNPHASE", "AND ", "ADC ",  "SBC ", "ADD ",   "SUB ",    "XOR ", "OR ",  "CP ",    "LD ",  "IM ",     "RST ",     "EI",   "DI",    "EXX",
"EXA",    "INF",     "LDIR", "LDDR",  "OTIR", "OTDR",   "OUTI",    "OUTD", "RETI", "RETN",   "INIR", "INDR",    "CPIR",     "CPDR", "NEG",   "CPD",
"CPI",    "IND",     "INI",  "LDD",   "LDI",  "CCF",    "CPL",     "DAA",  "HALT", "NOP",    "RLA",  "RLCA",    "RRA",      "RRCA", "SCF",   "RLD",
"RRD",    "EX ",     "RET",  "CALL ", "JP ",  "PUSH ",  "POP ",    "INC ", "DEC ", "OUT ",   "IN ",  "DJNZ ",   "JR ",      "BIT ", "RLC ",  "RRC ",
"RL ",    "RR ",     "SLA ", "SRA ",  "SLI ", "SRL ",   "RES ",    "SET ", "EQU ", "BEGIN ", "END",  "INCBIN ", "INCLUDE ", "DB ",  "DEFB ", "DEFS ",
"DEFW ",  "DS ",     "DW ",  "DOWN",  "UP",   "SYSTEM", "STOPKEY", "?",    "?",    "?",      "?",    "?",       "?",        "?",    "?",     "?"
};


#define GETBYTE(_dst)  do { \
  if (--buflen < 0) { free(buf); return -1; } \
  (_dst) = *from++; \
} while (0)


#define PUTCH(_ch)  do { \
  uint8_t _xch = (uint8_t)(_ch); \
  if (fwrite(&_xch, 1, 1, fo) != 1) { free(buf); return -1; } \
} while (0)


#define PUTCH_TK(_ch)  do { \
  uint8_t _xch = (uint8_t)(_ch); \
  if (locaseTokens && _xch >= 'A' && _xch <= 'Z') _xch = _xch-'A'+'a'; \
  if (fwrite(&_xch, 1, 1, fo) != 1) { free(buf); return -1; } \
} while (0)


static int extract (FILE *fo, FILE *fi) {
  int buflen;
  uint8_t *buf;
  const uint8_t *from;
  //
  if ((buf = loadWholeFile(fi, &buflen)) == NULL) return -1;
  if ((buflen -= sizeof(HOBHeader)) < 0) { free(buf); return -1; }
  //
  from = buf+sizeof(HOBHeader);
  while (buflen > 0) {
    uint8_t len;
    //
    GETBYTE(len);
    if (len == 0xFF) break;
    if (!len) {
      PUTCH('\n');
      continue;
    }
    for (int i = 0; i < len; ++i) {
      if (buflen < 1) break;
      if (*from < 0x20) {
        if (*from == 0x0A && i != len-1) {
          uint8_t spc;
          ++from; --buflen;
          ++i;
          GETBYTE(spc);
          for (int j = 0; j < spc; ++j) PUTCH(' ');
        } else {
          PUTCH(*from);
        }
      } else if (*from < 0x80) {
        PUTCH(*from);
      } else {
        const char *ptr = token[*from-0x80];
        while (*ptr) PUTCH_TK(*ptr++);
      }
      ++from; --buflen;
      if (buflen < 1) break;
    }
    PUTCH('\n');
    if (buflen < 1) break;
    if (*from != len) break;
    ++from; --buflen;
  }
  free(buf);
  return 0;
}


static const int detect (FILE *fi, const char *fname, const HOBHeader *hdr) {
  if (hdr->type == 'a' &&
      (hdr->start == 38667 || hdr->start == 38821 || hdr->start == 38663 || hdr->start == 38701)) return 1;
  return 0;
}


////////////////////////////////////////////////////////////////////////////////
static CvtMethods mtlist;


CvtMethods *cvt_masm (void) {
  mtlist.name = "Masm assembler";
  mtlist.fmtname = "masm";
  mtlist.extract = extract;
  mtlist.detect = detect;
  return &mtlist;
};

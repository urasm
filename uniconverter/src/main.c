#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "libhobeta/hobeta.h"
#include "cvt.h"

#include "cvtstorm/storm.h"
#include "cvtalasm/alasm.h"
#include "cvtmasm/masm.h"
#include "cvtzxasm/zxasm.h"
#include "cvttasm/tasm.h"
#include "cvtxas/xas.h"


// ////////////////////////////////////////////////////////////////////////// //
int locaseTokens = 1;


//==========================================================================
//
//  loadWholeFile
//
//==========================================================================
void *loadWholeFile (FILE *fl, int *size) {
  int bufsize = 65536, buflen = 0;
  uint8_t *buf = (uint8_t *)calloc(1, bufsize);
  //
  for (;;) {
    size_t rd;
    //
    if (buflen+4096 > bufsize) {
      uint8_t *b1;
      int newsz = bufsize+65536;
      //
      if (newsz > 1024*1024*4) { free(buf); return NULL; }
      if ((b1 = (uint8_t *)realloc(buf, newsz)) == NULL) { free(buf); return NULL; }
      memset(b1+bufsize, 0, newsz-bufsize);
      buf = b1;
    }
    if ((rd = fread(buf+buflen, 1, bufsize-buflen, fl)) < 1) break;
    buflen += rd;
  }
  if (buflen < (int)sizeof(HOBHeader) || (buflen-(int)sizeof(HOBHeader))&0xff) { free(buf); return NULL; }
  if (size) *size = buflen;
  return (void *)buf;
}


// ////////////////////////////////////////////////////////////////////////// //
static CvtMethods *cvtList = NULL;


//==========================================================================
//
//  cvtAdd
//
//==========================================================================
void cvtAdd (CvtMethods *mt) {
  //printf("registering converter: '%s'...\n", mt->name);
  mt->next = cvtList;
  cvtList = mt;
}


//==========================================================================
//
//  strCopyChangeExt
//
//==========================================================================
static char *strCopyChangeExt (const char *str, const char *ext) {
  char *res, *e, *dd;
  //
  res = (char *)calloc(strlen(str)+strlen(ext)+8, 1);
  strcpy(res, str);
  //
  if ((e = strrchr(res, '.')) != NULL) {
    if ((dd = strrchr(res, '/')) != NULL && e < dd) {
      strcat(res, ext);
    } else {
      strcpy(e, ext);
    }
  } else {
    strcat(res, ext);
  }
  return res;
}


//==========================================================================
//
//  registerConverters
//
//==========================================================================
static void registerConverters (void) {
  cvtAdd(cvt_masm());
  cvtAdd(cvt_xas());
  cvtAdd(cvt_tasm());
  cvtAdd(cvt_zxasm());
  cvtAdd(cvt_storm());
  cvtAdd(cvt_alasm());
}


//==========================================================================
//
//  help
//
//==========================================================================
static void help (const char *prgname) {
  fprintf(stderr, "usage: %s [options] infile [outfile]\n", prgname);
  fprintf(stderr,
    "WARNING! files must be in HoBeta format!\n"
    "options:\n"
    "  --upcase    -- use upcase tokens\n"
    "  --overwrite -- silently overwrite existing output file\n"
    "known converters:\n"
    "");
  for (CvtMethods *cvt = cvtList; cvt != NULL; cvt = cvt->next) fprintf(stderr, "  %s\n", cvt->name);
  exit(0);
}


// ////////////////////////////////////////////////////////////////////////// //
int main (int argc, char *argv[]) {
  HOBHeader hdr;
  FILE *fi, *fo;
  const char *inname = NULL;
  char *outname = NULL;
  int overwrite = 0;
  CvtMethods *cvt;

  registerConverters();

  int nomoreargs = 0;
  for (int f = 1; f < argc; ++f) {
    const char *arg = argv[f];
    if (!arg || !arg[0]) continue;
    if (!nomoreargs && arg[0] == '-') {
      if (strcmp(arg, "--") == 0) { nomoreargs = 1; continue; }
      if (strcmp(arg, "--overwrite") == 0) { overwrite = 1; continue; }
      if (strcmp(arg, "--upcase") == 0) { locaseTokens = 0; continue; }
      if (strcmp(arg, "--locase") == 0) { locaseTokens = 1; continue; }
      if (strcmp(arg, "--help") == 0) { help(argv[0]); continue; }
      if (strcmp(arg, "-h") == 0) { help(argv[0]); continue; }
      fprintf(stderr, "FATAL: unknown option '%s'!\n", arg);
      return 1;
    }
    if (!inname) {
      inname = arg;
      continue;
    }
    if (!outname) {
      outname = strdup(arg);
      continue;
    }
    fprintf(stderr, "FATAL: too many arguments!\n");
    return 1;
  }

  if (!inname) {
    help(argv[0]);
    return 1;
  }

  if ((fi = fopen(inname, "rb")) == NULL) {
    fprintf(stderr, "FATAL: can't open input file: '%s'!\n", inname);
    return 1;
  }

  if (fread(&hdr, sizeof(hdr), 1, fi) != 1) {
    fclose(fi);
    fprintf(stderr, "FATAL: invalid input file: '%s'!\n", inname);
    return 1;
  }
  //fseek(fi, -((int)sizeof(hdr)), SEEK_CUR);

  if (hobCalcCheckSum(&hdr) != hdr.checksum) {
    fclose(fi);
    fprintf(stderr, "FATAL: not HoBeta: '%s'!\n", inname);
    return 1;
  }

  for (cvt = cvtList; cvt != NULL; cvt = cvt->next) {
    fseek(fi, 0, SEEK_SET);
    if (cvt->detect(fi, inname, &hdr)) break;
  }

  if (cvt == NULL) {
    fclose(fi);
    fprintf(stderr, "FATAL: can't detect file type: '%s'!\n", inname);
    return 1;
  }

  printf("detected type: %s\n", cvt->fmtname);

  if (!outname) outname = strCopyChangeExt(inname, ".zas");

  if (!overwrite && access(outname, F_OK) == 0) {
    fprintf(stderr, "FATAL: output file '%s' already exists!\n", outname);
    fclose(fi);
    free(outname);
    return 1;
  }

  if ((fo = fopen(outname, "wb")) == NULL) {
    fclose(fi);
    fprintf(stderr, "FATAL: can't create output file: '%s'!\n", outname);
    free(outname);
    return 1;
  }

  fseek(fi, 0, SEEK_SET);
  if (cvt->extract(fo, fi) != 0) {
    fclose(fi);
    fclose(fo);
    unlink(outname);
    fprintf(stderr, "FATAL: can't convert input file: '%s'!\n", inname);
    free(outname);
    return 1;
  }

  fclose(fi);
  fclose(fo);
  free(outname);

  return 0;
}

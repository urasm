/* coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 */
#include "hobeta.h"


//==========================================================================
//
//  hobCalcCheckSum
//
//==========================================================================
uint16_t hobCalcCheckSum (const HOBHeader *hdr) {
  const uint8_t *buf = (const uint8_t *)hdr;
  uint16_t res = 0;
  for (int f = 0; f < 15; ++f) res += ((uint16_t)buf[f])*257+f;
  return res;
}

/* coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 */
#ifndef _HOBETA_H_
#define _HOBETA_H_

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif


#define HOBETA_PACKED  __attribute__((packed)) __attribute__((gcc_struct))


typedef struct HOBETA_PACKED {
  char name[8];
  char type;
  union HOBETA_PACKED {
    uint16_t start;
    struct HOBETA_PACKED { uint8_t type1, type2; };
  };
  uint16_t len;
  uint8_t zero; // always zero
  uint8_t secLen; // length in sectors
  uint16_t checksum;
} HOBHeader;


extern uint16_t hobCalcCheckSum (const HOBHeader *hdr);


#ifdef __cplusplus
}
#endif
#endif

#ifndef _CVT_XAS_H_
#define _CVT_XAS_H_

#include <stdio.h>
#include <stdlib.h>

#include "../libhobeta/hobeta.h"
#include "../cvt.h"

#ifdef __cplusplus
extern "C" {
#endif


extern CvtMethods *cvt_xas (void);


#ifdef __cplusplus
}
#endif
#endif

#include "xas.h"

#include <ctype.h>
#include <string.h>


static const char *opcodes[] = {
"LDIR", "LDDR", "LDI",  "LDD",  "CPIR", "CPDR", "CPI", "CPD",  "INIR", "INDR", "INI",  "IND",  "OUTI",  "OTIR",  "OUTD",  "OTDR",
"RETI", "RETN", "NEG",  "RLD",  "RRD",  "PUSH", "POP", "ADD",  "SUB",  "ADC",  "SBC",  "AND",  "OR",    "XOR",   "CP",    "INC",
"DEC",  "BIT",  "RES",  "SET",  "RLC",  "RRC",  "RL",  "RR",   "SLA",  "SRA",  "SLI",  "SRL",  "LD",    "EX",    "IN",    "OUT",
"IM",   "RST",  "DJNZ", "JP",   "JR",   "CALL", "RET", "EXX",  "CPL",  "DAA",  "RLCA", "RRCA", "RLA",   "RRA",   "NOP",   "HALT",
"DI",   "EI",   "SCF",  "CCF",  "ORG",  "ENT",  "EQU", "WORK", "DB",   "DW",   "DM",   "DS",   "!ASSM", "!CONT", "LTEXT", "LCODE",
"BC",   "DE",   "HL",   "IX",   "IY",   "SP",   "AF",  "(C)",  "B",    "C",    "D",    "E",    "H",     "L",     "(HL)",  "A",
"(BC)", "(DE)", "HX",   "LX",   "HY",   "LY",   "I",   "R",    "NZ",   "Z",    "NC",   "PO",   "PE",    "P",     "M",     "!ON",
"!OFF", "(SP)", "AF'",  "USEL", "IFNZ", "IFZ",  "MAKE" "?",    "?",    "?",    "?",    "?",    "?",     "?",     "?",     "?"
};


static uint8_t cyr (uint8_t b) {
  // OEM code page
  static const uint8_t cyr1[] = {'�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�'};
  static const uint8_t cyr2[] = {'�', '�', '�', '�'};
  //
  if (b >= 0x10 && b <= 0x1f) return cyr1[b-0x10];
  if (b >= 0x7b && b <= 0x7e) return cyr2[b-0x7b];
  return b;
}


static uint8_t toLower (uint8_t b) {
  // convert all UPPERCASE letters to lowercase
  if (b >= 'A' && b <= 'Z') return b-'A'+'a';
  return b;
}


/*
static uint8_t transform (uint8_t b) {
  b = cyr(b);
  b = toLower(b);
  return b;
}
*/


#define GETBYTE(_dst)  do { \
  if (--buflen < 0) { free(buf); return -1; } \
  (_dst) = *p++; \
} while (0)


#define PUTCH(_ch)  do { \
  uint8_t _xch = (uint8_t)(_ch); \
  if (fwrite(&_xch, 1, 1, fo) != 1) { free(buf); return -1; } \
} while (0)


#define PUTCH_TK(_ch)  do { \
  uint8_t _xch = (uint8_t)(_ch); \
  if (locaseTokens && _xch >= 'A' && _xch <= 'Z') _xch = _xch-'A'+'a'; \
  if (fwrite(&_xch, 1, 1, fo) != 1) { free(buf); return -1; } \
} while (0)


static int extract (FILE *fo, FILE *fi) {
  int buflen;
  uint8_t *buf;
  const uint8_t *p;
  const char *from;
  int needComma;
  //
  if ((buf = loadWholeFile(fi, &buflen)) == NULL) return -1;
  if ((buflen -= sizeof(HOBHeader)) < 1) { free(buf); return -1; }
  if ((buflen -= 29+2+1+1+1+1+1) < 1) { free(buf); return -1; }
  //
  p = buf+sizeof(HOBHeader)+29+2+1+1+1+1+1;
  while (buflen > 0 && *p) {
    uint8_t b;
    //
    GETBYTE(b);
    // process the label
    while (b < 0x80) {
      if (b == 0x0d || b == 0x0c || b == 0x09) {
        // EOL
        PUTCH('\n');
        goto NEXT_LINE;
      }
      if (b == ';') {
        // it's comment;
        while (b != 0x0d && b != 0x0c && b != 0x09) {
          PUTCH(cyr(b));
          if (buflen < 1) break;
          GETBYTE(b);
        }
        PUTCH('\n');
        goto NEXT_LINE;
      }
      PUTCH(toLower(b));
      if (buflen < 1) goto NEXT_LINE;
      GETBYTE(b);
    }
    // command
    //PUTCH('\t');
    //PUTCH('\t');
    PUTCH(' ');
    PUTCH(' ');
    from = opcodes[b-0x80];
    while (*from) PUTCH_TK(*from++);
    //PUTCH('\t');
    PUTCH(' ');
    needComma = 0;
    // loop foreach token
    while (buflen > 0) {
      GETBYTE(b);
      if (b == 0x0d || b == 0x0c || b == 0x09) {
        // EOL
        PUTCH('\n');
        goto NEXT_LINE;
      }
      //
      if (b == ';') {
        //PUTCH('\t');
        PUTCH(' ');
        while (b != 0x0d && b != 0x0c && b != 0x09) {
          PUTCH(cyr(b));
          if (buflen < 1) goto NEXT_LINE;
          GETBYTE(b);
        }
        PUTCH('\n');
        goto NEXT_LINE;
      }
      //
      if (b >= 0x80) {
        if (needComma) PUTCH(',');
        from = opcodes[b-0x80];
        while (*from) PUTCH_TK(*from++);
        needComma = 1;
        continue;
      }
      //
      if (b == '(') {
        if (needComma) PUTCH(',');
        PUTCH('(');
        GETBYTE(b);
        while (b != ')') {
          if (b >= 0x80) {
            from = opcodes[b-0x80];
            while (*from) PUTCH_TK(*from++);
          } else {
            PUTCH(toLower(b));
          }
          GETBYTE(b);
        }
        PUTCH(')');
        needComma = 1;
        continue;
      }
      //
      if (b == '"') {
        if (needComma) PUTCH(',');
        PUTCH('"');
        // special case """
        if (buflen > 1 && p[0] == '"' && p[1] == '"') {
          p += 2; buflen -= 2;
          PUTCH('\\');
          PUTCH('"');
          continue;
        }
        GETBYTE(b);
        while (b != '"') {
          PUTCH(cyr(b));
          GETBYTE(b);
        }
        PUTCH('"');
        needComma = 1;
        continue;
      }
      //
      if (needComma) PUTCH(',');
      while (b < 0x80 && b != '(' && b != ';' && b != 0x0d && b != 0x0c && b != 0x09) {
        PUTCH(toLower(b));
        if (buflen < 1) goto NEXT_LINE1;
        GETBYTE(b);
      }
      needComma = 1;
      --p; ++buflen;
    }
NEXT_LINE1:;
    PUTCH('\n');
NEXT_LINE:;
  }
  free(buf);
  return 0;
}


static const int detect (FILE *fi, const char *fname, const HOBHeader *hdr) {
  if ((hdr->type != 'X' && hdr->type != 'x') ||
      (hdr->type1 != 'A' && hdr->type1 != 'a') ||
      hdr->type2 != 'S') return 0;
  return 1;
}


////////////////////////////////////////////////////////////////////////////////
static CvtMethods mtlist;


CvtMethods *cvt_xas (void) {
  mtlist.name = "XAS assembler";
  mtlist.fmtname = "xas";
  mtlist.extract = extract;
  mtlist.detect = detect;
  return &mtlist;
};

#include "storm.h"

#include <ctype.h>
#include <string.h>


typedef enum {
  IFALSE,
  ITRUE
} ibool;


static const char *tokens[] = {
/*0x09 */ "ORG",  "EQU",  "DI",   "EI",   "EX AF,AFX",  "NOP",  "CCF",  "SCF",
/*0x11 */ "CPL",  "DAA",  "EXX",  "RLA",  "RRA",  "RLCA", "RRCA", "HALT",
/*0x19 */ "LDI",  "LDD",  "LDIR", "LDDR", "CPI",  "CPD",  "CPIR", "CPDR",
/*0x21 */ "INI",  "IND",  "INIR", "INDR", "OUTI", "OUTD", "OTIR", "OTDR",
/*0x29 */ "NEG",  "RLD",  "RRD",  "INF",  "RETI", "RETN", "",     "B",
/*0x31 */ "C",    "D",    "E",    "H",    "L",    "(HL)", "A",    "HX",
/*0x39 */ "LX",   "HY",   "LY",   "BC",   "DE",   "HL",   "SP",   "",
/*0x41 */ "DEFB", "DEFW", "DEFS", "AF'",  "XH",   "XL",   "YH",   "YL",
/*0x49 */ "",     "LD",   "INC",  "DEC",  "EX",   "JR",   "DJNZ", "JP",
/*0x51 */ "CALL", "RET",  "POP",  "PUSH", "ADD",  "ADC",  "SUB",  "SBC",
/*0x59 */ "AND",  "OR",   "XOR",  "CP",   "IN",   "OUT",  "BIT",  "RES",
/*0x61 */ "SET",  "RLC",  "RRC",  "RL",   "RR",   "SLA",  "SRA",  "SLI",
/*0x69 */ "SRL",  "IM",   "RST",  "DB",   "DW",   "DS",   "IX",   "IY",
/*0x71 */ "(BC)", "(DE)", "I",    "R",    "AF",   "(SP)", "(C)",  "NZ",
/*0x79 */ "Z",    "NC",   "C",    "PO",   "PE",   "P",    "M",    "INCL",
/*0x81 */ "INCB", "REPT", "ENDR", "IF",   "IFU",  "IFNU", "IFD",  "IFND",
/*0x89 */ "ELSE", "EIF",  "ENDM"
};


static const char *ar[] = {
  "+", "-", "*", "/", "\\", "&", "!", "|",
  "<<",">>","<=",">=","<",  ">", "=",
  "/256", "%256", "^", "`", "?", "~",  "@", "'"
};


typedef struct {
  char buf[256];
  char *p;
} LineBuffer;


static inline void lbuf_clear_line (LineBuffer *lb) {
  lb->p = lb->buf;
}


static inline void lbuf_puteol (LineBuffer *lb) {
  *(lb->p) = 0;
}


static inline void lbuf_putchar (LineBuffer *lb, char c) {
  if (lb->p-lb->buf < sizeof(lb->buf)-1) {
    *(lb->p) = c;
    ++lb->p;
  }
}


static inline void lbuf_putcstr (LineBuffer *lb, const char *s) { while (*s) lbuf_putchar(lb, *s++); }
static inline void lbuf_putcstr_lo (LineBuffer *lb, const char *s) { while (*s) lbuf_putchar(lb, tolower(*s++)); }

static inline void lbuf_putcstr_tk (LineBuffer *lb, const char *s) {
  if (locaseTokens) return lbuf_putcstr_lo(lb, s);
  return lbuf_putcstr(lb, s);
}


static inline void lbuf_putint (LineBuffer *lb, int n) {
  char tmp[128];
  snprintf(tmp, sizeof(tmp), "%d", n);
  lbuf_putcstr(lb, tmp);
}


static inline void lbuf_puthex (LineBuffer *lb, uint8_t b) {
  lbuf_putchar(lb, "0123456789ABCDEF"[(b>>4)&0x0f]);
  lbuf_putchar(lb, "0123456789ABCDEF"[b&0x0f]);
}


static inline void lbuf_putbin (LineBuffer *lb, uint8_t b) {
  uint8_t mask = 0x80;
  for (int i = 0; i < 8; ++i) {
    lbuf_putchar(lb, (b&mask ? '1' : '0'));
    mask >>= 1;
  }
}


static inline void lbuf_fillto (LineBuffer *lb, int pos) {
  while (lb->p-lb->buf < pos) lbuf_putchar(lb, ' ');
}


////////////////////////////////////////////////////////////////////////////////
static LineBuffer lb;


////////////////////////////////////////////////////////////////////////////////
static inline int bit (uint8_t num, uint8_t b) {
  return ((b&(1<<num)) != 0);
}


////////////////////////////////////////////////////////////////////////////////
typedef struct Line {
  uint8_t length;
  const uint8_t *start;
  struct Line *next;
  //Line(uint8_t* _start, uint8_t _length, Line* _next) : start(_start), length(_length), next(_next) {}
} Line;


////////////////////////////////////////////////////////////////////////////////
typedef struct {
  Line *top;
  Line *cur;
} Lines;


static void lines_init (Lines *ls) {
  ls->top = ls->cur = NULL;
}


static void lines_clear (Lines *ls) {
  Line *l = ls->top;
  while (l) {
    Line *tmp = l;
    l = l->next;
    free(tmp);
  }
  ls->top = ls->cur = NULL;
}


static void lines_push (Lines *ls, const uint8_t *_start, uint8_t _length) {
  Line *newLine = calloc(1, sizeof(Line));
  newLine->start = _start;
  newLine->length = _length;
  newLine->next = ls->top;
  ls->top = ls->cur = newLine;
}


static Line *lines_pop (Lines *ls) {
  Line *ret = ls->cur;
  if (ret == NULL) return NULL;
  ls->cur = ret->next;
  return ret;
}


////////////////////////////////////////////////////////////////////////////////
static const uint8_t *printString (const uint8_t *p) {
  while (*p) {
    if (*p <= 0x1f) {
      for (uint8_t i = 0; i < *p; ++i) lbuf_putchar(&lb, ' ');
    } else {
      lbuf_putchar(&lb, *p);
    } ++p;
  }
  return ++p;
}


static const uint8_t *printLabel (const uint8_t *p) {
  uint8_t b = *p;
  //
  if (b == 0xDA) lbuf_putchar(&lb, '_');
  else if (b > 0xDA) lbuf_putchar(&lb, '='); // ???
  else lbuf_putchar(&lb, b-0x7f);
  //
  do {
    uint8_t b = *(++p)&0x3f;
    //
    if (b <= 0x09) lbuf_putchar(&lb, '0' + b);
    else if (b == 0x0b) lbuf_putchar(&lb, '_');
    else if (b == 0x0a) {/* 0x8a|0x0a - end of one char label */}
    else if (b < 0x26) lbuf_putchar(&lb, b - 0x0c + 'A');
    else lbuf_putchar(&lb, b-0x26+'a');
  } while (*p < 0x80);
  return p+1;
}


static const uint8_t *printCommand (const uint8_t *p, ibool indent) {
  uint8_t b = *p;
  //
  if (indent) lbuf_fillto(&lb, 8);
  if (b >= 0x9 && b < 0x2f) {
    lbuf_putcstr_tk(&lb, tokens[b-9]);
    ++p;
  } else if (b > 0x2f && b < 0x40) {
    lbuf_putcstr_lo(&lb, "LD");
  } else if (b >= 0x40 && b < 0x6f) {
    if (b == 0x49) {
      lbuf_putcstr_lo(&lb, tokens[p[1]+0x80-0x09]);
      p += 2;
    } else {
      lbuf_putcstr_lo(&lb, tokens[b-9]);
      ++p;
    }
  } else if (b >= 0x6f && b < 0x75) {
    lbuf_putcstr_lo(&lb, "LD");
  } else if (b >= 0x75 && b < 0x77) {
    lbuf_putcstr_lo(&lb, "EX");
  } else if (b == 0x77) {
    lbuf_putcstr_lo(&lb, "OUT");
  } else if (b > 0x77 && b < 0x7c) {
    lbuf_putcstr_lo(&lb, "JR");
  } else if (b >= 0x7c && b < 0x80) {
    lbuf_putcstr_lo(&lb, "CALL");
  } else if (b >= 0x80 && b < 0xDC) {
    lbuf_putcstr_lo(&lb, bit(5, b) ? "LD" : "JR");
  } else if (b == 0xDC) {
    lbuf_putcstr_lo(&lb, "DB");
  } else if (b > 0xDC) {
    lbuf_putcstr_lo(&lb, "JR");
  }
  return p;
}


static const uint8_t *printExpression (const uint8_t *p, uint8_t op, ibool number) {
  uint8_t b;
  //
  for (;;) {
    if (number) {
      switch (op&0x07) {
        case 0:
          lbuf_putchar(&lb, '(');
          b = *p++;
          p = printExpression(p, b, !bit(4, op));
          lbuf_putchar(&lb, ')');
          break;
        case 1:
          lbuf_putchar(&lb, '#');
          lbuf_puthex(&lb, *p++);
          break;
        case 2:
          lbuf_putint(&lb, *p++);
          break;
        case 3:
          if (*p < 0xC0) {
            b = *p++;
            if (b == 0) {
              lbuf_putchar(&lb, '$');
            } else {
              --b;
              b &= 0x07;
              lbuf_putchar(&lb, '='); // ???
              lbuf_putchar(&lb, '0'+b);
            }
          } else {
            p = printLabel(p);
          }
          break;
        case 4:
          lbuf_putchar(&lb, '"');
          if (p[1] != 0) lbuf_putchar(&lb, p[1]);
          lbuf_putchar(&lb, *p);
          p += 2;
          lbuf_putchar(&lb, '"');
          break;
        case 5:
          lbuf_putchar(&lb, '#');
          lbuf_puthex(&lb, p[1]);
          lbuf_puthex(&lb, p[0]);
          p += 2;
          break;
        case 6:
          lbuf_putint(&lb, 256*p[1]+p[0]);
          p += 2;
          break;
        case 7:
          lbuf_putchar(&lb, '%');
          if (p[1] != 0) lbuf_putbin(&lb, p[1]);
          lbuf_putbin(&lb, *p);
          p += 2;
          break;
      }
      number = IFALSE;
      if (bit(3, op)) break;
      op = *p++;
    } else {
      uint8_t _b = op;
      //
      if ((op & 0xf0) != 0xf0) {
        op &= 0xf0;
        op >>= 4;
        lbuf_putcstr(&lb, ar[op]);
        op = _b;
        number = ITRUE;
      } else {
        op &= 0x07;
        op += 0x0f;
        lbuf_putcstr(&lb, ar[op]);
        number = IFALSE;
        // if it is last postfix operator...
        if (bit(3, _b)) break;
        op = *p++;
      }
    }
  }
  return p;
}


////////////////////////////////////////////////////////////////////////////////
static int extract (FILE *fo, FILE *fi) {
  Lines ls;
  Line *l = 0;
  int buflen;
  uint8_t *buf;
  const uint8_t *p;// = (const uint8_t *)buf+hdr->length-1;
  char separator;
  ibool indent;
  //
  if ((buf = (uint8_t *)loadWholeFile(fi, &buflen)) == NULL) return -1;
  //if (buflen < (int)sizeof(HOBHeader) || (buflen-(int)sizeof(HOBHeader))&0xff) { free(buf); return -1; }
  //
  lines_init(&ls);
  p = (const uint8_t *)(buf+sizeof(HOBHeader)+((const HOBHeader *)buf)->len-1);
  ls.top = ls.cur = 0;
  // fill list of lines
  while (p > buf+sizeof(HOBHeader)) {
    uint8_t length = (*p)&0x3f;
    //
    lines_push(&ls, p-length, length);
    p -= length+1;
  }
  //
  while ((l = lines_pop(&ls))) {
    uint8_t len = l->length;
    const uint8_t *p = l->start;
    //
    lbuf_clear_line(&lb);
    if (!len) goto EOL;
    if (*p == 0x2F) {
      lbuf_putchar(&lb, ';');
      printString(p+1);
      goto EOL;
    }
    indent = ITRUE;
    if (*p >= 0xC0 && *p < 0xDC) {
      if (bit(6, p[1])) p = printLabel(p);
    } else if (*p == 0x06) {
      lbuf_putchar(&lb, '_');
      ++p;
      indent = IFALSE;
    } else if (*p == 0x07) {
      lbuf_putchar(&lb, '.');
      lbuf_putint(&lb, p[1]);
      p += 2;
    }
    if (len-(p-l->start) <= 0) goto EOL;
COMMAND:
    p = printCommand(p, indent);
    indent = IFALSE;
    separator = ' ';
    for (;;) {
      if (len-(p-l->start) <= 0) break;
      if (*p == 0x2F) {
        lbuf_putcstr(&lb, ";");
        p = printString(p+1);
        break;
      }
      if (*p < 0x80) {
        uint8_t b = (*p)&0x3f;
        //
        if (b >= 0x2a && b < 0x2f) {
          switch (b) {
            case 0x2a: lbuf_putcstr(&lb, ":"); break;
            case 0x2b: lbuf_putcstr(&lb, " :"); break;
            case 0x2c: lbuf_putcstr(&lb, " : "); break;
            case 0x2d: lbuf_putcstr(&lb, " :  "); break;
            case 0x2e: lbuf_putcstr(&lb, "  : "); break;
          }
          ++p;
          goto COMMAND;
        }
      }
      lbuf_putchar(&lb, separator);
      separator = ',';
      if (*p < 0x80) {
        // token
        lbuf_putcstr_tk(&lb, tokens[*p-9]);
        ++p;
      } else if (*p >= 0x80 && *p < 0xC0) {
        // expression
        uint8_t b = *p++;
        ibool needBrackets = bit(5, b);
        ibool number = ITRUE;
        //
        if (needBrackets) lbuf_putchar(&lb, '(');
        if (!needBrackets) {
          number = !bit(4, b);
          if (!number) b &= 0x1f;
        }
        if (needBrackets && bit(4, b)) {
          // IX/IY operations
          lbuf_putchar(&lb, 'i');
          lbuf_putchar(&lb, bit(3, b) ? 'y' : 'x');
          b &= 0x07;
          if (!b) {
            lbuf_putchar(&lb, ')');
            continue;
          }
          if ((b&0x03) == 0) {
            b = *p++;
          } else {
            if (bit(2, b)) {
              b &= 0x03; // operation + or -
              b |= 0x10;
            }
            b |= 0x08; // one number required
          }
          number = IFALSE;
        } else {
          if (!number) b &= 0x1f;
        }
        p = printExpression(p, b, number);
        if (needBrackets) lbuf_putchar(&lb, ')');
      } else if (*p >= 0xC0 && *p <= 0xDB) {
        // label
        p = printLabel(p);
      } else if (*p == 0xDC) {
        // quoted srting
        lbuf_putchar(&lb, '"');
        p = printString(p+1);
        lbuf_putchar(&lb, '"');
      } else if (*p > 0xDC && *p <= 0xE6) {
        // one digit
        lbuf_putchar(&lb, (*p)-0xAD);
        ++p;
      } else {
        // > 0xE6 - $[+-]offset
        lbuf_putchar(&lb, '$');
        if (*p < 0xf3) {
          lbuf_putchar(&lb, '-');
          lbuf_putint(&lb, 0xF3 - *p);
        } else if (*p > 0xf3) {
          lbuf_putchar(&lb, '+');
          lbuf_putint(&lb, *p - 0xF3);
        }
        ++p;
      }
    }
    // this part of code will not be executed
    /*
    uint8_t rest = len - (p - l->start);
    lbuf_puthex(&lb, len);
    lbuf_putcstr(&lb, ": ");
    for(uint8_t i = 0; i < rest; ++i) {
      if(i > 0) lbuf_putcstr(&lb, ", ");
      lbuf_puthex(&lb, p[i]);
    }
    */
EOL:
    *lb.p = 0;
    strcat((char *)lb.buf, "\n");
    if (fwrite(lb.buf, strlen((const char *)lb.buf), 1, fo) != 1) { free(buf); return -2; }
  }
  lines_clear(&ls);
  free(buf);
  return 0;
}


////////////////////////////////////////////////////////////////////////////////
static const int detect (FILE *fi, const char *fname, const HOBHeader *hdr) {
  if (!(hdr->type == 'C' && (hdr->start == 0xC00B)) &&
      !(hdr->type == 'C' && (hdr->start == 0xC003)) &&
      !(hdr->type == 'R' && (hdr->start == 0xC00B))) return 0;
  return 1;
}


////////////////////////////////////////////////////////////////////////////////
static CvtMethods mtlist;


CvtMethods *cvt_storm (void) {
  mtlist.name = "Storm assembler";
  mtlist.fmtname = "storm";
  mtlist.extract = extract;
  mtlist.detect = detect;
  return &mtlist;
};

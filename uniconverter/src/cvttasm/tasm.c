#include "tasm.h"

#include <ctype.h>
#include <string.h>


static const char *token[] = {
"A",    "ADC ", "ADD ", "AF'",  "AF",   "AND ",     "B",       "BC",    "BIT ",  "C",     "CALL ", "CCF",    "CP ",   "CPD",  "CPDR", "CPI",
"CPIR", "CPL",  "D",    "DAA",  "DE",   "DEC ",     "DEFB ",   "DEFM ", "DEFS ", "DEFW ", "DI",    "PHASE ", "DJNZ ", "E",    "EI",   "UNPHASE ",
"EQU ", "EX ",  "EXX",  "H",    "HALT", "HL",       "I",       "IM ",   "IN ",   "INC ",  "IND",   "INDR",   "INI",   "INIR", "IX",   "IY",
"JP ",  "JR ",  "L",    "LD ",  "LDD",  "LDDR",     "LDI",     "LDIR",  "M",     "NC",    "NEG",   "NOP",    "NV",    "NZ",   "OR ",  "ORG ",
"OTDR", "OTIR", "OUT ", "OUTD", "OUTI", "P",        "PE",      "PO",    "POP ",  "PUSH ", "R",     "RES ",   "RET",   "RETI", "RETN", "RL ",
"RLA",  "RLC ", "RLCA", "RLD",  "RR ",  "RRA",      "RRC ",    "RRCA",  "RRD",   "RST ",  "SBC ",  "SCF",    "SET ",  "SLA ", "SP",   "SRA ",
"SRL ", "SUB ", "V",    "XOR ", "Z",    "INCLUDE ", "INCBIN ", "SLI ",  "INF",   "LX",    "HX",    "LY",     "HY",    "DB ",  "DM ",  "DS ",
"DW ",  "?",    "?",    "?",    "?",    "?",        "?",       "?",     "?",     "?",     "?",     "?",      "?",     "?",    "?",    "?"
};


#define GETBYTE(_dst)  do { \
  if (--buflen < 0) { free(buf); return -1; } \
  (_dst) = *from++; \
} while (0)


#define PUTCH(_ch)  do { \
  uint8_t _xch = (uint8_t)(_ch); \
  if (fwrite(&_xch, 1, 1, fo) != 1) { free(buf); return -1; } \
} while (0)


#define PUTCH_TK(_ch)  do { \
  uint8_t _xch = (uint8_t)(_ch); \
  if (locaseTokens && _xch >= 'A' && _xch <= 'Z') _xch = _xch-'A'+'a'; \
  if (fwrite(&_xch, 1, 1, fo) != 1) { free(buf); return -1; } \
} while (0)


static int extract (FILE *fo, FILE *fi) {
  int buflen;
  uint8_t *buf;
  const uint8_t *from;
  int isTasm4;
  //
  if ((buf = loadWholeFile(fi, &buflen)) == NULL) return -1;
  if ((buflen -= sizeof(HOBHeader)) < 1) { free(buf); return -1; }
  //
  isTasm4 = (((const HOBHeader *)buf)->start <= 4096);
  from = buf+sizeof(HOBHeader);
  for (;;) {
    uint8_t len;
    //
    GETBYTE(len);
    if (len == 0xFF) break;
    for (int i = 0; i < len; ++i) {
      if (buflen < 1) { free(buf); return -1; }
      if (*from < 0x20) {
        if (isTasm4 || (*from == 0x0A && i != len-1)) {
          if (!isTasm4 || (isTasm4 && *from == 0x01)) {
            ++from; --buflen;
            ++i;
            if (buflen < 1) { free(buf); return -1; }
          }
          for (int j = 0; j < *from; j++) PUTCH(0x20);
        } else {
          PUTCH(*from);
        }
      } else if (*from < 0x80) {
        PUTCH(*from);
      } else {
        const char *ptr;
        if (!isTasm4) {
          ptr = token[*from-0x80];
        } else {
          switch (*from) {
            case 0x97://"DEFM "
              ptr = "DEFMAC ";
              break;
            case 0x9B://"PHASE "
              ptr = "DISPLAY ";
              break;
            case 0x9F://"UNPHASE "
              ptr = "ENDMAC ";
              break;
            default:
              ptr = token[*from-0x80];
              break;
          }
        }
        while (*ptr) PUTCH_TK(*ptr++);
      }
      ++from; --buflen;
      if (buflen < 1) break;
    }
    PUTCH('\n');
    if (buflen < 1 || *from != len) break;
    ++from; --buflen;
    if (buflen < 1) break;
  }
  free(buf);
  return 0;
}


static const int detect (FILE *fi, const char *fname, const HOBHeader *hdr) {
  if (!(hdr->type == 'A' && (hdr->start == 39221)) &&
      !(hdr->type == 'A' && (hdr->start == 40872)) &&
      !(hdr->type == 'A' && (hdr->start <= 4096))) return 0;
  return 1;
}


////////////////////////////////////////////////////////////////////////////////
static CvtMethods mtlist;


CvtMethods *cvt_tasm (void) {
  mtlist.name = "TASM assembler";
  mtlist.fmtname = "tasm";
  mtlist.extract = extract;
  mtlist.detect = detect;
  return &mtlist;
};

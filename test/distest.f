: main  ( -- )
  " code_start" ur-get-label
  \ " code_undoc" ur-get-label
  \ 0x45
  tozx
  begin
\ ." !000\n" (ufe-bp)
    dup tozx " code_end" ur-get-label tozx <
\ ." !001\n" (ufe-bp)
  while
    tozx dup
\ ." !002\n" (ufe-bp)
    zx-disasm-one  ;; ( staddr addr count )
\ ." !003\n" (ufe-bp)
    rot
    base @ over hex <# # # # # #> type base ! ." : "
    zxdis-pc over -
    >r zxdis-pc tozx swap
    do
      base @ i tozx c@ hex <# bl hold # # #> type base !
    loop
    4 r> - 3 * spaces
    xtype cr
    zxdis-pc
\ ." !099\n" (ufe-bp)
  repeat
;

main
" done" ufe-fatal

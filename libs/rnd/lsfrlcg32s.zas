;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; generate random number
;; this is quite fast, quality pseudo-random number generator
;; it uses 32-bit seeds (and still returns a 16-bit result)
;; an advantage here is that they've been tested and passed randomness
;;  tests (all of thee ones offered by CAcert labs)
;; it combines a 32-bit Linear Feedback Shift Register and a 32-bit LCG
;; it has a period of 18,446,744,069,414,584,320 (roughly 18.4 quintillion)
;; LFSR taps: 0,2,6,7  = 11000101
;;
;; IN:
;;   rndSeed0: lower 2 bytes of the first seed
;;   rndSeed1: upper 2 bytes of the first seed
;;   rndSeed2: lower 2 bytes of the second seed
;;   rndSeed3: upper 2 bytes of the second seed
;;   WARNING! second seed must not be zero
;; OUT:
;;   HL: 16-bit random
;;   BC,DE: DEAD (can be used as lower quality values, but are not independent of HL)
;;   AF: dead
random:
  ld    hl,12345
rndSeed0 equ $-2
  ld    de,6789
rndSeed1 equ $-2
  ld    b,h
  ld    c,l
  add   hl,hl
  rl    e
  rl    d
  add   hl,hl
  rl    e
  rl    d
  inc   l
  add   hl,bc
  ld    (rndSeed0),hl
  ld    hl,(rndSeed1)
  adc   hl,de
  ld    (rndSeed1),hl
  ex    de,hl
  ld    hl,9876
rndSeed2 equ $-2
  ld    bc,54321
rndSeed3 equ $-2
  add   hl,hl
  rl    c
  rl    b
  ld    (rndSeed3),bc
  sbc   a,a
  and   %11000101
  xor   l
  ld    l,a
  ld    (rndSeed2),hl
  ex    de,hl
  add   hl,bc
  ret

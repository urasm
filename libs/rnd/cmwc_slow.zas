;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; generates a 8-bit random number from a 75-bit seed
;; CMWC generator passes all diehard tests.
;; Jun 2012, May 2015 Patrik Rak
;;
;; CMWC (Complimentary-Multiply-With-Carry) RNG for Z80
;; these generators are based on a prime p of the
;; form a*b^r+1, carry c < a and a sequence
;; of r x's < b such that
;;   t = a*x(n-r)+c(n-1),
;;   c(n) = t div b,
;;   x(n) = (b-1)-t mod b
;;
;; to make it suitable for Z80, I have chosen base b=2^8
;; I was also interested how well it could do with fairly small
;; state, so I have chosen r=8 rather than the tempting r=256
;;
;; from the possible primes, I have chosen the one with a=253,
;; for which the order of the generated sequence is
;; (p-1)/2^5 = 253*2^59 (assuming I got all the math right),
;; which is almost 2^67, and more than 2^66 or 10^20
;;
;; seeding:
;; the x(n-r) is basically the byte which is being rotated out
;; (r == 8 == number of x-es == size of the table), there is no
;; dependency among the remaining ones. the only condition for
;; all the associated math proofs is that all x-es are less than
;; base b, which they are by definition (0..255 < 256), and that
;; the carry c is less than a (253 in this case). so, basically
;; you can keep c set to zero and seed any of the x-es to whatever
;; you desire. even all zeros are fine.
;;
;; (technically, it seems you could do with c >= a, too, as it
;; would pretty soon become < a anyway, but that would complicate
;; the reasoning about the sequence lengths and all the proofs, so
;; it's easier not to).
;;
;; OUT:
;;   A: 8-bit random
;;   BC,DE,HL,F: dead
;; WARNING:
;;  be careful to not have all zeroes in rndSeed!

; 10-byte seed
rndseedtbl  defb 0,0,82,97,120,111,102,116,20,15

random8:
  ld    hl,rndseedtbl

  ld    a,(hl)  ; i = ( i & 7 ) + 1
  and   7
  inc   a
  ld    (hl),a

  IF low(rndseedtbl) != 0
  inc   hl      ; hl = &cy
  ELSE
  inc   l       ; hl = &cy
  ENDIF

  ld    b,h     ; bc = &q[i]
  add   a,l
  ld    c,a
  IF low(rndseedtbl) != 0
  jp    nc,.rndskip0
  inc   b
.rndskip0:
  ENDIF

  ld    a,(bc)  ; y = q[i]
  ld    d,a
  ld    e,a
  ld    a,(hl)  ; da = 256 * y + cy

  sub   e       ; da = 255 * y + cy
  jp    nc,.rndskip1
  dec   d
.rndskip1:
  sub   e       ; da = 254 * y + cy
  jp    nc,.rndskip2
  dec   d
.rndskip2:
  sub   e       ; da = 253 * y + cy
  jp    nc,.rndskip3
  dec   d
.rndskip3:

  ld    (hl),d  ; cy = da >> 8, x = da & 255
  cpl           ; x = (b-1) - x = -x - 1 = ~x + 1 - 1 = ~x
  ld    (bc),a  ; q[i] = x

  ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; generates a 16-bit random number from a 75-bit seed
;; CMWC generator passes all diehard tests.
;; Jun 2012, May 2015 Patrik Rak
;;
;; OUT:
;;   HL: 16-bit random
;;   BC,DE,AF,AF': dead
;; WARNING:
;;  be careful to not have all zeroes in rndSeed!
random:
  call  random8
  ex    af,af'
  call  random8
  ld    h,a
  ex    af,af'
  ld    l,a
  ret

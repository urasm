\ GPLv3 ONLY

$IFZX
  ZX-HERE
$ENDIF

0 value zxdis-pc
1 value zxdis-hex

0 value (zxdis-ixiy)  \ contains 0, [char] x or [char] y
0 value (zxdis-disp)
0 value (zxdis-dptr)
0 value (zxdis-dofs)
0 value (zxdis-opcode)

var-noallot (zxdis-ixydisp-table)
  [0x] 00 C, [0x] 00 C, [0x] 00 C, [0x] 00 C,
  [0x] 00 C, [0x] 00 C, [0x] 70 C, [0x] 00 C,
  [0x] 40 C, [0x] 40 C, [0x] 40 C, [0x] 40 C,
  [0x] 40 C, [0x] 40 C, [0x] BF C, [0x] 40 C,
  [0x] 40 C, [0x] 40 C, [0x] 40 C, [0x] 40 C,
  [0x] 40 C, [0x] 40 C, [0x] 40 C, [0x] 40 C,
  [0x] 00 C, [0x] 08 C, [0x] 00 C, [0x] 00 C,
  [0x] 00 C, [0x] 00 C, [0x] 00 C, [0x] 00 C,


\ ////////////////////////////////////////////////////////////////////////// //
: (zxdis-draw-str)  ( -- ) (zxdis-dptr) 1+ (zxdis-dofs) xtype cr ;

: (zxdis-put-char)  ( ch -- )
  (zxdis-dptr) (zxdis-dofs) + 1+ C!
  (zxdis-dofs) 1+ to (zxdis-dofs)
;

: (zxdis-put-str)  ( addr len -- )
  dup 0> if
    over + swap do
      i c@ (zxdis-put-char)
    loop
  else
    2drop
  endif
;

: (zxdis-put-space)  ( -- ) bl (zxdis-put-char) ;
: (zxdis-put-comma)  ( -- ) [char] , (zxdis-put-char) ;
: (zxdis-put-lpar)  ( -- ) [char] ( (zxdis-put-char) ;
: (zxdis-put-rpar)  ( -- ) [char] ) (zxdis-put-char) ;


: (zxdis-put-str-2)  ( addr -- ) 2 (zxdis-put-str) ;
: (zxdis-put-str-4)  ( addr -- ) drop + 4 (zxdis-put-str) ;
: (zxdis-put-str-4x)  ( addr -- ) dup 3 + C@ bl = if 3 else 4 endif (zxdis-put-str) ;


\ ////////////////////////////////////////////////////////////////////////// //
: (zxdis-put-n8)  ( n -- )
  [0x] ff and
  base @ >r
  zxdis-hex if
    hex <#n # # [char] # hold
  else
    decimal <#n #s
  endif
  #>
  (zxdis-put-str) r> base !
;

: (zxdis-put-n16)  ( n -- )
  [0x] ffff and
  base @ >r
  zxdis-hex if
    hex <#n # # # # [char] # hold
  else
    decimal <#n #s
  endif
  #>
  (zxdis-put-str) r> base !
;

: (zxdis-put-disp)  ( -- )
  (zxdis-disp) 0< if [char] - else [char] + endif (zxdis-put-char)
  (zxdis-disp) abs
  base @ >r decimal <#n #s #>
  (zxdis-put-str) r> base !
;


\ ////////////////////////////////////////////////////////////////////////// //
\ advances PC
: (zxdis-get-byte)  ( -- b )
  zxdis-pc tozx C@
  zxdis-pc 1+ [0x] FFFF and tozx to zxdis-pc
;


\ advances PC
: (zxdis-get-word)  ( -- b )
  (zxdis-get-byte) (zxdis-get-byte) 8 lshift or
;


: (zxdis-byte-to-signed)  ( b -- n )  dup [0x] 80 >= if [0x] 100 - endif ;


\ ////////////////////////////////////////////////////////////////////////// //
: (zxdis-put-ixy-mem) ( -- )
  " (i" (zxdis-put-str)
  (zxdis-ixiy) (zxdis-put-char)
  (zxdis-put-disp)
  (zxdis-put-rpar)
;


: (zxdis-put-r8)  ( r8 -- )
  7 and  " bcdehl.a" drop + c@
  dup [char] . = if
    \ (hl)
    drop
    (zxdis-ixiy) if
      (zxdis-put-ixy-mem)
    else
      " (hl)" (zxdis-put-str)
    endif
  else
    \ undocumented IX/IY 8-bit part access
    (zxdis-ixiy) if
      dup [char] h = over [char] l = or if
        \ [char] i (zxdis-put-char)
        (zxdis-ixiy) (zxdis-put-char)
      endif
    endif
    (zxdis-put-char)
  endif
;


: (zxdis-put-r16-hl-ixy)  ( -- )
  \ hl
  (zxdis-ixiy) if
    " i" (zxdis-put-str)
    (zxdis-ixiy) (zxdis-put-char)
  else
    " hl" (zxdis-put-str)
  endif
;

: (zxdis-put-v16)  ( -- ) (zxdis-get-word) (zxdis-put-n16) ;
: (zxdis-put-m16)  ( -- )  (zxdis-put-lpar) (zxdis-put-v16) (zxdis-put-rpar) ;

: (zxdis-put-r16-common)  ( r16 addr count -- )
  drop swap 3 and 2u* +
  dup c@ [char] h = if
    drop (zxdis-put-r16-hl-ixy)
  else
    (zxdis-put-str-2)
  endif
;

: (zxdis-put-r16-sp)  ( r16 -- )
  (zxdis-opcode) 4 rshift
  " bcdehlsp" (zxdis-put-r16-common)
;


: (zxdis-put-r16-af)  ( r16 -- )
  (zxdis-opcode) 4 rshift
  " bcdehlaf" (zxdis-put-r16-common)
;


: (zxdis-put-cc)  ( cc -- )
  7 and 2u* " nzz ncc popep m " drop +
  dup c@ (zxdis-put-char)
  1+ c@ dup 32 <> if (zxdis-put-char) else drop endif
;


\ ////////////////////////////////////////////////////////////////////////// //
: (zxdis-decode-cb-unixy)  ( -- )
  \ special undocumented thing
  (zxdis-ixiy) if
    \ `bit` doesn't need undoc ixy
    (zxdis-opcode) [0x] 80 and if
      (zxdis-opcode) 7 and 6 <> if
        (zxdis-put-comma)
        (zxdis-put-ixy-mem)
      endif
    endif
  endif
;

: (zxdis-decode-cb)  ( -- )
  (zxdis-opcode) [0x] c0 and
  if
    (zxdis-opcode) 4 rshift [0x] 0c and 4- " bit res set " (zxdis-put-str-4)
    (zxdis-put-space)
    (zxdis-opcode) 3 rshift 7 and [char] 0 + (zxdis-put-char)
    (zxdis-put-comma)
  else
    (zxdis-opcode) 2u/ [0x] 1c and
    " rlc rrc rl  rr  sla sra sll srl " (zxdis-put-str-4)
    (zxdis-put-space)
  endif
  (zxdis-opcode) (zxdis-put-r8)
  (zxdis-decode-cb-unixy)
;


\ ////////////////////////////////////////////////////////////////////////// //
: (zxdis-decode-ed-xrep)  ( -- )
  \ two instructions with the wrong mnemonic length
  (zxdis-opcode) [0x] a3 = if " outi" (zxdis-put-str) exit endif
  (zxdis-opcode) [0x] ab = if " outd" (zxdis-put-str) exit endif
  \ common code
  (zxdis-opcode) 3 and 2u* " ldcpinot" drop + (zxdis-put-str-2)
  (zxdis-opcode) [0x] 08 and if [char] d else [char] i endif (zxdis-put-char)
  (zxdis-opcode) [0x] 10 and if [char] r (zxdis-put-char) endif
;

: (zxdis-decode-ed)  ( -- )
  (zxdis-opcode) [0x] a4 and [0x] a0 = if (zxdis-decode-ed-xrep) exit endif
  (zxdis-opcode) [0x] c0 and [0x] 40 <> if " nope" (zxdis-put-str) exit endif
  (zxdis-opcode) [0x] 04 and if
    (zxdis-opcode) 7 and case
      [0x] 04 of " neg" (zxdis-put-str) endof
      [0x] 05 of " ret" (zxdis-put-str) (zxdis-opcode) [0x] 08 and if [char] i else [char] n endif (zxdis-put-char) endof
      [0x] 06 of \ im
        " im   " (zxdis-put-str)
        (zxdis-opcode) [0x] 47 = if " 0/1" (zxdis-put-str) exit endif
        (zxdis-opcode) [0x] 10 and if
          (zxdis-opcode) [0x] 08 and if [char] 2 else [char] 1 endif
        else
          [char] 0
        endif
        (zxdis-put-char)
      endof
      [0x] 07 of
        (zxdis-opcode) case
          [0x] 47 of " ld   i,a" endof
          [0x] 4f of " ld   r,a" endof
          [0x] 57 of " ld   a,i" endof
          [0x] 5f of " ld   a,r" endof
          [0x] 67 of " rrd" endof
          [0x] 6f of " rld" endof
          otherwise drop " nope"
        endcase
        (zxdis-put-str)
      endof
      otherwise drop " nope" (zxdis-put-str)
    endcase
  else
    (zxdis-opcode) [0x] 02 and if
      \ r16
      (zxdis-opcode) [0x] 01 and if
        " ld   " (zxdis-put-str)
        \ direction
        (zxdis-opcode) [0x] 08 and if
          \ to rr
          (zxdis-put-r16-sp)
          (zxdis-put-comma)
          (zxdis-put-m16)
        else
          \ to mem
          (zxdis-put-m16)
          (zxdis-put-comma)
          (zxdis-put-r16-sp)
        endif
      else
        (zxdis-opcode) 2u/ 4 and " sbc adc " (zxdis-put-str-4)
        "  hl," (zxdis-put-str)
        (zxdis-put-r16-sp)
      endif
    else
      (zxdis-opcode) [0x] 01 and if
        " out  (c)," (zxdis-put-str)
        (zxdis-opcode) 3 rshift
        \ check for `(hl)`, it is special here
        dup 7 and 6 = if
          drop [char] 0 (zxdis-put-char)
        else
          (zxdis-put-r8)
        endif
      else
        " in   " (zxdis-put-str)
        (zxdis-opcode) 3 rshift
        \ check for `(hl)`, it is special here
        dup 7 and 6 <> if
          (zxdis-put-r8)
          (zxdis-put-comma)
        else
          drop
        endif
        " (c)" (zxdis-put-str)
      endif
    endif
  endif
;


\ ////////////////////////////////////////////////////////////////////////// //
\ ld r8,r8 (and halt)
: (zxdis-decode-norm-grp1)  ( -- )
  (zxdis-opcode) [0x] 76 = if " halt" (zxdis-put-str) exit endif
  " ld   " (zxdis-put-str)
  (zxdis-opcode) 3 rshift (zxdis-put-r8)
  (zxdis-put-comma)
  (zxdis-opcode) (zxdis-put-r8)
;

: (zxdis-put-alu-str)  ( -- )
  (zxdis-opcode) 2u/ [0x] 1c and " add adc sub sbc and xor or  cp  " (zxdis-put-str-4)
  (zxdis-put-space)
  \ two special opcodes
  (zxdis-opcode) [0x] 38 and dup [0x] 08 = over [0x] 18 = or swap [0x] 00 = or if " a," (zxdis-put-str) endif
;


\ call,ret,push,pop,etc.
: (zxdis-decode-norm-grp3)  ( -- )
  (zxdis-opcode) 7 and case
    [0x] 00 of " ret  " (zxdis-put-str)  (zxdis-opcode) 3 rshift (zxdis-put-cc) endof
    [0x] 01 of
      (zxdis-opcode) [0x] 08 and if
        (zxdis-opcode) [0x] 30 and case
          [0x] 00 of " ret" (zxdis-put-str) endof
          [0x] 10 of " exx" (zxdis-put-str) endof
          [0x] 20 of " jp   (" (zxdis-put-str) (zxdis-put-r16-hl-ixy) (zxdis-put-rpar) endof
          [0x] 30 of " ld   sp," (zxdis-put-str) (zxdis-put-r16-hl-ixy) endof
        endcase
      else
        " pop  " (zxdis-put-str)
        (zxdis-put-r16-af)
      endif
    endof
    [0x] 02 of " jp   " (zxdis-put-str)  (zxdis-opcode) 3 rshift (zxdis-put-cc) (zxdis-put-comma) (zxdis-put-v16) endof
    [0x] 03 of
      (zxdis-opcode) [0x] 38 and case
        [0x] 00 of " jp   " (zxdis-put-str) (zxdis-put-v16) endof
        \ CB:[0x] 08 of endof
        [0x] 10 of " out  (" (zxdis-put-str) (zxdis-get-byte) (zxdis-put-n8) " ),a" (zxdis-put-str) endof
        [0x] 18 of " in   a,(" (zxdis-put-str) (zxdis-get-byte) (zxdis-put-n8) (zxdis-put-rpar) endof
        [0x] 20 of " ex   (sp)," (zxdis-put-str) (zxdis-put-r16-hl-ixy)  endof
        [0x] 28 of " ex   de,hl" (zxdis-put-str) endof
        [0x] 30 of " di" (zxdis-put-str) endof
        [0x] 38 of " ei" (zxdis-put-str) endof
      endcase
    endof
    [0x] 04 of " call " (zxdis-put-str)  (zxdis-opcode) 3 rshift (zxdis-put-cc) (zxdis-put-comma) (zxdis-put-v16) endof
    [0x] 05 of
      (zxdis-opcode) [0x] 08 and if
        \ prefixes already done, so only call is left
        " call " (zxdis-put-str)
        (zxdis-put-v16)
      else
        " push " (zxdis-put-str)
        (zxdis-put-r16-af)
      endif
    endof
    [0x] 06 of
      (zxdis-put-alu-str)
      (zxdis-get-byte) (zxdis-put-n8)
    endof
    [0x] 07 of
      " rst  " (zxdis-put-str)
      (zxdis-opcode) [0x] 38 and (zxdis-put-n8)
    endof
  endcase
;

: (zxdis-decode-norm-grp0)  ( -- )
  (zxdis-opcode) [0x] 06 and case
    [0x] 00 of
      (zxdis-opcode) 1 and if
        (zxdis-opcode) [0x] 08 and if
          " add  " (zxdis-put-str)
          (zxdis-put-r16-hl-ixy)
          (zxdis-put-comma)
          (zxdis-put-r16-sp)
        else
          " ld   " (zxdis-put-str)
          (zxdis-put-r16-sp)
          (zxdis-put-comma)
          (zxdis-put-v16)
        endif
      else
        (zxdis-opcode) [0x] 20 and if
          " jr   " (zxdis-put-str)
          (zxdis-opcode) 3 rshift 3 and (zxdis-put-cc)
          (zxdis-put-comma)
          (zxdis-get-byte) dup [0x] 80 >= if [0x] 100 - endif
          zxdis-pc + (zxdis-put-n16)
          exit
        endif
        (zxdis-opcode) [0x] 10 and if
          (zxdis-opcode) 2u/ 4 and " djnzjr  " (zxdis-put-str-4)
          (zxdis-put-space)
          (zxdis-get-byte) dup [0x] 80 >= if [0x] 100 - endif
          zxdis-pc + (zxdis-put-n16)
          exit
        endif
        (zxdis-opcode) [0x] 08 and if " ex   af,af'" else " nop" endif (zxdis-put-str)
      endif
    endof
    [0x] 02 of
      (zxdis-opcode) 1 and if
        (zxdis-opcode) 2u/ 4 and " inc dec " (zxdis-put-str-4)
        (zxdis-put-space)
        (zxdis-put-r16-sp)
      else
        " ld   " (zxdis-put-str)
        (zxdis-opcode) [0x] 3c and case
          [0x] 00 of " (bc),a" (zxdis-put-str) endof
          [0x] 08 of " a,(bc)" (zxdis-put-str) endof
          [0x] 10 of " (de),a" (zxdis-put-str) endof
          [0x] 18 of " a,(de)" (zxdis-put-str) endof
          [0x] 20 of (zxdis-put-m16) (zxdis-put-comma) (zxdis-put-r16-hl-ixy) endof
          [0x] 28 of (zxdis-put-r16-hl-ixy) (zxdis-put-comma) (zxdis-put-m16) endof
          [0x] 30 of (zxdis-put-m16) " ,a" (zxdis-put-str) endof
          [0x] 38 of " a," (zxdis-put-str) (zxdis-put-m16) endof
        endcase
      endif
    endof
    [0x] 04 of
      (zxdis-opcode) [0x] 01 and 2 lshift " inc dec " (zxdis-put-str-4)
      (zxdis-put-space)
      (zxdis-opcode) 3 rshift (zxdis-put-r8)
    endof
    [0x] 06 of
      (zxdis-opcode) 1 and if
        (zxdis-opcode) 2u/ [0x] 1c and " rlcarrcarla rra daa cpl scf ccf " drop + (zxdis-put-str-4x)
      else
        " ld   " (zxdis-put-str)
        (zxdis-opcode) 3 rshift (zxdis-put-r8)
        (zxdis-put-comma)
        (zxdis-get-byte) (zxdis-put-n8)
      endif
    endof
  endcase
;

: (zxdis-decode-norm)  ( -- )
  (zxdis-opcode) [0x] c0 and case
    [0x] 00 of (zxdis-decode-norm-grp0) endof
    [0x] 40 of (zxdis-decode-norm-grp1) endof
    [0x] 80 of (zxdis-put-alu-str) (zxdis-opcode) (zxdis-put-r8) endof \ alu a,r8
    otherwise drop (zxdis-decode-norm-grp3)
  endcase
;


\ ////////////////////////////////////////////////////////////////////////// //
\ returns disassembled test (only command, no address or bytes )
: zx-disasm-one ( addr -- saddr slen )
  to zxdis-pc
  0 to (zxdis-ixiy)
  PAD 420 + to (zxdis-dptr)
  0 to (zxdis-dofs)
  0 to (zxdis-disp)
  (zxdis-get-byte)

  \ check if I<X|Y> prefix
  dup [0x] dd = if [char] x to (zxdis-ixiy) endif
  dup [0x] fd = if [char] y to (zxdis-ixiy) endif
  (zxdis-ixiy) if
    drop (zxdis-get-byte) dup to (zxdis-opcode)
    dup [0x] dd = over [0x] fd = or if
      drop " nopx" (zxdis-put-str)
      \ one byte back
      zxdis-pc 1- [0x] FFFF and tozx to zxdis-pc
      exit
    endif
    \ check if we have disp here
    dup 3 rshift (zxdis-ixydisp-table) + c@
    1 rot 7 and lshift and
    if
      \ has disp
      (zxdis-get-byte) (zxdis-byte-to-signed) to (zxdis-disp)
    endif
  else
    to (zxdis-opcode)
  endif

  (zxdis-opcode) case
    [0x] cb of (zxdis-get-byte) to (zxdis-opcode) (zxdis-decode-cb) endof
    [0x] ed of (zxdis-get-byte) to (zxdis-opcode) (zxdis-decode-ed) endof
    otherwise drop (zxdis-decode-norm)
  endcase
  (zxdis-dofs) (zxdis-dptr) C!
  (zxdis-dptr) 1+ (zxdis-dofs)
;

$IFZX
  ZX-HERE SWAP -
  ." Z80 disasm size: " . ." bytes\n"
$ENDIF


$IFZX
: dct  ( addr -- )
  \ 0 to zxdis-pc
  to zxdis-pc
  endcr
  begin
    zxdis-pc tozx dup
    zx-disasm-one  \ ( staddr addr count )
    rot
    base @ over hex <#n # # # # #> type base ! ." : "
    zxdis-pc over -
    >r zxdis-pc tozx swap
    do
      base @ i tozx c@ hex <#n bl hold # # #> type base !
    loop
    4 r> - 3 * spaces
    2dup upcase-str
    type cr
  key 7 = until
;
$ENDIF

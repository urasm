;; for optimisers:
;; addr,count: word name
;; mode is always native (but will be restored after the call to an optimiser)


\ : OPTIMISE-NATIVE-WORD  ( addr count pfa -- )
\   ." *** compiled NATIVE word at #"
\   BASE @ HEX SWAP <# # # # # # # # # #> TYPE BASE !
\   ." , name is <" TYPE ." >\n"
\ ;

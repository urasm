\ ." loading dsForth word optimiser...\n"

\ $DEFINE DEBUG-ZX-OPTIMISER
\ $DEFINE DEBUG-ZX-OPTIMISER-STACK-COMBINER

$DEFINE ENABLE-ZX-OPTIMISER

;; for optimisers:
;; addr,count: word name
;; mode is always native (but will be restored after the call to an optimiser)


;; ////////////////////////////////////////////////////////////////////////// //
: (ZX-IS-BRANCH-AT)  ( pfa -- flag )
  @ UR-ZX-WORD-FLAGS-BY-CFA
  FWF-BRANCH AND
;

;; offset all branches greater or equal to `addrafter` by `ofs`
: (ZX-OFS-BRANCHES-AFTER)  ( pfa addrafter ofs -- )
  SWAP  ;; ( pfa ofs addrafter )
  TOZX  ;; convert `addrafter` to ZX address
  ROT   ;; ( ofs addrafter pfa )
  BEGIN
    DUP ZX-TRACER-WORD-END U<
  WHILE
    ;; ( ofs addrafter pfa )
    DUP (ZX-IS-BRANCH-AT) IF
      ;; this is a branch, do we need to offset it?
      2DUP 2+ @ TOZX  ;; ( ofs addrafter pfa addrafter dest )
      U<= IF
        ;; yep, need to fix it
        ;; ( ofs addrafter pfa )
        ROT >R  ;; ( addrafter pfa | ofs )
        DUP 2+ R@ SWAP +!
        R> NROT
      ENDIF
    ENDIF
    ZX-TRACE-SKIP-INSTR
  REPEAT
  2DROP DROP
;


;; ////////////////////////////////////////////////////////////////////////// //
;; returns number of bytes removed
;; decrements end address
: (ZX-REMOVE-INSTRUCTION-AT)  ( pfa -- diff )
  DUP ZX-TRACE-SKIP-INSTR
  ;; calc diff, to fix the last address later
  2DUP SWAP - >R
  ;; ( dest src )
  BEGIN
    DUP ZX-TRACER-WORD-END U<
  WHILE
    ;; ( dest src )
    DUP >R C@   ;; ( dest [src] | src )
    OVER C!     ;; ( dest | src )
    1+ R> 1+
  REPEAT
  2DROP
  ;; fix length
  ZX-TRACER-WORD-END R@ - TO ZX-TRACER-WORD-END
  R>
;


;; ////////////////////////////////////////////////////////////////////////// //
;; replaces [pfa] with cfa, removes next instruction, fixes branches and labels
: (ZX-OPT-COMBINE)  ( stpfa pfa cfa -- )
  OVER !
  ;; ( stpfa pfa )
    \ DUP ." =========== #" HEX 0xFFFF AND U. DECIMAL CR
  ZX-TRACE-SKIP-INSTR
  DUP (ZX-REMOVE-INSTRUCTION-AT)
  ;; ( stpfa pfanext count )
  ;; now fix all branches
  NEGATE >R  ;; save offset
  2DUP R@
  (ZX-OFS-BRANCHES-AFTER)
  ;; ( stpfa pfanext | ofs )
  ;; fix labels
  ZX-LABELS-COUNT IF
    R> SWAP
    ;; ( stpfa ofs pfanext )
    ZX-LABELS-END-ADDR ZX-LABELS-START-ADDR
    DO
      ;; ( stpfa ofs pfanext )
      I @ OVER U>= IF
        ;; need to fix it
        OVER I +!
      ENDIF
    LOOP
    ;; ( stpfa ofs pfanext )
  ELSE
    R>
  ENDIF
  2DROP
  ;; ( stpfa )
  DROP

    \ (ZX-DECOMP) " !!" UFE-FATAL
;


$IFDEF ENABLE-ZX-OPTIMISER
;; ////////////////////////////////////////////////////////////////////////// //
0 VALUE (ZX-OPT-LIT-CFA)
0 VALUE (ZX-OPT-SWAP-CFA)
0 VALUE (ZX-OPT-DUP-CFA)
0 VALUE (ZX-OPT-OVER-CFA)
0 VALUE (ZX-OPT-DROP-CFA)
0 VALUE (ZX-OPT-2DROP-CFA)
0 VALUE (ZX-OPT-NIP-CFA)
0 VALUE (ZX-OPT-TUCK-CFA)
0 VALUE (ZX-OPT-LSHIFT-CFA)
0 VALUE (ZX-OPT-RSHIFT-CFA)
0 VALUE (ZX-OPT-1-CFA)
0 VALUE (ZX-OPT-2U*-CFA)
0 VALUE (ZX-OPT-2U/-CFA)


;; optimise literals to superinstructions
: (ZX-OPT-LITERAL)  ( stpfa pfa -- diditflag )
  DUP @  ;; ( stpfa pfa cfa )
  ;; early exit
  DUP (ZX-OPT-LIT-CFA) = IFNOT 2DROP DROP 0 EXIT ENDIF
  UR-ZX-WORD-NAME-BY-CFA IFNOT " WTF??!" UFE-FATAL ENDIF
  ;; ( stpfa pfa naddr ncount )
  2DUP " LIT" STR= IF
    ;; get next instruction, and try to build a super one
    ;; ( stpfa pfa naddr ncount )
    ;; copy word name to the safe place
    >R PAD 256 + R@ CMOVE
    ;; ( stpfa pfa | ncount )
    DUP PAD 256 + R>
    ;; ( stpfa pfa pfa naddr ncount )
    ;; move to the next instruction, and get its name
    ROT ZX-TRACE-SKIP-INSTR @
    UR-ZX-WORD-NAME-BY-CFA IFNOT " WTF??!" UFE-FATAL ENDIF
    ;; ( stpfa pfa naddr ncount nnadr nncount )
    STR-CAT
    ;; ( stpfa pfa snaddr sncount )
      \ ." {" XTYPE ." }\n" " !" UFE-FATAL
    UR-ZX-FIND-WORD IF
      ;; there is a superinstruction here
      ;; ( stpfa pfa cfa )
      ;; replace LIT with superinstruction, remove next instruction after LIT
      (ZX-OPT-COMBINE)
      ;; the stack is empty
      1 ;; we did it
      EXIT
    ENDIF
    ;; cannot combine
    2DROP
    0 ;; nothing was done
    EXIT
  ENDIF
  2DROP 2DROP
  0 ;; nothing was done
;


;; optimise some stack operations to superunstructions
: (ZX-OPT-STACK)  ( stpfa pfa -- diditflag )
  DUP @  ;; ( stpfa pfa cfa )
  CASE
    (ZX-OPT-SWAP-CFA) OF
      ;; ( stpfa pfa )
      DUP 2+ @
      CASE
        (ZX-OPT-DROP-CFA) OF  ;; SWAP DROP -> NIP
          $IFDEF DEBUG-ZX-OPTIMISER-STACK-COMBINER
            ." \nCOMBINE: SWAP DROP -> NIP\n"
          $ENDIF
          (ZX-OPT-NIP-CFA) (ZX-OPT-COMBINE)
          1 EXIT
        ENDOF
        (ZX-OPT-OVER-CFA) OF  ;; SWAP OVER -> TUCK
          $IFDEF DEBUG-ZX-OPTIMISER-STACK-COMBINER
            ." \nCOMBINE: SWAP OVER -> TUCK\n"
          $ENDIF
          (ZX-OPT-TUCK-CFA) (ZX-OPT-COMBINE)
          1 EXIT
        ENDOF
      ENDCASE
    ENDOF
    (ZX-OPT-DROP-CFA) OF
      DUP 2+ @ (ZX-OPT-DROP-CFA) = IF  ;; DROP DROP -> 2DROP
        $IFDEF DEBUG-ZX-OPTIMISER-STACK-COMBINER
          ." \nCOMBINE: DROP DROP -> 2DROP\n"
        $ENDIF
        (ZX-OPT-2DROP-CFA) (ZX-OPT-COMBINE)
        1 EXIT
      ENDIF
    ENDOF
  ENDCASE
  2DROP 0
;


;; optimise "1 LSHIFT" to "2U*", and "1 RSHIFT" to "2U/"
: (ZX-OPT-SHIFTS)  ( stpfa pfa -- diditflag )
  DUP @ (ZX-OPT-1-CFA) = IFNOT 2DROP 0 EXIT ENDIF

  DUP 2+ @ (ZX-OPT-LSHIFT-CFA) = IF
    (ZX-OPT-2U*-CFA) (ZX-OPT-COMBINE)
    1 EXIT
  ENDIF

  DUP 2+ @ (ZX-OPT-RSHIFT-CFA) = IF
    (ZX-OPT-2U/-CFA) (ZX-OPT-COMBINE)
    1 EXIT
  ENDIF

  2DROP 0
;


;; ////////////////////////////////////////////////////////////////////////// //
: (ZX-CALL-OPTIMISERS)  ( stpfa pfa -- diditflag )
  2DUP (ZX-OPT-LITERAL) IF 2DROP 1 EXIT ENDIF
  2DUP (ZX-OPT-SHIFTS) IF 2DROP 1 EXIT ENDIF
  (ZX-OPT-STACK)
;


;; ////////////////////////////////////////////////////////////////////////// //
;; checks if we can combine
;; we cannot do that if any label points to the next instruction
;; also, we cannot optimise the last instruction (just in case)
: (ZX-OPT-CAN-COMBINE) ( pfa -- flag )
  ZX-TRACE-SKIP-INSTR
  DUP TOZX ZX-TRACER-WORD-END U< IFNOT DROP 0 ENDIF
  ZX-HAS-LABEL-AT NOT
;


;; ////////////////////////////////////////////////////////////////////////// //
: (ZX-CACHE-WORD-CFA)  ( addr count -- )  \ toname
  2DUP UR-ZX-FIND-WORD IFNOT ." NOT FOUND: <" TYPE ." >!\n" " WTF?!" UFE-FATAL ENDIF
  FROMZX >R  ;; save cfa; we need it as non-zx address for comparisons and such
  ;; build string
  " (ZX-OPT-" STR-TO-PAD
  2SWAP STR-CAT
  " -CFA)" STR-CAT
  R> NROT STRTO
;


;; cache some addresses
: (ZX-INIT-OPTIMISER)  ( -- )
  " LIT"     (ZX-CACHE-WORD-CFA)
  " SWAP"    (ZX-CACHE-WORD-CFA)
  " DUP"     (ZX-CACHE-WORD-CFA)
  " OVER"    (ZX-CACHE-WORD-CFA)
  " DROP"    (ZX-CACHE-WORD-CFA)
  " 2DROP"   (ZX-CACHE-WORD-CFA)
  " NIP"     (ZX-CACHE-WORD-CFA)
  " TUCK"    (ZX-CACHE-WORD-CFA)
  " LSHIFT"  (ZX-CACHE-WORD-CFA)
  " RSHIFT"  (ZX-CACHE-WORD-CFA)
  " 1"       (ZX-CACHE-WORD-CFA)
  " 2U*"     (ZX-CACHE-WORD-CFA)
  " 2U/"     (ZX-CACHE-WORD-CFA)
;


: OPTIMISE-ZX-WORD  ( addr count pfa -- )
  (ZX-INIT-OPTIMISER)

  $IFDEF DEBUG-ZX-OPTIMISER
    DUP >R
    ." *** compiled ZX word at #"
    BASE @ HEX SWAP <# # # # # #> TYPE BASE !
    ." , name is <" TYPE ." >\n"
  $ELSE
    >R 2DROP
  $ENDIF

  DP@ TO ZX-TRACER-SAVED-DP
  R@ ZX-TRACE-LABELS

  $IFDEF DEBUG-ZX-OPTIMISER
    R@ (ZX-DECOMP)
    ZX-HERE 0xFFFF AND HEX U. DECIMAL CR
  $ENDIF

  ;; superinstruction optimiser
  R@ DUP
  ;; ( stpfa pfa )
  BEGIN
    DUP ZX-TRACER-WORD-END U<
  WHILE
    ;; ( stpfa pfa )
    DUP (ZX-OPT-CAN-COMBINE) IF
      2DUP (ZX-CALL-OPTIMISERS)  ;; ( stpfa pfa diditflag )
      ;; if we did some optimisation, process optimised instruction again
    ELSE
      0 ;; do not process this instruction again
    ENDIF
    IFNOT ZX-TRACE-SKIP-INSTR ENDIF
  REPEAT
  2DROP

  $IFDEF DEBUG-ZX-OPTIMISER
    R@ (ZX-DECOMP)
  $ENDIF

  RDROP

  ;; restore HERE
  ZX-TRACER-SAVED-DP DP!

  ZX-HERE TOZX  ZX-TRACER-WORD-END TOZX -
  DUP 0> IF
    ." ZX word " UR-ZX-LAST-FORTH-WORD-NAME XTYPE ." : "
    DUP . ." bytes saved, size: "
    NEGATE ZX-ALLOT
      \ ZX-HERE HEX U. DECIMAL CR
  ELSE
    DROP
    ." ZX word " UR-ZX-LAST-FORTH-WORD-NAME XTYPE ." : size "
  ENDIF
  ZX-HERE ZX-TRACER-WORD-START TOZX - U. ." bytes\n"
;
$ENDIF

;; ////////////////////////////////////////////////////////////////////////// //
0 VALUE ZX-TRACER-SAVED-DP
0 VALUE ZX-TRACER-WORD-START  ;; always ZX address
0 VALUE ZX-TRACER-WORD-END    ;; always ZX address

;; flags
;;   bit 0: branch
;;   bit 1: numlit
;;   bit 2: strlit
;;   bit 3: noreturn
;;   bit 4: noturnkey
;;   bit 5: immediate
;;   bit 6: unconditional branch
;;   bit 7: used mark


;; ////////////////////////////////////////////////////////////////////////// //
: (ZX-WRITE-#HEX4) ( n -- )
  BASE @ HEX SWAP
  <# # # # # [CHAR] # HOLD #> TYPE
  BASE !
;


: (ZX-WRITE-HEX4) ( n -- )
  BASE @ HEX SWAP
  <# # # # # #> TYPE
  BASE !
;


;; ////////////////////////////////////////////////////////////////////////// //
: ZX-LABELS-COUNT ( -- addr )
  ZX-TRACER-SAVED-DP @
;

: ZX-LABELS-START-ADDR ( -- addr )
  ZX-TRACER-SAVED-DP 1+
;

: ZX-LABELS-END-ADDR ( -- addr )
  ZX-LABELS-START-ADDR ZX-LABELS-COUNT +
;

: ZX-HAS-LABEL-AT ( addr -- flag )
  ZX-LABELS-COUNT IFNOT DROP 0 EXIT ENDIF
  TOZX
  ZX-LABELS-END-ADDR ZX-LABELS-START-ADDR
  DO
    DUP I @ = IF DROP RDROP RDROP 1 EXIT ENDIF
  LOOP
  DROP 0
;

: ZX-HAS-LABEL-AFTER ( addr -- flag )
  ZX-LABELS-COUNT IFNOT DROP 0 EXIT ENDIF
  TOZX
  ZX-LABELS-END-ADDR ZX-LABELS-START-ADDR
  DO
    DUP I @ U< IF DROP RDROP RDROP 1 EXIT ENDIF
  LOOP
  DROP 0
;

: ZX-APPEND-LABEL ( addr -- )
    \ DUP ." checking label: #" HEX U. DECIMAL ZX-TRACER-SAVED-DP @ ." (labels:" 0 .R ." )\n"
  DUP ZX-HAS-LABEL-AT IF
      \ DUP ." duplicate label: #" HEX U. DECIMAL CR
    DROP
  ELSE
      \ DUP ." new label: #" HEX U. DECIMAL CR
    ZX-TRACER-SAVED-DP 1+!
    TOZX ,
  ENDIF
;


;; ////////////////////////////////////////////////////////////////////////// //
: ZX-TRACE-SKIP-INSTR ( pfa -- nextpfa )
  DUP @ UR-ZX-WORD-FLAGS-BY-CFA SWAP 2+
  ;; ( flags pfa )
  OVER FWF-CODEBLOCK AND IF 5 + SWAP DROP 0 SWAP ENDIF
  OVER FWF-BRANCH FWF-NUMLIT OR FWF-COMPILE OR AND IF 2+ ENDIF
  OVER FWF-STRLIT AND IF ZX-BCOUNT + ENDIF
  SWAP DROP
;


: ZX-PRINT-INSTR-ARGS ( pfa -- )
  DUP @ UR-ZX-WORD-FLAGS-BY-CFA SWAP 2+
  ;; ( flags pfa )
  ;; codeblock?
  OVER FWF-CODEBLOCK AND IF
    ;; codeblock is followed by jump destination, then cfa, then threaded code
    ."  L" @ (ZX-WRITE-HEX4)
    DROP EXIT
  ENDIF
  ;; branch?
  OVER FWF-BRANCH AND IF
    ."  L" @ (ZX-WRITE-HEX4)
    DROP EXIT
  ENDIF
  ;; numlit?
  OVER FWF-NUMLIT AND IF
    @ SPACE 0 U.R
    DROP EXIT
  ENDIF
  ;; strlit?
  OVER FWF-STRLIT AND IF
    ZX-BCOUNT ."  ~" XTYPE ." ~"
    DROP EXIT
  ENDIF
  ;; compile?
  OVER FWF-COMPILE AND IF
    @ UR-ZX-WORD-NAME-BY-CFA IF SPACE XTYPE ELSE ."  ???" ENDIF
    DROP EXIT
  ENDIF
  2DROP
;


;; ////////////////////////////////////////////////////////////////////////// //
: ZX-TRACE-LABELS ( pfa -- )
  DUP TO ZX-TRACER-WORD-START
  0 ,  ;; label count
  BEGIN
    ;; ( pfa )
    DUP @  ;; ( pfa cfa )
    UR-ZX-WORD-FLAGS-BY-CFA
    SWAP 2+ ;; move to the next word
    ;; ( flags pfa )
    ;; codeblock?
    OVER FWF-CODEBLOCK AND IF
      ;; codeblock is followed by jump destination, then cfa, then threaded code
      ;; it is guaranteed to jump forward, so take a shortcut here
      DUP @ ZX-APPEND-LABEL  ;; append label
      5 +  ;; skip branch and CFA; we want to continue tracing inside a codeblock anyway
      ;; no need to anaylze it further
      SWAP DROP 0 SWAP
    ENDIF
    ;; branch?
    OVER FWF-BRANCH AND IF
      DUP @ ZX-APPEND-LABEL
      2+  ;; skip branch destination
      ;; check if this is backward unconditional branch
      OVER FWF-UNCONDITIONAL AND IF
        ;; check if this is backward branch
        ;; ( flags pfa )
        DUP 2- @ TOZX  ;; ( flags pfa dest )
        OVER TOZX      ;; ( flags pfa dest pfa )
        ;; ( flags pfa dest pfa )
        U<= IF
          ;; check if we should stop here
          ;; ( flags pfa )
          DUP ZX-HAS-LABEL-AFTER
          IFNOT
            ;; no more code
            TO ZX-TRACER-WORD-END
            DROP EXIT
          ENDIF
        ENDIF
      ENDIF
      SWAP FWF-NORETURN BITNOT AND SWAP
    ENDIF
    ;; numlit?
    OVER FWF-NUMLIT AND IF
      2+
    ENDIF
    ;; strlit?
    OVER FWF-STRLIT AND IF
      ZX-BCOUNT +
    ENDIF
    ;; compile?
    OVER FWF-COMPILE AND IF
      2+
    ENDIF
    ;; noreturn?
    OVER FWF-NORETURN AND IF
      ;; ( flags pfa )
      ;; check if we have any label after this word
      DUP 2- ZX-HAS-LABEL-AFTER
      IFNOT
        TO ZX-TRACER-WORD-END
        DROP EXIT
      ENDIF
    ENDIF
    SWAP DROP  ;; drop flags
  AGAIN
;


;; ////////////////////////////////////////////////////////////////////////// //
;; labels must be already traced
: (ZX-DECOMP)  ( pfa -- )
  DUP 3- UR-ZX-WORD-NAME-BY-CFA IFNOT " WTF??!" UFE-FATAL ENDIF ." === " TYPE ."  ===\n"
  BEGIN
    DUP ZX-TRACER-WORD-END U<
  WHILE
    ;; address
    DUP (ZX-WRITE-#HEX4) ." :"
    ;; has label?
    DUP ZX-HAS-LABEL-AT IF
      ." L" DUP (ZX-WRITE-HEX4) CR
      DUP (ZX-WRITE-#HEX4) ." : "
    ELSE
      SPACE
    ENDIF
    ;; word name
    DUP @ UR-ZX-WORD-NAME-BY-CFA IFNOT " WTF?!" UFE-FATAL ENDIF XTYPE
    ;; word args
    DUP ZX-PRINT-INSTR-ARGS
    CR
    ZX-TRACE-SKIP-INSTR
  REPEAT
  DROP
;

;; ////////////////////////////////////////////////////////////////////////// //
;; ZX forth word types

 0 CONSTANT FWT-OTHER
 1 CONSTANT FWT-CODE
 2 CONSTANT FWT-FORTH
 3 CONSTANT FWT-VAR
 4 CONSTANT FWT-DVAR
 5 CONSTANT FWT-CONST
 6 CONSTANT FWT-VALUE
 7 CONSTANT FWT-DCONST
 8 CONSTANT FWT-DEFER
 9 CONSTANT FWT-DOES


;; ////////////////////////////////////////////////////////////////////////// //
;; ZX forth flag bits

0x0001 CONSTANT FWF-BRANCH
0x0002 CONSTANT FWF-NUMLIT
0x0004 CONSTANT FWF-STRLIT
0x0008 CONSTANT FWF-NORETURN
0x0010 CONSTANT FWF-NOTURNKEY
0x0020 CONSTANT FWF-IMMEDIATE
0x0040 CONSTANT FWF-UNCONDITIONAL  ;; unconditional branch
0x0080 CONSTANT FWF-USED-MARK
0x0100 CONSTANT FWF-CODEBLOCK
0x0200 CONSTANT FWF-COMPILE


;; ////////////////////////////////////////////////////////////////////////// //
\ : 1+!  ( addr -- )  DUP @ 1+ SWAP !  ;

32 CONSTANT BL

\ : SPACES  ( n -- )
\   DUP 0> IF 0 DO SPACE LOOP ELSE DROP ENDIF
\ ;

: [CHAR]  ( -- )
  BL WORD COUNT
  1 = IFNOT ." [CHAR] expects a char" UFE-FATAL ENDIF
  C@ LITERAL
; IMMEDIATE


: ZX-BCOUNT  ( addr -- zxaddr+1 bytecount )
  TOZX
  DUP C@
  SWAP 1+ SWAP
;


: ABS  ( n -- n )  DUP 0< IF NEGATE ENDIF ;


;; ////////////////////////////////////////////////////////////////////////// //
: STR-CAT-CHAR  ( addr count char -- addr count+ )
  >R 2DUP + R> SWAP C!
  1+
;

: STR-CAT  ( addr count addr1 count1 -- addr count+count1 )
  ?DUP IF
    OVER + SWAP DO
      I C@ STR-CAT-CHAR
    LOOP
  ELSE
    DROP
  ENDIF
;

;; copy string to PAD (count is not set)
: STR-TO-PAD  ( addr count -- pad+1 count )
  PAD 1+ 0 2SWAP STR-CAT
;


;; ////////////////////////////////////////////////////////////////////////// //
0 VARIABLE (#BUF-CURR-OFS)
312 VALUE (#BUF-PAD-OFS)

: (#BUF)  ( -- addr ) PAD (#BUF-PAD-OFS) + ;

: HOLD  ( ch -- )
  (#BUF) (#BUF-CURR-OFS) @ -
  DUP HERE <= IF " number too long" UFE-FATAL ENDIF
  C!
  (#BUF-CURR-OFS) 1+!
;

: <#  ( n -- n )
  0 (#BUF-CURR-OFS) !
;

: <#n  ( n -- n )  \ exactly the same as "<#", but different for ZX
  <#
;

: #>  ( n -- addr count )
  DROP
  (#BUF) (#BUF-CURR-OFS) @ - 1+
  (#BUF-CURR-OFS) @
;

: #  ( n -- n )
  DUP BASE @ UMOD 48 +    ;; ( n ch )
  DUP 57 > IF 7 + ENDIF   ;; ( n ch )
  HOLD                    ;; ( n )
  BASE @ U/               ;; ( n/base )
;

: #S  ( n -- n )
  BEGIN
    #
  DUP NOT UNTIL
;

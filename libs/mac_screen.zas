;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; various macros for screen$


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; convert character coords to screen$ bitmap address
;; if srcpair[0] is equal to dstpair[0], one stack slot will be used
;; WARNING! coords must be valid!
;; in:
;;   srcpair[0]: y (char)
;;   srcpair[1]: x (char)
;; out:
;;   destpair: scr$addr
;;   AF: dead
MACRO scrAddr8XX_fromXX  destpair, srcpair
  ;$printf  "srcpair[0]len=%d (%s)", strlen(marg2str(=srcpair[0])), marg2str(=srcpair[0])
  ld    a,=srcpair[0]
  IF marg2str(=srcpair[0]) == marg2str(=destpair[0])
  ; we'll need A later
  push  af
  ENDIF
  and   #18
  or    #40
  ld    =destpair[0],a
  IF marg2str(=srcpair[0]) == marg2str(=destpair[0])
  pop   af
  ELSE
  ld    a,=srcpair[0]
  ENDIF
  rrca
  rrca
  rrca
  and   #E0
  or    =srcpair[1]
  ld    =destpair[1],a
ENDM


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; down to the next screen$ line
;; in:
;;   regpair: scr$addr
;;   skipcmd: command to use to skip sections: "ret"/"jp"/"jr" (default:"jp")
;;   skipinc: if !0, skip first increment (default:0)
;;   skipaload: skip first A loading (A must contain =regpair[0]) (default:0)
;; out:
;;   regpair: scraddrnext -- next y line
;;   AF: dead
MACRO scrDownXX  regpair, skipcmd="jp", skipinc=0, skipaload=0
  ;$printf "regpair=%c%c; skipcmd=<%s>; skipinc=%d; skipaload=%d", =regpair[0], =regpair[1], =skipcmd, =skipinc, =skipaload
  IF =skipinc == 0
  inc  =regpair[0]
  ENDIF
  IF =skipaload == 0
  ld   a,=regpair[0]
  ENDIF
  and  #07
  IF =skipcmd == "ret"
  ret  nz
  ELSEIF =skipcmd == "jp"
  jp   nz,..done
  ELSEIF =skipcmd == "jr"
  jr   nz,..done
  ELSE
  $ERROR "skipcmd must be 'ret'/'jp'/'jr'"
  ENDIF
  ld   a,=regpair[1]
  add  a,32
  ld   =regpair[1],a
  IF =skipcmd == "ret"
  ret  c
  ELSEIF =skipcmd == "jp"
  jp   c,..done
  ELSEIF =skipcmd == "jr"
  jr   c,..done
  ELSE
  $ERROR "skipcmd must be 'ret'/'jp'/'jr'"
  ENDIF
  ld   a,=regpair[0]
  sub  8
  ld   =regpair[0],a
  ; add last ret
  IF =skipcmd == "ret"
  ret
  ELSE
..done:
  ENDIF
ENDM


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; up to the previous screen$ line
;; in:
;;   regpair: scr$addr
;;   skipcmd: command to use to skip sections: "ret"/"jp"/"jr" (default:"jp")
;;   skipdec: if !0, skip first increment (default:0)
;;   skipaload: skip first A loading (A must contain =regpair[0]) (default:0)
;; out:
;;   regpair: scraddrnext -- previous y line
;;   AF: dead
MACRO scrUpXX  regpair, skipcmd="jp", skipdec=0, skipaload=0
  ;$printf "regpair=%c%c; skipcmd=<%s>; skipdec=%d; skipaload=%d", =regpair[0], =regpair[1], =skipcmd, =skipdec, =skipaload
  IF =skipaload == 0
  ld   a,=regpair[0]
  ENDIF
  IF =skipdec == 0
  dec  =regpair[0]
  ENDIF
  and  #07
  IF =skipcmd == "ret"
  ret  nz
  ELSEIF =skipcmd == "jp"
  jp   nz,..done
  ELSEIF =skipcmd == "jr"
  jr   nz,..done
  ELSE
  $ERROR "skipcmd must be 'ret'/'jp'/'jr'"
  ENDIF
  ld   a,=regpair[1]
  sub  32
  ld   =regpair[1],a
  IF =skipcmd == "ret"
  ret  c
  ELSEIF =skipcmd == "jp"
  jp   c,..done
  ELSEIF =skipcmd == "jr"
  jr   c,..done
  ELSE
  $ERROR "skipcmd must be 'ret'/'jp'/'jr'"
  ENDIF
  ld   a,=regpair[0]
  add  a,8
  ld   =regpair[0],a
  ; add last ret
  IF =skipcmd == "ret"
  ret
  ELSE
..done:
  ENDIF
ENDM


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; convert bitmap address to attribute address
;; in:
;;   regpair: scr$addr
;;   skipaload: skip first A loading (A must contain =regpair[0]) (default:0)
;;   skipastore: skip final A storing (default:0)
;;   attrbasehi: high byte of the first attribute address
;; out:
;;   regpair: attraddr -- attribute address
;;   AF: dead
MACRO scr2AttrXX  regpair, skipaload=0, skipastore=0, attrbasehi=#58
  IF =skipaload == 0
  ld   a,=regpair[0]
  ENDIF
  IF =attrbasehi == #58
  ; fast code by Lethargeek
  or    #87
  rra
  rra
  srl   a    ;; rra for #C000 screen
  ELSEIF =attrbasehi == #C0
  or    #87
  rra
  rra
  rra
  ELSE
  rrca
  rrca
  rrca
  and  #03
  or   =attrbasehi
  ENDIF
  IF =skipastore == 0
  ld   =regpair[0],a
  ENDIF
ENDM


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; convert bitmap address to attribute address
;; works for #4000 and for #C000
;; in:
;;   regpair: attraddr
;;   skipaload: skip first A loading (A must contain =regpair[0]) (default:0)
;;   skipastore: skip final A storing (default:0)
;; out:
;;   regpair: scr$addr
;;   AF: dead
MACRO scrFromAttrXX  regpair, skipaload=0, skipastore=0
  IF =skipaload == 0
  ld   a,=regpair[0]
  ENDIF
  ; tnx, Lethargeek
  add   a
  add   a
  add   a
  and   =regpair[0]
  ENDIF
  IF =skipastore == 0
  ld   =regpair[0],a
  ENDIF
ENDM


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; calculate address in ROM font for the given char
;; WARNING! char must be in [32..127] range!
;; in:
;;   srcreg: charcode reg8 (can be empty to avoid load at all)
;;           default is A; if empty, load code to 'L'
;; out:
;;   HL: scraddrnext -- previous y line
;;   flags: dead (not yet, but i reserve it)
MACRO calcROMCharHL_A  srcreg=a
  IF strlen(marg2str(=srcreg)) > 0
    IF marg2str(=srcreg[0]) != "l"
    ld    l,=srcreg[0]
    ENDIF
  ENDIF
  add  hl,hl
  ld   h,15
  add  hl,hl
  add  hl,hl
ENDM


  ;scrDownXX  hl
  ;nop
  ;scrDownXX  de,"jr",:skipaload=1
  ;nop
  ;scrDownXX  bc,"jp",:skipinc=1
  ;nop
  ;scrDownXX  hl,"ret"

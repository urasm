#!/bin/sh

function dotest {
  inname="$1"
  name=`basename "$inname" .asm`
  #pasmo "$inname" "${name}.raw"
  #res=$?
  #if [ $res != 0 ]; then
  #  echo "PASMO FAILED: $name"
  #  exit 1
  #fi
  urasm --raw "$inname"
  if [ $res != 0 ]; then
    echo "URASM-OLD FAILED: $name"
    exit 1
  fi
  mv "${name}_0100.bin" "${name}.raw"
  urasm-new --org100h --raw "$inname"
  cmp -b "${name}.raw" "${name}_0100.bin"
  res=$?
  if [ $res = 0 ]; then
    rm "${name}.raw"
    mv "${name}_0100.bin" _out/
    echo "*** RESULT: OK"
  else
    echo "************************* FAILED!"
    exit 1
  fi
}


mkdir _out 2>/dev/null

if [ $# -eq 0 ]; then
  for asrc in *.asm; do
    dotest "$asrc"
  done
else
  while [ $# -ne 0 ]; do
    dotest "$1"
    shift
  done
fi

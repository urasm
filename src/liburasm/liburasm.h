/* coded by Ketmar // Invisible Vector (ketmar@ketmar.no-ip.org)
 * Understanding is not required. Only obedience.
 *
 * URASM Z80 assembler/disassembler core v0.1.3 (with ZXNext support)
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 */
#ifndef LIBURASM_HEADER
#define LIBURASM_HEADER

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif


/******************************************************************************/
/* all known tokens (opcode mnemonics)                                        */
/******************************************************************************/
enum {
  UT_ADC,  UT_ADD,  UT_AND,  UT_BIT,
  UT_CALL, UT_CCF,  UT_CP,   UT_CPD,
  UT_CPDR, UT_CPI,  UT_CPIR, UT_CPL,
  UT_DAA,  UT_DEC,  UT_DI,   UT_DJNZ,
  UT_EI,   UT_EX,   UT_EXX,  UT_HALT,
  UT_IM,   UT_IN,   UT_INC,  UT_IND,
  UT_INDR, UT_INI,  UT_INIR, UT_JP,
  UT_JR,   UT_LD,   UT_LDD,  UT_LDDR,
  UT_LDI,  UT_LDIR, UT_NEG,  UT_NOP,
  UT_OR,   UT_OTDR, UT_OTIR, UT_OUT,
  UT_OUTD, UT_OUTI, UT_POP,  UT_PUSH,
  UT_RES,  UT_RET,  UT_RETI, UT_RETN,
  UT_RL,   UT_RLA,  UT_RLC,  UT_RLCA,
  UT_RLD,  UT_RR,   UT_RRA,  UT_RRC,
  UT_RRCA, UT_RRD,  UT_RST,  UT_SBC,
  UT_SCF,  UT_SET,  UT_SLA,  UT_SLI,
  UT_SLL,  UT_SRA,  UT_SRL,  UT_SUB,
  UT_XOR,  UT_XSLT, UT_NOPX, UT_NOPY,
  /* ZXNext */
  UT_LDIX,    UT_LDWS,   UT_LDIRX,   UT_LDDX,
  UT_LDDRX,   UT_LDPIRX, UT_OUTINB,  UT_MUL,
  UT_SWAPNIB, UT_MIRROR, UT_NEXTREG, UT_PIXELDN,
  UT_PIXELAD, UT_SETAE,  UT_TEST,    UT_BSLA,
  UT_BSRA,    UT_BSRL,   UT_BSRF,    UT_BRLC,
  /* max values */
  URASM_MAX_TOKEN,
  URASM_MAX_NORMAL_TOKEN = UT_NOPY+1
};
extern const char *URASM_TOKENS[URASM_MAX_TOKEN];


/* can be used to decode various command operands */
extern const char *URA_REGS8[8]; /* 8-bit register names */
extern const char *URA_REGS16[4]; /* 16-bit register names, with SP, without AF */
extern const char *URA_REGS16A[4]; /* 16-bit register names, with AF, without SP */
extern const char *URA_COND[8]; /* branch condition names */


/* all possible operands */
/* there are so many for proper assembling */
/* i wanted to perform a simple table lookup w/o special cases, */
/* so all special cases were encoded as various operand types */
enum {
  UO_NONE,
  UO_IMM8,   /* immediate constant */
  UO_IMM16,  /* immediate constant */
  UO_IMM16BE,/* immediate constant, big-endian */
  UO_ADDR16, /* immediate address (JP/CALL) */
  UO_ADDR8,  /* immediate address (JR/DJNZ) */
  UO_MEM16,  /* immediate memory (nnnn) */
  /* 8 bit registers (bits 0-2 of opcode) */
  UO_R8,     /* B,C,D,E,H,L,(HL),A */
  UO_R8_NOM, /* B,C,D,E,H,L,A, but no (HL) */
  UO_R8_NOM_NOHL, /* B,C,D,E,A, but no H,L,(HL) */
  /* 8 bit registers (bits 3-5 of opcode) */
  UO_2R8,    /* B,C,D,E,H,L,(HL),A */
  UO_2R8_NOM,/* B,C,D,E,H,L,A, but no (HL) */
  UO_2R8_NOM_NOHL, /* B,C,D,E,A, but no H,L,(HL) */
  /* for port i/o */
  UO_PORTC,  /* (C) */
  UO_PORTIMM,/* (nn) */
  /* special 8 bit registers */
  UO_R8_XH,  /* XH */
  UO_R8_XL,  /* XL */
  UO_R8_YH,  /* YH */
  UO_R8_YL,  /* YL */
  UO_R8_A,   /* A */
  UO_R8_B,   /* B */
  UO_R8_R,   /* R */
  UO_R8_I,   /* I */
  /* 16 bit registers (bits 4-5 of opcode), with SP */
  UO_R16,    /* BC,DE,HL,SP */
  /* 16 bit registers (bits 4-5 of opcode), with AF */
  UO_R16A,   /* BC,DE,HL,AF */
  /* AF & AF' for EX AF,AF' */
  UO_R16AF,  /* AF */
  UO_R16AFX, /* AF' */
  UO_R16BC,  /* BC */
  UO_R16DE,  /* DE */
  UO_R16HL,  /* HL */
  UO_R16IX,  /* IX */
  UO_R16IY,  /* IY */
  UO_R16SP,  /* SP */
  UO_MSP,    /* (SP) */
  UO_MBC,    /* (BC) */
  UO_MDE,    /* (DE) */
  UO_MHL,    /* (HL) */
  UO_MIX,    /* (IX+disp) */
  UO_MIY,    /* (IY+disp) */
  UO_MIX0,   /* (IX) */
  UO_MIY0,   /* (IY) */
  /* JR condition (bits 3-4 of opcode) */
  UO_JRCOND,
  /* conditions (bits 3-5 of opcode) */
  UO_COND,
  /* CB opcodes -- bit numbers (bits 3-5 of opcode) */
  UO_BITN,   /* 0..7 */
  /* RST address (bits 3-5 of opcode <-- (address shr 3)) */
  UO_RSTDEST,
  /* IM operands */
  UO_IM0,   /* not necessary for IM, denotes any 0 */
  UO_IM1,   /* not necessary for IM, denotes any 1 */
  UO_IM2,   /* not necessary for IM, denotes any 2 */
  UO_NEXT,  /* for op3: this is ZXNext instruction */
  /*UO_IM01*/   /* undocumented IM 0/1 */
};


typedef struct {
  unsigned mnemo; /* see UT_xxx */
  uint32_t code; /* Z80 machine code */
  uint32_t mask; /* mask (for disassembler) */
  unsigned ops[3]; /* see UO_xxx */
} urasm_cmdinfo_t;


/* the longest matches must come first (for disassembler) */
/* solid-masked must come first (for disassembler) */
/* assembler searches the table from the last command */
/* disassembler searches the table from the first command */
/* heh, i spent the whole night creating this shit! %-) */
#define URASM_COMMANDS_NEXT  (29)
#define URASM_MAX_COMMAND    (366+URASM_COMMANDS_NEXT)
extern const urasm_cmdinfo_t URASM_COMMANDS[URASM_MAX_COMMAND];


/******************************************************************************/
/* callbacks                                                                  */
/******************************************************************************/

/* fixup types */
typedef enum {
  UR_FIXUP_NONE,
  UR_FIXUP_WORD,
  UR_FIXUP_LOBYTE,
  UR_FIXUP_HIBYTE
} urasm_fixup_type_t;


/* return label name for the given address or NULL for "no such label" */
typedef const char *(*urasm_label_by_addr_fn) (uint16_t addr);

/* addr is current PC */
/* return 0 for undefined labels */
/* set `*found` to 1 or library will barf */
/* set `*fixuptype` to UR_FIXUP_XXX (default: UR_FIXUP_NONE) */
/* if label value is not known yet, set `*defined` to 0 */
/* (this means that the label was used, but not yet defined) */
typedef int32_t (*urasm_label_by_name_fn) (const char *name, uint16_t addr,
                                           int *defined, int *found, int *fixuptype);

/* get byte at the given memory address */
typedef uint8_t (*urasm_getbyte_fn) (uint16_t addr);
/* set byte at the given memory address */
typedef void (*urasm_putbyte_fn) (uint16_t addr, uint8_t b);


/* allow ZXNext Z80N opcodes in assembler/disassembler? */
/* default is 0 (not allowed) */
extern int urasm_allow_zxnext;

/* set to non-zero to use decimal numeric literals in disassembler */
/* default is 0 (i.e. use hexadecimal) */
extern int urasm_disasm_decimal;

/* what to put after mnemonics (must not be empty/NULL, must not be bigger than 3 chars */
/* default is "\t" */
extern const char *urasm_disasm_mnemo_delimiter;

/* what to put between operands (must not be empty/NULL, must not be bigger than 3 chars */
/* default is "," */
extern const char *urasm_disasm_operand_delimiter;


/* use old priority table? default is 0 */
extern int urasm_use_old_priorities;

/* allow "&abc" hex numbers? */
extern int urasm_allow_hex_castrates;


/* callback for disasm; default is `NULL` */
extern urasm_label_by_addr_fn urasm_label_by_addr;
/* callback for disasm; default is `NULL` */
extern urasm_getbyte_fn urasm_getbyte;


/* callback for asm; default is `NULL` */
extern urasm_label_by_name_fn urasm_label_by_name;
/* callback for asm; default is `NULL` */
extern urasm_putbyte_fn urasm_putbyte;


/******************************************************************************/
/* disassembler                                                               */
/******************************************************************************/

/* returns length of the corresponding instruction */
/* it is always >0 (even for invalid idx) */
/* idx is index retunred by `urasm_disasm_opfind()` */
/* doesn't read memory */
int urasm_disasm_oplen (int idx);

/* find the corresponding record in URASM_COMMANDS */
/* returns -1 if memory read callback is not set, or command not found */
/* returns -2 for invalid ED prefix */
int urasm_disasm_opfind (uint16_t addr);

/* returns disassembled code in `dstr` & instruction length */
/* reserve 128 bytes for `dstr` (for default delimiters) */
/* opcode separated from operands with `urasm_disasm_mnemo_delimiter` */
/* operands are separated with `urasm_disasm_operand_delimiter` */
/* this one doesn't use memory read callback */
/* always fill all 8 bytes in `mem`, the code knows how much it needs */
/* returns -1 on error */
int urasm_disasm_opdisasm_ex (char *dstr, uint16_t addr, const uint8_t mem[8]);

/* returns disassembled code in `dstr` & instruction length */
/* reserve 128 bytes for `dstr` (for default delimiters) */
/* opcode separated from operands with `urasm_disasm_mnemo_delimiter` */
/* operands are separated with `urasm_disasm_operand_delimiter` */
/* returns -1 on error */
int urasm_disasm_opdisasm (char *dstr, uint16_t addr);


/******************************************************************************/
/* scanner                                                                    */
/******************************************************************************/

enum {
  UR_EXPRERR_NONE    = 0,
  UR_EXPRERR_EOS     = 1,
  UR_EXPRERR_DIV0    = 2,
  UR_EXPRERR_PARENS  = 3,
  UR_EXPRERR_NUMBER  = 4,
  UR_EXPRERR_STRING  = 5,
  UR_EXPRERR_LABEL   = 6,
  UR_EXPRERR_TERM    = 7,
  UR_EXPRERR_FUNC    = 8,
  UR_EXPRERR_TYPE    = 9,
  UR_EXPRERR_MARG    = 10,
  UR_EXPRERR_FIXUP   = 11,
  UR_EXPRERR_TYPES   = 12,
  UR_EXPRERR_TOOLONG = 13, /* operand text too long */
};


typedef struct {
  int32_t val;     /* for now */
  char *str;       /* NULL: non-string value; val is set for strings too (see dox, hehe) */
  int fixuptype;   /* UR_FIXUP_XXX; can be changed only by low() and high() */
  /* intertal fields */
  const char *pos; /* for internal use */
} urasm_exprval_t;


typedef struct {
  char s[258];   /* operand source text */
  int special;   /* bool: register or another reserved word */
  int mem;       /* bool: (...) */
  int32_t v;     /* expression value or index */
  int defined;   /* bool: expression defined? */
  uint8_t ixy;   /* $DD,$FD or 0 */
  int parsed;    /* was this operand sucessfully parsed? */
  int fixuptype; /* UR_FIXUP_XXX */
} urasm_operand_t;


/* returns 0 if label name conflicts with reserved word, or 1 for valid label name */
/* (or if the label is just a bad one) */
int urasm_is_valid_name (const char *lbl);

/* initialize expression evaluator */
void urasm_exprval_init (urasm_exprval_t *res);
/* clear initialised expression evaluator (frees `str` if necessary) */
void urasm_exprval_clear (urasm_exprval_t *res);
/* frees `str` if necessary */
void urasm_exprval_setint (urasm_exprval_t *res, int32_t v);
/* reallocs `str`; sets `val` for double quotes */
void urasm_exprval_tostr (urasm_exprval_t *res, const char *str);
/* reallocs `str`; sets `val` for single quotes */
void urasm_exprval_tostr_rev (urasm_exprval_t *res, const char *str);

/* will be called when parser encounters term starting with '=' or '*' (can be used for macros) */
/* first term char will *not* be skipped */
/* must return pointer to the first char after expression end */
/*   res: initialised; set this to expression result on exit */
/*   expr: rest of the expression; return advanced expr */
/*   addr: current PC */
/*   donteval: !0 if you don't need to evaluate anything; used in bool short-circuiting */
/*   *defined: set this to 0 if value is undefined yet (this is not necessarily an error) */
/*   *error: set this to `UR_EXPRERR_XXX` to indicate some error */
typedef const char *(*urasm_getval_fn) (urasm_exprval_t *res, const char *expr,
                                        uint16_t addr, int donteval,
                                        int *defined, int *error);

extern urasm_getval_fn urasm_getval;

/* returns !0 to signal error; oprlen is opr size (w/o ending 0), it's 255 for now */
/* this is called to expand macro operand (starting with '=') */
/* useful in macros to replace arguments with their values without evaluating */
/* somewhat quirky: will only be called if first operand */
/* char is '=', not for any special-prefixed name */
/* to expand, parse `opr` from the start, remove parsed token, and insert expansion */
/* take care for not using more than `oprlen` bytes (ending 0 is not counted, */
/* so effective buffer size is `oprlen+1`) */
typedef int (*urasm_expand_fn) (char *opr, int oprlen);

extern urasm_expand_fn urasm_expand;

/* parse and calculate expression */
/* returns position after the expression (spaces skipped) or error position */
/*   res: numeric result will be put here */
/*   expr: expression; return advanced expr */
/*   addr: current PC */
/*   *defined: will be set to 0 if value is undefined yet (this is not necessarily an error) */
/*   *fixuptype: will be set to `UR_FIXUP_XXX` */
/*   *error: will be set this to `UR_EXPRERR_XXX` to indicate some error */
const char *urasm_expr (int32_t *res, const char *expr, uint16_t addr,
                        int *defined, int *fixuptype, int *error);

/* `res` must be initialised! */
/* returns position after the expression (spaces skipped) or error position */
/*   *res: will be filled with expression result; must be initialised before calling! */
/*   expr: expression; return advanced expr */
/*   addr: current PC */
/*   *donteval: set to !0 to simply skip the expression; might be set on exit to indicate short-circuiting */
/*   *defined: will be set to 0 if value is undefined yet (this is not necessarily an error) */
/*   *fixuptype: will be set to `UR_FIXUP_XXX` */
/*   *error: will be set this to `UR_EXPRERR_XXX` to indicate some error */
const char *urasm_expr_ex (urasm_exprval_t *res, const char *expr, uint16_t addr,
                           int *donteval, int *defined, int *error);

/* note that starting '(' is skipped, and ending ')' must be skipped by fn */
/* returns position after the expression (spaces skipped) or error position */
/*   *res: fill with expression result; it is already initialised */
/*   expr: expression; return advanced expr */
/*   addr: current PC */
/*   donteval: !0 if you don't need to evaluate anything; used in bool short-circuiting */
/*   *defined: will be set to 0 if value is undefined yet (this is not necessarily an error); you may change it */
/*   *error: set this to `UR_EXPRERR_XXX` to indicate some error */
typedef const char *(*urasm_func_fn) (urasm_exprval_t *res, const char *expr,
                                      uint16_t addr, int donteval,
                                      int *defined, int *error);

/* fn==NULL: remove */
/* duplicate will be silently replaced */
/* use lower-case names */
void urasm_expr_register_func (const char *name, urasm_func_fn fn);

/* returns pointer to registered function, or NULL */
urasm_func_fn urasm_expr_find_func (const char *name);

/* result==NULL: no more operands */
/* skip operand; operands dilimited with ",", ":", ";" */
/* returns expr advanced to delimiter, or NULL of there are no more operands */
/* returned string may start with ":" (used for colon-delimited operators) */
/* note that NULL is returned only if there weren't any non-blank chars */
/* i.e. for last operand result will ne non-NULL (and may point to zero byte) */
/* correctly skips string and parens */
/*   seenarg: if not NULL, set to !0 if seen any non-space chars */
const char *urasm_skip_operand (const char *expr, int *seenarg);

/* result==NULL: no more operands (see above for skip, tho) */
/* parse operand; op will be reset */
/* returns expr advanced to delimiter, or NULL of there are no more operands */
/*   op: will be reset and filled with parsed operand data */
/*   expr: expression; returns advanced expr */
/*   addr: current PC */
/*   *error: will be set to `UR_EXPRERR_XXX` to indicate some error */
/* `op->parsed` will be set even for empty operand (i.e. immediately terminated with ",") */
/* `op->parsed` will be reset on error */
const char *urasm_next_operand (urasm_operand_t *op, const char *expr,
                                uint16_t addr, int *error);

/* tests operand validity, returns 0 if passed `op` is not valid */
/* for regsters & conditions: op->v will be set to reg number */
/* doesn't check `op->parsed` */
/*   op: parsed operand (result of successfull `urasm_next_operand()`) */
/*   addr: current PC */
/*   optype: required operand type (see `UO_XXX`) */
int urasm_is_valid_operand (urasm_operand_t *op, uint16_t addr, unsigned optype);


/******************************************************************************/
/* assembler                                                                  */
/******************************************************************************/

enum {
  URA_GENERIC = -1, /* call error (generic) */
  URA_BAD_MNEMO = -2, /* bad mnemonics */
  URA_BAD_OPERAND = -3, /* bad operand */
  URA_EXTRA_TEXT = -4, /* extra text after instruction */
  URA_BAD_INSTRUCTION = -5, /* bad instruction */
  URA_EXPR_ERR = -666, /* expression error; to get a real code (`UR_EXPRERR_XXX`), subtract `URA_EXPR_ERR` */
  URA_NOERROR = 0
};

/* called when `op->fixuptype` set to something else than `UR_FIXUP_NONE` */
/* also called on some special cases even if `op->fixuptype` is equal to `UR_FIXUP_NONE` */
/*   op: operand */
/*   opdestaddr: destination address (i.e. address to put into fixup table) */
/*   opaddr: operand address */
/*   fixuptype: type of fixup; use this instead of `op->fixuptype`! */
/*   size: fixup size in bytes; 1 or 2 */
typedef void (*urasm_fixup_operand_fn) (const urasm_operand_t *op,
                                        uint16_t opdestaddr, uint16_t opaddr,
                                        int fixuptype, int size); /* UR_FIXUP_XXX */

extern urasm_fixup_operand_fn urasm_fixup_operand;


/* returns size of the assembled code or negative error index (see `URA_XXX`) */
/* note that zero result is not an error, and is perfectly valid */
/* understands comments */
/* errpos can be set when returned length >=0 -- it will point to ':' (this is not an error) */
/*   expr: expression string; must not be NULL */
/*   addr: current PC */
/*   errpos: will be set to error position, or to ':' */
int urasm_opasm (const char *expr, uint16_t destaddr, uint16_t addr, const char **errpos);

/* returns simple Engrish string for the given error code (`URA_XXX`) */
const char *urasm_errormsg (int errcode);


#ifdef __cplusplus
}
#endif
#endif

/* coded by Ketmar // Invisible Vector (ketmar@ketmar.no-ip.org)
 * Understanding is not required. Only obedience.
 *
 * URASM Z80 assembler/disassembler core v0.1.3 (with ZXNext support)
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 */
#include <setjmp.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "liburasm.h"


int urasm_allow_zxnext = 0;
int urasm_disasm_decimal = 0;
const char *urasm_disasm_mnemo_delimiter = "\t";
const char *urasm_disasm_operand_delimiter = ",";
urasm_label_by_addr_fn urasm_label_by_addr = NULL;
urasm_label_by_name_fn urasm_label_by_name = NULL;
urasm_getbyte_fn urasm_getbyte = NULL;
urasm_putbyte_fn urasm_putbyte = NULL;
urasm_fixup_operand_fn urasm_fixup_operand = NULL;


const char *URASM_TOKENS[URASM_MAX_TOKEN] = {
  "ADC", "ADD", "AND", "BIT", "CALL","CCF", "CP",  "CPD",
  "CPDR","CPI", "CPIR","CPL", "DAA", "DEC", "DI",  "DJNZ",
  "EI",  "EX",  "EXX", "HALT","IM",  "IN",  "INC", "IND",
  "INDR","INI", "INIR","JP",  "JR",  "LD",  "LDD", "LDDR",
  "LDI", "LDIR","NEG", "NOP", "OR",  "OTDR","OTIR","OUT",
  "OUTD","OUTI","POP", "PUSH","RES", "RET", "RETI","RETN",
  "RL",  "RLA", "RLC", "RLCA","RLD", "RR",  "RRA", "RRC",
  "RRCA","RRD", "RST", "SBC", "SCF", "SET", "SLA", "SLI",
  "SLL", "SRA", "SRL", "SUB", "XOR", "XSLT","NOPX","NOPY",
  /* ZXNext */
  "LDIX",    "LDWS",   "LDIRX",   "LDDX",
  "LDDRX",   "LDPIRX", "OUTINB",  "MUL",
  "SWAPNIB", "MIRROR", "NEXTREG", "PIXELDN",
  "PIXELAD", "SETAE",  "TEST",    "BSLA",
  "BSRA",    "BSRL",   "BSRF",    "BRLC"
};

// various things...
const char *URA_REGS8[8] = {"B","C","D","E","H","L","(HL)","A"};
const char *URA_REGS16[4] = {"BC","DE","HL","SP"};
const char *URA_REGS16A[4] = {"BC","DE","HL","AF"};
const char *URA_COND[8] = {"NZ","Z","NC","C","PO","PE","P","M"};


// the longest matches must come first (for disassembler)
// solid-masked must come first (for disassembler)
// assembler searches the table from the last command
// disassembler searches the table from the first command
// heh, i spent the whole night creating this shit! %-)
const urasm_cmdinfo_t URASM_COMMANDS[URASM_MAX_COMMAND] = {
  {.mnemo=UT_NOPX, .code=0x000000DDUL, .mask=0x00000000UL, .ops={UO_NONE, UO_NONE, UO_NONE}},
  {.mnemo=UT_NOPY, .code=0x000000FDUL, .mask=0x00000000UL, .ops={UO_NONE, UO_NONE, UO_NONE}},
  // DD/CB opcodes (special)
  // RLC (IX+d)
  {.mnemo=UT_RLC,  .code=0x0600CBDDUL, .mask=0xFF00FFFFUL, .ops={UO_MIX, UO_NONE, UO_NONE}},
  // RRC (IX+d)
  {.mnemo=UT_RRC,  .code=0x0E00CBDDUL, .mask=0xFF00FFFFUL, .ops={UO_MIX, UO_NONE, UO_NONE}},
  // RL (IX+d)
  {.mnemo=UT_RL,   .code=0x1600CBDDUL, .mask=0xFF00FFFFUL, .ops={UO_MIX, UO_NONE, UO_NONE}},
  // RR (IX+d)
  {.mnemo=UT_RR,   .code=0x1E00CBDDUL, .mask=0xFF00FFFFUL, .ops={UO_MIX, UO_NONE, UO_NONE}},
  // SLA (IX+d)
  {.mnemo=UT_SLA,  .code=0x2600CBDDUL, .mask=0xFF00FFFFUL, .ops={UO_MIX, UO_NONE, UO_NONE}},
  // SRA (IX+d)
  {.mnemo=UT_SRA,  .code=0x2E00CBDDUL, .mask=0xFF00FFFFUL, .ops={UO_MIX, UO_NONE, UO_NONE}},
  // SLL (IX+d)
  {.mnemo=UT_SLL,  .code=0x3600CBDDUL, .mask=0xFF00FFFFUL, .ops={UO_MIX, UO_NONE, UO_NONE}},
  // SLI (IX+d)
  {.mnemo=UT_SLI,  .code=0x3600CBDDUL, .mask=0xFF00FFFFUL, .ops={UO_MIX, UO_NONE, UO_NONE}},
  // SRL (IX+d)
  {.mnemo=UT_SRL,  .code=0x3E00CBDDUL, .mask=0xFF00FFFFUL, .ops={UO_MIX, UO_NONE, UO_NONE}},
  // RES n,(IX+d)
  {.mnemo=UT_RES,  .code=0x8600CBDDUL, .mask=0xC700FFFFUL, .ops={UO_BITN, UO_MIX, UO_NONE}},
  // SET n,(IX+d)
  {.mnemo=UT_SET,  .code=0xC600CBDDUL, .mask=0xC700FFFFUL, .ops={UO_BITN, UO_MIX, UO_NONE}},
  // FD/CB opcodes (special)
  // RLC (IY+d)
  {.mnemo=UT_RLC,  .code=0x0600CBFDUL, .mask=0xFF00FFFFUL, .ops={UO_MIY, UO_NONE, UO_NONE}},
  // RRC (IY+d)
  {.mnemo=UT_RRC,  .code=0x0E00CBFDUL, .mask=0xFF00FFFFUL, .ops={UO_MIY, UO_NONE, UO_NONE}},
  // RL (IY+d)
  {.mnemo=UT_RL,   .code=0x1600CBFDUL, .mask=0xFF00FFFFUL, .ops={UO_MIY, UO_NONE, UO_NONE}},
  // RR (IY+d)
  {.mnemo=UT_RR,   .code=0x1E00CBFDUL, .mask=0xFF00FFFFUL, .ops={UO_MIY, UO_NONE, UO_NONE}},
  // SLA (IY+d)
  {.mnemo=UT_SLA,  .code=0x2600CBFDUL, .mask=0xFF00FFFFUL, .ops={UO_MIY, UO_NONE, UO_NONE}},
  // SRA (IY+d)
  {.mnemo=UT_SRA,  .code=0x2E00CBFDUL, .mask=0xFF00FFFFUL, .ops={UO_MIY, UO_NONE, UO_NONE}},
  // SLL (IY+d)
  {.mnemo=UT_SLL,  .code=0x3600CBFDUL, .mask=0xFF00FFFFUL, .ops={UO_MIY, UO_NONE, UO_NONE}},
  // SLI (IY+d)
  {.mnemo=UT_SLI,  .code=0x3600CBFDUL, .mask=0xFF00FFFFUL, .ops={UO_MIY, UO_NONE, UO_NONE}},
  // SRL (IY+d)
  {.mnemo=UT_SRL,  .code=0x3E00CBFDUL, .mask=0xFF00FFFFUL, .ops={UO_MIY, UO_NONE, UO_NONE}},
  // RES n,(IY+d)
  {.mnemo=UT_RES,  .code=0x8600CBFDUL, .mask=0xC700FFFFUL, .ops={UO_BITN, UO_MIY, UO_NONE}},
  // SET n,(IY+d)
  {.mnemo=UT_SET,  .code=0xC600CBFDUL, .mask=0xC700FFFFUL, .ops={UO_BITN, UO_MIY, UO_NONE}},

  // DD/CB opcodes
  // RLC (IX+d),r8
  {.mnemo=UT_RLC,  .code=0x0000CBDDUL, .mask=0xF800FFFFUL, .ops={UO_MIX, UO_R8_NOM, UO_NONE}},
  // RRC (IX+d),r8
  {.mnemo=UT_RRC,  .code=0x0800CBDDUL, .mask=0xF800FFFFUL, .ops={UO_MIX, UO_R8_NOM, UO_NONE}},
  // RL (IX+d),r8
  {.mnemo=UT_RL,   .code=0x1000CBDDUL, .mask=0xF800FFFFUL, .ops={UO_MIX, UO_R8_NOM, UO_NONE}},
  // RR (IX+d),r8
  {.mnemo=UT_RR,   .code=0x1800CBDDUL, .mask=0xF800FFFFUL, .ops={UO_MIX, UO_R8_NOM, UO_NONE}},
  // SLA (IX+d),r8
  {.mnemo=UT_SLA,  .code=0x2000CBDDUL, .mask=0xF800FFFFUL, .ops={UO_MIX, UO_R8_NOM, UO_NONE}},
  // SRA (IX+d),r8
  {.mnemo=UT_SRA,  .code=0x2800CBDDUL, .mask=0xF800FFFFUL, .ops={UO_MIX, UO_R8_NOM, UO_NONE}},
  // SLL (IX+d),r8
  {.mnemo=UT_SLL,  .code=0x3000CBDDUL, .mask=0xF800FFFFUL, .ops={UO_MIX, UO_R8_NOM, UO_NONE}},
  // SLI (IX+d),r8
  {.mnemo=UT_SLI,  .code=0x3000CBDDUL, .mask=0xF800FFFFUL, .ops={UO_MIX, UO_R8_NOM, UO_NONE}},
  // SRL (IX+d),r8
  {.mnemo=UT_SRL,  .code=0x3800CBDDUL, .mask=0xF800FFFFUL, .ops={UO_MIX, UO_R8_NOM, UO_NONE}},
  // BIT n,(IX+d)
  {.mnemo=UT_BIT,  .code=0x4600CBDDUL, .mask=0xC700FFFFUL, .ops={UO_BITN, UO_MIX, UO_NONE}},
  // BIT n,(IX+d),r8
  {.mnemo=UT_BIT,  .code=0x4000CBDDUL, .mask=0xC000FFFFUL, .ops={UO_BITN, UO_MIX, UO_R8_NOM}},
  // RES n,(IX+d),r8
  {.mnemo=UT_RES,  .code=0x8000CBDDUL, .mask=0xC000FFFFUL, .ops={UO_BITN, UO_MIX, UO_R8_NOM}},
  // SET n,(IX+d),r8
  {.mnemo=UT_SET,  .code=0xC000CBDDUL, .mask=0xC000FFFFUL, .ops={UO_BITN, UO_MIX, UO_R8_NOM}},
  // FD/CB opcodes
  // RLC (IY+d),r8
  {.mnemo=UT_RLC,  .code=0x0000CBFDUL, .mask=0xF800FFFFUL, .ops={UO_MIY, UO_R8_NOM, UO_NONE}},
  // RRC (IY+d),r8
  {.mnemo=UT_RRC,  .code=0x0800CBFDUL, .mask=0xF800FFFFUL, .ops={UO_MIY, UO_R8_NOM, UO_NONE}},
  // RL (IY+d),r8
  {.mnemo=UT_RL,   .code=0x1000CBFDUL, .mask=0xF800FFFFUL, .ops={UO_MIY, UO_R8_NOM, UO_NONE}},
  // RR (IY+d),r8
  {.mnemo=UT_RR,   .code=0x1800CBFDUL, .mask=0xF800FFFFUL, .ops={UO_MIY, UO_R8_NOM, UO_NONE}},
  // SLA (IY+d),r8
  {.mnemo=UT_SLA,  .code=0x2000CBFDUL, .mask=0xF800FFFFUL, .ops={UO_MIY, UO_R8_NOM, UO_NONE}},
  // SRA (IY+d),r8
  {.mnemo=UT_SRA,  .code=0x2800CBFDUL, .mask=0xF800FFFFUL, .ops={UO_MIY, UO_R8_NOM, UO_NONE}},
  // SLL (IY+d),r8
  {.mnemo=UT_SLL,  .code=0x3000CBFDUL, .mask=0xF800FFFFUL, .ops={UO_MIY, UO_R8_NOM, UO_NONE}},
  // SLI (IY+d),r8
  {.mnemo=UT_SLI,  .code=0x3000CBFDUL, .mask=0xF800FFFFUL, .ops={UO_MIY, UO_R8_NOM, UO_NONE}},
  // SRL (IY+d),r8
  {.mnemo=UT_SRL,  .code=0x3800CBFDUL, .mask=0xF800FFFFUL, .ops={UO_MIY, UO_R8_NOM, UO_NONE}},
  // BIT n,(IY+d)
  {.mnemo=UT_BIT,  .code=0x4600CBFDUL, .mask=0xC700FFFFUL, .ops={UO_BITN, UO_MIY, UO_NONE}},
  // BIT n,(IY+d),r8
  {.mnemo=UT_BIT,  .code=0x4000CBFDUL, .mask=0xC000FFFFUL, .ops={UO_BITN, UO_MIY, UO_R8_NOM}},
  // RES n,(IY+d),r8
  {.mnemo=UT_RES,  .code=0x8000CBFDUL, .mask=0xC000FFFFUL, .ops={UO_BITN, UO_MIY, UO_R8_NOM}},
  // SET n,(IY+d),r8
  {.mnemo=UT_SET,  .code=0xC000CBFDUL, .mask=0xC000FFFFUL, .ops={UO_BITN, UO_MIY, UO_R8_NOM}},
  // standard CB opcodes
  // RLC r8
  {.mnemo=UT_RLC,  .code=0x00CBUL, .mask=0xF8FFUL, .ops={UO_R8, UO_NONE, UO_NONE}},
  // RRC r8
  {.mnemo=UT_RRC,  .code=0x08CBUL, .mask=0xF8FFUL, .ops={UO_R8, UO_NONE, UO_NONE}},
  // RL r8
  {.mnemo=UT_RL,   .code=0x10CBUL, .mask=0xF8FFUL, .ops={UO_R8, UO_NONE, UO_NONE}},
  // RR r8
  {.mnemo=UT_RR,   .code=0x18CBUL, .mask=0xF8FFUL, .ops={UO_R8, UO_NONE, UO_NONE}},
  // SLA r8
  {.mnemo=UT_SLA,  .code=0x20CBUL, .mask=0xF8FFUL, .ops={UO_R8, UO_NONE, UO_NONE}},
  // SRA r8
  {.mnemo=UT_SRA,  .code=0x28CBUL, .mask=0xF8FFUL, .ops={UO_R8, UO_NONE, UO_NONE}},
  // SLL r8
  {.mnemo=UT_SLL,  .code=0x30CBUL, .mask=0xF8FFUL, .ops={UO_R8, UO_NONE, UO_NONE}},
  // SLI r8
  {.mnemo=UT_SLI,  .code=0x30CBUL, .mask=0xF8FFUL, .ops={UO_R8, UO_NONE, UO_NONE}},
  // SRL r8
  {.mnemo=UT_SRL,  .code=0x38CBUL, .mask=0xF8FFUL, .ops={UO_R8, UO_NONE, UO_NONE}},
  // BIT n,r8
  {.mnemo=UT_BIT,  .code=0x40CBUL, .mask=0xC0FFUL, .ops={UO_BITN, UO_R8, UO_NONE}},
  // RES n,r8
  {.mnemo=UT_RES,  .code=0x80CBUL, .mask=0xC0FFUL, .ops={UO_BITN, UO_R8, UO_NONE}},
  // SET n,r8
  {.mnemo=UT_SET,  .code=0xC0CBUL, .mask=0xC0FFUL, .ops={UO_BITN, UO_R8, UO_NONE}},

  // some ED opcodes
  // traps
  {.mnemo=UT_XSLT, .code=0xFBEDUL, .mask=0xFFFFUL, .ops={UO_NONE, UO_NONE, UO_NONE}},
  // ED string instructions
  {.mnemo=UT_LDI,  .code=0xA0EDUL, .mask=0xFFFFUL, .ops={UO_NONE, UO_NONE, UO_NONE}},
  {.mnemo=UT_LDIR, .code=0xB0EDUL, .mask=0xFFFFUL, .ops={UO_NONE, UO_NONE, UO_NONE}},
  {.mnemo=UT_CPI,  .code=0xA1EDUL, .mask=0xFFFFUL, .ops={UO_NONE, UO_NONE, UO_NONE}},
  {.mnemo=UT_CPIR, .code=0xB1EDUL, .mask=0xFFFFUL, .ops={UO_NONE, UO_NONE, UO_NONE}},
  {.mnemo=UT_INI,  .code=0xA2EDUL, .mask=0xFFFFUL, .ops={UO_NONE, UO_NONE, UO_NONE}},
  {.mnemo=UT_INIR, .code=0xB2EDUL, .mask=0xFFFFUL, .ops={UO_NONE, UO_NONE, UO_NONE}},
  {.mnemo=UT_OUTI, .code=0xA3EDUL, .mask=0xFFFFUL, .ops={UO_NONE, UO_NONE, UO_NONE}},
  {.mnemo=UT_OTIR, .code=0xB3EDUL, .mask=0xFFFFUL, .ops={UO_NONE, UO_NONE, UO_NONE}},
  {.mnemo=UT_LDD,  .code=0xA8EDUL, .mask=0xFFFFUL, .ops={UO_NONE, UO_NONE, UO_NONE}},
  {.mnemo=UT_LDDR, .code=0xB8EDUL, .mask=0xFFFFUL, .ops={UO_NONE, UO_NONE, UO_NONE}},
  {.mnemo=UT_CPD,  .code=0xA9EDUL, .mask=0xFFFFUL, .ops={UO_NONE, UO_NONE, UO_NONE}},
  {.mnemo=UT_CPDR, .code=0xB9EDUL, .mask=0xFFFFUL, .ops={UO_NONE, UO_NONE, UO_NONE}},
  {.mnemo=UT_IND,  .code=0xAAEDUL, .mask=0xFFFFUL, .ops={UO_NONE, UO_NONE, UO_NONE}},
  {.mnemo=UT_INDR, .code=0xBAEDUL, .mask=0xFFFFUL, .ops={UO_NONE, UO_NONE, UO_NONE}},
  {.mnemo=UT_OUTD, .code=0xABEDUL, .mask=0xFFFFUL, .ops={UO_NONE, UO_NONE, UO_NONE}},
  {.mnemo=UT_OTDR, .code=0xBBEDUL, .mask=0xFFFFUL, .ops={UO_NONE, UO_NONE, UO_NONE}},

  /* ZXNext opcodes (always ED-prefixed) */
  /* operand-less opcodes */
  {.mnemo=UT_LDIX,    .code=0xA4EDUL, .mask=0xFFFFUL, .ops={UO_NONE, UO_NONE, UO_NEXT}},
  {.mnemo=UT_LDWS,    .code=0xA5EDUL, .mask=0xFFFFUL, .ops={UO_NONE, UO_NONE, UO_NEXT}},
  {.mnemo=UT_LDIRX,   .code=0xB4EDUL, .mask=0xFFFFUL, .ops={UO_NONE, UO_NONE, UO_NEXT}},
  {.mnemo=UT_LDDX,    .code=0xACEDUL, .mask=0xFFFFUL, .ops={UO_NONE, UO_NONE, UO_NEXT}},
  {.mnemo=UT_LDDRX,   .code=0xBCEDUL, .mask=0xFFFFUL, .ops={UO_NONE, UO_NONE, UO_NEXT}},
  {.mnemo=UT_LDPIRX,  .code=0xB7EDUL, .mask=0xFFFFUL, .ops={UO_NONE, UO_NONE, UO_NEXT}},
  {.mnemo=UT_OUTINB,  .code=0x90EDUL, .mask=0xFFFFUL, .ops={UO_NONE, UO_NONE, UO_NEXT}},
  {.mnemo=UT_MUL,     .code=0x30EDUL, .mask=0xFFFFUL, .ops={UO_NONE, UO_NONE, UO_NEXT}},
  {.mnemo=UT_SWAPNIB, .code=0x23EDUL, .mask=0xFFFFUL, .ops={UO_NONE, UO_NONE, UO_NEXT}},
  {.mnemo=UT_PIXELDN, .code=0x93EDUL, .mask=0xFFFFUL, .ops={UO_NONE, UO_NONE, UO_NEXT}},
  {.mnemo=UT_PIXELAD, .code=0x94EDUL, .mask=0xFFFFUL, .ops={UO_NONE, UO_NONE, UO_NEXT}},
  {.mnemo=UT_SETAE,   .code=0x95EDUL, .mask=0xFFFFUL, .ops={UO_NONE, UO_NONE, UO_NEXT}},
  {.mnemo=UT_MIRROR,  .code=0x24EDUL, .mask=0xFFFFUL, .ops={UO_R8_A, UO_NONE, UO_NEXT}},
  /* ADD RR,A*/
  {.mnemo=UT_ADD,     .code=0x31EDUL, .mask=0xFFFFUL, .ops={UO_R16HL, UO_R8_A, UO_NEXT}},
  {.mnemo=UT_ADD,     .code=0x32EDUL, .mask=0xFFFFUL, .ops={UO_R16DE, UO_R8_A, UO_NEXT}},
  {.mnemo=UT_ADD,     .code=0x33EDUL, .mask=0xFFFFUL, .ops={UO_R16BC, UO_R8_A, UO_NEXT}},
  /* ADD RR,nnnn*/
  {.mnemo=UT_ADD,     .code=0x34EDUL, .mask=0xFFFFUL, .ops={UO_R16HL, UO_IMM16, UO_NEXT}},
  {.mnemo=UT_ADD,     .code=0x35EDUL, .mask=0xFFFFUL, .ops={UO_R16DE, UO_IMM16, UO_NEXT}},
  {.mnemo=UT_ADD,     .code=0x36EDUL, .mask=0xFFFFUL, .ops={UO_R16BC, UO_IMM16, UO_NEXT}},
  /* PUSH nnnn*/
  {.mnemo=UT_PUSH,    .code=0x8AEDUL, .mask=0xFFFFUL, .ops={UO_IMM16BE, UO_NONE, UO_NEXT}},
  /* TEST nn */
  {.mnemo=UT_TEST,    .code=0x27EDUL, .mask=0xFFFFUL, .ops={UO_IMM8, UO_NONE, UO_NEXT}},
  /* NEXTREG nn,nn*/
  {.mnemo=UT_NEXTREG, .code=0x91EDUL, .mask=0xFFFFUL, .ops={UO_IMM8, UO_IMM8, UO_NEXT}},
  /* NEXTREG nn,A*/
  {.mnemo=UT_NEXTREG, .code=0x92EDUL, .mask=0xFFFFUL, .ops={UO_IMM8, UO_R8_A, UO_NEXT}},
  /* BSXX RR,B*/
  {.mnemo=UT_BSLA,    .code=0x28EDUL, .mask=0xFFFFUL, .ops={UO_R16DE, UO_R8_B, UO_NEXT}},
  {.mnemo=UT_BSRA,    .code=0x29EDUL, .mask=0xFFFFUL, .ops={UO_R16DE, UO_R8_B, UO_NEXT}},
  {.mnemo=UT_BSRL,    .code=0x2AEDUL, .mask=0xFFFFUL, .ops={UO_R16DE, UO_R8_B, UO_NEXT}},
  {.mnemo=UT_BSRF,    .code=0x2BEDUL, .mask=0xFFFFUL, .ops={UO_R16DE, UO_R8_B, UO_NEXT}},
  {.mnemo=UT_BRLC,    .code=0x2CEDUL, .mask=0xFFFFUL, .ops={UO_R16DE, UO_R8_B, UO_NEXT}},
  /* JP (C) */
  {.mnemo=UT_JP,      .code=0x98EDUL, .mask=0xFFFFUL, .ops={UO_PORTC, UO_NONE, UO_NEXT}},

  // ED w/o operands
  {.mnemo=UT_RRD,  .code=0x67EDUL, .mask=0xFFFFUL, .ops={UO_NONE, UO_NONE, UO_NONE}},
  {.mnemo=UT_RLD,  .code=0x6FEDUL, .mask=0xFFFFUL, .ops={UO_NONE, UO_NONE, UO_NONE}},

  // IN (C)
  {.mnemo=UT_IN,   .code=0x70EDUL, .mask=0xFFFFUL, .ops={UO_PORTC, UO_NONE, UO_NONE}},
  // OUT (C),0
  {.mnemo=UT_OUT,  .code=0x71EDUL, .mask=0xFFFFUL, .ops={UO_PORTC, UO_IM0, UO_NONE}},

  // LD I,A
  {.mnemo=UT_LD,   .code=0x47EDUL, .mask=0xFFFFUL, .ops={UO_R8_I, UO_R8_A, UO_NONE}},
  // LD A,I
  {.mnemo=UT_LD,   .code=0x57EDUL, .mask=0xFFFFUL, .ops={UO_R8_A, UO_R8_I, UO_NONE}},
  // LD R,A
  {.mnemo=UT_LD,   .code=0x4FEDUL, .mask=0xFFFFUL, .ops={UO_R8_R, UO_R8_A, UO_NONE}},
  // LD A,R
  {.mnemo=UT_LD,   .code=0x5FEDUL, .mask=0xFFFFUL, .ops={UO_R8_A, UO_R8_R, UO_NONE}},
  // IM 0/1
  //(.mnemo=UT_IM,   .code=0x4EEDUL, .mask=0xFFFFUL, .ops={UO_IM01, UO_NONE, UO_NONE}},

  // ED w/o operands
  {.mnemo=UT_RETN, .code=0x45EDUL, .mask=0xCFFFUL, .ops={UO_NONE, UO_NONE, UO_NONE}},
  {.mnemo=UT_RETI, .code=0x4DEDUL, .mask=0xCFFFUL, .ops={UO_NONE, UO_NONE, UO_NONE}},

  // SBC HL,r16
  {.mnemo=UT_SBC,  .code=0x42EDUL, .mask=0xCFFFUL, .ops={UO_R16HL, UO_R16, UO_NONE}},
  // ADC HL,r16
  {.mnemo=UT_ADC,  .code=0x4AEDUL, .mask=0xCFFFUL, .ops={UO_R16HL, UO_R16, UO_NONE}},
  // LD (nnnn),r16
  {.mnemo=UT_LD,   .code=0x43EDUL, .mask=0xCFFFUL, .ops={UO_MEM16, UO_R16, UO_NONE}},
  // LD r16,(nnnn)
  {.mnemo=UT_LD,   .code=0x4BEDUL, .mask=0xCFFFUL, .ops={UO_R16, UO_MEM16, UO_NONE}},

  // ED w/o operands
  {.mnemo=UT_NEG,  .code=0x44EDUL, .mask=0xC7FFUL, .ops={UO_NONE, UO_NONE, UO_NONE}},

  // IN r8,(C)
  {.mnemo=UT_IN,   .code=0x40EDUL, .mask=0xC7FFUL, .ops={UO_2R8_NOM, UO_PORTC, UO_NONE}},
  // OUT (C),r8
  {.mnemo=UT_OUT,  .code=0x41EDUL, .mask=0xC7FFUL, .ops={UO_PORTC, UO_2R8_NOM, UO_NONE}},

  // IM 2
  {.mnemo=UT_IM,   .code=0x5EEDUL, .mask=0xDFFFUL, .ops={UO_IM2, UO_NONE, UO_NONE}},
  // IM 1
  {.mnemo=UT_IM,   .code=0x56EDUL, .mask=0xDFFFUL, .ops={UO_IM1, UO_NONE, UO_NONE}},
  // IM 0
  {.mnemo=UT_IM,   .code=0x46EDUL, .mask=0xD7FFUL, .ops={UO_IM0, UO_NONE, UO_NONE}},

  // LD SP,IX
  {.mnemo=UT_LD,   .code=0xF9DDUL, .mask=0xFFFFUL, .ops={UO_R16SP, UO_R16IX, UO_NONE}},
  // LD SP,IY
  {.mnemo=UT_LD,   .code=0xF9FDUL, .mask=0xFFFFUL, .ops={UO_R16SP, UO_R16IY, UO_NONE}},

  // EX (SP),IX
  {.mnemo=UT_EX,   .code=0xE3DDUL, .mask=0xFFFFUL, .ops={UO_MSP, UO_R16IX, UO_NONE}},
  // EX IX,(SP) (ditto)
  {.mnemo=UT_EX,   .code=0xE3DDUL, .mask=0xFFFFUL, .ops={UO_R16IX, UO_MSP, UO_NONE}},
  // EX (SP),IY
  {.mnemo=UT_EX,   .code=0xE3FDUL, .mask=0xFFFFUL, .ops={UO_MSP, UO_R16IY, UO_NONE}},
  // EX IY,(SP) (ditto)
  {.mnemo=UT_EX,   .code=0xE3FDUL, .mask=0xFFFFUL, .ops={UO_R16IY, UO_MSP, UO_NONE}},

  // JP (IX)
  {.mnemo=UT_JP,   .code=0xE9DDUL, .mask=0xFFFFUL, .ops={UO_MIX0, UO_NONE, UO_NONE}},
  // JP (IY)
  {.mnemo=UT_JP,   .code=0xE9FDUL, .mask=0xFFFFUL, .ops={UO_MIY0, UO_NONE, UO_NONE}},
  // JP IX
  {.mnemo=UT_JP,   .code=0xE9DDUL, .mask=0xFFFFUL, .ops={UO_R16IX, UO_NONE, UO_NONE}},
  // JP IY
  {.mnemo=UT_JP,   .code=0xE9FDUL, .mask=0xFFFFUL, .ops={UO_R16IY, UO_NONE, UO_NONE}},

  // POP IX
  {.mnemo=UT_POP,  .code=0xE1DDUL, .mask=0xFFFFUL, .ops={UO_R16IX, UO_NONE, UO_NONE}},
  // PUSH IX
  {.mnemo=UT_PUSH, .code=0xE5DDUL, .mask=0xFFFFUL, .ops={UO_R16IX, UO_NONE, UO_NONE}},
  // POP IY
  {.mnemo=UT_POP,  .code=0xE1FDUL, .mask=0xFFFFUL, .ops={UO_R16IY, UO_NONE, UO_NONE}},
  // PUSH IY
  {.mnemo=UT_PUSH, .code=0xE5FDUL, .mask=0xFFFFUL, .ops={UO_R16IY, UO_NONE, UO_NONE}},

  // ADD A,(IX+d)
  {.mnemo=UT_ADD,  .code=0x86DDUL, .mask=0xFFFFUL, .ops={UO_R8_A, UO_MIX, UO_NONE}},
  // ADD (IX+d)
  {.mnemo=UT_ADD,  .code=0x86DDUL, .mask=0xFFFFUL, .ops={UO_MIX, UO_NONE, UO_NONE}},
  // ADC A,(IX+d)
  {.mnemo=UT_ADC,  .code=0x8EDDUL, .mask=0xFFFFUL, .ops={UO_R8_A, UO_MIX, UO_NONE}},
  // ADC (IX+d)
  {.mnemo=UT_ADC,  .code=0x8EDDUL, .mask=0xFFFFUL, .ops={UO_MIX, UO_NONE, UO_NONE}},
  // SUB (IX+d)
  {.mnemo=UT_SUB,  .code=0x96DDUL, .mask=0xFFFFUL, .ops={UO_MIX, UO_NONE, UO_NONE}},
  // SUB A,(IX+d)
  {.mnemo=UT_SUB,  .code=0x96DDUL, .mask=0xFFFFUL, .ops={UO_R8_A, UO_MIX, UO_NONE}},
  // SBC A,(IX+d)
  {.mnemo=UT_SBC,  .code=0x9EDDUL, .mask=0xFFFFUL, .ops={UO_R8_A, UO_MIX, UO_NONE}},
  // SBC (IX+d)
  {.mnemo=UT_SBC,  .code=0x9EDDUL, .mask=0xFFFFUL, .ops={UO_MIX, UO_NONE, UO_NONE}},
  // AND (IX+d)
  {.mnemo=UT_AND,  .code=0xA6DDUL, .mask=0xFFFFUL, .ops={UO_MIX, UO_NONE, UO_NONE}},
  // AND A,(IX+d)
  {.mnemo=UT_AND,  .code=0xA6DDUL, .mask=0xFFFFUL, .ops={UO_R8_A, UO_MIX, UO_NONE}},
  // XOR (IX+d)
  {.mnemo=UT_XOR,  .code=0xAEDDUL, .mask=0xFFFFUL, .ops={UO_MIX, UO_NONE, UO_NONE}},
  // XOR A,(IX+d)
  {.mnemo=UT_XOR,  .code=0xAEDDUL, .mask=0xFFFFUL, .ops={UO_R8_A, UO_MIX, UO_NONE}},
  // OR (IX+d)
  {.mnemo=UT_OR,   .code=0xB6DDUL, .mask=0xFFFFUL, .ops={UO_MIX, UO_NONE, UO_NONE}},
  // OR A,(IX+d)
  {.mnemo=UT_OR,   .code=0xB6DDUL, .mask=0xFFFFUL, .ops={UO_R8_A, UO_MIX, UO_NONE}},
  // CP (IX+d)
  {.mnemo=UT_CP,   .code=0xBEDDUL, .mask=0xFFFFUL, .ops={UO_MIX, UO_NONE, UO_NONE}},
  // CP A,(IX+d)
  {.mnemo=UT_CP,   .code=0xBEDDUL, .mask=0xFFFFUL, .ops={UO_R8_A, UO_MIX, UO_NONE}},
  // ADD A,(IY+d)
  {.mnemo=UT_ADD,  .code=0x86FDUL, .mask=0xFFFFUL, .ops={UO_R8_A, UO_MIY, UO_NONE}},
  // ADD (IY+d)
  {.mnemo=UT_ADD,  .code=0x86FDUL, .mask=0xFFFFUL, .ops={UO_MIY, UO_NONE, UO_NONE}},
  // ADC A,(IY+d)
  {.mnemo=UT_ADC,  .code=0x8EFDUL, .mask=0xFFFFUL, .ops={UO_R8_A, UO_MIY, UO_NONE}},
  // ADC (IY+d)
  {.mnemo=UT_ADC,  .code=0x8EFDUL, .mask=0xFFFFUL, .ops={UO_MIY, UO_NONE, UO_NONE}},
  // SUB (IY+d)
  {.mnemo=UT_SUB,  .code=0x96FDUL, .mask=0xFFFFUL, .ops={UO_MIY, UO_NONE, UO_NONE}},
  // SUB A,(IY+d)
  {.mnemo=UT_SUB,  .code=0x96FDUL, .mask=0xFFFFUL, .ops={UO_R8_A, UO_MIY, UO_NONE}},
  // SBC A,(IY+d)
  {.mnemo=UT_SBC,  .code=0x9EFDUL, .mask=0xFFFFUL, .ops={UO_R8_A, UO_MIY, UO_NONE}},
  // SBC (IY+d)
  {.mnemo=UT_SBC,  .code=0x9EFDUL, .mask=0xFFFFUL, .ops={UO_MIY, UO_NONE, UO_NONE}},
  // AND (IY+d)
  {.mnemo=UT_AND,  .code=0xA6FDUL, .mask=0xFFFFUL, .ops={UO_MIY, UO_NONE, UO_NONE}},
  // AND A,(IY+d)
  {.mnemo=UT_AND,  .code=0xA6FDUL, .mask=0xFFFFUL, .ops={UO_R8_A, UO_MIY, UO_NONE}},
  // XOR (IY+d)
  {.mnemo=UT_XOR,  .code=0xAEFDUL, .mask=0xFFFFUL, .ops={UO_MIY, UO_NONE, UO_NONE}},
  // XOR A,(IY+d)
  {.mnemo=UT_XOR,  .code=0xAEFDUL, .mask=0xFFFFUL, .ops={UO_R8_A, UO_MIY, UO_NONE}},
  // OR (IY+d)
  {.mnemo=UT_OR,   .code=0xB6FDUL, .mask=0xFFFFUL, .ops={UO_MIY, UO_NONE, UO_NONE}},
  // OR A,(IY+d)
  {.mnemo=UT_OR,   .code=0xB6FDUL, .mask=0xFFFFUL, .ops={UO_R8_A, UO_MIY, UO_NONE}},
  // CP (IY+d)
  {.mnemo=UT_CP,   .code=0xBEFDUL, .mask=0xFFFFUL, .ops={UO_MIY, UO_NONE, UO_NONE}},
  // CP A,(IY+d)
  {.mnemo=UT_CP,   .code=0xBEFDUL, .mask=0xFFFFUL, .ops={UO_R8_A, UO_MIY, UO_NONE}},
  // ADD A,XH
  {.mnemo=UT_ADD,  .code=0x84DDUL, .mask=0xFFFFUL, .ops={UO_R8_A, UO_R8_XH, UO_NONE}},
  // ADD XH
  {.mnemo=UT_ADD,  .code=0x84DDUL, .mask=0xFFFFUL, .ops={UO_R8_XH, UO_NONE, UO_NONE}},
  // ADC A,XH
  {.mnemo=UT_ADC,  .code=0x8CDDUL, .mask=0xFFFFUL, .ops={UO_R8_A, UO_R8_XH, UO_NONE}},
  // ADC XH
  {.mnemo=UT_ADC,  .code=0x8CDDUL, .mask=0xFFFFUL, .ops={UO_R8_XH, UO_NONE, UO_NONE}},
  // SUB XH
  {.mnemo=UT_SUB,  .code=0x94DDUL, .mask=0xFFFFUL, .ops={UO_R8_XH, UO_NONE, UO_NONE}},
  // SUB A,XH
  {.mnemo=UT_SUB,  .code=0x94DDUL, .mask=0xFFFFUL, .ops={UO_R8_A, UO_R8_XH, UO_NONE}},
  // SBC A,XH
  {.mnemo=UT_SBC,  .code=0x9CDDUL, .mask=0xFFFFUL, .ops={UO_R8_A, UO_R8_XH, UO_NONE}},
  // SBC XH
  {.mnemo=UT_SBC,  .code=0x9CDDUL, .mask=0xFFFFUL, .ops={UO_R8_XH, UO_NONE, UO_NONE}},
  // AND XH
  {.mnemo=UT_AND,  .code=0xA4DDUL, .mask=0xFFFFUL, .ops={UO_R8_XH, UO_NONE, UO_NONE}},
  // AND A,XH
  {.mnemo=UT_AND,  .code=0xA4DDUL, .mask=0xFFFFUL, .ops={UO_R8_A, UO_R8_XH, UO_NONE}},
  // XOR XH
  {.mnemo=UT_XOR,  .code=0xACDDUL, .mask=0xFFFFUL, .ops={UO_R8_XH, UO_NONE, UO_NONE}},
  // XOR A,XH
  {.mnemo=UT_XOR,  .code=0xACDDUL, .mask=0xFFFFUL, .ops={UO_R8_A, UO_R8_XH, UO_NONE}},
  // OR XH
  {.mnemo=UT_OR,   .code=0xB4DDUL, .mask=0xFFFFUL, .ops={UO_R8_XH, UO_NONE, UO_NONE}},
  // OR A,XH
  {.mnemo=UT_OR,   .code=0xB4DDUL, .mask=0xFFFFUL, .ops={UO_R8_A, UO_R8_XH, UO_NONE}},
  // CP XH
  {.mnemo=UT_CP,   .code=0xBCDDUL, .mask=0xFFFFUL, .ops={UO_R8_XH, UO_NONE, UO_NONE}},
  // CP A,XH
  {.mnemo=UT_CP,   .code=0xBCDDUL, .mask=0xFFFFUL, .ops={UO_R8_A, UO_R8_XH, UO_NONE}},
  // ADD A,XL
  {.mnemo=UT_ADD,  .code=0x85DDUL, .mask=0xFFFFUL, .ops={UO_R8_A, UO_R8_XL, UO_NONE}},
  // ADD XL
  {.mnemo=UT_ADD,  .code=0x85DDUL, .mask=0xFFFFUL, .ops={UO_R8_XL, UO_NONE, UO_NONE}},
  // ADC A,XL
  {.mnemo=UT_ADC,  .code=0x8DDDUL, .mask=0xFFFFUL, .ops={UO_R8_A, UO_R8_XL, UO_NONE}},
  // ADC XL
  {.mnemo=UT_ADC,  .code=0x8DDDUL, .mask=0xFFFFUL, .ops={UO_R8_XL, UO_NONE, UO_NONE}},
  // SUB XL
  {.mnemo=UT_SUB,  .code=0x95DDUL, .mask=0xFFFFUL, .ops={UO_R8_XL, UO_NONE, UO_NONE}},
  // SUB A,XL
  {.mnemo=UT_SUB,  .code=0x95DDUL, .mask=0xFFFFUL, .ops={UO_R8_A, UO_R8_XL, UO_NONE}},
  // SBC A,XL
  {.mnemo=UT_SBC,  .code=0x9DDDUL, .mask=0xFFFFUL, .ops={UO_R8_A, UO_R8_XL, UO_NONE}},
  // SBC XL
  {.mnemo=UT_SBC,  .code=0x9DDDUL, .mask=0xFFFFUL, .ops={UO_R8_XL, UO_NONE, UO_NONE}},
  // AND XL
  {.mnemo=UT_AND,  .code=0xA5DDUL, .mask=0xFFFFUL, .ops={UO_R8_XL, UO_NONE, UO_NONE}},
  // AND A,XL
  {.mnemo=UT_AND,  .code=0xA5DDUL, .mask=0xFFFFUL, .ops={UO_R8_A, UO_R8_XL, UO_NONE}},
  // XOR XL
  {.mnemo=UT_XOR,  .code=0xADDDUL, .mask=0xFFFFUL, .ops={UO_R8_XL, UO_NONE, UO_NONE}},
  // XOR A,XL
  {.mnemo=UT_XOR,  .code=0xADDDUL, .mask=0xFFFFUL, .ops={UO_R8_A, UO_R8_XL, UO_NONE}},
  // OR XL
  {.mnemo=UT_OR,   .code=0xB5DDUL, .mask=0xFFFFUL, .ops={UO_R8_XL, UO_NONE, UO_NONE}},
  // OR A,XL
  {.mnemo=UT_OR,   .code=0xB5DDUL, .mask=0xFFFFUL, .ops={UO_R8_A, UO_R8_XL, UO_NONE}},
  // CP XL
  {.mnemo=UT_CP,   .code=0xBDDDUL, .mask=0xFFFFUL, .ops={UO_R8_XL, UO_NONE, UO_NONE}},
  // CP A,XL
  {.mnemo=UT_CP,   .code=0xBDDDUL, .mask=0xFFFFUL, .ops={UO_R8_A, UO_R8_XL, UO_NONE}},
  // ADD A,YH
  {.mnemo=UT_ADD,  .code=0x84FDUL, .mask=0xFFFFUL, .ops={UO_R8_A, UO_R8_YH, UO_NONE}},
  // ADD YH
  {.mnemo=UT_ADD,  .code=0x84FDUL, .mask=0xFFFFUL, .ops={UO_R8_YH, UO_NONE, UO_NONE}},
  // ADC A,YH
  {.mnemo=UT_ADC,  .code=0x8CFDUL, .mask=0xFFFFUL, .ops={UO_R8_A, UO_R8_YH, UO_NONE}},
  // ADC YH
  {.mnemo=UT_ADC,  .code=0x8CFDUL, .mask=0xFFFFUL, .ops={UO_R8_YH, UO_NONE, UO_NONE}},
  // SUB YH
  {.mnemo=UT_SUB,  .code=0x94FDUL, .mask=0xFFFFUL, .ops={UO_R8_YH, UO_NONE, UO_NONE}},
  // SUB A,YH
  {.mnemo=UT_SUB,  .code=0x94FDUL, .mask=0xFFFFUL, .ops={UO_R8_A, UO_R8_YH, UO_NONE}},
  // SBC A,YH
  {.mnemo=UT_SBC,  .code=0x9CFDUL, .mask=0xFFFFUL, .ops={UO_R8_A, UO_R8_YH, UO_NONE}},
  // SBC YH
  {.mnemo=UT_SBC,  .code=0x9CFDUL, .mask=0xFFFFUL, .ops={UO_R8_YH, UO_NONE, UO_NONE}},
  // AND YH
  {.mnemo=UT_AND,  .code=0xA4FDUL, .mask=0xFFFFUL, .ops={UO_R8_YH, UO_NONE, UO_NONE}},
  // AND A,YH
  {.mnemo=UT_AND,  .code=0xA4FDUL, .mask=0xFFFFUL, .ops={UO_R8_A, UO_R8_YH, UO_NONE}},
  // XOR YH
  {.mnemo=UT_XOR,  .code=0xACFDUL, .mask=0xFFFFUL, .ops={UO_R8_YH, UO_NONE, UO_NONE}},
  // XOR A,YH
  {.mnemo=UT_XOR,  .code=0xACFDUL, .mask=0xFFFFUL, .ops={UO_R8_A, UO_R8_YH, UO_NONE}},
  // OR YH
  {.mnemo=UT_OR,   .code=0xB4FDUL, .mask=0xFFFFUL, .ops={UO_R8_YH, UO_NONE, UO_NONE}},
  // OR A,YH
  {.mnemo=UT_OR,   .code=0xB4FDUL, .mask=0xFFFFUL, .ops={UO_R8_A, UO_R8_YH, UO_NONE}},
  // CP YH
  {.mnemo=UT_CP,   .code=0xBCFDUL, .mask=0xFFFFUL, .ops={UO_R8_YH, UO_NONE, UO_NONE}},
  // CP A,YH
  {.mnemo=UT_CP,   .code=0xBCFDUL, .mask=0xFFFFUL, .ops={UO_R8_A, UO_R8_YH, UO_NONE}},
  // ADD A,YL
  {.mnemo=UT_ADD,  .code=0x85FDUL, .mask=0xFFFFUL, .ops={UO_R8_A, UO_R8_YL, UO_NONE}},
  // ADD YL
  {.mnemo=UT_ADD,  .code=0x85FDUL, .mask=0xFFFFUL, .ops={UO_R8_YL, UO_NONE, UO_NONE}},
  // ADC A,YL
  {.mnemo=UT_ADC,  .code=0x8DFDUL, .mask=0xFFFFUL, .ops={UO_R8_A, UO_R8_YL, UO_NONE}},
  // ADC YL
  {.mnemo=UT_ADC,  .code=0x8DFDUL, .mask=0xFFFFUL, .ops={UO_R8_YL, UO_NONE, UO_NONE}},
  // SUB YL
  {.mnemo=UT_SUB,  .code=0x95FDUL, .mask=0xFFFFUL, .ops={UO_R8_YL, UO_NONE, UO_NONE}},
  // SUB A,YL
  {.mnemo=UT_SUB,  .code=0x95FDUL, .mask=0xFFFFUL, .ops={UO_R8_A, UO_R8_YL, UO_NONE}},
  // SBC A,YL
  {.mnemo=UT_SBC,  .code=0x9DFDUL, .mask=0xFFFFUL, .ops={UO_R8_A, UO_R8_YL, UO_NONE}},
  // SBC YL
  {.mnemo=UT_SBC,  .code=0x9DFDUL, .mask=0xFFFFUL, .ops={UO_R8_YL, UO_NONE, UO_NONE}},
  // AND YL
  {.mnemo=UT_AND,  .code=0xA5FDUL, .mask=0xFFFFUL, .ops={UO_R8_YL, UO_NONE, UO_NONE}},
  // AND A,YL
  {.mnemo=UT_AND,  .code=0xA5FDUL, .mask=0xFFFFUL, .ops={UO_R8_A, UO_R8_YL, UO_NONE}},
  // XOR YL
  {.mnemo=UT_XOR,  .code=0xADFDUL, .mask=0xFFFFUL, .ops={UO_R8_YL, UO_NONE, UO_NONE}},
  // XOR A,YL
  {.mnemo=UT_XOR,  .code=0xADFDUL, .mask=0xFFFFUL, .ops={UO_R8_A, UO_R8_YL, UO_NONE}},
  // OR YL
  {.mnemo=UT_OR,   .code=0xB5FDUL, .mask=0xFFFFUL, .ops={UO_R8_YL, UO_NONE, UO_NONE}},
  // OR A,YL
  {.mnemo=UT_OR,   .code=0xB5FDUL, .mask=0xFFFFUL, .ops={UO_R8_A, UO_R8_YL, UO_NONE}},
  // CP YL
  {.mnemo=UT_CP,   .code=0xBDFDUL, .mask=0xFFFFUL, .ops={UO_R8_YL, UO_NONE, UO_NONE}},
  // CP A,YL
  {.mnemo=UT_CP,   .code=0xBDFDUL, .mask=0xFFFFUL, .ops={UO_R8_A, UO_R8_YL, UO_NONE}},

  // LD XH,XH
  {.mnemo=UT_LD,   .code=0x64DDUL, .mask=0xFFFFUL, .ops={UO_R8_XH, UO_R8_XH, UO_NONE}},
  // LD XH,XL
  {.mnemo=UT_LD,   .code=0x65DDUL, .mask=0xFFFFUL, .ops={UO_R8_XH, UO_R8_XL, UO_NONE}},
  // LD XL,XH
  {.mnemo=UT_LD,   .code=0x6CDDUL, .mask=0xFFFFUL, .ops={UO_R8_XL, UO_R8_XH, UO_NONE}},
  // LD XL,XL
  {.mnemo=UT_LD,   .code=0x6DDDUL, .mask=0xFFFFUL, .ops={UO_R8_XL, UO_R8_XL, UO_NONE}},
  // LD YH,YH
  {.mnemo=UT_LD,   .code=0x64FDUL, .mask=0xFFFFUL, .ops={UO_R8_YH, UO_R8_YH, UO_NONE}},
  // LD YH,YL
  {.mnemo=UT_LD,   .code=0x65FDUL, .mask=0xFFFFUL, .ops={UO_R8_YH, UO_R8_YL, UO_NONE}},
  // LD YL,YH
  {.mnemo=UT_LD,   .code=0x6CFDUL, .mask=0xFFFFUL, .ops={UO_R8_YL, UO_R8_YH, UO_NONE}},
  // LD YL,YL
  {.mnemo=UT_LD,   .code=0x6DFDUL, .mask=0xFFFFUL, .ops={UO_R8_YL, UO_R8_YL, UO_NONE}},

  // LD (nnnn),IX
  {.mnemo=UT_LD,   .code=0x22DDUL, .mask=0xFFFFUL, .ops={UO_MEM16, UO_R16IX, UO_NONE}},
  // LD IX,(nnnn)
  {.mnemo=UT_LD,   .code=0x2ADDUL, .mask=0xFFFFUL, .ops={UO_R16IX, UO_MEM16, UO_NONE}},
  // LD (nnnn),IY
  {.mnemo=UT_LD,   .code=0x22FDUL, .mask=0xFFFFUL, .ops={UO_MEM16, UO_R16IY, UO_NONE}},
  // LD IY,(nnnn)
  {.mnemo=UT_LD,   .code=0x2AFDUL, .mask=0xFFFFUL, .ops={UO_R16IY, UO_MEM16, UO_NONE}},

  // LD IX,nnnn
  {.mnemo=UT_LD,   .code=0x21DDUL, .mask=0xFFFFUL, .ops={UO_R16IX, UO_IMM16, UO_NONE}},
  // LD IY,nnnn
  {.mnemo=UT_LD,   .code=0x21FDUL, .mask=0xFFFFUL, .ops={UO_R16IY, UO_IMM16, UO_NONE}},

  // INC IX
  {.mnemo=UT_INC,   .code=0x23DDUL, .mask=0xFFFFUL, .ops={UO_R16IX, UO_NONE, UO_NONE}},
  // DEC IX
  {.mnemo=UT_DEC,   .code=0x2BDDUL, .mask=0xFFFFUL, .ops={UO_R16IX, UO_NONE, UO_NONE}},
  // INC IY
  {.mnemo=UT_INC,   .code=0x23FDUL, .mask=0xFFFFUL, .ops={UO_R16IY, UO_NONE, UO_NONE}},
  // DEC IY
  {.mnemo=UT_DEC,   .code=0x2BFDUL, .mask=0xFFFFUL, .ops={UO_R16IY, UO_NONE, UO_NONE}},

  // INC (IX+d)
  {.mnemo=UT_INC,  .code=0x34DDUL, .mask=0xFFFFUL, .ops={UO_MIX, UO_NONE, UO_NONE}},
  // DEC (IX+d)
  {.mnemo=UT_DEC,  .code=0x35DDUL, .mask=0xFFFFUL, .ops={UO_MIX, UO_NONE, UO_NONE}},
  // LD (IX+d),nn
  {.mnemo=UT_LD,   .code=0x36DDUL, .mask=0xFFFFUL, .ops={UO_MIX, UO_IMM8, UO_NONE}},
  // INC (IY+d)
  {.mnemo=UT_INC,  .code=0x34FDUL, .mask=0xFFFFUL, .ops={UO_MIY, UO_NONE, UO_NONE}},
  // DEC (IY+d)
  {.mnemo=UT_DEC,  .code=0x35FDUL, .mask=0xFFFFUL, .ops={UO_MIY, UO_NONE, UO_NONE}},
  // LD (IY+d),nn
  {.mnemo=UT_LD,   .code=0x36FDUL, .mask=0xFFFFUL, .ops={UO_MIY, UO_IMM8, UO_NONE}},

  // INC XH
  {.mnemo=UT_INC,  .code=0x24DDUL, .mask=0xFFFFUL, .ops={UO_R8_XH, UO_NONE, UO_NONE}},
  // DEC XH
  {.mnemo=UT_DEC,  .code=0x25DDUL, .mask=0xFFFFUL, .ops={UO_R8_XH, UO_NONE, UO_NONE}},
  // INC XL
  {.mnemo=UT_INC,  .code=0x2CDDUL, .mask=0xFFFFUL, .ops={UO_R8_XL, UO_NONE, UO_NONE}},
  // DEC XL
  {.mnemo=UT_DEC,  .code=0x2DDDUL, .mask=0xFFFFUL, .ops={UO_R8_XL, UO_NONE, UO_NONE}},
  // INC YH
  {.mnemo=UT_INC,  .code=0x24FDUL, .mask=0xFFFFUL, .ops={UO_R8_YH, UO_NONE, UO_NONE}},
  // DEC YH
  {.mnemo=UT_DEC,  .code=0x25FDUL, .mask=0xFFFFUL, .ops={UO_R8_YH, UO_NONE, UO_NONE}},
  // INC YL
  {.mnemo=UT_INC,  .code=0x2CFDUL, .mask=0xFFFFUL, .ops={UO_R8_YL, UO_NONE, UO_NONE}},
  // DEC YL
  {.mnemo=UT_DEC,  .code=0x2DFDUL, .mask=0xFFFFUL, .ops={UO_R8_YL, UO_NONE, UO_NONE}},

  // LD XH,nn
  {.mnemo=UT_LD,   .code=0x26DDUL, .mask=0xFFFFUL, .ops={UO_R8_XH, UO_IMM8, UO_NONE}},
  // LD XL,nn
  {.mnemo=UT_LD,   .code=0x2EDDUL, .mask=0xFFFFUL, .ops={UO_R8_XL, UO_IMM8, UO_NONE}},
  // LD YH,nn
  {.mnemo=UT_LD,   .code=0x26FDUL, .mask=0xFFFFUL, .ops={UO_R8_YH, UO_IMM8, UO_NONE}},
  // LD YL,nn
  {.mnemo=UT_LD,   .code=0x2EFDUL, .mask=0xFFFFUL, .ops={UO_R8_YL, UO_IMM8, UO_NONE}},

  // ADD IX,BC
  {.mnemo=UT_ADD,  .code=0x09DDUL, .mask=0xFFFFUL, .ops={UO_R16IX, UO_R16BC, UO_NONE}},
  // ADD IX,DE
  {.mnemo=UT_ADD,  .code=0x19DDUL, .mask=0xFFFFUL, .ops={UO_R16IX, UO_R16DE, UO_NONE}},
  // ADD IX,IX
  {.mnemo=UT_ADD,  .code=0x29DDUL, .mask=0xFFFFUL, .ops={UO_R16IX, UO_R16IX, UO_NONE}},
  // ADD IX,SP
  {.mnemo=UT_ADD,  .code=0x39DDUL, .mask=0xFFFFUL, .ops={UO_R16IX, UO_R16SP, UO_NONE}},
  // ADD IY,BC
  {.mnemo=UT_ADD,  .code=0x09FDUL, .mask=0xFFFFUL, .ops={UO_R16IY, UO_R16BC, UO_NONE}},
  // ADD IY,DE
  {.mnemo=UT_ADD,  .code=0x19FDUL, .mask=0xFFFFUL, .ops={UO_R16IY, UO_R16DE, UO_NONE}},
  // ADD IY,IY
  {.mnemo=UT_ADD,  .code=0x29FDUL, .mask=0xFFFFUL, .ops={UO_R16IY, UO_R16IY, UO_NONE}},
  // ADD IY,SP
  {.mnemo=UT_ADD,  .code=0x39FDUL, .mask=0xFFFFUL, .ops={UO_R16IY, UO_R16SP, UO_NONE}},

  // LD XH,r8
  {.mnemo=UT_LD,   .code=0x60DDUL, .mask=0xF8FFUL, .ops={UO_R8_XH, UO_R8_NOM_NOHL, UO_NONE}},
  // LD XL,r8
  {.mnemo=UT_LD,   .code=0x68DDUL, .mask=0xF8FFUL, .ops={UO_R8_XL, UO_R8_NOM_NOHL, UO_NONE}},
  // LD (IX+d),r8
  {.mnemo=UT_LD,   .code=0x70DDUL, .mask=0xF8FFUL, .ops={UO_MIX, UO_R8_NOM, UO_NONE}},
  // LD YH,r8
  {.mnemo=UT_LD,   .code=0x60FDUL, .mask=0xF8FFUL, .ops={UO_R8_YH, UO_R8_NOM_NOHL, UO_NONE}},
  // LD YL,r8
  {.mnemo=UT_LD,   .code=0x68FDUL, .mask=0xF8FFUL, .ops={UO_R8_YL, UO_R8_NOM_NOHL, UO_NONE}},
  // LD (IY+d),r8
  {.mnemo=UT_LD,   .code=0x70FDUL, .mask=0xF8FFUL, .ops={UO_MIY, UO_R8_NOM, UO_NONE}},

  // LD r8,XH
  {.mnemo=UT_LD,   .code=0x44DDUL, .mask=0xC7FFUL, .ops={UO_2R8_NOM_NOHL, UO_R8_XH, UO_NONE}},
  // LD r8,XL
  {.mnemo=UT_LD,   .code=0x45DDUL, .mask=0xC7FFUL, .ops={UO_2R8_NOM_NOHL, UO_R8_XL, UO_NONE}},
  // LD r8,(IX+d)
  {.mnemo=UT_LD,   .code=0x46DDUL, .mask=0xC7FFUL, .ops={UO_2R8_NOM, UO_MIX, UO_NONE}},

  // LD r8,YH
  {.mnemo=UT_LD,   .code=0x44FDUL, .mask=0xC7FFUL, .ops={UO_2R8_NOM_NOHL, UO_R8_YH, UO_NONE}},
  // LD r8,YL
  {.mnemo=UT_LD,   .code=0x45FDUL, .mask=0xC7FFUL, .ops={UO_2R8_NOM_NOHL, UO_R8_YL, UO_NONE}},
  // LD r8,(IY+d)
  {.mnemo=UT_LD,   .code=0x46FDUL, .mask=0xC7FFUL, .ops={UO_2R8_NOM, UO_MIY, UO_NONE}},

  // instructions w/o operands or with unchangeable operands
  {.mnemo=UT_NOP,  .code=0x00UL, .mask=0xFFUL, .ops={UO_NONE, UO_NONE, UO_NONE}},
  {.mnemo=UT_RLCA, .code=0x07UL, .mask=0xFFUL, .ops={UO_NONE, UO_NONE, UO_NONE}},
  {.mnemo=UT_RRCA, .code=0x0FUL, .mask=0xFFUL, .ops={UO_NONE, UO_NONE, UO_NONE}},
  {.mnemo=UT_RLA,  .code=0x17UL, .mask=0xFFUL, .ops={UO_NONE, UO_NONE, UO_NONE}},
  {.mnemo=UT_RRA,  .code=0x1FUL, .mask=0xFFUL, .ops={UO_NONE, UO_NONE, UO_NONE}},
  {.mnemo=UT_DAA,  .code=0x27UL, .mask=0xFFUL, .ops={UO_NONE, UO_NONE, UO_NONE}},
  {.mnemo=UT_CPL,  .code=0x2FUL, .mask=0xFFUL, .ops={UO_NONE, UO_NONE, UO_NONE}},
  {.mnemo=UT_SCF,  .code=0x37UL, .mask=0xFFUL, .ops={UO_NONE, UO_NONE, UO_NONE}},
  {.mnemo=UT_CCF,  .code=0x3FUL, .mask=0xFFUL, .ops={UO_NONE, UO_NONE, UO_NONE}},
  {.mnemo=UT_HALT, .code=0x76UL, .mask=0xFFUL, .ops={UO_NONE, UO_NONE, UO_NONE}},
  {.mnemo=UT_RET,  .code=0xC9UL, .mask=0xFFUL, .ops={UO_NONE, UO_NONE, UO_NONE}},
  {.mnemo=UT_EXX,  .code=0xD9UL, .mask=0xFFUL, .ops={UO_NONE, UO_NONE, UO_NONE}},
  {.mnemo=UT_DI,   .code=0xF3UL, .mask=0xFFUL, .ops={UO_NONE, UO_NONE, UO_NONE}},
  {.mnemo=UT_EI,   .code=0xFBUL, .mask=0xFFUL, .ops={UO_NONE, UO_NONE, UO_NONE}},
  // LD SP,HL
  {.mnemo=UT_LD,   .code=0xF9UL, .mask=0xFFUL, .ops={UO_R16SP, UO_R16HL, UO_NONE}},
  // EX AF,AF'
  {.mnemo=UT_EX,   .code=0x08UL, .mask=0xFFUL, .ops={UO_R16AF, UO_R16AFX, UO_NONE}},
  // EX AF',AF (ditto)
  {.mnemo=UT_EX,   .code=0x08UL, .mask=0xFFUL, .ops={UO_R16AFX, UO_R16AF, UO_NONE}},
  // EX (SP),HL
  {.mnemo=UT_EX,   .code=0xE3UL, .mask=0xFFUL, .ops={UO_MSP, UO_R16HL, UO_NONE}},
  // EX HL,(SP) (ditto)
  {.mnemo=UT_EX,   .code=0xE3UL, .mask=0xFFUL, .ops={UO_R16HL, UO_MSP, UO_NONE}},
  // EX DE,HL
  {.mnemo=UT_EX,   .code=0xEBUL, .mask=0xFFUL, .ops={UO_R16DE, UO_R16HL, UO_NONE}},
  // EX HL,DE (ditto)
  {.mnemo=UT_EX,   .code=0xEBUL, .mask=0xFFUL, .ops={UO_R16HL, UO_R16DE, UO_NONE}},
  // JP (HL)
  {.mnemo=UT_JP,   .code=0xE9UL, .mask=0xFFUL, .ops={UO_MHL, UO_NONE, UO_NONE}},
  // JP HL
  {.mnemo=UT_JP,   .code=0xE9UL, .mask=0xFFUL, .ops={UO_R16HL, UO_NONE, UO_NONE}},
  // JP nnnn
  {.mnemo=UT_JP,   .code=0xC3UL, .mask=0xFFUL, .ops={UO_ADDR16, UO_NONE, UO_NONE}},
  // CALL nnnn
  {.mnemo=UT_CALL, .code=0xCDUL, .mask=0xFFUL, .ops={UO_ADDR16, UO_NONE, UO_NONE}},
  // OUT (n),A
  {.mnemo=UT_OUT,  .code=0xD3UL, .mask=0xFFUL, .ops={UO_PORTIMM, UO_R8_A, UO_NONE}},
  // IN A,(n)
  {.mnemo=UT_IN,   .code=0xDBUL, .mask=0xFFUL, .ops={UO_R8_A, UO_PORTIMM, UO_NONE}},

  // ADD A,nn
  {.mnemo=UT_ADD,  .code=0xC6UL, .mask=0xFFUL, .ops={UO_R8_A, UO_IMM8, UO_NONE}},
  // ADD nn (ditto)
  {.mnemo=UT_ADD,  .code=0xC6UL, .mask=0xFFUL, .ops={UO_IMM8, UO_NONE, UO_NONE}},
  // ADC A,nn
  {.mnemo=UT_ADC,  .code=0xCEUL, .mask=0xFFUL, .ops={UO_R8_A, UO_IMM8, UO_NONE}},
  // ADC nn (ditto)
  {.mnemo=UT_ADC,  .code=0xCEUL, .mask=0xFFUL, .ops={UO_IMM8, UO_NONE, UO_NONE}},
  // SUB nn
  {.mnemo=UT_SUB,  .code=0xD6UL, .mask=0xFFUL, .ops={UO_IMM8, UO_NONE, UO_NONE}},
  // SUB A,nn (ditto)
  {.mnemo=UT_SUB,  .code=0xD6UL, .mask=0xFFUL, .ops={UO_R8_A, UO_IMM8, UO_NONE}},
  // SBC A,nn
  {.mnemo=UT_SBC,  .code=0xDEUL, .mask=0xFFUL, .ops={UO_R8_A, UO_IMM8, UO_NONE}},
  // SBC nn (ditto)
  {.mnemo=UT_SBC,  .code=0xDEUL, .mask=0xFFUL, .ops={UO_IMM8, UO_NONE, UO_NONE}},
  // AND nn
  {.mnemo=UT_AND,  .code=0xE6UL, .mask=0xFFUL, .ops={UO_IMM8, UO_NONE, UO_NONE}},
  // AND A,nn (ditto)
  {.mnemo=UT_AND,  .code=0xE6UL, .mask=0xFFUL, .ops={UO_R8_A, UO_IMM8, UO_NONE}},
  // XOR nn
  {.mnemo=UT_XOR,  .code=0xEEUL, .mask=0xFFUL, .ops={UO_IMM8, UO_NONE, UO_NONE}},
  // XOR A,nn (ditto)
  {.mnemo=UT_XOR,  .code=0xEEUL, .mask=0xFFUL, .ops={UO_R8_A, UO_IMM8, UO_NONE}},
  // OR nn
  {.mnemo=UT_OR,   .code=0xF6UL, .mask=0xFFUL, .ops={UO_IMM8, UO_NONE, UO_NONE}},
  // OR A,nn (ditto)
  {.mnemo=UT_OR,   .code=0xF6UL, .mask=0xFFUL, .ops={UO_R8_A, UO_IMM8, UO_NONE}},
  // CP nn
  {.mnemo=UT_CP,   .code=0xFEUL, .mask=0xFFUL, .ops={UO_IMM8, UO_NONE, UO_NONE}},
  // CP A,nn (ditto)
  {.mnemo=UT_CP,   .code=0xFEUL, .mask=0xFFUL, .ops={UO_R8_A, UO_IMM8, UO_NONE}},
  // LD (BC),A
  {.mnemo=UT_LD,   .code=0x02UL, .mask=0xFFUL, .ops={UO_MBC, UO_R8_A, UO_NONE}},
  // LD (DE),A
  {.mnemo=UT_LD,   .code=0x12UL, .mask=0xFFUL, .ops={UO_MDE, UO_R8_A, UO_NONE}},
  // LD A,(BC)
  {.mnemo=UT_LD,   .code=0x0AUL, .mask=0xFFUL, .ops={UO_R8_A, UO_MBC, UO_NONE}},
  // LD A,(DE)
  {.mnemo=UT_LD,   .code=0x1AUL, .mask=0xFFUL, .ops={UO_R8_A, UO_MDE, UO_NONE}},
  // LD (nnnn),HL
  {.mnemo=UT_LD,   .code=0x22UL, .mask=0xFFUL, .ops={UO_MEM16, UO_R16HL, UO_NONE}},
  // LD HL,(nnnn)
  {.mnemo=UT_LD,   .code=0x2AUL, .mask=0xFFUL, .ops={UO_R16HL, UO_MEM16, UO_NONE}},
  // LD (nnnn),A
  {.mnemo=UT_LD,   .code=0x32UL, .mask=0xFFUL, .ops={UO_MEM16, UO_R8_A, UO_NONE}},
  // LD A,(nnnn)
  {.mnemo=UT_LD,   .code=0x3AUL, .mask=0xFFUL, .ops={UO_R8_A, UO_MEM16, UO_NONE}},
  // DJNZ d
  {.mnemo=UT_DJNZ, .code=0x10UL, .mask=0xFFUL, .ops={UO_ADDR8, UO_NONE, UO_NONE}},
  // JR d
  {.mnemo=UT_JR,   .code=0x18UL, .mask=0xFFUL, .ops={UO_ADDR8, UO_NONE, UO_NONE}},

  // ADD HL,r16
  {.mnemo=UT_ADD,  .code=0x09UL, .mask=0xCFUL, .ops={UO_R16HL, UO_R16, UO_NONE}},

  // ADD A,r8
  {.mnemo=UT_ADD,  .code=0x80UL, .mask=0xF8UL, .ops={UO_R8_A, UO_R8, UO_NONE}},
  // ADD r8
  {.mnemo=UT_ADD,  .code=0x80UL, .mask=0xF8UL, .ops={UO_R8, UO_NONE, UO_NONE}},
  // ADC A,r8
  {.mnemo=UT_ADC,  .code=0x88UL, .mask=0xF8UL, .ops={UO_R8_A, UO_R8, UO_NONE}},
  // ADC r8
  {.mnemo=UT_ADC,  .code=0x88UL, .mask=0xF8UL, .ops={UO_R8, UO_NONE, UO_NONE}},
  // SUB r8
  {.mnemo=UT_SUB,  .code=0x90UL, .mask=0xF8UL, .ops={UO_R8, UO_NONE, UO_NONE}},
  // SUB A,r8
  {.mnemo=UT_SUB,  .code=0x90UL, .mask=0xF8UL, .ops={UO_R8_A, UO_R8, UO_NONE}},
  // SBC A,r8
  {.mnemo=UT_SBC,  .code=0x98UL, .mask=0xF8UL, .ops={UO_R8_A, UO_R8, UO_NONE}},
  // SBC r8
  {.mnemo=UT_SBC,  .code=0x98UL, .mask=0xF8UL, .ops={UO_R8, UO_NONE, UO_NONE}},
  // AND r8
  {.mnemo=UT_AND,  .code=0xA0UL, .mask=0xF8UL, .ops={UO_R8, UO_NONE, UO_NONE}},
  // AND A,r8
  {.mnemo=UT_AND,  .code=0xA0UL, .mask=0xF8UL, .ops={UO_R8_A, UO_R8, UO_NONE}},
  // XOR r8
  {.mnemo=UT_XOR,  .code=0xA8UL, .mask=0xF8UL, .ops={UO_R8, UO_NONE, UO_NONE}},
  // XOR A,r8
  {.mnemo=UT_XOR,  .code=0xA8UL, .mask=0xF8UL, .ops={UO_R8_A, UO_R8, UO_NONE}},
  // OR r8
  {.mnemo=UT_OR,   .code=0xB0UL, .mask=0xF8UL, .ops={UO_R8, UO_NONE, UO_NONE}},
  // OR A,r8
  {.mnemo=UT_OR,   .code=0xB0UL, .mask=0xF8UL, .ops={UO_R8_A, UO_R8, UO_NONE}},
  // CP r8
  {.mnemo=UT_CP,   .code=0xB8UL, .mask=0xF8UL, .ops={UO_R8, UO_NONE, UO_NONE}},
  // CP A,r8
  {.mnemo=UT_CP,   .code=0xB8UL, .mask=0xF8UL, .ops={UO_R8_A, UO_R8, UO_NONE}},

  // JR cc,d
  {.mnemo=UT_JR,   .code=0x20UL, .mask=0xE7UL, .ops={UO_JRCOND, UO_ADDR8, UO_NONE}},

  // POP r16
  {.mnemo=UT_POP,  .code=0xC1UL, .mask=0xCFUL, .ops={UO_R16A, UO_NONE, UO_NONE}},
  // PUSH r16
  {.mnemo=UT_PUSH, .code=0xC5UL, .mask=0xCFUL, .ops={UO_R16A, UO_NONE, UO_NONE}},
  // RET cc
  {.mnemo=UT_RET,  .code=0xC0UL, .mask=0xC7UL, .ops={UO_COND, UO_NONE, UO_NONE}},
  // JP cc,nnnn
  {.mnemo=UT_JP,   .code=0xC2UL, .mask=0xC7UL, .ops={UO_COND, UO_ADDR16, UO_NONE}},
  // CALL cc,nnnn
  {.mnemo=UT_CALL, .code=0xC4UL, .mask=0xC7UL, .ops={UO_COND, UO_ADDR16, UO_NONE}},
  // RST n
  {.mnemo=UT_RST,  .code=0xC7UL, .mask=0xC7UL, .ops={UO_RSTDEST, UO_NONE, UO_NONE}},

  // INC r8
  {.mnemo=UT_INC,  .code=0x04UL, .mask=0xC7UL, .ops={UO_2R8, UO_NONE, UO_NONE}},
  // DEC r8
  {.mnemo=UT_DEC,  .code=0x05UL, .mask=0xC7UL, .ops={UO_2R8, UO_NONE, UO_NONE}},
  // LD r8,nn
  {.mnemo=UT_LD,   .code=0x06UL, .mask=0xC7UL, .ops={UO_2R8, UO_IMM8, UO_NONE}},

  // LD r16,nnnn
  {.mnemo=UT_LD,   .code=0x01UL, .mask=0xCFUL, .ops={UO_R16, UO_IMM16, UO_NONE}},
  // INC r16
  {.mnemo=UT_INC,  .code=0x03UL, .mask=0xCFUL, .ops={UO_R16, UO_NONE, UO_NONE}},
  // DEC r16
  {.mnemo=UT_DEC,  .code=0x0BUL, .mask=0xCFUL, .ops={UO_R16, UO_NONE, UO_NONE}},

  // LD r8,r8
  {.mnemo=UT_LD,   .code=0x40UL, .mask=0xC0UL, .ops={UO_2R8, UO_R8, UO_NONE}},

  // syntetics
  // LD BC,BC
  {.mnemo=UT_LD,   .code=0x4940UL, .mask=0xFFFFUL, .ops={UO_R16BC, UO_R16BC, UO_NONE}},
  // LD BC,DE
  {.mnemo=UT_LD,   .code=0x4B42UL, .mask=0xFFFFUL, .ops={UO_R16BC, UO_R16DE, UO_NONE}},
  // LD BC,HL
  {.mnemo=UT_LD,   .code=0x4D44UL, .mask=0xFFFFUL, .ops={UO_R16BC, UO_R16HL, UO_NONE}},
  // LD DE,BC
  {.mnemo=UT_LD,   .code=0x5950UL, .mask=0xFFFFUL, .ops={UO_R16DE, UO_R16BC, UO_NONE}},
  // LD DE,DE
  {.mnemo=UT_LD,   .code=0x5B52UL, .mask=0xFFFFUL, .ops={UO_R16DE, UO_R16DE, UO_NONE}},
  // LD DE,HL
  {.mnemo=UT_LD,   .code=0x5D54UL, .mask=0xFFFFUL, .ops={UO_R16DE, UO_R16HL, UO_NONE}},
  // LD HL,BC
  {.mnemo=UT_LD,   .code=0x6960UL, .mask=0xFFFFUL, .ops={UO_R16HL, UO_R16BC, UO_NONE}},
  // LD HL,DE
  {.mnemo=UT_LD,   .code=0x6B62UL, .mask=0xFFFFUL, .ops={UO_R16HL, UO_R16DE, UO_NONE}},
  // LD HL,HL
  {.mnemo=UT_LD,   .code=0x6D64UL, .mask=0xFFFFUL, .ops={UO_R16HL, UO_R16HL, UO_NONE}},

  // LD BC,IX
  {.mnemo=UT_LD,   .code=0x4DDD44DDUL, .mask=0xFFFFFFFFUL, .ops={UO_R16BC, UO_R16IX, UO_NONE}},
  // LD BC,IY
  {.mnemo=UT_LD,   .code=0x4DFD44FDUL, .mask=0xFFFFFFFFUL, .ops={UO_R16BC, UO_R16IY, UO_NONE}},
  // LD DE,IX
  {.mnemo=UT_LD,   .code=0x5DDD54DDUL, .mask=0xFFFFFFFFUL, .ops={UO_R16DE, UO_R16IX, UO_NONE}},
  // LD DE,IY
  {.mnemo=UT_LD,   .code=0x5DFD54FDUL, .mask=0xFFFFFFFFUL, .ops={UO_R16DE, UO_R16IY, UO_NONE}},
  // LD IX,BC
  {.mnemo=UT_LD,   .code=0x69DD60DDUL, .mask=0xFFFFFFFFUL, .ops={UO_R16IX, UO_R16BC, UO_NONE}},
  // LD IY,BC
  {.mnemo=UT_LD,   .code=0x69FD60FDUL, .mask=0xFFFFFFFFUL, .ops={UO_R16IY, UO_R16BC, UO_NONE}},
  // LD IX,DE
  {.mnemo=UT_LD,   .code=0x6BDD62DDUL, .mask=0xFFFFFFFFUL, .ops={UO_R16IX, UO_R16DE, UO_NONE}},
  // LD IY,DE
  {.mnemo=UT_LD,   .code=0x6BFD62FDUL, .mask=0xFFFFFFFFUL, .ops={UO_R16IY, UO_R16DE, UO_NONE}},
};


// instructions unaffected by DD/FF prefixes
// this table used by disassembler (we don't want to eat prefixes)
const unsigned char URASM_DDFD_INSENSITIVE[256] = {
1,1,1,1,1,1,1,1,1,0,1,1,1,1,1,1,
1,1,1,1,1,1,1,1,1,0,1,1,1,1,1,1,
1,0,0,0,0,0,0,1,1,0,0,0,0,0,0,1,
1,1,1,1,0,0,0,1,1,0,1,1,1,1,1,1,
1,1,1,1,0,0,0,1,1,1,1,1,0,0,0,1,
1,1,1,1,0,0,0,1,1,1,1,1,0,0,0,1,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,1,0,1,1,1,1,0,0,0,1,
1,1,1,1,0,0,0,1,1,1,1,1,0,0,0,1,
1,1,1,1,0,0,0,1,1,1,1,1,0,0,0,1,
1,1,1,1,0,0,0,1,1,1,1,1,0,0,0,1,
1,1,1,1,0,0,0,1,1,1,1,1,0,0,0,1,
1,1,1,1,1,1,1,1,1,1,1,0,1,1,1,1,
1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,
1,0,1,0,1,0,1,1,1,0,1,1,1,1,1,1,
1,1,1,1,1,1,1,1,1,0,1,1,1,1,1,1,
/*
0xff,0xbf,0xff,0xbf,0x81,0x81,0xf1,0xbf,0xf1,0xf1,0xf1,0xf1,0x00,0x00,0x02,0xf1,
0xf1,0xf1,0xf1,0xf1,0xf1,0xf1,0xf1,0xf1,0xff,0xef,0xff,0xff,0xab,0xbf,0xff,0xbf,
*/
};

#define IS_DD_SENSITIVE(c)  URASM_DDFD_INSENSITIVE[(c)&0xff]
/*
static inline int IS_DD_SENSITIVE (uint8_t opc) {
  return URASM_DDFD_INSENSITIVE[opc/8]&(0x80>>(opc%8));
}
*/


#if 0
const unsigned char URASM_DDFD_HAS_DISP[256] = {
/*
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,1,1,1,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,1,0,0,0,0,0,0,0,1,0,
0,0,0,0,0,0,1,0,0,0,0,0,0,0,1,0,
0,0,0,0,0,0,1,0,0,0,0,0,0,0,1,0,
1,1,1,1,1,1,0,1,0,0,0,0,0,0,1,0,
0,0,0,0,0,0,1,0,0,0,0,0,0,0,1,0,
0,0,0,0,0,0,1,0,0,0,0,0,0,0,1,0,
0,0,0,0,0,0,1,0,0,0,0,0,0,0,1,0,
0,0,0,0,0,0,1,0,0,0,0,0,0,0,1,0,
0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
*/
0x00,0x00,0x00,0x00,0x00,0x00,0x0e,0x00,0x02,0x02,0x02,0x02,0x02,0x02,0xfd,0x02,
0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x00,0x10,0x00,0x00,0x00,0x00,0x00,0x00,
};

static inline int HAS_DDFD_DISP (uint8_t opc) {
  return URASM_DDFD_HAS_DISP[opc/8]&(0x80>>(opc%8));
}
#endif


static __attribute__((always_inline)) inline const char *label_by_addr (uint16_t addr) {
  if (!urasm_label_by_addr) return NULL;
  return urasm_label_by_addr(addr);
}


static __attribute__((always_inline)) inline char ur_toupper (char ch) {
  return (ch >= 'a' && ch <= 'z' ? ch-'a'+'A' : ch);
}


static __attribute__((always_inline)) inline char ur_isalpha (char ch) {
  return
    (ch >= 'A' && ch <= 'Z') ||
    (ch >= 'a' && ch <= 'z');
}


static __attribute__((always_inline)) inline char ur_isalnum (char ch) {
  return
    (ch >= 'A' && ch <= 'Z') ||
    (ch >= 'a' && ch <= 'z') ||
    (ch >= '0' && ch <= '9');
}


static __attribute__((always_inline)) inline char ur_isspace (char ch) {
  return (ch != 0 && ((ch&0xff) <= 32 || ch == 127));
}


static inline int dig2n (char ch, int base) {
  if (ch < '0') return -1;
  if (base <= 10) {
    if (ch > '0'+base) return -1;
    return ch-'0';
  }
  if (ch >= '0' && ch <= '9') return ch-'0';
       if (ch >= 'a' && ch <= 'z') ch = ch-'a'+10;
  else if (ch >= 'A' && ch <= 'Z') ch = ch-'A'+10;
  else return -1;
  if (ch >= base) return -1;
  return ch;
}


/******************************************************************************/
/* disassembler                                                               */
/******************************************************************************/

/* opc: opcode */
/* nextW: next 2 bytes (after opcode) */
/* idx: I? displacement */
static void ua_op2str (char *res, int op, uint16_t addr, uint8_t opc, uint16_t nextW, int idx) {
  const char *lbl;
  int add, ismem = 0;
  switch (op) {
    case UO_NONE: case UO_NEXT: strcpy(res, ""); break;
    case UO_IMM8:
      if (!urasm_disasm_decimal) sprintf(res, "#%02X", nextW&0xFFU); else sprintf(res, "%u", nextW&0xFFU);
      break;
    case UO_IMM16:
      if (!urasm_disasm_decimal) sprintf(res, "#%04X", nextW); else sprintf(res, "%u", nextW);
      break;
    case UO_IMM16BE:
      nextW = (uint16_t)(((nextW>>8)&0xffu)|((nextW<<8)&0xff00u));
      if (!urasm_disasm_decimal) sprintf(res, "#%04X", nextW); else sprintf(res, "%u", nextW);
      break;
    case UO_ADDR8:
      addr += 2; nextW &= 0xFFU;
      add = (nextW < 128 ? nextW : ((int)nextW)-256);
      addr += add;
      nextW = addr;
      /* fallthru */
    case UO_ADDR16:
dolabel:
      lbl = label_by_addr(nextW);
      if (lbl) { strcpy(res, lbl); break; }
      lbl = label_by_addr(nextW-1);
      if (lbl) { sprintf(res, "%s-1", lbl); break; }
      lbl = label_by_addr(nextW-2);
      if (lbl) { sprintf(res, "%s-2", lbl); break; }
      lbl = label_by_addr(nextW+1);
      if (lbl) { sprintf(res, "%s+1", lbl); break; }
      lbl = label_by_addr(nextW+2);
      if (lbl) { sprintf(res, "%s+2", lbl); break; }
      if (!urasm_disasm_decimal) sprintf(res, "#%04X", nextW); else sprintf(res, "%u", nextW);
      break;
    case UO_MEM16:
      ismem = 1;
      strcpy(res, "("); ++res;
      goto dolabel;
    case UO_R8:
    case UO_R8_NOM:
    case UO_R8_NOM_NOHL:
      strcpy(res, URA_REGS8[opc&0x07UL]);
      break;
    case UO_2R8:
    case UO_2R8_NOM:
    case UO_2R8_NOM_NOHL:
      strcpy(res, URA_REGS8[(opc>>3)&0x07UL]);
      break;
    case UO_PORTC: strcpy(res, "(C)"); break;
    case UO_PORTIMM:
      if (!urasm_disasm_decimal) sprintf(res, "(#%02X)", nextW&0xFFU); else sprintf(res, "(%u)", nextW&0xFFU);
      break;
    case UO_R8_XH: strcpy(res, "XH"); break;
    case UO_R8_XL: strcpy(res, "XL"); break;
    case UO_R8_YH: strcpy(res, "YH"); break;
    case UO_R8_YL: strcpy(res, "YL"); break;
    case UO_R8_A: strcpy(res, "A"); break;
    case UO_R8_B: strcpy(res, "B"); break;
    case UO_R8_R: strcpy(res, "R"); break;
    case UO_R8_I: strcpy(res, "I"); break;
    case UO_R16: strcpy(res, URA_REGS16[(opc>>4)&0x03UL]); break;
    case UO_R16A: strcpy(res, URA_REGS16A[(opc>>4)&0x03UL]); break;
    case UO_R16AF: strcpy(res, "AF"); break;
    case UO_R16AFX: strcpy(res, "AF'"); break;
    case UO_R16BC: strcpy(res, "BC"); break;
    case UO_R16DE: strcpy(res, "DE"); break;
    case UO_R16HL: strcpy(res, "HL"); break;
    case UO_R16IX: strcpy(res, "IX"); break;
    case UO_R16IY: strcpy(res, "IY"); break;
    case UO_R16SP: strcpy(res, "SP"); break;
    case UO_MSP: strcpy(res, "(SP)"); break;
    case UO_MBC: strcpy(res, "(BC)"); break;
    case UO_MDE: strcpy(res, "(DE)"); break;
    case UO_MHL: strcpy(res, "(HL)"); break;
    case UO_MIX0: strcpy(res, "(IX)"); break;
    case UO_MIY0: strcpy(res, "(IY)"); break;
    case UO_MIX:
      if (idx) {
        sprintf(res, "(IX%s%d)", (idx < 0 ? "" : "+"), idx);
      } else {
        strcpy(res, "(IX)");
      }
      break;
    case UO_MIY:
      if (idx) {
        sprintf(res, "(IY%s%d)", (idx < 0 ? "" : "+"), idx);
      } else {
        strcpy(res, "(IY)");
      }
      break;
    case UO_JRCOND: strcpy(res, URA_COND[(opc>>3)&0x03U]); break;
    case UO_COND: strcpy(res, URA_COND[(opc>>3)&0x07U]); break;
    case UO_BITN: sprintf(res, "%u", (opc>>3)&0x07U); break;
    case UO_RSTDEST:
      if (!urasm_disasm_decimal) sprintf(res, "#%02X", opc&0x38U); else sprintf(res, "%u", opc&0x38U);
      break;
    case UO_IM0: strcpy(res, "0"); break;
    case UO_IM1: strcpy(res, "1"); break;
    case UO_IM2: strcpy(res, "2"); break;
    default: strcpy(res, ""); break; /* we'll never come here */
  }
  if (ismem) strcat(res, ")");
}


/* common code for two disasm functions */
static inline int is_opc_invalid_op (int op, uint8_t opc) {
  switch (op) {
    case UO_R8_NOM:
      return ((opc&0x07U) == 6); /* bad (HL) */
    case UO_R8_NOM_NOHL:
      opc &= 0x07;
      return (opc >= 4 && opc <= 6); /* bad H,L,(HL) */
    case UO_2R8_NOM:
      return (((opc>>3)&0x07U) == 6); /* bad (HL) */
    case UO_2R8_NOM_NOHL:
      opc = (opc>>3)&0x07U;
      return (opc >= 4 && opc <= 6); /* bad H,L,(HL) */
  }
  return 0;
}


/* find the corresponding record in URASM_COMMANDS */
int urasm_disasm_opfind (uint16_t addr) {
  uint8_t buf[8];
  uint32_t ci, f, c;
  uint8_t opc;
  int bpos, opn, op;
  if (!urasm_getbyte) return -1;
  for (f = 0; f < 8; ++f) buf[f] = urasm_getbyte(addr+f);
  if (buf[0] == 0xDDU || buf[0] == 0xFDU) {
    /* dummy prefix */
    if (IS_DD_SENSITIVE(buf[1])) return (buf[0] == 0xDDU ? 0 : 1);
  }
  ci = ((uint32_t)buf[0]) | (((uint32_t)buf[1])<<8) | (((uint32_t)buf[2])<<16) | (((uint32_t)buf[3])<<24);
  for (opn = 0; opn < URASM_MAX_COMMAND; ++opn) {
    /* find command */
    for (; opn < URASM_MAX_COMMAND && (ci&URASM_COMMANDS[opn].mask) != URASM_COMMANDS[opn].code; ++opn) {}
    if (!urasm_allow_zxnext && URASM_COMMANDS[opn].ops[2] == UO_NEXT) opn = URASM_MAX_COMMAND; /* skip ZXNext if not allowed */
    if (opn >= URASM_MAX_COMMAND) {
      if (buf[0] == 0xEDU) return -2;
      return -1;
    }
    /* skip prefixes, determine command length */
    f = URASM_COMMANDS[opn].mask; c = URASM_COMMANDS[opn].code;
    for (bpos = 0; ; ++bpos) {
      uint8_t b;
      if ((f&0xFFUL) != 0xFFUL) break;
      b = c&0xFFUL;
      if (b != 0xFDU && b != 0xDDU && b != 0xEDU && b != 0xCBU) break;
      f >>= 8; c >>= 8;
    }
    /* are there any operands? */
    if (URASM_COMMANDS[opn].ops[0] == UO_NONE) return opn;
    /* is this CB-prefixed? */
    if (((URASM_COMMANDS[opn].code&0xFFFFUL) == 0xCBDDUL) ||
        ((URASM_COMMANDS[opn].code&0xFFFFUL) == 0xCBFDUL)) ++bpos; /* skip displacement */
    opc = buf[bpos];
    /* do operands */
    for (f = 0; f <= 3; ++f) {
      if (f == 3) return opn;
      op = URASM_COMMANDS[opn].ops[f];
      if (op == UO_NONE || op == UO_NEXT) return opn;
      /* check for valid operand */
      if (is_opc_invalid_op(op, opc)) break;
    }
  }
  return -1;
}


/* length of the corresponding instruction */
int urasm_disasm_oplen (int idx) {
  int res = 0, f, op;
  uint32_t m, c;
  if (idx == -2) return 2;
  if (idx < 0 || idx >= URASM_MAX_COMMAND) return 1;
  if (idx < 2) return 1;
  m = URASM_COMMANDS[idx].mask; c = URASM_COMMANDS[idx].code;
  /* I?/CB? */
  /*
  if (((m&0xFFFFUL) == 0xFFFFUL) &&
      ((c&0xFF00UL) == 0xCBUL) && ((c&0xFFUL) == 0xDDUL || (c&0xFFUL) == 0xFDUL)) return 4;
  */
  /* skip prefixes, determine command length */
  for (;;) {
    uint8_t b;
    if ((m&0xFFUL) != 0xFFUL) break;
    b = c&0xFFUL;
    if (b != 0xFDU && b != 0xDDU && b != 0xEDU && b != 0xCBU) break;
    m >>= 8; c >>= 8; ++res;
  }
  /* is this CB-prefixed? */
  if (((URASM_COMMANDS[idx].code&0xFFFFUL) == 0xCBDDUL) ||
      ((URASM_COMMANDS[idx].code&0xFFFFUL) == 0xCBFDUL)) m >>= 8;
  /* count opcodes */
  while (m != 0) { m >>= 8; ++res; }
  /* process operands */
  for (f = 0; f <= 2; ++f) {
    op = URASM_COMMANDS[idx].ops[f];
    if (op == UO_NONE || op == UO_NEXT) break;
    switch (op) {
      /* command with displacement */
      case UO_MIX: case UO_MIY: ++res; break;
      /* command has immediate operand */
      case UO_IMM8: case UO_ADDR8: case UO_PORTIMM: ++res; break;
      case UO_IMM16: case UO_IMM16BE: case UO_ADDR16: case UO_MEM16: res += 2; break;
    }
  }
  return res;
}


extern int urasm_disasm_opdisasm_ex (char *dstr, uint16_t addr, const uint8_t mem[8]) {
  int res, idx = 0;
  uint32_t ci, f, c;
  uint8_t opc;
  int bpos, opn, op;
  uint16_t nextW;
  char opstr[129];
  if (!dstr) return -1;
  if (mem[0] == 0xDDU || mem[0] == 0xFDU) {
    /* dummy prefix */
    if (IS_DD_SENSITIVE(mem[1])) {
      strcpy(dstr, URASM_TOKENS[mem[0]==0xDDU ? UT_NOPX : UT_NOPY]);
      return 1;
    }
    /* take possible I? displacement */
    idx = (int8_t)mem[2];
  }
  ci = ((uint32_t)mem[0]) | (((uint32_t)mem[1])<<8) | (((uint32_t)mem[2])<<16) | (((uint32_t)mem[3])<<24);
  for (opn = 0; opn < URASM_MAX_COMMAND; ++opn) {
    res = 0; dstr[0] = '\0';
    /* find command */
    for (; opn < URASM_MAX_COMMAND && (ci&URASM_COMMANDS[opn].mask) != URASM_COMMANDS[opn].code; ++opn) {}
    if (!urasm_allow_zxnext && URASM_COMMANDS[opn].ops[2] == UO_NEXT) opn = URASM_MAX_COMMAND; /* skip ZXNext if not allowed */
    if (opn >= URASM_MAX_COMMAND) {
      if (mem[0] == 0xEDU) {
        if (!urasm_disasm_decimal) sprintf(opstr, "#%02X,#%02X", mem[0], mem[1]); else sprintf(opstr, "%u,%u", mem[0], mem[1]);
        /*if (!urasm_disasm_decimal) sprintf(opstr, "#%04X", (uint16_t)buf[0]|(((uint16_t)buf[1])<<8));
        else sprintf(opstr, "%u", (uint16_t)buf[0]|(((uint16_t)buf[1])<<8));*/
        sprintf(dstr, "DB\t%s", opstr);
        return 2;
      }
      if (!urasm_disasm_decimal) sprintf(opstr, "#%02X", mem[0]); else sprintf(opstr, "%u", mem[0]);
      sprintf(dstr, "DB\t%s", opstr);
      return 1;
    }
    /* skip prefixes, determine command length */
    f = URASM_COMMANDS[opn].mask; c = URASM_COMMANDS[opn].code;
    for (bpos = 0; ; ++bpos) {
      uint8_t b;
      if ((f&0xFFUL) != 0xFFUL) break;
      b = c&0xFFUL;
      if (b != 0xFDU && b != 0xDDU && b != 0xEDU && b != 0xCBU) break;
      f >>= 8; c >>= 8; ++res;
    }
    /* is this CB-prefixed? */
    if (((URASM_COMMANDS[opn].code&0xFFFFUL) == 0xCBDDUL) ||
        ((URASM_COMMANDS[opn].code&0xFFFFUL) == 0xCBFDUL)) f >>= 8;
    while (f != 0) { f >>= 8; ++res; }
    /* copy mnemonics */
    strcpy(dstr, URASM_TOKENS[URASM_COMMANDS[opn].mnemo]);
    /* are there any operands? */
    if (URASM_COMMANDS[opn].ops[0] == UO_NONE) return res;
    /* is this CB-prefixed? */
    if (((URASM_COMMANDS[opn].code&0xFFFFUL) == 0xCBDDUL) ||
        ((URASM_COMMANDS[opn].code&0xFFFFUL) == 0xCBFDUL)) ++bpos; /* skip displacement */
    else {
      if ((URASM_COMMANDS[opn].ops[0] == UO_MIX || URASM_COMMANDS[opn].ops[0] == UO_MIY) &&
          URASM_COMMANDS[opn].ops[1] == UO_IMM8 &&
          (URASM_COMMANDS[opn].ops[2] == UO_NONE || URASM_COMMANDS[opn].ops[2] == UO_NEXT)) ++bpos; /* skip displacement */
    }
    opc = mem[bpos++];
    nextW = (uint16_t)mem[bpos] | (((uint16_t)mem[bpos+1])<<8);
    /* do operands */
    for (f = 0; f <= 3; ++f) {
      if (f == 3) {
        //printf("OPN=%d\n", opn);
        return res;
      }
      op = URASM_COMMANDS[opn].ops[f];
      if (op == UO_NONE || op == UO_NEXT) {
        //printf("OPN=%d\n", opn);
        return res;
      }
      /* check for valid operand */
      if (is_opc_invalid_op(op, opc)) break;
      /* command with displacement? */
      if (op == UO_MIX || op == UO_MIY) ++res;
      /* command has immediate operand? */
      if (op == UO_IMM8 || op == UO_ADDR8 || op == UO_PORTIMM) ++res;
      if (op == UO_IMM16 || op == UO_IMM16BE || op == UO_ADDR16 || op == UO_MEM16) res += 2;
      /* add delimiter */
      strcat(dstr, (f == 0 ? urasm_disasm_mnemo_delimiter : urasm_disasm_operand_delimiter));
      /* decode operand */
      ua_op2str(opstr, op, addr, opc, nextW, idx);
      strcat(dstr, opstr);
    }
  }
  return -1;
}


int urasm_disasm_opdisasm (char *dstr, uint16_t addr) {
  uint8_t mem[8];
  if (!urasm_getbyte) return -1;
  for (unsigned f = 0; f < 8; ++f) mem[f] = urasm_getbyte((uint16_t)((addr+f)&0xffffu));
  return urasm_disasm_opdisasm_ex(dstr, addr, mem);
}


///////////////////////////////////////////////////////////////////////////////
// assembler

/* returns 0 if label name conflicts with reserved word */
/* (or if the label is just a bad one) */
int urasm_is_valid_name (const char *lbl) {
  int f, hasNA = 0;
  char ch, buf[6];
  if (!lbl || !lbl[0]) return 0;
  if (lbl[0] == '@') {
    ch = lbl[1];
  } else {
    ch = lbl[0];
  }
  if (ch >= '0' && ch <= '9') return 0;
  if (!ur_isalpha(ch) && ch != '.' && ch != '_' && ch != '@') return 0;
  for (f = 1; lbl[f]; ++f) {
    ch = lbl[f];
    if (ch >= '0' && ch <= '9') continue;
    if (!ur_isalpha(ch)) {
      hasNA = 1;
      if (ch != '$' && ch != '.' && ch != '_' && ch != '@') return 0;
    }
  }
  if (hasNA || f > 4) return 1;
  for (f = 0; lbl[f]; ++f) buf[f] = ur_toupper(lbl[f]); buf[f] = '\0';
  for (f = 0; f < (!urasm_allow_zxnext ? URASM_MAX_NORMAL_TOKEN : URASM_MAX_TOKEN); ++f) if (!strcmp(buf, URASM_TOKENS[f])) return 0;
  for (f = 0; f < 8; ++f) if (!strcmp(buf, URA_REGS8[f])) return 0;
  for (f = 0; f < 4; ++f) if (!strcmp(buf, URA_REGS16[f])) return 0;
  for (f = 0; f < 4; ++f) if (!strcmp(buf, URA_REGS16A[f])) return 0;
  for (f = 0; f < 8; ++f) if (!strcmp(buf, URA_COND[f])) return 0;
  if (!strcmp(buf, "AFX")) return 0;
  if (!strcmp(buf, "IXH")) return 0;
  if (!strcmp(buf, "IXL")) return 0;
  if (!strcmp(buf, "IYH")) return 0;
  if (!strcmp(buf, "IYL")) return 0;
  if (!strcmp(buf, "IX")) return 0;
  if (!strcmp(buf, "IY")) return 0;
  if (!strcmp(buf, "XH")) return 0;
  if (!strcmp(buf, "HX")) return 0;
  if (!strcmp(buf, "XL")) return 0;
  if (!strcmp(buf, "LX")) return 0;
  if (!strcmp(buf, "YH")) return 0;
  if (!strcmp(buf, "HY")) return 0;
  if (!strcmp(buf, "YL")) return 0;
  if (!strcmp(buf, "LY")) return 0;
  if (!strcmp(buf, "R")) return 0;
  if (!strcmp(buf, "I")) return 0;
  return 1;
}


///////////////////////////////////////////////////////////////////////////////
// scanner / parser

// parse and calculate expression
static inline const char *skip_blanks (const char *expr) {
  while (*expr && ur_isspace(*expr)) ++expr;
  return expr;
}


///////////////////////////////////////////////////////////////////////////////
// expression parser
urasm_getval_fn urasm_getval = NULL;
urasm_expand_fn urasm_expand = NULL;


typedef struct ur_function_s {
  struct ur_function_s *next;
  char *name;
  urasm_func_fn fn;
} ur_function_t;

static ur_function_t *fnlist = NULL;

static void atexit_func_finalize (void) {
  while (fnlist != NULL) {
    ur_function_t *f = fnlist;
    fnlist = f->next;
    free(f->name);
    free(f);
  }
}


void urasm_expr_register_func (const char *name, urasm_func_fn fn) {
  if (name != NULL && name[0]) {
    static int atexitset = 0;
    ur_function_t *p, *c;
    if (!atexitset) { atexit(atexit_func_finalize); atexitset = 1; }
    for (p = NULL, c = fnlist; c != NULL; p = c, c = c->next) {
      if (strcasecmp(name, c->name) == 0) {
        /* replace or remove */
        if (fn == NULL) {
          /* remove */
          if (p == NULL) fnlist = c->next; else p->next = c->next;
          free(c->name);
          free(c);
        } else {
          /* replace */
          c->fn = fn;
        }
        return;
      }
    }
    if (fn != NULL) {
      /* add new */
      c = malloc(sizeof(*c));
      c->name = strdup(name);
      c->fn = fn;
      c->next = fnlist;
      fnlist = c;
    }
  }
}


urasm_func_fn urasm_expr_find_func (const char *name) {
  if (name != NULL && name[0]) {
    for (ur_function_t *c = fnlist; c != NULL; c = c->next) if (strcasecmp(name, c->name) == 0) return c->fn;
  }
  return NULL;
}


///////////////////////////////////////////////////////////////////////////////
typedef struct {
  const char *expr; /* can be changed */
  uint16_t addr; /* current address */
  int defined; /* !0: all used labels are defined */
  int error; /* !0: error */
  const char *errpos; /* will be set on error, has no meaning otherwise */
  jmp_buf errJP;
  int logic_done;
  int32_t logic_res;
} expr_info_t;


/* do math; op0 is the result */
typedef void (*expr_doit_fn) (urasm_exprval_t *op0, urasm_exprval_t *op1, expr_info_t *ei);

enum {
  optype_none,
  optype_unary,
  optype_math,
  optype_bitwise,
  optype_shift,
  optype_logic,
  optype_comparison,
};

typedef struct {
  char sn; /* short name or 0 */
  const char *ln; /* long name or NULL if `sn`!=0 */
  int prio; /* priority */
  expr_doit_fn doer;
  int type;
} expr_operator_t;


///////////////////////////////////////////////////////////////////////////////
// math ops
/* returns !0 is passed string is not NULL, and longer than 2 chars */
static inline __attribute__((always_inline)) __attribute__((pure))
int ur_is_long_str (const char *s) { return (s && s[0] && s[1] && s[2]); }

#define EERROR(code)  longjmp(ei->errJP, code)

#define CHECKVALNOTSTR(_v,_vkill)  do { \
  if ((_v)->str != NULL) { \
    if (ur_is_long_str((_v)->str)) { \
      if ((_vkill) != NULL) urasm_exprval_clear((_vkill)); \
      EERROR(UR_EXPRERR_TYPE); \
    } \
    free((_v)->str); \
    (_v)->str = NULL; \
  } \
} while (0)

#define CHECKARGSNOTSTR  do { \
  CHECKVALNOTSTR(op0, op1); \
  CHECKVALNOTSTR(op1, op1); \
} while (0)


#define PROPAGATE_FIXUP  do { \
  if (op0->fixuptype == UR_FIXUP_NONE) { \
    op0->fixuptype = op1->fixuptype; \
  } else if (op0->fixuptype == UR_FIXUP_WORD) { \
    if (op1->fixuptype != UR_FIXUP_NONE) op0->fixuptype = op1->fixuptype; \
  } else if (op1->fixuptype != UR_FIXUP_NONE && op0->fixuptype != op1->fixuptype) { \
    urasm_exprval_clear(op1);\
    urasm_exprval_clear(op0); \
    EERROR(UR_EXPRERR_FIXUP); \
  } \
} while (0)


static void mdo_bitnot (urasm_exprval_t *op0, urasm_exprval_t *op1, expr_info_t *ei) {
  op0->val = ~op0->val;
  op0->fixuptype = UR_FIXUP_NONE;
}
static void mdo_lognot (urasm_exprval_t *op0, urasm_exprval_t *op1, expr_info_t *ei) {
  op0->val = !op0->val;
  op0->fixuptype = UR_FIXUP_NONE;
}
static void mdo_uminus (urasm_exprval_t *op0, urasm_exprval_t *op1, expr_info_t *ei) {
  op0->val = -op0->val;
  op0->fixuptype = UR_FIXUP_NONE;
}
static void mdo_uplus (urasm_exprval_t *op0, urasm_exprval_t *op1, expr_info_t *ei) {
}
//
static void mdo_bitand (urasm_exprval_t *op0, urasm_exprval_t *op1, expr_info_t *ei) {
  op0->val &= op1->val;
  if (op1->val == 0) op0->fixuptype = UR_FIXUP_NONE;
  else if (op0->fixuptype != UR_FIXUP_NONE) {
    switch (op1->val) {
      case 0x00ff: op0->fixuptype = (op0->fixuptype == UR_FIXUP_WORD ? UR_FIXUP_LOBYTE : UR_FIXUP_NONE); break;
      case 0xff00: op0->fixuptype = (op0->fixuptype == UR_FIXUP_WORD ? UR_FIXUP_HIBYTE : UR_FIXUP_NONE); break;
    }
  } else {
    PROPAGATE_FIXUP;
  }
}
static void mdo_bitor (urasm_exprval_t *op0, urasm_exprval_t *op1, expr_info_t *ei) { op0->val |= op1->val; op0->fixuptype = UR_FIXUP_NONE; }
static void mdo_bitxor (urasm_exprval_t *op0, urasm_exprval_t *op1, expr_info_t *ei) { op0->val ^= op1->val; op0->fixuptype = UR_FIXUP_NONE; }
static void mdo_shl (urasm_exprval_t *op0, urasm_exprval_t *op1, expr_info_t *ei) { op0->val <<= op1->val; op0->fixuptype = UR_FIXUP_NONE; }
static void mdo_shr (urasm_exprval_t *op0, urasm_exprval_t *op1, expr_info_t *ei) { op0->val >>= op1->val; }
static void mdo_shru (urasm_exprval_t *op0, urasm_exprval_t *op1, expr_info_t *ei) { *(uint32_t *)&op0->val >>= op1->val; }
static void mdo_mul (urasm_exprval_t *op0, urasm_exprval_t *op1, expr_info_t *ei) { op0->val *= op1->val; op0->fixuptype = UR_FIXUP_NONE; }
static void mdo_div (urasm_exprval_t *op0, urasm_exprval_t *op1, expr_info_t *ei) { if (op1->val == 0) EERROR(UR_EXPRERR_DIV0); op0->val /= op1->val; op0->fixuptype = UR_FIXUP_NONE; }
static void mdo_mod (urasm_exprval_t *op0, urasm_exprval_t *op1, expr_info_t *ei) { if (op1->val == 0) EERROR(UR_EXPRERR_DIV0); op0->val %= op1->val; op0->fixuptype = UR_FIXUP_NONE; }
static void mdo_add (urasm_exprval_t *op0, urasm_exprval_t *op1, expr_info_t *ei) { op0->val += op1->val; PROPAGATE_FIXUP; }
static void mdo_sub (urasm_exprval_t *op0, urasm_exprval_t *op1, expr_info_t *ei) { op0->val -= op1->val; PROPAGATE_FIXUP; }

static int mdo_compare (urasm_exprval_t *op0, urasm_exprval_t *op1, expr_info_t *ei) {
  const int isnum0 = !ur_is_long_str(op0->str);
  const int isnum1 = !ur_is_long_str(op1->str);
  //fprintf(stderr, "mdo_compare: num0=%d; num1=%d (%s) (%s)\n", isnum0, isnum1, op0->str, op1->str);
  if (isnum0 != isnum1) {
    if (!op0->str || !op1->str) EERROR(UR_EXPRERR_TYPES);
    return strcmp(op0->str, op1->str);
  }
  if (isnum0) {
    return
      op0->val < op1->val ? -1 :
      op0->val > op1->val ? 1 :
      0;
  } else {
    return strcmp(op0->str, op1->str);
  }
}

static void mdo_set_int (urasm_exprval_t *op0, const int32_t v) {
  urasm_exprval_clear(op0);
  op0->val = v;
}

static void mdo_cmp_ls (urasm_exprval_t *op0, urasm_exprval_t *op1, expr_info_t *ei) { mdo_set_int(op0, (mdo_compare(op0, op1, ei) < 0)); }
static void mdo_cmp_gt (urasm_exprval_t *op0, urasm_exprval_t *op1, expr_info_t *ei) { mdo_set_int(op0, (mdo_compare(op0, op1, ei) > 0)); }
static void mdo_cmp_eq (urasm_exprval_t *op0, urasm_exprval_t *op1, expr_info_t *ei) { mdo_set_int(op0, (mdo_compare(op0, op1, ei) == 0)); }
static void mdo_cmp_ne (urasm_exprval_t *op0, urasm_exprval_t *op1, expr_info_t *ei) { mdo_set_int(op0, (mdo_compare(op0, op1, ei) != 0)); }
static void mdo_cmp_le (urasm_exprval_t *op0, urasm_exprval_t *op1, expr_info_t *ei) { mdo_set_int(op0, (mdo_compare(op0, op1, ei) <= 0)); }
static void mdo_cmp_ge (urasm_exprval_t *op0, urasm_exprval_t *op1, expr_info_t *ei) { mdo_set_int(op0, (mdo_compare(op0, op1, ei) >= 0)); }

static void mdo_log_and (urasm_exprval_t *op0, urasm_exprval_t *op1, expr_info_t *ei) {
  ei->logic_res = op0->val = (op0->val && op1->val);
  if (!op0->val) ei->logic_done = 1;
  op0->fixuptype = UR_FIXUP_NONE;
}

static void mdo_log_or (urasm_exprval_t *op0, urasm_exprval_t *op1, expr_info_t *ei) {
  ei->logic_res = op0->val = (op0->val || op1->val);
  if (op0->val) ei->logic_done = 1;
  op0->fixuptype = UR_FIXUP_NONE;
}


// priority level 1 -- opertiors line "." and "[]", function calls
// priority level 2 -- unary opertiors like "!" and "~"
// short forms must be put before long
// priorities must be sorted
static const expr_operator_t operators_old[] = {
  // unaries
  { '~', NULL, 2, mdo_bitnot, optype_unary },
  { '!', NULL, 2, mdo_lognot, optype_unary },
  { '-', NULL, 2, mdo_uminus, optype_unary },
  { '+', NULL, 2, mdo_uplus, optype_unary },
  // shifts
  { 0, "<<", 3, mdo_shl, optype_shift },
  { 0, ">>", 3, mdo_shr, optype_shift },
  { 0, ">>>", 3, mdo_shru, optype_shift },
  // bitwise and
  { '&', NULL, 4, mdo_bitand, optype_bitwise },
  // bitwise or and xor
  { '|', NULL, 5, mdo_bitor, optype_bitwise },
  { '^', NULL, 5, mdo_bitxor, optype_bitwise },
  // mul/div/mod
  { '*', NULL, 6, mdo_mul, optype_math },
  { '/', NULL, 6, mdo_div, optype_math },
  { '%', NULL, 6, mdo_mod, optype_math },
  // add/subtract
  { '+', NULL, 7, mdo_add, optype_math },
  { '-', NULL, 7, mdo_sub, optype_math },
  // logic and
  { 0, "&&", 8, mdo_log_and, optype_logic },
  // logic or
  { 0, "||", 9, mdo_log_or, optype_logic },
  // comparisons
  { '<', NULL, 10, mdo_cmp_ls, optype_comparison },
  { '>', NULL, 10, mdo_cmp_gt, optype_comparison },
  { '=', NULL, 10, mdo_cmp_eq, optype_comparison },
  { 0,   "==", 10, mdo_cmp_eq, optype_comparison },
  { 0,   "!=", 10, mdo_cmp_ne, optype_comparison },
  { 0,   "<>", 10, mdo_cmp_ne, optype_comparison },
  { 0,   "<=", 10, mdo_cmp_le, optype_comparison },
  { 0,   ">=", 10, mdo_cmp_ge, optype_comparison },
  // no more
  { 0, NULL, -1, NULL, optype_none },
};
// new C-like priorities
static const expr_operator_t operators_new[] = {
  // unaries
  { '~', NULL, 2, mdo_bitnot, optype_unary },
  { '!', NULL, 2, mdo_lognot, optype_unary },
  { '-', NULL, 2, mdo_uminus, optype_unary },
  { '+', NULL, 2, mdo_uplus, optype_unary },
  // shifts
  { 0, "<<", 3, mdo_shl, optype_shift },
  { 0, ">>", 3, mdo_shr, optype_shift },
  { 0, ">>>", 3, mdo_shru, optype_shift },
  // bitwise and
  { '&', NULL, 4, mdo_bitand, optype_bitwise },
  // bitwise or and xor
  { '|', NULL, 5, mdo_bitor, optype_bitwise },
  { '^', NULL, 5, mdo_bitxor, optype_bitwise },
  // mul/div/mod
  { '*', NULL, 6, mdo_mul, optype_math },
  { '/', NULL, 6, mdo_div, optype_math },
  { '%', NULL, 6, mdo_mod, optype_math },
  // add/subtract
  { '+', NULL, 7, mdo_add, optype_math },
  { '-', NULL, 7, mdo_sub, optype_math },
  // comparisons
  { '<', NULL, 8, mdo_cmp_ls, optype_comparison },
  { '>', NULL, 8, mdo_cmp_gt, optype_comparison },
  { '=', NULL, 8, mdo_cmp_eq, optype_comparison },
  { 0,   "==", 8, mdo_cmp_eq, optype_comparison },
  { 0,   "!=", 8, mdo_cmp_ne, optype_comparison },
  { 0,   "<>", 8, mdo_cmp_ne, optype_comparison },
  { 0,   "<=", 8, mdo_cmp_le, optype_comparison },
  { 0,   ">=", 8, mdo_cmp_ge, optype_comparison },
  // logic and
  { 0, "&&", 9, mdo_log_and, optype_logic },
  // logic or
  { 0, "||", 10, mdo_log_or, optype_logic },
  // no more
  { 0, NULL, -1, NULL, optype_none },
};
#define UNARY_PRIO    (2)
/*
#define MAX_PRIO      (10)
#define PRIO_LAND     (8)
#define PRIO_LOR      (9)
*/
static int max_prio_old = 0;
static int prio_land_old = 0;
static int prio_lor_old = 0;

static int max_prio = 0;
static int prio_land = 0;
static int prio_lor = 0;

int urasm_use_old_priorities = 0;
int urasm_allow_hex_castrates = 0;

static inline __attribute__((always_inline)) int get_max_prio () { return (!urasm_use_old_priorities ? max_prio : max_prio_old); }
static inline __attribute__((always_inline)) int get_land_prio () { return (!urasm_use_old_priorities ? prio_land : prio_land_old); }
static inline __attribute__((always_inline)) int get_lor_prio () { return (!urasm_use_old_priorities ? prio_lor : prio_lor_old); }

static __attribute((constructor)) void init_priorities (void) {
  if (max_prio_old) return;
  // old priorities
  for (const expr_operator_t *op = operators_old; op->prio >= 0; ++op) {
    if (max_prio_old < op->prio) max_prio_old = op->prio;
         if (op->doer == &mdo_log_and) prio_land_old = op->prio;
    else if (op->doer == &mdo_log_or) prio_lor_old = op->prio;
  }
  // new priorities
  for (const expr_operator_t *op = operators_new; op->prio >= 0; ++op) {
    if (max_prio < op->prio) max_prio = op->prio;
         if (op->doer == &mdo_log_and) prio_land = op->prio;
    else if (op->doer == &mdo_log_or) prio_lor = op->prio;
  }
}


/******************************************************************************/
/* expression parser engine                                                   */
/******************************************************************************/
static void expression (urasm_exprval_t *res, expr_info_t *ei);


#define SKIP_BLANKS   { ei->errpos = ei->expr = skip_blanks(ei->expr); }

#define PSEMITCHAR(_ch)  do { \
  if (!doinglen) res->str[len++] = (_ch); else ++len; \
} while (0)

// qch skipped
static const char *parse_string (urasm_exprval_t *res, char qch, const char *expr, int *error, int *lenp) {
  int doinglen = 2;
  if (lenp) *lenp = 0;
  if (*error) return expr;
  if (!expr[0]) { *error = 1; return expr; }
  //memset(buf, 0, sizeof(buf));
  for (;;) {
    int len = 0, n, f, base;
    const char *a = expr;
    --doinglen;
    for (; *a; ++a) {
      if (*a == '\\') {
        if (!a[1]) break;
        switch (*(++a)) {
          case 'a': PSEMITCHAR('\a'); break;
          case 'b': PSEMITCHAR('\b'); break;
          case 'e': PSEMITCHAR('\x1b'); break;
          case 'f': PSEMITCHAR('\f'); break;
          case 'n': PSEMITCHAR('\n'); break;
          case 'r': PSEMITCHAR('\r'); break;
          case 't': PSEMITCHAR('\t'); break;
          case 'v': PSEMITCHAR('\v'); break;
          case 'z': PSEMITCHAR('\0'); break;
          case 'x': case 'X': /* hex */
            ++a; /* skip 'x' */
            base = 16; f = 2;
donum:      for (n = 0; f > 0; --f) {
              char ch = dig2n(*a++, base);
              if (ch < 0) { --a; break; }
              n *= base;
              n += ch;
            }
            PSEMITCHAR(n);
            --a; /* return to the last digit, 'for' will skip it */
            break;
          case '0': /* octal */
            base = 8; f = 4;
            goto donum;
          case '1' ... '9': /* decimal */
            base = 10; f = 3;
            goto donum;
          default: PSEMITCHAR(a[0]); break; /* others */
        }
      } else {
        if (*a == qch) { ++a; break; }
        PSEMITCHAR(*a);
      }
    }
    if (doinglen) {
      res->str = calloc(len+1, 1);
      if (lenp) *lenp = len;
    } else {
      return a;
    }
  }
}


static int try_suffix_base (const char *s, char sfx, int base) {
  if (!s || !s[0]) return 0;
  while (*s == '_') ++s; /* just in case */
  if (dig2n(*s++, base) < 0) return 0;
  /* we don't need to check for zero here, because `dig2n()` will do this for us */
  for (;;) {
    const char ch = *s++;
    if (ch != '_' && dig2n(ch, base) < 0) {
      return (ch && ur_toupper(ch) == sfx && dig2n(*s, 16) < 0);
    }
  }
}


/*TODO: make suffix detector one-pass */
static int check_number_base_sfx (const char *s) {
  if (!s || !s[0]) return 0;
  while (*s == '_') ++s; /* just in case */
  /* check suffixes (suffix char must be passed as uppercase) */
  if (try_suffix_base(s, 'H', 16)) return 16;
  if (try_suffix_base(s, 'B', 2)) return 2;
  if (try_suffix_base(s, 'O', 8)) return 8;
  if (try_suffix_base(s, 'D', 10)) return 10;
  return 0;
}


static int check_number_base_char (char ch) {
  switch (ur_toupper(ch)) {
    case 'X': case 'H': return 16;
    case 'B': return 2;
    case 'O': return 8;
    case 'D': return 10;
    default: break;
  }
  return 0;
}


static const char *parse_number (urasm_exprval_t *res, const char *expr, int *error) {
  int prefixed = 1;
  if (*error) return expr;
  if (!expr[0]) { *error = 1; return expr; }
  const char *estart = expr;
  /* detect suffix first */
  int base = check_number_base_sfx(expr);
  if (base) {
    /* the suffix is ok */
    prefixed = 0; /* note that it is suffixed number, not prefixed */
  } else {
    /* no good suffix, check prefixes */
    switch (*expr) {
      case '0': /* this allows "0habcd" as hex number too; why not? */
        base = check_number_base_char(expr[1]);
        if (base) expr += 2; else base = 8;
        break;
      case '%': base = 2; ++expr; break;
      case '#': case '$': base = 16; ++expr; break;
      case '1' ... '9': base = 10; break;
      case '&':
        base = check_number_base_char(expr[1]);
        // check for ambiguity
        if (urasm_allow_hex_castrates && base == 2) {
          // '&b10' can be either binary, or hex; parse it as hex if castrates are allowed
          base = 16;
        }
        if (base) { expr += 2; break; }
        // some assemblers are using lone '&' for hex numbers, let's support it too
        // note that there is an ambiguity: '&b10' can be either binary, or hex
        // currently, it will be parsed as binary
        if (urasm_allow_hex_castrates && dig2n(expr[1], 16) >= 0) { base = 16; ++expr; break; }
        /* fallthru */
      default: *error = 1; return estart;
    }
  }
  /* parse number (ignore underscores) */
  while (*expr && *expr == '_') ++expr;
  if (dig2n(*expr, base) < 0) { *error = 1; return estart; }
  int n = 0;
  for (;;) {
    char ch = *expr++;
    if (ch == '_') continue;
    int d = dig2n(ch, base);
    if (d < 0) { res->val = n; return expr-prefixed; } /* `prefixed` set to 1 if there's no suffix */
    n *= base;
    /* this check is UB in standards-compliant C, because standard was created by morons */
    if (n < 0) { *error = 1; return estart; }
    n += d;
  }
}


static void get_addr (const char *lbl, urasm_exprval_t *res, expr_info_t *ei) {
  int defined = 0, found = 0;
  int32_t val = 0;
  if (urasm_label_by_name == NULL) EERROR(UR_EXPRERR_LABEL);
  res->fixuptype = UR_FIXUP_NONE;
  val = urasm_label_by_name(lbl, ei->addr, &defined, &found, &res->fixuptype);
  if (!found) EERROR(UR_EXPRERR_LABEL);
  if (!defined) ei->defined = 0;
  res->val = val;
}


// !0: invalid label
static int read_label_name (char *buf, expr_info_t *ei) {
  int pos = 0;
  for (;;) {
    if (pos >= 128) return -3;
    char ch = ei->expr[0];
    if (!ch) break;
    if (ur_isalnum(ch) || ch == '$' || ch == '.' || ch == '_' || ch == '@') {
      buf[pos++] = ch;
      ++(ei->expr);
    } else {
      break;
    }
  }
  if (pos < 1) return -2;
  buf[pos] = '\0';
  if (!urasm_is_valid_name(buf)) return -1;
  return 0;
}


static void term (urasm_exprval_t *res, expr_info_t *ei) {
  char ch, lbl[130];
  int tmp = 0, len;
  if (res->str != NULL) { free(res->str); res->str = NULL; }
  SKIP_BLANKS
  ch = (res->pos = ei->errpos = ei->expr)[0];
  if (!ch) EERROR(UR_EXPRERR_EOS);
  switch (ch) {
    case '[': case '(':
      ++(ei->expr);
      ch = (ch == '[' ? ']' : ')');
      expression(res, ei);
      if (ei->expr[0] != ch) EERROR(UR_EXPRERR_PARENS);
      ++(ei->expr);
      break;
    case '0' ... '9': case '#': case '%':
donumber:
      ei->expr = parse_number(res, ei->expr, &tmp);
      if (tmp) EERROR(UR_EXPRERR_NUMBER);
      break;
    case '$':
      if (dig2n(ei->expr[1], 16) >= 0) goto donumber;
      res->val = ei->addr;
      res->fixuptype = UR_FIXUP_WORD;
      ++(ei->expr);
      break;
    case '&':
      if (ur_toupper(ei->expr[1]) == 'H' && dig2n(ei->expr[2], 16) >= 0) goto donumber;
      if (ur_toupper(ei->expr[1]) == 'X' && dig2n(ei->expr[2], 16) >= 0) goto donumber;
      if (ur_toupper(ei->expr[1]) == 'O' && dig2n(ei->expr[2], 8) >= 0) goto donumber;
      if (ur_toupper(ei->expr[1]) == 'B' && dig2n(ei->expr[2], 2) >= 0) goto donumber;
      if (ur_toupper(ei->expr[1]) == 'D' && dig2n(ei->expr[2], 10) >= 0) goto donumber;
      // some assemblers are using lone '&' for hex numbers, let's support it too
      if (urasm_allow_hex_castrates && dig2n(ei->expr[1], 16) >= 0) goto donumber;
      goto dodefault;
    case '"': /* char or 2 chars */
    case '\'': /* char or 2 reversed chars */
      res->val = 0;
      ei->expr = parse_string(res, ch, ei->expr+1, &tmp, &len);
      if (tmp) EERROR(UR_EXPRERR_STRING);
      if (len == 1) {
        res->val = (unsigned char)res->str[0];
      } else if (len >= 2) {
        res->val = ((unsigned char)res->str[0])<<(ch == '"' ? 0 : 8);
        res->val |= ((unsigned char)res->str[1])<<(ch == '"' ? 8 : 0);
      }
      break;
    case ';':
    case ':':
      EERROR(UR_EXPRERR_TERM);
    case ')':
    case ']':
      return;
    case '=':
    case '*':
      if (urasm_getval != NULL) {
        ei->expr = urasm_getval(res, ei->expr, ei->addr, ei->logic_done, &ei->defined, &ei->error);
        if (ei->error != UR_EXPRERR_NONE) EERROR(ei->error);
      } else {
        EERROR(UR_EXPRERR_MARG);
      }
      break;
    default:
dodefault:
      if (read_label_name(lbl, ei)) EERROR(UR_EXPRERR_LABEL);
      SKIP_BLANKS
      if (ei->expr[0] == '(') {
        /* function call */
        urasm_func_fn fn = urasm_expr_find_func(lbl);
        const char *e;
        if (fn == NULL) EERROR(UR_EXPRERR_FUNC);
        ++(ei->expr); /* skip '(' */
        e = fn(res, ei->expr, ei->addr, ei->logic_done, &ei->defined, &ei->error);
        if (ei->error != UR_EXPRERR_NONE) EERROR(ei->error);
        else if (e == NULL) EERROR(UR_EXPRERR_FUNC);
        else ei->expr = e;
      } else {
        /* just a label */
        if (!ei->logic_done) get_addr(lbl, res, ei);
      }
      break;
  }
}


static const expr_operator_t *get_operator (int prio, expr_info_t *ei) {
  const char opc = ei->expr[0];
  int oplen = 1;
  const expr_operator_t *res = NULL;
  for (const expr_operator_t *op = (!urasm_use_old_priorities ? operators_new : operators_old); op->prio >= 0; ++op) {
    if (op->sn) {
      //fprintf(stderr, "get_operator: prio=%d; sn=%c; opc=%c\n", prio, op->sn, opc);
      if (op->prio == prio && oplen <= 1 && opc == op->sn) { res = op; oplen = 1; }
    } else {
      const int l = (int)strlen(op->ln);
      if (l > oplen && strncmp(ei->expr, op->ln, l) == 0) { res = op; oplen = l; }
    }
  }
  if (res && res->prio != prio) res = NULL;
  //if (res) fprintf(stderr, "res: prio=%d; sn='%c' \"%s\" (%d : %.*s)\n", prio, res->sn, res->ln, oplen, (unsigned)oplen, ei->expr);
  if (res) ei->expr += oplen; /* eat operator */
  return res;
}


#define CHECKNOTSTR  do { \
  if (ur_is_long_str(res->str) || ur_is_long_str(o1.str)) { \
    urasm_exprval_clear(&o1); \
    urasm_exprval_clear(res); \
    EERROR(UR_EXPRERR_TYPE); \
  } \
} while (0)


static void expr_do (int prio, urasm_exprval_t *res, expr_info_t *ei) {
  const expr_operator_t *op;
  urasm_exprval_t o1;
  SKIP_BLANKS
  if (ei->expr[0] == ')' || ei->expr[0] == ']') return;
  if (prio <= 0) { term(res, ei); return; }
  urasm_exprval_init(&o1);
  //fprintf(stderr, "expr_do: prio=%d <%s>\n", prio, ei->expr);
  if (prio == UNARY_PRIO) {
    //fprintf(stderr, "expr_do: prio=%d <%s>\n", prio, ei->expr);
    int wasIt = 0;
    for (;;) {
      SKIP_BLANKS
      ei->errpos = ei->expr;
      if (!(op = get_operator(prio, ei))) break;
      //fprintf(stderr, "UNARY: op=%c\n", op->sn);
      //expression(res, ei);
      if (op->sn == '-' || op->sn == '+') {
        expr_do(prio-1, res, ei); /* left-associative */
      } else {
        expr_do(prio, res, ei); /* right-associative */
      }
      if (!ei->logic_done) {
        CHECKNOTSTR;
        op->doer(res, &o1, ei);
      } else {
        res->fixuptype = UR_FIXUP_NONE;
        res->val = ei->logic_res;
      }
      wasIt = 1;
      if (op->sn == '-' || op->sn == '+') break;
    }
    if (!wasIt) expr_do(prio-1, res, ei); /* left-associative */
    return;
  }
  /* first operand */
  expr_do(prio-1, res, ei); /* first operand, left-associative */
  /* go on */
  for (;;) {
    SKIP_BLANKS
    ei->errpos = ei->expr;
    if (!ei->expr[0] || ei->expr[0] == ';' || ei->expr[0] == ':' || ei->expr[0] == ')' || ei->expr[0] == ']') break;
    //fprintf(stderr, "BINARY: prio=%d; expr=<%s>\n", prio, ei->expr);
    if (!(op = get_operator(prio, ei))) break;
    if (!ei->logic_done) {
      if (prio == get_land_prio()) {
        /* && */
        if (!res->val) { ei->logic_done = 1; ei->logic_res = 0; }
      } else if (prio == get_lor_prio()) {
        /* || */
        if (res->val) { ei->logic_done = 1; ei->logic_res = res->val; }
      }
    }
    //fprintf(stderr, "BINARY:000: prio=%d; expr=<%s>; op=%c(%s)\n", prio, ei->expr, op->sn, op->ln);
    expr_do(prio-1, &o1, ei); /* second operand, left-associative */
    //fprintf(stderr, "BINARY:001: prio=%d; expr=<%s>; op=%c(%s)\n", prio, ei->expr, op->sn, op->ln);
    if (!ei->logic_done) {
      if (op->type != optype_comparison) {
        CHECKNOTSTR;
      }
      //fprintf(stderr, "DOER:BEFORE: op=%c(%s); prio=%d; val=%d (%d : %d)\n", op->sn, op->ln, op->prio, res->val, ei->logic_done, ei->logic_res);
      op->doer(res, &o1, ei);
      //fprintf(stderr, "DOER:AFTER: val=%d (%d : %d)\n", res->val, ei->logic_done, ei->logic_res);
      urasm_exprval_clear(&o1);
    } else {
      res->fixuptype = UR_FIXUP_NONE;
      res->val = ei->logic_res;
    }
  }
}


static void expression (urasm_exprval_t *res, expr_info_t *ei) {
  SKIP_BLANKS
  ei->errpos = ei->expr;
  if (!ei->expr[0]) EERROR(UR_EXPRERR_EOS);
  expr_do(get_max_prio(), res, ei);
  SKIP_BLANKS
}


const char *urasm_expr_ex (urasm_exprval_t *res, const char *expr, uint16_t addr, int *donteval, int *defined, int *error) {
  expr_info_t ei;
  int jr;
  urasm_exprval_clear(res);
  if (error) *error = 0;
  if (!expr) { if (error) *error = UR_EXPRERR_EOS; return NULL; }
  if (res->str != NULL) { free(res->str); res->str = NULL; }
  ei.expr = expr;
  ei.addr = addr;
  ei.defined = (defined ? *defined : 1);
  ei.error = 0;
  ei.logic_done = (donteval ? *donteval : 0);
  ei.logic_res = 0;
  jr = setjmp(ei.errJP);
  if (jr) {
    urasm_exprval_clear(res);
    if (error) *error = ei.error;
    return ei.errpos;
  }
  expression(res, &ei);
  if (defined) *defined = ei.defined;
  if (donteval) *donteval = ei.logic_done;
  return ei.expr;
}


const char *urasm_expr (int32_t *res, const char *expr, uint16_t addr, int *defined, int *fixuptype, int *error) {
  urasm_exprval_t r;
  const char *ret;
  int err;
  if (defined) *defined = 1;
  if (fixuptype) *fixuptype = UR_FIXUP_NONE;
  urasm_exprval_init(&r);
  ret = urasm_expr_ex(&r, expr, addr, NULL, defined, &err);
  if (error) *error = err;
  if (!err) {
    if (fixuptype) *fixuptype = r.fixuptype;
    if (ur_is_long_str(r.str)) {
      if (error) *error = UR_EXPRERR_TYPE;
      ret = r.pos;
    } else if (res) {
      *res = r.val;
    }
  } else {
    if (res) *res = 0;
  }
  urasm_exprval_clear(&r);
  return ret;
}


void urasm_exprval_init (urasm_exprval_t *res) {
  if (res != NULL) {
    memset(res, 0, sizeof(*res));
    res->fixuptype = UR_FIXUP_NONE;
  }
}


void urasm_exprval_clear (urasm_exprval_t *res) {
  if (res != NULL) {
    if (res->str != NULL) {
      free(res->str);
      res->str = NULL;
    }
    res->val = 0;
    res->fixuptype = UR_FIXUP_NONE;
  }
}


void urasm_exprval_setint (urasm_exprval_t *res, int32_t v) {
  if (res->str) { free(res->str); res->str = NULL; }
  res->val = v;
}


/* sets val as in double quotes */
void urasm_exprval_tostr (urasm_exprval_t *res, const char *str) {
  if (!str) {
    urasm_exprval_setint(res, 0);
  } else {
    const size_t slen = strlen(str);
    res->str = realloc(res->str, slen+1);
    strcpy(res->str, str);
    if (slen == 1) {
      res->val = (unsigned char)res->str[0];
    } else {
      res->val = (unsigned char)res->str[0];
      res->val |= ((unsigned char)res->str[1])<<8;
    }
  }
}


/* sets val as in single quotes */
void urasm_exprval_tostr_rev (urasm_exprval_t *res, const char *str) {
  if (!str) {
    urasm_exprval_setint(res, 0);
  } else {
    const size_t slen = strlen(str);
    res->str = realloc(res->str, slen+1);
    strcpy(res->str, str);
    if (slen == 1) {
      res->val = (unsigned char)res->str[0];
    } else {
      res->val = ((unsigned char)res->str[0])<<8;
      res->val |= (unsigned char)res->str[1];
    }
  }
}


///////////////////////////////////////////////////////////////////////////////
// operand parser
//
const char *urasm_skip_operand (const char *expr, int *seenarg) {
  if (seenarg) *seenarg = 0;
  if (!expr) return NULL;
  expr = skip_blanks(expr);
  if (!expr[0] || expr[0] == ';') return NULL;
  if (expr[0] == ':' || expr[0] == ',') return expr; /* colon or comma, nothing to do here */
  if (seenarg) *seenarg = 1;
  char inQ = 0;
  int parens = 0;
  const char *oe = expr;
  for (; *expr; ++expr) {
    const char ch = *expr;
    if (inQ) {
           if (ch == inQ) inQ = 0;
      else if (ch == '\\' && expr[1]) ++expr;
    } else {
           if (ch == '"' || ch == '`') inQ = ch;
      else if (ch == '\'' && (expr == oe || !ur_isalnum(expr[-1]))) inQ = ch;
      else if (ch == '(') ++parens;
      else if (ch == ')') { if (--parens < 0) parens = 0; }
      else if (ch == ';') break;
      else if (parens == 0 && (ch == ',' || ch == ':')) break;
    }
  }
  return expr;
}


static int check_ixy (const char *str) {
  if (str[0] != 'I' || (str[1] != 'X' && str[1] != 'Y')) return 0;
  // it should end with non-id char
  return !(ur_isalnum(str[2]) || str[2] == '_');
}


/* error is `UR_EXPRERR_XXX` */
const char *urasm_next_operand (urasm_operand_t *op, const char *expr, uint16_t addr, int *error) {
  const char *oe;
  char opstr[256];
  char inQ = 0;
  int parens = 0;
  int f;
  *error = UR_EXPRERR_NONE;
  memset(op, 0, sizeof(urasm_operand_t));
  op->defined = 1;
  if (!expr) return NULL;
  expr = skip_blanks(expr);
  if (!expr[0] || expr[0] == ';') return NULL;
  if (expr[0] == ':') return expr;
  op->parsed = 1;
  op->fixuptype = UR_FIXUP_NONE;
  /* skip operand */
  for (oe = expr; *expr; ++expr) {
    const char ch = *expr;
    if (inQ) {
           if (ch == inQ) inQ = 0;
      else if (ch == '\\' && expr[1]) ++expr;
    } else {
           if (ch == '"' || ch == '`') inQ = ch;
      else if (ch == '\'' && (oe == expr || !ur_isalnum(expr[-1]))) inQ = ch;
      else if (ch == '(') ++parens;
      else if (ch == ')') { if (--parens < 0) parens = 0; }
      else if (ch == ';') break;
      else if (parens == 0 && (ch == ',' || ch == ':')) break;
    }
  }
  if (expr-oe > 255) { *error = UR_EXPRERR_TOOLONG; return oe; }
  memset(opstr, 0, sizeof(opstr));
  memcpy(opstr, oe, expr-oe);
  while (opstr[0] && ur_isspace(opstr[strlen(opstr)-1])) opstr[strlen(opstr)-1] = '\0';
  if (!opstr[0]) { *error = UR_EXPRERR_EOS; return oe; }
  /* determine operand type */
  if (opstr[0] == '=' && urasm_expand != NULL) {
    if (urasm_expand(opstr, (int)sizeof(opstr)-1) != 0) { *error = UR_EXPRERR_MARG; return oe; }
    //fprintf(stderr, "exp: [%s]\n", opstr);
  }
  if (ur_isalpha(opstr[0]) && urasm_is_valid_name(opstr)) {
    goto doexpression;
  }
  if (opstr[0] == '(') {
    /* memref */
    op->mem = 1; op->special = 1;
    if (!opstr[1] || opstr[strlen(opstr)-1] != ')') { *error = UR_EXPRERR_PARENS; return oe; }
    /* operand w/o "()" */
    opstr[strlen(opstr)-1] = '\0';
    memmove(opstr, opstr+1, sizeof(opstr)-1);
    /* trim spaces */
    while (opstr[0] && ur_isspace(opstr[0])) memmove(opstr, opstr+1, sizeof(opstr)-1);
    while (opstr[0] && ur_isspace(opstr[strlen(opstr)-1])) opstr[strlen(opstr)-1] = '\0';
    /* empty brackets? */
    if (!opstr[0]) { *error = UR_EXPRERR_TERM; return oe; }
    for (f = 0; opstr[f]; ++f) op->s[f] = ur_toupper(opstr[f]);
    if (!strcmp(op->s, "C")) {
      op->special = 1; op->mem = 0; strcpy(op->s, "(C)");
      return expr;
    }
    if (!strcmp(op->s, "HL")) return expr;
    if (!strcmp(op->s, "DE")) return expr;
    if (!strcmp(op->s, "BC")) return expr;
    if (!strcmp(op->s, "SP")) return expr;
    if (check_ixy(op->s)) {
      /* memref: IX or IY */
      op->ixy = (op->s[1] == 'X' ? 0xDDU : 0xFDU);
      op->v = 0;
      memmove(opstr, opstr+1, sizeof(opstr)-1);
      opstr[0] = '0';
      while (opstr[1] && ur_isspace(opstr[1])) memmove(opstr+1, opstr+2, sizeof(opstr)-2);
      if (!opstr[1]) return expr;
      if (opstr[1] != '+' && opstr[1] != '-') { *error = UR_EXPRERR_NUMBER; return oe; }
      /* opstr must be an expression */
      const char *t = urasm_expr(&op->v, opstr, addr, &op->defined, NULL, error);
      if (*error) return oe;
      if (t[0]) { *error = UR_EXPRERR_TOOLONG; return oe; }
      if (op->defined && (op->v < -128 || op->v > 127)) { *error = UR_EXPRERR_NUMBER; return oe; }
      return expr;
    }
    op->special = 0;
  } else {
    if (!strcasecmp(opstr, "af'")) goto registerop;
    for (f = 0; ur_isalpha(opstr[f]); ++f) {}
    if (!opstr[f]) {
registerop:
      for (f = 0; opstr[f]; ++f) opstr[f] = ur_toupper(opstr[f]);
      strcpy(op->s, opstr);
      op->special = 1;
      return skip_blanks(expr);
    }
  }
  /* this must be an expression */
doexpression:
  strcpy(op->s, opstr);
  {
    urasm_exprval_t e;
    const char *t;
    int donteval = 0;
    urasm_exprval_init(&e);
    t = urasm_expr_ex(&e, opstr, addr, &donteval, &op->defined, error);
    op->v = e.val;
    op->fixuptype = e.fixuptype;
    urasm_exprval_clear(&e);
    //const char *t = urasm_expr(&op->v, opstr, addr, &op->defined, error);
    if (*error) { op->parsed = 0; return oe; }
    if (t == NULL || t[0]) { op->parsed = 0; *error = UR_EXPRERR_EOS; return oe; }
  }
  return skip_blanks(expr);
}


///////////////////////////////////////////////////////////////////////////////
// assembler

// for regsters & conditions: op->v will be set to reg number
int urasm_is_valid_operand (urasm_operand_t *op, uint16_t addr, unsigned optype) {
  int i;
  if (optype == UO_NONE || optype == UO_NEXT) return (op->s[0] == '\0');
  if (!op->s[0]) return 0;
  switch (optype) {
    case UO_IMM8:
      if (op->special || op->mem) return 0;
      if (op->defined && (op->v < -128 || op->v > 255)) return 0;
      return 1;
    case UO_IMM16:
      if (op->special || op->mem) return 0;
      if (op->defined && (op->v < -32768 || op->v > 65535)) return 0;
      return 1;
    case UO_IMM16BE:
      if (op->special || op->mem) return 0;
      if (op->defined && (op->v < -32768 || op->v > 65535)) return 0;
      return 1;
    case UO_ADDR16:
      if (op->special || op->mem) return 0;
      if (op->defined && (op->v < -32768 || op->v > 65535)) return 0;
      if (op->v < 0) op->v = 65536+op->v;
      return 1;
    case UO_ADDR8:
      if (op->special || op->mem) return 0;
      if (op->defined && (op->v < -32768 || op->v > 65535)) return 0;
      if (!op->defined) op->v = addr;
      if (op->v < 0) op->v = 65536+op->v;
      i = op->v-((int)addr+2);
      if (i < -128 || i > 127) return 0;
      op->v = i&0xff;
      return 1;
    case UO_MEM16:
      if (op->special || !op->mem) return 0;
      if (op->defined && (op->v < -32768 || op->v > 65535)) return 0;
      if (op->v < 0) op->v = 65536+op->v;
      return 1;
    case UO_R8: case UO_2R8:
      if (op->mem && !strcmp(op->s, "HL")) { /*strcpy(op->s, "M");*/ op->v = 6; return 1; }
      /* fallthru */
    case UO_R8_NOM: case UO_2R8_NOM:
      if (!op->special || op->mem || op->s[1]) return 0;
      switch (op->s[0]) {
        case 'B': op->v = 0; break;
        case 'C': op->v = 1; break;
        case 'D': op->v = 2; break;
        case 'E': op->v = 3; break;
        case 'H': op->v = 4; break;
        case 'L': op->v = 5; break;
        case 'A': op->v = 7; break;
        default: return 0;
      }
      return 1;
    case UO_R8_NOM_NOHL: case UO_2R8_NOM_NOHL:
      if (!op->special || op->mem || op->s[1]) return 0;
      switch (op->s[0]) {
        case 'B': op->v = 0; break;
        case 'C': op->v = 1; break;
        case 'D': op->v = 2; break;
        case 'E': op->v = 3; break;
        case 'A': op->v = 7; break;
        default: return 0;
      }
      return 1;
    case UO_PORTC:
      if (!op->special || op->mem) return 0;
      return strcmp(op->s, "(C)")==0;
    case UO_PORTIMM: /* mem, 'cause (n) */
      if (op->special || !op->mem) return 0;
      op->mem = 0;
      if (op->defined && (op->v < 0 || op->v > 255)) return 0;
      return 1;
    case UO_R8_XH:
      if (!op->special || op->mem) return 0;
      if (!strcmp(op->s, "XH") || !strcmp(op->s, "HX") || !strcmp(op->s, "IXH")) return 1;
      return 0;
    case UO_R8_XL:
      if (!op->special || op->mem) return 0;
      if (!strcmp(op->s, "XL") || !strcmp(op->s, "LX") || !strcmp(op->s, "IXL")) return 1;
      return 0;
    case UO_R8_YH:
      if (!op->special || op->mem) return 0;
      if (!strcmp(op->s, "YH") || !strcmp(op->s, "HY") || !strcmp(op->s, "IYH")) return 1;
      return 0;
    case UO_R8_YL:
      if (!op->special || op->mem) return 0;
      if (!strcmp(op->s, "YL") || !strcmp(op->s, "LY") || !strcmp(op->s, "IYL")) return 1;
      return 0;
    case UO_R8_A:
      if (!op->special || op->mem) return 0;
      if (!strcmp(op->s, "A")) return 1;
      return 0;
    case UO_R8_B:
      if (!op->special || op->mem) return 0;
      if (!strcmp(op->s, "B")) return 1;
      return 0;
    case UO_R8_R:
      if (!op->special || op->mem) return 0;
      if (!strcmp(op->s, "R")) return 1;
      return 0;
    case UO_R8_I:
      if (!op->special || op->mem) return 0;
      if (!strcmp(op->s, "I")) return 1;
      return 0;
    case UO_R16:
      if (!op->special || op->mem || !op->s[1] || op->s[2]) return 0;
      if (!strcmp(op->s, "BC")) { op->v = 0; return 1; }
      if (!strcmp(op->s, "DE")) { op->v = 1; return 1; }
      if (!strcmp(op->s, "HL")) { op->v = 2; return 1; }
      if (!strcmp(op->s, "SP")) { op->v = 3; return 1; }
      return 0;
    case UO_R16A:
      if (!op->special || op->mem || !op->s[1] || op->s[2]) return 0;
      if (!strcmp(op->s, "BC")) { op->v = 0; return 1; }
      if (!strcmp(op->s, "DE")) { op->v = 1; return 1; }
      if (!strcmp(op->s, "HL")) { op->v = 2; return 1; }
      if (!strcmp(op->s, "AF")) { op->v = 3; return 1; }
      return 0;
    case UO_R16AF:
      if (!op->special || op->mem || strcmp(op->s, "AF")) return 0;
      op->v = 3;
      return 1;
    case UO_R16AFX:
      if (!op->special || op->mem || (strcmp(op->s, "AF'") && strcmp(op->s, "AFX"))) return 0;
      op->v = 3;
      return 1;
    case UO_R16BC:
      if (!op->special || op->mem || strcmp(op->s, "BC")) return 0;
      op->v = 0;
      return 1;
    case UO_R16DE:
      if (!op->special || op->mem || strcmp(op->s, "DE")) return 0;
      op->v = 1;
      return 1;
    case UO_R16HL:
      if (!op->special || op->mem || strcmp(op->s, "HL")) return 0;
      op->v = 2;
      return 1;
    case UO_R16IX:
      if (!op->special || op->mem || strcmp(op->s, "IX")) return 0;
      op->v = 2;
      return 1;
    case UO_R16IY:
      if (!op->special || op->mem || strcmp(op->s, "IY")) return 0;
      op->v = 2;
      return 1;
    case UO_R16SP:
      if (!op->special || op->mem || strcmp(op->s, "SP")) return 0;
      op->v = 3;
      return 1;
    case UO_MSP:
      if (!op->special || !op->mem || strcmp(op->s, "SP")) return 0;
      op->v = 3;
      return 1;
    case UO_MBC:
      if (!op->special || !op->mem || strcmp(op->s, "BC")) return 0;
      op->v = 0;
      return 1;
    case UO_MDE:
      if (!op->special || !op->mem || strcmp(op->s, "DE")) return 0;
      op->v = 1;
      return 1;
    case UO_MHL:
      if (!op->special || !op->mem || strcmp(op->s, "HL")) return 0;
      op->v = 2;
      return 1;
    case UO_MIX:
      if (!op->special || !op->mem || op->ixy != 0xDDU) return 0;
      if (op->defined && (op->v < -128 || op->v > 127)) return 0;
      return 1;
    case UO_MIY:
      if (!op->special || !op->mem || op->ixy != 0xFDU) return 0;
      if (op->defined && (op->v < -128 || op->v > 127)) return 0;
      return 1;
    case UO_MIX0:
      if (!op->special || !op->mem || op->ixy != 0xDDU) return 0;
      if (op->defined && op->v != 0) return 0;
      return 1;
    case UO_MIY0:
      if (!op->special || !op->mem || op->ixy != 0xFDU) return 0;
      if (op->defined && op->v != 0) return 0;
      return 1;
    case UO_JRCOND:
      if (!op->special || op->mem || ur_is_long_str(op->s)) return 0;
      if (!strcmp(op->s, "NZ")) { op->v = 0; return 1; }
      if (!strcmp(op->s, "Z")) { op->v = 1; return 1; }
      if (!strcmp(op->s, "NC")) { op->v = 2; return 1; }
      if (!strcmp(op->s, "C")) { op->v = 3; return 1; }
      return 0;
    case UO_COND:
      if (!op->special || op->mem || ur_is_long_str(op->s)) return 0;
      if (!strcmp(op->s, "NZ")) { op->v = 0; return 1; }
      if (!strcmp(op->s, "Z")) { op->v = 1; return 1; }
      if (!strcmp(op->s, "NC")) { op->v = 2; return 1; }
      if (!strcmp(op->s, "C")) { op->v = 3; return 1; }
      if (!strcmp(op->s, "PO")) { op->v = 4; return 1; }
      if (!strcmp(op->s, "PE")) { op->v = 5; return 1; }
      if (!strcmp(op->s, "P")) { op->v = 6; return 1; }
      if (!strcmp(op->s, "M")) { op->v = 7; return 1; }
      return 0;
    case UO_BITN:
      if (op->special || op->mem) return 0;
      if (op->defined && (op->v < 0 || op->v > 7)) return 0;
      return 1;
    case UO_RSTDEST:
      if (op->special || op->mem) return 0;
      if (op->defined && (op->v < 0 || op->v > 0x38 || (op->v&7))) return 0;
      op->v >>= 3;
      return 1;
    case UO_IM0:
      if (op->special || op->mem) return 0;
      if (op->defined && op->v != 0) return 0;
      return 1;
    case UO_IM1:
      if (op->special || op->mem) return 0;
      if (op->defined && op->v != 1) return 0;
      return 1;
    case UO_IM2:
      if (op->special || op->mem) return 0;
      if (op->defined && op->v != 2) return 0;
      return 1;
  }
  return 0;
}


// mem size should be at least 7 bytes
// returns size of the assembled code
// result <0 on error
// understands comments
int urasm_opasm (const char *expr, uint16_t destaddr, uint16_t addr, const char **errpos) {
  uint8_t buf[8];
  int len = 0;
  const char *oe;
  char mnem[16];
  int tkn;
  urasm_operand_t ops[3];
  const urasm_cmdinfo_t *cm;
  uint32_t code, mask;
  int opcPos; /* opcode will be placed here */
  int ret;

  int doOperand (int idx) {
    const urasm_operand_t *op = ops+idx;
    switch (cm->ops[idx]) {
      case UO_IMM8:
        if (op->fixuptype != UR_FIXUP_NONE && urasm_fixup_operand != NULL) {
          if (op->fixuptype == UR_FIXUP_WORD) return URA_EXPR_ERR-UR_EXPRERR_FIXUP;
          urasm_fixup_operand(op, (destaddr+len)&0xffff, (destaddr+len)&0xffff, op->fixuptype, 1);
        }
        /* fallthru */
      case UO_PORTIMM:
      case UO_ADDR8:
        buf[len++] = op->v&0xFFU;
        break;
      case UO_IMM16:
      case UO_ADDR16:
      case UO_MEM16:
        if (op->fixuptype != UR_FIXUP_NONE && urasm_fixup_operand != NULL) {
          urasm_fixup_operand(op, (destaddr+len)&0xffff, (destaddr+len)&0xffff, op->fixuptype, 2);
        }
        buf[len++] = op->v&0xFFU;
        buf[len++] = ((op->v&0xFFFFU)>>8)&0xFFU;
        break;
      case UO_IMM16BE:
        /*FIXME: this is prolly not right*/
        if (op->fixuptype != UR_FIXUP_NONE && urasm_fixup_operand != NULL) {
          switch (op->fixuptype) {
            case UR_FIXUP_WORD:
              urasm_fixup_operand(op, (destaddr+len)&0xffff, (destaddr+len)&0xffff, UR_FIXUP_HIBYTE, 1);
              urasm_fixup_operand(op, (destaddr+len+1)&0xffff, (destaddr+len+1)&0xffff, UR_FIXUP_LOBYTE, 1);
              break;
            case UR_FIXUP_LOBYTE:
              urasm_fixup_operand(op, (destaddr+len)&0xffff, (destaddr+len)&0xffff, UR_FIXUP_HIBYTE, 1);
              break;
            case UR_FIXUP_HIBYTE:
              urasm_fixup_operand(op, (destaddr+len+1)&0xffff, (destaddr+len+1)&0xffff, UR_FIXUP_LOBYTE, 1);
              break;
            default: break;
          }
        }
        buf[len++] = ((op->v&0xFFFFU)>>8)&0xFFU;
        buf[len++] = op->v&0xFFU;
        break;
      case UO_R8:
      case UO_R8_NOM:
      case UO_R8_NOM_NOHL:
        code |= op->v&0xFFU;
        break;
      case UO_JRCOND:
      case UO_COND:
      case UO_BITN:
      case UO_RSTDEST:
      case UO_2R8:
      case UO_2R8_NOM:
      case UO_2R8_NOM_NOHL:
        code |= (op->v&0xFFU)<<3;
        break;
      case UO_R16:
      case UO_R16A:
        code |= (op->v&0xFFU)<<4;
        break;
      case UO_MIX:
      case UO_MIY:
        buf[len++] = op->v;
        break;
    }
    return 0;
  }

  /* <0: error; 0: ok; >0: last operand (ops[opix].parsed != 0: non-empty one) */
  int parseOperand (int opidx) {
    int error = 0;
    const char *oe = expr;
    expr = urasm_next_operand(ops+opidx, expr, addr, &error);
    if (error) {
      if (errpos) *errpos = oe;
      //return URA_BAD_OPERAND;
      return -error+URA_EXPR_ERR;
    }
    if (!expr || !expr[0] || expr[0] == ';' || expr[0] == ':') return 1;
    if (*expr != ',') { if (errpos) *errpos = oe; return URA_BAD_OPERAND; }
    ++expr;
    return 0;
  }

  int genCode (void) {
    int operr = 0;
    /* special macro for ADD DE,HL */
    if (tkn == UT_ADD &&
        urasm_is_valid_operand(&ops[0], addr, UO_R16DE) &&
        urasm_is_valid_operand(&ops[1], addr, UO_R16HL) &&
        urasm_is_valid_operand(&ops[2], addr, UO_NONE))
    {
      // generate:
      //   EX  DE,HL
      //   ADD HL,DE
      //   EX  DE,HL
      buf[0] = 0xEBU;
      buf[1] = 0x19U;
      buf[2] = 0xEBU;
      for (unsigned n = 0; n < 3; ++n) urasm_putbyte(destaddr++, buf[n]);
      addr += 3;
      return 3;
    }
    /* hack for invalid instructions */
    for (int pos = URASM_MAX_COMMAND-1; pos >= 0; --pos) {
      int f;
      if (tkn != URASM_COMMANDS[pos].mnemo) continue;
      if (!urasm_allow_zxnext && URASM_COMMANDS[pos].ops[2] == UO_NEXT) continue;
      for (f = 0; f < 3; ++f) {
        if (!urasm_is_valid_operand(&ops[f], addr, URASM_COMMANDS[pos].ops[f])) break;
      }
      if (f < 3) continue;
      /* command found, generate code */
      len = 0; cm = URASM_COMMANDS+pos;
      code = cm->code; mask = cm->mask;
      if ((code&0xFFFFUL) == 0xCBDDUL || (code&0xFFFFUL) == 0xCBFDUL) {
        /* special commands */
        /* emit unmasked code */
        buf[0] = (code&0xFFU); buf[1] = 0xCBU;
        for (f = 0; f < 3; ++f) {
          if (cm->ops[f] == UO_MIX || cm->ops[f] == UO_MIY) {
                 if (ops[f].defined && (ops[f].v < -128 || ops[f].v > 127)) f = -1;
            else if (ops[f].defined) buf[2] = ops[f].v;
            break;
          }
        }
        if (f < 0) continue; /* not me */
        len = 4;
        code >>= 24; mask >>= 24;
        if ((mask&0xFFUL) != 0xFFUL) {
          for (f = 0; f < 3; ++f) if (cm->ops[f] != UO_MIX && cm->ops[f] != UO_MIY) {
            if ((operr = doOperand(f)) != 0) goto badi;
          }
        }
        buf[3] = code;
        /* that's all */
        for (f = 0; f < len; ++f) { urasm_putbyte(destaddr++, buf[f]); ++addr; }
        return len;
      } else {
        /* normal commands */
        /* emit unmasked code */
        while ((mask&0xFFUL) == 0xFFUL) {
          buf[len++] = code&0xFFUL;
          code >>= 8; mask >>= 8;
        }
        //ASSERT((code&0xFFFFFF00UL) == 0);
        if (mask == 0) {
          //ASSERT(len > 0);
          code = buf[--len];
        }
        opcPos = len++;
        if ((operr = doOperand(0)) != 0) goto badi;
        if ((operr = doOperand(1)) != 0) goto badi;
        if ((operr = doOperand(2)) != 0) goto badi;
        buf[opcPos] = code;
        /* that's all */
        for (f = 0; f < len; ++f) { urasm_putbyte(destaddr++, buf[f]); ++addr; }
        return len;
      }
    }
badi:
    if (errpos) *errpos = oe;
    return (operr < 0 ? operr : URA_BAD_INSTRUCTION);
  }

  int doPushPop (void) {
    int f, len;
    for (f = len = 0; ; ++f) {
      int doQuit = 0;
      const char *oe = expr;
      int res = parseOperand(0);
      if (res < 0) return res;
      if (res > 0) {
        if (f == 0 && !ops[0].parsed) { *errpos = oe; return URA_BAD_INSTRUCTION; }
        doQuit = 1;
      }
      code = (tkn == UT_POP ? 0xC1U : 0xC5U);
      if (urasm_is_valid_operand(ops, addr, UO_R16A)) {
        code |= (ops[0].v&0xFFU)<<4;
        urasm_putbyte(destaddr++, code);
        ++addr;
        ++len;
      } else if (urasm_is_valid_operand(ops, addr, UO_R16IX)) {
        code |= (ops[0].v&0xFFU)<<4;
        urasm_putbyte(destaddr++, 0xDDU);
        urasm_putbyte(destaddr++, code);
        addr += 2;
        len += 2;
      } else if (urasm_is_valid_operand(ops, addr, UO_R16IY)) {
        code |= (ops[0].v&0xFFU)<<4;
        urasm_putbyte(destaddr++, 0xFDU);
        urasm_putbyte(destaddr++, code);
        addr += 2;
        len += 2;
      } else if (urasm_allow_zxnext && code == 0xC5U && urasm_is_valid_operand(ops, addr, UO_IMM16BE)) {
        /* ZXNext PUSH nnnn */
        urasm_putbyte(destaddr++, 0xEDU);
        urasm_putbyte(destaddr++, 0x8AU);
        /*FIXME: this is prolly not right*/
        if (ops[0].fixuptype != UR_FIXUP_NONE && urasm_fixup_operand != NULL) {
          switch (ops[0].fixuptype) {
            case UR_FIXUP_WORD:
              urasm_fixup_operand(&ops[0], (destaddr+len)&0xffff, (destaddr+len)&0xffff, UR_FIXUP_HIBYTE, 1);
              urasm_fixup_operand(&ops[0], (destaddr+len+1)&0xffff, (destaddr+len+1)&0xffff, UR_FIXUP_LOBYTE, 1);
              break;
            case UR_FIXUP_LOBYTE:
              urasm_fixup_operand(&ops[0], (destaddr+len)&0xffff, (destaddr+len)&0xffff, UR_FIXUP_HIBYTE, 1);
              break;
            case UR_FIXUP_HIBYTE:
              urasm_fixup_operand(&ops[0], (destaddr+len+1)&0xffff, (destaddr+len+1)&0xffff, UR_FIXUP_LOBYTE, 1);
              break;
            default: break;
          }
        }
        urasm_putbyte(destaddr++, ((ops[0].v&0xFFFFU)>>8)&0xFFU);
        urasm_putbyte(destaddr++, ops[0].v&0xFFU);
        addr += 4;
        len += 4;
      } else {
        if (errpos) *errpos = oe;
        return URA_BAD_OPERAND;
      }
      if (doQuit) break;
    }
    if (expr) {
      expr = skip_blanks(expr);
      if (expr[0] && expr[0] != ';') {
        if (errpos) *errpos = expr;
        if (expr[0] != ':') return URA_EXTRA_TEXT;
      }
    }
    return len;
  }

  int dorep (int opcnt) {
    int f, len;
    memset(ops, 0, sizeof(ops));
    for (f = len = 0; ; ++f) {
      int doQuit = 0, res;
      const char *oe;
      for (int c = 0; c < opcnt; ++c) {
        oe = expr;
        res = parseOperand(c);
        if (res < 0) return res;
        if (res > 0) {
          if (c != opcnt-1 || !ops[c].parsed) { *errpos = oe; return URA_BAD_INSTRUCTION; }
          doQuit = 1;
        }
      }
      if ((res = genCode()) < 0) return res;
      addr += res;
      len += res;
      if (doQuit) break;
    }
    if (expr) {
      expr = skip_blanks(expr);
      if (expr[0] && expr[0] != ';') {
        if (errpos) *errpos = expr;
        if (expr[0] != ':') return URA_EXTRA_TEXT;
      }
    }
    return len;
  }

  if (errpos) *errpos = NULL;
  if (!urasm_putbyte || !expr) return URA_GENERIC;

  expr = skip_blanks(expr);
  if (!expr[0] || expr[0] == ';') return 0;
  if (expr[0] == ':') { if (errpos) *errpos = expr; return 0; }
  for (oe = expr; *expr && !ur_isspace(*expr) && *expr != ':' && *expr != ';'; ++expr) {}
  if (urasm_allow_zxnext) {
    if (expr-oe > 7) { if (errpos) *errpos = oe; return URA_BAD_MNEMO; } /* bad mnemonics */
  } else {
    if (expr-oe > 4) { if (errpos) *errpos = oe; return URA_BAD_MNEMO; } /* bad mnemonics */
  }

  memset(mnem, 0, sizeof(mnem));
  memcpy(mnem, oe, expr-oe);
  for (int f = 0; mnem[f]; ++f) mnem[f] = ur_toupper(mnem[f]);
  /* find it */
  for (tkn = 0; tkn < URASM_MAX_TOKEN; ++tkn) if (!strcmp(mnem, URASM_TOKENS[tkn])) break;
  if (tkn >= URASM_MAX_TOKEN) { if (errpos) *errpos = oe; return URA_BAD_MNEMO; } /* unknown mnemonics */

  expr = skip_blanks(expr);
  if (expr[0] == ',') { if (errpos) *errpos = oe; return URA_BAD_OPERAND; }
  /* special for PUSH and POP */
  if (tkn == UT_POP || tkn == UT_PUSH) return doPushPop();
  /* special for LD */
  if (tkn == UT_LD /*|| tkn == UT_ADD || tkn == UT_ADC || tkn == UT_SBC*/) return dorep(2);
  /* special for RR/RL */
  if (tkn == UT_RL || tkn == UT_RR || tkn == UT_INC || tkn == UT_DEC ||
      tkn == UT_SRL || tkn == UT_SRA || tkn == UT_SLI || tkn == UT_SLL || tkn == UT_SLA) return dorep(1);

  memset(ops, 0, sizeof(ops));
  for (int f = 0; f < 3; ++f) {
    int res = parseOperand(f);
    if (res > 0) break;
    if (res < 0) return res;
  }

  ret = genCode();

  if (ret >= 0 && expr) {
    expr = skip_blanks(expr);
    if (expr[0] && expr[0] != ';') {
      if (errpos) *errpos = expr;
      if (expr[0] != ':') return URA_EXTRA_TEXT;
    }
  }

  return ret;
}


// ////////////////////////////////////////////////////////////////////////// //
static const char *error_messages_asm[5] = {
  "generic error",
  "bad mnemonics",
  "bad operand",
  "extra text after instruction",
  "bad instruction",
};


static const char *error_messages_expr[14] = {
  "", /* none */
  "unexpected end of expression",
  "division by zero",
  "unbalanced parens",
  "number expected",
  "string expected",
  "unknown label",
  "term expected", /*FIXME: better message!*/
  "unknown function",
  "invalid operand type", /*FIXME: better message! this is string/number conflict*/
  "unknown macro argument", /*FIXME: better message! */
  "invalid fixup",
  "type mismatch", /* in comparisons */
  "operand text too long",
};


const char *urasm_errormsg (int errcode) {
  if (errcode >= 0) return "";
  if (errcode < URA_EXPR_ERR) {
    errcode = -(errcode-URA_EXPR_ERR);
    if (errcode >= UR_EXPRERR_EOS && errcode <= UR_EXPRERR_TOOLONG) {
      return error_messages_expr[errcode];
    }
    return "unknown error";
  } else {
    if (errcode < -5) return "unknown error";
    return error_messages_asm[(-errcode)-1];
  }
}

// UrForth/C standalone
// coded by Ketmar // Invisible Vector
// GPLv3 ONLY
//
#ifndef _GNU_SOURCE
# define _GNU_SOURCE
#endif

#include <getopt.h>
#include <setjmp.h>
#include <stdarg.h>
#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#ifdef WIN32
# include <windows.h>
#else
# include <signal.h>
#endif

#include "liburforth/urforth.h"


#define UR_FORCE_INLINE static inline __attribute__((always_inline))
#define UR_INLINE static inline


#define VERSION_HI  0
#define VERSION_MID 2
#define VERSION_LO  2


#define MAYBE_UNUSED __attribute__((unused))


// ////////////////////////////////////////////////////////////////////////// //
__attribute((unused))
static const char *ur_assert_failure (const char *cond, const char *fname, int fline,
                                      const char *func)
{
  for (const char *t = fname; *t; ++t) {
    #ifdef WIN32
    if (*t == '/' || *t == '\\') fname = t+1;
    #else
    if (*t == '/') fname = t+1;
    #endif
  }
  fflush(stdout);
  fprintf(stderr, "\n%s:%d: Assertion in `%s` failed: %s\n", fname, fline, func, cond);
  fflush(stderr);
  abort();
}

#define ur_assert(cond_)  do { if (__builtin_expect((!(cond_)), 0)) { ur_assert_failure(#cond_, __FILE__, __LINE__, __PRETTY_FUNCTION__); } } while (0)


// ////////////////////////////////////////////////////////////////////////// //
static char *ufoIncludeDir = NULL;


//==========================================================================
//
//  initUFEInclideDir
//
//==========================================================================
static void initUFEInclideDir (void) {
  const char *id = getenv("URASM_URFORTH_INCLUDE_DIR");
  if (id && id[0]) {
    ufoIncludeDir = strdup(id);
  } else {
    char myDir[4096];
    memset(myDir, 0, sizeof(myDir));
    #ifndef WIN32
    if (readlink("/proc/self/exe", myDir, sizeof(myDir)-1) < 0) {
      strcpy(myDir, ".");
    } else {
      char *p = (char *)strrchr(myDir, '/');
      if (!p) strcpy(myDir, "."); else *p = '\0';
    }
    #else
    GetModuleFileName(GetModuleHandle(NULL), myDir, sizeof(myDir)-1);
    char *p = strrchr(myDir, '\\');
    if (!p) strcpy(myDir, "."); else *p = '\0';
    #endif
    strcat(myDir, "/urflibs");
    ur_assert(ufoIncludeDir == NULL);
    ufoIncludeDir = strdup(myDir);
  }
  while (ufoIncludeDir[0] && ufoIncludeDir[strlen(ufoIncludeDir)-1] == '/') ufoIncludeDir[strlen(ufoIncludeDir)-1] = '\0';
  if (!ufoIncludeDir[0]) strcpy(ufoIncludeDir, ".");
}


static jmp_buf errJP;
static int g_argc;
static char **g_argv;


//==========================================================================
//
//  ufoFatalError
//
//==========================================================================
__attribute__((noreturn))
void ufoFatalError (void) {
  longjmp(errJP, 666);
}


//==========================================================================
//
//  strprintfVA
//
//==========================================================================
static char *strprintfVA (const char *fmt, va_list vaorig) {
  char *buf = NULL;
  int olen, len = 128;
  //
  buf = malloc(len);
  if (buf == NULL) { fprintf(stderr, "\nFATAL: out of memory!\n"); abort(); }
  for (;;) {
    char *nb;
    va_list va;
    //
    va_copy(va, vaorig);
    olen = vsnprintf(buf, len, fmt, va);
    va_end(va);
    if (olen >= 0 && olen < len) return buf;
    if (olen < 0) olen = len*2-1;
    nb = realloc(buf, olen+1);
    if (nb == NULL) { fprintf(stderr, "\nFATAL: out of memory!\n"); abort(); }
    buf = nb;
    len = olen+1;
  }
}


//==========================================================================
//
//  strprintf
//
//==========================================================================
static __attribute__((format(printf,1,2))) char *strprintf (const char *fmt, ...) {
  char *buf = NULL;
  va_list va;
  va_start(va, fmt);
  buf = strprintfVA(fmt, va);
  va_end(va);
  return buf;
}


//==========================================================================
//
//  strHasPathDelim
//
//==========================================================================
static int strHasPathDelim (const char *s) {
  if (!s || !s[0]) return 0;
  #ifdef WIN32
  return (strchr(s, '/') || strchr(s, '\\') ? 1 : 0);
  #else
  return (strchr(s, '/') ? 1 : 0);
  #endif
}


//==========================================================================
//
//  createIncludeName
//
//  returns malloced string
//
//==========================================================================
static char *createIncludeName (const char *fname, int assystem, const char *defaultmain,
                                const char *lastIncPathUFO)
{
  if (!fname || !fname[0]) return NULL;
  char *res;
  if (fname[0] != '/') {
    const char *incdir;
    if (!assystem) {
      incdir = NULL;
    } else if (assystem == -669) {
      incdir = lastIncPathUFO;
    } else if (assystem == -666) {
      incdir = lastIncPathUFO;
      if (!incdir || !incdir[0]) incdir = ufoIncludeDir;
    } else {
      ur_assert(0);
    }
    if (incdir == NULL || incdir[0] == 0) incdir = ".";
    res = strprintf("%s/%s", (incdir && incdir[0] ? incdir : "."), fname);
  } else {
    res = strprintf("%s", fname);
  }
  struct stat st;
  if (defaultmain && defaultmain[0]) {
    if (stat(res, &st) == 0) {
      if (S_ISDIR(st.st_mode)) {
        char *rs = strprintf("%s/%s", res, defaultmain);
        free(res);
        res = rs;
      }
    }
  }
  /* check if there is the disk file */
  if (strHasPathDelim(fname) && stat(res, &st) != 0) {
    /* no file, try "root include" */
    const char *incdir = (!assystem ? NULL : ufoIncludeDir);
    char *rs = strprintf("%s/%s", (incdir && incdir[0] ? incdir : "."), fname);
    free(res);
    res = rs;
    /* check for dir again */
    if (defaultmain && defaultmain[0]) {
      if (stat(res, &st) == 0) {
        if (S_ISDIR(st.st_mode)) {
          char *rs = strprintf("%s/%s", res, defaultmain);
          free(res);
          res = rs;
        }
      }
    }
  }
  //fprintf(stderr, "inc: fname=<%s>; sys=%d; def=<%s>; res=<%s>\n", fname, assystem, defaultmain, res);
  return res;
}


//==========================================================================
//
//  ufoCreateIncludeName
//
//==========================================================================
char *ufoCreateIncludeName (const char *fname, int assystem, const char *lastIncPath) {
  #if 0
  fprintf(stderr, "ICIN: fname=<%s>; lastinc=<%s>; assystem=%d\n",
          fname, lastIncPath, assystem);
  #endif
  assystem = (assystem ? -666 : -669);
  if (lastIncPath == NULL) lastIncPath = ufoIncludeDir;
  char *res = createIncludeName(fname, assystem, NULL, lastIncPath);
  struct stat st;
  char *tmp;
  if (res == NULL || res[0] == 0 || stat(res, &st) != 0) return res;
  if (S_ISDIR(st.st_mode)) {
    tmp = strprintf("%s/%s", res, "00-main-loader.f");
    free(res);
    res = tmp;
  }
  return res;
}


#ifndef WIN32
//==========================================================================
//
//  sigCtrlC
//
//==========================================================================
static void sigCtrlC (int sig) {
  ufoSetUserAbort();
}
#endif


//==========================================================================
//
//  ufcWord_N_BYE
//
//==========================================================================
static void ufcWord_N_BYE (uint32_t pfa) {
  uint32_t ec = ufoPopData();
  exit((int)ec);
}


//==========================================================================
//
//  ufcWord_BYE
//
//==========================================================================
static void ufcWord_BYE (uint32_t pfa) {
  exit(0);
}


//==========================================================================
//
//  ufcWord_ARGC
//
//==========================================================================
static void ufcWord_ARGC (uint32_t pfa) {
  int rest = g_argc - optind;
  if (rest <= 1) ufoPushData(0); else ufoPushData((uint32_t)(rest - 1));
}


//==========================================================================
//
//  ufcWord_ARGV
//
//  ARGV ( idx -- addr count )
//  copy CLI arg to PAD+4, and set counter to PAD.
//  0 is first arg, not program name!
//
//==========================================================================
static void ufcWord_ARGV (uint32_t pfa) {
  uint32_t idx = ufoPopData();
  int rest = g_argc - optind - 1;
  const uint32_t addr = ufoGetPAD() + 4u;
  uint32_t count = 0;
  if (rest > 0 || idx < rest) {
    const unsigned char *arg = (const unsigned char *)(g_argv[optind + 1 + (int)idx]);
    while (*arg) {
      ufoPokeByte(addr + count, *arg);
      count += 1u; arg += 1u;
    }
  }
  ufoPokeCell(addr - 4u, count);
  ufoPushData(addr);
  ufoPushData(count);
}


static int norepl = 0;


//==========================================================================
//
//  initUrForth
//
//==========================================================================
static void initUrForth (void) {
  #ifdef WIN32
  ufoCondDefine("SHITDOZE");
  #else
  ufoCondDefine("NIX");
  #endif
  ufoCondDefine("STANDALONE");
  if (ufoSStepAllowed()) ufoCondDefine("DEBUGGER");
  if (norepl) ufoCondDefine("SKIP-REPL");
  ufoRegisterWord("N-BYE", &ufcWord_N_BYE, UFW_FLAG_PROTECTED);
  ufoRegisterWord("BYE", &ufcWord_BYE, UFW_FLAG_PROTECTED);
  ufoRegisterWord("ARGC", &ufcWord_ARGC, UFW_FLAG_PROTECTED);
  ufoRegisterWord("ARGV", &ufcWord_ARGV, UFW_FLAG_PROTECTED);
}


//==========================================================================
//
//  urcShutdown
//
//==========================================================================
static void urcShutdown (void) {
  ufoDeinit();
  if (ufoIncludeDir) free(ufoIncludeDir);
}


///////////////////////////////////////////////////////////////////////////////
// options
//
static struct option longOpts[] = {
  {"help", 0, NULL, 'h'},
  {"debug", 0, NULL, 666},
  {"no-repl", 0, NULL, 667},
  {NULL, 0, NULL, 0}
};


//==========================================================================
//
//  usage
//
//==========================================================================
static void usage (const char *pname) {
  printf(
    "usage: %s [options] infile\n"
    "\n"
    "options:\n"
    "  -h --help   this help\n"
    "     --debug  enable interactive debugger\n"
  , pname);
}


//==========================================================================
//
//  main
//
//==========================================================================
int main (int argc, char *argv[]) {
  int res = 0, c;
  const char *pname = argv[0];
  int allowDebug = 0;

  ufoSetUserPostInit(&initUrForth);

  initUFEInclideDir();

  printf("UrForth/C v%d.%d.%d, compile date: %s %s\n", VERSION_HI, VERSION_MID, VERSION_LO, __DATE__, __TIME__);
  while ((c = getopt_long(argc, argv, "h", longOpts, NULL)) >= 0) {
    switch (c) {
      case '?': return 1;
      case 'h': usage(pname); res = 0; if (ufoIncludeDir) free(ufoIncludeDir); exit(0);
      case 666:
        if (!ufoIsMTaskEnabled()) {
          fflush(NULL); fprintf(stderr, "FATAL: cannot enable debugger, no multitask support!\n");
          exit(1);
        }
        allowDebug = 1;
        break;
      case 667: norepl = 1; break;
    }
  }

  g_argc = argc;
  g_argv = argv;

  #ifndef WIN32
  signal(SIGINT, &sigCtrlC);
  #endif

  atexit(&urcShutdown);

  if (setjmp(errJP) == 0) {
    ufoInit();
  } else {
    fprintf(stderr, "FATAL: INITILIZATION FAILED!\n");
    exit(1);
  }

  ufoSetSStepAllowed(allowDebug);

  if (optind >= argc) {
    for (;;) {
      if (setjmp(errJP) == 0) {
        uint32_t cfa;
        cfa = ufoFindWordInVocabulary("UFO-RUN-REPL", ufoGetForthVocId());
        if (cfa == 0) {
          fprintf(stderr, "FATAL: REPL is not available.\n");
          exit(1);
        } else {
          ufoRunWord(cfa);
        }
      }
    }
  } else {
    if (setjmp(errJP) == 0) {
      ufoRunFile(argv[optind]);
    } else {
      res = 1;
    }
  }

  return (res ? 1 : 0);
}

// UrForth/C standalone
// coded by Ketmar // Invisible Vector
// GPLv3 ONLY
//
#ifndef _GNU_SOURCE
# define _GNU_SOURCE
#endif

#include <setjmp.h>
#include <stdarg.h>
#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#ifdef WIN32
# include <windows.h>
#else
# include <signal.h>
#endif

#include "liburforth/urforth.h"


#define UR_FORCE_INLINE static inline __attribute__((always_inline))
#define UR_INLINE static inline


#define VERSION_HI  2
#define VERSION_MID 0
#define VERSION_LO  1


#define MAYBE_UNUSED __attribute__((unused))


// ////////////////////////////////////////////////////////////////////////// //
__attribute((unused))
static const char *ur_assert_failure (const char *cond, const char *fname, int fline,
                                      const char *func)
{
  for (const char *t = fname; *t; ++t) {
    #ifdef WIN32
    if (*t == '/' || *t == '\\') fname = t+1;
    #else
    if (*t == '/') fname = t+1;
    #endif
  }
  fflush(stdout);
  fprintf(stderr, "\n%s:%d: Assertion in `%s` failed: %s\n", fname, fline, func, cond);
  fflush(stderr);
  abort();
}

#define ur_assert(cond_)  do { if (__builtin_expect((!(cond_)), 0)) { ur_assert_failure(#cond_, __FILE__, __LINE__, __PRETTY_FUNCTION__); } } while (0)


// ////////////////////////////////////////////////////////////////////////// //
static char *ufoIncludeDir = NULL;


//==========================================================================
//
//  initUFEInclideDir
//
//==========================================================================
static void initUFEInclideDir (void) {
  const char *id = getenv("URASM_URFORTH_INCLUDE_DIR");
  if (id && id[0]) {
    ufoIncludeDir = strdup(id);
  } else {
    char myDir[4096];
    memset(myDir, 0, sizeof(myDir));
    #ifndef WIN32
    if (readlink("/proc/self/exe", myDir, sizeof(myDir)-1) < 0) {
      strcpy(myDir, ".");
    } else {
      char *p = (char *)strrchr(myDir, '/');
      if (!p) strcpy(myDir, "."); else *p = '\0';
    }
    #else
    GetModuleFileName(GetModuleHandle(NULL), myDir, sizeof(myDir)-1);
    char *p = strrchr(myDir, '\\');
    if (!p) strcpy(myDir, "."); else *p = '\0';
    #endif
    strcat(myDir, "/urflibs");
    ur_assert(ufoIncludeDir == NULL);
    ufoIncludeDir = strdup(myDir);
  }
  while (ufoIncludeDir[0] && ufoIncludeDir[strlen(ufoIncludeDir)-1] == '/') ufoIncludeDir[strlen(ufoIncludeDir)-1] = '\0';
  if (!ufoIncludeDir[0]) strcpy(ufoIncludeDir, ".");
}


static jmp_buf errJP;
static int g_argc;
static char **g_argv;


//==========================================================================
//
//  ufoFatalError
//
//==========================================================================
__attribute__((noreturn))
void ufoFatalError (void) {
  longjmp(errJP, 666);
}


//==========================================================================
//
//  strprintfVA
//
//==========================================================================
static char *strprintfVA (const char *fmt, va_list vaorig) {
  char *buf = NULL;
  int olen, len = 128;
  //
  buf = malloc(len);
  if (buf == NULL) { fprintf(stderr, "\nFATAL: out of memory!\n"); abort(); }
  for (;;) {
    char *nb;
    va_list va;
    //
    va_copy(va, vaorig);
    olen = vsnprintf(buf, len, fmt, va);
    va_end(va);
    if (olen >= 0 && olen < len) return buf;
    if (olen < 0) olen = len*2-1;
    nb = realloc(buf, olen+1);
    if (nb == NULL) { fprintf(stderr, "\nFATAL: out of memory!\n"); abort(); }
    buf = nb;
    len = olen+1;
  }
}


//==========================================================================
//
//  strprintf
//
//==========================================================================
static __attribute__((format(printf,1,2))) char *strprintf (const char *fmt, ...) {
  char *buf = NULL;
  va_list va;
  va_start(va, fmt);
  buf = strprintfVA(fmt, va);
  va_end(va);
  return buf;
}


//==========================================================================
//
//  findMain
//
//  return NULL or new path
//
//==========================================================================
static char *findMain (const char *path, const char **mainList) {
  struct stat st;
  while (*mainList != NULL) {
    char *newpath = strprintf("%s/%s", path, *mainList);
    if (stat(newpath, &st) == 0) {
      if (!S_ISDIR(st.st_mode)) {
        return newpath;
      }
    }
    free(newpath);
    mainList += 1u;
  }
  return NULL;
}


static const char *sysMains[] = {
  "00-main-loader.f",
  NULL
};


static const char *userMains[] = {
  "zzmain.f",
  "main.f",
  "00-main-loader.f",
  "zzmain.zas",
  "main.zas",
  NULL,
};


//==========================================================================
//
//  createIncludeName
//
//  return malloced string
//
//==========================================================================
static char *createIncludeName (const char *fname, int assystem, const char *lastIncPathUFO) {
  struct stat st;
  char *res;
  char *newpath;

  if (!fname || !fname[0]) return NULL;

  if (fname[0] != '/') {
    const char *incdir = lastIncPathUFO;
    if (assystem && (!incdir || !incdir[0])) incdir = ufoIncludeDir;
    if (incdir == NULL || incdir[0] == 0) incdir = ".";
    res = strprintf("%s/%s", incdir, fname);
  } else {
    res = strprintf("%s", fname);
  }

  if (stat(res, &st) == 0) {
    if (S_ISDIR(st.st_mode)) {
      newpath = findMain(res, (assystem ? sysMains : userMains));
      if (newpath != NULL) {
        free(res);
        res = newpath;
      }
    }
  } else if (assystem && fname[0] != '/' && ufoIncludeDir != NULL && ufoIncludeDir[0] != 0) {
    // if it is a system include, try from system root
    newpath = strprintf("%s/%s", ufoIncludeDir, fname);
    if (stat(newpath, &st) == 0) {
      free(res);
      res = newpath;
      if (S_ISDIR(st.st_mode)) {
        newpath = findMain(res, sysMains);
        if (newpath != NULL) {
          free(res);
          res = newpath;
        }
      }
    } else {
      free(newpath);
    }
  }

  //fprintf(stderr, "inc: fname=<%s>; sys=%d; def=<%s>; res=<%s>\n", fname, assystem, defaultmain, res);
  return res;
}


//==========================================================================
//
//  ufoCreateIncludeName
//
//==========================================================================
char *ufoCreateIncludeName (const char *fname, int assystem, const char *lastIncPath) {
  #if 0
  fprintf(stderr, "ICIN: fname=<%s>; lastinc=<%s>; assystem=%d\n",
          fname, lastIncPath, assystem);
  #endif
  if (lastIncPath == NULL && assystem) lastIncPath = ufoIncludeDir;
  return createIncludeName(fname, assystem, lastIncPath);
}


#ifndef WIN32
//==========================================================================
//
//  sigCtrlC
//
//==========================================================================
static void sigCtrlC (int sig) {
  ufoSetUserAbort();
}
#endif


//==========================================================================
//
//  ufcWord_N_BYE
//
//==========================================================================
static void ufcWord_N_BYE (uint32_t pfa) {
  uint32_t ec = ufoPopData();
  exit((int)ec);
}


//==========================================================================
//
//  ufcWord_BYE
//
//==========================================================================
static void ufcWord_BYE (uint32_t pfa) {
  exit(0);
}


//==========================================================================
//
//  ufcWord_ARGC
//
//==========================================================================
static void ufcWord_ARGC (uint32_t pfa) {
  int rest = g_argc;
  if (rest <= 0) ufoPushData(0); else ufoPushData((uint32_t)rest);
}


//==========================================================================
//
//  ufcWord_ARGV
//
//  ARGV ( idx -- addr count )
//  copy CLI arg to PAD+4, and set counter to PAD.
//  0 is first arg, not program name!
//
//==========================================================================
static void ufcWord_ARGV (uint32_t pfa) {
  uint32_t idx = ufoPopData();
  int rest = g_argc;
  const uint32_t addr = ufoGetPAD() + 4u;
  uint32_t count = 0;
  if (rest > 0 || idx < rest) {
    const unsigned char *arg = (const unsigned char *)(g_argv[(int)idx]);
    while (*arg) {
      ufoPokeByte(addr + count, *arg);
      count += 1u; arg += 1u;
    }
  }
  ufoPokeCell(addr - 4u, count);
  ufoPushData(addr);
  ufoPushData(count);
}


//==========================================================================
//
//  initUrForth
//
//==========================================================================
static void initUrForth (void) {
  #ifdef WIN32
  ufoCondDefine("SHITDOZE");
  #else
  ufoCondDefine("NIX");
  #endif
  ufoCondDefine("URASM-NEW");
  if (ufoSStepAllowed()) ufoCondDefine("DEBUGGER");
  ufoRegisterWord("N-BYE", &ufcWord_N_BYE, UFW_FLAG_PROTECTED);
  ufoRegisterWord("BYE", &ufcWord_BYE, UFW_FLAG_PROTECTED);
  ufoRegisterWord("ARGC", &ufcWord_ARGC, UFW_FLAG_PROTECTED);
  ufoRegisterWord("ARGV", &ufcWord_ARGV, UFW_FLAG_PROTECTED);
}


//==========================================================================
//
//  urcShutdown
//
//==========================================================================
static void urcShutdown (void) {
  ufoDeinit();
  if (ufoIncludeDir) free(ufoIncludeDir);
}


//==========================================================================
//
//  main
//
//==========================================================================
int main (int argc, char *argv[]) {
  int res = 0;
  int dorepl = 0;
  int allowDebug = 0;

  ufoSetUserPostInit(&initUrForth);

  initUFEInclideDir();

  printf("UrForth/C & UrAsm v%d.%d.%d, compile date: %s %s\n",
         VERSION_HI, VERSION_MID, VERSION_LO, __DATE__, __TIME__);

  g_argc = argc - 1;
  g_argv = argv + 1;

  if (g_argc > 0 && strcmp(g_argv[0], "--repl") == 0) {
    dorepl = 1;
    g_argc -= 1; g_argv += 1;
  }

  if (g_argc > 0 && strcmp(g_argv[0], "--debug") == 0) {
    if (!ufoIsMTaskEnabled()) {
      fflush(NULL); fprintf(stderr, "FATAL: cannot enable debugger, no multitask support!\n");
      exit(1);
    }
    allowDebug = 1;
    g_argc -= 1; g_argv += 1;
  }

  #ifndef WIN32
  signal(SIGINT, &sigCtrlC);
  #endif

  atexit(&urcShutdown);

  if (setjmp(errJP) == 0) {
    ufoInit();
  } else {
    fprintf(stderr, "FATAL: INITILIZATION FAILED!\n");
    exit(1);
  }

  ufoSetSStepAllowed(allowDebug);

  if (dorepl) {
    for (;;) {
      if (setjmp(errJP) == 0) {
        uint32_t cfa;
        cfa = ufoFindWordInVocabulary("UFO-RUN-REPL", ufoGetForthVocId());
        if (cfa == 0) {
          fprintf(stderr, "FATAL: REPL is not available.\n");
          exit(1);
        } else {
          ufoRunWord(cfa);
        }
      }
    }
  } else {
    uint32_t cfa = ufoFindWordInVocabulary("RUN-URASM", ufoGetForthVocId());
    if (cfa == 0) {
      fprintf(stderr, "FATAL: cannot find main URASM loop word.\n");
      exit(1);
    }
    if (setjmp(errJP) == 0) {
      ufoRunWord(cfa);
    } else {
      res = 1;
    }
  }

  return (res ? 1 : 0);
}

// and now for something completely different...
// UrAsm built-in Forth Engine!
// GPLv3 ONLY
#ifndef URFORTH_HEADER
#define URFORTH_HEADER

#include <stddef.h>
#include <stdint.h>


// should abort; defined by the main app
extern void ufoFatalError (void) __attribute__((noreturn));

// returns malloced string
extern char *ufoCreateIncludeName (const char *fname, int assystem, const char *lastIncPath);

// required for UrAsm
void ufoClearIncludePath (void);

// can be called by the main app
void ufoFatal (const char *fmt, ...) __attribute__((noreturn)) __attribute__((format(printf, 1, 2)));

// called after main initialisation.
void ufoSetUserPostInit (void (*cb) (void));

// will be called automatically if necessary.
// it is safe to call it several times.
void ufoInit (void);

int ufoIsInited (void);

// this will NOT be called automatically!
// WARNING! reiniting after calling this may, or may not work.
void ufoDeinit (void);

// check if the system was compiled with multitasking support
int ufoIsMTaskEnabled (void);

int ufoSStepAllowed (void);
void ufoSetSStepAllowed (int enabled);

// call this, and VM will abort soon.
// used for Ctrl+C handler.
void ufoSetUserAbort (void);


// ////////////////////////////////////////////////////////////////////////// //
// conditional defines (case-insensitive)
//

int ufoHasCondDefine (const char *name);
void ufoCondDefine (const char *name);
void ufoCondUndef (const char *name);


// ////////////////////////////////////////////////////////////////////////// //
// extension API
//

// if set, this will be used when we are out of include files. intended for UrAsm.
// return 0 if there is no more lines, otherwise the string should be copied
// to buffer, `*fname` and `*fline` should be properly set.
// max line length is 510 bytes for now.
extern int (*ufoFileReadLine) (void *buf, size_t bufsize, const char **fname, int *fline);

// get "FORTH" vocid.
uint32_t ufoGetForthVocId (void);


// ////////////////////////////////////////////////////////////////////////// //
// register new word
//

// possible word flags
#define UFW_FLAG_IMMEDIATE  (1u<<16)
#define UFW_FLAG_SMUDGE     (1u<<17)
#define UFW_FLAG_NORETURN   (1u<<18)
#define UFW_FLAG_HIDDEN     (1u<<19)
#define UFW_FLAG_CBLOCK     (1u<<20)
#define UFW_FLAG_VOCAB      (1u<<21)
#define UFW_FLAG_SCOLON     (1u<<22)
#define UFW_FLAG_PROTECTED  (1u<<23)
#define UFW_WARG_CONDBRANCH (1u<<24)
#define UFW_FLAG_MAYRETURN  (1u<<25)

#define UFW_WARG_MASK    ((uint32_t)0xff00U)

// possible word arg types
#define UFW_WARG_NONE     (0u<<8)
#define UFW_WARG_BRANCH   (1u<<8)
#define UFW_WARG_LIT      (2u<<8)
#define UFW_WARG_C4STRZ   (3u<<8)
#define UFW_WARG_CFA      (4u<<8)
#define UFW_WARG_CBLOCK   (5u<<8)
#define UFW_WARG_VOCID    (6u<<8)
#define UFW_WARG_C1STRZ   (7u<<8)
// the arg is amount of bytes to skip (not including the counter itself)
// the following inline data cannot (and shouldn't) be decompiled
// addr += UFO_ALIGN4(4u + ufoImgGetU32(addr))
#define UFW_WARG_DATASKIP (8u<<8)
#define UFW_WARG_PFA      (9u<<8)


typedef void (*ufoNativeCFA) (uint32_t pfa);


// create new vocabulary. return vocid.
uint32_t ufoCreateVoc (const char *wname, uint32_t parentvocid, uint32_t flags);
// set "CURRENT" and "CONTEXT" to this vocavulary.
void ufoVocSetOnlyDefs (uint32_t vocid);

// `flags` is ORed args and flags
// returns CFA of the new word
uint32_t ufoRegisterWord (const char *wname, ufoNativeCFA cfa, uint32_t flags);

void ufoRegisterConstant (const char *wname, uint32_t value, uint32_t flags);
// the following words returs value address
uint32_t ufoRegisterVariable (const char *wname, uint32_t value, uint32_t flags);
uint32_t ufoRegisterValue (const char *wname, uint32_t value, uint32_t flags);
uint32_t ufoRegisterDefer (const char *wname, uint32_t value, uint32_t flags);

// check if we have the corresponding word.
// return CFA suitable for executing, or 0.
uint32_t ufoFindWordInVocabulary (const char *wname, uint32_t vocid);

uint32_t ufoGetIP (void);
void ufoSetIP (uint32_t newip);

// get and set "STATE"
int ufoIsExecuting (void);
int ufoIsCompiling (void);
void ufoSetExecuting (void);
void ufoSetCompiling (void);

uint32_t ufoGetHere ();
uint32_t ufoGetPad ();

uint8_t ufoTIBPeekCh (uint32_t ofs);
uint8_t ufoTIBGetCh (void);
void ufoTIBSkipCh (void);
// returns 0 on EOF
int ufoTIBSRefill (int allowCrossIncludes);

// ( -- addr count TRUE / FALSE )
// does base TIB parsing; never copies anything.
// as our reader is line-based, returns FALSE on EOL.
// EOL is detected after skipping leading delimiters.
// passing -1 as delimiter skips the whole line, and always returns FALSE.
// trailing delimiter is always skipped.
// result is on the data stack.
void ufoCallParseIntr (uint32_t delim, int skipLeading);
// ( -- addr count )
// parse with leading blanks skipping. doesn't copy anything.
// return empty string on EOL.
void ufoCallParseName (void);
// ( delim -- addr count TRUE / FALSE )
// parse without skipping delimiters; never copies anything.
// as our reader is line-based, returns FALSE on EOL.
// passing 0 as delimiter skips the whole line, and always returns FALSE.
// trailing delimiter is always skipped.
void ufoCallParse (uint32_t delim);
// utils
void ufoCallParseSkipBlanks (void);
void ufoCallParseSkipComments (void); // multiline
void ufoCallParseSkipLineComments (void); // will not refill
void ufoCallParseSkipLine (void); // to the end of line; doesn't refill

// convert number from addrl+1
// returns address of the first inconvertible char
// (BASED-NUMBER) ( -- num TRUE / FALSE )
void ufoCallBasedNumber (uint32_t addr, uint32_t count, int allowSign, int base);


uint32_t ufoPeekData (void);
uint32_t ufoPopData (void);
void ufoPushData (uint32_t value);
// WARNING! *ALWAYS* use this to push boolean values!
//          this is because "TRUE" value is not always `1`.
void ufoPushBoolData (int val);

uint32_t ufoPeekRet (void);
uint32_t ufoPopRet (void);
void ufoPushRet (uint32_t value);
// WARNING! *ALWAYS* use this to push boolean values!
//          this is because "TRUE" value is not always `1`.
void ufoPushBoolRet (int val);

uint8_t ufoPeekByte (uint32_t addr);
uint16_t ufoPeekWord (uint32_t addr);
uint32_t ufoPeekCell (uint32_t addr);

void ufoPokeByte (uint32_t addr, uint32_t value);
void ufoPokeWord (uint32_t addr, uint32_t value);
void ufoPokeCell (uint32_t addr, uint32_t value);

uint32_t ufoGetPAD (void);

// dictionary emitters
void ufoEmitByte (uint32_t value);
void ufoEmitWord (uint32_t value);
void ufoEmitCell (uint32_t value);

// include and interpret the given file.
// cannot be called recursively!
// doesn't reset internal state.
// WARNING! returns when include stack is empty,
//          not when the file ends.
void ufoRunFile (const char *fname);

// run word, return after it returned.
// cannot be called recursively!
// doesn't reset internal state.
// CFA is from `ufoFindWordInVocabulary()`.
void ufoRunWord (uint32_t cfa);

// run default interpret loop.
void ufoRunInterpretLoop (void);

// run word in "MACRO" mode.
// in this mode, includes are ignored, and current line
// will be set with `ufoFileReadLine()`.
// CFA is from `ufoFindWordInVocabulary()`.
void ufoRunMacroWord (uint32_t cfa);

// check if we are currently in "MACRO" mode.
// should be called from registered words.
int ufoIsInMacroMode (void);

// can be called to stop execution started with `ufoRunInterpretLoop()`.
// WARNING! UNRELIABLE!
void ufoFinishVM (void);

// check if VM was exited due to `ufoFinishVM()`
//int ufoWasVMFinished (void);


#endif

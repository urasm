// and now for something completely different...
// UrAsm built-in Forth Engine!
// GPLv3 ONLY
#ifndef WIN32
#include <errno.h>
#include <termios.h>
#include <sys/select.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <sys/time.h>
#endif


#define UFO_TTY_OUT_BUFFER_SIZE  (4096)
static uint8_t ufoOutBuffer[UFO_TTY_OUT_BUFFER_SIZE];
static uint32_t ufoOutBufferUsed = 0;

static int ufoTermIsRaw = 0; /* for atexit() function to check if restore is needed */
#ifndef WIN32
static struct termios ufoOrigTIOS; /* in order to restore at exit */
static int ufoAtExitSet = 0; /* register atexit just 1 time */


static void ufoAtExitCB (void);


//==========================================================================
//
//  ufoGetTTYSize
//
//==========================================================================
static void ufoGetTTYSize (int *w, int *h) {
  struct winsize ws;
  if (ioctl(1, TIOCGWINSZ, &ws) == 0) {
    *w = (int)ws.ws_col;
    *h = (int)ws.ws_row;
    if (*w == 0) *w = 80;
    if (*h == 0) *h = 24;
  } else {
    *w = 80; *h = 24;
  }
}


//==========================================================================
//
//  ufoEnableRaw
//
//==========================================================================
static int ufoEnableRaw (void) {
  struct termios raw;
  if (ufoTermIsRaw) return 0;
  if (!isatty(STDIN_FILENO)) goto fatal;
  if (tcgetattr(STDIN_FILENO, &ufoOrigTIOS) == -1) goto fatal;
  raw = ufoOrigTIOS; /* modify the original mode */
  /* input modes: no break, no CR to NL, no parity check, no strip char,
   * no start/stop output control. */
  raw.c_iflag &= ~(BRKINT|ICRNL|INPCK|ISTRIP|IXON);
  /* output modes - disable post processing */
  raw.c_oflag &= ~(OPOST);
  /* control modes - set 8 bit chars */
  raw.c_cflag |= (CS8);
  /* local modes - choing off, canonical off, no extended functions,
   * no signal chars (^Z,^C) */
  raw.c_lflag &= ~(ECHO|ICANON|IEXTEN|ISIG);
  /* control chars - set return condition: min number of bytes and timer;
   * we want read to return every single byte, without timeout */
  raw.c_cc[VMIN] = 1; raw.c_cc[VTIME] = 0; /* 1 byte, no timer */
  /* put terminal in raw mode after flushing */
  if (tcsetattr(STDIN_FILENO, TCSAFLUSH, &raw) < 0) goto fatal;
  if (!ufoAtExitSet) { atexit(&ufoAtExitCB); ufoAtExitSet = 1; }
  ufoTermIsRaw = 1;
  return 0;
fatal:
  return -1;
}


//==========================================================================
//
//  ufoDisableRaw
//
//==========================================================================
static void ufoDisableRaw (void) {
  if (ufoTermIsRaw && tcsetattr(STDIN_FILENO, TCSAFLUSH, &ufoOrigTIOS) != -1) {
    ufoTermIsRaw = 0;
  }
}


//==========================================================================
//
//  ufoAtExitCB
//
//  at exit we'll try to fix the terminal to the initial conditions
//
//==========================================================================
static void ufoAtExitCB (void) {
  ufoDisableRaw();
}
#endif


//==========================================================================
//
//  ufoIsGoodTTY
//
//==========================================================================
static int ufoIsGoodTTY (void) {
  uint32_t res = 0;
  #ifndef WIN32
  if (isatty(STDIN_FILENO) && isatty(STDOUT_FILENO)) {
    // this checks for pseudo-tty from my SXED, for example
    // it doesn't have a proper size set
    struct winsize ws;
    if (ioctl(1, TIOCGWINSZ, &ws) == 0) {
      if (ws.ws_col >= 40 && ws.ws_row >= 24) {
        res = 1;
      }
    }
  }
  #endif
  return res;
}


// TTY:TTY?
// ( -- bool )
// check if input and output are valid TTY(s).
UFWORD(TTY_TTYQ) {
  ufoPushBool(ufoIsGoodTTY());
}

// TTY:RAW?
// ( -- bool )
// check if current TTY mode is raw.
UFWORD(TTY_RAWQ) {
  ufoPushBool(ufoTermIsRaw);
}

// TTY:SIZE
// ( -- width height )
// get TTY size. for non-TTYs retur default 80x24.
UFWORD(TTY_SIZE) {
  int w, h;
  #ifndef WIN32
  ufoGetTTYSize(&w, &h);
  #else
  w = 80; h = 24;
  #endif
  ufoPush(w);
  ufoPush(h);
}

// TTY:SET-RAW
// ( -- success-bool )
// switch TTY to raw mode.
UFWORD(TTY_SET_RAW) {
  #ifndef WIN32
  ufoPushBool(ufoEnableRaw() == 0);
  #else
  ufoPushBool(0);
  #endif
}

// TTY:SET-COOKED
// ( -- success-bool )
// switch TTY to cooked mode.
UFWORD(TTY_SET_COOKED) {
  #ifndef WIN32
  ufoDisableRaw();
  ufoPushBool(ufoTermIsRaw == 0);
  #else
  ufoPushBool(0);
  #endif
}


//==========================================================================
//
//  ufoTTYRawFlush
//
//==========================================================================
static void ufoTTYRawFlush (void) {
  if (ufoOutBufferUsed != 0) {
    (void)write(STDOUT_FILENO, ufoOutBuffer, ufoOutBufferUsed);
    ufoOutBufferUsed = 0;
  }
}


#ifndef WIN32
//==========================================================================
//
//  ufoTTYRawEmit
//
//==========================================================================
static void ufoTTYRawEmit (uint8_t ch) {
  if (ufoOutBufferUsed == UFO_TTY_OUT_BUFFER_SIZE) ufoTTYRawFlush();
  ufoOutBuffer[ufoOutBufferUsed] = ch; ufoOutBufferUsed += 1;
  #if 0
  if (ch == '\n') ufoTTYRawFlush();
  #endif
}
#endif


// TTY:RAW-EMIT
// ( n -- )
UFWORD(TTY_RAW_EMIT) {
  #ifndef WIN32
  ufoTTYRawEmit((uint8_t)ufoPop());
  #else
  ufoDrop();
  #endif
}

// TTY:RAW-TYPE
// ( addr count -- )
UFWORD(TTY_RAW_TYPE) {
  #ifndef WIN32
  int32_t count = (int32_t)ufoPop();
  uint32_t addr = ufoPop();
  while (count > 0) {
    const uint8_t ch = ufoImgGetU8(addr);
    ufoTTYRawEmit(ch);
    addr += 1; count -= 1;
  }
  #else
  ufoDrop(); ufoDrop();
  #endif
}

// TTY:RAW-FLUSH
// ( -- )
UFWORD(TTY_RAW_FLUSH) {
  #ifndef WIN32
  ufoTTYRawFlush();
  #endif
}

// TTY:RAW-READCH
// ( -- ch / -1 )
// -1 returned on error, or on EOF.
UFWORD(TTY_RAW_READCH) {
  uint32_t res = ~(uint32_t)0; // -1
  #ifndef WIN32
  if (ufoTermIsRaw) {
    uint8_t ch;
    const ssize_t nread = read(STDIN_FILENO, &ch, 1);
    if (nread == 1) res = ch;
  }
  #endif
  ufoPush(res);
}

// TTY:RAW-READY?
// ( -- bool )
// check if raw TTY has some data to read.
UFWORD(TTY_RAW_READYQ) {
  #ifndef WIN32
  if (ufoTermIsRaw) {
    for (;;) {
      fd_set rd;
      FD_ZERO(&rd);
      FD_SET(STDIN_FILENO, &rd);
      struct timeval to;
      to.tv_sec = 0; to.tv_usec = 0;
      const int res = select(STDIN_FILENO + 1, &rd, NULL, NULL, &to);
      if (res == -1) {
        if (errno == EINTR) continue;
      }
      ufoPushBool(res == 1);
      return;
    }
  }
  #endif
  ufoPushBool(0);
}

// and now for something completely different...
// UrAsm built-in Forth Engine!
// GPLv3 ONLY
#ifdef WIN32
#include <windows.h>
#endif
#include <stdarg.h>
#include <setjmp.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

#include <sys/fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>

#include "urforth.h"

#ifdef WIN32
# define realpath(shit,fuck)  _fullpath(fuck, shit, 32768)
#endif

// if defined, UFO will allocate 64MB main image, and 4MB temp image immediately
#define UFO_HUGE_IMAGES

// use relative branch addresses for position-independent code?
#define UFO_RELATIVE_BRANCH

// if defined, multitasking engine is allowed.
// multitasker is currently used only for debugger.
//#define UFO_MTASK_ALLOWED


//#define UFO_DEBUG_WRITE_MAIN_IMAGE
//#define UFO_DEBUG_WRITE_DEBUG_IMAGE


#define UFO_DEBUG_STARTUP_TIMES
//#define UFO_DEBUG_FATAL_ABORT
#define UFO_DEBUG_DEBUG  /* ;-) */
//#define UFO_TRACE_VM_DOER
//#define UFO_TRACE_VM_RUN
//#define UFO_DEBUG_INCLUDE
//#define UFO_DEBUG_DUMP_NEW_HEADERS
//#define UFO_DEBUG_FIND_WORD
//#define UFO_DEBUG_FIND_WORD_IN_VOC
//#define UFO_DEBUG_FIND_WORD_COLON

// 2/8 msecs w/o inlining
// 1/5 msecs with inlining
#if 1
# define UFO_FORCE_INLINE static inline __attribute__((always_inline))
#else
# define UFO_FORCE_INLINE static __attribute__((noinline)) /*__attribute__((unused))*/
#endif
#define UFO_DISABLE_INLINE static __attribute__((noinline)) /*__attribute__((unused))*/

// detect arch, and use faster memory access code on x86
#if defined(__x86_64__) || defined(_M_X64) || \
    defined(i386) || defined(__i386__) || defined(__i386) || defined(_M_IX86)
# define UFO_FAST_MEM_ACCESS
#endif

// should not be bigger than this!
#define UFO_MAX_WORD_LENGTH  (250)

//#define UFO_ALIGN4(v_)  (((v_) + 3u) / 4u * 4u)
#define UFO_ALIGN4(v_)  (((v_) + 3u) & ~(uint32_t)3)


// ////////////////////////////////////////////////////////////////////////// //
static void ufoFlushOutput (void);

UFO_DISABLE_INLINE const char *ufo_assert_failure (const char *cond, const char *fname,
                                                   int fline, const char *func)
{
  for (const char *t = fname; *t; ++t) {
    #ifdef WIN32
    if (*t == '/' || *t == '\\') fname = t+1;
    #else
    if (*t == '/') fname = t+1;
    #endif
  }
  ufoFlushOutput();
  fprintf(stderr, "\n%s:%d: Assertion in `%s` failed: %s\n", fname, fline, func, cond);
  ufoFlushOutput();
  abort();
}

#define ufo_assert(cond_)  do { if (__builtin_expect((!(cond_)), 0)) { ufo_assert_failure(#cond_, __FILE__, __LINE__, __PRETTY_FUNCTION__); } } while (0)


static char ufoRealPathBuf[32769];
static char ufoRealPathHashBuf[32769];


//==========================================================================
//
//  ufoRealPath
//
//==========================================================================
static char *ufoRealPath (const char *fname) {
  char *res;
  if (fname != NULL && fname[0] != 0) {
    res = realpath(fname, NULL);
    if (res != NULL) {
      const size_t slen = strlen(res);
      if (slen < 32768) {
        strcpy(ufoRealPathBuf, res);
        free(res);
        res = ufoRealPathBuf;
      } else {
        free(res);
        res = NULL;
      }
    }
  } else {
    res = NULL;
  }
  return res;
}


#ifndef WIN32
static time_t secstart = 0;
#endif



//==========================================================================
//
//  ufo_get_msecs
//
//==========================================================================
static uint64_t ufo_get_msecs (void) {
  #ifdef WIN32
  return GetTickCount();
  #else
  struct timespec ts;
  #ifdef CLOCK_MONOTONIC
  ufo_assert(clock_gettime(CLOCK_MONOTONIC, &ts) == 0);
  #else
  // this should be available everywhere
  ufo_assert(clock_gettime(CLOCK_REALTIME, &ts) == 0);
  #endif
  // first run?
  if (secstart == 0) {
    secstart = ts.tv_sec+1;
    ufo_assert(secstart); // it should not be zero
  }
  return (uint64_t)(ts.tv_sec-secstart+2)*1000U+(uint32_t)ts.tv_nsec/1000000U;
  // nanoseconds
  //return (uint64_t)(ts.tv_sec-secstart+2)*1000000000U+(uint32_t)ts.tv_nsec;
  #endif
}


//==========================================================================
//
//  joaatHashBuf
//
//==========================================================================
UFO_FORCE_INLINE uint32_t joaatHashBuf (const void *buf, size_t len, uint8_t orbyte) {
  uint32_t hash = 0x29a;
  const uint8_t *s = (const uint8_t *)buf;
  while (len--) {
    hash += (*s++)|orbyte;
    hash += hash<<10;
    hash ^= hash>>6;
  }
  // finalize
  hash += hash<<3;
  hash ^= hash>>11;
  hash += hash<<15;
  return hash;
}


// this converts ASCII capitals to locase (and destroys other, but who cares)
#define joaatHashBufCI(buf_,len_)  joaatHashBuf((buf_), (len_), 0x20)


//==========================================================================
//
//  toUpper
//
//==========================================================================
UFO_FORCE_INLINE char toUpper (char ch) {
  return (ch >= 'a' && ch <= 'z' ? ch-'a'+'A' : ch);
}


//==========================================================================
//
//  toUpperU8
//
//==========================================================================
UFO_FORCE_INLINE uint8_t toUpperU8 (uint8_t ch) {
  return (ch >= 'a' && ch <= 'z' ? ch-'a'+'A' : ch);
}


//==========================================================================
//
//  digitInBase
//
//==========================================================================
UFO_FORCE_INLINE int digitInBase (char ch, int base) {
  switch (ch) {
    case '0' ... '9': ch = ch - '0'; break;
    case 'A' ... 'Z': ch = ch - 'A' + 10; break;
    case 'a' ... 'z': ch = ch - 'a' + 10; break;
    default: base = -1; break;
  }
  return (ch >= 0 && ch < base ? ch : -1);
}


/*
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; word header format:
;; note than name hash is ALWAYS calculated with ASCII-uppercased name
;; (actually, bit 5 is always reset for all bytes, because we don't need the
;; exact uppercase, only something that resembles it)
;; bfa points to next bfa or to 0 (this is "hash bucket pointer")
;; before nfa, we have such "hidden" fields:
;;   dd xfa       ; points to the previous word header XFA, regardless of vocabularies (or 0)
;;   dd yfa       ; points to the next word header YFA, regardless of vocabularies (or 0)
;;   dd bfa       ; next word in hashtable bucket; it is always here, even if hashtable is turned off
;;                ; if there is no hashtable, this field is not used
;; lfa:
;;   dd lfa       ; previous vocabulary word LFA or 0 (lfa links points here)
;;   dd namehash  ; it is always here, and always calculated, even if hashtable is turned off
;; nfa:
;;   dd flags-and-name-len   ; see below
;;   db name      ; no terminating zero or other "termination flag" here
;;  here could be some 0 bytes to align everything to 4 bytes
;;   db namelen   ; yes, name length again, so CFA->NFA can avoid guessing
;;                ; full length, including padding, but not including this byte
;; cfa:
;;   dd cfaidx    ; our internal CFA index, or image address for DOES>
;;   dd ?         ; reserved for "does"
;; pfa:
;;   word data follows
;;
;; first word cell contains combined name length (low byte), argtype and flags (other bytes)
;; layout:
;;   db namelen
;;   db argtype
;;   dw flags
;; i.e. we have 16 bits for flags, and 256 possible argument types. why not.
;;
;; flags:
;;  bit 0: immediate
;;  bit 1: smudge
;;  bit 2: noreturn
;;  bit 3: hidden
;;  bit 4: codeblock
;;  bit 5: vocabulary
;;  bit 6: *UNUSED* main scattered colon word (with "...")
;;  bit 7: protected
;;  bit 8: conditional branch (has sense only for words with branch address)
;;  bit 9: may return, but may not (unreliable flag ;-)
;;
;;  argtype is the type of the argument that this word reads from the threaded code.
;;  possible argument types:
;;    0: none
;;    1: branch address
;;    2: cell-size numeric literal
;;    3: cell-counted string with terminating zero (not counted)
;;    4: cfa of another word
;;    5: cblock
;;    6: vocid
;;    7: byte-counted string with terminating zero (not counted)
;;    8: data skip: the arg is amout of bytes to skip (not including the counter itself)
;;    9: pfa address


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; wordlist structure (at PFA)
;; -4: wordlist type id (used by structs, for example)
;;   dd latest
;;   dd voclink (voclink always points here)
;;   dd parent (if not zero, all parent words are visible)
;;   dd header-nfa (can be 0 for anonymous wordlists)
;;   hashtable (if enabled), or ~0U if no hash table
*/


// ////////////////////////////////////////////////////////////////////////// //
#define UFO_BFA_TO_LFA(bfa_)    ((bfa_) + 1u * 4u)
#define UFO_LFA_TO_XFA(lfa_)    ((lfa_) - 3u * 4u)
#define UFO_LFA_TO_YFA(lfa_)    ((lfa_) - 2u * 4u)
#define UFO_LFA_TO_BFA(lfa_)    ((lfa_) - 1u * 4u)
#define UFO_LFA_TO_NFA(lfa_)    ((lfa_) + 2u * 4u)
#define UFO_LFA_TO_CFA(lfa_)    UFO_ALIGN4((lfa_) + 3u * 4u + 1u + ufoImgGetU8((lfa_) + 2u * 4u))
#define UFO_LFA_TO_NHASH(lfa_)  ((lfa_) + 1u * 4u)
#define UFO_CFA_TO_NFA(cfa_)    ((cfa_) - 1u - 4u - ufoImgGetU8((cfa_) - 1u))
#define UFO_CFA_TO_LFA(cfa_)    ((cfa_) - 1u - 4u * 3u - ufoImgGetU8((cfa_) - 1u))
#define UFO_CFA_TO_PFA(cfa_)    ((cfa_) + 2u * 4u)
#define UFO_PFA_TO_CFA(pfa_)    ((pfa_) - 2u * 4u)
#define UFO_NFA_TO_CFA(nfa_)    UFO_ALIGN4((nfa_) + 4u + 1u + ufoImgGetU8((nfa_)))
#define UFO_NFA_TO_LFA(nfa_)    ((nfa_) - 2u * 4u)
#define UFO_XFA_TO_YFA(xfa_)    ((xfa_) + 4u)
#define UFO_YFA_TO_XFA(yfa_)    ((xfa_) - 4u)
#define UFO_YFA_TO_WST(yfa_)    ((yfa_) - 4u)  /* to xfa */
#define UFO_YFA_TO_NFA(yfa_)    ((yfa_) + 4u * 4u)

#define UFO_CFA_TO_DOES_CFA(cfa_)  ((cfa_) + 4u)
#define UFO_PFA_TO_DOES_CFA(pfa_)  ((pfa_) - 4u)


// ////////////////////////////////////////////////////////////////////////// //
#define UFW_VOCAB_OFS_LATEST   (0u * 4u)
#define UFW_VOCAB_OFS_VOCLINK  (1u * 4u)
#define UFW_VOCAB_OFS_PARENT   (2u * 4u)
#define UFW_VOCAB_OFS_HEADER   (3u * 4u)
#define UFW_VOCAB_OFS_HTABLE   (4u * 4u)

#define UFO_HASHTABLE_SIZE  (256)

#define UFO_NO_HTABLE_FLAG  (~(uint32_t)0)

#define UFO_MAX_NATIVE_CFAS  (1024u)
static ufoNativeCFA ufoForthCFAs[UFO_MAX_NATIVE_CFAS];
static uint32_t ufoCFAsUsed = 0;

static uint32_t ufoDoForthCFA;
static uint32_t ufoDoVariableCFA;
static uint32_t ufoDoValueCFA;
static uint32_t ufoDoConstCFA;
static uint32_t ufoDoDeferCFA;
static uint32_t ufoDoDoesCFA;
static uint32_t ufoDoRedirectCFA;
static uint32_t ufoDoVocCFA;
static uint32_t ufoDoCreateCFA;
static uint32_t ufoDoUserVariableCFA;

static uint32_t ufoLitStr8CFA;

#ifdef UFO_MTASK_ALLOWED
static uint32_t ufoSingleStepAllowed;
#endif

// special address types:
#define UFO_ADDR_CFA_BIT   ((uint32_t)1<<31)
#define UFO_ADDR_CFA_MASK  (UFO_ADDR_CFA_BIT-1u)

// handles are somewhat special: first 12 bits can be used as offset for "@", and are ignored
#define UFO_ADDR_HANDLE_BIT       ((uint32_t)1<<30)
#define UFO_ADDR_HANDLE_MASK      ((UFO_ADDR_HANDLE_BIT-1u)&~((uint32_t)0xfff))
#define UFO_ADDR_HANDLE_SHIFT     (12)
#define UFO_ADDR_HANDLE_OFS_MASK  ((uint32_t)((1 << UFO_ADDR_HANDLE_SHIFT) - 1))

// temporary area is 1MB buffer out of the main image
#define UFO_ADDR_TEMP_BIT   ((uint32_t)1<<29)
#define UFO_ADDR_TEMP_MASK  (UFO_ADDR_TEMP_BIT-1u)

#define UFO_ADDR_SPECIAL_BITS_MASK  (UFO_ADDR_CFA_BIT|UFO_ADDR_HANDLE_BIT|UFO_ADDR_TEMP_BIT)


#ifdef UFO_HUGE_IMAGES
#define  ufoImageSize (1024u * 1024u * 64u)
static uint32_t ufoImage[ufoImageSize / 4u];
#else
static uint32_t *ufoImage = NULL;
static uint32_t ufoImageSize = 0;
#endif

static uint8_t *ufoDebugImage = NULL;
static uint32_t ufoDebugImageUsed = 0; // in bytes
static uint32_t ufoDebugImageSize = 0; // in bytes
static uint32_t ufoDebugFileNameHash = 0; // current file name hash
static uint32_t ufoDebugFileNameLen = 0; // current file name length
static uint32_t ufoDebugLastLine = 0;
static uint32_t ufoDebugLastLinePCOfs = 0;
static uint32_t ufoDebugLastLineDP = 0;
static uint32_t ufoDebugCurrDP = 0;

static uint32_t ufoInRunWord = 0;

//static volatile int ufoVMAbort = 0;

#define ufoTrueValue  (~(uint32_t)0)

enum {
  UFO_MODE_NONE = -1,
  UFO_MODE_NATIVE = 0, // executing forth code
  UFO_MODE_MACRO = 1, // executing forth asm macro
};
static uint32_t ufoMode = UFO_MODE_NONE;

#define UFO_DSTACK_SIZE    (8192)
#define UFO_RSTACK_SIZE    (4096)
#define UFO_LSTACK_SIZE    (4096)
#define UFO_MAX_TASK_NAME  (127)
#define UFO_VOCSTACK_SIZE  (16u)

#define UFO_MAX_TEMP_IMAGE  (1024u * 1024u * 8u)

// to support multitasking (required for the debugger),
// our virtual machine state is encapsulated in a struct.
typedef struct UfoState_t {
  uint32_t id;
  uint32_t dStack[UFO_DSTACK_SIZE];
  uint32_t rStack[UFO_RSTACK_SIZE];
  uint32_t lStack[UFO_LSTACK_SIZE];
  uint32_t IP; // in image
  uint32_t SP; // points AFTER the last value pushed
  uint32_t RP; // points AFTER the last value pushed
  // address register
  uint32_t regA;
  // for locals
  uint32_t LP;
  uint32_t LBP;
  // vocstack
  uint32_t vocStack[UFO_VOCSTACK_SIZE]; // cfas
  uint32_t VSP;
  // temp image
  #ifdef UFO_HUGE_IMAGES
  uint32_t imageTemp[UFO_MAX_TEMP_IMAGE / 4u];
  #else
  uint32_t *imageTemp;
  uint32_t imageTempSize;
  #endif
  // linked list of all allocated states (tasks)
  char name[UFO_MAX_TASK_NAME + 1];
} UfoState;

// 'cmon!
#define UFO_MAX_STATES  (8192)

#ifdef UFO_MTASK_ALLOWED
// this is indexed by id
static UfoState *ufoStateMap[UFO_MAX_STATES] = {NULL};
static uint32_t ufoStateUsedBitmap[UFO_MAX_STATES/32] = {0};

// currently active execution state
static UfoState *ufoCurrState = NULL;
// state we're yielded from
static UfoState *ufoYieldedState = NULL;
// if debug state is not NULL, VM will switch to it
// after executing one instruction from the current state.
// it will store current state in `ufoDebugeeState`.
static UfoState *ufoDebuggerState = NULL;
static uint32_t ufoSingleStep = 0;

#define ufoDStack         (ufoCurrState->dStack)
#define ufoRStack         (ufoCurrState->rStack)
#define ufoLStack         (ufoCurrState->lStack)
#define ufoIP             (ufoCurrState->IP)
#define ufoSP             (ufoCurrState->SP)
#define ufoRP             (ufoCurrState->RP)
#define ufoLP             (ufoCurrState->LP)
#define ufoLBP            (ufoCurrState->LBP)
#define ufoRegA           (ufoCurrState->regA)
#define ufoImageTemp      (ufoCurrState->imageTemp)
#ifdef UFO_HUGE_IMAGES
# define ufoImageTempSize  UFO_MAX_TEMP_IMAGE
# define ufoSTImageTempSize(st_)  UFO_MAX_TEMP_IMAGE
#else
# define ufoImageTempSize  (ufoCurrState->imageTempSize)
# define ufoSTImageTempSize(st_)  ((st_)->imageTempSize)
#endif
#define ufoVocStack       (ufoCurrState->vocStack)
#define ufoVSP            (ufoCurrState->VSP)

#else /* no multitasking */

static UfoState ufoCurrState;

#define ufoDStack         (ufoCurrState.dStack)
#define ufoRStack         (ufoCurrState.rStack)
#define ufoLStack         (ufoCurrState.lStack)
#define ufoIP             (ufoCurrState.IP)
#define ufoSP             (ufoCurrState.SP)
#define ufoRP             (ufoCurrState.RP)
#define ufoLP             (ufoCurrState.LP)
#define ufoLBP            (ufoCurrState.LBP)
#define ufoRegA           (ufoCurrState.regA)
#define ufoImageTemp      (ufoCurrState.imageTemp)
#ifdef UFO_HUGE_IMAGES
# define ufoImageTempSize  UFO_MAX_TEMP_IMAGE
# define ufoSTImageTempSize(st_)  UFO_MAX_TEMP_IMAGE
#else
# define ufoImageTempSize  (ufoCurrState.imageTempSize)
# define ufoSTImageTempSize(st_)  ((st_)->imageTempSize)
#endif
#define ufoVocStack       (ufoCurrState.vocStack)
#define ufoVSP            (ufoCurrState.VSP)

#endif

static jmp_buf ufoStopVMJP;

// 256 bytes for user variables
#define UFO_USER_AREA_ADDR  UFO_ADDR_TEMP_BIT
#define UFO_USER_AREA_SIZE  (256u)
#define UFO_NBUF_ADDR       UFO_USER_AREA_ADDR + UFO_USER_AREA_SIZE
#define UFO_NBUF_SIZE       (256u)
#define UFO_PAD_ADDR        (UFO_NBUF_ADDR + UFO_NBUF_SIZE)
#define UFO_DEF_TIB_ADDR    (UFO_PAD_ADDR + 2048u)

// dynamically allocated text input buffer
// always ends with zero (this is word name too)
static const uint32_t ufoAddrTIBx = UFO_ADDR_TEMP_BIT + 0u * 4u; // TIB
static const uint32_t ufoAddrINx = UFO_ADDR_TEMP_BIT + 1u * 4u; // >IN
static const uint32_t ufoAddrDefTIB = UFO_ADDR_TEMP_BIT + 2u * 4u; // default TIB (handle); user cannot change it
static const uint32_t ufoAddrBASE = UFO_ADDR_TEMP_BIT + 3u * 4u;
static const uint32_t ufoAddrSTATE = UFO_ADDR_TEMP_BIT + 4u * 4u;
static const uint32_t ufoAddrContext = UFO_ADDR_TEMP_BIT + 5u * 4u; // CONTEXT
static const uint32_t ufoAddrCurrent = UFO_ADDR_TEMP_BIT + 6u * 4u; // CURRENT (definitions will go there)
static const uint32_t ufoAddrSelf = UFO_ADDR_TEMP_BIT + 7u * 4u; // CURRENT (definitions will go there)
static const uint32_t ufoAddrInterNextLine = UFO_ADDR_TEMP_BIT + 8u * 4u; // (INTERPRET-NEXT-LINE)
static const uint32_t ufoAddrEP = UFO_ADDR_TEMP_BIT + 9u * 4u; // (EP) -- exception frame pointer
static const uint32_t ufoAddrDPTemp = UFO_ADDR_TEMP_BIT + 10u * 4u; // pointer to currently active DP in temp dict
static const uint32_t ufoAddrHereDP = UFO_ADDR_TEMP_BIT + 11u * 4u; // pointer to currently active DP for HERE
static const uint32_t ufoAddrUserVarUsed = UFO_ADDR_TEMP_BIT + 12u * 4u;

#define UFO_DPTEMP_BASE_ADDR  (UFO_ADDR_TEMP_BIT + 256u * 1024u)

static uint32_t ufoAddrVocLink;
static uint32_t ufoAddrDP; // DP for main dict
static uint32_t ufoAddrNewWordFlags;
static uint32_t ufoAddrRedefineWarning;
static uint32_t ufoAddrLastXFA;

static uint32_t ufoForthVocId;
static uint32_t ufoCompilerVocId;
static uint32_t ufoInterpNextLineCFA;

static uint32_t ufoUserAbortCFA;

// allows to redefine even protected words
#define UFO_REDEF_WARN_DONT_CARE  (~(uint32_t)0)
// do not warn about ordinary words, allow others
#define UFO_REDEF_WARN_NONE    (0)
// do warn (or fail on protected)
#define UFO_REDEF_WARN_NORMAL  (1)
// do warn (or fail on protected) for parent dicts too
#define UFO_REDEF_WARN_PARENTS (2)

#define UFO_GET_DP()      (ufoImgGetU32(ufoImgGetU32(ufoAddrHereDP)))

#define UFO_MAX_NESTED_INCLUDES  (32)
typedef struct {
  FILE *fl;
  char *fname;
  char *incpath;
  char *sysincpath;
  int fline;
  uint32_t id; // non-zero unique id
} UFOFileStackEntry;

static UFOFileStackEntry ufoFileStack[UFO_MAX_NESTED_INCLUDES];
static uint32_t ufoFileStackPos; // after the last used item

static FILE *ufoInFile = NULL;
static uint32_t ufoInFileNameLen = 0;
static uint32_t ufoInFileNameHash = 0;
static char *ufoInFileName = NULL;
static char *ufoLastIncPath = NULL;
static char *ufoLastSysIncPath = NULL;
static int ufoInFileLine = 0;
static uint32_t ufoFileId = 0;
static uint32_t ufoLastUsedFileId = 0;
static int ufoLastEmitWasCR = 1;
static long ufoCurrIncludeLineFileOfs = 0;

// dynamic memory handles
typedef struct UHandleInfo_t {
  uint32_t ufoHandle;
  uint32_t typeid;
  uint8_t *data;
  uint32_t size;
  uint32_t used;
  // in free list
  struct UHandleInfo_t *next;
} UfoHandle;

static UfoHandle *ufoHandleFreeList = NULL;
static UfoHandle **ufoHandles = NULL;
static uint32_t ufoHandlesUsed = 0;
static uint32_t ufoHandlesAlloted = 0;

#define UFO_HANDLE_FREE  (~(uint32_t)0)

static char ufoCurrFileLine[520];

// for `ufoFatal()`
static uint32_t ufoInBacktrace = 0;


// ////////////////////////////////////////////////////////////////////////// //
static void ufoClearCondDefines (void);

static void ufoBacktrace (uint32_t ip, int showDataStack);
static void ufoBTShowWordName (uint32_t nfa);

static void ufoClearCondDefines (void);

#ifdef UFO_MTASK_ALLOWED
static UfoState *ufoNewState (void);
static void ufoFreeState (UfoState *st);
static UfoState *ufoFindState (uint32_t stid);
static void ufoSwitchToState (UfoState *newst);
#endif
static void ufoInitStateUserVars (UfoState *st);

__attribute__((unused)) static void ufoDumpWordHeader (const uint32_t lfa);

#ifndef WIN32
static void ufoDisableRaw (void);
#endif
static void ufoTTYRawFlush (void);
static int ufoIsGoodTTY (void);

#ifdef UFO_DEBUG_DEBUG
static void ufoDumpDebugImage (void);
#endif


// ////////////////////////////////////////////////////////////////////////// //
#ifdef UFO_MTASK_ALLOWED
#define UFO_EXEC_CFA(cfa_)  do { \
  const uint32_t cfa = (cfa_); \
  if (ufoCurrState == NULL) ufoFatal("execution state is lost"); \
  const uint32_t cfaidx = ufoImgGetU32(cfa); \
  if (cfaidx >= UFO_ADDR_CFA_BIT && cfaidx < UFO_MAX_NATIVE_CFAS + UFO_ADDR_CFA_BIT) { \
    ufoForthCFAs[cfaidx & UFO_ADDR_CFA_MASK](UFO_CFA_TO_PFA(cfa)); \
  } else { \
    ufoFatal("tried to execute an unknown word: %u (max is %u); IP=%u", cfaidx, ufoCFAsUsed, ufoIP - 4u); \
  } \
  /* that's all we need to activate the debugger */ \
  if (ufoSingleStep) { \
    ufoSingleStep -= 1; \
    if (ufoSingleStep == 0 && ufoDebuggerState != NULL) { \
      if (ufoCurrState == ufoDebuggerState) ufoFatal("debugger cannot debug itself"); \
      UfoState *ost = ufoCurrState; \
      ufoSwitchToState(ufoDebuggerState); /* always use API call for this! */ \
      ufoPush(-2); \
      ufoPush(ost->id); \
    } \
  } \
} while (0)

#else

#if 0
# define UFO_EXEC_CFA_DEBUG  do { \
  fprintf(stderr, "IP:%08X CFA:%08X (CFA):%08X\n", ufoIP, xxcfa, xxcfaidx); \
  uint32_t nfa = ufoFindWordForIP(ufoIP - 4u); \
  if (nfa != 0) { \
    fprintf(stderr, "  IP: "); ufoBTShowWordName(nfa); \
    /*fname = ufoFindFileForIP(ip, &fline, NULL, NULL);*/ \
    /*if (fname != NULL) { fprintf(stderr, " (at %s:%u)", fname, fline); }*/ \
    fputc('\n', stderr); \
  } \
  nfa = ufoFindWordForIP(xxcfa); \
  if (nfa != 0) { \
    fprintf(stderr, "  CFA:"); ufoBTShowWordName(nfa); \
    /*fname = ufoFindFileForIP(ip, &fline, NULL, NULL);*/ \
    /*if (fname != NULL) { fprintf(stderr, " (at %s:%u)", fname, fline); }*/ \
    fputc('\n', stderr); \
  } \
} while (0);
#else
# define UFO_EXEC_CFA_DEBUG
#endif

#define UFO_EXEC_CFA(cfa__)  do { \
  const uint32_t xxcfa = (cfa__); \
  const uint32_t xxcfaidx = ufoImgGetU32(xxcfa); \
  UFO_EXEC_CFA_DEBUG \
  if (xxcfaidx >= UFO_ADDR_CFA_BIT && xxcfaidx < UFO_MAX_NATIVE_CFAS + UFO_ADDR_CFA_BIT) { \
    ufoForthCFAs[xxcfaidx & UFO_ADDR_CFA_MASK](UFO_CFA_TO_PFA(xxcfa)); \
  } else { \
    ufoFatal("tried to execute an unknown word: %u (max is %u); IP=%u; CFA=%u", \
             xxcfaidx, ufoCFAsUsed, ufoIP - 4u, xxcfa); \
  } \
} while (0)

#endif


// ////////////////////////////////////////////////////////////////////////// //
#define UFWORD(name_) \
static void ufoWord_##name_ (uint32_t mypfa)

#define UFCALL(name_)  ufoWord_##name_(0)
#define UFCFA(name_)   (&ufoWord_##name_)

// for TIB words
UFWORD(CPOKE_REGA_IDX);

// for peek and poke
UFWORD(PAR_HANDLE_LOAD_BYTE);
UFWORD(PAR_HANDLE_LOAD_WORD);
UFWORD(PAR_HANDLE_LOAD_CELL);
UFWORD(PAR_HANDLE_STORE_BYTE);
UFWORD(PAR_HANDLE_STORE_WORD);
UFWORD(PAR_HANDLE_STORE_CELL);


//==========================================================================
//
//  ufoFlushOutput
//
//==========================================================================
static void ufoFlushOutput (void) {
  ufoTTYRawFlush();
  fflush(NULL);
}


//==========================================================================
//
//  ufoSetInFileName
//
//  if `reuse` is not 0, reuse/free `fname`
//
//==========================================================================
static void ufoSetInFileNameEx (const char *fname, int reuse) {
  ufo_assert(fname == NULL || (fname != ufoInFileName));
  if (fname == NULL || fname[0] == 0) {
    if (ufoInFileName) { free(ufoInFileName); ufoInFileName = NULL; }
    ufoInFileNameLen = 0;
    ufoInFileNameHash = 0;
    if (reuse && fname != NULL) free((void *)fname);
  } else {
    const uint32_t fnlen = (uint32_t)strlen(fname);
    const uint32_t fnhash = joaatHashBuf(fname, fnlen, 0);
    if (ufoInFileNameLen != fnlen || ufoInFileNameHash != fnhash) {
      if (ufoInFileName) { free(ufoInFileName); ufoInFileName = NULL; }
      if (reuse) {
        ufoInFileName = (char *)fname;
      } else {
        ufoInFileName = strdup(fname);
        if (ufoInFileName == NULL) ufoFatal("out of memory for filename info");
      }
      ufoInFileNameLen = fnlen;
      ufoInFileNameHash = fnhash;
    } else {
      if (reuse && fname != NULL) free((void *)fname);
    }
  }
}


//==========================================================================
//
//  ufoSetInFileName
//
//==========================================================================
UFO_FORCE_INLINE void ufoSetInFileName (const char *fname) {
  ufoSetInFileNameEx(fname, 0);
}


//==========================================================================
//
//  ufoSetInFileNameReuse
//
//==========================================================================
UFO_FORCE_INLINE void ufoSetInFileNameReuse (const char *fname) {
  ufoSetInFileNameEx(fname, 1);
}


//==========================================================================
//
//  ufoAllocHandle
//
//==========================================================================
static UfoHandle *ufoAllocHandle (uint32_t typeid) {
  ufo_assert(typeid != UFO_HANDLE_FREE);
  UfoHandle *newh = ufoHandleFreeList;
  if (newh == NULL) {
    if (ufoHandlesUsed == ufoHandlesAlloted) {
      uint32_t newsz = ufoHandlesAlloted + 16384;
      // due to offsets, this is the maximum number of handles we can have
      if (newsz > 0x1ffffU) {
        if (ufoHandlesAlloted > 0x1ffffU) ufoFatal("too many dynamic handles");
        newsz = 0x1ffffU + 1U;
        ufo_assert(newsz > ufoHandlesAlloted);
      }
      UfoHandle **nh = realloc(ufoHandles, sizeof(ufoHandles[0]) * newsz);
      if (nh == NULL) ufoFatal("out of memory for handle table");
      ufoHandles = nh;
      ufoHandlesAlloted = newsz;
    }
    newh = calloc(1, sizeof(UfoHandle));
    if (newh == NULL) ufoFatal("out of memory for handle info");
    ufoHandles[ufoHandlesUsed] = newh;
    // setup new handle info
    newh->ufoHandle = (ufoHandlesUsed << UFO_ADDR_HANDLE_SHIFT) | UFO_ADDR_HANDLE_BIT;
    ufoHandlesUsed += 1;
  } else {
    ufo_assert(newh->typeid == UFO_HANDLE_FREE);
    ufoHandleFreeList = newh->next;
  }
  // setup new handle info
  newh->typeid = typeid;
  newh->data = NULL;
  newh->size = 0;
  newh->used = 0;
  newh->next = NULL;
  return newh;
}


//==========================================================================
//
//  ufoFreeHandle
//
//==========================================================================
static void ufoFreeHandle (UfoHandle *hh) {
  if (hh != NULL) {
    ufo_assert(hh->typeid != UFO_HANDLE_FREE);
    if (hh->data) free(hh->data);
    hh->typeid = UFO_HANDLE_FREE;
    hh->data = NULL;
    hh->size = 0;
    hh->used = 0;
    hh->next = ufoHandleFreeList;
    ufoHandleFreeList = hh;
  }
}


//==========================================================================
//
//  ufoGetHandle
//
//==========================================================================
static UfoHandle *ufoGetHandle (uint32_t hh) {
  UfoHandle *res;
  if (hh != 0 && (hh & UFO_ADDR_HANDLE_BIT) != 0) {
    hh = (hh & UFO_ADDR_HANDLE_MASK) >> UFO_ADDR_HANDLE_SHIFT;
    if (hh < ufoHandlesUsed) {
      res = ufoHandles[hh];
      if (res->typeid == UFO_HANDLE_FREE) res = NULL;
    } else {
      res = NULL;
    }
  } else {
    res = NULL;
  }
  return res;
}


#define POP_PREPARE_HANDLE_XX() \
  if ((hx & UFO_ADDR_HANDLE_BIT) == 0) ufoFatal("not a handle"); \
  if (idx > 0x1fffffffU - UFO_ADDR_HANDLE_OFS_MASK) ufoFatal("handle index %u out of range", idx); \
  idx += (hx & UFO_ADDR_HANDLE_OFS_MASK); \
  UfoHandle *hh = ufoGetHandle(hx); \
  if (hh == NULL) ufoFatal("invalid handle")

UFO_DISABLE_INLINE uint32_t ufoHandleLoadByte (uint32_t hx, uint32_t idx) {
  POP_PREPARE_HANDLE_XX();
  if (idx >= hh->size) ufoFatal("handle index %u out of range (%u)", idx, hh->size);
  return hh->data[idx];
}

UFO_DISABLE_INLINE uint32_t ufoHandleLoadWord (uint32_t hx, uint32_t idx) {
  POP_PREPARE_HANDLE_XX();
  if (idx >= hh->size || hh->size - idx < 2u) {
    ufoFatal("handle index %u out of range (%u)", idx, hh->size);
  }
  #ifdef UFO_FAST_MEM_ACCESS
  return *(const uint16_t *)(hh->data + idx);
  #else
  uint32_t res = hh->data[idx];
  res |= hh->data[idx + 1u] << 8;
  return res;
  #endif
}

UFO_DISABLE_INLINE uint32_t ufoHandleLoadCell (uint32_t hx, uint32_t idx) {
  POP_PREPARE_HANDLE_XX();
  if (idx >= hh->size || hh->size - idx < 4u) {
    ufoFatal("handle index %u out of range (%u)", idx, hh->size);
  }
  #ifdef UFO_FAST_MEM_ACCESS
  return *(const uint32_t *)(hh->data + idx);
  #else
  uint32_t res = hh->data[idx];
  res |= hh->data[idx + 1u] << 8;
  res |= hh->data[idx + 2u] << 16;
  res |= hh->data[idx + 3u] << 24;
  return res;
  #endif
}

UFO_DISABLE_INLINE void ufoHandleStoreByte (uint32_t hx, uint32_t idx, uint32_t value) {
  POP_PREPARE_HANDLE_XX();
  if (idx >= hh->size) ufoFatal("handle index %u out of range (%u)", idx, hh->size);
  hh->data[idx] = (uint8_t)value;
}

UFO_DISABLE_INLINE void ufoHandleStoreWord (uint32_t hx, uint32_t idx, uint32_t value) {
  POP_PREPARE_HANDLE_XX();
  if (idx >= hh->size || hh->size - idx < 2u) {
    ufoFatal("handle index %u out of range (%u)", idx, hh->size);
  }
  #ifdef UFO_FAST_MEM_ACCESS
  *(uint16_t *)(hh->data + idx) = (uint16_t)value;
  #else
  hh->data[idx] = (uint8_t)value;
  hh->data[idx + 1u] = (uint8_t)(value >> 8);
  #endif
}

UFO_DISABLE_INLINE void ufoHandleStoreCell (uint32_t hx, uint32_t idx, uint32_t value) {
  POP_PREPARE_HANDLE_XX();
  if (idx >= hh->size || hh->size - idx < 4u) {
    ufoFatal("handle index %u out of range (%u)", idx, hh->size);
  }
  #ifdef UFO_FAST_MEM_ACCESS
  *(uint32_t *)(hh->data + idx) = value;
  #else
  hh->data[idx] = (uint8_t)value;
  hh->data[idx + 1u] = (uint8_t)(value >> 8);
  hh->data[idx + 2u] = (uint8_t)(value >> 16);
  hh->data[idx + 3u] = (uint8_t)(value >> 24);
  #endif
}


//==========================================================================
//
//  setLastIncPath
//
//==========================================================================
static void setLastIncPath (const char *fname, int system) {
  if (fname == NULL || fname[0] == 0) {
    if (system) {
      if (ufoLastSysIncPath) free(ufoLastIncPath);
      ufoLastSysIncPath = NULL;
    } else {
      if (ufoLastIncPath) free(ufoLastIncPath);
      ufoLastIncPath = strdup(".");
    }
  } else {
    char *lslash;
    char *cpos;
    if (system) {
      if (ufoLastSysIncPath) free(ufoLastSysIncPath);
      ufoLastSysIncPath = strdup(fname);
      lslash = ufoLastSysIncPath;
      cpos = ufoLastSysIncPath;
    } else {
      if (ufoLastIncPath) free(ufoLastIncPath);
      ufoLastIncPath = strdup(fname);
      lslash = ufoLastIncPath;
      cpos = ufoLastIncPath;
    }
    while (*cpos) {
      #ifdef WIN32
      if (*cpos == '/' || *cpos == '\\') lslash = cpos;
      #else
      if (*cpos == '/') lslash = cpos;
      #endif
      cpos += 1;
    }
    *lslash = 0;
  }
}


//==========================================================================
//
//  ufoClearIncludePath
//
//  required for UrAsm
//
//==========================================================================
void ufoClearIncludePath (void) {
  if (ufoLastIncPath != NULL) {
    free(ufoLastIncPath);
    ufoLastIncPath = NULL;
  }
  if (ufoLastSysIncPath != NULL) {
    free(ufoLastSysIncPath);
    ufoLastSysIncPath = NULL;
  }
}


//==========================================================================
//
//  ufoErrorPrintFile
//
//==========================================================================
static void ufoErrorPrintFile (FILE *fo, const char *errwarn) {
  if (ufoInFileName != NULL) {
    fprintf(fo, "UFO %s at file %s, line %d: ", errwarn, ufoInFileName, ufoInFileLine);
  } else {
    fprintf(fo, "UFO %s somewhere in time: ", errwarn);
  }
}


//==========================================================================
//
//  ufoErrorMsgV
//
//==========================================================================
static void ufoErrorMsgV (const char *errwarn, const char *fmt, va_list ap) {
  ufoFlushOutput();
  if (!ufoLastEmitWasCR) { printf("\n"); ufoLastEmitWasCR = 1; }
  ufoErrorPrintFile(stderr, errwarn);
  vfprintf(stderr, fmt, ap);
  va_end(ap);
  fputc('\n', stderr);
  ufoFlushOutput();
}


//==========================================================================
//
//  ufoWarning
//
//==========================================================================
__attribute__((format(printf, 1, 2)))
void ufoWarning (const char *fmt, ...) {
  va_list ap;
  va_start(ap, fmt);
  ufoErrorMsgV("WARNING", fmt, ap);
}


//==========================================================================
//
//  ufoFatal
//
//==========================================================================
__attribute__((noreturn)) __attribute__((format(printf, 1, 2)))
void ufoFatal (const char *fmt, ...) {
  va_list ap;
  #ifndef WIN32
  ufoDisableRaw();
  #endif
  va_start(ap, fmt);
  ufoErrorMsgV("ERROR", fmt, ap);
  if (!ufoInBacktrace) {
    ufoInBacktrace = 1;
    ufoBacktrace(ufoIP, 1);
    ufoInBacktrace = 0;
  } else {
    fprintf(stderr, "DOUBLE FATAL: error in backtrace!\n");
    abort();
  }
  #ifdef UFO_DEBUG_FATAL_ABORT
  abort();
  #endif
  // allow restart
  ufoInRunWord = 0;
  ufoFatalError();
}


// ////////////////////////////////////////////////////////////////////////// //
// working with the stacks
#define UFO_TOS    (ufoDStack[ufoSP - 1u])
#define UFO_RTOS   (ufoRStack[ufoRP - 1u])

#define UFO_S(n_)    (ufoDStack[ufoSP - 1u - (n_)])
#define UFO_R(n_)    (ufoRStack[ufoRP - 1u - (n_)])

#define UFO_STACK(n_)   if (ufoSP < (uint32_t)(n_)) ufoFatal("data stack underflow")
#define UFO_RSTACK(n_)  if (ufoRP < (uint32_t)(n_)) ufoFatal("return stack underflow")

UFO_FORCE_INLINE void ufoPush (uint32_t v) { if (ufoSP >= UFO_DSTACK_SIZE) ufoFatal("data stack overflow"); ufoDStack[ufoSP++] = v; }
UFO_FORCE_INLINE void ufoDrop (void) { if (ufoSP == 0) ufoFatal("data stack underflow"); ufoSP -= 1u; }
UFO_FORCE_INLINE uint32_t ufoPop (void) { if (ufoSP == 0) { ufoFatal("data stack underflow"); } return ufoDStack[--ufoSP]; }
UFO_FORCE_INLINE uint32_t ufoPeek (void) { if (ufoSP == 0) ufoFatal("data stack underflow"); return ufoDStack[ufoSP-1u]; }
UFO_FORCE_INLINE void ufoDup (void) { if (ufoSP == 0) ufoFatal("data stack underflow"); ufoPush(ufoDStack[ufoSP-1u]); }
UFO_FORCE_INLINE void ufoOver (void) { if (ufoSP < 2u) ufoFatal("data stack underflow"); ufoPush(ufoDStack[ufoSP-2u]); }
UFO_FORCE_INLINE void ufoSwap (void) { if (ufoSP < 2u) ufoFatal("data stack underflow"); const uint32_t t = ufoDStack[ufoSP-1u]; ufoDStack[ufoSP-1u] = ufoDStack[ufoSP-2u]; ufoDStack[ufoSP-2u] = t; }
UFO_FORCE_INLINE void ufoRot (void) { if (ufoSP < 3u) ufoFatal("data stack underflow"); const uint32_t t = ufoDStack[ufoSP-3u]; ufoDStack[ufoSP-3u] = ufoDStack[ufoSP-2u]; ufoDStack[ufoSP-2u] = ufoDStack[ufoSP-1u]; ufoDStack[ufoSP-1u] = t; }
UFO_FORCE_INLINE void ufoNRot (void) { if (ufoSP < 3u) ufoFatal("data stack underflow"); const uint32_t t = ufoDStack[ufoSP-1u]; ufoDStack[ufoSP-1u] = ufoDStack[ufoSP-2u]; ufoDStack[ufoSP-2u] = ufoDStack[ufoSP-3u]; ufoDStack[ufoSP-3u] = t; }

UFO_FORCE_INLINE void ufo2Dup (void) { ufoOver(); ufoOver(); }
UFO_FORCE_INLINE void ufo2Drop (void) { UFO_STACK(2); ufoSP -= 2u; }
UFO_FORCE_INLINE void ufo2Over (void) { if (ufoSP < 4u) ufoFatal("data stack underflow"); const uint32_t n0 = ufoDStack[ufoSP-4u]; const uint32_t n1 = ufoDStack[ufoSP-3u]; ufoPush(n0); ufoPush(n1); }
UFO_FORCE_INLINE void ufo2Swap (void) { if (ufoSP < 4u) ufoFatal("data stack underflow"); const uint32_t n0 = ufoDStack[ufoSP-4u]; const uint32_t n1 = ufoDStack[ufoSP-3u]; ufoDStack[ufoSP-4u] = ufoDStack[ufoSP-2u]; ufoDStack[ufoSP-3u] = ufoDStack[ufoSP-1u]; ufoDStack[ufoSP-2u] = n0; ufoDStack[ufoSP-1u] = n1; }

UFO_FORCE_INLINE void ufoRPush (uint32_t v) { if (ufoRP >= UFO_RSTACK_SIZE) ufoFatal("return stack overflow"); ufoRStack[ufoRP++] = v; }
UFO_FORCE_INLINE void ufoRDrop (void) { if (ufoRP == 0) ufoFatal("return stack underflow"); --ufoRP; }
UFO_FORCE_INLINE uint32_t ufoRPop (void) { if (ufoRP == 0) ufoFatal("return stack underflow"); return ufoRStack[--ufoRP]; }
UFO_FORCE_INLINE uint32_t ufoRPeek (void) { if (ufoRP == 0) ufoFatal("return stack underflow"); return ufoRStack[ufoRP-1u]; }
UFO_FORCE_INLINE void ufoRDup (void) { if (ufoRP == 0) ufoFatal("return stack underflow"); ufoPush(ufoRStack[ufoRP-1u]); }

UFO_FORCE_INLINE void ufoPushBool (int v) { ufoPush(v ? ufoTrueValue : 0u); }


#ifndef UFO_HUGE_IMAGES
//==========================================================================
//
//  ufoImgEnsureSize
//
//==========================================================================
static void ufoImgEnsureSize (uint32_t addr) {
  if ((addr & UFO_ADDR_SPECIAL_BITS_MASK) != 0) ufoFatal("ufoImgEnsureSize: internal error");
  if (addr >= ufoImageSize) {
    // 64MB should be enough for everyone!
    if (addr >= 0x04000000U) {
      ufoFatal("image grown too big (addr=0%08XH)", addr);
    }
    const uint32_t osz = ufoImageSize;
    // grow by 1MB steps
    const uint32_t nsz = (addr|0x000fffffU) + 1U;
    ufo_assert(nsz > addr);
    uint32_t *nimg = realloc(ufoImage, nsz);
    if (nimg == NULL) {
      ufoFatal("out of memory for UFO image (%u -> %u MBs)",
               ufoImageSize / 1024u / 1024u,
               nsz / 1024u / 1024u);
    }
    ufoImage = nimg;
    ufoImageSize = nsz;
    memset((char *)ufoImage + osz, 0, (nsz - osz));
  }
}


//==========================================================================
//
//  ufoImgEnsureTemp
//
//==========================================================================
static void ufoImgEnsureTemp (uint32_t addr) {
  if ((addr & UFO_ADDR_SPECIAL_BITS_MASK) != 0) ufoFatal("ufoImgEnsureTemp: internal error");
  if (addr >= ufoImageTempSize) {
    if (addr >= 1024u * 1024u) {
      ufoFatal("Forth segmentation fault at address 0x%08X", addr | UFO_ADDR_TEMP_BIT);
    }
    const uint32_t osz = ufoImageTempSize;
    // grow by 64KB steps
    const uint32_t nsz = (addr|0x0000ffffU) + 1U;
    uint32_t *nimg = realloc(ufoImageTemp, nsz);
    if (nimg == NULL) {
      ufoFatal("out of memory for temp UFO image (%u -> %u KBs)",
               ufoImageTempSize / 1024u,
               nsz / 1024u);
    }
    ufoImageTemp = nimg;
    ufoImageTempSize = nsz;
    memset((char *)ufoImageTemp + osz, 0, (nsz - osz));
  }
}
#endif


#ifdef UFO_FAST_MEM_ACCESS
//==========================================================================
//
//  ufoImgPutU8
//
//  fast
//
//==========================================================================
UFO_FORCE_INLINE void ufoImgPutU8 (uint32_t addr, const uint32_t value) {
  if ((addr & UFO_ADDR_SPECIAL_BITS_MASK) == 0) {
    if (addr >= ufoImageSize) {
      #ifdef UFO_HUGE_IMAGES
      ufoFatal("Forth segmentation fault at address 0x%08X", addr);
      #else
      ufoImgEnsureSize(addr);
      #endif
    }
    *((uint8_t *)ufoImage + addr) = (uint8_t)value;
  } else if (addr & UFO_ADDR_TEMP_BIT) {
    addr &= UFO_ADDR_TEMP_MASK;
    if (addr >= ufoImageTempSize) {
      #ifdef UFO_HUGE_IMAGES
      ufoFatal("Forth segmentation fault at address 0x%08X", addr);
      #else
      ufoImgEnsureTemp(addr);
      #endif
    }
    *((uint8_t *)ufoImageTemp + addr) = (uint8_t)value;
  } else if ((addr & UFO_ADDR_HANDLE_BIT) != 0) {
    ufoHandleStoreByte(addr, 0, value);
  } else {
    ufoFatal("Forth segmentation fault at address 0x%08X", addr);
  }
}


//==========================================================================
//
//  ufoImgPutU16
//
//  fast
//
//==========================================================================
UFO_FORCE_INLINE void ufoImgPutU16 (uint32_t addr, const uint32_t value) {
  if ((addr & UFO_ADDR_SPECIAL_BITS_MASK) == 0) {
    if (addr + 1u >= ufoImageSize) {
      #ifdef UFO_HUGE_IMAGES
      ufoFatal("Forth segmentation fault at address 0x%08X", addr);
      #else
      ufoImgEnsureSize(addr + 1u);
      #endif
    }
    *(uint16_t *)((uint8_t *)ufoImage + addr) = (uint16_t)value;
  } else if (addr & UFO_ADDR_TEMP_BIT) {
    addr &= UFO_ADDR_TEMP_MASK;
    if (addr + 1u >= ufoImageTempSize) {
      #ifdef UFO_HUGE_IMAGES
      ufoFatal("Forth segmentation fault at address 0x%08X", addr);
      #else
      ufoImgEnsureTemp(addr + 1u);
      #endif
    }
    *(uint16_t *)((uint8_t *)ufoImageTemp + addr) = (uint16_t)value;
  } else if ((addr & UFO_ADDR_HANDLE_BIT) != 0) {
    ufoHandleStoreWord(addr, 0, value);
  } else {
    ufoFatal("Forth segmentation fault at address 0x%08X", addr);
  }
}


//==========================================================================
//
//  ufoImgPutU32
//
//  fast
//
//==========================================================================
UFO_FORCE_INLINE void ufoImgPutU32 (uint32_t addr, const uint32_t value) {
  if ((addr & UFO_ADDR_SPECIAL_BITS_MASK) == 0) {
    if (addr + 3u >= ufoImageSize) {
      #ifdef UFO_HUGE_IMAGES
      ufoFatal("Forth segmentation fault at address 0x%08X", addr);
      #else
      ufoImgEnsureSize(addr + 3u);
      #endif
    }
    *(uint32_t *)((uint8_t *)ufoImage + addr) = value;
  } else if (addr & UFO_ADDR_TEMP_BIT) {
    addr &= UFO_ADDR_TEMP_MASK;
    if (addr + 3u >= ufoImageTempSize) {
      #ifdef UFO_HUGE_IMAGES
      ufoFatal("Forth segmentation fault at address 0x%08X", addr);
      #else
      ufoImgEnsureTemp(addr + 3u);
      #endif
    }
    *(uint32_t *)((uint8_t *)ufoImageTemp + addr) = value;
  } else if ((addr & UFO_ADDR_HANDLE_BIT) != 0) {
    ufoHandleStoreCell(addr, 0, value);
  } else {
    ufoFatal("Forth segmentation fault at address 0x%08X", addr);
  }
}


//==========================================================================
//
//  ufoImgIOPtrU32
//
//  fast
//
//==========================================================================
UFO_FORCE_INLINE uint32_t *ufoImgIOPtrU32 (uint32_t addr) {
  if ((addr & UFO_ADDR_SPECIAL_BITS_MASK) == 0) {
    if (addr + 3u >= ufoImageSize) {
      #ifdef UFO_HUGE_IMAGES
      ufoFatal("Forth segmentation fault at address 0x%08X", addr);
      #else
      ufoImgEnsureSize(addr + 3u);
      #endif
    }
    return (uint32_t *)((uint8_t *)ufoImage + addr);
  } else if (addr & UFO_ADDR_TEMP_BIT) {
    addr &= UFO_ADDR_TEMP_MASK;
    if (addr + 3u >= ufoImageTempSize) {
      #ifdef UFO_HUGE_IMAGES
      ufoFatal("Forth segmentation fault at address 0x%08X", addr);
      #else
      ufoImgEnsureTemp(addr + 3u);
      #endif
    }
    return (uint32_t *)((uint8_t *)ufoImageTemp + addr);
  } else {
    ufoFatal("Forth segmentation fault at address 0x%08X", addr);
  }
}


//==========================================================================
//
//  ufoImgGetU8
//
//  false
//
//==========================================================================
UFO_FORCE_INLINE uint32_t ufoImgGetU8 (uint32_t addr) {
  if ((addr & UFO_ADDR_SPECIAL_BITS_MASK) == 0) {
    if (addr >= ufoImageSize) {
      // accessing unallocated image area is segmentation fault
      ufoFatal("Forth segmentation fault (unallocated access) at address 0x%08X", addr);
    }
    return *((const uint8_t *)ufoImage + addr);
  } else if (addr & UFO_ADDR_TEMP_BIT) {
    addr &= UFO_ADDR_TEMP_MASK;
    if (addr >= ufoImageTempSize) {
      // accessing unallocated image area is segmentation fault
      ufoFatal("Forth segmentation fault (unallocated access) at address 0x%08X", addr | UFO_ADDR_TEMP_BIT);
    }
    return *((const uint8_t *)ufoImageTemp + addr);
  } else if ((addr & UFO_ADDR_HANDLE_BIT) != 0) {
    return ufoHandleLoadByte(addr, 0);
  } else {
    ufoFatal("Forth segmentation fault at address 0x%08X", addr);
  }
}


//==========================================================================
//
//  ufoImgGetU16
//
//  fast
//
//==========================================================================
UFO_FORCE_INLINE uint32_t ufoImgGetU16 (uint32_t addr) {
  if ((addr & UFO_ADDR_SPECIAL_BITS_MASK) == 0) {
    if (addr + 1u >= ufoImageSize) {
      // accessing unallocated image area is segmentation fault
      ufoFatal("Forth segmentation fault (unallocated access) at address 0x%08X", addr);
    }
    return *(const uint16_t *)((const uint8_t *)ufoImage + addr);
  } else if (addr & UFO_ADDR_TEMP_BIT) {
    addr &= UFO_ADDR_TEMP_MASK;
    if (addr + 1u >= ufoImageTempSize) {
      // accessing unallocated image area is segmentation fault
      ufoFatal("Forth segmentation fault (unallocated access) at address 0x%08X", addr | UFO_ADDR_TEMP_BIT);
    }
    return *(const uint16_t *)((const uint8_t *)ufoImageTemp + addr);
  } else if ((addr & UFO_ADDR_HANDLE_BIT) != 0) {
    return ufoHandleLoadWord(addr, 0);
  } else {
    ufoFatal("Forth segmentation fault at address 0x%08X", addr);
  }
}


//==========================================================================
//
//  ufoImgGetU32
//
//  fast
//
//==========================================================================
UFO_FORCE_INLINE uint32_t ufoImgGetU32 (uint32_t addr) {
  if ((addr & UFO_ADDR_SPECIAL_BITS_MASK) == 0) {
    if (addr + 3u >= ufoImageSize) {
      // accessing unallocated image area is segmentation fault
      ufoFatal("Forth segmentation fault (unallocated access) at address 0x%08X", addr);
    }
    return *(const uint32_t *)((const uint8_t *)ufoImage + addr);
  } else if (addr & UFO_ADDR_TEMP_BIT) {
    addr &= UFO_ADDR_TEMP_MASK;
    if (addr + 3u >= ufoImageTempSize) {
      // accessing unallocated image area is segmentation fault
      ufoFatal("Forth segmentation fault (unallocated access) at address 0x%08X", addr | UFO_ADDR_TEMP_BIT);
    }
    return *(const uint32_t *)((const uint8_t *)ufoImageTemp + addr);
  } else if ((addr & UFO_ADDR_HANDLE_BIT) != 0) {
    return ufoHandleLoadCell(addr, 0);
  } else {
    ufoFatal("Forth segmentation fault at address 0x%08X", addr);
  }
}

#else

//==========================================================================
//
//  ufoImgPutU8
//
//  general
//
//==========================================================================
UFO_FORCE_INLINE void ufoImgPutU8 (uint32_t addr, const uint32_t value) {
  uint32_t *imgptr;
  if ((addr & UFO_ADDR_SPECIAL_BITS_MASK) == 0) {
    if (addr >= ufoImageSize) {
      #ifdef UFO_HUGE_IMAGES
      ufoFatal("Forth segmentation fault at address 0x%08X", addr);
      #else
      ufoImgEnsureSize(addr);
      #endif
    }
    imgptr = &ufoImage[addr/4u];
  } else if (addr & UFO_ADDR_TEMP_BIT) {
    addr &= UFO_ADDR_TEMP_MASK;
    if (addr >= ufoImageTempSize) {
      #ifdef UFO_HUGE_IMAGES
      ufoFatal("Forth segmentation fault at address 0x%08X", addr);
      #else
      ufoImgEnsureTemp(addr);
      #endif
    }
    imgptr = &ufoImageTemp[addr/4u];
  } else if ((addr & UFO_ADDR_HANDLE_BIT) != 0) {
    ufoHandleStoreByte(addr, 0, value);
  } else {
    ufoFatal("Forth segmentation fault at address 0x%08X", addr);
  }
  const uint8_t val = (uint8_t)value;
  memcpy((uint8_t *)imgptr + (addr&3), &val, 1);
}


//==========================================================================
//
//  ufoImgPutU16
//
//  general
//
//==========================================================================
UFO_FORCE_INLINE void ufoImgPutU16 (uint32_t addr, const uint32_t value) {
  ufoImgPutU8(addr, value&0xffU);
  ufoImgPutU8(addr + 1u, (value>>8)&0xffU);
}


//==========================================================================
//
//  ufoImgPutU32
//
//  general
//
//==========================================================================
UFO_FORCE_INLINE void ufoImgPutU32 (uint32_t addr, const uint32_t value) {
  ufoImgPutU16(addr, value&0xffffU);
  ufoImgPutU16(addr + 2u, (value>>16)&0xffffU);
}


//==========================================================================
//
//  ufoImgGetU8
//
//  general
//
//==========================================================================
UFO_FORCE_INLINE uint32_t ufoImgGetU8 (uint32_t addr) {
  uint32_t *imgptr;
  if ((addr & UFO_ADDR_SPECIAL_BITS_MASK) == 0) {
    if (addr >= ufoImageSize) {
      // accessing unallocated image area is segmentation fault
      ufoFatal("Forth segmentation fault (unallocated access) at address 0x%08X", addr);
    }
    imgptr = &ufoImage[addr/4u];
  } else if (addr & UFO_ADDR_TEMP_BIT) {
    addr &= UFO_ADDR_TEMP_MASK;
    if (addr >= ufoImageTempSize) return 0;
    imgptr = &ufoImageTemp[addr/4u];
  } else if ((addr & UFO_ADDR_HANDLE_BIT) != 0) {
    return ufoHandleLoadByte(addr, 0, value);
  } else {
    ufoFatal("Forth segmentation fault at address 0x%08X", addr);
  }
  uint8_t val;
  memcpy(&val, (uint8_t *)imgptr + (addr&3), 1);
  return (uint32_t)val;
}


//==========================================================================
//
//  ufoImgGetU16
//
//  general
//
//==========================================================================
UFO_FORCE_INLINE uint32_t ufoImgGetU16 (uint32_t addr) {
  return ufoImgGetU8(addr) | (ufoImgGetU8(addr + 1u) << 8);
}


//==========================================================================
//
//  ufoImgGetU32
//
//  general
//
//==========================================================================
UFO_FORCE_INLINE uint32_t ufoImgGetU32 (uint32_t addr) {
  return ufoImgGetU16(addr) | (ufoImgGetU16(addr + 2u) << 16);
}
#endif


//==========================================================================
//
//  ufoEnsureDebugSize
//
//==========================================================================
UFO_DISABLE_INLINE void ufoEnsureDebugSize (uint32_t sdelta) {
  ufo_assert(sdelta != 0);
  if (ufoDebugImageSize != 0) {
    if (ufoDebugImageUsed + sdelta >= 0x40000000U) ufoFatal("debug info too big");
    if (ufoDebugImageUsed + sdelta > ufoDebugImageSize) {
      // grow by 32KB, this should be more than enough
      const uint32_t newsz = ((ufoDebugImageUsed + sdelta) | 0x7fffU) + 1u;
      uint8_t *ndb = realloc(ufoDebugImage, newsz);
      if (ndb == NULL) ufoFatal("out of memory for debug info");
      ufoDebugImage = ndb;
      ufoDebugImageSize = newsz;
    }
  } else {
    // initial allocation: 32KB, quite a lot
    ufo_assert(ufoDebugImage == NULL);
    ufo_assert(ufoDebugImageUsed == 0);
    ufoDebugImageSize = 1024 * 32;
    ufoDebugImage = malloc(ufoDebugImageSize);
    if (ufoDebugImage == NULL) ufoFatal("out of memory for debug info");
  }
}


#define UFO_DBG_PUT_U4(val_)  do { \
  const uint32_t vv_ = (val_); \
  *((uint32_t *)(ufoDebugImage + ufoDebugImageUsed)) = vv_; \
  ufoDebugImageUsed += 4u; \
} while (0)


/*
  debug info header:
    dd lastFileInfoOfs
    ...first line info header...
  line info header (or reset):
    db 0   ; zero line delta
    dw followFileInfoSize  ; either it, or 0 if reused
    dd fileInfoOfs         ; present only if reused
  lines:
    dv lineDelta
    dv pcBytes

  file info record:
    dd prevFileInfoOfs
    dd fileNameHash
    dd nameLen  ; without terminating 0
    ...name... (0-terminated)

  we will never compare file names: length and hash should provide
  good enough unique identifier.

static uint8_t *ufoDebugImage = NULL;
static uint32_t ufoDebugImageUsed = 0; // in bytes
static uint32_t ufoDebugImageSize = 0; // in bytes
static uint32_t ufoDebugFileNameHash = 0; // current file name hash
static uint32_t ufoDebugFileNameLen = 0; // current file name length
static uint32_t ufoDebugCurrDP = 0;
*/


//==========================================================================
//
//  ufoSkipDebugVarInt
//
//==========================================================================
static __attribute__((unused)) uint32_t ufoSkipDebugVarInt (uint32_t ofs) {
  uint8_t byte;
  do {
    if (ofs >= ufoDebugImageUsed) ufoFatal("invalid debug data");
    byte = ufoDebugImage[ofs]; ofs += 1u;
  } while (byte >= 0x80);
  return ofs;
}


//==========================================================================
//
//  ufoCalcDebugVarIntSize
//
//==========================================================================
UFO_FORCE_INLINE uint8_t ufoCalcDebugVarIntSize (uint32_t v) {
  uint8_t count = 0;
  do {
    count += 1u;
    v >>= 7;
  } while (v != 0);
  return count;
}


//==========================================================================
//
//  ufoGetDebugVarInt
//
//==========================================================================
static __attribute__((unused)) uint32_t ufoGetDebugVarInt (uint32_t ofs) {
  uint32_t v = 0;
  uint8_t shift = 0;
  uint8_t byte;
  do {
    if (ofs >= ufoDebugImageUsed) ufoFatal("invalid debug data");
    byte = ufoDebugImage[ofs];
    v |= (uint32_t)(byte & 0x7f) << shift;
    if (byte >= 0x80) {
      shift += 7;
      ofs += 1u;
    }
  } while (byte >= 0x80);
  return v;
}


//==========================================================================
//
//  ufoPutDebugVarInt
//
//==========================================================================
UFO_FORCE_INLINE void ufoPutDebugVarInt (uint32_t v) {
  ufoEnsureDebugSize(5u); // maximum size
  do {
    if (v >= 0x80) {
      ufoDebugImage[ufoDebugImageUsed] = (uint8_t)(v | 0x80u);
    } else {
      ufoDebugImage[ufoDebugImageUsed] = (uint8_t)v;
    }
    ufoDebugImageUsed += 1;
    v >>= 7;
  } while (v != 0);
}


#ifdef UFO_DEBUG_DEBUG
//==========================================================================
//
//  ufoDumpDebugInfo
//
//==========================================================================
static void ufoDumpDebugImage (void) {
  #if 0
  uint32_t dbgpos = 4u; // first line header info
  uint32_t lastline = 0;
  uint32_t lastdp = 0;
  while (dbgpos < ufoDebugImageUsed) {
    if (ufoDebugImage[dbgpos] == 0) {
      // new file info
      dbgpos += 1u; // skip flag
      const uint32_t fhdrSize = *(const uint16_t *)(ufoDebugImage + dbgpos); dbgpos += 2u;
      lastdp = ufoGetDebugVarInt(dbgpos);
      dbgpos = ufoSkipDebugVarInt(dbgpos);
      if (fhdrSize == 0) {
        // reused
        const uint32_t infoOfs = *(const uint32_t *)(ufoDebugImage + dbgpos); dbgpos += 4u;
        fprintf(stderr, "*** OLD FILE: %s\n", (const char *)(ufoDebugImage + infoOfs + 3u * 4u));
        fprintf(stderr, "FILE NAME LEN: %u\n", ((const uint32_t *)(ufoDebugImage + infoOfs))[2]);
        fprintf(stderr, "FILE NAME HASH: 0x%08x\n", ((const uint32_t *)(ufoDebugImage + infoOfs))[1]);
      } else {
        // new
        fprintf(stderr, "*** NEW FILE: %s\n", (const char *)(ufoDebugImage + dbgpos + 3u * 4u));
        fprintf(stderr, "FILE NAME LEN: %u\n", ((const uint32_t *)(ufoDebugImage + dbgpos))[2]);
        fprintf(stderr, "FILE NAME HASH: 0x%08x\n", ((const uint32_t *)(ufoDebugImage + dbgpos))[1]);
      }
      dbgpos += fhdrSize;
      fprintf(stderr, "LINES-OFS: 0x%08x (hsz: %u -- 0x%08x)\n", dbgpos, fhdrSize, fhdrSize);
      lastline = ~(uint32_t)0;
    } else {
      const uint32_t ln = ufoGetDebugVarInt(dbgpos);
      dbgpos = ufoSkipDebugVarInt(dbgpos);
      ufo_assert(ln != 0);
      lastline += ln;
      const uint32_t edp = ufoGetDebugVarInt(dbgpos);
      dbgpos = ufoSkipDebugVarInt(dbgpos);
      lastdp += edp;
      fprintf(stderr, "  line %6u: edp=%u\n", lastline, lastdp);
    }
  }
  #endif
}
#endif


//==========================================================================
//
//  ufoRecordDebugCheckFile
//
//  if we moved to the new file:
//    put "line info header"
//    put new file info (or reuse old)
//
//==========================================================================
UFO_FORCE_INLINE void ufoRecordDebugCheckFile (void) {
  if (ufoDebugImageUsed == 0 ||
      ufoDebugFileNameLen != ufoInFileNameLen ||
      ufoDebugFileNameHash != ufoInFileNameHash)
  {
    // new file record (or reuse old one)
    const int initial = (ufoDebugImageUsed == 0);
    uint32_t fileRec = 0;
    // try to find and old one
    if (!initial) {
      fileRec = *(const uint32_t *)ufoDebugImage;
      #if 0
      fprintf(stderr, "*** NEW-FILE(%u): 0x%08x: <%s> (frec=0x%08x)\n", ufoInFileNameLen,
              ufoInFileNameHash, ufoInFileName, fileRec);
      #endif
      while (fileRec != 0 &&
             (ufoInFileNameLen != ((const uint32_t *)(ufoDebugImage + fileRec))[1] ||
              ufoInFileNameHash != ((const uint32_t *)(ufoDebugImage + fileRec))[2]))
      {
        #if 0
        fprintf(stderr, "*** FRCHECK: 0x%08x\n", fileRec);
        fprintf(stderr, "  FILE NAME: %s\n", (const char *)(ufoDebugImage + fileRec + 3u * 4u));
        fprintf(stderr, "  FILE NAME LEN: %u\n", ((const uint32_t *)(ufoDebugImage + fileRec))[2]);
        fprintf(stderr, "  FILE NAME HASH: 0x%08x\n", ((const uint32_t *)(ufoDebugImage + fileRec))[1]);
        fprintf(stderr, "  FILE PREV: 0x%08x\n", *(const uint32_t *)(ufoDebugImage + fileRec));
        #endif
        fileRec = *(const uint32_t *)(ufoDebugImage + fileRec);
      }
      #if 0
      fprintf(stderr, "*** FRCHECK-DONE: 0x%08x\n", fileRec);
      if (fileRec != 0) {
        fprintf(stderr, "    FILE NAME: %s\n", (const char *)(ufoDebugImage + fileRec + 3u * 4u));
        fprintf(stderr, "    FILE NAME LEN: %u\n", ((const uint32_t *)(ufoDebugImage + fileRec))[2]);
        fprintf(stderr, "    FILE NAME HASH: 0x%08x\n", ((const uint32_t *)(ufoDebugImage + fileRec))[1]);
        fprintf(stderr, "    FILE PREV: 0x%08x\n", *(const uint32_t *)(ufoDebugImage + fileRec));
      }
      #endif
    } else {
      ufoEnsureDebugSize(8u);
      *(uint32_t *)ufoDebugImage = 0;
    }
    // write "line info header"
    if (fileRec != 0) {
      ufoEnsureDebugSize(32u);
      ufoDebugImage[ufoDebugImageUsed] = 0; ufoDebugImageUsed += 1u; // header flag (0 delta)
      // file record size: 0 (reused)
      *((uint16_t *)(ufoDebugImage + ufoDebugImageUsed)) = 0; ufoDebugImageUsed += 2u;
      // put last DP
      ufoPutDebugVarInt(ufoDebugCurrDP);
      // file info offset
      UFO_DBG_PUT_U4(fileRec);
    } else {
      // name, trailing 0 byte, 3 dword fields
      const uint32_t finfoSize = ufoInFileNameLen + 1u + 3u * 4u;
      ufo_assert(finfoSize < 65536u);
      ufoEnsureDebugSize(finfoSize + 32u);
      if (initial) {
        *(uint32_t *)ufoDebugImage = 0;
        ufoDebugImageUsed = 4;
      }
      ufoDebugImage[ufoDebugImageUsed] = 0; ufoDebugImageUsed += 1u; // header flag (0 delta)
      // file record size
      *((uint16_t *)(ufoDebugImage + ufoDebugImageUsed)) = (uint16_t)finfoSize; ufoDebugImageUsed += 2u;
      // put last DP
      ufoPutDebugVarInt(ufoDebugCurrDP);
      // file record follows
      // fix file info offsets
      uint32_t lastOfs = *(const uint32_t *)ufoDebugImage;
      *(uint32_t *)ufoDebugImage = ufoDebugImageUsed;
      UFO_DBG_PUT_U4(lastOfs);
      // save file info hash
      UFO_DBG_PUT_U4(ufoInFileNameHash);
      // save file info length
      UFO_DBG_PUT_U4(ufoInFileNameLen);
      // save file name
      if (ufoInFileNameLen != 0) {
        memcpy(ufoDebugImage + ufoDebugImageUsed, ufoInFileName, ufoInFileNameLen + 1u);
        ufoDebugImageUsed += ufoInFileNameLen + 1u;
      } else {
        ufoDebugImage[ufoDebugImageUsed] = 0; ufoDebugImageUsed += 1u;
      }
    }
    ufoDebugFileNameLen = ufoInFileNameLen;
    ufoDebugFileNameHash = ufoInFileNameHash;
    ufoDebugLastLine = ~(uint32_t)0;
    ufoDebugLastLinePCOfs = 0;
    ufoDebugLastLineDP = ufoDebugCurrDP;
  }
}


//==========================================================================
//
//  ufoRecordDebugRecordLine
//
//==========================================================================
UFO_FORCE_INLINE void ufoRecordDebugRecordLine (uint32_t line, uint32_t newhere) {
  if (line == ufoDebugLastLine) {
    ufo_assert(ufoDebugLastLinePCOfs != 0);
    ufoDebugImageUsed = ufoDebugLastLinePCOfs;
  } else {
    #if 0
    fprintf(stderr, "FL-NEW-LINE(0x%08x): <%s>; new line: %u (old: %u)\n",
            ufoDebugImageUsed,
            ufoInFileName, line, ufoDebugLastLine);
    #endif
    ufoPutDebugVarInt(line - ufoDebugLastLine);
    ufoDebugLastLinePCOfs = ufoDebugImageUsed;
    ufoDebugLastLine = line;
    ufoDebugLastLineDP = ufoDebugCurrDP;
  }
  ufoPutDebugVarInt(newhere - ufoDebugLastLineDP);
  ufoDebugCurrDP = newhere;
}


//==========================================================================
//
//  ufoRecordDebug
//
//==========================================================================
UFO_DISABLE_INLINE void ufoRecordDebug (uint32_t newhere) {
  if (newhere > ufoDebugCurrDP) {
    uint32_t ln = (uint32_t)ufoInFileLine;
    if (ln == ~(uint32_t)0) ln = 0;
    #if 0
    fprintf(stderr, "FL: <%s>; line: %d\n", ufoInFileName, ufoInFileLine);
    #endif
    ufoRecordDebugCheckFile();
    ufoRecordDebugRecordLine(ln, newhere);
  }
}


//==========================================================================
//
//  ufoGetWordEndAddrYFA
//
//==========================================================================
static uint32_t ufoGetWordEndAddrYFA (uint32_t yfa) {
  if (yfa > 8u) {
    const uint32_t oyfa = yfa;
    yfa = ufoImgGetU32(yfa); // YFA points to next YFA
    if (yfa == 0) {
      // last defined word
      if ((oyfa & UFO_ADDR_TEMP_BIT) == 0) {
        yfa = ufoImgGetU32(ufoAddrDP);
      } else {
        yfa = ufoImgGetU32(ufoAddrDPTemp);
      }
    } else {
      yfa = UFO_YFA_TO_WST(yfa);
    }
  } else {
    yfa = 0;
  }
  return yfa;
}


//==========================================================================
//
//  ufoGetWordEndAddr
//
//==========================================================================
static uint32_t ufoGetWordEndAddr (const uint32_t cfa) {
  if (cfa != 0) {
    const uint32_t lfa = UFO_CFA_TO_LFA(cfa);
    const uint32_t yfa = UFO_LFA_TO_YFA(lfa);
    return ufoGetWordEndAddrYFA(yfa);
  } else {
    return 0;
  }
}


//==========================================================================
//
//  ufoFindWordForIP
//
//  return NFA or 0
//
//  WARNING: this is SLOW!
//
//==========================================================================
static uint32_t ufoFindWordForIP (const uint32_t ip) {
  uint32_t res = 0;
  if (ip != 0) {
    //fprintf(stderr, "ufoFindWordForIP:000: ip=0x%08x\n", ip);
    // iterate over all words
    uint32_t xfa = ufoImgGetU32(ufoAddrLastXFA);
    //fprintf(stderr, "ufoFindWordForIP:001: xfa=0x%08x\n", xfa);
    if (xfa != 0) {
      while (res == 0 && xfa != 0) {
        const uint32_t yfa = UFO_XFA_TO_YFA(xfa);
        const uint32_t wst = UFO_YFA_TO_WST(yfa);
        //fprintf(stderr, "ufoFindWordForIP:002:   yfa=0x%08x; wst=0x%08x\n", yfa, wst);
        const uint32_t wend = ufoGetWordEndAddrYFA(yfa);
        if (ip >= wst && ip < wend) {
          res = UFO_YFA_TO_NFA(yfa);
        } else {
          xfa = ufoImgGetU32(xfa);
        }
      }
    }
  }
  return res;
}


//==========================================================================
//
//  ufoFindFileForIP
//
//  return file name or `NULL`
//
//  WARNING: this is SLOW!
//
//==========================================================================
static const char *ufoFindFileForIP (uint32_t ip, uint32_t *line,
                                     uint32_t *nlen, uint32_t *nhash)
{
  if (ip != 0 && ufoDebugImageUsed != 0) {
    const char *filename = NULL;
    uint32_t dbgpos = 4u; // first line header info
    uint32_t lastline = 0;
    uint32_t lastdp = 0;
    uint32_t namelen = 0;
    uint32_t namehash = 0;
    while (dbgpos < ufoDebugImageUsed) {
      if (ufoDebugImage[dbgpos] == 0) {
        // new file info
        dbgpos += 1u; // skip flag
        const uint32_t fhdrSize = *(const uint16_t *)(ufoDebugImage + dbgpos); dbgpos += 2u;
        lastdp = ufoGetDebugVarInt(dbgpos);
        dbgpos = ufoSkipDebugVarInt(dbgpos);
        uint32_t infoOfs;
        if (fhdrSize == 0) {
          // reused
          infoOfs = *(const uint32_t *)(ufoDebugImage + dbgpos); dbgpos += 4u;
        } else {
          // new
          infoOfs = dbgpos;
        }
        filename = (const char *)(ufoDebugImage + infoOfs + 3u * 4u);
        namelen = ((const uint32_t *)(ufoDebugImage + infoOfs))[2];
        namehash = ((const uint32_t *)(ufoDebugImage + infoOfs))[1];
        if (filename[0] == 0) filename = NULL;
        dbgpos += fhdrSize;
        lastline = ~(uint32_t)0;
      } else {
        const uint32_t ln = ufoGetDebugVarInt(dbgpos);
        dbgpos = ufoSkipDebugVarInt(dbgpos);
        ufo_assert(ln != 0);
        lastline += ln;
        const uint32_t edp = ufoGetDebugVarInt(dbgpos);
        dbgpos = ufoSkipDebugVarInt(dbgpos);
        if (ip >= lastdp && ip < lastdp + edp) {
          if (line) *line = lastline;
          if (nlen) *nlen = namelen;
          if (nhash) *nhash = namehash;
          return filename;
        }
        lastdp += edp;
      }
    }
  }
  if (line) *line = 0;
  if (nlen) *nlen = 0;
  if (nhash) *nlen = 0;
  return NULL;
}


//==========================================================================
//
//  ufoBumpDP
//
//==========================================================================
UFO_FORCE_INLINE void ufoBumpDP (uint32_t delta) {
  const uint32_t dpa = ufoImgGetU32(ufoAddrHereDP);
  uint32_t dp = ufoImgGetU32(dpa);
  if ((dp & UFO_ADDR_SPECIAL_BITS_MASK) == 0) ufoRecordDebug(dp + delta);
  dp += delta;
  ufoImgPutU32(dpa, dp);
}


//==========================================================================
//
//  ufoImgEmitU8
//
//==========================================================================
UFO_FORCE_INLINE void ufoImgEmitU8 (uint32_t value) {
  ufoImgPutU8(UFO_GET_DP(), value);
  ufoBumpDP(1);
}


//==========================================================================
//
//  ufoImgEmitU16
//
//==========================================================================
UFO_FORCE_INLINE void ufoImgEmitU16 (uint32_t value) {
  ufoImgPutU16(UFO_GET_DP(), value);
  ufoBumpDP(2);
}


//==========================================================================
//
//  ufoImgEmitU32
//
//==========================================================================
UFO_FORCE_INLINE void ufoImgEmitU32 (uint32_t value) {
  ufoImgPutU32(UFO_GET_DP(), value);
  ufoBumpDP(4);
}


//==========================================================================
//
//  ufoImgEmitCFA
//
//==========================================================================
UFO_FORCE_INLINE void ufoImgEmitCFA (uint32_t cfa) {
  const uint32_t addr = UFO_GET_DP();
  ufoImgPutU32(addr, cfa);
  ufoImgPutU32(addr + 4u, 0);
  ufoBumpDP(8);
}


#ifdef UFO_FAST_MEM_ACCESS

//==========================================================================
//
//  ufoImgEmitU32_NoInline
//
//  false
//
//==========================================================================
UFO_FORCE_INLINE void ufoImgEmitU32_NoInline (uint32_t value) {
  ufoImgPutU32(UFO_GET_DP(), value);
  ufoBumpDP(4);
}

#else

//==========================================================================
//
//  ufoImgEmitU32_NoInline
//
//  general
//
//==========================================================================
UFO_DISABLE_INLINE void ufoImgEmitU32_NoInline (uint32_t value) {
  ufoImgPutU32(UFO_GET_DP(), value);
  ufoBumpDP(4);
}

#endif


//==========================================================================
//
//  ufoImgEmitAlign
//
//==========================================================================
UFO_FORCE_INLINE void ufoImgEmitAlign (void) {
  while ((UFO_GET_DP() & 3) != 0) ufoImgEmitU8(0);
}


//==========================================================================
//
//  ufoResetTib
//
//==========================================================================
UFO_FORCE_INLINE void ufoResetTib (void) {
  uint32_t defTIB = ufoImgGetU32(ufoAddrDefTIB);
  //fprintf(stderr, "ufoResetTib(%p): defTIB=0x%08x\n", ufoCurrState, defTIB);
  if (defTIB == 0) {
    // create new TIB handle
    UfoHandle *tibh = ufoAllocHandle(0x69a029a6); // arbitrary number
    defTIB = tibh->ufoHandle;
    ufoImgPutU32(ufoAddrDefTIB, defTIB);
  }
  if ((defTIB & UFO_ADDR_HANDLE_BIT) != 0) {
    UfoHandle *hh = ufoGetHandle(defTIB);
    if (hh == NULL) ufoFatal("default TIB is not allocated");
    if (hh->size == 0) {
      ufo_assert(hh->data == NULL);
      hh->data = calloc(1, UFO_ADDR_HANDLE_OFS_MASK + 1);
      if (hh->data == NULL) ufoFatal("out of memory for default TIB");
      hh->size = UFO_ADDR_HANDLE_OFS_MASK + 1;
    }
  }
  const uint32_t oldA = ufoRegA;
  ufoImgPutU32(ufoAddrTIBx, defTIB);
  ufoImgPutU32(ufoAddrINx, 0);
  ufoRegA = defTIB;
  ufoPush(0); // value
  ufoPush(0); // offset
  UFCALL(CPOKE_REGA_IDX);
  ufoRegA = oldA;
}


//==========================================================================
//
//  ufoTibEnsureSize
//
//==========================================================================
UFO_DISABLE_INLINE void ufoTibEnsureSize (uint32_t size) {
  if (size > 1024u * 1024u * 256u) ufoFatal("TIB size too big");
  const uint32_t tib = ufoImgGetU32(ufoAddrTIBx);
  //fprintf(stderr, "ufoTibEnsureSize: TIB=0x%08x; size=%u\n", tib, size);
  if ((tib & UFO_ADDR_HANDLE_BIT) != 0) {
    UfoHandle *hh = ufoGetHandle(tib);
    if (hh == NULL) {
      ufoFatal("cannot resize TIB, TIB is not a handle");
    }
    if (hh->size < size) {
      const uint32_t newsz = (size | 0xfffU) + 1u;
      uint8_t *nx = realloc(hh->data, newsz);
      if (nx == NULL) ufoFatal("out of memory for restored TIB");
      hh->data = nx;
      hh->size = newsz;
    }
  }
  #if 0
  else {
    ufoFatal("cannot resize TIB, TIB is not a handle (0x%08x)", tib);
  }
  #endif
}


//==========================================================================
//
//  ufoTibGetSize
//
//==========================================================================
/*
UFO_DISABLE_INLINE uint32_t ufoTibGetSize (void) {
  const uint32_t tib = ufoImgGetU32(ufoAddrTIBx);
  if ((tib & UFO_ADDR_HANDLE_BIT) == 0) {
    ufoFatal("cannot query TIB, TIB is not a handle");
  }
  UfoHandle *hh = ufoGetHandle(tib);
  if (hh == NULL) {
    ufoFatal("cannot query TIB, TIB is not a handle");
  }
  return hh->size;
}
*/


//==========================================================================
//
//  ufoTibPeekCh
//
//==========================================================================
UFO_FORCE_INLINE uint8_t ufoTibPeekCh (void) {
  return (uint8_t)ufoImgGetU8(ufoImgGetU32(ufoAddrTIBx) + ufoImgGetU32(ufoAddrINx));
}


//==========================================================================
//
//  ufoTibPeekChOfs
//
//==========================================================================
UFO_FORCE_INLINE uint8_t ufoTibPeekChOfs (uint32_t ofs) {
  const uint32_t tib = ufoImgGetU32(ufoAddrTIBx);
  if (ofs <= UFO_ADDR_HANDLE_OFS_MASK || (tib & UFO_ADDR_HANDLE_BIT) == 0) {
    return (uint8_t)ufoImgGetU8(tib + ufoImgGetU32(ufoAddrINx) + ofs);
  } else {
    return 0;
  }
}


//==========================================================================
//
//  ufoTibPokeChOfs
//
//==========================================================================
UFO_DISABLE_INLINE void ufoTibPokeChOfs (uint8_t ch, uint32_t ofs) {
  const uint32_t oldA = ufoRegA;
  ufoRegA = ufoImgGetU32(ufoAddrTIBx);
  ufoPush(ch);
  ufoPush(ufoImgGetU32(ufoAddrINx) + ofs);
  UFCALL(CPOKE_REGA_IDX);
  ufoRegA = oldA;
}


//==========================================================================
//
//  ufoTibGetCh
//
//==========================================================================
UFO_FORCE_INLINE uint8_t ufoTibGetCh (void) {
  const uint8_t ch = ufoTibPeekCh();
  if (ch) ufoImgPutU32(ufoAddrINx, ufoImgGetU32(ufoAddrINx) + 1u);
  return ch;
}


//==========================================================================
//
//  ufoTibSkipCh
//
//==========================================================================
UFO_FORCE_INLINE void ufoTibSkipCh (void) {
  (void)ufoTibGetCh();
}


// ////////////////////////////////////////////////////////////////////////// //
// native CFA implementations
//

//==========================================================================
//
//  ufoDoForth
//
//==========================================================================
static void ufoDoForth (uint32_t pfa) {
  ufoRPush(ufoIP);
  ufoIP = pfa;
}


//==========================================================================
//
//  ufoDoVariable
//
//==========================================================================
static void ufoDoVariable (uint32_t pfa) {
  ufoPush(pfa);
}


//==========================================================================
//
//  ufoDoUserVariable
//
//==========================================================================
static void ufoDoUserVariable (uint32_t pfa) {
  ufoPush(ufoImgGetU32(pfa));
}


//==========================================================================
//
//  ufoDoValue
//
//==========================================================================
static void ufoDoValue (uint32_t pfa) {
  ufoPush(ufoImgGetU32(pfa));
}


//==========================================================================
//
//  ufoDoConst
//
//==========================================================================
static void ufoDoConst (uint32_t pfa) {
  ufoPush(ufoImgGetU32(pfa));
}


//==========================================================================
//
//  ufoDoDefer
//
//==========================================================================
static void ufoDoDefer (uint32_t pfa) {
  pfa = ufoImgGetU32(pfa);
  UFO_EXEC_CFA(pfa);
}


//==========================================================================
//
//  ufoDoDoes
//
//==========================================================================
static void ufoDoDoes (uint32_t pfa) {
  ufoPush(pfa);
  ufoRPush(ufoIP);
  ufoIP = ufoImgGetU32(UFO_PFA_TO_DOES_CFA(pfa));
}


//==========================================================================
//
//  ufoDoRedirect
//
//==========================================================================
static void ufoDoRedirect (uint32_t pfa) {
  pfa = ufoImgGetU32(UFO_PFA_TO_DOES_CFA(pfa));
  UFO_EXEC_CFA(pfa);
}


//==========================================================================
//
//  ufoDoVoc
//
//==========================================================================
static void ufoDoVoc (uint32_t pfa) {
  ufoImgPutU32(ufoAddrContext, ufoImgGetU32(pfa));
}


//==========================================================================
//
//  ufoDoCreate
//
//==========================================================================
static void ufoDoCreate (uint32_t pfa) {
  ufoPush(pfa);
}


//==========================================================================
//
//  ufoPushInFile
//
//  this also increments last used file id
//
//==========================================================================
static void ufoPushInFile (void) {
  if (ufoFileStackPos >= UFO_MAX_NESTED_INCLUDES) ufoFatal("too many includes");
  UFOFileStackEntry *stk = &ufoFileStack[ufoFileStackPos];
  stk->fl = ufoInFile;
  stk->fname = ufoInFileName;
  stk->fline = ufoInFileLine;
  stk->id = ufoFileId;
  stk->incpath = (ufoLastIncPath ? strdup(ufoLastIncPath) : NULL);
  stk->sysincpath = (ufoLastSysIncPath ? strdup(ufoLastSysIncPath) : NULL);
  ufoFileStackPos += 1;
  ufoInFile = NULL;
  ufoInFileName = NULL; ufoInFileNameLen = 0; ufoInFileNameHash = 0;
  ufoInFileLine = 0;
  ufoLastUsedFileId += 1;
  ufo_assert(ufoLastUsedFileId != 0); // just in case ;-)
  //ufoLastIncPath = NULL;
}


//==========================================================================
//
//  ufoWipeIncludeStack
//
//==========================================================================
static void ufoWipeIncludeStack (void) {
  if (ufoInFileName) { free(ufoInFileName); ufoInFileName = NULL; }
  if (ufoInFile) { fclose(ufoInFile); ufoInFile = NULL; }
  if (ufoLastIncPath) { free(ufoLastIncPath); ufoLastIncPath = NULL; }
  if (ufoLastSysIncPath) { free(ufoLastSysIncPath); ufoLastSysIncPath = NULL; }
  while (ufoFileStackPos != 0) {
    ufoFileStackPos -= 1;
    UFOFileStackEntry *stk = &ufoFileStack[ufoFileStackPos];
    if (stk->fl) fclose(stk->fl);
    if (stk->fname) free(stk->fname);
    if (stk->incpath) free(stk->incpath);
  }
}


//==========================================================================
//
//  ufoPopInFile
//
//==========================================================================
static void ufoPopInFile (void) {
  if (ufoFileStackPos == 0) ufoFatal("trying to pop include from empty stack");
  if (ufoInFileName) { free(ufoInFileName); ufoInFileName = NULL; }
  if (ufoInFile) { fclose(ufoInFile); ufoInFile = NULL; }
  if (ufoLastIncPath) { free(ufoLastIncPath); ufoLastIncPath = NULL; }
  if (ufoLastSysIncPath) { free(ufoLastSysIncPath); ufoLastSysIncPath = NULL; }
  ufoFileStackPos -= 1;
  UFOFileStackEntry *stk = &ufoFileStack[ufoFileStackPos];
  ufoInFile = stk->fl;
  ufoSetInFileNameReuse(stk->fname);
  ufoInFileLine = stk->fline;
  ufoLastIncPath = stk->incpath;
  ufoLastSysIncPath = stk->sysincpath;
  ufoFileId = stk->id;
  ufoResetTib();
  #ifdef UFO_DEBUG_INCLUDE
  if (ufoInFileName == NULL) {
    fprintf(stderr, "INC-POP: no more files.\n");
  } else {
    fprintf(stderr, "INC-POP: fname: %s\n", ufoInFileName);
  }
  #endif
}


//==========================================================================
//
//  ufoDeinit
//
//==========================================================================
void ufoDeinit (void) {
  #ifdef UFO_DEBUG_WRITE_MAIN_IMAGE
  {
    FILE *fo = fopen("zufo_main.img", "w");
    const uint32_t dpMain = ufoImgGetU32(ufoAddrDP);
    fwrite(ufoImage, dpMain, 1, fo);
    fclose(fo);
  }
  #endif

  #ifdef UFO_DEBUG_WRITE_DEBUG_IMAGE
  {
    FILE *fo = fopen("zufo_debug.img", "w");
    fwrite(ufoDebugImage, ufoDebugImageUsed, 1, fo);
    fclose(fo);
  }
  #endif

  #ifdef UFO_DEBUG_DEBUG
  {
    const uint32_t dpMain = ufoImgGetU32(ufoAddrDP);
    fprintf(stderr, "UFO: image used: %u; size: %u\n", dpMain, ufoImageSize);
    fprintf(stderr, "UFO: debug image used: %u; size: %u\n", ufoDebugImageUsed, ufoDebugImageSize);
    ufoDumpDebugImage();
  }
  #endif

  // free all states
  #ifdef UFO_MTASK_ALLOWED
  ufoCurrState = NULL;
  ufoYieldedState = NULL;
  ufoDebuggerState = NULL;
  for (uint32_t fidx = 0; fidx < (uint32_t)(UFO_MAX_STATES/32); fidx += 1u) {
    uint32_t bmp = ufoStateUsedBitmap[fidx];
    if (bmp != 0) {
      uint32_t stid = fidx * 32u;
      while (bmp != 0) {
        if ((bmp & 0x01) != 0) ufoFreeState(ufoStateMap[stid]);
        stid += 1u; bmp >>= 1;
      }
    }
  }
  #endif

  free(ufoDebugImage);
  ufoDebugImage = NULL;
  ufoDebugImageUsed = 0;
  ufoDebugImageSize = 0;
  ufoDebugFileNameHash = 0;
  ufoDebugFileNameLen = 0;
  ufoDebugLastLine = 0;
  ufoDebugLastLinePCOfs = 0;
  ufoDebugLastLineDP = 0;
  ufoDebugCurrDP = 0;

  ufoInBacktrace = 0;
  ufoClearCondDefines();
  ufoWipeIncludeStack();

  // release all includes
  ufoInFile = NULL;
  if (ufoInFileName) free(ufoInFileName);
  if (ufoLastIncPath) free(ufoLastIncPath);
  if (ufoLastSysIncPath) free(ufoLastSysIncPath);
  ufoInFileName = NULL; ufoLastIncPath = NULL; ufoLastSysIncPath = NULL;
  ufoInFileNameHash = 0; ufoInFileNameLen = 0;
  ufoInFileLine = 0;

  //free(ufoForthCFAs);
  //ufoForthCFAs = NULL;
  ufoCFAsUsed = 0;

  #ifndef UFO_HUGE_IMAGES
  free(ufoImage);
  ufoImage = NULL;
  ufoImageSize = 0;
  #endif

  ufoMode = UFO_MODE_NATIVE;
  ufoForthVocId = 0; ufoCompilerVocId = 0;
  #ifdef UFO_MTASK_ALLOWED
  ufoSingleStep = 0;
  #endif

  // free all handles
  for (uint32_t f = 0; f < ufoHandlesUsed; f += 1) {
    UfoHandle *hh = ufoHandles[f];
    if (hh != NULL) {
      if (hh->data != NULL) free(hh->data);
      free(hh);
    }
  }
  if (ufoHandles != NULL) free(ufoHandles);
  ufoHandles = NULL; ufoHandlesUsed = 0; ufoHandlesAlloted = 0;
  ufoHandleFreeList = NULL;

  ufoLastEmitWasCR = 1;

  ufoClearCondDefines();
}


//==========================================================================
//
//  ufoDumpWordHeader
//
//==========================================================================
__attribute__((unused)) static void ufoDumpWordHeader (const uint32_t lfa) {
  fprintf(stderr, "=== WORD: LFA: 0x%08x ===\n", lfa);
  if (lfa != 0) {
    fprintf(stderr, "  (XFA): 0x%08x\n", ufoImgGetU32(UFO_LFA_TO_XFA(lfa)));
    fprintf(stderr, "  (YFA): 0x%08x\n", ufoImgGetU32(UFO_LFA_TO_YFA(lfa)));
    fprintf(stderr, "  (BFA): 0x%08x\n", ufoImgGetU32(UFO_LFA_TO_BFA(lfa)));
    fprintf(stderr, "  (LFA): 0x%08x\n", ufoImgGetU32(lfa));
    fprintf(stderr, "  (NHH): 0x%08x\n", ufoImgGetU32(UFO_LFA_TO_NHASH(lfa)));
    const uint32_t cfa = UFO_LFA_TO_CFA(lfa);
    fprintf(stderr, "    CFA: 0x%08x\n", cfa);
    fprintf(stderr, "    PFA: 0x%08x\n", UFO_CFA_TO_PFA(cfa));
    fprintf(stderr, "  (CFA): 0x%08x\n", ufoImgGetU32(cfa));
    const uint32_t nfa = UFO_LFA_TO_NFA(lfa);
    const uint32_t nlen = ufoImgGetU8(nfa);
    fprintf(stderr, "    NFA: 0x%08x  (nlen: %u)\n", nfa, nlen);
    const uint32_t flags = ufoImgGetU32(nfa);
    fprintf(stderr, "  FLAGS: 0x%08x\n", flags);
    if ((flags & 0xffff0000U) != 0) {
      fprintf(stderr, "  FLAGS:");
      if (flags & UFW_FLAG_IMMEDIATE) fprintf(stderr, " IMM");
      if (flags & UFW_FLAG_SMUDGE) fprintf(stderr, " SMUDGE");
      if (flags & UFW_FLAG_NORETURN) fprintf(stderr, " NORET");
      if (flags & UFW_FLAG_HIDDEN) fprintf(stderr, " HIDDEN");
      if (flags & UFW_FLAG_CBLOCK) fprintf(stderr, " CBLOCK");
      if (flags & UFW_FLAG_VOCAB) fprintf(stderr, " VOCAB");
      if (flags & UFW_FLAG_SCOLON) fprintf(stderr, " SCOLON");
      if (flags & UFW_FLAG_PROTECTED) fprintf(stderr, " PROTECTED");
      if (flags & UFW_WARG_CONDBRANCH) fprintf(stderr, " CONDBRANCH");
      if (flags & UFW_FLAG_MAYRETURN) fprintf(stderr, " MAYRETURN");
      fputc('\n', stderr);
    }
    if ((flags & 0xff00U) != 0) {
      fprintf(stderr, "   ARGS: ");
      switch (flags & UFW_WARG_MASK) {
        case UFW_WARG_NONE: fprintf(stderr, "NONE"); break;
        case UFW_WARG_BRANCH: fprintf(stderr, "BRANCH"); break;
        case UFW_WARG_LIT: fprintf(stderr, "LIT"); break;
        case UFW_WARG_C4STRZ: fprintf(stderr, "C4STRZ"); break;
        case UFW_WARG_CFA: fprintf(stderr, "CFA"); break;
        case UFW_WARG_CBLOCK: fprintf(stderr, "CBLOCK"); break;
        case UFW_WARG_VOCID: fprintf(stderr, "VOCID"); break;
        case UFW_WARG_C1STRZ: fprintf(stderr, "C1STRZ"); break;
        case UFW_WARG_DATASKIP: fprintf(stderr, "DATA"); break;
        case UFW_WARG_PFA: fprintf(stderr, "PFA"); break;
        default: fprintf(stderr, "wtf?!"); break;
      }
      fputc('\n', stderr);
    }
    fprintf(stderr, "  BACKLEN: %u (nfa at 0x%08x)\n", ufoImgGetU8(cfa - 1u), UFO_CFA_TO_NFA(cfa));
    fprintf(stderr, "  NAME(%u): ", nlen);
    for (uint32_t f = 0; f < nlen; f += 1) {
      const uint8_t ch = ufoImgGetU8(nfa + 4u + f);
      if (ch <= 32 || ch >= 127) {
        fprintf(stderr, "\\x%02x", ch);
      } else {
        fprintf(stderr, "%c", (char)ch);
      }
    }
    fprintf(stderr, "\n");
    ufo_assert(UFO_CFA_TO_LFA(cfa) == lfa);
  }
}


//==========================================================================
//
//  ufoVocCheckName
//
//  return 0 or CFA
//
//==========================================================================
static uint32_t ufoVocCheckName (uint32_t lfa, const void *wname, uint32_t wnlen, uint32_t hash,
                                 int allowvochid)
{
  uint32_t res = 0;
  #ifdef UFO_DEBUG_FIND_WORD
  fprintf(stderr, "CHECK-NAME: %.*s; LFA: 0x%08x; hash: 0x%08x (wname: 0x%08x)\n",
          (unsigned) wnlen, (const char *)wname,
          lfa, (lfa != 0 ? ufoImgGetU32(UFO_LFA_TO_NHASH(lfa)) : 0), hash);
  ufoDumpWordHeader(lfa);
  #endif
  if (lfa != 0 && ufoImgGetU32(UFO_LFA_TO_NHASH(lfa)) == hash) {
    const uint32_t lenflags = ufoImgGetU32(UFO_LFA_TO_NFA(lfa));
    if ((lenflags & UFW_FLAG_SMUDGE) == 0 &&
        (allowvochid || (lenflags & UFW_FLAG_HIDDEN) == 0))
    {
      const uint32_t nlen = lenflags&0xffU;
      if (nlen == wnlen) {
        uint32_t naddr = UFO_LFA_TO_NFA(lfa) + 4u;
        uint32_t pos = 0;
        while (pos < nlen) {
          uint8_t c0 = ((const unsigned char *)wname)[pos];
          if (c0 >= 'a' && c0 <= 'z') c0 = c0 - 'a' + 'A';
          uint8_t c1 = ufoImgGetU8(naddr + pos);
          if (c1 >= 'a' && c1 <= 'z') c1 = c1 - 'a' + 'A';
          if (c0 != c1) break;
          pos += 1u;
        }
        if (pos == nlen) {
          // i found her!
          naddr += pos + 1u;
          res = UFO_ALIGN4(naddr);
        }
      }
    }
  }
  return res;
}


//==========================================================================
//
//  ufoFindWordInVoc
//
//  return 0 or CFA
//
//==========================================================================
static uint32_t ufoFindWordInVoc (const void *wname, uint32_t wnlen, uint32_t hash,
                                  uint32_t vocid, int allowvochid)
{
  uint32_t res = 0;
  if (wname == NULL) ufo_assert(wnlen == 0);
  if (wnlen != 0 && vocid != 0) {
    if (hash == 0) hash = joaatHashBufCI(wname, wnlen);
    #if defined(UFO_DEBUG_FIND_WORD) || defined(UFO_DEBUG_FIND_WORD_IN_VOC)
    fprintf(stderr, "IN-VOC: %.*s; VOCID: 0x%08x; whash: 0x%08x; htbl[0]: 0x%08x\n",
            (unsigned) wnlen, (const char *)wname,
            vocid, hash, ufoImgGetU32(vocid + UFW_VOCAB_OFS_HTABLE));
    #endif
    const uint32_t htbl = vocid + UFW_VOCAB_OFS_HTABLE;
    if (ufoImgGetU32(htbl) != UFO_NO_HTABLE_FLAG) {
      // hash table present, use it
      uint32_t bfa = htbl + (hash % (uint32_t)UFO_HASHTABLE_SIZE) * 4u;
      bfa = ufoImgGetU32(bfa);
      while (res == 0 && bfa != 0) {
        #if defined(UFO_DEBUG_FIND_WORD) || defined(UFO_DEBUG_FIND_WORD_IN_VOC)
        fprintf(stderr, "IN-VOC:   bfa: 0x%08x\n", bfa);
        #endif
        res = ufoVocCheckName(UFO_BFA_TO_LFA(bfa), wname, wnlen, hash, allowvochid);
        bfa = ufoImgGetU32(bfa);
      }
    } else {
      // no hash table, use linear search
      uint32_t lfa = vocid + UFW_VOCAB_OFS_LATEST;
      lfa = ufoImgGetU32(lfa);
      while (res == 0 && lfa != 0) {
        res = ufoVocCheckName(lfa, wname, wnlen, hash, allowvochid);
        lfa = ufoImgGetU32(lfa);
      }
    }
  }
  return res;
}


//==========================================================================
//
//  ufoFindColon
//
//  return part after the colon, or `NULL`
//
//==========================================================================
static const void *ufoFindColon (const void *wname, uint32_t wnlen) {
  const void *res = NULL;
  if (wnlen != 0) {
    ufo_assert(wname != NULL);
    const char *str = (const char *)wname;
    while (wnlen != 0 && str[0] != ':') {
      str += 1; wnlen -= 1;
    }
    if (wnlen != 0) {
      res = (const void *)(str + 1); // skip colon
    }
  }
  return res;
}


//==========================================================================
//
//  ufoFindWordInVocAndParents
//
//==========================================================================
static uint32_t ufoFindWordInVocAndParents (const void *wname, uint32_t wnlen, uint32_t hash,
                                            uint32_t vocid, int allowvochid)
{
  uint32_t res = 0;
  if (hash == 0) hash = joaatHashBufCI(wname, wnlen);
  while (res == 0 && vocid != 0) {
    res = ufoFindWordInVoc(wname, wnlen, hash, vocid, allowvochid);
    vocid = ufoImgGetU32(vocid + UFW_VOCAB_OFS_PARENT);
  }
  return res;
}


//==========================================================================
//
//  ufoFindWordNameRes
//
//  find with name resolution
//
//  return 0 or CFA
//
//==========================================================================
static uint32_t ufoFindWordNameRes (const void *wname, uint32_t wnlen) {
  uint32_t res = 0;
  if (wnlen != 0 && *(const char *)wname != ':') {
    ufo_assert(wname != NULL);

    const void *stx = wname;
    wname = ufoFindColon(wname, wnlen);
    if (wname != NULL && wname != stx + wnlen) {
      // look in all vocabs (excluding hidden ones)
      uint32_t xlen = (uint32_t)(ptrdiff_t)(wname - stx) - 1u;
      ufo_assert(xlen > 0 && xlen < 255);
      uint32_t xhash = joaatHashBufCI(stx, xlen);
      uint32_t voclink = ufoImgGetU32(ufoAddrVocLink);
      #ifdef UFO_DEBUG_FIND_WORD_COLON
      fprintf(stderr, "COLON-FIND: first voc: {%.*s}; xhash=0x%08x; voc-link: 0x%08x\n",
              (unsigned)xlen, (const char *)stx, xhash, voclink);
      #endif
      while (res == 0 && voclink != 0) {
        const uint32_t vhdraddr = voclink - UFW_VOCAB_OFS_VOCLINK + UFW_VOCAB_OFS_HEADER;
        const uint32_t vhdr = ufoImgGetU32(vhdraddr);
        if (vhdr != 0) {
          res = ufoVocCheckName(UFO_NFA_TO_LFA(vhdr), stx, xlen, xhash, 0);
        }
        if (res == 0) voclink = ufoImgGetU32(voclink);
      }
      if (res != 0) {
        uint32_t vocid = voclink - UFW_VOCAB_OFS_VOCLINK;
        ufo_assert(voclink != 0);
        wnlen -= xlen + 1;
        #ifdef UFO_DEBUG_FIND_WORD_COLON
        fprintf(stderr, "searching {%.*s}(%u) in {%.*s}\n",
                (unsigned)wnlen, wname, wnlen, (unsigned)xlen, stx);
        #endif
        while (res != 0 && wname != NULL) {
          // first, the whole rest
          res = ufoFindWordInVocAndParents(wname, wnlen, 0, vocid, 1);
          if (res != 0) {
            wname = NULL;
          } else {
            stx = wname;
            wname = ufoFindColon(wname, wnlen);
            if (wname == NULL) xlen = wnlen; else xlen = (uint32_t)(ptrdiff_t)(wname - stx) - 1u;
            ufo_assert(xlen > 0 && xlen < 255);
            res = ufoFindWordInVocAndParents(stx, xlen, 0, vocid, 1);
            if (res != 0) {
              wnlen -= xlen + 1;
              if (wname != NULL) {
                // it should be a vocabulary
                const uint32_t nfa = UFO_CFA_TO_NFA(res);
                if ((ufoImgGetU32(nfa) & UFW_FLAG_VOCAB) != 0) {
                  vocid = ufoImgGetU32(UFO_CFA_TO_PFA(res)); // pfa points to vocabulary
                } else {
                  res = 0;
                }
              }
            }
          }
        }
      }
    }
  }

  return res;
}


//==========================================================================
//
//  ufoFindWord
//
//  doesn't look in CURRENT, does name resolution ("a:b" is word "b" in "a")
//
//  return 0 or CFA
//
//==========================================================================
static uint32_t ufoFindWord (const char *wname) {
  uint32_t res = 0;
  if (wname && wname[0] != 0) {
    const size_t wnlen = strlen(wname);
    ufo_assert(wnlen < 8192);
    uint32_t ctx = ufoImgGetU32(ufoAddrContext);
    const uint32_t hash = joaatHashBufCI(wname, (uint32_t)wnlen);

    //fprintf(stderr, "FIND-WORD: whash: 0x%08x; name:{%s}\n", hash, wname);

    // first search in context
    res = ufoFindWordInVocAndParents(wname, (uint32_t)wnlen, hash, ctx, (ctx == ufoImgGetU32(ufoAddrCurrent)));

    // now try vocabulary stack
    uint32_t vstp = ufoVSP;
    while (res == 0 && vstp != 0) {
      vstp -= 1;
      ctx = ufoVocStack[vstp];
      res = ufoFindWordInVocAndParents(wname, (uint32_t)wnlen, hash, ctx, (ctx == ufoImgGetU32(ufoAddrCurrent)));
    }

    // if not found, try name resolution
    if (res == 0) res = ufoFindWordNameRes(wname, (uint32_t)wnlen);
  }

  return res;
}


//==========================================================================
//
//  ufoCreateWordHeader
//
//  create word header up to CFA, link to the current dictionary
//
//==========================================================================
static void ufoCreateWordHeader (const char *wname, uint32_t flags) {
  if (wname == NULL) wname = "";
  const size_t wnlen = strlen(wname);
  ufo_assert(wnlen < UFO_MAX_WORD_LENGTH);
  const uint32_t hash = joaatHashBufCI(wname, (uint32_t)wnlen);
  const uint32_t curr = ufoImgGetU32(ufoAddrCurrent);
  ufo_assert(curr != 0);

  // redefine check
  const uint32_t warn = ufoImgGetU32(ufoAddrRedefineWarning);
  if (wnlen != 0 && warn != UFO_REDEF_WARN_DONT_CARE) {
    uint32_t cfa;
    if (warn != UFO_REDEF_WARN_PARENTS) {
      cfa = ufoFindWordInVoc(wname, wnlen, hash, curr, 1);
    } else {
      cfa = ufoFindWordInVocAndParents(wname, wnlen, hash, curr, 1);
    }
    if (cfa != 0) {
      const uint32_t nfa = UFO_CFA_TO_NFA(cfa);
      const uint32_t flags = ufoImgGetU32(nfa);
      if ((flags & UFW_FLAG_PROTECTED) != 0) {
        ufoFatal("trying to redefine protected word '%s'", wname);
      } else if (warn != UFO_REDEF_WARN_NONE) {
        ufoWarning("redefining word '%s'", wname);
      }
    }
  }

  const uint32_t bkt = (hash % (uint32_t)UFO_HASHTABLE_SIZE) * 4u;
  const uint32_t htbl = curr + UFW_VOCAB_OFS_HTABLE;

  ufoImgEmitAlign();
  const uint32_t xfaAddr = UFO_GET_DP();
  if ((xfaAddr & UFO_ADDR_TEMP_BIT) == 0) {
    // link previous yfa here
    const uint32_t lastxfa = ufoImgGetU32(ufoAddrLastXFA);
    // fix YFA of the previous word (it points to our YFA)
    if (lastxfa != 0) {
      ufoImgPutU32(UFO_XFA_TO_YFA(lastxfa), UFO_XFA_TO_YFA(xfaAddr));
    }
    // our XFA points to the previous XFA
    ufoImgEmitU32(lastxfa); // xfa
    // update last XFA
    ufoImgPutU32(ufoAddrLastXFA, xfaAddr);
  } else {
    ufoImgEmitU32(0); // xfa
  }
  ufoImgEmitU32(0); // yfa

  // bucket link (bfa)
  if (wnlen == 0 || ufoImgGetU32(htbl) == UFO_NO_HTABLE_FLAG) {
    ufoImgEmitU32(0);
  } else {
    #ifdef UFO_DEBUG_DUMP_NEW_HEADERS
    fprintf(stderr, "NEW HEADER: %s; curr: 0x%08x; htbl: 0x%08x; bkt: 0x%08x\n",
            wname, curr, htbl, bkt);
    fprintf(stderr, "  [bkt]: 0x%08x; nbk: 0x%08x\n", ufoImgGetU32(htbl + bkt), UFO_GET_DP());
    #endif
    // bfa points to bfa
    const uint32_t bfa = UFO_GET_DP();
    ufoImgEmitU32(ufoImgGetU32(htbl + bkt));
    ufoImgPutU32(htbl + bkt, bfa);
  }

  // lfa
  const uint32_t lfa = UFO_GET_DP();
  ufoImgEmitU32(ufoImgGetU32(curr + UFW_VOCAB_OFS_LATEST));
  // fix voc latest
  ufoImgPutU32(curr + UFW_VOCAB_OFS_LATEST, lfa);
  // name hash
  ufoImgEmitU32(hash);
  // name length
  const uint32_t nfa = UFO_GET_DP();
  ufoImgEmitU32(((uint32_t)wnlen&0xffU) | (flags & 0xffffff00U));
  const uint32_t nstart = UFO_GET_DP();
  // put name
  for (size_t f = 0; f < wnlen; f += 1) {
    ufoImgEmitU8(((const unsigned char *)wname)[f]);
  }
  while ((UFO_GET_DP() & 3) != 3) ufoImgEmitU8(0);
  const uint32_t nend = UFO_GET_DP(); // length byte itself is not included
  // name length, again
  ufo_assert(nend - nstart <= 255);
  ufoImgEmitU8((uint8_t)(nend - nstart));
  ufo_assert((UFO_GET_DP() & 3) == 0);
  ufo_assert(UFO_CFA_TO_NFA(UFO_GET_DP()) == nfa);
  if ((nend & UFO_ADDR_SPECIAL_BITS_MASK) == 0) ufoRecordDebug(nend);
  #ifdef UFO_DEBUG_DUMP_NEW_HEADERS
  fprintf(stderr, "*** NEW HEADER ***\n");
  fprintf(stderr, "CFA: 0x%08x\n", UFO_GET_DP());
  fprintf(stderr, "NSTART: 0x%08x\n", nstart);
  fprintf(stderr, "NEND: 0x%08x\n", nend);
  fprintf(stderr, "NLEN: %u (%u)\n", nend - nstart, ufoImgGetU8(UFO_GET_DP() - 1u));
  ufoDumpWordHeader(lfa);
  #endif
  #if 0
  fprintf(stderr, "NEW WORD CFA 0x%08x: %s\n", UFO_GET_DP(), wname);
  #endif
}


//==========================================================================
//
//  ufoDecompilePart
//
//==========================================================================
static void ufoDecompilePart (uint32_t addr, uint32_t eaddr, int indent) {
  uint32_t count;
  FILE *fo = stdout;
  while (addr < eaddr) {
    uint32_t cfa = ufoImgGetU32(addr);
    for (int n = 0; n < indent; n += 1) fputc(' ', fo);
    fprintf(fo, "%6u: 0x%08x: ", addr, cfa);
    uint32_t nfa = UFO_CFA_TO_NFA(cfa);
    uint32_t flags = ufoImgGetU32(nfa);
    //fprintf(fo, "[0x%08x] ", flags & UFW_WARG_MASK);
    uint32_t nlen = flags & 0xffU;
    for (uint32_t f = 0; f < nlen; f += 1) {
      const uint8_t ch = ufoImgGetU8(nfa + 4u + f);
      if (ch <= 32 || ch >= 127) {
        fprintf(fo, "\\x%02x", ch);
      } else {
        fprintf(fo, "%c", (char)ch);
      }
    }
    addr += 4u;
    switch (flags & UFW_WARG_MASK) {
      case UFW_WARG_NONE:
        break;
      case UFW_WARG_BRANCH:
        #ifdef UFO_RELATIVE_BRANCH
        fprintf(fo, "  @%u", addr + ufoImgGetU32(addr)); addr += 4u;
        #else
        fprintf(fo, "  @%u", ufoImgGetU32(addr)); addr += 4u;
        #endif
        break;
      case UFW_WARG_LIT:
        fprintf(fo, "  %u : %d : 0x%08x", ufoImgGetU32(addr),
                (int32_t)ufoImgGetU32(addr), ufoImgGetU32(addr)); addr += 4u;
        break;
      case UFW_WARG_C4STRZ:
        count = ufoImgGetU32(addr); addr += 4;
       print_str:
        fprintf(fo, "  str:");
        for (int f = 0; f < count; f += 1) {
          const uint8_t ch = ufoImgGetU8(addr); addr += 1u;
          if (ch <= 32 || ch >= 127) {
            fprintf(fo, "\\x%02x", ch);
          } else {
            fprintf(fo, "%c", (char)ch);
          }
        }
        addr += 1u; // skip zero byte
        addr = UFO_ALIGN4(addr);
        break;
      case UFW_WARG_CFA:
        cfa = ufoImgGetU32(addr); addr += 4u;
        fprintf(fo, "  CFA:%u: ", cfa);
        nfa = UFO_CFA_TO_NFA(cfa);
        nlen = ufoImgGetU8(nfa);
        for (uint32_t f = 0; f < nlen; f += 1) {
          const uint8_t ch = ufoImgGetU8(nfa + 4u + f);
          if (ch <= 32 || ch >= 127) {
            fprintf(fo, "\\x%02x", ch);
          } else {
            fprintf(fo, "%c", (char)ch);
          }
        }
        break;
      case UFW_WARG_PFA:
        cfa = ufoImgGetU32(addr); addr += 4u;
        fprintf(fo, "  PFA:%u: ", cfa);
        cfa = UFO_PFA_TO_CFA(cfa);
        nfa = UFO_CFA_TO_NFA(cfa);
        nlen = ufoImgGetU8(nfa);
        for (uint32_t f = 0; f < nlen; f += 1) {
          const uint8_t ch = ufoImgGetU8(nfa + 4u + f);
          if (ch <= 32 || ch >= 127) {
            fprintf(fo, "\\x%02x", ch);
          } else {
            fprintf(fo, "%c", (char)ch);
          }
        }
        break;
      case UFW_WARG_CBLOCK:
        fprintf(fo, "  CBLOCK:%u", ufoImgGetU32(addr)); addr += 4u;
        break;
      case UFW_WARG_VOCID:
        fprintf(fo, "  VOCID:%u", ufoImgGetU32(addr)); addr += 4u;
        break;
      case UFW_WARG_C1STRZ:
        count = ufoImgGetU8(addr); addr += 1;
        goto print_str;
      case UFW_WARG_DATASKIP:
        fprintf(fo, "  DATA:%u", ufoImgGetU32(addr));
        addr += UFO_ALIGN4(4u + ufoImgGetU32(addr));
        break;
      default:
        fprintf(fo, " -- WTF?!\n");
        abort();
    }
    fputc('\n', fo);
  }
}


//==========================================================================
//
//  ufoDecompileWord
//
//==========================================================================
static void ufoDecompileWord (const uint32_t cfa) {
  if (cfa != 0) {
    const uint32_t lfa = UFO_CFA_TO_LFA(cfa);
    fprintf(stdout, "#### DECOMPILING CFA %u ###\n", cfa);
    ufoDumpWordHeader(lfa);
    const uint32_t yfa = ufoGetWordEndAddr(cfa);
    if (ufoImgGetU32(cfa) == ufoDoForthCFA) {
      fprintf(stdout, "--- DECOMPILED CODE ---\n");
      ufoDecompilePart(UFO_CFA_TO_PFA(cfa), yfa, 0);
      fprintf(stdout, "=======================\n");
    }
  }
}


//==========================================================================
//
//  ufoBTShowWordName
//
//==========================================================================
static void ufoBTShowWordName (uint32_t nfa) {
  if (nfa != 0) {
    uint32_t len = ufoImgGetU8(nfa); nfa += 4u;
    //fprintf(stderr, "(0x%08x)", ufoImgGetU32(nfa - 4u));
    while (len != 0) {
      uint8_t ch = ufoImgGetU8(nfa); nfa += 1u; len -= 1u;
      if (ch <= 32 || ch >= 127) {
        fprintf(stderr, "\\x%02x", ch);
      } else {
        fprintf(stderr, "%c", (char)ch);
      }
    }
  }
}


//==========================================================================
//
//  ufoBacktrace
//
//==========================================================================
static void ufoBacktrace (uint32_t ip, int showDataStack) {
  // dump data stack (top 16)
  ufoFlushOutput();
  if (!ufoLastEmitWasCR) { printf("\n"); ufoLastEmitWasCR = 1; }

  if (showDataStack) {
    fprintf(stderr, "***UFO STACK DEPTH: %u\n", ufoSP);
    uint32_t xsp = ufoSP;
    if (xsp > 16) xsp = 16;
    for (uint32_t sp = 0; sp < xsp; ++sp) {
      fprintf(stderr, "  %2u: 0x%08x  %d%s\n",
              sp, ufoDStack[xsp - sp - 1], (int32_t)ufoDStack[xsp - sp - 1],
              (sp == 0 ? " -- TOS" : ""));
    }
    if (ufoSP > 16) fprintf(stderr, "    ...more...\n");
  }

  // dump return stack (top 32)
  uint32_t nfa;
  uint32_t fline;
  const char *fname;

  fprintf(stderr, "***UFO RETURN STACK DEPTH: %u\n", ufoRP);
  if (ip != 0) {
    nfa = ufoFindWordForIP(ip);
    if (nfa != 0) {
      fprintf(stderr, "  **: %8u -- ", ip);
      ufoBTShowWordName(nfa);
      fname = ufoFindFileForIP(ip, &fline, NULL, NULL);
      if (fname != NULL) { fprintf(stderr, " (at %s:%u)", fname, fline); }
      fputc('\n', stderr);
    }
  }
  uint32_t rp = ufoRP;
  uint32_t rscount = 0;
  if (rp > UFO_RSTACK_SIZE) rp = UFO_RSTACK_SIZE;
  while (rscount != 32 && rp != 0) {
    rp -= 1;
    const uint32_t val = ufoRStack[rp];
    nfa = ufoFindWordForIP(val - 4u);
    if (nfa != 0) {
      fprintf(stderr, "  %2u: %8u -- ", ufoRP - rp - 1u, val);
      ufoBTShowWordName(nfa);
      fname = ufoFindFileForIP(val - 4u, &fline, NULL, NULL);
      if (fname != NULL) { fprintf(stderr, " (at %s:%u)", fname, fline); }
      fputc('\n', stderr);
    } else {
      fprintf(stderr, "  %2u: 0x%08x  %d\n", ufoRP - rp - 1u, val, (int32_t)val);
    }
    rscount += 1;
  }
  if (ufoRP > 32) fprintf(stderr, "    ...more...\n");

  ufoFlushOutput();
}


//==========================================================================
//
//  ufoDumpVocab
//
//==========================================================================
/*
static void ufoDumpVocab (uint32_t vocid) {
  if (vocid != 0) {
    fprintf(stderr, "*** VOCID: 0x%08x ***\n", vocid);
    uint32_t vochdr = vocid + UFW_VOCAB_OFS_HEADER;
    vochdr = ufoImgGetU32(vochdr);
    if (vochdr != 0) {
      fprintf(stderr, "--- HEADER ---\n");
      ufoDumpWordHeader(UFO_NFA_TO_LFA(vochdr));
      fprintf(stderr, "========\n");
      uint32_t htbl = vocid + UFW_VOCAB_OFS_HTABLE;
      if (ufoImgGetU32(htbl) != UFO_NO_HTABLE_FLAG) {
        fprintf(stderr, "--- HASH TABLE ---\n");
        for (int f = 0; f < UFO_HASHTABLE_SIZE; f += 1) {
          uint32_t bfa = ufoImgGetU32(htbl);
          if (bfa != 0) {
            fprintf(stderr, "**** %2d: 0x%08x\n", f, bfa);
            do {
              ufoDumpWordHeader(UFO_BFA_TO_LFA(bfa));
              bfa = ufoImgGetU32(bfa);
            } while (bfa != 0);
          }
          htbl += 4u;
        }
      }
    }
  }
}
*/


// if set, this will be used when we are out of include files. intended for UrAsm.
// return 0 if there is no more lines, otherwise the string should be copied
// to buffer, `*fname` and `*fline` should be properly set.
int (*ufoFileReadLine) (void *buf, size_t bufsize, const char **fname, int *fline) = NULL;


//==========================================================================
//
//  ufoLoadNextUserLine
//
//==========================================================================
static int ufoLoadNextUserLine (void) {
  uint32_t tibPos = 0;
  const char *fname = NULL;
  int fline = 0;
  ufoResetTib();
  if (ufoFileReadLine != NULL && ufoFileReadLine(ufoCurrFileLine, 510, &fname, &fline) != 0) {
    ufoCurrFileLine[510] = 0;
    uint32_t slen = (uint32_t)strlen(ufoCurrFileLine);
    while (slen != 0 && (ufoCurrFileLine[slen - 1u] == 10 || ufoCurrFileLine[slen - 1u] == 13)) {
      slen -= 1u;
    }
    if (tibPos + slen + 1u > UFO_ADDR_HANDLE_OFS_MASK) {
      ufoFatal("input text line too long (at least %u bytes)", tibPos + slen);
    }
    ufoTibEnsureSize(tibPos + slen + 1u);
    for (uint32_t f = 0; f < slen; f += 1) {
      ufoTibPokeChOfs(((const unsigned char *)ufoCurrFileLine)[f], tibPos + f);
    }
    ufoTibPokeChOfs(0, tibPos + slen);
    tibPos += slen;
    if (fname == NULL) fname = "<user>";
    ufoSetInFileName(fname);
    ufoInFileLine = fline;
    return 1;
  } else {
    return 0;
  }
}


//==========================================================================
//
//  ufoLoadNextLine_NativeMode
//
//  load next file line into TIB
//  always strips final '\n'
//
//  return 0 on EOF, 1 on success
//
//==========================================================================
static int ufoLoadNextLine (int crossInclude) {
  int done = 0;
  uint32_t tibPos = 0;
  ufoResetTib();

  if (ufoMode == UFO_MODE_MACRO) {
    //fprintf(stderr, "***MAC!\n");
    return 0;
  }

  while (ufoInFile != NULL && !done) {
    ufoCurrIncludeLineFileOfs = ftell(ufoInFile);
    if (fgets(ufoCurrFileLine, 510, ufoInFile) != NULL) {
      // check for a newline
      // if there is no newline char at the end, the string was truncated
      ufoCurrFileLine[510] = 0;
      const uint32_t slen = (uint32_t)strlen(ufoCurrFileLine);
      if (tibPos + slen + 1u > UFO_ADDR_HANDLE_OFS_MASK) {
        ufoFatal("input text line too long (at least %u bytes)", tibPos + slen);
      }
      ufoTibEnsureSize(tibPos + slen + 1u);
      for (uint32_t f = 0; f < slen; f += 1) {
        ufoTibPokeChOfs(((const unsigned char *)ufoCurrFileLine)[f], tibPos + f);
      }
      ufoTibPokeChOfs(0, tibPos + slen);
      tibPos += slen;
      if (slen != 0 && (ufoCurrFileLine[slen - 1u] == 13 || ufoCurrFileLine[slen - 1u] == 10)) {
        ++ufoInFileLine;
        done = 1;
      } else {
        // continuation, nothing to do
      }
    } else {
      // if we read nothing, this is EOF
      if (tibPos == 0 && crossInclude) {
        // we read nothing, and allowed to cross include boundaries
        ufoPopInFile();
      } else {
        done = 1;
      }
    }
  }

  if (tibPos == 0) {
    // eof, try user-supplied input
    if (ufoFileStackPos == 0) {
      return ufoLoadNextUserLine();
    } else {
      return 0;
    }
  } else {
    // if we read at least something, this is not EOF
    return 1;
  }
}


// ////////////////////////////////////////////////////////////////////////// //
// debug

// DUMP-STACK
// ( -- )
UFWORD(DUMP_STACK) {
  if (!ufoLastEmitWasCR) { printf("\n"); ufoLastEmitWasCR = 1; }
  printf("***UFO STACK DEPTH: %u\n", ufoSP);
  uint32_t xsp = ufoSP;
  if (xsp > 16) xsp = 16;
  for (uint32_t sp = 0; sp < xsp; ++sp) {
    printf("  %2u: 0x%08x  %d%s\n",
           sp, ufoDStack[xsp - sp - 1], (int32_t)ufoDStack[xsp - sp - 1],
           (sp == 0 ? " -- TOS" : ""));
  }
  if (ufoSP > 16) printf("    ...more...\n");
  ufoLastEmitWasCR = 1;
}

// BACKTRACE
// ( -- )
UFWORD(UFO_BACKTRACE) {
  ufoFlushOutput();
  if (!ufoLastEmitWasCR) { printf("\n"); ufoLastEmitWasCR = 1; }
  if (ufoInFile != NULL) {
    fprintf(stderr, "*** at file %s, line %d ***\n", ufoInFileName, ufoInFileLine);
  } else {
    fprintf(stderr, "*** somewhere in time ***\n");
  }
  ufoBacktrace(ufoIP, 1);
}

#ifdef UFO_MTASK_ALLOWED
// DUMP-STACK-TASK
// ( stid -- )
UFWORD(DUMP_STACK_TASK) {
  UfoState *st = ufoFindState(ufoPop());
  if (st == NULL) ufoFatal("invalid state id");
  // temporarily switch the task
  UfoState *oldst = ufoCurrState; ufoCurrState = st;
  // dump
  if (!ufoLastEmitWasCR) { printf("\n"); ufoLastEmitWasCR = 1; }
  printf("***UFO STACK DEPTH: %u\n", ufoSP);
  uint32_t xsp = ufoSP;
  if (xsp > 16) xsp = 16;
  for (uint32_t sp = 0; sp < xsp; ++sp) {
    printf("  %2u: 0x%08x  %d%s\n",
           sp, ufoDStack[xsp - sp - 1], (int32_t)ufoDStack[xsp - sp - 1],
           (sp == 0 ? " -- TOS" : ""));
  }
  if (ufoSP > 16) printf("    ...more...\n");
  ufoLastEmitWasCR = 1;
  // restore state
  ufoCurrState = oldst;
}

// DUMP-RSTACK-TASK
// ( stid -- )
UFWORD(DUMP_RSTACK_TASK) {
  UfoState *st = ufoFindState(ufoPop());
  if (st == NULL) ufoFatal("invalid state id");
  // temporarily switch the task
  UfoState *oldst = ufoCurrState; ufoCurrState = st;
  // dump
  ufoFlushOutput();
  if (!ufoLastEmitWasCR) { printf("\n"); ufoLastEmitWasCR = 1; }
  if (ufoInFile != NULL) {
    fprintf(stderr, "*** at file %s, line %d ***\n", ufoInFileName, ufoInFileLine);
  } else {
    fprintf(stderr, "*** somewhere in time ***\n");
  }
  ufoBacktrace(ufoIP, 0);
  // restore state
  ufoCurrState = oldst;
}

// BACKTRACE-TASK
// ( stid -- )
UFWORD(UFO_BACKTRACE_TASK) {
  UfoState *st = ufoFindState(ufoPop());
  if (st == NULL) ufoFatal("invalid state id");
  // temporarily switch the task
  UfoState *oldst = ufoCurrState; ufoCurrState = st;
  // dump
  ufoFlushOutput();
  if (!ufoLastEmitWasCR) { printf("\n"); ufoLastEmitWasCR = 1; }
  if (ufoInFile != NULL) {
    fprintf(stderr, "*** at file %s, line %d ***\n", ufoInFileName, ufoInFileLine);
  } else {
    fprintf(stderr, "*** somewhere in time ***\n");
  }
  ufoBacktrace(ufoIP, 1);
  // restore state
  ufoCurrState = oldst;
}
#endif


// ////////////////////////////////////////////////////////////////////////// //
// some init words, and PAD
//

// NOOP
// ( -- )
UFWORD(NOOP) {}

// (NOTIMPL)
// ( -- )
UFWORD(PAR_NOTIMPL) {
  ufoFatal("not implemented");
}

// SP0!
// ( -- )
UFWORD(SP0_STORE) { ufoSP = 0; }

// RP0!
// ( -- )
UFWORD(RP0_STORE) {
  ufoRP = 0;
}

// PAD
// ( -- pad )
// PAD is at the beginning of temp area
UFWORD(PAD) {
  ufoPush(UFO_PAD_ADDR);
}

// HERE
// ( -- here )
UFWORD(HERE) {
  ufoPush(UFO_GET_DP());
}

// ALIGN-HERE
// ( -- )
UFWORD(ALIGN_HERE) {
  ufoImgEmitAlign();
}


// ////////////////////////////////////////////////////////////////////////// //
// peeks and pokes with address register
//

// A>
// ( -- regA )
UFWORD(REGA_LOAD) {
  ufoPush(ufoRegA);
}

// >A
// ( regA -- )
UFWORD(REGA_STORE) {
  ufoRegA = ufoPop();
}

// A-SWAP
// ( regA -- oldA )
// swap TOS and A
UFWORD(REGA_SWAP) {
  const uint32_t newa = ufoPop();
  ufoPush(ufoRegA);
  ufoRegA = newa;
}

// +1>A
// ( -- )
UFWORD(REGA_INC) {
  ufoRegA += 1u;
}

// +2>A
// ( -- )
UFWORD(REGA_INC_WORD) {
  ufoRegA += 2u;
}

// +4>A
// ( -- )
UFWORD(REGA_INC_CELL) {
  ufoRegA += 4u;
}

// -1>A
// ( -- )
UFWORD(REGA_DEC) {
  ufoRegA -= 1u;
}

// -2>A
// ( -- )
UFWORD(REGA_DEC_WORD) {
  ufoRegA -= 2u;
}

// -4>A
// ( -- )
UFWORD(REGA_DEC_CELL) {
  ufoRegA -= 4u;
}

// A>R
// ( -- | rega )
UFWORD(REGA_TO_R) {
  ufoRPush(ufoRegA);
}

// R>A
// ( | rega -- )
UFWORD(R_TO_REGA) {
  ufoRegA = ufoRPop();
}


// ////////////////////////////////////////////////////////////////////////// //
// useful to work with handles and normal addreses uniformly
//

// C@A
// ( -- byte )
UFWORD(CPEEK_REGA) {
  ufoPush(ufoImgGetU8(ufoRegA));
}

// W@A
// ( -- word )
UFWORD(WPEEK_REGA) {
  ufoPush(ufoImgGetU16(ufoRegA));
}

// @A
// ( -- value )
UFWORD(PEEK_REGA) {
  ufoPush(ufoImgGetU32(ufoRegA));
}

// C!A
// ( byte -- )
UFWORD(CPOKE_REGA) {
  ufoImgPutU8(ufoRegA, ufoPop());
}

// W!A
// ( word -- )
UFWORD(WPOKE_REGA) {
  ufoImgPutU16(ufoRegA, ufoPop());
}

// !A
// ( value -- )
UFWORD(POKE_REGA) {
  ufoImgPutU32(ufoRegA, ufoPop());
}

// C@A+
// ( idx -- byte )
UFWORD(CPEEK_REGA_IDX) {
  if ((ufoRegA & UFO_ADDR_HANDLE_BIT) == 0) {
    UFO_STACK(1);
    const uint32_t newaddr = ufoRegA + UFO_TOS;
    if ((ufoRegA & UFO_ADDR_SPECIAL_BITS_MASK) == (newaddr & UFO_ADDR_SPECIAL_BITS_MASK)) {
      UFO_TOS = ufoImgGetU8(newaddr);
    } else {
      ufoFatal("address offset out of range; addr=0x%08x; offset=%u; res=0x%08x",
               ufoRegA, UFO_TOS, newaddr);
    }
  } else {
    ufoPush(ufoRegA);
    UFCALL(PAR_HANDLE_LOAD_BYTE);
  }
}

// W@A+
// ( idx -- word )
UFWORD(WPEEK_REGA_IDX) {
  if ((ufoRegA & UFO_ADDR_HANDLE_BIT) == 0) {
    UFO_STACK(1);
    const uint32_t newaddr = ufoRegA + UFO_TOS;
    if ((ufoRegA & UFO_ADDR_SPECIAL_BITS_MASK) == (newaddr & UFO_ADDR_SPECIAL_BITS_MASK) &&
        (ufoRegA & UFO_ADDR_SPECIAL_BITS_MASK) == ((newaddr + 1u) & UFO_ADDR_SPECIAL_BITS_MASK))
    {
      UFO_TOS = ufoImgGetU16(newaddr);
    } else {
      ufoFatal("address offset out of range; addr=0x%08x; offset=%u; res=0x%08x",
               ufoRegA, UFO_TOS, newaddr);
    }
  } else {
    ufoPush(ufoRegA);
    UFCALL(PAR_HANDLE_LOAD_WORD);
  }
}

// @A+
// ( idx -- value )
UFWORD(PEEK_REGA_IDX) {
  if ((ufoRegA & UFO_ADDR_HANDLE_BIT) == 0) {
    UFO_STACK(1);
    const uint32_t newaddr = ufoRegA + UFO_TOS;
    if ((ufoRegA & UFO_ADDR_SPECIAL_BITS_MASK) == (newaddr & UFO_ADDR_SPECIAL_BITS_MASK) &&
        (ufoRegA & UFO_ADDR_SPECIAL_BITS_MASK) == ((newaddr + 3u) & UFO_ADDR_SPECIAL_BITS_MASK))
    {
      UFO_TOS = ufoImgGetU32(newaddr);
    } else {
      ufoFatal("address offset out of range; addr=0x%08x; offset=%u; res=0x%08x",
               ufoRegA, UFO_TOS, newaddr);
    }
  } else {
    ufoPush(ufoRegA);
    UFCALL(PAR_HANDLE_LOAD_CELL);
  }
}

// C!A+
// ( byte idx -- )
UFWORD(CPOKE_REGA_IDX) {
  if ((ufoRegA & UFO_ADDR_HANDLE_BIT) == 0) {
    const uint32_t idx = ufoPop();
    const uint32_t newaddr = ufoRegA + idx;
    if ((ufoRegA & UFO_ADDR_SPECIAL_BITS_MASK) == (newaddr & UFO_ADDR_SPECIAL_BITS_MASK)) {
      const uint32_t value = ufoPop();
      ufoImgPutU8(newaddr, value);
    } else {
      ufoFatal("address offset out of range; addr=0x%08x; offset=%u; res=0x%08x",
               ufoRegA, idx, newaddr);
    }
  } else {
    ufoPush(ufoRegA);
    UFCALL(PAR_HANDLE_STORE_BYTE);
  }
}

// W!A+
// ( word idx -- )
UFWORD(WPOKE_REGA_IDX) {
  if ((ufoRegA & UFO_ADDR_HANDLE_BIT) == 0) {
    const uint32_t idx = ufoPop();
    const uint32_t newaddr = ufoRegA + idx;
    if ((ufoRegA & UFO_ADDR_SPECIAL_BITS_MASK) == (newaddr & UFO_ADDR_SPECIAL_BITS_MASK) &&
        (ufoRegA & UFO_ADDR_SPECIAL_BITS_MASK) == ((newaddr + 1u) & UFO_ADDR_SPECIAL_BITS_MASK))
    {
      const uint32_t value = ufoPop();
      ufoImgPutU16(newaddr, value);
    } else {
      ufoFatal("address offset out of range; addr=0x%08x; offset=%u; res=0x%08x",
               ufoRegA, idx, newaddr);
    }
  } else {
    ufoPush(ufoRegA);
    UFCALL(PAR_HANDLE_STORE_WORD);
  }
}

// !A+
// ( value idx -- )
UFWORD(POKE_REGA_IDX) {
  if ((ufoRegA & UFO_ADDR_HANDLE_BIT) == 0) {
    const uint32_t idx = ufoPop();
    const uint32_t newaddr = ufoRegA + idx;
    if ((ufoRegA & UFO_ADDR_SPECIAL_BITS_MASK) == (newaddr & UFO_ADDR_SPECIAL_BITS_MASK) &&
        (ufoRegA & UFO_ADDR_SPECIAL_BITS_MASK) == ((newaddr + 3u) & UFO_ADDR_SPECIAL_BITS_MASK))
    {
      const uint32_t value = ufoPop();
      ufoImgPutU32(newaddr, value);
    } else {
      ufoFatal("address offset out of range; addr=0x%08x; offset=%u; res=0x%08x",
               ufoRegA, idx, newaddr);
    }
  } else {
    ufoPush(ufoRegA);
    UFCALL(PAR_HANDLE_STORE_CELL);
  }
}

// C!+1>A
// ( byte -- )
UFWORD(CPOKE_REGA_INC1) {
  ufoImgPutU8(ufoRegA, ufoPop());
  ufoRegA += 1u;
}

// W!+2>A
// ( byte -- )
UFWORD(WPOKE_REGA_INC2) {
  ufoImgPutU16(ufoRegA, ufoPop());
  ufoRegA += 2u;
}

// !+4>A
// ( val32 -- )
UFWORD(POKE_REGA_INC4) {
  ufoImgPutU32(ufoRegA, ufoPop());
  ufoRegA += 4u;
}

// C@+1>A
// ( -- byte )
UFWORD(CPEEK_REGA_INC1) {
  ufoPush(ufoImgGetU8(ufoRegA));
  ufoRegA += 1u;
}

// W@+2>A
// ( -- byte )
UFWORD(WPEEK_REGA_INC2) {
  ufoPush(ufoImgGetU16(ufoRegA));
  ufoRegA += 2u;
}

// @+4>A
// ( -- val32 )
UFWORD(PEEK_REGA_INC4) {
  ufoPush(ufoImgGetU32(ufoRegA));
  ufoRegA += 4u;
}


// ////////////////////////////////////////////////////////////////////////// //
// peeks and pokes
//

// COMPILER:CFA,
// ( cfa -- )
UFWORD(CFA_COMMA) {
  const uint32_t cfa = ufoPop();
  ufoImgEmitCFA(cfa);
}

// (BRANCH-ADDR!)
// ( destaddr addr -- )
// write "branch to destaddr" address to addr
UFWORD(PAR_BRANCH_ADDR_POKE) {
  const uint32_t addr = ufoPop();
  const uint32_t dest = ufoPop();
  #ifdef UFO_RELATIVE_BRANCH
  ufoImgPutU32(addr, dest - addr);
  #else
  ufoImgPutU32(addr, dest);
  #endif
}

// (BRANCH-ADDR@)
// ( addr -- dest )
// read branch address
UFWORD(PAR_BRANCH_ADDR_PEEK) {
  UFO_STACK(1);
  #ifdef UFO_RELATIVE_BRANCH
  UFO_TOS += ufoImgGetU32(UFO_TOS);
  #else
  UFO_TOS = ufoImgGetU32(UFO_TOS);
  #endif
}

// C@
// ( addr -- value8 )
UFWORD(CPEEK) {
  UFO_STACK(1);
  UFO_TOS = ufoImgGetU8(UFO_TOS);
}

// W@
// ( addr -- value16 )
UFWORD(WPEEK) {
  UFO_STACK(1);
  UFO_TOS = ufoImgGetU16(UFO_TOS);
}

// @
// ( addr -- value32 )
UFWORD(PEEK) {
  UFO_STACK(1);
  UFO_TOS = ufoImgGetU32(UFO_TOS);
}

// C!
// ( val8 addr -- )
UFWORD(CPOKE) {
  UFO_STACK(2);
  ufoImgPutU8(UFO_TOS, UFO_S(1));
  ufoSP -= 2u;
}

// W!
// ( val16 addr -- )
UFWORD(WPOKE) {
  UFO_STACK(2);
  ufoImgPutU16(UFO_TOS, UFO_S(1));
  ufoSP -= 2u;
}

// !
// ( val32 addr -- )
UFWORD(POKE) {
  UFO_STACK(2);
  ufoImgPutU32(UFO_TOS, UFO_S(1));
  ufoSP -= 2u;
}

// (DIRECT:@)
// ( -- value32 )
// code arg is address
UFWORD(DIRECT_PEEK) {
  const uint32_t addr = ufoImgGetU32(ufoIP); ufoIP += 4u;
  ufoPush(ufoImgGetU32(addr));
}

// (DIRECT:0:!)
// ( -- )
// code arg is address
UFWORD(DIRECT_POKE0) {
  const uint32_t addr = ufoImgGetU32(ufoIP); ufoIP += 4u;
  ufoImgPutU32(addr, 0);
}

// (DIRECT:1:!)
// ( -- )
// code arg is address
UFWORD(DIRECT_POKE1) {
  const uint32_t addr = ufoImgGetU32(ufoIP); ufoIP += 4u;
  ufoImgPutU32(addr, 1);
}

// (DIRECT:-1:!)
// ( -- )
// code arg is address
UFWORD(DIRECT_POKEM1) {
  const uint32_t addr = ufoImgGetU32(ufoIP); ufoIP += 4u;
  ufoImgPutU32(addr, ~(uint32_t)0);
}

// (DIRECT:!)
// ( value32 -- )
// code arg is address
UFWORD(DIRECT_POKE) {
  const uint32_t addr = ufoImgGetU32(ufoIP); ufoIP += 4u;
  const uint32_t val = ufoPop();
  ufoImgPutU32(addr, val);
}

// (DIRECT:+!)
// ( value32 -- )
// code arg is address
UFWORD(DIRECT_ADD_POKE) {
  const uint32_t addr = ufoImgGetU32(ufoIP); ufoIP += 4u;
  uint32_t val = ufoPop();
  val += ufoImgGetU32(addr);
  ufoImgPutU32(addr, val);
}

// (DIRECT:-!)
// ( value32 -- )
// code arg is address
UFWORD(DIRECT_SUB_POKE) {
  const uint32_t addr = ufoImgGetU32(ufoIP); ufoIP += 4u;
  uint32_t val = ufoPop();
  val -= ufoImgGetU32(addr);
  ufoImgPutU32(addr, val);
}

// (DIRECT:+:@)
// ( addr -- value32 )
// code arg is offset
UFWORD(DIRECT_OFS_PEEK) {
  UFO_STACK(1);
  const uint32_t addr = UFO_TOS + ufoImgGetU32(ufoIP); ufoIP += 4u;
  UFO_TOS = ufoImgGetU32(addr);
}

// (DIRECT:+:!)
// ( value32 addr -- )
// code arg is offset
UFWORD(DIRECT_OFS_POKE) {
  UFO_STACK(2);
  const uint32_t addr = UFO_TOS + ufoImgGetU32(ufoIP); ufoIP += 4u;
  ufoImgPutU32(addr, UFO_S(1));
  ufoSP -= 2u;
}

// (DIRECT:1+!)
// ( -- )
// code arg is address
UFWORD(DIRECT_POKE_INC1) {
  const uint32_t addr = ufoImgGetU32(ufoIP); ufoIP += 4u;
  const uint32_t val = ufoImgGetU32(addr);
  ufoImgPutU32(addr, val + 1u);
}

// (DIRECT:2+!)
// ( -- )
// code arg is address
UFWORD(DIRECT_POKE_INC2) {
  const uint32_t addr = ufoImgGetU32(ufoIP); ufoIP += 4u;
  const uint32_t val = ufoImgGetU32(addr);
  ufoImgPutU32(addr, val + 2u);
}

// (DIRECT:4+!)
// ( -- )
// code arg is address
UFWORD(DIRECT_POKE_INC4) {
  const uint32_t addr = ufoImgGetU32(ufoIP); ufoIP += 4u;
  const uint32_t val = ufoImgGetU32(addr);
  ufoImgPutU32(addr, val + 4u);
}

// (DIRECT:8+!)
// ( -- )
// code arg is address
UFWORD(DIRECT_POKE_INC8) {
  const uint32_t addr = ufoImgGetU32(ufoIP); ufoIP += 4u;
  const uint32_t val = ufoImgGetU32(addr);
  ufoImgPutU32(addr, val + 8u);
}

// (DIRECT:1-!)
// ( -- )
// code arg is address
UFWORD(DIRECT_POKE_DEC1) {
  const uint32_t addr = ufoImgGetU32(ufoIP); ufoIP += 4u;
  const uint32_t val = ufoImgGetU32(addr);
  ufoImgPutU32(addr, val - 1u);
}

// (DIRECT:2-!)
// ( -- )
// code arg is address
UFWORD(DIRECT_POKE_DEC2) {
  const uint32_t addr = ufoImgGetU32(ufoIP); ufoIP += 4u;
  const uint32_t val = ufoImgGetU32(addr);
  ufoImgPutU32(addr, val - 2u);
}

// (DIRECT:4-!)
// ( -- )
// code arg is address
UFWORD(DIRECT_POKE_DEC4) {
  const uint32_t addr = ufoImgGetU32(ufoIP); ufoIP += 4u;
  const uint32_t val = ufoImgGetU32(addr);
  ufoImgPutU32(addr, val - 4u);
}

// (DIRECT:8-!)
// ( -- )
// code arg is address
UFWORD(DIRECT_POKE_DEC8) {
  const uint32_t addr = ufoImgGetU32(ufoIP); ufoIP += 4u;
  const uint32_t val = ufoImgGetU32(addr);
  ufoImgPutU32(addr, val - 8u);
}

// SWAP-C!
// ( addr value -- )
UFWORD(SWAP_CPOKE) {
  UFO_STACK(2);
  ufoImgPutU8(UFO_S(1), UFO_TOS);
  ufoSP -= 2u;
}

// SWAP-W!
// ( addr value -- )
UFWORD(SWAP_WPOKE) {
  UFO_STACK(2);
  ufoImgPutU16(UFO_S(1), UFO_TOS);
  ufoSP -= 2u;
}

// SWAP!
// ( addr value -- )
UFWORD(SWAP_POKE) {
  UFO_STACK(2);
  ufoImgPutU32(UFO_S(1), UFO_TOS);
  ufoSP -= 2u;
}

// OR-C!
// ( value addr -- )
UFWORD(OR_CPOKE) {
  UFO_STACK(2);
  ufoImgPutU8(UFO_TOS, UFO_S(1) | ufoImgGetU8(UFO_TOS));
  ufoSP -= 2u;
}

// OR-W!
// ( value addr -- )
UFWORD(OR_WPOKE) {
  UFO_STACK(2);
  ufoImgPutU16(UFO_TOS, UFO_S(1) | ufoImgGetU16(UFO_TOS));
  ufoSP -= 2u;
}

// OR!
// ( value addr -- )
UFWORD(OR_POKE) {
  UFO_STACK(2);
  #ifdef UFO_FAST_MEM_ACCESS
  if ((UFO_TOS & UFO_ADDR_HANDLE_BIT) == 0) {
    uint32_t *uptr = ufoImgIOPtrU32(UFO_TOS);
    *uptr |= UFO_S(1);
  } else {
    ufoImgPutU32(UFO_TOS, UFO_S(1) | ufoImgGetU32(UFO_TOS));
  }
  #else
  ufoImgPutU32(UFO_TOS, UFO_S(1) | ufoImgGetU32(UFO_TOS));
  #endif
  ufoSP -= 2u;
}

// XOR-C!
// ( value addr -- )
UFWORD(XOR_CPOKE) {
  UFO_STACK(2);
  ufoImgPutU8(UFO_TOS, UFO_S(1) ^ ufoImgGetU8(UFO_TOS));
  ufoSP -= 2u;
}

// XOR-W!
// ( value addr -- )
UFWORD(XOR_WPOKE) {
  UFO_STACK(2);
  ufoImgPutU16(UFO_TOS, UFO_S(1) ^ ufoImgGetU16(UFO_TOS));
  ufoSP -= 2u;
}

// XOR!
// ( value addr -- )
UFWORD(XOR_POKE) {
  UFO_STACK(2);
  #ifdef UFO_FAST_MEM_ACCESS
  if ((UFO_TOS & UFO_ADDR_HANDLE_BIT) == 0) {
    uint32_t *uptr = ufoImgIOPtrU32(UFO_TOS);
    *uptr ^= UFO_S(1);
  } else {
    ufoImgPutU32(UFO_TOS, UFO_S(1) ^ ufoImgGetU32(UFO_TOS));
  }
  #else
  ufoImgPutU32(UFO_TOS, UFO_S(1) ^ ufoImgGetU32(UFO_TOS));
  #endif
  ufoSP -= 2u;
}

// ~AND-C!
// ( value addr -- )
UFWORD(NAND_CPOKE) {
  UFO_STACK(2);
  ufoImgPutU8(UFO_TOS, ufoImgGetU8(UFO_TOS) & ~UFO_S(1));
  ufoSP -= 2u;
}

// ~AND-W!
// ( value addr -- )
UFWORD(NAND_WPOKE) {
  UFO_STACK(2);
  ufoImgPutU16(UFO_TOS, ufoImgGetU16(UFO_TOS) & ~UFO_S(1));
  ufoSP -= 2u;
}

// ~AND!
// ( value addr -- )
UFWORD(NAND_POKE) {
  UFO_STACK(2);
  #ifdef UFO_FAST_MEM_ACCESS
  if ((UFO_TOS & UFO_ADDR_HANDLE_BIT) == 0) {
    uint32_t *uptr = ufoImgIOPtrU32(UFO_TOS);
    *uptr = *uptr & ~UFO_S(1);
  } else {
    ufoImgPutU32(UFO_TOS, ufoImgGetU32(UFO_TOS) & ~UFO_S(1));
  }
  #else
  ufoImgPutU32(UFO_TOS, ufoImgGetU32(UFO_TOS) & ~UFO_S(1));
  #endif
  ufoSP -= 2u;
}

// COUNT
// ( addr -- addr+4 addr@ )
UFWORD(COUNT) {
  UFO_STACK(1);
  const uint32_t count = ufoImgGetU32(UFO_TOS);
  UFO_TOS += 4u;
  ufoPush(count);
}

// ID-COUNT
// ( addr -- addr+4 addr@&0xff )
UFWORD(ID_COUNT) {
  UFO_STACK(1);
  const uint32_t count = ufoImgGetU32(UFO_TOS);
  UFO_TOS += 4u;
  ufoPush(count & 0xffU);
}

// BCOUNT
// ( addr -- addr+1 addrC@ )
UFWORD(BCOUNT) {
  UFO_STACK(1);
  const uint32_t count = ufoImgGetU8(UFO_TOS);
  UFO_TOS += 1u;
  ufoPush(count);
}

// 0!
// ( addr -- )
UFWORD(POKE_0) {
  ufoImgPutU32(ufoPop(), 0);
}

// 1!
// ( addr -- )
UFWORD(POKE_1) {
  ufoImgPutU32(ufoPop(), 1);
}

// 1+!
// ( addr -- )
UFWORD(POKE_INC_1) {
  const uint32_t addr = ufoPop();
  const uint32_t val = ufoImgGetU32(addr);
  ufoImgPutU32(addr, val + 1u);
}

// 1-!
// ( addr -- )
UFWORD(POKE_DEC_1) {
  const uint32_t addr = ufoPop();
  const uint32_t val = ufoImgGetU32(addr);
  ufoImgPutU32(addr, val - 1u);
}

// +!
// ( delta addr -- )
UFWORD(POKE_INC) {
  UFO_STACK(2);
  ufoImgPutU32(UFO_TOS, ufoImgGetU32(UFO_TOS) + UFO_S(1));
  ufoSP -= 2u;
}

// -!
// ( delta addr -- )
UFWORD(POKE_DEC) {
  UFO_STACK(2);
  ufoImgPutU32(UFO_TOS, ufoImgGetU32(UFO_TOS) - UFO_S(1));
  ufoSP -= 2u;
}


// ////////////////////////////////////////////////////////////////////////// //
// dictionary emitters
//

// C,
// ( val8 -- )
UFWORD(CCOMMA) { const uint32_t val = ufoPop(); ufoImgEmitU8(val); }

// W,
// ( val16 -- )
UFWORD(WCOMMA) { const uint32_t val = ufoPop(); ufoImgEmitU16(val); }

// ,
// ( val -- )
UFWORD(COMMA) { const uint32_t val = ufoPop(); ufoImgEmitU32(val); }


// ////////////////////////////////////////////////////////////////////////// //
// literal pushers
//

// (LIT) ( -- n )
UFWORD(PAR_LIT) {
  const uint32_t v = ufoImgGetU32(ufoIP); ufoIP += 4;
  ufoPush(v);
}

// (LITCFA) ( -- n )
UFWORD(PAR_LITCFA) {
  const uint32_t v = ufoImgGetU32(ufoIP); ufoIP += 4;
  ufoPush(v);
}

// (LITPFA) ( -- n )
UFWORD(PAR_LITPFA) {
  const uint32_t v = ufoImgGetU32(ufoIP); ufoIP += 4;
  ufoPush(v);
}

// (LITVOCID) ( -- n )
UFWORD(PAR_LITVOCID) {
  const uint32_t v = ufoImgGetU32(ufoIP); ufoIP += 4;
  ufoPush(v);
}

// (LITSTR8)
UFWORD(PAR_LITSTR8) {
  const uint32_t count = ufoImgGetU8(ufoIP); ufoIP += 1;
  ufoPush(ufoIP);
  ufoPush(count);
  ufoIP += count + 1; // 1 for terminating 0
  ufoIP = UFO_ALIGN4(ufoIP);
}


// ////////////////////////////////////////////////////////////////////////// //
// jumps, etc.
//

#ifdef UFO_RELATIVE_BRANCH
# define UFO_IP_BRANCH()  (ufoIP += ufoImgGetU32(ufoIP))
#else
# define UFO_IP_BRANCH()  (ufoIP = ufoImgGetU32(ufoIP))
#endif

// (BRANCH) ( -- )
UFWORD(PAR_BRANCH) {
  UFO_IP_BRANCH();
}

// (TBRANCH) ( flag )
UFWORD(PAR_TBRANCH) {
  if (ufoPop()) {
    UFO_IP_BRANCH();
  } else {
    ufoIP += 4u;
  }
}

// (0BRANCH) ( flag )
UFWORD(PAR_0BRANCH) {
  if (!ufoPop()) {
    UFO_IP_BRANCH();
  } else {
    ufoIP += 4u;
  }
}

// (+0BRANCH) ( flag )
UFWORD(PAR_P0BRANCH) {
  if ((ufoPop() & 0x80000000u) == 0) {
    UFO_IP_BRANCH();
  } else {
    ufoIP += 4u;
  }
}

// (+BRANCH) ( flag )
UFWORD(PAR_PBRANCH) {
  const int32_t v = (int32_t)ufoPop();
  if (v > 0) {
    UFO_IP_BRANCH();
  } else {
    ufoIP += 4u;
  }
}

// (-0BRANCH) ( flag )
UFWORD(PAR_M0BRANCH) {
  const int32_t v = (int32_t)ufoPop();
  if (v <= 0) {
    UFO_IP_BRANCH();
  } else {
    ufoIP += 4u;
  }
}

// (-BRANCH) ( flag )
UFWORD(PAR_MBRANCH) {
  if ((ufoPop() & 0x80000000u) != 0) {
    UFO_IP_BRANCH();
  } else {
    ufoIP += 4u;
  }
}

// (DATASKIP) ( -- )
UFWORD(PAR_DATASKIP) {
  ufoIP += UFO_ALIGN4(4u + ufoImgGetU32(ufoIP));
}

// (OR-BRANCH)
// ( !0 -- !0 ) -- jmp
// ( 0 -- ) -- no jmp
UFWORD(PAR_OR_BRANCH) {
  UFO_STACK(1);
  if (UFO_TOS != 0) {
    UFO_IP_BRANCH();
  } else {
    ufoSP -= 1u;
    ufoIP += 4u;
  }
}

// (AND-BRANCH)
// ( 0 -- 0 ) -- jmp
// ( !0 -- ) -- no jmp
UFWORD(PAR_AND_BRANCH) {
  UFO_STACK(1);
  if (UFO_TOS == 0) {
    UFO_IP_BRANCH();
  } else {
    ufoSP -= 1u;
    ufoIP += 4u;
  }
}

// (?DUP-0BRANCH)
// ( 0 -- ) -- jmp
// ( !0 -- !0 ) -- no jmp
UFWORD(PAR_QDUP_0BRANCH) {
  UFO_STACK(1);
  if (UFO_TOS != 0) {
    ufoIP += 4u;
  } else {
    ufoSP -= 1u;
    UFO_IP_BRANCH();
  }
}

// (CASE-BRANCH)
// ( 0 -- ) -- jmp
// ( n !0 -- ) -- no jmp
UFWORD(PAR_CASE_BRANCH) {
  UFO_STACK(2);
  if (UFO_TOS == 0) {
    ufoSP -= 1u;
    UFO_IP_BRANCH();
  } else {
    ufoSP -= 2u;
    ufoIP += 4u;
  }
}


// ////////////////////////////////////////////////////////////////////////// //
// execute words by CFA
//

// EXECUTE ( cfa )
UFWORD(EXECUTE) {
  UFO_EXEC_CFA(ufoPop());
}

// EXECUTE-TAIL ( cfa )
UFWORD(EXECUTE_TAIL) {
  if (ufoRP != 0) ufoIP = ufoRPop();
  UFO_EXEC_CFA(ufoPop());
}

// @EXECUTE ( addr )
UFWORD(LOAD_EXECUTE) {
  const uint32_t addr = ufoPop();
  UFO_EXEC_CFA(ufoImgGetU32(addr));
}

// @EXECUTE-TAIL ( cfa )
UFWORD(LOAD_EXECUTE_TAIL) {
  if (ufoRP != 0) ufoIP = ufoRPop();
  const uint32_t addr = ufoPop();
  UFO_EXEC_CFA(ufoImgGetU32(addr));
}

// (FORTH-CALL) ( pfa )
UFWORD(FORTH_CALL) {
  ufoRPush(ufoIP);
  ufoIP = ufoPop();
}

// (FORTH-TAIL-CALL) ( pfa )
UFWORD(FORTH_TAIL_CALL) {
  ufoIP = ufoPop();
}


// ////////////////////////////////////////////////////////////////////////// //
// word termination, locals support
//

// (EXIT)
UFWORD(PAR_EXIT) {
  if (ufoRP == 0) longjmp(ufoStopVMJP, 667);
  ufoRP -= 1u;
  ufoIP = ufoRStack[ufoRP];
  //ufoIP = ufoRPop();
}

// (SELF@)
// ( -- self-value )
UFWORD(PAR_SELF_LOAD) {
  ufoPush(ufoImgGetU32(ufoAddrSelf));
}

// (SELF!)
// ( self-value -- )
UFWORD(PAR_SELF_STORE) {
  const uint32_t val = ufoPop();
  ufoImgPutU32(ufoAddrSelf, val);
}

// (SELF@+)
// ( ofs -- @self+ofs )
UFWORD(PAR_SELF_GET_ADD) {
  UFO_STACK(1);
  UFO_TOS += ufoImgGetU32(ufoAddrSelf);
}

// (L-ENTER)
// ( loccount -- )
UFWORD(PAR_LENTER) {
  // low byte of loccount is total number of locals
  // high byte is the number of args
  uint32_t lcount = ufoImgGetU32(ufoIP); ufoIP += 4u;
  uint32_t acount = (lcount >> 8) & 0xff;
  lcount &= 0xff;
  if (lcount == 0 || lcount < acount) ufoFatal("invalid call to (L-ENTER)");
  if ((ufoLBP != 0 && ufoLBP >= ufoLP) || UFO_LSTACK_SIZE - ufoLP <= lcount + 2) {
    ufoFatal("out of locals stack");
  }
  uint32_t newbp;
  if (ufoLP == 0) { ufoLP = 1; newbp = 1; } else newbp = ufoLP;
  //fprintf(stderr, "LP: %u; LBP: %u; locs: %u; word: %s\n", ufoLP, ufoLBP, fw->locs, fw->name);
  ufoLStack[ufoLP] = ufoLBP; ufoLP += 1;
  ufoLBP = newbp; ufoLP += lcount;
  // and copy args
  newbp += acount;
  while (newbp != ufoLBP) {
    ufoLStack[newbp] = ufoPop();
    newbp -= 1;
  }
}

// (L-LEAVE)
UFWORD(PAR_LLEAVE) {
  if (ufoLBP == 0) ufoFatal("(L-LEAVE) with empty locals stack");
  if (ufoLBP >= ufoLP) ufoFatal("(L-LEAVE) broken locals stack");
  ufoLP = ufoLBP;
  ufoLBP = ufoLStack[ufoLBP];
}

//==========================================================================
//
//  ufoLoadLocal
//
//==========================================================================
UFO_FORCE_INLINE void ufoLoadLocal (const uint32_t lidx) {
  if (lidx == 0 || lidx >= UFO_LSTACK_SIZE) ufoFatal("invalid local index");
  if (ufoLBP == 0 || ufoLBP >= ufoLP || ufoLP - ufoLBP <= lidx) ufoFatal("invalid local index");
  ufoPush(ufoLStack[ufoLBP + lidx]);
}

//==========================================================================
//
//  ufoStoreLocal
//
//==========================================================================
UFO_FORCE_INLINE void ufoStoreLocal (const uint32_t lidx) {
  const uint32_t value = ufoPop();
  if (lidx == 0 || lidx >= UFO_LSTACK_SIZE) ufoFatal("invalid local index");
  if (ufoLBP == 0 || ufoLBP >= ufoLP || ufoLP - ufoLBP <= lidx) ufoFatal("invalid local index");
  ufoLStack[ufoLBP + lidx] = value;
}

// (LOCAL@)
// ( idx -- value )
UFWORD(PAR_LOCAL_LOAD) { ufoLoadLocal(ufoPop()); }

// (LOCAL!)
// ( value idx -- )
UFWORD(PAR_LOCAL_STORE) { ufoStoreLocal(ufoPop()); }


// ////////////////////////////////////////////////////////////////////////// //
// stack manipulation
//

// DUP
// ( n -- n n )
UFWORD(DUP) { ufoDup(); }
// NIP
// ( a b -- b )
UFWORD(NIP) {
  UFO_STACK(2);
  const uint32_t b = UFO_TOS;
  ufoSP -= 1;
  UFO_TOS = b;
}
// TUCK
// ( a b -- b a b )
UFWORD(TUCK) {
  const uint32_t b = ufoPop();
  const uint32_t a = ufoPop();
  ufoPush(b); ufoPush(a); ufoPush(b);
}
// ?DUP
// ( n -- n n ) | ( 0 -- 0 )
UFWORD(QDUP) {
  UFO_STACK(1);
  const uint32_t n = UFO_TOS;
  if (n) ufoPush(n);
}
// 2DUP
// ( n0 n1 -- n0 n1 n0 n1 )
UFWORD(DDUP) { ufo2Dup(); }
// DROP
// ( n -- )
UFWORD(DROP) { ufoDrop(); }
// 2DROP
// ( n0 n1 -- )
UFWORD(DDROP) { ufo2Drop(); }
// SWAP
// ( n0 n1 -- n1 n0 )
UFWORD(SWAP) { ufoSwap(); }
// 2SWAP
// ( n0 n1 -- n1 n0 )
UFWORD(DSWAP) { ufo2Swap(); }
// OVER
// ( n0 n1 -- n0 n1 n0 )
UFWORD(OVER) { ufoOver(); }
// 2OVER
// ( n0 n1 -- n0 n1 n0 )
UFWORD(DOVER) { ufo2Over(); }
// ROT
// ( n0 n1 n2 -- n1 n2 n0 )
UFWORD(ROT) { ufoRot(); }
// NROT
// ( n0 n1 n2 -- n2 n0 n1 )
UFWORD(NROT) { ufoNRot(); }

// RDUP
// ( n -- n n )
UFWORD(RDUP) { ufoRDup(); }
// RDROP
// ( n -- )
UFWORD(RDROP) { ufoRDrop(); }

// >R
// ( n -- | n )
UFWORD(DTOR) { ufoRPush(ufoPop()); }
// R>
// ( | n -- n )
UFWORD(RTOD) { ufoPush(ufoRPop()); }
// R@
// ( | n -- n | n )
UFWORD(RPEEK) { ufoPush(ufoRPeek()); }
// 2RDROP
// ( | n n -- )
UFWORD(2RDROP) {
  UFO_RSTACK(2);
  ufoRP -= 2u;
}
// 2>R
// ( a b | -- | a b )
UFWORD(2DTOR) {
  const uint32_t b = ufoPop();
  const uint32_t a = ufoPop();
  ufoRPush(a);
  ufoRPush(b);
}
// 2R>
// ( | a b -- a b | )
UFWORD(2RTOD) {
  const uint32_t b = ufoRPop();
  const uint32_t a = ufoRPop();
  ufoPush(a);
  ufoPush(b);
}
// 2R@
// ( | a b -- a b | a b )
UFWORD(2RPEEK) {
  UFO_RSTACK(2);
  ufoPush(UFO_R(1));
  ufoPush(UFO_RTOS);
}

// PICK
// ( idx -- n )
UFWORD(PICK) {
  const uint32_t n = ufoPop();
  if (n >= ufoSP) ufoFatal("invalid PICK index %u", n);
  ufoPush(ufoDStack[ufoSP - n - 1u]);
}

// RPICK
// ( idx -- n )
UFWORD(RPICK) {
  const uint32_t n = ufoPop();
  if (n >= ufoRP) ufoFatal("invalid RPICK index %u", n);
  const uint32_t rp = ufoRP - n - 1u;
  ufoPush(ufoRStack[rp]);
}

// ROLL
// ( idx -- n )
UFWORD(ROLL) {
  const uint32_t n = ufoPop();
  if (n >= ufoSP) ufoFatal("invalid ROLL index %u", n);
  switch (n) {
    case 0: break; // do nothing
    case 1: ufoSwap(); break;
    case 2: ufoRot(); break;
    default:
      {
        const uint32_t val = ufoDStack[ufoSP - n - 1u];
        for (uint32_t f = ufoSP - n; f < ufoSP; f += 1) ufoDStack[f - 1] = ufoDStack[f];
        ufoDStack[ufoSP - 1u] = val;
      }
      break;
  }
}

// RROLL
// ( idx -- n )
UFWORD(RROLL) {
  const uint32_t n = ufoPop();
  if (n >= ufoRP) ufoFatal("invalid RROLL index %u", n);
  if (n != 0) {
    const uint32_t rp = ufoRP - n - 1u;
    const uint32_t val = ufoRStack[rp];
    for (uint32_t f = rp + 1u; f < ufoRP; f += 1u) ufoRStack[f - 1u] = ufoRStack[f];
    ufoRStack[ufoRP - 1u] = val;
  }
}

// RSWAP
// ( | a b -- | b a )
UFWORD(RSWAP) {
  UFO_RSTACK(2);
  const uint32_t b = UFO_RTOS;
  const uint32_t a = UFO_R(1);
  UFO_RTOS = a;
  UFO_R(1) = b;
}

// ROVER
// ( | a b -- | a b a )
UFWORD(ROVER) {
  UFO_RSTACK(2);
  const uint32_t a = UFO_R(1);
  ufoRPush(a);
}

// RROT
// ( | a b c -- | b c a )
UFWORD(RROT) {
  UFO_RSTACK(3);
  const uint32_t c = UFO_RTOS;
  const uint32_t b = UFO_R(1);
  const uint32_t a = UFO_R(2);
  UFO_R(2) = b;
  UFO_R(1) = c;
  UFO_RTOS = a;
}

// RNROT
// ( | a b c -- | c a b )
UFWORD(RNROT) {
  UFO_RSTACK(3);
  const uint32_t c = UFO_RTOS;
  const uint32_t b = UFO_R(1);
  const uint32_t a = UFO_R(2);
  UFO_R(2) = c;
  UFO_R(1) = a;
  UFO_RTOS = b;
}


// ////////////////////////////////////////////////////////////////////////// //
// TIB API
//

// REFILL
// ( -- eofflag )
UFWORD(REFILL) {
  ufoPushBool(ufoLoadNextLine(1));
}

// REFILL-NOCROSS
// ( -- eofflag )
UFWORD(REFILL_NOCROSS) {
  ufoPushBool(ufoLoadNextLine(0));
}

// (TIB-IN)
// ( -- addr )
UFWORD(TIB_IN) {
  ufoPush(ufoImgGetU32(ufoAddrTIBx) + ufoImgGetU32(ufoAddrINx));
}

// TIB-PEEKCH
// ( -- char )
UFWORD(TIB_PEEKCH) {
  ufoPush(ufoTibPeekCh());
}

// TIB-PEEKCH-OFS
// ( ofs -- char )
UFWORD(TIB_PEEKCH_OFS) {
  const uint32_t ofs = ufoPop();
  ufoPush(ufoTibPeekChOfs(ofs));
}

// TIB-GETCH
// ( -- char )
UFWORD(TIB_GETCH) {
  ufoPush(ufoTibGetCh());
}

// TIB-SKIPCH
// ( -- )
UFWORD(TIB_SKIPCH) {
  ufoTibSkipCh();
}


// ////////////////////////////////////////////////////////////////////////// //
// TIB parsing
//

//==========================================================================
//
//  ufoIsDelim
//
//==========================================================================
UFO_FORCE_INLINE int ufoIsDelim (uint8_t ch, uint8_t delim) {
  return (delim == 32 ? (ch <= 32) : (ch == delim));
}

// (PARSE)
// ( delim skip-leading-delim? -- addr count TRUE / FALSE )
// does base TIB parsing; never copies anything.
// as our reader is line-based, returns FALSE on EOL.
// EOL is detected after skipping leading delimiters.
// passing -1 as delimiter skips the whole line, and always returns FALSE.
// trailing delimiter is always skipped.
UFWORD(PAR_PARSE) {
  const uint32_t skipDelim = ufoPop();
  const uint32_t delim = ufoPop();
  uint8_t ch;

  if (delim == 0 || delim > 0xffU) {
    // skip everything
    while (ufoTibGetCh() != 0) {}
    ufoPushBool(0);
  } else {
    ch = ufoTibPeekCh();
    // skip initial delimiters
    if (skipDelim) {
      while (ch != 0 && ufoIsDelim(ch, delim)) {
        ufoTibSkipCh();
        ch = ufoTibPeekCh();
      }
    }
    if (ch == 0) {
      ufoPushBool(0);
    } else {
      // parse
      const uint32_t staddr = ufoImgGetU32(ufoAddrTIBx) + ufoImgGetU32(ufoAddrINx);
      uint32_t count = 0;
      while (ch != 0 && !ufoIsDelim(ch, delim)) {
        count += 1u;
        ufoTibSkipCh();
        ch = ufoTibPeekCh();
      }
      // skip delimiter
      if (ch != 0) ufoTibSkipCh();
      ufoPush(staddr);
      ufoPush(count);
      ufoPushBool(1);
    }
  }
}

// PARSE-SKIP-BLANKS
// ( -- )
UFWORD(PARSE_SKIP_BLANKS) {
  uint8_t ch = ufoTibPeekCh();
  while (ch != 0 && ch <= 32) {
    ufoTibSkipCh();
    ch = ufoTibPeekCh();
  }
}

//==========================================================================
//
//  ufoParseMLComment
//
//  initial two chars are skipped
//
//==========================================================================
static void ufoParseMLComment (uint32_t allowMulti, int nested) {
  uint32_t level = 1;
  uint8_t ch, ch1;
  while (level != 0) {
    ch = ufoTibGetCh();
    if (ch == 0) {
      if (allowMulti) {
        UFCALL(REFILL_NOCROSS);
        if (ufoPop() == 0) ufoFatal("unexpected end of file in comment");
      } else {
        ufoFatal("unexpected end of line in comment");
      }
    } else {
      ch1 = ufoTibPeekCh();
      if (nested && ch == '(' && ch1 == '(') { ufoTibSkipCh(); level += 1; }
      else if (nested && ch == ')' && ch1 == ')') { ufoTibSkipCh(); level -= 1; }
      else if (!nested && ch == '*' && ch1 == ')') { ufo_assert(level == 1); ufoTibSkipCh(); level = 0; }
    }
  }
}

// (PARSE-SKIP-COMMENTS)
// ( allow-multiline? -- )
// skip all blanks and comments
UFWORD(PAR_PARSE_SKIP_COMMENTS) {
  const uint32_t allowMulti = ufoPop();
  uint8_t ch, ch1;
  ch = ufoTibPeekCh();
  #if 0
  fprintf(stderr, "(PARSE-SKIP-COMMENTS): first-ch:'%c'\n", (char)ch);
  #endif
  while (ch != 0) {
    if (ch <= 32) {
      ufoTibSkipCh();
      ch = ufoTibPeekCh();
      #if 0
      fprintf(stderr, "(PARSE-SKIP-COMMENTS): blank-ch:'%c'\n", (char)ch);
      #endif
    } else if (ch == '(') {
      #if 0
      fprintf(stderr, "(PARSE-SKIP-COMMENTS): ch:'%c'; ch1:'%c' ('%c')\n", (char)ch, (char)ch1,
              ufoTibPeekChOfs(0));
      #endif
      ch1 = ufoTibPeekChOfs(1);
      if (ch1 <= 32) {
        // single-line comment
        do { ch = ufoTibGetCh(); } while (ch != 0 && ch != ')');
        ch = ufoTibPeekCh();
      } else if ((ch1 == '*' || ch1 == '(') && ufoTibPeekChOfs(2) <= 32) {
        // possibly multiline
        ufoTibSkipCh(); ufoTibSkipCh(); // skip opening
        ufoParseMLComment(allowMulti, (ch1 == '('));
        ch = ufoTibPeekCh();
      } else {
        ch = 0;
      }
    } else if (ch == '\\' && ufoTibPeekChOfs(1) <= 32) {
      // single-line comment
      while (ch != 0) ch = ufoTibGetCh();
    } else if (ch == '-' && ufoTibPeekChOfs(1) == ch && ufoTibPeekChOfs(2) <= 32) {
      // skip to EOL
      while (ch != 0) ch = ufoTibGetCh();
    } else if ((ch == ';' || ch == '/') && ufoTibPeekChOfs(1) == ch) {
      // skip to EOL
      while (ch != 0) ch = ufoTibGetCh();
    } else {
      ch = 0;
    }
  }
  #if 0
  fprintf(stderr, "(PARSE-SKIP-COMMENTS): end-ch:'%c'\n", ufoTibPeekCh());
  #endif
}

// PARSE-SKIP-LINE
// ( -- )
UFWORD(PARSE_SKIP_LINE) {
  ufoPush(0); ufoPushBool(0); UFCALL(PAR_PARSE);
  if (ufoPop() != 0) {
    ufo2Drop();
  }
}

// PARSE-NAME
// ( -- addr count )
// parse with leading blanks skipping. doesn't copy anything.
// return empty string on EOL.
UFWORD(PARSE_NAME) {
  ufoPush(32); ufoPushBool(1); UFCALL(PAR_PARSE);
  if (ufoPop() == 0) {
    ufoPush(0);
    ufoPush(0);
  }
}

// PARSE
// ( delim -- addr count TRUE / FALSE )
// parse without skipping delimiters; never copies anything.
// as our reader is line-based, returns FALSE on EOL.
// passing 0 as delimiter skips the whole line, and always returns FALSE.
// trailing delimiter is always skipped.
UFWORD(PARSE) {
  ufoPushBool(0); UFCALL(PAR_PARSE);
}


// ////////////////////////////////////////////////////////////////////////// //
// char output
//

// (NORM-EMIT-CHAR)
// ( ch -- )
UFWORD(PAR_NORM_EMIT_CHAR) {
  uint32_t ch = ufoPop()&0xffU;
  if (ch < 32 || ch == 127) {
    if (ch != 9 && ch != 10 && ch != 13) ch = '?';
  }
  ufoPush(ch);
}

// (NORM-XEMIT-CHAR)
// ( ch -- )
UFWORD(PAR_NORM_XEMIT_CHAR) {
  uint32_t ch = ufoPop()&0xffU;
  if (ch < 32 || ch == 127) ch = '?';
  ufoPush(ch);
}

// (EMIT)
// ( n -- )
UFWORD(PAR_EMIT) {
  uint32_t ch = ufoPop()&0xffU;
  ufoLastEmitWasCR = (ch == 10);
  putchar((char)ch);
}

// LASTCR?
// ( -- bool )
UFWORD(LASTCRQ) {
  ufoPushBool(ufoLastEmitWasCR);
}

// LASTCR!
// ( bool -- )
UFWORD(LASTCRSET) {
  ufoLastEmitWasCR = !!ufoPop();
}

// FLUSH-EMIT
// ( -- )
UFWORD(FLUSH_EMIT) {
  ufoFlushOutput();
}


// ////////////////////////////////////////////////////////////////////////// //
// simple math
//

#define UF_BMATH(name_,op_)  \
UFWORD(name_) { \
  UFO_STACK(2); \
  const uint32_t b = UFO_TOS; \
  const uint32_t a = UFO_S(1); \
  ufoSP -= 1u; \
  UFO_TOS = (op_); \
}

#define UF_BDIV(name_,op_)  \
UFWORD(name_) { \
  UFO_STACK(2); \
  const uint32_t b = UFO_TOS; \
  const uint32_t a = UFO_S(1); \
  if (b == 0) ufoFatal("division by zero"); \
  ufoSP -= 1u; \
  UFO_TOS = (op_); \
}

#define UFO_POP_U64()  ({ \
  UFO_STACK(2); \
  const uint32_t hi_ = UFO_TOS; \
  const uint32_t lo_ = UFO_S(1); \
  ufoSP -= 2u; \
  (((uint64_t)hi_ << 32) | lo_); \
})

// this is UB by the idiotic C standard. i don't care.
#define UFO_POP_I64()  ((int64_t)UFO_POP_U64())

#define UFO_PUSH_U64(vn_)  do { \
  ufoPush((uint32_t)(vn_)); \
  ufoPush((uint32_t)((vn_) >> 32)); \
} while (0)

// this is UB by the idiotic C standard. i don't care.
#define UFO_PUSH_I64(vn_)  UFO_PUSH_U64((uint64_t)(vn_))

// +
// ( a b -- a+b )
UF_BMATH(PLUS, a + b);

// -
// ( a b -- a-b )
UF_BMATH(MINUS, a - b);

// *
// ( a b -- a*b )
UF_BMATH(MUL, (uint32_t)((int32_t)a * (int32_t)b));

// U*
// ( a b -- a*b )
UF_BMATH(UMUL, a * b);

// /
// ( a b -- a/b )
UF_BDIV(DIV, (uint32_t)((int32_t)a / (int32_t)b));

// U/
// ( a b -- a/b )
UF_BDIV(UDIV, a / b);

// MOD
// ( a b -- a%b )
UF_BDIV(MOD, (uint32_t)((int32_t)a % (int32_t)b));

// UMOD
// ( a b -- a%b )
UF_BDIV(UMOD, a % b);

// /MOD
// ( a b -- a/b, a%b )
UFWORD(DIVMOD) {
  const int32_t b = (int32_t)ufoPop();
  const int32_t a = (int32_t)ufoPop();
  if (b == 0) ufoFatal("division by zero");
  ufoPush((uint32_t)(a/b));
  ufoPush((uint32_t)(a%b));
}

// U/MOD
// ( a b -- a/b, a%b )
UFWORD(UDIVMOD) {
  const uint32_t b = ufoPop();
  const uint32_t a = ufoPop();
  if (b == 0) ufoFatal("division by zero");
  ufoPush((uint32_t)(a/b));
  ufoPush((uint32_t)(a%b));
}

// */
// ( a b c -- a*b/c )
// this uses 64-bit intermediate value
UFWORD(MULDIV) {
  const int32_t c = (int32_t)ufoPop();
  const int32_t b = (int32_t)ufoPop();
  const int32_t a = (int32_t)ufoPop();
  if (c == 0) ufoFatal("division by zero");
  int64_t xval = a; xval *= b; xval /= c;
  ufoPush((uint32_t)(int32_t)xval);
}

// U*/
// ( a b c -- a*b/c )
// this uses 64-bit intermediate value
UFWORD(UMULDIV) {
  const uint32_t c = ufoPop();
  const uint32_t b = ufoPop();
  const uint32_t a = ufoPop();
  if (c == 0) ufoFatal("division by zero");
  uint64_t xval = a; xval *= b; xval /= c;
  ufoPush((uint32_t)xval);
}

// */MOD
// ( a b c -- a*b/c a*b%c )
// this uses 64-bit intermediate value
UFWORD(MULDIVMOD) {
  const int32_t c = (int32_t)ufoPop();
  const int32_t b = (int32_t)ufoPop();
  const int32_t a = (int32_t)ufoPop();
  if (c == 0) ufoFatal("division by zero");
  int64_t xval = a; xval *= b;
  ufoPush((uint32_t)(int32_t)(xval / c));
  ufoPush((uint32_t)(int32_t)(xval % c));
}

// U*/
// ( a b c -- a*b/c )
// this uses 64-bit intermediate value
UFWORD(UMULDIVMOD) {
  const uint32_t c = ufoPop();
  const uint32_t b = ufoPop();
  const uint32_t a = ufoPop();
  if (c == 0) ufoFatal("division by zero");
  uint64_t xval = a; xval *= b;
  ufoPush((uint32_t)(xval / c));
  ufoPush((uint32_t)(xval % c));
}

// M*
// ( a b -- lo(a*b) hi(a*b) )
// this leaves 64-bit result
UFWORD(MMUL) {
  const int32_t b = (int32_t)ufoPop();
  const int32_t a = (int32_t)ufoPop();
  int64_t xval = a; xval *= b;
  UFO_PUSH_I64(xval);
}

// UM*
// ( a b -- lo(a*b) hi(a*b) )
// this leaves 64-bit result
UFWORD(UMMUL) {
  const uint32_t b = ufoPop();
  const uint32_t a = ufoPop();
  uint64_t xval = a; xval *= b;
  UFO_PUSH_U64(xval);
}

// M/MOD
// ( alo ahi b -- a/b a%b )
UFWORD(MDIVMOD) {
  const int32_t b = (int32_t)ufoPop();
  if (b == 0) ufoFatal("division by zero");
  int64_t a = UFO_POP_I64();
  int32_t adiv = (int32_t)(a / b);
  int32_t amod = (int32_t)(a % b);
  ufoPush((uint32_t)adiv);
  ufoPush((uint32_t)amod);
}

// UM/MOD
// ( alo ahi b -- a/b a%b )
UFWORD(UMDIVMOD) {
  const uint32_t b = ufoPop();
  if (b == 0) ufoFatal("division by zero");
  uint64_t a = UFO_POP_U64();
  uint32_t adiv = (uint32_t)(a / b);
  uint32_t amod = (uint32_t)(a % b);
  ufoPush(adiv);
  ufoPush(amod);
}

// UDS*
// ( alo ahi u -- lo hi )
UFWORD(UDSMUL) {
  const uint32_t b = ufoPop();
  uint64_t a = UFO_POP_U64();
  a *= b;
  UFO_PUSH_U64(a);
}

// D-
// ( lo0 hi0 lo1 hi1 -- lo hi )
UFWORD(DMINUS) {
  uint64_t n1 = UFO_POP_U64();
  uint64_t n0 = UFO_POP_U64();
  n0 -= n1;
  UFO_PUSH_U64(n0);
}

// D+
// ( lo0 hi0 lo1 hi1 -- lo hi )
UFWORD(DPLUS) {
  uint64_t n1 = UFO_POP_U64();
  uint64_t n0 = UFO_POP_U64();
  n0 += n1;
  UFO_PUSH_U64(n0);
}

// D=
// ( lo0 hi0 lo1 hi1 -- bool )
UFWORD(DEQU) {
  uint64_t n1 = UFO_POP_U64();
  uint64_t n0 = UFO_POP_U64();
  ufoPushBool(n0 == n1);
}

// D<
// ( lo0 hi0 lo1 hi1 -- bool )
UFWORD(DLESS) {
  int64_t n1 = UFO_POP_I64();
  int64_t n0 = UFO_POP_I64();
  ufoPushBool(n0 < n1);
}

// D<=
// ( lo0 hi0 lo1 hi1 -- bool )
UFWORD(DLESSEQU) {
  int64_t n1 = UFO_POP_I64();
  int64_t n0 = UFO_POP_I64();
  ufoPushBool(n0 <= n1);
}

// DU<
// ( lo0 hi0 lo1 hi1 -- bool )
UFWORD(DULESS) {
  uint64_t n1 = UFO_POP_U64();
  uint64_t n0 = UFO_POP_U64();
  ufoPushBool(n0 < n1);
}

// DU<=
// ( lo0 hi0 lo1 hi1 -- bool )
UFWORD(DULESSEQU) {
  uint64_t n1 = UFO_POP_U64();
  uint64_t n0 = UFO_POP_U64();
  ufoPushBool(n0 <= n1);
}

// SM/REM
// ( dlo dhi n -- nmod ndiv )
// rounds toward zero
UFWORD(SMREM) {
  const int32_t n = (int32_t)ufoPop();
  if (n == 0) ufoFatal("division by zero");
  int64_t d = UFO_POP_I64();
  int32_t ndiv = (int32_t)(d / n);
  int32_t nmod = (int32_t)(d % n);
  ufoPush(nmod);
  ufoPush(ndiv);
}

// FM/MOD
// ( dlo dhi n -- nmod ndiv )
// rounds toward negative infinity
UFWORD(FMMOD) {
  const int32_t n = (int32_t)ufoPop();
  if (n == 0) ufoFatal("division by zero");
  int64_t d = UFO_POP_I64();
  int32_t ndiv = (int32_t)(d / n);
  int32_t nmod = (int32_t)(d % n);
  if (nmod != 0 && ((uint32_t)n ^ (uint32_t)(d >> 32)) >= 0x80000000u) {
    ndiv -= 1;
    nmod += n;
  }
  ufoPush(nmod);
  ufoPush(ndiv);
}


// ////////////////////////////////////////////////////////////////////////// //
// simple logic and bit manipulation
//

#define UF_CMP(name_,op_)  \
UFWORD(name_) { \
  const uint32_t b = ufoPop(); \
  const uint32_t a = ufoPop(); \
  ufoPushBool(op_); \
}

// <
// ( a b -- a<b )
UF_CMP(LESS, (int32_t)a < (int32_t)b);

// U<
// ( a b -- a<b )
UF_CMP(ULESS, a < b);

// >
// ( a b -- a>b )
UF_CMP(GREAT, (int32_t)a > (int32_t)b);

// U>
// ( a b -- a>b )
UF_CMP(UGREAT, a > b);

// <=
// ( a b -- a<=b )
UF_CMP(LESSEQU, (int32_t)a <= (int32_t)b);

// U<=
// ( a b -- a<=b )
UF_CMP(ULESSEQU, a <= b);

// >=
// ( a b -- a>=b )
UF_CMP(GREATEQU, (int32_t)a >= (int32_t)b);

// U>=
// ( a b -- a>=b )
UF_CMP(UGREATEQU, a >= b);

// =
// ( a b -- a=b )
UF_CMP(EQU, a == b);

// <>
// ( a b -- a<>b )
UF_CMP(NOTEQU, a != b);

// 0=
// ( a -- a==0 )
UFWORD(ZERO_EQU) {
  const uint32_t a = ufoPop();
  ufoPushBool(a == 0);
}

// 0<>
// ( a -- a<>0 )
UFWORD(ZERO_NOTEQU) {
  const uint32_t a = ufoPop();
  ufoPushBool(a != 0);
}

// LAND
// ( a b -- a&&b )
UF_CMP(LOGAND, a && b);

// LOR
// ( a b -- a||b )
UF_CMP(LOGOR, a || b);

// AND
// ( a b -- a&b )
UFWORD(AND) {
  const uint32_t b = ufoPop();
  const uint32_t a = ufoPop();
  ufoPush(a&b);
}

// OR
// ( a b -- a|b )
UFWORD(OR) {
  const uint32_t b = ufoPop();
  const uint32_t a = ufoPop();
  ufoPush(a|b);
}

// XOR
// ( a b -- a^b )
UFWORD(XOR) {
  const uint32_t b = ufoPop();
  const uint32_t a = ufoPop();
  ufoPush(a^b);
}

// (LIT-AND)
// ( a -- a&[ip] )
UFWORD(LIT_AND) {
  UFO_STACK(1);
  UFO_TOS &= ufoImgGetU32(ufoIP); ufoIP += 4u;
}

// (LIT-~AND)
// ( a -- a&~[ip] )
UFWORD(LIT_NAND) {
  UFO_STACK(1);
  UFO_TOS &= ~ufoImgGetU32(ufoIP); ufoIP += 4u;
}

// (LIT-OR)
// ( a -- a|[ip] )
UFWORD(LIT_OR) {
  UFO_STACK(1);
  UFO_TOS |= ufoImgGetU32(ufoIP); ufoIP += 4u;
}

// (LIT-XOR)
// ( a -- a^[ip] )
UFWORD(LIT_XOR) {
  UFO_STACK(1);
  UFO_TOS ^= ufoImgGetU32(ufoIP); ufoIP += 4u;
}


// BITNOT
// ( a -- ~a )
UFWORD(BITNOT) {
  const uint32_t a = ufoPop();
  ufoPush(~a);
}

// ASH
// ( n count -- )
// arithmetic shift; positive `n` shifts to the left
UFWORD(ASH) {
  int32_t c = (int32_t)ufoPop();
  if (c < 0) {
    // right
    int32_t n = (int32_t)ufoPop();
    if (c >= -30) {
      n >>= (uint8_t)(-c);
    } else {
      if (n < 0) n = -1; else n = 0;
    }
    ufoPush((uint32_t)n);
  } else if (c > 0) {
    // left
    uint32_t u = ufoPop();
    if (c <= 31) {
      u <<= (uint8_t)c;
    } else {
      u = 0;
    }
    ufoPush(u);
  }
}

// LSH
// ( n count -- )
// logical shift; positive `n` shifts to the left
UFWORD(LSH) {
  int32_t c = (int32_t) ufoPop();
  uint32_t u = ufoPop();
  if (c < 0) {
    // right
    if (c >= -31) {
      u >>= (uint8_t)(-c);
    } else {
      u = 0;
    }
  } else if (c > 0) {
    // left
    if (c <= 31) {
      u <<= (uint8_t)c;
    } else {
      u = 0;
    }
  }
  ufoPush(u);
}

// ARSHIFT
// ( n count -- )
// arithmetic shift right
UFWORD(ARSHIFT) {
  int32_t c = (int32_t)ufoPop();
  if (c >= 0) {
    int32_t n = (int32_t)ufoPop();
    if (c <= 30) {
      n >>= (uint8_t)(c);
    } else {
      if (n < 0) n = -1; else n = 0;
    }
    ufoPush((uint32_t)n);
  } else {
    ufoFatal("negative shift");
  }
}

// RSHIFT
// ( n count -- )
// logical shift right
UFWORD(RSHIFT) {
  uint32_t c = (int32_t)ufoPop();
  if (c >= 0) {
    uint32_t n = (int32_t)ufoPop();
    if (c <= 31) {
      n >>= (uint8_t)(c);
    } else {
      n = 0;
    }
    ufoPush((uint32_t)n);
  } else {
    ufoFatal("negative shift");
  }
}

// LSHIFT
// ( n count -- )
// logical shift left
UFWORD(LSHIFT) {
  int32_t c = (int32_t) ufoPop();
  uint32_t u = ufoPop();
  if (c >= 0) {
    if (c <= 31) {
      u <<= (uint8_t)c;
    } else {
      u = 0;
    }
    ufoPush(u);
  } else {
    ufoFatal("negative shift");
  }
}

// ~AND
// ( a b -- a&~b )
UFWORD(BN_AND) {
  const uint32_t b = ufoPop();
  const uint32_t a = ufoPop();
  ufoPush(a&~b);
}

// ABS
// ( a -- |a| )
UFWORD(ABS) {
  if ((ufoPeek() & 0x80000000) != 0) {
    UFO_TOS = ~UFO_TOS + 1u;
  }
}

// NEGATE
// ( a -- -a )
UFWORD(NEGATE) {
  UFO_STACK(1);
  UFO_TOS = ~UFO_TOS + 1u;
}

// SIGN?
// ( n -- -1|0|1 )
UFWORD(SIGNQ) {
  const uint32_t a = ufoPop();
  if ((a & 0x80000000) != 0) ufoPush(~(uint32_t)0);
  else if (a != 0) ufoPush(1);
  else ufoPush(0);
}

// LO-WORD
// ( a -- a&0xffff )
UFWORD(LO_WORD) {
  UFO_STACK(1);
  UFO_TOS &= 0xffffU;
}

// LO-BYTE
// ( a -- a&0xff )
UFWORD(LO_BYTE) {
  UFO_STACK(1);
  UFO_TOS &= 0xffU;
}

// HI-WORD
// ( a -- (a>>16)&0xffff )
UFWORD(HI_WORD) {
  UFO_STACK(1);
  UFO_TOS = (UFO_TOS>>16)&0xffffU;
}

// HI-BYTE
// ( a -- (a>>8)&0xff )
UFWORD(HI_BYTE) {
  UFO_STACK(1);
  UFO_TOS = (UFO_TOS>>8)&0xffU;
}

// MIN
// ( a b -- min[a,b] )
UFWORD(MIN) {
  const int32_t b = (int32_t)ufoPop();
  UFO_STACK(1);
  if ((int32_t)UFO_TOS > b) UFO_TOS = (uint32_t)b;
}

// MAX
// ( a b -- max[a,b] )
UFWORD(MAX) {
  const int32_t b = (int32_t)ufoPop();
  UFO_STACK(1);
  if ((int32_t)UFO_TOS < b) UFO_TOS = (uint32_t)b;
}

// UMIN
// ( a b -- umin[a,b] )
UFWORD(UMIN) {
  const uint32_t b = ufoPop();
  UFO_STACK(1);
  if (UFO_TOS > b) UFO_TOS = b;
}

// UMAX
// ( a b -- umax[a,b] )
UFWORD(UMAX) {
  const uint32_t b = ufoPop();
  UFO_STACK(1);
  if (UFO_TOS < b) UFO_TOS = b;
}

// WITHIN
// ( a lo hi -- a>=lo&&a<hi )
UFWORD(WITHIN) {
  //const int32_t hi = (int32_t)ufoPop();
  //const int32_t lo = (int32_t)ufoPop();
  //const int32_t a = (int32_t)ufoPop();
  //ufoPushBool(a >= lo && a < hi);
  // sadly, idiotic ANS standard requires this:
  const uint32_t hi = ufoPop();
  const uint32_t lo = ufoPop();
  const uint32_t a = ufoPop();
  ufoPushBool(a - lo < hi - lo);
}

// UWITHIN
// ( ua ulo uhi -- ua>=ulo&&ua<uhi )
UFWORD(UWITHIN) {
  const uint32_t hi = ufoPop();
  const uint32_t lo = ufoPop();
  const uint32_t a = ufoPop();
  ufoPushBool(a >= lo && a < hi);
}

// BOUNDS?
// ( ua ulo uhi -- ua>=ulo&&ua<=uhi )
UFWORD(BOUNDSQ) {
  const uint32_t hi = ufoPop();
  const uint32_t lo = ufoPop();
  const uint32_t a = ufoPop();
  ufoPushBool(a >= lo && a <= hi);
}

// BSWAP16
// ( u -- u )
UFWORD(BSWAP16) {
  UFO_STACK(1);
  const uint32_t a = UFO_TOS;
  UFO_TOS = (uint32_t)__builtin_bswap16((uint16_t)a);
}

// BSWAP32
// ( u -- u )
UFWORD(BSWAP32) {
  UFO_STACK(1);
  const uint32_t a = UFO_TOS;
  UFO_TOS = __builtin_bswap32(a);
}

// (SWAP:1+:SWAP)
// ( a b -- a+1 b )
UFWORD(PAR_SWAP_INC_SWAP) {
  UFO_STACK(2);
  UFO_S(1) += 1u;
}

// 1+
// ( a -- a+1 )
UFWORD(1ADD) {
  UFO_STACK(1);
  UFO_TOS += 1u;
}

// 1-
// ( a -- a-1 )
UFWORD(1SUB) {
  UFO_STACK(1);
  UFO_TOS -= 1u;
}

// 2+
// ( a -- a+2 )
UFWORD(2ADD) {
  UFO_STACK(1);
  UFO_TOS += 2u;
}

// 2-
// ( a -- a-2 )
UFWORD(2SUB) {
  UFO_STACK(1);
  UFO_TOS -= 2u;
}

// 4+
// ( a -- a+4 )
UFWORD(4ADD) {
  UFO_STACK(1);
  UFO_TOS += 4u;
}

// 4-
// ( a -- a-4 )
UFWORD(4SUB) {
  UFO_STACK(1);
  UFO_TOS -= 4u;
}

// 8+
// ( a -- a+8 )
UFWORD(8ADD) {
  UFO_STACK(1);
  UFO_TOS += 8u;
}

// 8-
// ( a -- a-8 )
UFWORD(8SUB) {
  UFO_STACK(1);
  UFO_TOS -= 8u;
}

// +CELLS
// ( a n -- a+n*4 )
UFWORD(ADD_CELLS) {
  const uint32_t n = ufoPop();
  UFO_STACK(1);
  UFO_TOS += n << 2;
}

// -CELLS
// ( a n -- a-n*4 )
UFWORD(SUB_CELLS) {
  const uint32_t n = ufoPop();
  UFO_STACK(1);
  UFO_TOS -= n << 2;
}

// 2*
// ( a -- a<<1 )
UFWORD(2MUL) {
  UFO_STACK(1);
  UFO_TOS <<= 1;
}

// 4*
// ( a -- a<<2 )
UFWORD(4MUL) {
  UFO_STACK(1);
  UFO_TOS <<= 2;
}

// 8*
// ( a -- a<<3 )
UFWORD(8MUL) {
  UFO_STACK(1);
  UFO_TOS <<= 3;
}

// 2/
// ( a -- a>>1 )
UFWORD(2DIV) {
  UFO_STACK(1);
  UFO_TOS = (uint32_t)((int32_t)UFO_TOS >> 1);
}

// 4/
// ( a -- a>>2 )
UFWORD(4DIV) {
  UFO_STACK(1);
  UFO_TOS = (uint32_t)((int32_t)UFO_TOS >> 2);
}

// 8/
// ( a -- a>>3 )
UFWORD(8DIV) {
  UFO_STACK(1);
  UFO_TOS = (uint32_t)((int32_t)UFO_TOS >> 3);
}

// 2U/
// ( a -- a>>1 )
UFWORD(2UDIV) {
  UFO_STACK(1);
  UFO_TOS >>= 1;
}

// 4U/
// ( a -- a>>2 )
UFWORD(4UDIV) {
  UFO_STACK(1);
  UFO_TOS >>= 2;
}

// 8U/
// ( a -- a>>3 )
UFWORD(8UDIV) {
  UFO_STACK(1);
  UFO_TOS >>= 3;
}

// 0<
// ( a -- a<0? )
UFWORD(0LESS) {
  UFO_STACK(1);
  if ((int32_t)UFO_TOS < 0) UFO_TOS = ufoTrueValue; else UFO_TOS = 0;
}

// 0<=
// ( a -- a<=0? )
UFWORD(0LESSEQU) {
  UFO_STACK(1);
  if ((int32_t)UFO_TOS <= 0) UFO_TOS = ufoTrueValue; else UFO_TOS = 0;
}

// 0>
// ( a -- a>0? )
UFWORD(0GREAT) {
  UFO_STACK(1);
  if ((int32_t)UFO_TOS > 0) UFO_TOS = ufoTrueValue; else UFO_TOS = 0;
}

// 0>=
// ( a -- a>=0? )
UFWORD(0GREATEQU) {
  UFO_STACK(1);
  if ((int32_t)UFO_TOS >= 0) UFO_TOS = ufoTrueValue; else UFO_TOS = 0;
}


// ////////////////////////////////////////////////////////////////////////// //
// string unescaping
//

// (UNESCAPE)
// ( addr count -- addr count )
UFWORD(PAR_UNESCAPE) {
  const uint32_t count = ufoPop();
  const uint32_t addr = ufoPeek();
  if ((count & ((uint32_t)1<<31)) == 0) {
    const uint32_t eaddr = addr + count;
    uint32_t caddr = addr;
    uint32_t daddr = addr;
    while (caddr != eaddr) {
      uint8_t ch = ufoImgGetU8(caddr); caddr += 1u;
      if (ch == '\\' && caddr != eaddr) {
        ch = ufoImgGetU8(caddr); caddr += 1u;
        switch (ch) {
          case 'r': ch = '\r'; break;
          case 'n': ch = '\n'; break;
          case 't': ch = '\t'; break;
          case 'e': ch = '\x1b'; break;
          case '`': ch = '"'; break; // special escape to insert double-quote
          case '"': ch = '"'; break;
          case '\\': ch = '\\'; break;
          case 'x': case 'X':
            if (eaddr - daddr >= 1) {
              const int dg0 = digitInBase((char)(ufoImgGetU8(caddr)), 16);
              if (dg0 < 0) ufoFatal("invalid hex string escape");
              if (eaddr - daddr >= 2) {
                const int dg1 = digitInBase((char)(ufoImgGetU8(caddr + 1u)), 16);
                if (dg1 < 0) ufoFatal("invalid hex string escape");
                ch = (uint8_t)(dg0 * 16 + dg1);
                caddr += 2u;
              } else {
                ch = (uint8_t)dg0;
                caddr += 1u;
              }
            } else {
              ufoFatal("invalid hex string escape");
            }
            break;
          default: ufoFatal("invalid string escape");
        }
      }
      ufoImgPutU8(daddr, ch); daddr += 1u;
    }
    ufoPush(daddr - addr);
  } else {
    ufoPush(count);
  }
}


// ////////////////////////////////////////////////////////////////////////// //
// numeric conversions
//

// (BASED-NUMBER) ( addr count allowsign? base -- num TRUE / FALSE )
UFWORD(PAR_BASED_NUMBER) {
  const uint32_t xbase = ufoPop();
  const uint32_t allowSign = ufoPop();
  int32_t count = (int32_t)ufoPop();
  uint32_t addr = ufoPop();
  uint32_t n = 0;
  int base = 0;
  int neg = 0;
  uint8_t ch;

  if (allowSign && count > 1) {
    ch = ufoImgGetU8(addr);
    if (ch == '-') { neg = 1; addr += 1u; count -= 1; }
    else if (ch == '+') { neg = 0; addr += 1u; count -= 1; }
  }

  // special-based numbers
  ch = ufoImgGetU8(addr);
  if (count >= 3 && ch == '0') {
    switch (ufoImgGetU8(addr + 1u)) {
      case 'x': case 'X': base = 16; break;
      case 'o': case 'O': base = 8; break;
      case 'b': case 'B': base = 2; break;
      case 'd': case 'D': base = 10; break;
      default: break;
    }
    if (base && digitInBase((char)ufoImgGetU8(addr + (uint32_t)count - 1u), base) >= 0) {
      addr += 2; count -= 2;
    } else {
      base = 0;
    }
  } else if (count >= 2 && ch == '$') {
    base = 16;
    addr += 1u; count -= 1;
  } else if (count >= 2 && ch == '#') {
    base = 16;
    addr += 1u; count -= 1;
  } else if (count >= 2 && ch == '%') {
    base = 2;
    addr += 1u; count -= 1;
  } else if (count >= 3 && ch == '&') {
    switch (ufoImgGetU8(addr + 1u)) {
      case 'h': case 'H': base = 16; break;
      case 'o': case 'O': base = 8; break;
      case 'b': case 'B': base = 2; break;
      case 'd': case 'D': base = 10; break;
      default: break;
    }
    if (base) { addr += 2u; count -= 2; }
  }
  if (!base && count > 2 && ch >= '0' && ch <= '9') {
    ch = ufoImgGetU8(addr + (uint32_t)count - 1u);
    switch (ch) {
      case 'b': case 'B': if (xbase < 12) base = 2; break;
      case 'o': case 'O': if (xbase < 25) base = 8; break;
      case 'h': case 'H': if (xbase < 18) base = 16; break;
    }
    if (base) count -= 1;
  }

  // in current base?
  if (!base && xbase < 255) base = xbase;

  if (count <= 0 || base < 1 || base > 36) {
    ufoPushBool(0);
  } else {
    uint32_t nc;
    int wasDig = 0, wasUnder = 1, error = 0, dig;
    while (!error && count != 0) {
      ch = ufoImgGetU8(addr); addr += 1u; count -= 1;
      if (ch != '_') {
        error = 1; wasUnder = 0; wasDig = 1;
        dig = digitInBase((char)ch, (int)base);
        if (dig >= 0) {
          nc = n * (uint32_t)base;
          if (nc >= n) {
            nc += (uint32_t)dig;
            if (nc >= n) {
              n = nc;
              error = 0;
            }
          }
        }
      } else {
        error = wasUnder;
        wasUnder = 1;
      }
    }

    if (!error && wasDig && !wasUnder) {
      if (allowSign && neg) n = ~n + 1u;
      ufoPush(n);
      ufoPushBool(1);
    } else {
      ufoPushBool(0);
    }
  }
}


// ////////////////////////////////////////////////////////////////////////// //
// compiler-related, dictionary-related
//

static char ufoWNameBuf[256];

// (CREATE-WORD-HEADER)
// ( addr count word-flags -- )
UFWORD(PAR_CREATE_WORD_HEADER) {
  const uint32_t flags = ufoPop();
  const uint32_t wlen = ufoPop();
  const uint32_t waddr = ufoPop();
  if (wlen == 0) ufoFatal("word name expected");
  if (wlen >= UFO_MAX_WORD_LENGTH) ufoFatal("word name too long");
  // copy to separate buffer
  for (uint32_t f = 0; f < wlen; f += 1) {
    ufoWNameBuf[f] = (char)ufoImgGetU8(waddr + f);
  }
  ufoWNameBuf[wlen] = 0;
  ufoCreateWordHeader(ufoWNameBuf, flags);
}

// (CREATE-NAMELESS-WORD-HEADER)
// ( word-flags -- )
UFWORD(PAR_CREATE_NAMELESS_WORD_HEADER) {
  const uint32_t flags = ufoPop();
  ufoCreateWordHeader("", flags);
}

// FIND-WORD
// ( addr count -- cfa TRUE / FALSE )
UFWORD(FIND_WORD) {
  const uint32_t wlen = ufoPop();
  const uint32_t waddr = ufoPop();
  if (wlen > 0 && wlen < UFO_MAX_WORD_LENGTH) {
    // copy to separate buffer
    for (uint32_t f = 0; f < wlen; f += 1) {
      ufoWNameBuf[f] = (char)ufoImgGetU8(waddr + f);
    }
    ufoWNameBuf[wlen] = 0;
    const uint32_t cfa = ufoFindWord(ufoWNameBuf);
    if (cfa != 0) {
      ufoPush(cfa);
      ufoPushBool(1);
    } else {
      ufoPushBool(0);
    }
  } else {
    ufoPushBool(0);
  }
}

// (FIND-WORD-IN-VOC)
// ( addr count vocid allowhidden -- cfa TRUE / FALSE)
// find only in the given voc; no name resolution
UFWORD(PAR_FIND_WORD_IN_VOC) {
  const uint32_t allowHidden = ufoPop();
  const uint32_t vocid = ufoPop();
  const uint32_t wlen = ufoPop();
  const uint32_t waddr = ufoPop();
  if (wlen > 0 && wlen < UFO_MAX_WORD_LENGTH) {
    // copy to separate buffer
    for (uint32_t f = 0; f < wlen; f += 1) {
      ufoWNameBuf[f] = (char)ufoImgGetU8(waddr + f);
    }
    ufoWNameBuf[wlen] = 0;
    const uint32_t cfa = ufoFindWordInVoc(ufoWNameBuf, wlen, 0, vocid, (allowHidden ? 1 : 0));
    if (cfa != 0) {
      ufoPush(cfa);
      ufoPushBool(1);
    } else {
      ufoPushBool(0);
    }
  } else {
    ufoPushBool(0);
  }
}

// (FIND-WORD-IN-VOC-AND-PARENTS)
// ( addr count vocid allowhidden -- cfa TRUE / FALSE)
// find only in the given voc; no name resolution
UFWORD(PAR_FIND_WORD_IN_VOC_AND_PARENTS) {
  const uint32_t allowHidden = ufoPop();
  const uint32_t vocid = ufoPop();
  const uint32_t wlen = ufoPop();
  const uint32_t waddr = ufoPop();
  if (wlen > 0 && wlen < UFO_MAX_WORD_LENGTH) {
    // copy to separate buffer
    for (uint32_t f = 0; f < wlen; f += 1) {
      ufoWNameBuf[f] = (char)ufoImgGetU8(waddr + f);
    }
    ufoWNameBuf[wlen] = 0;
    const uint32_t cfa = ufoFindWordInVocAndParents(ufoWNameBuf, wlen, 0, vocid, (allowHidden ? 1 : 0));
    if (cfa != 0) {
      ufoPush(cfa);
      ufoPushBool(1);
    } else {
      ufoPushBool(0);
    }
  } else {
    ufoPushBool(0);
  }
}

// FIND-WORD-IN-VOC
// ( addr count vocid -- cfa TRUE / FALSE)
// find only in the given voc; no name resolution, no hidden words
UFWORD(FIND_WORD_IN_VOC) { ufoPush(0); UFCALL(PAR_FIND_WORD_IN_VOC); }

// FIND-WORD-IN-VOC-AND-PARENTS
// ( addr count vocid -- cfa TRUE / FALSE)
// find only in the given voc; no name resolution, no hidden words
UFWORD(FIND_WORD_IN_VOC_AND_PARENTS) { ufoPush(0); UFCALL(PAR_FIND_WORD_IN_VOC_AND_PARENTS); }


// ////////////////////////////////////////////////////////////////////////// //
// more compiler words
//

// ////////////////////////////////////////////////////////////////////////// //
// vocabulary and wordlist utilities
//

// (VSP@)
// ( -- vsp )
UFWORD(PAR_GET_VSP) {
  ufoPush(ufoVSP);
}

// (VSP!)
// ( vsp -- )
UFWORD(PAR_SET_VSP) {
  const uint32_t vsp = ufoPop();
  if (vsp > UFO_VOCSTACK_SIZE) ufoFatal("VSP %u out of range (%u)", vsp, UFO_VOCSTACK_SIZE);
  ufoVSP = vsp;
}

// (VSP-AT@)
// ( idx -- value )
UFWORD(PAR_VSP_LOAD) {
  const uint32_t vsp = ufoPop();
  if (vsp >= UFO_VOCSTACK_SIZE) ufoFatal("VSP %u out of range (%u)", vsp, UFO_VOCSTACK_SIZE);
  ufoPush(ufoVocStack[vsp]);
}

// (VSP-AT!)
// ( value idx -- )
UFWORD(PAR_VSP_STORE) {
  const uint32_t vsp = ufoPop();
  const uint32_t value = ufoPop();
  if (vsp >= UFO_VOCSTACK_SIZE) ufoFatal("VSP %u out of range (%u)", vsp, UFO_VOCSTACK_SIZE);
  ufoVocStack[vsp] = value;
}


// ////////////////////////////////////////////////////////////////////////// //
// word field address conversion
//

// CFA->DOES-CFA
// ( cfa -- does-cfa )
UFWORD(CFA2DOESCFA) {
  UFO_STACK(1);
  UFO_TOS = UFO_CFA_TO_DOES_CFA(UFO_TOS);
}

// CFA->PFA
// ( cfa -- pfa )
UFWORD(CFA2PFA) {
  UFO_STACK(1);
  UFO_TOS = UFO_CFA_TO_PFA(UFO_TOS);
}

// CFA->NFA
// ( cfa -- nfa )
UFWORD(CFA2NFA) {
  UFO_STACK(1);
  UFO_TOS = UFO_CFA_TO_NFA(UFO_TOS);
}

// CFA->LFA
// ( cfa -- lfa )
UFWORD(CFA2LFA) {
  UFO_STACK(1);
  UFO_TOS = UFO_CFA_TO_LFA(UFO_TOS);
}

// CFA->WEND
// ( cfa -- wend-addr )
UFWORD(CFA2WEND) {
  UFO_STACK(1);
  UFO_TOS = ufoGetWordEndAddr(UFO_TOS);
}

// PFA->CFA
// ( pfa -- cfa )
UFWORD(PFA2CFA) {
  UFO_STACK(1);
  UFO_TOS = UFO_PFA_TO_CFA(UFO_TOS);
}

// PFA->NFA
// ( pfa -- nfa )
UFWORD(PFA2NFA) {
  UFO_STACK(1);
  UFO_TOS = UFO_PFA_TO_CFA(UFO_TOS);
  UFO_TOS = UFO_CFA_TO_NFA(UFO_TOS);
}

// NFA->CFA
// ( nfa -- cfa )
UFWORD(NFA2CFA) {
  UFO_STACK(1);
  UFO_TOS = UFO_NFA_TO_CFA(UFO_TOS);
}

// NFA->PFA
// ( nfa -- pfa )
UFWORD(NFA2PFA) {
  UFO_STACK(1);
  UFO_TOS = UFO_NFA_TO_CFA(UFO_TOS);
  UFO_TOS = UFO_CFA_TO_PFA(UFO_TOS);
}

// NFA->LFA
// ( nfa -- lfa )
UFWORD(NFA2LFA) {
  UFO_STACK(1);
  UFO_TOS = UFO_NFA_TO_LFA(UFO_TOS);
}

// LFA->CFA
// ( lfa -- cfa )
UFWORD(LFA2CFA) {
  UFO_STACK(1);
  UFO_TOS = UFO_LFA_TO_CFA(UFO_TOS);
}

// LFA->PFA
// ( lfa -- pfa )
UFWORD(LFA2PFA) {
  UFO_STACK(1);
  UFO_TOS = UFO_LFA_TO_CFA(UFO_TOS);
  UFO_TOS = UFO_CFA_TO_PFA(UFO_TOS);
}

// LFA->BFA
// ( lfa -- bfa )
UFWORD(LFA2BFA) {
  UFO_STACK(1);
  UFO_TOS = UFO_LFA_TO_BFA(UFO_TOS);
}

// LFA->XFA
// ( lfa -- xfa )
UFWORD(LFA2XFA) {
  UFO_STACK(1);
  UFO_TOS = UFO_LFA_TO_XFA(UFO_TOS);
}

// LFA->YFA
// ( lfa -- yfa )
UFWORD(LFA2YFA) {
  UFO_STACK(1);
  UFO_TOS = UFO_LFA_TO_YFA(UFO_TOS);
}

// LFA->NFA
// ( lfa -- nfa )
UFWORD(LFA2NFA) {
  UFO_STACK(1);
  UFO_TOS = UFO_LFA_TO_NFA(UFO_TOS);
}

// IP->NFA
// ( ip -- nfa / 0 )
UFWORD(IP2NFA) {
  UFO_STACK(1);
  UFO_TOS = ufoFindWordForIP(UFO_TOS);
}

// IP->FILE/LINE
// ( ip -- addr count line TRUE / FALSE )
// name is at PAD; it is safe to use PAD, because each task has its own temp image
UFWORD(IP2FILELINE) {
  const uint32_t ip = ufoPop();
  uint32_t fline;
  const char *fname = ufoFindFileForIP(ip, &fline, NULL, NULL);
  if (fname != NULL) {
    uint32_t addr = UFO_PAD_ADDR;
    uint32_t count = 0;
    while (*fname != 0) {
      ufoImgPutU8(addr, *(const unsigned char *)fname);
      fname += 1u; addr += 1u; count += 1u;
    }
    ufoImgPutU8(addr, 0); // just in case
    ufoPush(count);
    ufoPush(fline);
    ufoPushBool(1);
  } else {
    ufoPushBool(0);
  }
}


// IP->FILE-HASH/LINE
// ( ip -- len hash line TRUE / FALSE )
UFWORD(IP2FILEHASHLINE) {
  const uint32_t ip = ufoPop();
  uint32_t fline, fhash, flen;
  const char *fname = ufoFindFileForIP(ip, &fline, &flen, &fhash);
  if (fname != NULL) {
    ufoPush(flen);
    ufoPush(fhash);
    ufoPush(fline);
    ufoPushBool(1);
  } else {
    ufoPushBool(0);
  }
}


// ////////////////////////////////////////////////////////////////////////// //
// string operations
//

UFO_FORCE_INLINE uint32_t ufoHashBuf (uint32_t addr, uint32_t size, uint8_t orbyte) {
  uint32_t hash = 0x29a;
  if ((size & ((uint32_t)1<<31)) == 0) {
    while (size != 0) {
      hash += ufoImgGetU8(addr) | orbyte;
      hash += hash<<10;
      hash ^= hash>>6;
      addr += 1u; size -= 1u;
    }
  }
  // finalize
  hash += hash<<3;
  hash ^= hash>>11;
  hash += hash<<15;
  return hash;
}

//==========================================================================
//
//  ufoBufEqu
//
//==========================================================================
UFO_FORCE_INLINE int ufoBufEqu (uint32_t addr0, uint32_t addr1, uint32_t count) {
  int res;
  if ((count & ((uint32_t)1<<31)) == 0) {
    res = 1;
    while (res != 0 && count != 0) {
      res = (toUpperU8(ufoImgGetU8(addr0)) == toUpperU8(ufoImgGetU8(addr1)));
      addr0 += 1u; addr1 += 1u; count -= 1u;
    }
  } else {
    res = 0;
  }
  return res;
}

// STRING:=
// ( a0 c0 a1 c1 -- bool )
UFWORD(STREQU) {
  int32_t c1 = (int32_t)ufoPop();
  uint32_t a1 = ufoPop();
  int32_t c0 = (int32_t)ufoPop();
  uint32_t a0 = ufoPop();
  if (c0 < 0) c0 = 0;
  if (c1 < 0) c1 = 0;
  if (c0 == c1) {
    int res = 1;
    while (res != 0 && c0 != 0) {
      res = (ufoImgGetU8(a0) == ufoImgGetU8(a1));
      a0 += 1; a1 += 1; c0 -= 1;
    }
    ufoPushBool(res);
  } else {
    ufoPushBool(0);
  }
}

// STRING:=CI
// ( a0 c0 a1 c1 -- bool )
UFWORD(STREQUCI) {
  int32_t c1 = (int32_t)ufoPop();
  uint32_t a1 = ufoPop();
  int32_t c0 = (int32_t)ufoPop();
  uint32_t a0 = ufoPop();
  if (c0 < 0) c0 = 0;
  if (c1 < 0) c1 = 0;
  if (c0 == c1) {
    int res = 1;
    while (res != 0 && c0 != 0) {
      res = (toUpperU8(ufoImgGetU8(a0)) == toUpperU8(ufoImgGetU8(a1)));
      a0 += 1; a1 += 1; c0 -= 1;
    }
    ufoPushBool(res);
  } else {
    ufoPushBool(0);
  }
}

// search the string specified by c-addr1 u1 for the string specified by c-addr2 u2.
// if flag is true, a match was found at c-addr3 with u3 characters remaining.
// if flag is false there was no match and c-addr3 is c-addr1 and u3 is u1.
// ( c-addr1 u1 c-addr2 u2 -- c-addr3 u3 flag )
UFWORD(SEARCH) {
  const uint32_t pcount = ufoPop();
  const uint32_t paddr = ufoPop();
  const uint32_t tcount = ufoPop();
  const uint32_t taddr = ufoPop();
  if ((pcount & ((uint32_t)1 << 31)) == 0 && (tcount & ((uint32_t)1 << 31)) == 0) {
    for (uint32_t f = 0; tcount - f >= pcount; f += 1) {
      if (ufoBufEqu(taddr + f, paddr, pcount)) {
        ufoPush(taddr + f);
        ufoPush(tcount - f);
        ufoPushBool(1);
        return;
      }
    }
  }
  ufoPush(taddr);
  ufoPush(tcount);
  ufoPushBool(0);
}

// STRING:HASH
// ( addr count -- hash )
UFWORD(STRHASH) {
  uint32_t count = ufoPop();
  uint32_t addr = ufoPop();
  ufoPush(ufoHashBuf(addr, count, 0));
}

// STRING:HASH-CI
// ( addr count -- hash )
UFWORD(STRHASHCI) {
  uint32_t count = ufoPop();
  uint32_t addr = ufoPop();
  ufoPush(ufoHashBuf(addr, count, 0x20));
}

// STRING:CHAR-UPPER
// ( ch -- ch )
UFWORD(CHAR_UPPER) {
  UFO_STACK(1);
  uint32_t c = UFO_TOS & 0xffU;
  if (c >= 'a' && c <= 'z') c = c - 'a' + 'A';
  UFO_TOS = c;
}

// STRING:CHAR-LOWER
// ( ch -- ch )
UFWORD(CHAR_LOWER) {
  UFO_STACK(1);
  uint32_t c = UFO_TOS & 0xffU;
  if (c >= 'A' && c <= 'Z') c = c - 'A' + 'a';
  UFO_TOS = c;
}

// STRING:UPPER
// ( addr count -- )
UFWORD(STRUPPER) {
  int32_t count = (int32_t)ufoPop();
  uint32_t addr = ufoPop();
  while (count > 0) {
    uint32_t c = ufoImgGetU8(addr);
    if (c >= 'a' && c <= 'z') {
      c = c - 'a' + 'A';
      ufoImgPutU8(addr, c);
    }
    addr += 1u; count -= 1;
  }
}

// STRING:LOWER
// ( addr count -- )
UFWORD(STRLOWER) {
  int32_t count = (int32_t)ufoPop();
  uint32_t addr = ufoPop();
  while (count > 0) {
    uint32_t c = ufoImgGetU8(addr);
    if (c >= 'A' && c <= 'Z') {
      c = c - 'A' + 'a';
      ufoImgPutU8(addr, c);
    }
    addr += 1u; count -= 1;
  }
}

// STRING:(CHAR-DIGIT)
// ( ch -- digit true // false )
UFWORD(CHAR_DIGIT) {
  UFO_STACK(1);
  const uint32_t c = UFO_TOS;
  if (c >= '0' && c <= '9') { UFO_TOS = c - '0'; ufoPushBool(1); }
  else if (c >= 'A' && c <= 'Z') { UFO_TOS = c - 'A' + 10; ufoPushBool(1); }
  else if (c >= 'a' && c <= 'z') { UFO_TOS = c - 'a' + 10; ufoPushBool(1); }
  else UFO_TOS = 0;
}

// STRING:DIGIT
// ( char base -- digit TRUE / FALSE )
UFWORD(DIGIT) {
  const uint32_t base = ufoPop();
  UFO_STACK(1);
  if (base > 0 && base < 0x80000000u) {
    uint32_t c = UFO_TOS;
    if (c >= '0' && c <= '9') c = c - '0';
    else if (c >= 'A' && c <= 'Z') c = c - 'A' + 10;
    else if (c >= 'a' && c <= 'z') c = c - 'a' + 10;
    else { UFO_TOS = 0; return; }
    if (c < base) { UFO_TOS = c; ufoPushBool(1); } else UFO_TOS = 0;
  } else {
    UFO_TOS = 0;
  }
}

// STRING:DIGIT?
// ( char base -- TRUE / FALSE )
UFWORD(DIGITQ) {
  const uint32_t base = ufoPop();
  UFO_STACK(1);
  if (base > 0 && base < 0x80000000u) {
    uint32_t c = UFO_TOS;
    if (c >= '0' && c <= '9') c = c - '0';
    else if (c >= 'A' && c <= 'Z') c = c - 'A' + 10;
    else if (c >= 'a' && c <= 'z') c = c - 'a' + 10;
    else { UFO_TOS = 0; return; }
    if (c < base) UFO_TOS = ufoTrueValue; else UFO_TOS = 0;
  } else {
    UFO_TOS = 0;
  }
}


// MEMCMP
// ( addr1 addr2 size -- -1|0|1 )
UFWORD(MEMCMP) {
  uint32_t count = ufoPop();
  uint32_t addr1 = ufoPop();
  uint32_t addr0 = ufoPop();
  if ((count & 0x80000000u) == 0) {
    while (count != 0) {
      const int n = (int)ufoImgGetU8(addr0) - (int)ufoImgGetU8(addr1);
      if (n != 0) {
        if (n < 0) ufoPush(~(uint32_t)0); else ufoPush(1);
        return;
      }
      addr0 += 1u; addr1 += 1u; count -= 1u;
    }
    ufoPush(0);
  } else {
    ufoFatal("invalid MEMCMP counter");
  }
}

// MEMCMP-CI
// ( addr1 addr2 size -- -1|0|1 )
UFWORD(MEMCMP_CI) {
  uint32_t count = ufoPop();
  uint32_t addr1 = ufoPop();
  uint32_t addr0 = ufoPop();
  if ((count & 0x80000000u) == 0) {
    while (count != 0) {
      const int c0 = (int)toUpperU8(ufoImgGetU8(addr0));
      const int c1 = (int)toUpperU8(ufoImgGetU8(addr1));
      const int n = c0 - c1;
      if (n != 0) {
        if (n < 0) ufoPush(~(uint32_t)0); else ufoPush(1);
        return;
      }
      addr0 += 1u; addr1 += 1u; count -= 1u;
    }
    ufoPush(0);
  } else {
    ufoFatal("invalid MEMCMP counter");
  }
}

// FILL-CELLS
// ( addr count u32 -- )
UFWORD(FILL_CELLS) {
  const uint32_t v = ufoPop();
  uint32_t count = ufoPop();
  uint32_t dest = ufoPop();
  if ((count & 0x80000000u) == 0) {
    while (count != 0) {
      ufoImgPutU32(dest, v);
      dest += 4u; count -= 1u;
    }
  }
}

// FILL
// ( addr count byte -- )
UFWORD(FILL) {
  const uint32_t v = ufoPop() & 0xffU;
  uint32_t count = ufoPop();
  uint32_t dest = ufoPop();
  if (count != 0 && (count & 0x80000000u) == 0) {
    while (count != 0 && (dest & 3) != 0) {
      ufoImgPutU8(dest, v);
      dest += 1u; count -= 1u;
    }
    if (count >= 4u) {
      const uint32_t vv = (v << 24) | (v << 16) | (v << 8) | v;
      while (count >= 4u) {
        ufoImgPutU32(dest, vv);
        dest += 4u; count -= 4u;
      }
    }
    while (count != 0) {
      ufoImgPutU8(dest, v);
      dest += 1u; count -= 1u;
    }
  }
}

//==========================================================================
//
//  doCMoveFwd
//
//==========================================================================
static void doCMoveFwd (uint32_t src, uint32_t dest, uint32_t count) {
  uint32_t v;
  if (count != 0 && (count & 0x80000000u) == 0 && src != dest) {
    if ((src & 3) == (dest & 3)) {
      // we can align addresses
      while (count != 0 && (src & 3) != 0) {
        v = ufoImgGetU8(src); ufoImgPutU8(dest, v);
        src += 1u; dest += 1u; count -= 1u;
      }
      // ...and move by whole cells
      while (count >= 4u) {
        v = ufoImgGetU32(src); ufoImgPutU32(dest, v);
        src += 4u; dest += 4u; count -= 4u;
      }
    }
    // do the rest
    while (count != 0) {
      v = ufoImgGetU8(src); ufoImgPutU8(dest, v);
      src += 1u; dest += 1u; count -= 1u;
    }
  }
}

//==========================================================================
//
//  doCMoveBwd
//
//==========================================================================
static void doCMoveBwd (uint32_t src, uint32_t dest, uint32_t count) {
  if (count != 0 && (count & 0x80000000u) == 0 && src != dest) {
    src += count; dest += count;
    while (count != 0) {
      src -= 1u; dest -= 1u; count -= 1u;
      const uint8_t v = ufoImgGetU8(src); ufoImgPutU8(dest, v);
    }
  }
}

// CMOVE-CELLS
// ( source dest count -- )
UFWORD(CMOVE_CELLS_FWD) {
  uint32_t count = ufoPop();
  uint32_t dest = ufoPop();
  uint32_t src = ufoPop();
  if (count != 0 && (count & 0x80000000u) == 0 && src != dest) {
    if (count * 4u >= 0x80000000u) ufoFatal("invalid CMOVE-CELLS counter");
    doCMoveFwd(src, dest, count * 4u);
  }
}

// CMOVE>-CELLS
// ( source dest count -- )
UFWORD(CMOVE_CELLS_BWD) {
  uint32_t count = ufoPop();
  uint32_t dest = ufoPop();
  uint32_t src = ufoPop();
  if ((count & 0x80000000u) == 0) {
    src += count * 4u; dest += count * 4u;
    while (count != 0) {
      src -= 4u; dest -= 4u; count -= 1u;
      const uint32_t v = ufoImgGetU32(src); ufoImgPutU32(dest, v);
    }
  }
}

// CMOVE
// ( source dest count -- )
UFWORD(CMOVE_FWD) {
  uint32_t count = ufoPop();
  uint32_t dest = ufoPop();
  uint32_t src = ufoPop();
  doCMoveFwd(src, dest, count);
}

// CMOVE>
// ( source dest count -- )
UFWORD(CMOVE_BWD) {
  uint32_t count = ufoPop();
  uint32_t dest = ufoPop();
  uint32_t src = ufoPop();
  doCMoveBwd(src, dest, count);
}

// MOVE
// ( source dest count -- )
UFWORD(MOVE) {
  uint32_t count = ufoPop();
  uint32_t dest = ufoPop();
  uint32_t src = ufoPop();
  if (count != 0 && (count & 0x80000000u) == 0 && src != dest) {
    if (src + count <= src || dest + count <= dest) ufoFatal("invalid MOVE");
    if (src <= dest && src + count > dest) doCMoveBwd(src, dest, count);
    else doCMoveFwd(src, dest, count);
  }
}


// ////////////////////////////////////////////////////////////////////////// //
// heavily used in UrAsm
//

// IS-DIGIT
// ( ch -- bool )
UFWORD(IS_DIGIT) {
  UFO_STACK(1);
  const uint32_t c = UFO_TOS & 0xffU;
  if (c >= '0' && c <= '9') UFO_TOS = ufoTrueValue; else UFO_TOS = 0;
}

// IS-BIN-DIGIT
// ( ch -- bool )
UFWORD(IS_BIN_DIGIT) {
  UFO_STACK(1);
  const uint32_t c = UFO_TOS & 0xffU;
  if (c >= '0' && c <= '1') UFO_TOS = ufoTrueValue; else UFO_TOS = 0;
}

// IS-OCT-DIGIT
// ( ch -- bool )
UFWORD(IS_OCT_DIGIT) {
  UFO_STACK(1);
  const uint32_t c = UFO_TOS & 0xffU;
  if (c >= '0' && c <= '7') UFO_TOS = ufoTrueValue; else UFO_TOS = 0;
}

// IS-HEX-DIGIT
// ( ch -- bool )
UFWORD(IS_HEX_DIGIT) {
  UFO_STACK(1);
  const uint32_t c = UFO_TOS & 0xffU;
  if ((c >= '0' && c <= '9') ||
      (c >= 'A' && c <= 'F') ||
      (c >= 'a' && c <= 'f')) UFO_TOS = ufoTrueValue; else UFO_TOS = 0;
}

// IS-ALPHA
// ( ch -- bool )
UFWORD(IS_ALPHA) {
  UFO_STACK(1);
  const uint32_t c = UFO_TOS & 0xffU;
  if ((c >= 'A' && c <= 'Z') ||
      (c >= 'a' && c <= 'z')) UFO_TOS = ufoTrueValue; else UFO_TOS = 0;
}

// IS-UNDER-DOT
// ( ch -- bool )
UFWORD(IS_UNDER_DOT) {
  UFO_STACK(1);
  const uint32_t c = UFO_TOS & 0xffU;
  if (c == '_' || c == '.') UFO_TOS = ufoTrueValue; else UFO_TOS = 0;
}

// IS-ALNUM
// ( ch -- bool )
UFWORD(IS_ALNUM) {
  UFO_STACK(1);
  const uint32_t c = UFO_TOS & 0xffU;
  if ((c >= 'A' && c <= 'Z') ||
      (c >= 'a' && c <= 'z') ||
      (c >= '0' && c <= '9')) UFO_TOS = ufoTrueValue; else UFO_TOS = 0;
}

// IS-ID-START
// ( ch -- bool )
UFWORD(IS_ID_START) {
  UFO_STACK(1);
  const uint32_t c = UFO_TOS & 0xffU;
  if ((c >= 'A' && c <= 'Z') ||
      (c >= 'a' && c <= 'z') ||
      c == '_' || c == '.') UFO_TOS = ufoTrueValue; else UFO_TOS = 0;
}

// IS-ID-CHAR
// ( ch -- bool )
UFWORD(IS_ID_CHAR) {
  UFO_STACK(1);
  const uint32_t c = UFO_TOS & 0xffU;
  if ((c >= 'A' && c <= 'Z') ||
      (c >= 'a' && c <= 'z') ||
      (c >= '0' && c <= '9') ||
      c == '_' || c == '.') UFO_TOS = ufoTrueValue; else UFO_TOS = 0;
}


// ////////////////////////////////////////////////////////////////////////// //
// conditional defines
//

typedef struct UForthCondDefine_t UForthCondDefine;
struct UForthCondDefine_t {
  char *name;
  uint32_t namelen;
  uint32_t hash;
  UForthCondDefine *next;
};

static UForthCondDefine *ufoCondDefines = NULL;
static char ufoErrMsgBuf[4096];


//==========================================================================
//
//  ufoStrEquCI
//
//==========================================================================
UFO_DISABLE_INLINE int ufoStrEquCI (const void *str0, const void *str1) {
  const unsigned char *s0 = (const unsigned char *)str0;
  const unsigned char *s1 = (const unsigned char *)str1;
  while (*s0 && *s1) {
    if (toUpperU8(*s0) != toUpperU8(*s1)) return 0;
    s0 += 1; s1 += 1;
  }
  return (*s0 == 0 && *s1 == 0);
}


//==========================================================================
//
//  ufoBufEquCI
//
//==========================================================================
UFO_FORCE_INLINE int ufoBufEquCI (uint32_t addr, uint32_t count, const void *buf) {
  int res;
  if ((count & ((uint32_t)1<<31)) == 0) {
    const unsigned char *src = (const unsigned char *)buf;
    res = 1;
    while (res != 0 && count != 0) {
      res = (toUpperU8(*src) == toUpperU8(ufoImgGetU8(addr)));
      src += 1; addr += 1u; count -= 1u;
    }
  } else {
    res = 0;
  }
  return res;
}


//==========================================================================
//
//  ufoClearCondDefines
//
//==========================================================================
static void ufoClearCondDefines (void) {
  while (ufoCondDefines) {
    UForthCondDefine *df = ufoCondDefines;
    ufoCondDefines = df->next;
    if (df->name) free(df->name);
    free(df);
  }
}


//==========================================================================
//
//  ufoHasCondDefine
//
//==========================================================================
int ufoHasCondDefine (const char *name) {
  int res = 0;
  if (name != NULL && name[0] != 0) {
    const size_t nlen = strlen(name);
    if (nlen <= 255) {
      const uint32_t hash = joaatHashBufCI(name, nlen);
      UForthCondDefine *dd = ufoCondDefines;
      while (res == 0 && dd != NULL) {
        if (dd->hash == hash && dd->namelen == (uint32_t)nlen) {
          res = ufoStrEquCI(name, dd->name);
        }
        dd = dd->next;
      }
    }
  }
  return res;
}


//==========================================================================
//
//  ufoCondDefine
//
//==========================================================================
void ufoCondDefine (const char *name) {
  if (name != NULL && name[0] != 0) {
    const size_t nlen = strlen(name);
    if (nlen > 255) ufoFatal("conditional define name too long");
    const uint32_t hash = joaatHashBufCI(name, nlen);
    UForthCondDefine *dd = ufoCondDefines;
    int res = 0;
    while (res == 0 && dd != NULL) {
      if (dd->hash == hash && dd->namelen == (uint32_t)nlen) {
        res = ufoStrEquCI(name, dd->name);
      }
      dd = dd->next;
    }
    if (res == 0) {
      // new define
      dd = calloc(1, sizeof(UForthCondDefine));
      if (dd == NULL) ufoFatal("out of memory for defines");
      dd->name = strdup(name);
      if (dd->name == NULL) { free(dd); ufoFatal("out of memory for defines"); }
      dd->namelen = (uint32_t)nlen;
      dd->hash = hash;
      dd->next = ufoCondDefines;
      ufoCondDefines = dd;
    }
  }
}


//==========================================================================
//
//  ufoCondUndef
//
//==========================================================================
void ufoCondUndef (const char *name) {
  if (name != NULL && name[0] != 0) {
    const size_t nlen = strlen(name);
    if (nlen <= 255) {
      const uint32_t hash = joaatHashBufCI(name, nlen);
      UForthCondDefine *dd = ufoCondDefines;
      UForthCondDefine *prev = NULL;
      while (dd != NULL) {
        if (dd->hash == hash && dd->namelen == (uint32_t)nlen) {
          if (ufoStrEquCI(name, dd->name)) {
            if (prev != NULL) prev->next = dd->next; else ufoCondDefines = dd->next;
            free(dd->name);
            free(dd);
            dd = NULL;
          }
        }
        if (dd != NULL) { prev = dd; dd = dd->next; }
      }
    }
  }
}


// ($DEFINE)
// ( addr count -- )
UFWORD(PAR_DLR_DEFINE) {
  uint32_t count = ufoPop();
  uint32_t addr = ufoPop();
  if (count == 0) ufoFatal("empty define");
  if (count >= UFO_MAX_WORD_LENGTH) ufoFatal("define too long");
  const uint32_t hash = ufoHashBuf(addr, count, 0x20);
  UForthCondDefine *dd;
  for (dd = ufoCondDefines; dd != NULL; dd = dd->next) {
    if (dd->hash == hash && dd->namelen == count) {
      if (ufoBufEquCI(addr, count, dd->name)) return;
    }
  }
  // new define
  dd = calloc(1, sizeof(UForthCondDefine));
  if (dd == NULL) ufoFatal("out of memory for defines");
  dd->name = calloc(1, count + 1u);
  if (dd->name == NULL) { free(dd); ufoFatal("out of memory for defines"); }
  for (uint32_t f = 0; f < count; f += 1) {
    ((unsigned char *)dd->name)[f] = ufoImgGetU8(addr + f);
  }
  dd->namelen = count;
  dd->hash = hash;
  dd->next = ufoCondDefines;
  ufoCondDefines = dd;
}

// ($UNDEF)
// ( addr count -- )
UFWORD(PAR_DLR_UNDEF) {
  uint32_t count = ufoPop();
  uint32_t addr = ufoPop();
  if (count == 0) ufoFatal("empty define");
  if (count >= UFO_MAX_WORD_LENGTH) ufoFatal("define too long");
  const uint32_t hash = ufoHashBuf(addr, count, 0x20);
  UForthCondDefine *prev = NULL;
  UForthCondDefine *dd;
  for (dd = ufoCondDefines; dd != NULL; prev = dd, dd = dd->next) {
    if (dd->hash == hash && dd->namelen == count) {
      if (ufoBufEquCI(addr, count, dd->name)) {
        if (prev == NULL) ufoCondDefines = dd->next; else prev->next = dd->next;
        free(dd->name);
        free(dd);
        return;
      }
    }
  }
}

// ($DEFINED?)
// ( addr count -- bool )
UFWORD(PAR_DLR_DEFINEDQ) {
  uint32_t count = ufoPop();
  uint32_t addr = ufoPop();
  if (count == 0) ufoFatal("empty define");
  if (count >= UFO_MAX_WORD_LENGTH) ufoFatal("define too long");
  const uint32_t hash = ufoHashBuf(addr, count, 0x20);
  int found = 0;
  UForthCondDefine *dd = ufoCondDefines;
  while (!found && dd != NULL) {
    if (dd->hash == hash && dd->namelen == count) {
      found = ufoBufEquCI(addr, count, dd->name);
    }
    dd = dd->next;
  }
  ufoPushBool(found);
}


// ////////////////////////////////////////////////////////////////////////// //
// error reporting
//

// ERROR
// ( addr count -- )
UFWORD(ERROR) {
  uint32_t count = ufoPop();
  uint32_t addr = ufoPop();
  if (count & (1u<<31)) ufoFatal("invalid error message");
  if (count == 0) ufoFatal("some error");
  if (count > (uint32_t)sizeof(ufoErrMsgBuf) - 1u) count = (uint32_t)sizeof(ufoErrMsgBuf) - 1u;
  for (uint32_t f = 0; f < count; f += 1) {
    ufoErrMsgBuf[f] = (char)ufoImgGetU8(addr + f);
  }
  ufoErrMsgBuf[count] = 0;
  ufoFatal("%s", ufoErrMsgBuf);
}

// (USER-ABORT)
UFWORD(PAR_USER_ABORT) {
  ufoFatal("user abort");
}

// ?ERROR
// ( errflag addr count -- )
UFWORD(QERROR) {
  UFO_STACK(3);
  if (UFO_S(2)) {
    UFCALL(ERROR);
  } else {
    ufoSP -= 3u;
  }
}

// ?NOT-ERROR
// ( errflag addr count -- )
UFWORD(QNOTERROR) {
  UFO_STACK(3);
  if (UFO_S(2) == 0) {
    UFCALL(ERROR);
  } else {
    ufoSP -= 3u;
  }
}


// ////////////////////////////////////////////////////////////////////////// //
// includes
//

static char ufoFNameBuf[4096];


//==========================================================================
//
//  ufoScanIncludeFileName
//
//  `*psys` and `*psoft` must be initialised!
//
//==========================================================================
static void ufoScanIncludeFileName (uint32_t addr, uint32_t count, char *dest, size_t destsz,
                                    uint32_t *psys, uint32_t *psoft)
{
  uint8_t ch;
  uint32_t dpos;
  ufo_assert(dest != NULL);
  ufo_assert(destsz > 0);

  while (count != 0) {
    ch = ufoImgGetU8(addr);
    if (ch == '!') {
      //if (system) ufoFatal("invalid file name (duplicate system mark)");
      *psys = 1;
    } else if (ch == '?') {
      //if (softinclude) ufoFatal("invalid file name (duplicate soft mark)");
      *psoft = 1;
    } else {
      break;
    }
    do {
      addr += 1; count -= 1;
      ch = ufoImgGetU8(addr);
    } while (ch <= 32 && count != 0);
  }

  if (count == 0) ufoFatal("empty include file name");
  if (count >= destsz) ufoFatal("include file name too long");

  dpos = 0;
  while (count != 0) {
    dest[dpos] = (char)ufoImgGetU8(addr); dpos += 1;
    addr += 1; count -= 1;
  }
  dest[dpos] = 0;
}


// (INCLUDE-LINE-FOFS)
// ( -- fofs )
UFWORD(PAR_INCLUDE_LINE_FOFS) {
  ufoPush((uint32_t)(int32_t)ufoCurrIncludeLineFileOfs);
}

// (INCLUDE-LINE-SEEK)
// ( lidx fofs -- )
UFWORD(PAR_INCLUDE_LINE_SEEK) {
  uint32_t fofs = ufoPop();
  uint32_t lidx = ufoPop();
  if (lidx >= 0x0fffffffU) lidx = 0;
  if (ufoInFile == NULL) ufoFatal("cannot seek without opened include file");
  if (fseek(ufoInFile, (long)fofs, SEEK_SET) != 0) {
    ufoFatal("error seeking in include file");
  }
  ufoInFileLine = lidx;
}

// (INCLUDE-DEPTH)
// ( -- depth )
// return number of items in include stack
UFWORD(PAR_INCLUDE_DEPTH) {
  ufoPush(ufoFileStackPos);
}

// (INCLUDE-FILE-ID)
// ( isp -- id ) -- isp 0 is current, then 1, etc.
// each include file has unique non-zero id.
UFWORD(PAR_INCLUDE_FILE_ID) {
  const uint32_t isp = ufoPop();
  if (isp == 0) {
    ufoPush(ufoFileId);
  } else if (isp <= ufoFileStackPos) {
    UFOFileStackEntry *stk = &ufoFileStack[ufoFileStackPos - isp];
    ufoPush(stk->id);
  } else {
    ufoFatal("invalid include stack index");
  }
}

// (INCLUDE-FILE-LINE)
// ( isp -- line )
UFWORD(PAR_INCLUDE_FILE_LINE) {
  const uint32_t isp = ufoPop();
  if (isp == 0) {
    ufoPush(ufoInFileLine);
  } else if (isp <= ufoFileStackPos) {
    UFOFileStackEntry *stk = &ufoFileStack[ufoFileStackPos - isp];
    ufoPush(stk->fline);
  } else {
    ufoFatal("invalid include stack index");
  }
}

// (INCLUDE-FILE-NAME)
// ( isp -- addr count )
// current file name; at PAD
UFWORD(PAR_INCLUDE_FILE_NAME) {
  const uint32_t isp = ufoPop();
  const char *fname = NULL;
  if (isp == 0) {
    fname = ufoInFileName;
  } else if (isp <= ufoFileStackPos) {
    UFOFileStackEntry *stk = &ufoFileStack[ufoFileStackPos - isp];
    fname = stk->fname;
  } else {
    ufoFatal("invalid include stack index");
  }
  uint32_t addr = UFO_PAD_ADDR + 4u;
  uint32_t count = 0;
  if (fname != NULL) {
    while (fname[count] != 0) {
      ufoImgPutU8(addr + count, ((const unsigned char *)fname)[count]);
      count += 1;
    }
  }
  ufoImgPutU32(addr - 4u, count);
  ufoImgPutU8(addr + count, 0);
  ufoPush(addr);
  ufoPush(count);
}


// (INCLUDE-BUILD-NAME)
// ( addr count soft? system? -- addr count )
// to PAD
UFWORD(PAR_INCLUDE_BUILD_NAME) {
  uint32_t system = ufoPop();
  uint32_t softinclude = ufoPop();
  uint32_t count = ufoPop();
  uint32_t addr = ufoPop();

  if ((count & ((uint32_t)1<<31)) != 0) ufoFatal("invalid include name");

  ufoScanIncludeFileName(addr, count, ufoFNameBuf, sizeof(ufoFNameBuf),
                         &system, &softinclude);

  char *ffn = ufoCreateIncludeName(ufoFNameBuf, system, (system ? ufoLastSysIncPath : ufoLastIncPath));
  addr = UFO_PAD_ADDR + 4u;
  count = 0;
  while (ffn[count] != 0) {
    ufoImgPutU8(addr + count, ((const unsigned char *)ffn)[count]);
    count += 1u;
  }
  free(ffn);
  ufoImgPutU8(addr + count, 0);
  ufoImgPutU32(addr - 4u, count);
  ufoPush(addr);
  ufoPush(count);
}

// (INCLUDE-NO-REFILL)
// ( addr count soft? system? -- )
UFWORD(PAR_INCLUDE_NO_REFILL) {
  uint32_t system = ufoPop();
  uint32_t softinclude = ufoPop();
  uint32_t count = ufoPop();
  uint32_t addr = ufoPop();

  if (ufoMode == UFO_MODE_MACRO) ufoFatal("macros cannot include files");

  if ((count & ((uint32_t)1<<31)) != 0) ufoFatal("invalid include name");

  ufoScanIncludeFileName(addr, count, ufoFNameBuf, sizeof(ufoFNameBuf),
                         &system, &softinclude);

  char *ffn = ufoCreateIncludeName(ufoFNameBuf, system, (system ? ufoLastSysIncPath : ufoLastIncPath));
  #ifdef WIN32
  FILE *fl = fopen(ffn, "rb");
  #else
  FILE *fl = fopen(ffn, "r");
  #endif
  if (!fl) {
    if (softinclude) { free(ffn); return; }
    ufoFatal("include file '%s' not found", ffn);
  }
  #ifdef UFO_DEBUG_INCLUDE
  fprintf(stderr, "INC-PUSH: new fname: %s\n", ffn);
  #endif
  ufoPushInFile();
  ufoInFile = fl;
  ufoInFileLine = 0;
  ufoSetInFileNameReuse(ffn);
  ufoFileId = ufoLastUsedFileId;
  setLastIncPath(ufoInFileName, system);
}

// (INCLUDE-DROP)
// ( -- )
UFWORD(PAR_INCLUDE_DROP) {
  ufoPopInFile();
}

// (INCLUDE)
// ( addr count soft? system? -- )
UFWORD(PAR_INCLUDE) {
  UFCALL(PAR_INCLUDE_NO_REFILL);
  // trigger next line loading
  UFCALL(REFILL);
  if (!ufoPop()) ufoFatal("(INCLUDE) internal error");
}

// $INCLUDE "str"
UFWORD(DLR_INCLUDE_IMM) {
  int soft = 0, system = 0;
  // parse include filename
  ufoPushBool(0); UFCALL(PAR_PARSE_SKIP_COMMENTS);
  uint8_t ch = ufoTibPeekCh();
  if (ch == '"') {
    ufoTibSkipCh(); // skip quote
    ufoPush(34);
  } else if (ch == '<') {
    ufoTibSkipCh(); // skip quote
    ufoPush(62);
    system = 1;
  } else {
    ufoFatal("expected quoted string");
  }
  UFCALL(PARSE);
  if (!ufoPop()) ufoFatal("file name expected");
  ufoPushBool(0); UFCALL(PAR_PARSE_SKIP_COMMENTS);
  if (ufoTibPeekCh() != 0) {
    ufoFatal("$INCLUDE doesn't accept extra args yet");
  }
  // ( addr count soft? system? -- )
  ufoPushBool(soft); ufoPushBool(system); UFCALL(PAR_INCLUDE);
}


//==========================================================================
//
//  ufoCreateFileGuard
//
//==========================================================================
static const char *ufoCreateFileGuard (const char *fname) {
  if (fname == NULL || fname[0] == 0) return NULL;
  char *rp = ufoRealPath(fname);
  if (rp == NULL) return NULL;
  #ifdef WIN32
  for (char *s = rp; *s; s += 1) if (*s == '\\') *s = '/';
  #endif
  // hash the buffer; extract file name; create string with path len, file name, and hash
  const size_t orgplen = strlen(rp);
  const uint32_t phash = joaatHashBuf(rp, orgplen, 0);
  size_t plen = orgplen;
  while (plen != 0 && rp[plen - 1u] != '/') plen -= 1;
  snprintf(ufoRealPathHashBuf, sizeof(ufoRealPathHashBuf),
           "__INCLUDE_GUARD_%08X_%08X_%s__", phash, (uint32_t)orgplen, rp + plen);
  return ufoRealPathHashBuf;
}


// $INCLUDE-ONCE "str"
// includes file only once; unreliable on shitdoze, i believe
UFWORD(DLR_INCLUDE_ONCE_IMM) {
  uint32_t softinclude = 0, system = 0;
  // parse include filename
  ufoPushBool(0); UFCALL(PAR_PARSE_SKIP_COMMENTS);
  uint8_t ch = ufoTibPeekCh();
  if (ch == '"') {
    ufoTibSkipCh(); // skip quote
    ufoPush(34);
  } else if (ch == '<') {
    ufoTibSkipCh(); // skip quote
    ufoPush(62);
    system = 1;
  } else {
    ufoFatal("expected quoted string");
  }
  UFCALL(PARSE);
  if (!ufoPop()) ufoFatal("file name expected");
  const uint32_t count = ufoPop();
  const uint32_t addr = ufoPop();
  ufoPushBool(0); UFCALL(PAR_PARSE_SKIP_COMMENTS);
  if (ufoTibPeekCh() != 0) {
    ufoFatal("$REQUIRE doesn't accept extra args yet");
  }
  ufoScanIncludeFileName(addr, count, ufoRealPathHashBuf, sizeof(ufoRealPathHashBuf),
                         &system, &softinclude);
  char *incfname = ufoCreateIncludeName(ufoRealPathHashBuf, system, (system ? ufoLastSysIncPath : ufoLastIncPath));
  if (incfname == NULL) ufoFatal("cannot resolve include file '%s'", ufoRealPathHashBuf);
  //fprintf(stderr, "?:%d;!:%d;%s|%s\n", softinclude, system, ufoRealPathHashBuf, incfname);
  // this will overwrite `ufoRealPathHashBuf`
  const char *guard = ufoCreateFileGuard(incfname);
  free(incfname);
  if (guard == NULL) {
    if (!softinclude) ufoFatal("cannot include file '%s'", ufoRealPathHashBuf);
    return;
  }
  #if 0
  fprintf(stderr, "GUARD: <%s>\n", guard);
  #endif
  // now check for the guard
  const uint32_t glen = (uint32_t)strlen(guard);
  const uint32_t ghash = joaatHashBuf(guard, glen, 0);
  UForthCondDefine *dd;
  for (dd = ufoCondDefines; dd != NULL; dd = dd->next) {
    if (dd->hash == ghash && dd->namelen == glen && strcmp(guard, dd->name) == 0) {
      // nothing to do: already included
      return;
    }
  }
  // add guard
  dd = calloc(1, sizeof(UForthCondDefine));
  if (dd == NULL) ufoFatal("out of memory for defines");
  dd->name = calloc(1, glen + 1u);
  if (dd->name == NULL) { free(dd); ufoFatal("out of memory for defines"); }
  strcpy(dd->name, guard);
  dd->namelen = glen;
  dd->hash = ghash;
  dd->next = ufoCondDefines;
  ufoCondDefines = dd;
  // ( addr count soft? system? -- )
  ufoPush(addr); ufoPush(count); ufoPushBool(softinclude); ufoPushBool(system);
  UFCALL(PAR_INCLUDE);
}


// ////////////////////////////////////////////////////////////////////////// //
// handles
//

// HANDLE:NEW
// ( typeid -- hx )
UFWORD(PAR_NEW_HANDLE) {
  const uint32_t typeid = ufoPop();
  if (typeid == UFO_HANDLE_FREE) ufoFatal("invalid handle typeid");
  UfoHandle *hh = ufoAllocHandle(typeid);
  ufoPush(hh->ufoHandle);
}

// HANDLE:FREE
// ( hx -- )
UFWORD(PAR_FREE_HANDLE) {
  const uint32_t hx = ufoPop();
  if (hx != 0) {
    if ((hx & UFO_ADDR_HANDLE_BIT) == 0) ufoFatal("trying to free something that is not a handle");
    UfoHandle *hh = ufoGetHandle(hx);
    if (hh == NULL) ufoFatal("trying to free invalid handle");
    ufoFreeHandle(hh);
  }
}

// HANDLE:TYPEID@
// ( hx -- typeid )
UFWORD(PAR_HANDLE_GET_TYPEID) {
  const uint32_t hx = ufoPop();
  if ((hx & UFO_ADDR_HANDLE_BIT) == 0) ufoFatal("not a handle");
  UfoHandle *hh = ufoGetHandle(hx);
  if (hh == NULL) ufoFatal("invalid handle");
  ufoPush(hh->typeid);
}

// HANDLE:TYPEID!
// ( typeid hx -- )
UFWORD(PAR_HANDLE_SET_TYPEID) {
  const uint32_t hx = ufoPop();
  const uint32_t typeid = ufoPop();
  if ((hx & UFO_ADDR_HANDLE_BIT) == 0) ufoFatal("not a handle");
  if (typeid == UFO_HANDLE_FREE) ufoFatal("invalid handle typeid");
  UfoHandle *hh = ufoGetHandle(hx);
  if (hh == NULL) ufoFatal("invalid handle");
  hh->typeid = typeid;
}

// HANDLE:SIZE@
// ( hx -- size )
UFWORD(PAR_HANDLE_GET_SIZE) {
  const uint32_t hx = ufoPop();
  if (hx != 0) {
    if ((hx & UFO_ADDR_HANDLE_BIT) == 0) ufoFatal("not a handle");
    UfoHandle *hh = ufoGetHandle(hx);
    if (hh == NULL) ufoFatal("invalid handle");
    ufoPush(hh->size);
  } else {
    ufoPush(0);
  }
}

// HANDLE:SIZE!
// ( size hx -- )
UFWORD(PAR_HANDLE_SET_SIZE) {
  const uint32_t hx = ufoPop();
  const uint32_t size = ufoPop();
  if (size > 0x04000000) ufoFatal("invalid handle size");
  if ((hx & UFO_ADDR_HANDLE_BIT) == 0) ufoFatal("not a handle");
  UfoHandle *hh = ufoGetHandle(hx);
  if (hh == NULL) ufoFatal("invalid handle");
  if (hh->size != size) {
    if (size == 0) {
      free(hh->data);
      hh->data = NULL;
    } else {
      uint8_t *nx = realloc(hh->data, size * sizeof(hh->data[0]));
      if (nx == NULL) ufoFatal("out of memory for handle of size %u", size);
      hh->data = nx;
      if (size > hh->size) memset(hh->data, 0, size - hh->size);
    }
    hh->size = size;
    if (hh->used > size) hh->used = size;
  }
}

// HANDLE:USED@
// ( hx -- used )
UFWORD(PAR_HANDLE_GET_USED) {
  const uint32_t hx = ufoPop();
  if (hx != 0) {
    if ((hx & UFO_ADDR_HANDLE_BIT) == 0) ufoFatal("not a handle");
    UfoHandle *hh = ufoGetHandle(hx);
    if (hh == NULL) ufoFatal("invalid handle");
    ufoPush(hh->used);
  } else {
    ufoPush(0);
  }
}

// HANDLE:USED!
// ( size hx -- )
UFWORD(PAR_HANDLE_SET_USED) {
  const uint32_t hx = ufoPop();
  const uint32_t used = ufoPop();
  if (used > 0x04000000) ufoFatal("invalid handle used");
  if ((hx & UFO_ADDR_HANDLE_BIT) == 0) ufoFatal("not a handle");
  UfoHandle *hh = ufoGetHandle(hx);
  if (hh == NULL) ufoFatal("invalid handle");
  if (used > hh->size) ufoFatal("handle used %u out of range (%u)", used, hh->size);
  hh->used = used;
}

#define POP_PREPARE_HANDLE() \
  const uint32_t hx = ufoPop(); \
  uint32_t idx = ufoPop()


// HANDLE:C@
// ( idx hx -- value )
UFWORD(PAR_HANDLE_LOAD_BYTE) {
  POP_PREPARE_HANDLE();
  ufoPush(ufoHandleLoadByte(hx, idx));
}

// HANDLE:W@
// ( idx hx -- value )
UFWORD(PAR_HANDLE_LOAD_WORD) {
  POP_PREPARE_HANDLE();
  ufoPush(ufoHandleLoadWord(hx, idx));
}

// HANDLE:@
// ( idx hx -- value )
UFWORD(PAR_HANDLE_LOAD_CELL) {
  POP_PREPARE_HANDLE();
  ufoPush(ufoHandleLoadCell(hx, idx));
}

// HANDLE:C!
// ( value idx hx -- value )
UFWORD(PAR_HANDLE_STORE_BYTE) {
  POP_PREPARE_HANDLE();
  const uint32_t value = ufoPop();
  ufoHandleStoreByte(hx, idx, value);
}

// HANDLE:W!
// ( value idx hx -- )
UFWORD(PAR_HANDLE_STORE_WORD) {
  POP_PREPARE_HANDLE();
  const uint32_t value = ufoPop();
  ufoHandleStoreWord(hx, idx, value);
}

// HANDLE:!
// ( value idx hx -- )
UFWORD(PAR_HANDLE_STORE_CELL) {
  POP_PREPARE_HANDLE();
  const uint32_t value = ufoPop();
  ufoHandleStoreCell(hx, idx, value);
}


// HANDLE:LOAD-FILE
// ( addr count -- stx / FALSE )
UFWORD(PAR_HANDLE_LOAD_FILE) {
  uint32_t count = ufoPop();
  uint32_t addr = ufoPop();

  if ((count & ((uint32_t)1<<31)) != 0) ufoFatal("invalid file name");

  uint8_t *dest = (uint8_t *)ufoFNameBuf;
  while (count != 0 && dest < (uint8_t *)ufoFNameBuf + sizeof(ufoFNameBuf)) {
    uint8_t ch = ufoImgGetU8(addr);
    *dest = ch;
    dest += 1u; addr += 1u; count -= 1u;
  }
  if (dest == (uint8_t *)ufoFNameBuf + sizeof(ufoFNameBuf)) ufoFatal("file name too long");
  *dest = 0;

  if (*ufoFNameBuf == 0) ufoFatal("empty file name");

  char *ffn = ufoCreateIncludeName(ufoFNameBuf, 0/*system*/, ufoLastIncPath);
  #ifdef WIN32
  FILE *fl = fopen(ffn, "rb");
  #else
  FILE *fl = fopen(ffn, "r");
  #endif
  if (!fl) {
    free(ffn);
    ufoPush(0);
    return;
  }

  if (fseek(fl, 0, SEEK_END) != 0) {
    fclose(fl);
    ufoFatal("seek error in file '%s'", ffn);
  }

  long sz = ftell(fl);
  if (sz < 0 || sz >= 1024 * 1024 * 64) {
    fclose(fl);
    ufoFatal("tell error in file '%s' (or too big)", ffn);
  }

  if (fseek(fl, 0, SEEK_SET) != 0) {
    fclose(fl);
    ufoFatal("seek error in file '%s'", ffn);
  }

  UfoHandle *hh = ufoAllocHandle(0);
  if (sz != 0) {
    hh->data = malloc((uint32_t)sz);
    if (hh->data == NULL) {
      fclose(fl);
      ufoFatal("out of memory for file '%s'", ffn);
    }
    hh->size = (uint32_t)sz;
    if (fread(hh->data, (uint32_t)sz, 1, fl) != 1) {
      fclose(fl);
      ufoFatal("error reading file '%s'", ffn);
    }
    fclose(fl);
  }

  free(ffn);
  ufoPush(hh->ufoHandle);
}


// ////////////////////////////////////////////////////////////////////////// //
// utils
//

#ifdef UFO_MTASK_ALLOWED
#define UFO_MTASK_POP_STATE()  \
  UfoState *st = ufoFindState(ufoPop()); \
  if (st == NULL) ufoFatal("unknown state")
#else
#define UFO_MTASK_POP_STATE()  \
  if (ufoPop() != 0) ufoFatal("no multitasking support compiled in"); \
  UfoState *st = &ufoCurrState
#endif

// DEBUG:(DECOMPILE-CFA)
// ( cfa -- )
UFWORD(DEBUG_DECOMPILE_CFA) {
  const uint32_t cfa = ufoPop();
  ufoFlushOutput();
  ufoDecompileWord(cfa);
}

// DEBUG:(DECOMPILE-MEM)
// ( addr-start addr-end -- )
UFWORD(DEBUG_DECOMPILE_MEM) {
  const uint32_t end = ufoPop();
  const uint32_t start = ufoPop();
  ufoFlushOutput();
  ufoDecompilePart(start, end, 0);
}

// GET-MSECS
// ( -- u32 )
UFWORD(GET_MSECS) {
  ufoPush((uint32_t)ufo_get_msecs());
}

// this is called by INTERPRET when it is out of input stream
UFWORD(UFO_INTERPRET_FINISHED_ACTION) {
  longjmp(ufoStopVMJP, 666);
}

#ifdef UFO_MTASK_ALLOWED
// MTASK:NEW-STATE
// ( cfa -- stid )
UFWORD(MT_NEW_STATE) {
  UfoState *st = ufoNewState();
  const uint32_t cfa = ufoPop();
  const uint32_t cfaidx = ufoImgGetU32(cfa);
  if (cfaidx != ufoDoForthCFA) ufoFatal("state starting word should be in Forth");
  ufoInitStateUserVars(st);
  st->ip = UFO_CFA_TO_PFA(cfa);
  st->rStack[0] = 0xdeadf00d; // dummy value
  st->RP = 1;
  ufoPush(st->id);
}

// MTASK:FREE-STATE
// ( stid -- )
UFWORD(MT_FREE_STATE) {
  UfoState *st = ufoFindState(ufoPop());
  if (st == NULL) ufoFatal("cannot free unknown state");
  if (st == ufoCurrState) ufoFatal("cannot free current state");
  ufoFreeState(st);
}
#endif

// MTASK:STATE-NAME@
// ( stid -- addr count )
// to PAD
UFWORD(MT_GET_STATE_NAME) {
  UFO_MTASK_POP_STATE();
  uint32_t addr = UFO_PAD_ADDR;
  uint32_t count = 0;
  while (st->name[count] != 0) {
    ufoImgPutU8(addr + count, ((const unsigned char *)st->name)[count]);
    count += 1u;
  }
  ufoImgPutU8(addr + count, 0);
  ufoPush(addr);
  ufoPush(count);
}

// MTASK:STATE-NAME!
// ( addr count stid -- )
UFWORD(MT_SET_STATE_NAME) {
  UFO_MTASK_POP_STATE();
  uint32_t count = ufoPop();
  uint32_t addr = ufoPop();
  if ((count & ((uint32_t)1 << 31)) == 0) {
    if (count > UFO_MAX_TASK_NAME) ufoFatal("task name too long");
    for (uint32_t f = 0; f < count; f += 1u) {
      ((unsigned char *)st->name)[f] = ufoImgGetU8(addr + f);
    }
    st->name[count] = 0;
  }
}

#ifdef UFO_MTASK_ALLOWED
// MTASK:STATE-FIRST
// ( -- stid )
UFWORD(MT_STATE_FIRST) {
  uint32_t fidx = 0;
  while (fidx != (uint32_t)(UFO_MAX_STATES/32) && ufoStateUsedBitmap[fidx] == 0) fidx += 1u;
  // there should be at least one allocated state
  ufo_assert(fidx != (uint32_t)(UFO_MAX_STATES/32));
  uint32_t bmp = ufoStateUsedBitmap[fidx];
  fidx *= 32u;
  while ((bmp & 0x01) == 0) { fidx += 1u; bmp >>= 1; }
  ufoPush(fidx + 1u);
}

// MTASK:STATE-NEXT
// ( stid -- stid / 0 )
UFWORD(MT_STATE_NEXT) {
  uint32_t stid = ufoPop();
  if (stid != 0 && stid < (uint32_t)(UFO_MAX_STATES/32)) {
    // it is already incremented for us, yay!
    uint32_t fidx = stid / 32u;
    uint8_t fofs = stid & 0x1f;
    while (fidx < (uint32_t)(UFO_MAX_STATES/32)) {
      const uint32_t bmp = ufoStateUsedBitmap[fidx];
      if (bmp != 0) {
        while (fofs != 32u) {
          if ((bmp & ((uint32_t)1 << (fofs & 0x1f))) == 0) fofs += 1u;
        }
        if (fofs != 32u) {
          ufoPush(fidx * 32u + fofs + 1u);
          return; // sorry!
        }
      }
      fidx += 1u; fofs = 0;
    }
  }
  ufoPush(0);
}

// MTASK:YIELD-TO
// ( ... argc stid -- )
UFWORD(MT_YIELD_TO) {
  UfoState *st = ufoFindState(ufoPop());
  if (st == NULL) ufoFatal("cannot yield to unknown state");
  //if (st == ufoDebuggerState) ufoFatal("cannot yield to debugger"); // why not?
  const uint32_t argc = ufoPop();
  if (argc > 256) ufoFatal("too many YIELD-TO arguments");
  UfoState *curst = ufoCurrState;
  if (st != ufoCurrState) {
    for (uint32_t f = 0; f < argc; f += 1) {
      ufoCurrState = curst;
      const uint32_t n = ufoPop();
      ufoCurrState = st;
      ufoPush(n);
    }
    ufoCurrState = curst; // we need to use API call to switch states
  }
  ufoSwitchToState(st); // always use API call for this!
  ufoPush(argc);
  ufoPush(curst->id);
}

// MTASK:SET-SELF-AS-DEBUGGER
// ( -- )
UFWORD(MT_SET_SELF_AS_DEBUGGER) {
  ufoDebuggerState = ufoCurrState;
}

// DEBUG:SINGLE-STEP@
// ( -- enabled? )
UFWORD(DBG_GET_SS) {
  ufoPush(ufoSingleStepAllowed);
}
#endif

// DEBUG:(BP)
// ( -- )
// debugger task receives debugge stid on the data stack, and -1 as argc.
// i.e. debugger stask is: ( -1 old-stid )
UFWORD(MT_DEBUGGER_BP) {
  #ifdef UFO_MTASK_ALLOWED
  if (ufoDebuggerState != NULL && ufoCurrState != ufoDebuggerState && ufoIsGoodTTY()) {
    UfoState *st = ufoCurrState;
    ufoSwitchToState(ufoDebuggerState); // always use API call for this!
    ufoPush(-1);
    ufoPush(st->id);
    ufoSingleStep = 0;
  } else {
    UFCALL(UFO_BACKTRACE);
  }
  #else
  UFCALL(UFO_BACKTRACE);
  #endif
}

#ifdef UFO_MTASK_ALLOWED
// MTASK:DEBUGGER-RESUME
// ( stid -- )
UFWORD(MT_RESUME_DEBUGEE) {
  if (ufoCurrState != ufoDebuggerState) ufoFatal("cannot resume from non-debugger");
  UfoState *st = ufoFindState(ufoPop());
  if (st == NULL) ufoFatal("cannot yield to unknown state");
  if (st == ufoCurrState) ufoFatal("cannot resume into debugger itself");
  ufoSwitchToState(st); // always use API call for this!
  ufoSingleStep = 0;
}

// MTASK:DEBUGGER-SINGLE-STEP
// ( stid -- )
UFWORD(MT_SINGLE_STEP_DEBUGEE) {
  if (ufoCurrState != ufoDebuggerState) ufoFatal("cannot resume from non-debugger");
  UfoState *st = ufoFindState(ufoPop());
  if (st == NULL) ufoFatal("cannot yield to unknown state");
  if (st == ufoCurrState) ufoFatal("cannot resume into debugger itself");
  ufoSwitchToState(st); // always use API call for this!
  ufoSingleStep = 2; // it will be decremented after returning from this word
}
#endif

// MTASK:STATE-IP@
// ( stid -- ip )
UFWORD(MT_STATE_IP_GET) {
  UFO_MTASK_POP_STATE();
  ufoPush(st->IP);
}

// MTASK:STATE-IP!
// ( ip stid -- )
UFWORD(MT_STATE_IP_SET) {
  UFO_MTASK_POP_STATE();
  st->IP = ufoPop();
}

// MTASK:STATE-A>
// ( stid -- ip )
UFWORD(MT_STATE_REGA_GET) {
  UFO_MTASK_POP_STATE();
  ufoPush(st->regA);
}

// MTASK:STATE->A
// ( ip stid -- )
UFWORD(MT_STATE_REGA_SET) {
  UFO_MTASK_POP_STATE();
  st->regA = ufoPop();
}

// MTASK:STATE-USER@
// ( addr stid -- value )
UFWORD(MT_STATE_USER_GET) {
  UFO_MTASK_POP_STATE();
  const uint32_t addr = ufoPop();
  if ((addr & UFO_ADDR_TEMP_BIT) != 0 && (addr & UFO_ADDR_TEMP_MASK) + 3u < ufoSTImageTempSize(st)) {
    uint32_t v = *(const uint32_t *)((const uint8_t *)st->imageTemp + (addr & UFO_ADDR_TEMP_MASK));
    ufoPush(v);
  } else {
    ufoFatal("invalid user area address");
  }
}

// MTASK:STATE-USER!
// ( value addr stid -- )
UFWORD(MT_STATE_USER_SET) {
  UFO_MTASK_POP_STATE();
  const uint32_t addr = ufoPop();
  const uint32_t value = ufoPop();
  if ((addr & UFO_ADDR_TEMP_BIT) != 0 && (addr & UFO_ADDR_TEMP_MASK) + 3u < ufoSTImageTempSize(st)) {
    *(uint32_t *)((const uint8_t *)st->imageTemp + (addr & UFO_ADDR_TEMP_MASK)) = value;
  } else {
    ufoFatal("invalid user area address");
  }
}

// MTASK:ACTIVE-STATE
// ( -- stid )
UFWORD(MT_ACTIVE_STATE) {
  #ifdef UFO_MTASK_ALLOWED
  ufoPush(ufoCurrState->id);
  #else
  ufoPush(0);
  #endif
}

// MTASK:YIELDED-FROM
// ( -- stid / 0 )
UFWORD(MT_YIELDED_FROM) {
  #ifdef UFO_MTASK_ALLOWED
  if (ufoYieldedState != NULL) {
    ufoPush(ufoYieldedState->id);
  } else {
    ufoPush(0);
  }
  #else
  ufoPush(0);
  #endif
}

// MTASK:STATE-SP@
// ( stid -- depth )
UFWORD(MT_DSTACK_DEPTH_GET) {
  UFO_MTASK_POP_STATE();
  ufoPush(st->SP);
}

// MTASK:STATE-RP@
// ( stid -- depth )
UFWORD(MT_RSTACK_DEPTH_GET) {
  UFO_MTASK_POP_STATE();
  ufoPush(st->RP);
}

// MTASK:STATE-LP@
// ( stid -- lp )
UFWORD(MT_LP_GET) {
  UFO_MTASK_POP_STATE();
  ufoPush(st->LP);
}

// MTASK:STATE-LBP@
// ( stid -- lbp )
UFWORD(MT_LBP_GET) {
  UFO_MTASK_POP_STATE();
  ufoPush(st->LBP);
}

// MTASK:STATE-SP!
// ( depth stid -- )
UFWORD(MT_DSTACK_DEPTH_SET) {
  UFO_MTASK_POP_STATE();
  const uint32_t idx = ufoPop();
  if (idx >= UFO_DSTACK_SIZE) ufoFatal("invalid stack index %u (%u)", idx, UFO_DSTACK_SIZE);
  st->SP = idx;
}

// MTASK:STATE-RP!
// ( depth stid -- )
UFWORD(MT_RSTACK_DEPTH_SET) {
  UFO_MTASK_POP_STATE();
  const uint32_t idx = ufoPop();
  const uint32_t left = UFO_RSTACK_SIZE;
  if (idx >= left) ufoFatal("invalid rstack index %u (%u)", idx, left);
  st->RP = idx;
}

// MTASK:STATE-LP!
// ( lp stid -- )
UFWORD(MT_LP_SET) {
  UFO_MTASK_POP_STATE();
  st->LP = ufoPop();
}

// MTASK:STATE-LBP!
// ( lbp stid -- )
UFWORD(MT_LBP_SET) {
  UFO_MTASK_POP_STATE();
  st->LBP = ufoPop();
}

// MTASK:STATE-DS@
// ( idx stid -- value )
UFWORD(MT_DSTACK_LOAD) {
  UFO_MTASK_POP_STATE();
  const uint32_t idx = ufoPop();
  if (idx >= st->SP) ufoFatal("invalid stack index %u (%u)", idx, st->SP);
  ufoPush(st->dStack[st->SP - idx - 1u]);
}

// MTASK:STATE-RS@
// ( idx stid -- value )
UFWORD(MT_RSTACK_LOAD) {
  UFO_MTASK_POP_STATE();
  const uint32_t idx = ufoPop();
  if (idx >= st->RP) ufoFatal("invalid stack index %u (%u)", idx, st->RP);
  ufoPush(st->dStack[st->RP - idx - 1u]);
}

// MTASK:STATE-LS@
// ( idx stid -- value )
UFWORD(MT_LSTACK_LOAD) {
  UFO_MTASK_POP_STATE();
  const uint32_t idx = ufoPop();
  if (idx >= st->LP) ufoFatal("invalid lstack index %u (%u)", idx, st->LP);
  ufoPush(st->lStack[st->LP - idx - 1u]);
}

// MTASK:STATE-DS!
// ( value idx stid -- )
UFWORD(MT_DSTACK_STORE) {
  UFO_MTASK_POP_STATE();
  const uint32_t idx = ufoPop();
  const uint32_t value = ufoPop();
  if (idx >= st->SP) ufoFatal("invalid stack index %u (%u)", idx, st->SP);
  st->dStack[st->SP - idx - 1u] = value;
}

// MTASK:STATE-RS!
// ( value idx stid -- )
UFWORD(MT_RSTACK_STORE) {
  UFO_MTASK_POP_STATE();
  const uint32_t idx = ufoPop();
  const uint32_t value = ufoPop();
  if (idx >= st->RP) ufoFatal("invalid stack index %u (%u)", idx, st->RP);
  st->dStack[st->RP - idx - 1u] = value;
}

// MTASK:STATE-LS!
// ( value idx stid -- )
UFWORD(MT_LSTACK_STORE) {
  UFO_MTASK_POP_STATE();
  const uint32_t idx = ufoPop();
  const uint32_t value = ufoPop();
  if (idx >= st->LP) ufoFatal("invalid stack index %u (%u)", idx, st->LP);
  st->dStack[st->LP - idx - 1u] = value;
}

// MTASK:STATE-VSP@
// ( stid -- vsp )
UFWORD(MT_VSP_GET) {
  UFO_MTASK_POP_STATE();
  ufoPush(st->VSP);
}

// MTASK:STATE-VSP!
// ( vsp stid -- )
UFWORD(MT_VSP_SET) {
  UFO_MTASK_POP_STATE();
  const uint32_t vsp = ufoPop();
  if (vsp > UFO_VOCSTACK_SIZE) ufoFatal("VSP %u out of range (%u)", vsp, UFO_VOCSTACK_SIZE);
  st->VSP = vsp;
}

// MTASK:STATE-VSP-AT@
// ( idx stidf -- value )
UFWORD(MT_VSP_LOAD) {
  UFO_MTASK_POP_STATE();
  const uint32_t vsp = ufoPop();
  if (vsp >= UFO_VOCSTACK_SIZE) ufoFatal("VSP %u out of range (%u)", vsp, UFO_VOCSTACK_SIZE);
  ufoPush(st->vocStack[vsp]);
}

// MTASK:STATE-VSP-AT!
// ( value idx stid -- )
UFWORD(MT_VSP_STORE) {
  UFO_MTASK_POP_STATE();
  const uint32_t vsp = ufoPop();
  const uint32_t value = ufoPop();
  if (vsp > UFO_VOCSTACK_SIZE) ufoFatal("VSP %u out of range (%u)", vsp, UFO_VOCSTACK_SIZE);
  st->vocStack[vsp] = value;
}


#include "urforth_tty.c"


// ////////////////////////////////////////////////////////////////////////// //
// "FILES" words
//

static unsigned char ufoFileIOBuffer[4096];


//==========================================================================
//
//  ufoPopFileName
//
//==========================================================================
static char *ufoPopFileName (void) {
  uint32_t count = ufoPop();
  uint32_t addr = ufoPop();

  if ((count & 0x80000000U) != 0) ufoFatal("invalid file name");
  if (count == 0) ufoFatal("empty file name");
  if (count > (uint32_t)sizeof(ufoFNameBuf) - 1u) ufoFatal("file name too long");

  unsigned char *dest = (unsigned char *)ufoFNameBuf;
  while (count != 0) {
    *dest = ufoImgGetU8(addr);
    dest += 1u; addr += 1u; count -= 1u;
  }
  *dest = 0;

  return ufoFNameBuf;
}

// FILES:ERRNO
// ( -- errno )
UFWORD(FILES_ERRNO) {
  ufoPush((uint32_t)errno);
}

// FILES:UNLINK
// ( addr count -- success? )
UFWORD(FILES_UNLINK) {
  const char *fname = ufoPopFileName();
  ufoPushBool(unlink(fname) == 0);
}

// FILES:OPEN-R/O
// ( addr count -- handle TRUE / FALSE )
UFWORD(FILES_OPEN_RO) {
  const char *fname = ufoPopFileName();
  const int fd = open(fname, O_RDONLY);
  if (fd >= 0) {
    ufoPush((uint32_t)fd);
    ufoPushBool(1);
  } else {
    ufoPushBool(0);
  }
}

// FILES:OPEN-R/W
// ( addr count -- handle TRUE / FALSE )
UFWORD(FILES_OPEN_RW) {
  const char *fname = ufoPopFileName();
  const int fd = open(fname, O_RDWR);
  if (fd >= 0) {
    ufoPush((uint32_t)fd);
    ufoPushBool(1);
  } else {
    ufoPushBool(0);
  }
}

// FILES:CREATE
// ( addr count -- handle TRUE / FALSE )
UFWORD(FILES_CREATE) {
  const char *fname = ufoPopFileName();
  //FIXME: add variable with default flags
  const int fd = open(fname, O_RDWR|O_CREAT|O_TRUNC, 0644);
  if (fd >= 0) {
    ufoPush((uint32_t)fd);
    ufoPushBool(1);
  } else {
    ufoPushBool(0);
  }
}

// FILES:CLOSE
// ( handle -- success? )
UFWORD(FILES_CLOSE) {
  const int32_t fd = (int32_t)ufoPop();
  if (fd < 0) ufoFatal("invalid file handle in 'CLOSE'");
  ufoPushBool(close(fd) == 0);
}

// FILES:TELL
// ( handle -- ofs TRUE / FALSE )
// `handle` cannot be 0.
UFWORD(FILES_TELL) {
  const int32_t fd = (int32_t)ufoPop();
  if (fd < 0) ufoFatal("invalid file handle in 'TELL'");
  const off_t pos = lseek(fd, 0, SEEK_CUR);
  if (pos != (off_t)-1) {
    ufoPush((uint32_t)pos);
    ufoPushBool(1);
  } else {
    ufoPushBool(0);
  }
}

// FILES:SEEK-EX
// ( ofs whence handle -- TRUE / FALSE )
// `handle` cannot be 0.
UFWORD(FILES_SEEK_EX) {
  const int32_t fd = (int32_t)ufoPop();
  const uint32_t whence = ufoPop();
  const uint32_t ofs = ufoPop();
  if (fd < 0) ufoFatal("invalid file handle in 'SEEK-EX'");
  if (whence != (uint32_t)SEEK_SET &&
      whence != (uint32_t)SEEK_CUR &&
      whence != (uint32_t)SEEK_END) ufoFatal("invalid `whence` in 'SEEK-EX'");
  const off_t pos = lseek(fd, (off_t)ofs, (int)whence);
  ufoPushBool(pos != (off_t)-1);
}

// FILES:SIZE
// ( handle -- size TRUE / FALSE )
// `handle` cannot be 0.
UFWORD(FILES_SIZE) {
  const int32_t fd = (int32_t)ufoPop();
  if (fd < 0) ufoFatal("invalid file handle in 'SIZE'");
  const off_t origpos = lseek(fd, 0, SEEK_CUR);
  if (origpos == (off_t)-1) {
    ufoPushBool(0);
  } else {
    const off_t size = lseek(fd, 0, SEEK_END);
    if (size == (off_t)-1) {
      (void)lseek(origpos, 0, SEEK_SET);
      ufoPushBool(0);
    } else if (lseek(origpos, 0, SEEK_SET) == origpos) {
      ufoPush((uint32_t)size);
      ufoPushBool(1);
    } else {
      ufoPushBool(0);
    }
  }
}

// FILES:READ
// ( addr count handle -- rdsize TRUE / FALSE )
// `handle` cannot be 0.
UFWORD(FILES_READ) {
  const int32_t fd = (int32_t)ufoPop();
  if (fd < 0) ufoFatal("invalid file handle in 'READ'");
  uint32_t count = ufoPop();
  uint32_t addr = ufoPop();
  uint32_t done = 0;
  if (count != 0) {
    if ((count & 0x80000000U) != 0) ufoFatal("invalid number of bytes to read from file");
    while (count != done) {
      uint32_t rd = (uint32_t)sizeof(ufoFileIOBuffer);
      if (rd > count) rd = count;
      for (;;) {
        const ssize_t xres = read(fd, ufoFileIOBuffer, rd);
        if (xres >= 0) { rd = (uint32_t)xres; break; }
        if (errno == EINTR) continue;
        if (errno == EAGAIN || errno == EWOULDBLOCK) { rd = 0; break; }
        // error
        ufoPushBool(0);
        return;
      }
      if (rd == 0) break;
      done += rd;
      for (uint32_t f = 0; f != rd; f += 1u) {
        ufoImgPutU8(addr, ufoFileIOBuffer[f]);
        addr += 1u;
      }
    }
  }
  ufoPush(done);
  ufoPushBool(1);
}

// FILES:READ-EXACT
// ( addr count handle -- TRUE / FALSE )
// `handle` cannot be 0.
UFWORD(FILES_READ_EXACT) {
  const int32_t fd = (int32_t)ufoPop();
  if (fd < 0) ufoFatal("invalid file handle in 'READ-EXACT'");
  uint32_t count = ufoPop();
  uint32_t addr = ufoPop();
  if (count != 0) {
    if ((count & 0x80000000U) != 0) ufoFatal("invalid number of bytes to read from file");
    while (count != 0) {
      uint32_t rd = (uint32_t)sizeof(ufoFileIOBuffer);
      if (rd > count) rd = count;
      for (;;) {
        const ssize_t xres = read(fd, ufoFileIOBuffer, rd);
        if (xres >= 0) { rd = (uint32_t)xres; break; }
        if (errno == EINTR) continue;
        if (errno == EAGAIN || errno == EWOULDBLOCK) { rd = 0; break; }
        // error
        ufoPushBool(0);
        return;
      }
      if (rd == 0) { ufoPushBool(0); return; } // still error
      count -= rd;
      for (uint32_t f = 0; f != rd; f += 1u) {
        ufoImgPutU8(addr, ufoFileIOBuffer[f]);
        addr += 1u;
      }
    }
  }
  ufoPushBool(1);
}

// FILES:WRITE
// ( addr count handle -- TRUE / FALSE )
// `handle` cannot be 0.
UFWORD(FILES_WRITE) {
  const int32_t fd = (int32_t)ufoPop();
  if (fd < 0) ufoFatal("invalid file handle in 'WRITE'");
  uint32_t count = ufoPop();
  uint32_t addr = ufoPop();
  if (count != 0) {
    if ((count & 0x80000000U) != 0) ufoFatal("invalid number of bytes to write to file");
    while (count != 0) {
      uint32_t wr = (uint32_t)sizeof(ufoFileIOBuffer);
      if (wr > count) wr = count;
      for (uint32_t f = 0; f != wr; f += 1u) {
        ufoFileIOBuffer[f] = ufoImgGetU8(addr + f);
      }
      for (;;) {
        const ssize_t xres = write(fd, ufoFileIOBuffer, wr);
        if (xres >= 0) { wr = (uint32_t)xres; break; }
        if (errno == EINTR) continue;
        fprintf(stderr, "ERRNO: %d (fd=%d)\n", errno, fd);
        //if (errno == EAGAIN || errno == EWOULDBLOCK) { wr = 0; break; }
        // error
        ufoPushBool(0);
        return;
      }
      if (wr == 0) { ufoPushBool(1); return; } // still error
      count -= wr; addr += wr;
    }
  }
  ufoPushBool(1);
}


// ////////////////////////////////////////////////////////////////////////// //
// states
//

#ifdef UFO_MTASK_ALLOWED
//==========================================================================
//
//  ufoNewState
//
//  create a new state, its execution will start from the given CFA.
//  state is not automatically activated.
//
//==========================================================================
static UfoState *ufoNewState (void) {
  // find free state id
  uint32_t fidx = 0;
  uint32_t bmp = ufoStateUsedBitmap[0];
  while (fidx != (uint32_t)(UFO_MAX_STATES/32) && bmp == ~(uint32_t)0) {
    fidx += 1u;
    bmp = ufoStateUsedBitmap[fidx];
  }
  if (fidx == (uint32_t)(UFO_MAX_STATES/32)) ufoFatal("too many execution states");
  //fprintf(stderr, "NST:000: fidx=%u; bmp=0x%08x\n", fidx, bmp);
  fidx *= 32u;
  while ((bmp & 0x01) != 0) { fidx += 1u; bmp >>= 1; }
  ufo_assert(fidx < UFO_MAX_STATES);
  ufo_assert((ufoStateUsedBitmap[fidx / 32u] & ((uint32_t)1 << (fidx & 0x1f))) == 0);
  ufo_assert(ufoStateMap[fidx] == NULL);
  UfoState *st = calloc(1, sizeof(UfoState));
  if (st == NULL) ufoFatal("out of memory for states");
  st->id = fidx + 1u;
  ufoStateMap[fidx] = st;
  ufoStateUsedBitmap[fidx / 32u] |= ((uint32_t)1 << (fidx & 0x1f));
  //fprintf(stderr, "NST: fidx=%u; 0x%08x\n", fidx, ufoStateUsedBitmap[fidx / 32u]);
  return st;
}


//==========================================================================
//
//  ufoFreeState
//
//  free all memory used for the state, remove it from state list.
//  WARNING! never free current state!
//
//==========================================================================
static void ufoFreeState (UfoState *st) {
  if (st != NULL) {
    if (st == ufoCurrState) ufoFatal("cannot free active state");
    if (ufoYieldedState == st) ufoYieldedState = NULL;
    if (ufoDebuggerState == st) ufoDebuggerState = NULL;
    const uint32_t fidx = st->id - 1u;
    //fprintf(stderr, "FST: fidx=%u; 0x%08x\n", fidx, ufoStateUsedBitmap[fidx / 32u]);
    ufo_assert(fidx < UFO_MAX_STATES);
    ufo_assert((ufoStateUsedBitmap[fidx / 32u] & (1u << (fidx & 0x1f))) != 0);
    ufo_assert(ufoStateMap[fidx] == st);
    // free default TIB handle
    UfoState *oldst = ufoCurrState;
    ufoCurrState = st;
    const uint32_t tib = ufoImgGetU32(ufoAddrDefTIB);
    if ((tib & UFO_ADDR_TEMP_BIT) != 0) {
      UfoHandle *tibh = ufoGetHandle(tib);
      if (tibh != NULL) ufoFreeHandle(tibh);
    }
    ufoCurrState = oldst;
    // free temp buffer
    #ifndef UFO_HUGE_IMAGES
    if (st->imageTemp != NULL) free(st->imageTemp);
    #endif
    free(st);
    ufoStateMap[fidx] = NULL;
    ufoStateUsedBitmap[fidx / 32u] &= ~((uint32_t)1 << (fidx & 0x1f));
  }
}


//==========================================================================
//
//  ufoFindState
//
//==========================================================================
static UfoState *ufoFindState (uint32_t stid) {
  UfoState *res = NULL;
  if (stid >= 0 && stid <= UFO_MAX_STATES) {
    if (stid == 0) {
      // current
      ufo_assert(ufoCurrState != NULL);
      stid = ufoCurrState->id - 1u;
    } else {
      stid -= 1u;
    }
    res = ufoStateMap[stid];
    if (res != NULL) {
      ufo_assert((ufoStateUsedBitmap[stid / 32u] & (1u << (stid & 0x1f))) != 0);
      ufo_assert(res->id == stid + 1u);
    } else {
      ufo_assert((ufoStateUsedBitmap[stid / 32u] & (1u << (stid & 0x1f))) == 0);
    }
  }
  return res;
}


//==========================================================================
//
//  ufoSwitchToState
//
//==========================================================================
static void ufoSwitchToState (UfoState *newst) {
  ufo_assert(newst != NULL);
  if (newst != ufoCurrState) {
    ufoCurrState = newst;
  }
}
#endif


// ////////////////////////////////////////////////////////////////////////// //
// initial dictionary definitions
//

#undef UFWORD

#define UFWORD(name_)  do { \
  const uint32_t xcfa_ = ufoCFAsUsed; \
  ufo_assert(xcfa_ < UFO_MAX_NATIVE_CFAS); \
  ufoForthCFAs[xcfa_] = &ufoWord_##name_; \
  ufoCFAsUsed += 1; \
  ufoDefineNative(""#name_, xcfa_, 0); \
} while (0)

#define UFWORDX(strname_,name_)  do { \
  const uint32_t xcfa_ = ufoCFAsUsed; \
  ufo_assert(xcfa_ < UFO_MAX_NATIVE_CFAS); \
  ufoForthCFAs[xcfa_] = &ufoWord_##name_; \
  ufoCFAsUsed += 1; \
  ufoDefineNative(strname_, xcfa_, 0); \
} while (0)

#define UFWORD_IMM(name_)  do { \
  const uint32_t xcfa_ = ufoCFAsUsed; \
  ufo_assert(xcfa_ < UFO_MAX_NATIVE_CFAS); \
  ufoForthCFAs[xcfa_] = &ufoWord_##name_; \
  ufoCFAsUsed += 1; \
  ufoDefineNative(""#name_, xcfa_, UFW_FLAG_IMMEDIATE); \
} while (0)

#define UFWORDX_IMM(strname_,name_)  do { \
  const uint32_t xcfa_ = ufoCFAsUsed; \
  ufo_assert(xcfa_ < UFO_MAX_NATIVE_CFAS); \
  ufoForthCFAs[xcfa_] = &ufoWord_##name_; \
  ufoCFAsUsed += 1; \
  ufoDefineNative(strname_, xcfa_, UFW_FLAG_IMMEDIATE); \
} while (0)

#define UFC(name_)  ufoImgEmitU32_NoInline(ufoFindWordChecked(name_))


//==========================================================================
//
//  ufoFindWordChecked
//
//==========================================================================
UFO_DISABLE_INLINE uint32_t ufoFindWordChecked (const char *wname) {
  const uint32_t cfa = ufoFindWord(wname);
  if (cfa == 0) ufoFatal("word '%s' not found", wname);
  return cfa;
}


//==========================================================================
//
//  ufoGetForthVocId
//
//  get "FORTH" vocid
//
//==========================================================================
uint32_t ufoGetForthVocId (void) {
  return ufoForthVocId;
}


//==========================================================================
//
//  ufoVocSetOnlyDefs
//
//==========================================================================
void ufoVocSetOnlyDefs (uint32_t vocid) {
  ufoImgPutU32(ufoAddrCurrent, vocid);
  ufoImgPutU32(ufoAddrContext, vocid);
}


//==========================================================================
//
//  ufoCreateVoc
//
//  return voc PFA (vocid)
//
//==========================================================================
uint32_t ufoCreateVoc (const char *wname, uint32_t parentvocid, uint32_t flags) {
  // create wordlist struct
  // typeid, used by Forth code (structs and such)
  ufoImgEmitU32(0); // typeid
  // vocid points here, to "LATEST-LFA"
  const uint32_t vocid = UFO_GET_DP();
  //fprintf(stderr, "NEW VOCID (%s): 0x%08x\n", wname, vocid);
  ufoImgEmitU32(0); // latest
  const uint32_t vlink = UFO_GET_DP();
  if ((vocid & UFO_ADDR_TEMP_BIT) == 0) {
    ufoImgEmitU32(ufoImgGetU32(ufoAddrVocLink)); // voclink
    ufoImgPutU32(ufoAddrVocLink, vlink); // update voclink
  } else {
    abort();
    ufoImgEmitU32(0);
  }
  ufoImgEmitU32(parentvocid); // parent
  const uint32_t hdraddr = UFO_GET_DP();
  ufoImgEmitU32(0); // word header
  // create empty hash table
  for (int f = 0; f < UFO_HASHTABLE_SIZE; f += 1) ufoImgEmitU32(0);
  // update CONTEXT and CURRENT if this is the first wordlist ever
  if (ufoImgGetU32(ufoAddrContext) == 0) {
    ufoImgPutU32(ufoAddrContext, vocid);
  }
  if (ufoImgGetU32(ufoAddrCurrent) == 0) {
    ufoImgPutU32(ufoAddrCurrent, vocid);
  }
  // create word header
  if (wname != NULL && wname[0] != 0) {
    /*
    uint32_t flags = ufoImgGetU32(ufoAddrNewWordFlags);
    flags &=
      //UFW_FLAG_IMMEDIATE|
      //UFW_FLAG_SMUDGE|
      //UFW_FLAG_NORETURN|
      UFW_FLAG_HIDDEN|
      //UFW_FLAG_CBLOCK|
      //UFW_FLAG_VOCAB|
      //UFW_FLAG_SCOLON|
      UFW_FLAG_PROTECTED;
    flags |= UFW_FLAG_VOCAB;
    */
    flags &= 0xffffff00u;
    flags |= UFW_FLAG_VOCAB;
    ufoCreateWordHeader(wname, flags);
    const uint32_t cfa = UFO_GET_DP();
    ufoImgEmitCFA(ufoDoVocCFA); // cfa
    ufoImgEmitU32(vocid); // pfa
    // update vocab header pointer
    const uint32_t lfa = UFO_CFA_TO_LFA(cfa);
    ufoImgPutU32(hdraddr, UFO_LFA_TO_NFA(lfa));
    #ifdef UFO_DEBUG_DUMP_NEW_HEADERS
    ufoDumpWordHeader(lfa);
    #endif
  }
  return vocid;
}


//==========================================================================
//
//  ufoSetLatestArgs
//
//==========================================================================
static void ufoSetLatestArgs (uint32_t warg) {
  const uint32_t curr = ufoImgGetU32(ufoAddrCurrent);
  const uint32_t lfa = ufoImgGetU32(curr);
  const uint32_t nfa = UFO_LFA_TO_NFA(lfa);
  uint32_t flags = ufoImgGetU32(nfa);
  //fprintf(stderr, "OLD FLAGS: 0x%08x\n", flags);
  flags &= ~UFW_WARG_MASK;
  flags |= warg & UFW_WARG_MASK;
  //fprintf(stderr, "NEW FLAGS: 0x%08x\n", flags);
  ufoImgPutU32(nfa, flags);
  #ifdef UFO_DEBUG_DUMP_NEW_HEADERS
  ufoDumpWordHeader(lfa);
  #endif
}


//==========================================================================
//
//  ufoSetLatestFlags
//
//==========================================================================
static void ufoSetLatestFlags (uint32_t orflags) {
  const uint32_t curr = ufoImgGetU32(ufoAddrCurrent);
  const uint32_t lfa = ufoImgGetU32(curr);
  const uint32_t nfa = UFO_LFA_TO_NFA(lfa);
  uint32_t flags = ufoImgGetU32(nfa);
  flags |= orflags;
  ufoImgPutU32(nfa, flags);
  #ifdef UFO_DEBUG_DUMP_NEW_HEADERS
  ufoDumpWordHeader(lfa);
  #endif
}


//==========================================================================
//
//  ufoDefine
//
//==========================================================================
static void ufoDefineNative (const char *wname, uint32_t cfaidx, uint32_t orflags) {
  cfaidx |= UFO_ADDR_CFA_BIT;
  uint32_t flags = ufoImgGetU32(ufoAddrNewWordFlags);
  flags &=
      //UFW_FLAG_IMMEDIATE|
      //UFW_FLAG_SMUDGE|
      //UFW_FLAG_NORETURN|
      UFW_FLAG_HIDDEN|
      //UFW_FLAG_CBLOCK|
      //UFW_FLAG_VOCAB|
      //UFW_FLAG_SCOLON|
      UFW_FLAG_PROTECTED;
  //if (immed) flags |= UFW_FLAG_IMMEDIATE;
  flags |= orflags;
  ufoCreateWordHeader(wname, flags);
  ufoImgEmitCFA(cfaidx);
}


//==========================================================================
//
//  ufoDefineConstant
//
//==========================================================================
static void ufoDefineConstant (const char *name, uint32_t value) {
  ufoDefineNative(name, ufoDoConstCFA, 0);
  ufoImgEmitU32(value);
}


//==========================================================================
//
//  ufoDefineUserVar
//
//==========================================================================
static void ufoDefineUserVar (const char *name, uint32_t addr) {
  ufoDefineNative(name, ufoDoUserVariableCFA, 0);
  ufoImgEmitU32(addr);
}


//==========================================================================
//
//  ufoDefineVar
//
//==========================================================================
static void ufoDefineVar (const char *name, uint32_t value) {
  ufoDefineNative(name, ufoDoVariableCFA, 0);
  ufoImgEmitU32(value);
}


//==========================================================================
//
//  ufoDefineDefer
//
//==========================================================================
static void ufoDefineDefer (const char *name, uint32_t value) {
  ufoDefineNative(name, ufoDoDeferCFA, 0);
  ufoImgEmitU32(value);
}


//==========================================================================
//
//  ufoHiddenWords
//
//==========================================================================
static void ufoHiddenWords (void) {
  const uint32_t flags = ufoImgGetU32(ufoAddrNewWordFlags);
  ufoImgPutU32(ufoAddrNewWordFlags, flags | UFW_FLAG_HIDDEN);
}


//==========================================================================
//
//  ufoPublicWords
//
//==========================================================================
static void ufoPublicWords (void) {
  const uint32_t flags = ufoImgGetU32(ufoAddrNewWordFlags);
  ufoImgPutU32(ufoAddrNewWordFlags, flags & ~UFW_FLAG_HIDDEN);
}


//==========================================================================
//
//  ufoDefineForth
//
//==========================================================================
/*
static void ufoDefineForth (const char *name) {
  ufoDefineNative(name, ufoDoForthCFA, 0);
}
*/


//==========================================================================
//
//  ufoDefineForthImm
//
//==========================================================================
/*
static void ufoDefineForthImm (const char *name) {
  ufoDefineNative(name, ufoDoForthCFA, 1);
}
*/


//==========================================================================
//
//  ufoDefineForthHidden
//
//==========================================================================
/*
static void ufoDefineForthHidden (const char *name) {
  const uint32_t flags = ufoImgGetU32(ufoAddrNewWordFlags);
  ufoImgPutU32(ufoAddrNewWordFlags, flags | UFW_FLAG_HIDDEN);
  ufoDefineNative(name, ufoDoForthCFA, 0);
  ufoImgPutU32(ufoAddrNewWordFlags, flags);
}
*/


//==========================================================================
//
//  ufoDefineSColonForth
//
//  create word suitable for scattered colon extension
//
//==========================================================================
static void ufoDefineSColonForth (const char *name) {
  ufoDefineNative(name, ufoDoForthCFA, UFW_FLAG_SCOLON);
  // placeholder for scattered colon
  // it will compile two branches:
  // the first branch will jump to the first "..:" word (or over the two branches)
  // the second branch is never taken, and works as a pointer to the latest branch addr in the list
  // this way, each extension word will simply fix the last branch address, and update list tail
  // at the creation time, second branch points to the first branch
  UFC("FORTH:(BRANCH)");
  const uint32_t xjmp = UFO_GET_DP();
  ufoImgEmitU32(0);
  UFC("FORTH:(BRANCH)");
  #ifdef UFO_RELATIVE_BRANCH
  ufoImgEmitU32(xjmp - UFO_GET_DP()); // address of the fist branch dest
  ufoImgPutU32(xjmp, UFO_GET_DP() - xjmp); // jump over the jump
  #else
  ufoImgEmitU32(xjmp);
  ufoImgPutU32(xjmp, UFO_GET_DP());
  #endif
}


//==========================================================================
//
//  ufoDoneForth
//
//==========================================================================
UFO_FORCE_INLINE void ufoDoneForth (void) {
  UFC("FORTH:(EXIT)");
}


//==========================================================================
//
//  ufoCompileStrLit
//
//  compile string literal, the same as QUOTE_IMM
//
//==========================================================================
static void ufoCompileStrLitEx (const char *str, const uint32_t slen) {
  if (str == NULL) str = "";
  if (slen > 255) ufoFatal("string literal too long");
  UFC("FORTH:(LITSTR8)");
  ufoImgEmitU8((uint8_t)slen);
  for (size_t f = 0; f < slen; f += 1) {
    ufoImgEmitU8(((const unsigned char *)str)[f]);
  }
  ufoImgEmitU8(0);
  ufoImgEmitAlign();
}


//==========================================================================
//
//  ufoCompileStrLit
//
//==========================================================================
/*
static void ufoCompileStrLit (const char *str) {
  ufoCompileStrLitEx(str, (uint32_t)strlen(str));
}
*/


//==========================================================================
//
//  ufoCompileLit
//
//==========================================================================
static void ufoCompileLit (uint32_t value) {
  UFC("FORTH:(LIT)");
  ufoImgEmitU32(value);
}


//==========================================================================
//
//  ufoCompileCFALit
//
//==========================================================================
/*
static void ufoCompileCFALit (const char *wname) {
  UFC("FORTH:(LITCFA)");
  const uint32_t cfa = ufoFindWordChecked(wname);
  ufoImgEmitU32(cfa);
}
*/


//==========================================================================
//
//  ufoXStrEquCI
//
//==========================================================================
static int ufoXStrEquCI (const char *word, const char *text, uint32_t tlen) {
  while (tlen != 0 && *word) {
    if (toUpper(*word) != toUpper(*text)) return 0;
    word += 1u; text += 1u; tlen -= 1u;
  }
  return (tlen == 0 && *word == 0);
}


#define UFO_MAX_LABEL_NAME  (63)
typedef struct UfoLabel_t {
  uint32_t hash;
  uint32_t namelen;
  char name[UFO_MAX_LABEL_NAME];
  uint32_t addr; // jump chain tail, or address
  uint32_t defined;
  uint32_t word; // is this a forward word definition?
  struct UfoLabel_t *next;
} UfoLabel;

static UfoLabel *ufoLabels = NULL;


//==========================================================================
//
//  ufoFindAddLabelEx
//
//==========================================================================
static UfoLabel *ufoFindAddLabelEx (const char *name, uint32_t namelen, int allowAdd) {
  if (namelen == 0 || namelen > UFO_MAX_LABEL_NAME) ufoFatal("invalid label name");
  const uint32_t hash = joaatHashBufCI(name, namelen);
  UfoLabel *lbl = ufoLabels;
  while (lbl != NULL) {
    if (lbl->hash == hash && lbl->namelen == namelen) {
      int ok = 1;
      uint32_t sidx = 0;
      while (ok && sidx != namelen) {
        ok = (toUpper(name[sidx]) == toUpper(lbl->name[sidx]));
        sidx += 1;
      }
      if (ok) return lbl;
    }
    lbl = lbl->next;
  }
  if (allowAdd) {
    // create new label
    lbl = calloc(1, sizeof(UfoLabel));
    lbl->hash = hash;
    lbl->namelen = namelen;
    memcpy(lbl->name, name, namelen);
    lbl->name[namelen] = 0;
    lbl->next = ufoLabels;
    ufoLabels = lbl;
    return lbl;
  } else {
    return NULL;
  }
}


//==========================================================================
//
//  ufoFindAddLabel
//
//==========================================================================
static UfoLabel *ufoFindAddLabel (const char *name, uint32_t namelen) {
  return ufoFindAddLabelEx(name, namelen, 1);
}


//==========================================================================
//
//  ufoFindLabel
//
//==========================================================================
static UfoLabel *ufoFindLabel (const char *name, uint32_t namelen) {
  return ufoFindAddLabelEx(name, namelen, 0);
}


//==========================================================================
//
//  ufoTrySimpleNumber
//
//  only decimal and C-like hexes; with an optional sign
//
//==========================================================================
static int ufoTrySimpleNumber (const char *text, uint32_t tlen, uint32_t *num) {
  int neg = 0;

  if (tlen != 0 && *text == '+') { text += 1u; tlen -= 1u; }
  else if (tlen != 0 && *text == '-') { neg = 1; text += 1u; tlen -= 1u; }

  int base = 10; // default base
  if (tlen > 2 && text[0] == '0' && toUpper(text[1]) == 'X') {
    // hex
    base = 16;
    text += 2u; tlen -= 2u;
  }

  if (tlen == 0 || digitInBase(*text, base) < 0) return 0;

  int wasDigit = 0;
  uint32_t n = 0;
  int dig;
  while (tlen != 0) {
    if (*text == '_') {
      if (!wasDigit) return 0;
      wasDigit = 0;
    } else {
      dig = digitInBase(*text, base);
      if (dig < 0) return 0;
      wasDigit = 1;
      n = n * (uint32_t)base + (uint32_t)dig;
    }
    text += 1u; tlen -= 1u;
  }

  if (!wasDigit) return 0;
  if (neg) n = ~n + 1u;
  *num = n;
  return 1;
}


//==========================================================================
//
//  ufoEmitLabelChain
//
//==========================================================================
static void ufoEmitLabelChain (UfoLabel *lbl) {
  ufo_assert(lbl != NULL);
  ufo_assert(lbl->defined == 0);
  const uint32_t here = UFO_GET_DP();
  ufoImgEmitU32(lbl->addr);
  lbl->addr = here;
}


#ifdef UFO_RELATIVE_BRANCH
#define UFO_XCOMPILER_BRANCH_SET(addr_,dest_)  { \
  const uint32_t a = (addr_); \
  const uint32_t da = (dest_); \
  ufoImgPutU32(a, da - a); \
} while (0)
#else
#define UFO_XCOMPILER_BRANCH_SET(addr_,dest_)  { \
  const uint32_t a = (addr_); \
  const uint32_t da = (dest_); \
  ufoImgPutU32(a, da); \
} while (0)
#endif


//==========================================================================
//
//  ufoEmitLabelRefHere
//
//==========================================================================
UFO_FORCE_INLINE void ufoEmitLabelRefHere (UfoLabel *lbl) {
  ufo_assert(lbl != NULL);
  ufo_assert(lbl->defined != 0);
  if (lbl->word) {
    ufoImgEmitU32(lbl->addr);
  } else {
    const uint32_t here = UFO_GET_DP();
    ufoImgEmitU32(0);
    UFO_XCOMPILER_BRANCH_SET(here, lbl->addr);
  }
}


//==========================================================================
//
//  ufoFixLabelChainHere
//
//==========================================================================
static void ufoFixLabelChainHere (UfoLabel *lbl) {
  ufo_assert(lbl != NULL);
  ufo_assert(lbl->defined == 0);
  const uint32_t here = UFO_GET_DP();
  lbl->defined = 1;
  while (lbl->addr != 0) {
    const uint32_t aprev = ufoImgGetU32(lbl->addr);
    if (lbl->word) {
      ufoImgPutU32(lbl->addr, here);
    } else {
      UFO_XCOMPILER_BRANCH_SET(lbl->addr, here);
    }
    lbl->addr = aprev;
  }
  lbl->addr = here;
}


#define UFO_MII_WORD_X_COMPILE   (-5)
#define UFO_MII_WORD_COMPILE_IMM (-4)
#define UFO_MII_WORD_CFA_LIT     (-3)
#define UFO_MII_WORD_COMPILE     (-2)
#define UFO_MII_IN_WORD          (-1)
#define UFO_MII_NO_WORD          (0)
#define UFO_MII_WORD_NAME        (1)
#define UFO_MII_WORD_NAME_IMM    (2)
#define UFO_MII_WORD_NAME_HIDDEN (3)

static int ufoMinInterpState = UFO_MII_NO_WORD;


//==========================================================================
//
//  ufoFinalLabelCheck
//
//==========================================================================
static void ufoFinalLabelCheck (void) {
  int errorCount = 0;
  if (ufoMinInterpState != UFO_MII_NO_WORD) {
    ufoFatal("missing semicolon");
  }
  while (ufoLabels != NULL) {
    UfoLabel *lbl = ufoLabels; ufoLabels = lbl->next;
    if (!lbl->defined) {
      fprintf(stderr, "UFO ERROR: label '%s' is not defined!\n", lbl->name);
      errorCount += 1;
    }
    free(lbl);
  }
  if (errorCount != 0) {
    ufoFatal("%d undefined label%s", errorCount, (errorCount != 1 ? "s" : ""));
  }
}


//==========================================================================
//
//  ufoInterpretLine
//
//  this is so i could write Forth definitions more easily
//
//  labels:
//    $name -- reference
//    $name: -- definition
//
//==========================================================================
UFO_DISABLE_INLINE void ufoInterpretLine (const char *line) {
  char wname[UFO_MAX_WORD_LENGTH];
  uint32_t wlen, num, cfa;
  UfoLabel *lbl;
  while (*line) {
    if (*(const unsigned char *)line <= 32) {
      line += 1;
    } else if (ufoMinInterpState == UFO_MII_WORD_CFA_LIT ||
               ufoMinInterpState == UFO_MII_WORD_COMPILE ||
               ufoMinInterpState == UFO_MII_WORD_COMPILE_IMM ||
               ufoMinInterpState == UFO_MII_WORD_X_COMPILE)
    {
      // "[']"/"COMPILE"/"[COMPILE]" argument
      wlen = 1;
      while (((const unsigned char *)line)[wlen] > 32) wlen += 1;
      if (wlen >= UFO_MAX_WORD_LENGTH) ufoFatal("word name too long");
      memcpy(wname, line, wlen);
      wname[wlen] = 0;
      switch (ufoMinInterpState) {
        case UFO_MII_WORD_CFA_LIT: UFC("FORTH:(LITCFA)"); break;
        case UFO_MII_WORD_COMPILE: UFC("FORTH:(LITCFA)"); break;
        case UFO_MII_WORD_X_COMPILE: UFC("FORTH:(LITCFA)"); break;
        case UFO_MII_WORD_COMPILE_IMM: break;
        default: ufo_assert(0);
      }
      cfa = ufoFindWord(wname);
      if (cfa != 0) {
        ufoImgEmitU32(cfa);
      } else {
        // forward reference
        lbl = ufoFindAddLabel(line, wlen);
        if (lbl->defined || (lbl->word == 0 && lbl->addr)) {
          ufoFatal("unknown word: '%s'", wname);
        }
        lbl->word = 1;
        ufoEmitLabelChain(lbl);
      }
      switch (ufoMinInterpState) {
        case UFO_MII_WORD_CFA_LIT: break;
        case UFO_MII_WORD_COMPILE: UFC("FORTH:COMPILE,"); break;
        case UFO_MII_WORD_X_COMPILE: UFC("FORTH:,"); break;
        case UFO_MII_WORD_COMPILE_IMM: break;
        default: ufo_assert(0);
      }
      ufoMinInterpState = UFO_MII_IN_WORD;
      line += wlen;
    } else if (ufoMinInterpState > UFO_MII_NO_WORD) {
      // new word
      wlen = 1;
      while (((const unsigned char *)line)[wlen] > 32) wlen += 1;
      if (wlen >= UFO_MAX_WORD_LENGTH) ufoFatal("word name too long");
      if (wlen > 2 && line[0] == ':' && line[wlen - 1u] == ':') ufoFatal("invalid word name");
      memcpy(wname, line, wlen);
      wname[wlen] = 0;
      const uint32_t oldFlags = ufoImgGetU32(ufoAddrNewWordFlags);
      if (ufoMinInterpState == UFO_MII_WORD_NAME_HIDDEN) {
        ufoImgPutU32(ufoAddrNewWordFlags, oldFlags | UFW_FLAG_HIDDEN);
      }
      ufoDefineNative(wname, ufoDoForthCFA,
                      (ufoMinInterpState == UFO_MII_WORD_NAME_IMM ? UFW_FLAG_IMMEDIATE : 0));
      ufoImgPutU32(ufoAddrNewWordFlags, oldFlags);
      ufoMinInterpState = UFO_MII_IN_WORD;
      // check for forward references
      lbl = ufoFindLabel(line, wlen);
      if (lbl != NULL) {
        if (lbl->defined || !lbl->word) {
          ufoFatal("label/word conflict for '%.*s'", (unsigned)wlen, line);
        }
        ufoFixLabelChainHere(lbl);
      }
      line += wlen;
    } else if ((line[0] == ';' && line[1] == ';') ||
               (line[0] == '-' && line[1] == '-') ||
               (line[0] == '/' && line[1] == '/') ||
               (line[0] == '\\' && ((const unsigned char *)line)[1] <= 32))
    {
      ufoFatal("do not use single-line comments");
    } else if (line[0] == '(' && ((const unsigned char *)line)[1] <= 32) {
      while (*line && *line != ')') line += 1;
      if (*line == ')') line += 1;
    } else {
      // word
      wlen = 1;
      while (((const unsigned char *)line)[wlen] > 32) wlen += 1;
      if (wlen == 1 && (line[0] == '"' || line[0] == '`')) {
        // string literal
        const char qch = line[0];
        if (!line[1]) ufoFatal("unterminated string literal");
        // skip quote and space
        if (((const unsigned char *)line)[1] <= 32) line += 2u; else line += 1u;
        wlen = 0;
        while (line[wlen] && line[wlen] != qch) wlen += 1u;
        if (line[wlen] != qch) ufoFatal("unterminated string literal");
        ufoCompileStrLitEx(line, wlen);
        line += wlen + 1u; // skip final quote
      } else if (wlen == 1 && line[0] == ':') {
        // new word
        if (ufoMinInterpState != UFO_MII_NO_WORD) ufoFatal("unexpected colon");
        ufoMinInterpState = UFO_MII_WORD_NAME;
        line += wlen;
      } else if (wlen == 1 && line[0] == ';') {
        // end word
        if (ufoMinInterpState != UFO_MII_IN_WORD) ufoFatal("unexpected semicolon");
        ufoImgEmitU32(ufoFindWordChecked("FORTH:(EXIT)"));
        ufoMinInterpState = UFO_MII_NO_WORD;
        line += wlen;
      } else if (wlen == 2 && line[0] == '!' && line[1] == ':') {
        // new immediate word
        if (ufoMinInterpState != UFO_MII_NO_WORD) ufoFatal("unexpected colon");
        ufoMinInterpState = UFO_MII_WORD_NAME_IMM;
        line += wlen;
      } else if (wlen == 2 && line[0] == '*' && line[1] == ':') {
        // new hidden word
        if (ufoMinInterpState != UFO_MII_NO_WORD) ufoFatal("unexpected colon");
        ufoMinInterpState = UFO_MII_WORD_NAME_HIDDEN;
        line += wlen;
      } else if (wlen == 3 && memcmp(line, "[']", 3) == 0) {
        // cfa literal
        if (ufoMinInterpState != UFO_MII_IN_WORD) ufoFatal("unexpected immediate tick");
        ufoMinInterpState = UFO_MII_WORD_CFA_LIT;
        line += wlen;
      } else if (wlen == 7 && ufoXStrEquCI("COMPILE", line, wlen)) {
        // "COMPILE"
        if (ufoMinInterpState != UFO_MII_IN_WORD) ufoFatal("unexpected immediate tick");
        ufoMinInterpState = UFO_MII_WORD_COMPILE;
        line += wlen;
      } else if (wlen == 9 && ufoXStrEquCI("X-COMPILE", line, wlen)) {
        // "COMPILE"
        if (ufoMinInterpState != UFO_MII_IN_WORD) ufoFatal("unexpected immediate tick");
        ufoMinInterpState = UFO_MII_WORD_X_COMPILE;
        line += wlen;
      } else if (wlen == 9 && ufoXStrEquCI("[COMPILE]", line, wlen)) {
        // "[COMPILE]"
        if (ufoMinInterpState != UFO_MII_IN_WORD) ufoFatal("unexpected immediate tick");
        ufoMinInterpState = UFO_MII_WORD_COMPILE_IMM;
        line += wlen;
      } else {
        // look for a word
        if (wlen >= UFO_MAX_WORD_LENGTH) ufoFatal("word name too long");
        memcpy(wname, line, wlen);
        wname[wlen] = 0;
        cfa = ufoFindWord(wname);
        if (cfa != 0) {
          // compile word
          ufoImgEmitU32(cfa);
        } else if (ufoTrySimpleNumber(line, wlen, &num)) {
          // compile numeric literal
          ufoCompileLit(num);
        } else {
          // unknown word, this may be a forward reference, or a label definition
          // label defintion starts with "$"
          // (there are no words starting with "$" in the initial image)
          if (line[0] == '$') {
            if (wlen == 1) ufoFatal("dollar what?");
            if (wlen > 2 && line[wlen - 1u] == ':') {
              // label definition
              lbl = ufoFindAddLabel(line, wlen - 1u);
              if (lbl->defined) ufoFatal("double label '%s' definition", lbl->name);
              if (lbl->word)  ufoFatal("double label '%s' word conflict", lbl->name);
              ufoFixLabelChainHere(lbl);
            } else {
              // label reference
              lbl = ufoFindAddLabel(line, wlen);
              if (lbl->defined) {
                ufoEmitLabelRefHere(lbl);
              } else {
                ufoEmitLabelChain(lbl);
              }
            }
          } else {
            // forward reference
            lbl = ufoFindAddLabel(line, wlen);
            if (lbl->defined || (lbl->word == 0 && lbl->addr)) {
              ufoFatal("unknown word: '%s'", wname);
            }
            lbl->word = 1;
            ufoEmitLabelChain(lbl);
          }
        }
        line += wlen;
      }
    }
  }
}


//==========================================================================
//
//  ufoReset
//
//==========================================================================
UFO_DISABLE_INLINE void ufoReset (void) {
  #ifdef UFO_MTASK_ALLOWED
  if (ufoCurrState == NULL) ufoFatal("no active execution state");
  #endif

  ufoSP = 0; ufoRP = 0;
  ufoLP = 0; ufoLBP = 0;

  ufoInRunWord = 0;

  ufoInBacktrace = 0;

  // save TIB
  const uint32_t tib = ufoImgGetU32(ufoAddrTIBx);
  const uint32_t tibDef = ufoImgGetU32(ufoAddrDefTIB);
  #ifdef UFO_MTASK_ALLOWED
  ufoInitStateUserVars(ufoCurrState);
  #else
  ufoInitStateUserVars(&ufoCurrState);
  #endif
  // restore TIB
  ufoImgPutU32(ufoAddrTIBx, tib);
  ufoImgPutU32(ufoAddrDefTIB, tibDef);
  ufoImgPutU32(ufoAddrRedefineWarning, UFO_REDEF_WARN_NORMAL);
  ufoResetTib();

  ufoImgPutU32(ufoAddrNewWordFlags, 0);
  ufoVocSetOnlyDefs(ufoForthVocId);
}


//==========================================================================
//
//  ufoDefineEmitType
//
//==========================================================================
UFO_DISABLE_INLINE void ufoDefineEmitType (void) {
  // EMIT
  // ( ch -- )
  ufoInterpretLine(": EMIT  ( ch -- )  (NORM-EMIT-CHAR) (EMIT) ;");

  // XEMIT
  // ( ch -- )
  ufoInterpretLine(": XEMIT  ( ch -- )  (NORM-XEMIT-CHAR) (EMIT) ;");

  // CR
  // ( -- )
  ufoInterpretLine(": CR  ( -- )  NL (EMIT) ;");

  // ENDCR
  // ( -- )
  ufoInterpretLine(
    ": ENDCR  ( -- ) "
    "  LASTCR? FORTH:(TBRANCH) $endcr-exit CR "
    "$endcr-exit: "
    ";");
  //ufoDecompileWord(ufoFindWordChecked("ENDCR"));

  // SPACE
  // ( -- )
  ufoInterpretLine(": SPACE  ( -- )  BL (EMIT) ;");

  // SPACES
  // ( count -- )
  ufoInterpretLine(
    ": SPACES  ( count -- ) "
    "$spaces-again: "
    "    DUP 0> FORTH:(0BRANCH) $spaces-exit "
    "    SPACE 1- "
    "    FORTH:(BRANCH) $spaces-again "
    "$spaces-exit: "
    "  DROP "
    ";");

  // (TYPE)
  // ( addr count -- )
  ufoInterpretLine(
    ": (TYPE)  ( addr count -- ) "
    "  A>R SWAP >A "
    "$par-type-again: "
    "    DUP 0> FORTH:(0BRANCH) $par-type-exit "
    "    C@A (EMIT) +1>A "
    "    1- "
    "    FORTH:(BRANCH) $par-type-again "
    "$par-type-exit: "
    "  DROP R>A "
    ";");

  // TYPE
  // ( addr count -- )
  ufoInterpretLine(
    ": TYPE  ( addr count -- ) "
    "  A>R SWAP >A "
    "$type-again: "
    "    DUP 0> FORTH:(0BRANCH) $type-exit "
    "    C@A EMIT +1>A "
    "    1- "
    "    FORTH:(BRANCH) $type-again "
    "$type-exit: "
    "  DROP R>A "
    ";");

  // XTYPE
  // ( addr count -- )
  ufoInterpretLine(
    ": XTYPE  ( addr count -- ) "
    "  A>R SWAP >A "
    "$xtype-again: "
    "    DUP 0> FORTH:(0BRANCH) $xtype-exit "
    "    C@A XEMIT +1>A "
    "    1- "
    "    FORTH:(BRANCH) $xtype-again "
    "$xtype-exit: "
    "  DROP R>A "
    ";");

  // STRLITERAL
  // ( C:addr count -- )  ( E: -- addr count )
  ufoInterpretLine(
    ": STRLITERAL  ( C:addr count -- )  ( E: -- addr count ) "
    "  DUP 255 U> ` string literal too long` ?ERROR "
    "  COMPILER:EXEC? FORTH:(TBRANCH) $strlit-exit "
    "  HERE >R ( addr count | here ) "
    "  X-COMPILE FORTH:(LITSTR8) "
    "  A>R SWAP >A "
    "  ( compile length ) "
    "  DUP C, "
    "  ( compile chars ) "
    "$strlit-loop: "
    "    DUP FORTH:(0BRANCH) $strlit-loop-exit "
    "    C@+1>A C, 1- "
    "  FORTH:(BRANCH) $strlit-loop "
    "$strlit-loop-exit: "
    "  R>A "
    "  ( final 0: our counter is 0 here, so use it ) "
    "  C, ALIGN-HERE "
    "  R> COMPILER:(AFTER-COMPILE-WORD) "
    "$strlit-exit: "
    ";");

  // quote
  // ( -- addr count )
  ufoInterpretLine(
    "!: \"  ( -- addr count ) "
    "  34 PARSE ` string literal expected` ?NOT-ERROR "
    "  COMPILER:(UNESCAPE) STRLITERAL "
    ";");
}


//==========================================================================
//
//  ufoDefineInterpret
//
//  define "INTERPRET" in Forth
//
//==========================================================================
UFO_DISABLE_INLINE void ufoDefineInterpret (void) {
  UFWORDX("(UFO-INTERPRET-FINISHED-ACTION)", UFO_INTERPRET_FINISHED_ACTION);

  // return "stop flag"
  ufoInterpretLine(
    "*: (UFO-INTERPRET-NEXT-LINE)  ( -- continue? ) "
    "  COMPILER:COMP? FORTH:(TBRANCH) $ipn_incomp "
    "    ( interpreter allowed to cross include boundary ) "
    "    REFILL FORTH:(BRANCH) $ipn_done "
    "$ipn_incomp: "
    "    ( compiler is not allowed to cross include boundary ) "
    "    REFILL-NOCROSS ` compiler cannot cross file boundaries` ?NOT-ERROR "
    "    TRUE "
    "$ipn_done: "
    ";");

  ufoInterpNextLineCFA = ufoFindWordChecked("FORTH:(UFO-INTERPRET-NEXT-LINE)");
  ufoInterpretLine("*: (INTERPRET-NEXT-LINE)  (USER-INTERPRET-NEXT-LINE) @ EXECUTE-TAIL ;");

  // skip comments, parse name, refilling lines if necessary
  // returning FALSE as counter means: "no addr, exit INTERPRET"
  ufoInterpretLine(
    "*: (INTERPRET-PARSE-NAME)  ( -- addr count / FALSE ) "
    "$label_ipn_again: "
    "  TRUE (PARSE-SKIP-COMMENTS) PARSE-NAME "
    "  DUP FORTH:(TBRANCH) $label_ipn_exit_fwd "
    "  2DROP (INTERPRET-NEXT-LINE) "
    "  FORTH:(TBRANCH) $label_ipn_again "
    "  FALSE "
    "$label_ipn_exit_fwd: "
    ";");
  //ufoDecompileWord(ufoFindWordChecked("(INTERPRET-PARSE-NAME)"));

  ufoInterpretLine(
    ": INTERPRET "
    "$interp-again: "
    "  FORTH:(INTERPRET-PARSE-NAME) ( addr count / FALSE )"
    "  ?DUP FORTH:(0BRANCH) $interp-done "
    "  ( try defered checker ) "
    "  ( addr count FALSE -- addr count FALSE / TRUE ) "
    "  FALSE (INTERPRET-CHECK-WORD) FORTH:(TBRANCH) $interp-again "
    "  2DUP FIND-WORD  ( addr count cfa TRUE / addr count FALSE ) "
    "  FORTH:(0BRANCH) $interp-try-number "
    "  ( word found ) "
    "  NROT 2DROP  ( drop word string ) "
    "  COMPILER:EXEC? FORTH:(TBRANCH) $interp-exec "
    "  ( compiling; check immediate bit ) "
    "  DUP CFA->NFA @ COMPILER:(WFLAG-IMMEDIATE) AND FORTH:(TBRANCH) $interp-exec "
    "  ( compile it ) "
    "  FORTH:COMPILE, FORTH:(BRANCH) $interp-again "
    "  ( execute it ) "
    "$interp-exec: "
    "  EXECUTE FORTH:(BRANCH) $interp-again "
    "  ( not a word, try a number ) "
    "$interp-try-number: "
    "  2DUP TRUE BASE @ (BASED-NUMBER)  ( addr count allowsign? base -- num TRUE / FALSE ) "
    "  FORTH:(0BRANCH) $interp-number-error "
    "  ( number ) "
    "  NROT 2DROP  ( drop word string ) "
    "  LITERAL FORTH:(BRANCH) $interp-again "
    "  ( error ) "
    "$interp-number-error: "
    "  ( addr count FALSE -- addr count FALSE / TRUE ) "
    "  FALSE (INTERPRET-WORD-NOT-FOUND) FORTH:(TBRANCH) $interp-again "
    "  (INTERPRET-WORD-NOT-FOUND-POST) "
    "  ENDCR SPACE XTYPE `  -- wut?` TYPE CR "
    "  ` unknown word` ERROR "
    "$interp-done: "
    ";");
  //ufoDecompileWord(ufoFindWordChecked("INTERPRET"));
}


//==========================================================================
//
//  ufoInitBaseDict
//
//==========================================================================
UFO_DISABLE_INLINE void ufoInitBaseDict (void) {
  uint32_t imgAddr = 0;

  // reserve 32 bytes for nothing
  for (uint32_t f = 0; f < 32; f += 1) {
    ufoImgPutU8(imgAddr, 0);
    imgAddr += 1;
  }
  // align
  while ((imgAddr & 3) != 0) {
    ufoImgPutU8(imgAddr, 0);
    imgAddr += 1;
  }

  // DP
  ufoAddrDP = imgAddr;
  ufoImgPutU32(imgAddr, 0); imgAddr += 4u;

  // (LATEST-XFA)
  ufoAddrLastXFA = imgAddr;
  ufoImgPutU32(imgAddr, 0); imgAddr += 4u;

  // (VOC-LINK)
  ufoAddrVocLink = imgAddr;
  ufoImgPutU32(imgAddr, 0); imgAddr += 4u;

  // (NEW-WORD-FLAGS)
  ufoAddrNewWordFlags = imgAddr;
  ufoImgPutU32(imgAddr, UFW_FLAG_PROTECTED); imgAddr += 4u;

  // WORD-REDEFINE-WARN-MODE
  ufoAddrRedefineWarning = imgAddr;
  ufoImgPutU32(imgAddr, UFO_REDEF_WARN_NORMAL); imgAddr += 4u;

  // setup (DP) and (DP-TEMP)
  ufoImgPutU32(ufoAddrDP, imgAddr);
  ufoImgPutU32(ufoAddrDPTemp, UFO_DPTEMP_BASE_ADDR);
  ufoImgPutU32(ufoAddrHereDP, ufoAddrDP);

  #if 0
  fprintf(stderr, "INITIAL HERE: 0x%08x (0x%08x)\n", imgAddr, UFO_GET_DP());
  #endif
}


//==========================================================================
//
//  ufoInitStateUserVars
//
//==========================================================================
static void ufoInitStateUserVars (UfoState *st) {
  ufo_assert(st != NULL);
  #ifndef UFO_HUGE_IMAGES
  if (st->imageTempSize < 8192u) {
    uint32_t *itmp = realloc(st->imageTemp, 8192);
    if (itmp == NULL) ufoFatal("out of memory for state user area");
    st->imageTemp = itmp;
    memset((uint8_t *)st->imageTemp + st->imageTempSize, 0, 8192u - st->imageTempSize);
    st->imageTempSize = 8192;
  }
  #endif
  st->imageTemp[(ufoAddrBASE & UFO_ADDR_TEMP_MASK) / 4u] = 10;
  st->imageTemp[(ufoAddrSTATE & UFO_ADDR_TEMP_MASK) / 4u] = 0;
  st->imageTemp[(ufoAddrUserVarUsed & UFO_ADDR_TEMP_MASK) / 4u] = ufoAddrUserVarUsed;
  st->imageTemp[(ufoAddrDefTIB & UFO_ADDR_TEMP_MASK) / 4u] = UFO_DEF_TIB_ADDR;
  st->imageTemp[(ufoAddrTIBx & UFO_ADDR_TEMP_MASK) / 4u] = UFO_DEF_TIB_ADDR;
  st->imageTemp[(ufoAddrINx & UFO_ADDR_TEMP_MASK) / 4u] = 0;
  st->imageTemp[(ufoAddrContext & UFO_ADDR_TEMP_MASK) / 4u] = ufoForthVocId;
  st->imageTemp[(ufoAddrCurrent & UFO_ADDR_TEMP_MASK) / 4u] = ufoForthVocId;
  st->imageTemp[(ufoAddrSelf & UFO_ADDR_TEMP_MASK) / 4u] = 0;
  st->imageTemp[(ufoAddrInterNextLine & UFO_ADDR_TEMP_MASK) / 4u] = ufoInterpNextLineCFA;
  st->imageTemp[(ufoAddrEP & UFO_ADDR_TEMP_MASK) / 4u] = 0;
  st->imageTemp[(ufoAddrDPTemp & UFO_ADDR_TEMP_MASK) / 4u] = UFO_DPTEMP_BASE_ADDR;
  st->imageTemp[(ufoAddrHereDP & UFO_ADDR_TEMP_MASK) / 4u] = ufoAddrDP;

  // init other things, because this procedure is used in `ufoReset()` too
  st->SP = 0; st->RP = 0; st->regA = 0;
  st->LP = 0; st->LBP = 0;
  st->VSP = 0;
}


//==========================================================================
//
//  ufoInitBasicWords
//
//==========================================================================
UFO_DISABLE_INLINE void ufoInitBasicWords (void) {
  ufoDefineConstant("FALSE", 0);
  ufoDefineConstant("TRUE", ufoTrueValue);

  ufoDefineConstant("BL", 32);
  ufoDefineConstant("NL", 10);

  UFWORDX("NOOP", NOOP);
  UFWORDX("(NOTIMPL)", PAR_NOTIMPL); ufoSetLatestFlags(UFW_FLAG_NORETURN);

  // user variables
  ufoDefineUserVar("BASE", ufoAddrBASE);
  ufoDefineUserVar("TIB", ufoAddrTIBx);
  ufoDefineUserVar(">IN", ufoAddrINx);
  ufoDefineUserVar("(STD-TIB-ADDR)", ufoAddrDefTIB);
  ufoDefineUserVar("(USER-VAR-USED)", ufoAddrUserVarUsed);
  ufoDefineConstant("(USER-VAR-ADDR)", UFO_ADDR_TEMP_BIT);
  ufoDefineConstant("(USER-VAR-SIZE)", UFO_USER_AREA_SIZE);
  ufoDefineConstant("(USER-TIB)", UFO_DEF_TIB_ADDR);
  ufoDefineConstant("(USER-TIB-SIZE)", UFO_ADDR_HANDLE_OFS_MASK);

  ufoDefineUserVar("STATE", ufoAddrSTATE);
  ufoDefineConstant("CONTEXT", ufoAddrContext);
  ufoDefineConstant("CURRENT", ufoAddrCurrent);
  ufoDefineConstant("(SELF)", ufoAddrSelf); // used in OOP implementations
  ufoDefineConstant("(USER-INTERPRET-NEXT-LINE)", ufoAddrInterNextLine);
  ufoDefineConstant("(EXC-FRAME-PTR)", ufoAddrEP);

  ufoHiddenWords();
  ufoDefineConstant("(LATEST-XFA)", ufoAddrLastXFA);
  ufoDefineConstant("(VOC-LINK)", ufoAddrVocLink);
  ufoDefineConstant("(NEW-WORD-FLAGS)", ufoAddrNewWordFlags);
  ufoDefineConstant("(ADDR-TEMP-BIT)", UFO_ADDR_TEMP_BIT);
  ufoDefineConstant("(ADDR-CFA-BIT)", UFO_ADDR_CFA_BIT);
  ufoDefineConstant("(ADDR-HANDLE-BIT)", UFO_ADDR_HANDLE_BIT);
  ufoDefineConstant("(MAX-HANDLE-OFS)", UFO_ADDR_HANDLE_OFS_MASK);
  ufoDefineConstant("(DP-TEMP-BASE-ADDR))", UFO_DPTEMP_BASE_ADDR);

  ufoDefineConstant("(PAD-ADDR)", UFO_PAD_ADDR);
  ufoDefineConstant("(#BUF)", UFO_NBUF_ADDR + 4u); // reserve room for counter
  ufoDefineConstant("(#BUF-SIZE)", UFO_NBUF_SIZE - 8u);

  ufoDefineConstant("(DP-MAIN)", ufoAddrDP);
  ufoDefineConstant("(DP-TEMP)", ufoAddrDPTemp); // in user vars
  ufoDefineConstant("(DP-HERE)", ufoAddrHereDP); // in user vars
  ufoPublicWords();

  //UFWORDX("(UFO-BUCKET-STATS)", PAR_UFO_BUCKET_STATS);
  UFWORDX("SP0!", SP0_STORE);
  UFWORDX("RP0!", RP0_STORE);

  UFWORDX("(SELF@)", PAR_SELF_LOAD);
  UFWORDX("(SELF!)", PAR_SELF_STORE);
  UFWORDX("(SELF@+)", PAR_SELF_GET_ADD);

  UFWORDX("PAD", PAD);
  UFWORDX("HERE", HERE);
  UFWORDX("ALIGN-HERE", ALIGN_HERE);

  UFWORDX("@", PEEK);
  UFWORDX("C@", CPEEK);
  UFWORDX("W@", WPEEK);

  UFWORDX("!", POKE);
  UFWORDX("C!", CPOKE);
  UFWORDX("W!", WPOKE);

  UFWORDX("(DIRECT:@)", DIRECT_PEEK); ufoSetLatestArgs(UFW_WARG_PFA);
  UFWORDX("(DIRECT:!)", DIRECT_POKE); ufoSetLatestArgs(UFW_WARG_PFA);
  UFWORDX("(DIRECT:0:!)", DIRECT_POKE0); ufoSetLatestArgs(UFW_WARG_PFA);
  UFWORDX("(DIRECT:1:!)", DIRECT_POKE1); ufoSetLatestArgs(UFW_WARG_PFA);
  UFWORDX("(DIRECT:-1:!)", DIRECT_POKEM1); ufoSetLatestArgs(UFW_WARG_PFA);
  UFWORDX("(DIRECT:+!)", DIRECT_ADD_POKE); ufoSetLatestArgs(UFW_WARG_PFA);
  UFWORDX("(DIRECT:-!)", DIRECT_SUB_POKE); ufoSetLatestArgs(UFW_WARG_PFA);
  UFWORDX("(DIRECT:+:@)", DIRECT_OFS_PEEK); ufoSetLatestArgs(UFW_WARG_LIT);
  UFWORDX("(DIRECT:+:!)", DIRECT_OFS_POKE); ufoSetLatestArgs(UFW_WARG_LIT);
  UFWORDX("(DIRECT:1+!)", DIRECT_POKE_INC1); ufoSetLatestArgs(UFW_WARG_LIT);
  UFWORDX("(DIRECT:2+!)", DIRECT_POKE_INC2); ufoSetLatestArgs(UFW_WARG_LIT);
  UFWORDX("(DIRECT:4+!)", DIRECT_POKE_INC4); ufoSetLatestArgs(UFW_WARG_LIT);
  UFWORDX("(DIRECT:8+!)", DIRECT_POKE_INC8); ufoSetLatestArgs(UFW_WARG_LIT);
  UFWORDX("(DIRECT:1-!)", DIRECT_POKE_DEC1); ufoSetLatestArgs(UFW_WARG_LIT);
  UFWORDX("(DIRECT:2-!)", DIRECT_POKE_DEC2); ufoSetLatestArgs(UFW_WARG_LIT);
  UFWORDX("(DIRECT:4-!)", DIRECT_POKE_DEC4); ufoSetLatestArgs(UFW_WARG_LIT);
  UFWORDX("(DIRECT:8-!)", DIRECT_POKE_DEC8); ufoSetLatestArgs(UFW_WARG_LIT);

  UFWORDX("(LIT-AND)", LIT_AND); ufoSetLatestArgs(UFW_WARG_LIT);
  UFWORDX("(LIT-~AND)", LIT_NAND); ufoSetLatestArgs(UFW_WARG_LIT);
  UFWORDX("(LIT-OR)", LIT_OR); ufoSetLatestArgs(UFW_WARG_LIT);
  UFWORDX("(LIT-XOR)", LIT_XOR); ufoSetLatestArgs(UFW_WARG_LIT);

  UFWORDX("0!", POKE_0);
  UFWORDX("1!", POKE_1);
  UFWORDX("1+!", POKE_INC_1);
  UFWORDX("1-!", POKE_DEC_1);
  UFWORDX("+!", POKE_INC);
  UFWORDX("-!", POKE_DEC);

  UFWORDX("SWAP!", SWAP_POKE);
  UFWORDX("SWAP-C!", SWAP_CPOKE);
  UFWORDX("SWAP-W!", SWAP_WPOKE);
  UFWORDX("OR!", OR_POKE);
  UFWORDX("OR-C!", OR_CPOKE);
  UFWORDX("OR-W!", OR_WPOKE);
  UFWORDX("XOR!", XOR_POKE);
  UFWORDX("XOR-C!", XOR_CPOKE);
  UFWORDX("XOR-W!", XOR_WPOKE);
  UFWORDX("~AND!", NAND_POKE);
  UFWORDX("~AND-C!", NAND_CPOKE);
  UFWORDX("~AND-W!", NAND_WPOKE);

  UFWORDX("COUNT", COUNT);
  UFWORDX("BCOUNT", BCOUNT);
  UFWORDX("ID-COUNT", ID_COUNT);

  UFWORDX(",", COMMA);
  UFWORDX("C,", CCOMMA);
  UFWORDX("W,", WCOMMA);

  UFWORDX("A>", REGA_LOAD);
  UFWORDX(">A", REGA_STORE);
  UFWORDX("A-SWAP", REGA_SWAP);
  UFWORDX("+1>A", REGA_INC);
  UFWORDX("+2>A", REGA_INC_WORD);
  UFWORDX("+4>A", REGA_INC_CELL);
  UFWORDX("-1>A", REGA_DEC);
  UFWORDX("-2>A", REGA_DEC_WORD);
  UFWORDX("-4>A", REGA_DEC_CELL);
  UFWORDX("A>R", REGA_TO_R);
  UFWORDX("R>A", R_TO_REGA);

  UFWORDX("@A", PEEK_REGA);
  UFWORDX("C@A", CPEEK_REGA);
  UFWORDX("W@A", WPEEK_REGA);

  UFWORDX("!A", POKE_REGA);
  UFWORDX("C!A", CPOKE_REGA);
  UFWORDX("W!A", WPOKE_REGA);

  UFWORDX("@A+", PEEK_REGA_IDX);
  UFWORDX("C@A+", CPEEK_REGA_IDX);
  UFWORDX("W@A+", WPEEK_REGA_IDX);

  UFWORDX("!A+", POKE_REGA_IDX);
  UFWORDX("C!A+", CPOKE_REGA_IDX);
  UFWORDX("W!A+", WPOKE_REGA_IDX);

  UFWORDX("C!+1>A", CPOKE_REGA_INC1);
  UFWORDX("W!+2>A", WPOKE_REGA_INC2);
  UFWORDX("!+4>A", POKE_REGA_INC4);
  UFWORDX("C@+1>A", CPEEK_REGA_INC1);
  UFWORDX("W@+2>A", WPEEK_REGA_INC2);
  UFWORDX("@+4>A", PEEK_REGA_INC4);

  ufoHiddenWords();
  UFWORDX("(LIT)", PAR_LIT); ufoSetLatestArgs(UFW_WARG_LIT);
  UFWORDX("(LITCFA)", PAR_LITCFA); ufoSetLatestArgs(UFW_WARG_CFA);
  UFWORDX("(LITPFA)", PAR_LITPFA); ufoSetLatestArgs(UFW_WARG_PFA);
  UFWORDX("(LITVOCID)", PAR_LITVOCID); ufoSetLatestArgs(UFW_WARG_VOCID);
  UFWORDX("(LITSTR8)", PAR_LITSTR8); ufoSetLatestArgs(UFW_WARG_C1STRZ);
  UFWORDX("(EXIT)", PAR_EXIT); ufoSetLatestFlags(UFW_FLAG_NORETURN);

  ufoLitStr8CFA = ufoFindWordChecked("FORTH:(LITSTR8)");

  UFWORDX("(L-ENTER)", PAR_LENTER); ufoSetLatestArgs(UFW_WARG_LIT);
  UFWORDX("(L-LEAVE)", PAR_LLEAVE);
  UFWORDX("(LOCAL@)", PAR_LOCAL_LOAD);
  UFWORDX("(LOCAL!)", PAR_LOCAL_STORE);

  UFWORDX("(BRANCH)", PAR_BRANCH); ufoSetLatestArgs(UFW_WARG_BRANCH);
  UFWORDX("(TBRANCH)", PAR_TBRANCH); ufoSetLatestArgs(UFW_WARG_BRANCH); ufoSetLatestFlags(UFW_WARG_CONDBRANCH);
  UFWORDX("(0BRANCH)", PAR_0BRANCH); ufoSetLatestArgs(UFW_WARG_BRANCH); ufoSetLatestFlags(UFW_WARG_CONDBRANCH);
  UFWORDX("(+0BRANCH)", PAR_P0BRANCH); ufoSetLatestArgs(UFW_WARG_BRANCH); ufoSetLatestFlags(UFW_WARG_CONDBRANCH);
  UFWORDX("(+BRANCH)", PAR_PBRANCH); ufoSetLatestArgs(UFW_WARG_BRANCH); ufoSetLatestFlags(UFW_WARG_CONDBRANCH);
  UFWORDX("(-0BRANCH)", PAR_M0BRANCH); ufoSetLatestArgs(UFW_WARG_BRANCH); ufoSetLatestFlags(UFW_WARG_CONDBRANCH);
  UFWORDX("(-BRANCH)", PAR_MBRANCH); ufoSetLatestArgs(UFW_WARG_BRANCH); ufoSetLatestFlags(UFW_WARG_CONDBRANCH);
  UFWORDX("(DATASKIP)", PAR_DATASKIP); ufoSetLatestArgs(UFW_WARG_DATASKIP);
  UFWORDX("(OR-BRANCH)", PAR_OR_BRANCH); ufoSetLatestArgs(UFW_WARG_BRANCH); ufoSetLatestFlags(UFW_WARG_CONDBRANCH);
  UFWORDX("(AND-BRANCH)", PAR_AND_BRANCH); ufoSetLatestArgs(UFW_WARG_BRANCH); ufoSetLatestFlags(UFW_WARG_CONDBRANCH);
  UFWORDX("(?DUP-0BRANCH)", PAR_QDUP_0BRANCH); ufoSetLatestArgs(UFW_WARG_BRANCH); ufoSetLatestFlags(UFW_WARG_CONDBRANCH);
  UFWORDX("(CASE-BRANCH)", PAR_CASE_BRANCH); ufoSetLatestArgs(UFW_WARG_BRANCH); ufoSetLatestFlags(UFW_WARG_CONDBRANCH);
  ufoPublicWords();
}


//==========================================================================
//
//  ufoInitMoreWords
//
//==========================================================================
UFO_DISABLE_INLINE void ufoInitMoreWords (void) {
  UFWORDX("CFA->DOES-CFA", CFA2DOESCFA);
  UFWORDX("CFA->PFA", CFA2PFA);
  UFWORDX("CFA->NFA", CFA2NFA);
  UFWORDX("CFA->LFA", CFA2LFA);
  UFWORDX("CFA->WEND", CFA2WEND);

  UFWORDX("PFA->CFA", PFA2CFA);
  UFWORDX("PFA->NFA", PFA2NFA);

  UFWORDX("NFA->CFA", NFA2CFA);
  UFWORDX("NFA->PFA", NFA2PFA);
  UFWORDX("NFA->LFA", NFA2LFA);

  UFWORDX("LFA->CFA", LFA2CFA);
  UFWORDX("LFA->PFA", LFA2PFA);
  UFWORDX("LFA->BFA", LFA2BFA);
  UFWORDX("LFA->XFA", LFA2XFA);
  UFWORDX("LFA->YFA", LFA2YFA);
  UFWORDX("LFA->NFA", LFA2NFA);

  UFWORDX("(BASED-NUMBER)", PAR_BASED_NUMBER);
  UFWORDX("FIND-WORD", FIND_WORD);
  UFWORDX("(FIND-WORD-IN-VOC)", PAR_FIND_WORD_IN_VOC);
  UFWORDX("(FIND-WORD-IN-VOC-AND-PARENTS)", PAR_FIND_WORD_IN_VOC_AND_PARENTS);
  UFWORDX("FIND-WORD-IN-VOC", FIND_WORD_IN_VOC);
  UFWORDX("FIND-WORD-IN-VOC-AND-PARENTS", FIND_WORD_IN_VOC_AND_PARENTS);

  UFWORD(EXECUTE);
  UFWORDX("EXECUTE-TAIL", EXECUTE_TAIL); ufoSetLatestFlags(UFW_FLAG_NORETURN);
  UFWORDX("@EXECUTE", LOAD_EXECUTE);
  UFWORDX("@EXECUTE-TAIL", LOAD_EXECUTE_TAIL); ufoSetLatestFlags(UFW_FLAG_NORETURN);
  UFWORDX("(FORTH-CALL)", FORTH_CALL);
  UFWORDX("(FORTH-TAIL-CALL)", FORTH_TAIL_CALL); ufoSetLatestFlags(UFW_FLAG_NORETURN);

  UFWORD(DUP);
  UFWORD(NIP);
  UFWORD(TUCK);
  UFWORDX("?DUP", QDUP);
  UFWORDX("2DUP", DDUP);
  UFWORD(DROP);
  UFWORDX("2DROP", DDROP);
  UFWORD(SWAP);
  UFWORDX("2SWAP", DSWAP);
  UFWORD(OVER);
  UFWORDX("2OVER", DOVER);
  UFWORD(ROT);
  UFWORD(NROT);
  UFWORDX("PICK", PICK);
  UFWORDX("ROLL", ROLL);

  UFWORD(RDUP);
  UFWORD(RDROP);
  UFWORDX(">R", DTOR);
  UFWORDX("R>", RTOD);
  UFWORDX("R@", RPEEK);
  UFWORDX("2>R", 2DTOR);
  UFWORDX("2R>", 2RTOD);
  UFWORDX("2R@", 2RPEEK);
  UFWORDX("2RDROP", 2RDROP);
  UFWORDX("RPICK", RPICK);
  UFWORDX("RROLL", RROLL);
  UFWORDX("RSWAP", RSWAP);
  UFWORDX("ROVER", ROVER);
  UFWORDX("RROT", RROT);
  UFWORDX("RNROT", RNROT);

  UFWORDX("FLUSH-EMIT", FLUSH_EMIT);
  UFWORDX("(EMIT)", PAR_EMIT);
  UFWORDX("(NORM-EMIT-CHAR)", PAR_NORM_EMIT_CHAR);
  UFWORDX("(NORM-XEMIT-CHAR)", PAR_NORM_XEMIT_CHAR);
  UFWORDX("LASTCR?", LASTCRQ);
  UFWORDX("LASTCR!", LASTCRSET);

  // simple math
  UFWORDX("+", PLUS);
  UFWORDX("-", MINUS);
  UFWORDX("*", MUL);
  UFWORDX("U*", UMUL);
  UFWORDX("/", DIV);
  UFWORDX("U/", UDIV);
  UFWORDX("MOD", MOD);
  UFWORDX("UMOD", UMOD);
  UFWORDX("/MOD", DIVMOD);
  UFWORDX("U/MOD", UDIVMOD);
  UFWORDX("*/", MULDIV);
  UFWORDX("U*/", UMULDIV);
  UFWORDX("*/MOD", MULDIVMOD);
  UFWORDX("U*/MOD", UMULDIVMOD);
  UFWORDX("M*", MMUL);
  UFWORDX("UM*", UMMUL);
  UFWORDX("M/MOD", MDIVMOD);
  UFWORDX("UM/MOD", UMDIVMOD);
  UFWORDX("UDS*", UDSMUL);

  UFWORDX("SM/REM", SMREM);
  UFWORDX("FM/MOD", FMMOD);

  UFWORDX("D-", DMINUS);
  UFWORDX("D+", DPLUS);
  UFWORDX("D=", DEQU);
  UFWORDX("D<", DLESS);
  UFWORDX("D<=", DLESSEQU);
  UFWORDX("DU<", DULESS);
  UFWORDX("DU<=", DULESSEQU);

  UFWORD(ASH);
  UFWORD(LSH);
  UFWORD(ARSHIFT);
  UFWORD(LSHIFT);
  UFWORD(RSHIFT);

  UFWORDX("~AND", BN_AND);
  UFWORDX("ABS", ABS);
  UFWORDX("NEGATE", NEGATE);
  UFWORDX("SIGN?", SIGNQ);
  UFWORDX("LO-WORD", LO_WORD);
  UFWORDX("HI-WORD", HI_WORD);
  UFWORDX("LO-BYTE", LO_BYTE);
  UFWORDX("HI-BYTE", HI_BYTE);
  UFWORDX("MIN", MIN);
  UFWORDX("MAX", MAX);
  UFWORDX("UMIN", UMIN);
  UFWORDX("UMAX", UMAX);
  UFWORDX("WITHIN", WITHIN);
  UFWORDX("UWITHIN", UWITHIN);
  UFWORDX("BOUNDS?", BOUNDSQ);
  UFWORDX("BSWAP16", BSWAP16);
  UFWORDX("BSWAP32", BSWAP32);

  // for optimiser
  UFWORDX("(SWAP:1+:SWAP)", PAR_SWAP_INC_SWAP);

  // logic
  UFWORDX("<", LESS);
  UFWORDX(">", GREAT);
  UFWORDX("<=", LESSEQU);
  UFWORDX(">=", GREATEQU);
  UFWORDX("U<", ULESS);
  UFWORDX("U>", UGREAT);
  UFWORDX("U<=", ULESSEQU);
  UFWORDX("U>=", UGREATEQU);
  UFWORDX("=", EQU);
  UFWORDX("<>", NOTEQU);

  UFWORDX("0=", ZERO_EQU);
  UFWORDX("0<>", ZERO_NOTEQU);
  UFWORDX("0<", 0LESS);
  UFWORDX("0>", 0GREAT);
  UFWORDX("0<=", 0LESSEQU);
  UFWORDX("0>=", 0GREATEQU);

  UFWORDX("NOT", ZERO_EQU);
  UFWORDX("NOTNOT", ZERO_NOTEQU);

  UFWORD(BITNOT);
  UFWORD(AND);
  UFWORD(OR);
  UFWORD(XOR);
  UFWORDX("LOGAND", LOGAND);
  UFWORDX("LOGOR", LOGOR);

  UFWORDX("2*", 2MUL);
  UFWORDX("4*", 4MUL);
  UFWORDX("8*", 8MUL);
  UFWORDX("2/", 2DIV);
  UFWORDX("4/", 4DIV);
  UFWORDX("8/", 8DIV);
  UFWORDX("2U/", 2UDIV);
  UFWORDX("4U/", 4UDIV);
  UFWORDX("8U/", 8UDIV);

  UFWORDX("1+", 1ADD);
  UFWORDX("1-", 1SUB);
  UFWORDX("2+", 2ADD);
  UFWORDX("2-", 2SUB);
  UFWORDX("4+", 4ADD);
  UFWORDX("4-", 4SUB);
  UFWORDX("8+", 8ADD);
  UFWORDX("8-", 8SUB);

  ufoDefineConstant("CELL", 4);

  UFWORDX("CELL+", 4ADD);
  UFWORDX("CELL-", 4SUB);

  UFWORDX("CELLS", 4MUL);
  UFWORDX("/CELLS", 4DIV);
  UFWORDX("+CELLS", ADD_CELLS);
  UFWORDX("-CELLS", SUB_CELLS);

  UFWORDX("MEMCMP", MEMCMP);
  UFWORDX("MEMCMP-CI", MEMCMP_CI);

  UFWORDX("CMOVE-CELLS", CMOVE_CELLS_FWD);
  UFWORDX("CMOVE>-CELLS", CMOVE_CELLS_BWD);
  UFWORDX("CMOVE", CMOVE_FWD);
  UFWORDX("CMOVE>", CMOVE_BWD);
  UFWORDX("MOVE", MOVE);

  UFWORDX("FILL-CELLS", FILL_CELLS);
  UFWORDX("FILL", FILL);

  // TIB and parser
  UFWORDX("(TIB-IN)", TIB_IN);
  UFWORDX("TIB-PEEKCH", TIB_PEEKCH);
  UFWORDX("TIB-PEEKCH-OFS", TIB_PEEKCH_OFS);
  UFWORDX("TIB-GETCH", TIB_GETCH);
  UFWORDX("TIB-SKIPCH", TIB_SKIPCH);

  UFWORDX("REFILL", REFILL);
  UFWORDX("REFILL-NOCROSS", REFILL_NOCROSS);

  ufoHiddenWords();
  UFWORDX("(PARSE)", PAR_PARSE);
  UFWORDX("(PARSE-SKIP-COMMENTS)", PAR_PARSE_SKIP_COMMENTS);
  ufoPublicWords();
  UFWORDX("PARSE-SKIP-BLANKS", PARSE_SKIP_BLANKS);
  UFWORDX("PARSE-NAME", PARSE_NAME);
  UFWORDX("PARSE-SKIP-LINE", PARSE_SKIP_LINE);
  UFWORDX("PARSE", PARSE);

  ufoHiddenWords();
  UFWORDX("(VSP@)", PAR_GET_VSP);
  UFWORDX("(VSP!)", PAR_SET_VSP);
  UFWORDX("(VSP-AT@)", PAR_VSP_LOAD);
  UFWORDX("(VSP-AT!)", PAR_VSP_STORE);
  ufoDefineConstant("(VSP-SIZE)", UFO_VOCSTACK_SIZE);

  ufoDefineConstant("(SP-SIZE)", UFO_DSTACK_SIZE);
  ufoDefineConstant("(RP-SIZE)", UFO_RSTACK_SIZE);
  ufoDefineConstant("(LP-SIZE)", UFO_LSTACK_SIZE);
  ufoPublicWords();

  UFWORDX("ERROR", ERROR); ufoSetLatestFlags(UFW_FLAG_NORETURN);
  UFWORDX("FATAL-ERROR", ERROR); ufoSetLatestFlags(UFW_FLAG_NORETURN);
  UFWORDX("(USER-ABORT)", PAR_USER_ABORT); ufoSetLatestFlags(UFW_FLAG_NORETURN);

  ufoUserAbortCFA = ufoImgGetU32(ufoAddrCurrent);
  ufoUserAbortCFA = ufoImgGetU32(ufoUserAbortCFA + UFW_VOCAB_OFS_LATEST);
  ufoUserAbortCFA = UFO_LFA_TO_CFA(ufoUserAbortCFA);

  UFWORDX("?ERROR", QERROR); ufoSetLatestFlags(UFW_FLAG_MAYRETURN);
  UFWORDX("?NOT-ERROR", QNOTERROR); ufoSetLatestFlags(UFW_FLAG_MAYRETURN);

  // ABORT
  // ( -- )
  ufoInterpretLine(": ABORT  ` \"ABORT\" called` ERROR ;"); ufoSetLatestFlags(UFW_FLAG_NORETURN);

  UFWORDX("GET-MSECS", GET_MSECS);
}


//==========================================================================
//
//  ufoInitBasicCompilerWords
//
//==========================================================================
UFO_DISABLE_INLINE void ufoInitBasicCompilerWords (void) {
  // create "COMPILER" vocabulary
  ufoCompilerVocId = ufoCreateVoc("COMPILER", 0, UFW_FLAG_PROTECTED);
  ufoVocSetOnlyDefs(ufoCompilerVocId);

  ufoDefineConstant("(CFAIDX-DO-FORTH)", ufoDoForthCFA);
  ufoDefineConstant("(CFAIDX-DO-VAR)", ufoDoVariableCFA);
  ufoDefineConstant("(CFAIDX-DO-VALUE)", ufoDoValueCFA);
  ufoDefineConstant("(CFAIDX-DO-CONST)", ufoDoConstCFA);
  ufoDefineConstant("(CFAIDX-DO-DEFER)", ufoDoDeferCFA);
  ufoDefineConstant("(CFAIDX-DO-DOES)", ufoDoDoesCFA);
  ufoDefineConstant("(CFAIDX-DO-REDIRECT)", ufoDoRedirectCFA);
  ufoDefineConstant("(CFAIDX-DO-VOC)", ufoDoVocCFA);
  ufoDefineConstant("(CFAIDX-DO-CREATE)", ufoDoCreateCFA);
  ufoDefineConstant("(CFAIDX-DO-USER-VAR)", ufoDoUserVariableCFA);

  ufoDefineConstant("(WFLAG-IMMEDIATE)", UFW_FLAG_IMMEDIATE);
  ufoDefineConstant("(WFLAG-SMUDGE)", UFW_FLAG_SMUDGE);
  ufoDefineConstant("(WFLAG-NORETURN)", UFW_FLAG_NORETURN);
  ufoDefineConstant("(WFLAG-HIDDEN)", UFW_FLAG_HIDDEN);
  ufoDefineConstant("(WFLAG-CBLOCK)", UFW_FLAG_CBLOCK);
  ufoDefineConstant("(WFLAG-VOCAB)", UFW_FLAG_VOCAB);
  ufoDefineConstant("(WFLAG-SCOLON)", UFW_FLAG_SCOLON);
  ufoDefineConstant("(WFLAG-PROTECTED)", UFW_FLAG_PROTECTED);
  ufoDefineConstant("(WFLAG-CONDBRANCH)", UFW_WARG_CONDBRANCH);
  ufoDefineConstant("(WFLAG-MAYRETURN)", UFW_FLAG_MAYRETURN);

  ufoDefineConstant("(WARG-MASK)", UFW_WARG_MASK);
  ufoDefineConstant("(WARG-NONE)", UFW_WARG_NONE);
  ufoDefineConstant("(WARG-BRANCH)", UFW_WARG_BRANCH);
  ufoDefineConstant("(WARG-LIT)", UFW_WARG_LIT);
  ufoDefineConstant("(WARG-C4STRZ)", UFW_WARG_C4STRZ);
  ufoDefineConstant("(WARG-CFA)", UFW_WARG_CFA);
  ufoDefineConstant("(WARG-CBLOCK)", UFW_WARG_CBLOCK);
  ufoDefineConstant("(WARG-VOCID)", UFW_WARG_VOCID);
  ufoDefineConstant("(WARG-C1STRZ)", UFW_WARG_C1STRZ);
  ufoDefineConstant("(WARG-DATASKIP)", UFW_WARG_DATASKIP);
  ufoDefineConstant("(WARG-PFA)", UFW_WARG_PFA);

  ufoDefineConstant("(VOCOFS-LATEST)", UFW_VOCAB_OFS_LATEST);
  ufoDefineConstant("(VOCOFS-VOCLINK)", UFW_VOCAB_OFS_VOCLINK);
  ufoDefineConstant("(VOCOFS-PARENT)", UFW_VOCAB_OFS_PARENT);
  ufoDefineConstant("(VOCOFS-HEADER)", UFW_VOCAB_OFS_HEADER);
  ufoDefineConstant("(VOCOFS-HTABLE)", UFW_VOCAB_OFS_HTABLE);
  ufoDefineConstant("(VOC-HTABLE-SIZE)", UFO_HASHTABLE_SIZE);
  ufoDefineConstant("(VOC-HTABLE-NOFLAG)", UFO_NO_HTABLE_FLAG);

  ufoDefineConstant("(REDEFINE-WARN-DON'T-CARE)", UFO_REDEF_WARN_DONT_CARE);
  ufoDefineConstant("(REDEFINE-WARN-NONE)", UFO_REDEF_WARN_NONE);
  ufoDefineConstant("(REDEFINE-WARN-NORMAL)", UFO_REDEF_WARN_NORMAL);
  ufoDefineConstant("(REDEFINE-WARN-PARENTS)", UFO_REDEF_WARN_PARENTS);

  ufoDefineConstant("WORD-REDEFINE-WARN-MODE", ufoAddrRedefineWarning);

  UFWORDX("(BRANCH-ADDR!)", PAR_BRANCH_ADDR_POKE);
  UFWORDX("(BRANCH-ADDR@)", PAR_BRANCH_ADDR_PEEK);

  UFWORDX("CFA,", CFA_COMMA);
  UFWORDX("(UNESCAPE)", PAR_UNESCAPE);

  const uint32_t dropCFA = ufoFindWordChecked("FORTH:DROP");
  const uint32_t noopCFA = ufoFindWordChecked("FORTH:NOOP");

  ufoDefineDefer("(AFTER-COMPILE-WORD)", dropCFA); // ( start-addr -- )
  ufoDefineDefer("(AFTER-COMPILE-LIT)", dropCFA); // ( start-addr -- )
  ufoDefineDefer("(JUMP-HERE-MARKED)", noopCFA); // ( -- )
  ufoDefineDefer("(RESET-SINOPT)", noopCFA); // ( -- )

  ufoDefineVar("(COMPILE-START-HERE)", 0);

  ufoInterpretLine(
    ": ?EXEC  ( -- ) "
    "  FORTH:STATE FORTH:@ ` expecting interpretation mode` FORTH:?ERROR "
    ";");

  ufoInterpretLine(
    ": ?COMP  ( -- ) "
    "  FORTH:STATE FORTH:@ ` expecting compilation mode` FORTH:?NOT-ERROR "
    ";");

  ufoInterpretLine(
    ": EXEC?  ( -- bool ) "
    "  FORTH:STATE FORTH:@ FORTH:0= "
    ";");

  ufoInterpretLine(
    ": COMP?  ( -- bool ) "
    "  FORTH:STATE FORTH:@ FORTH:0<> "
    ";");

  ufoInterpretLine(
    ": EXEC!  ( -- ) "
    "  FORTH:STATE FORTH:0! "
    ";");

  ufoInterpretLine(
    ": COMP!  ( -- ) "
    "  FORTH:STATE FORTH:1! "
    ";");

  UFWORDX("(CREATE-WORD-HEADER)", PAR_CREATE_WORD_HEADER);
  UFWORDX("(CREATE-NAMELESS-WORD-HEADER)", PAR_CREATE_NAMELESS_WORD_HEADER);

  ufoVocSetOnlyDefs(ufoForthVocId);

  // [
  ufoInterpretLine("!: [  COMPILER:?COMP COMPILER:EXEC! ;");
  // ]
  ufoInterpretLine(": ]  COMPILER:?EXEC COMPILER:COMP! ;");

  ufoInterpretLine(
    ": COMPILE,  ( n -- ) "
    "  COMPILER:(COMPILE-START-HERE) @ ` compile sequence is not finished... somewhere` ?ERROR "
    "  HERE >R , R> COMPILER:(AFTER-COMPILE-WORD) "
    ";");

  ufoInterpretLine(
    ": <COMPILE,  ( n -- ) "
    "  COMPILER:(COMPILE-START-HERE) @ ` compile sequence is not finished... somewhere` ?ERROR "
    "  HERE COMPILER:(COMPILE-START-HERE) ! , "
    ";");

  ufoInterpretLine(
    ": COMPILE>  ( n -- ) "
    "  COMPILER:(COMPILE-START-HERE) @ DUP ` compile sequence is not started... somewhere` ?NOT-ERROR "
    "  COMPILER:(COMPILE-START-HERE) 0! COMPILER:(AFTER-COMPILE-WORD) "
    ";");

  // LITERAL
  // ( C:n -- )  ( E:n -- n )
  ufoInterpretLine(
    ": LITERAL  ( C:n -- )  ( E:n -- n ) "
    "  COMPILER:COMP? FORTH:(0BRANCH) $literal_exit "
    "  HERE >R X-COMPILE FORTH:(LIT) , "
    "  R> COMPILER:(AFTER-COMPILE-LIT) "
    "$literal_exit: "
    ";");
  //ufoDecompileWord(ufoFindWordChecked("LITERAL"));

  // CFALITERAL
  // ( C:cfa -- )  ( E:cfa -- cfa )
  ufoInterpretLine(
    ": CFALITERAL  ( C:cfa -- )  ( E:cfa -- cfa ) "
    "  COMPILER:COMP? FORTH:(0BRANCH) $cfa_literal_exit "
    "  HERE >R X-COMPILE FORTH:(LITCFA) , "
    "  R> COMPILER:(AFTER-COMPILE-LIT) "
    "$cfa_literal_exit: "
    ";");

  // PFALITERAL
  // ( C:pfa -- )  ( E:pfa -- pfa )
  ufoInterpretLine(
    ": PFALITERAL  ( C:pfa -- )  ( E:pfa -- pfa ) "
    "  COMPILER:COMP? FORTH:(0BRANCH) $pfa_literal_exit "
    "  HERE >R X-COMPILE FORTH:(LITPFA) , "
    "  R> COMPILER:(AFTER-COMPILE-LIT) "
    "$pfa_literal_exit: "
    ";");

  ufoInterpretLine("!: IMM-LITERAL  LITERAL ;");
  ufoInterpretLine("!: IMM-CFALITERAL  CFALITERAL ;");
  ufoInterpretLine("!: IMM-PFALITERAL  PFALITERAL ;");
}


//==========================================================================
//
//  ufoInitHandleWords
//
//==========================================================================
UFO_DISABLE_INLINE void ufoInitHandleWords (void) {
  // create "HANDLE" vocabulary
  const uint32_t handleVocId = ufoCreateVoc("HANDLE", 0, UFW_FLAG_PROTECTED);
  ufoVocSetOnlyDefs(handleVocId);
  UFWORDX("NEW", PAR_NEW_HANDLE);
  UFWORDX("FREE", PAR_FREE_HANDLE);
  UFWORDX("TYPEID@", PAR_HANDLE_GET_TYPEID);
  UFWORDX("TYPEID!", PAR_HANDLE_SET_TYPEID);
  UFWORDX("SIZE@", PAR_HANDLE_GET_SIZE);
  UFWORDX("SIZE!", PAR_HANDLE_SET_SIZE);
  UFWORDX("USED@", PAR_HANDLE_GET_USED);
  UFWORDX("USED!", PAR_HANDLE_SET_USED);
  UFWORDX("C@", PAR_HANDLE_LOAD_BYTE);
  UFWORDX("W@", PAR_HANDLE_LOAD_WORD);
  UFWORDX("@", PAR_HANDLE_LOAD_CELL);
  UFWORDX("C!", PAR_HANDLE_STORE_BYTE);
  UFWORDX("W!", PAR_HANDLE_STORE_WORD);
  UFWORDX("!", PAR_HANDLE_STORE_CELL);
  UFWORDX("LOAD-FILE", PAR_HANDLE_LOAD_FILE);
  ufoVocSetOnlyDefs(ufoForthVocId);
}


//==========================================================================
//
//  ufoInitHigherWords
//
//==========================================================================
UFO_DISABLE_INLINE void ufoInitHigherWords (void) {
  UFWORDX("(INCLUDE)", PAR_INCLUDE);
  UFWORDX("(INCLUDE-DROP)", PAR_INCLUDE_DROP);
  UFWORDX("(INCLUDE-BUILD-NAME)", PAR_INCLUDE_BUILD_NAME);
  UFWORDX("(INCLUDE-NO-REFILL)", PAR_INCLUDE_NO_REFILL);
  UFWORDX("(INCLUDE-LINE-SEEK)", PAR_INCLUDE_LINE_SEEK);

  UFWORDX("(INCLUDE-LINE-FOFS)", PAR_INCLUDE_LINE_FOFS);
  UFWORDX("(INCLUDE-DEPTH)", PAR_INCLUDE_DEPTH);
  UFWORDX("(INCLUDE-FILE-ID)", PAR_INCLUDE_FILE_ID);
  UFWORDX("(INCLUDE-FILE-LINE)", PAR_INCLUDE_FILE_LINE);
  UFWORDX("(INCLUDE-FILE-NAME)", PAR_INCLUDE_FILE_NAME);

  UFWORDX("($DEFINED?)", PAR_DLR_DEFINEDQ);
  UFWORDX("($DEFINE)", PAR_DLR_DEFINE);
  UFWORDX("($UNDEF)", PAR_DLR_UNDEF);

  UFWORDX_IMM("$INCLUDE", DLR_INCLUDE_IMM);
  UFWORDX_IMM("$INCLUDE-ONCE", DLR_INCLUDE_ONCE_IMM);
}


//==========================================================================
//
//  ufoInitStringWords
//
//==========================================================================
UFO_DISABLE_INLINE void ufoInitStringWords (void) {
  // create "STRING" vocabulary
  const uint32_t stringVocId = ufoCreateVoc("STRING", 0, UFW_FLAG_PROTECTED);
  ufoVocSetOnlyDefs(stringVocId);
  UFWORDX("=", STREQU);
  UFWORDX("=CI", STREQUCI);
  UFWORDX("SEARCH", SEARCH);
  UFWORDX("HASH", STRHASH);
  UFWORDX("HASH-CI", STRHASHCI);
  UFWORDX("CHAR-UPPER", CHAR_UPPER);
  UFWORDX("CHAR-LOWER", CHAR_LOWER);
  UFWORDX("UPPER", STRUPPER);
  UFWORDX("LOWER", STRLOWER);
  UFWORDX("(CHAR-DIGIT)", CHAR_DIGIT);
  UFWORDX("DIGIT", DIGIT);
  UFWORDX("DIGIT?", DIGITQ);

  UFWORDX("IS-DIGIT", IS_DIGIT);
  UFWORDX("IS-BIN-DIGIT", IS_BIN_DIGIT);
  UFWORDX("IS-OCT-DIGIT", IS_OCT_DIGIT);
  UFWORDX("IS-HEX-DIGIT", IS_HEX_DIGIT);
  UFWORDX("IS-ALPHA", IS_ALPHA);
  UFWORDX("IS-UNDER-DOT", IS_UNDER_DOT);
  UFWORDX("IS-ALNUM", IS_ALNUM);
  UFWORDX("IS-ID-START", IS_ID_START);
  UFWORDX("IS-ID-CHAR", IS_ID_CHAR);

  ufoVocSetOnlyDefs(ufoForthVocId);
}


//==========================================================================
//
//  ufoInitDebugWords
//
//==========================================================================
UFO_DISABLE_INLINE void ufoInitDebugWords (void) {
  // create "DEBUG" vocabulary
  const uint32_t debugVocId = ufoCreateVoc("DEBUG", 0, UFW_FLAG_PROTECTED);
  ufoVocSetOnlyDefs(debugVocId);
  UFWORDX("(DECOMPILE-CFA)", DEBUG_DECOMPILE_CFA);
  UFWORDX("(DECOMPILE-MEM)", DEBUG_DECOMPILE_MEM);
  UFWORDX("BACKTRACE", UFO_BACKTRACE);
  UFWORDX("DUMP-STACK", DUMP_STACK);
  #ifdef UFO_MTASK_ALLOWED
  UFWORDX("BACKTRACE-TASK", UFO_BACKTRACE_TASK);
  UFWORDX("DUMP-STACK-TASK", DUMP_STACK_TASK);
  UFWORDX("DUMP-RSTACK-TASK", DUMP_RSTACK_TASK);
  #endif
  UFWORDX("(BP)", MT_DEBUGGER_BP);
  UFWORDX("IP->NFA", IP2NFA);
  UFWORDX("IP->FILE/LINE", IP2FILELINE);
  UFWORDX("IP->FILE-HASH/LINE", IP2FILEHASHLINE);
  #ifdef UFO_MTASK_ALLOWED
  UFWORDX("SINGLE-STEP@", DBG_GET_SS);
  #endif
  ufoVocSetOnlyDefs(ufoForthVocId);
}


//==========================================================================
//
//  ufoInitMTWords
//
//==========================================================================
UFO_DISABLE_INLINE void ufoInitMTWords (void) {
  // create "MTASK" vocabulary
  const uint32_t mtVocId = ufoCreateVoc("MTASK", 0, UFW_FLAG_PROTECTED);
  ufoVocSetOnlyDefs(mtVocId);
  #ifdef UFO_MTASK_ALLOWED
  UFWORDX("NEW-STATE", MT_NEW_STATE);
  UFWORDX("FREE-STATE", MT_FREE_STATE);
  #endif
  UFWORDX("STATE-NAME@", MT_GET_STATE_NAME);
  UFWORDX("STATE-NAME!", MT_SET_STATE_NAME);
  #ifdef UFO_MTASK_ALLOWED
  UFWORDX("STATE-FIRST", MT_STATE_FIRST);
  UFWORDX("STATE-NEXT", MT_STATE_NEXT);
  UFWORDX("YIELD-TO", MT_YIELD_TO);
  UFWORDX("SET-SELF-AS-DEBUGGER", MT_SET_SELF_AS_DEBUGGER);
  UFWORDX("DEBUGGER-RESUME", MT_RESUME_DEBUGEE);
  UFWORDX("DEBUGGER-SINGLE-STEP", MT_SINGLE_STEP_DEBUGEE);
  #endif
  UFWORDX("ACTIVE-STATE", MT_ACTIVE_STATE);
  UFWORDX("STATE-IP@", MT_STATE_IP_GET);
  UFWORDX("STATE-IP!", MT_STATE_IP_SET);
  UFWORDX("STATE-A>", MT_STATE_REGA_GET);
  UFWORDX("STATE->A", MT_STATE_REGA_SET);
  UFWORDX("STATE-USER@", MT_STATE_USER_GET);
  UFWORDX("STATE-USER!", MT_STATE_USER_SET);
  UFWORDX("YIELDED-FROM", MT_YIELDED_FROM);
  UFWORDX("STATE-SP@", MT_DSTACK_DEPTH_GET);
  UFWORDX("STATE-RP@", MT_RSTACK_DEPTH_GET);
  UFWORDX("STATE-LP@", MT_LP_GET);
  UFWORDX("STATE-LBP@", MT_LBP_GET);
  UFWORDX("STATE-SP!", MT_DSTACK_DEPTH_SET);
  UFWORDX("STATE-RP!", MT_RSTACK_DEPTH_SET);
  UFWORDX("STATE-LP!", MT_LP_SET);
  UFWORDX("STATE-LBP!", MT_LBP_SET);
  UFWORDX("STATE-DS@", MT_DSTACK_LOAD);
  UFWORDX("STATE-RS@", MT_RSTACK_LOAD);
  UFWORDX("STATE-LS@", MT_LSTACK_LOAD);
  UFWORDX("STATE-DS!", MT_DSTACK_STORE);
  UFWORDX("STATE-RS!", MT_RSTACK_STORE);
  UFWORDX("STATE-LS!", MT_LSTACK_STORE);
  UFWORDX("STATE-VSP@", MT_VSP_GET);
  UFWORDX("STATE-VSP!", MT_VSP_SET);
  UFWORDX("STATE-VSP-AT@", MT_VSP_LOAD);
  UFWORDX("STATE-VSP-AT!", MT_VSP_STORE);
  ufoVocSetOnlyDefs(ufoForthVocId);
}


//==========================================================================
//
//  ufoInitTTYWords
//
//==========================================================================
UFO_DISABLE_INLINE void ufoInitTTYWords (void) {
  // create "TTY" vocabulary
  const uint32_t ttyVocId = ufoCreateVoc("TTY", 0, UFW_FLAG_PROTECTED);
  ufoVocSetOnlyDefs(ttyVocId);
  UFWORDX("TTY?", TTY_TTYQ);
  UFWORDX("RAW?", TTY_RAWQ);
  UFWORDX("SIZE", TTY_SIZE);
  UFWORDX("SET-RAW", TTY_SET_RAW);
  UFWORDX("SET-COOKED", TTY_SET_COOKED);
  UFWORDX("RAW-EMIT", TTY_RAW_EMIT);
  UFWORDX("RAW-TYPE", TTY_RAW_TYPE);
  UFWORDX("RAW-FLUSH", TTY_RAW_FLUSH);
  UFWORDX("RAW-READCH", TTY_RAW_READCH);
  UFWORDX("RAW-READY?", TTY_RAW_READYQ);
  ufoVocSetOnlyDefs(ufoForthVocId);
}


//==========================================================================
//
//  ufoInitFilesWords
//
//==========================================================================
UFO_DISABLE_INLINE void ufoInitFilesWords (void) {
  // create "FILES" vocabulary
  const uint32_t filesVocId = ufoCreateVoc("FILES", 0, UFW_FLAG_PROTECTED);
  ufoVocSetOnlyDefs(filesVocId);
  ufoDefineConstant("SEEK-SET", SEEK_SET);
  ufoDefineConstant("SEEK-CUR", SEEK_CUR);
  ufoDefineConstant("SEEK-END", SEEK_END);

  UFWORDX("OPEN-R/O", FILES_OPEN_RO);
  UFWORDX("OPEN-R/W", FILES_OPEN_RW);
  UFWORDX("CREATE", FILES_CREATE);
  UFWORDX("CLOSE", FILES_CLOSE);
  UFWORDX("TELL", FILES_TELL);
  UFWORDX("SEEK-EX", FILES_SEEK_EX);
  UFWORDX("SIZE", FILES_SIZE);
  UFWORDX("READ", FILES_READ);
  UFWORDX("READ-EXACT", FILES_READ_EXACT);
  UFWORDX("WRITE", FILES_WRITE);

  UFWORDX("UNLINK", FILES_UNLINK);

  UFWORDX("ERRNO", FILES_ERRNO);

  ufoInterpretLine(
    ": SEEK  ( ofs handle -- success? ) "
    "  SEEK-SET FORTH:SWAP SEEK-EX "
    ";");

  ufoVocSetOnlyDefs(ufoForthVocId);
}


//==========================================================================
//
//  ufoInitVeryVeryHighWords
//
//==========================================================================
UFO_DISABLE_INLINE void ufoInitVeryVeryHighWords (void) {
  // interpret defer
  //ufoDefineDefer("INTERPRET", idumbCFA);

  ufoDefineEmitType();

  // ( addr count FALSE -- addr count FALSE / TRUE )
  ufoDefineSColonForth("(INTERPRET-CHECK-WORD)");
  ufoDoneForth();
  // ( addr count FALSE -- addr count FALSE / TRUE )
  ufoDefineSColonForth("(INTERPRET-WORD-NOT-FOUND)");
  ufoDoneForth();
  // ( addr count -- addr count )
  ufoDefineSColonForth("(INTERPRET-WORD-NOT-FOUND-POST)");
  ufoDoneForth();
  // ( -- ) -- called in "EXIT", before compiling "FORTH:(EXIT)"
  ufoDefineSColonForth("(EXIT-EXTENDER)");
  ufoDoneForth();

  // EXIT  ( -- )
  ufoInterpretLine(
    "!: EXIT  ( -- ) "
    " COMPILER:?COMP (EXIT-EXTENDER) "
    //" HERE >R "
    " COMPILE FORTH:(EXIT) "
    //" R> COMPILER:(AFTER-COMPILE-WORD) "
    ";");

  ufoDefineInterpret();

  //ufoDumpVocab(ufoCompilerVocId);

  ufoInterpretLine(
    ": RUN-INTERPRET-LOOP "
    "$run-interp-loop-again: "
    "  RP0! INTERPRET (UFO-INTERPRET-FINISHED-ACTION) "
    "  FORTH:(BRANCH) $run-interp-loop-again "
    ";"); ufoSetLatestFlags(UFW_FLAG_NORETURN);
}

#define UFO_ADD_DO_CFA(cfx_)  do { \
  ufoDo##cfx_##CFA = ufoCFAsUsed | UFO_ADDR_CFA_BIT; \
  ufoForthCFAs[ufoCFAsUsed] = &ufoDo##cfx_; \
  ufoCFAsUsed += 1; \
} while (0)


//==========================================================================
//
//  ufoBadCFA
//
//==========================================================================
static void ufoBadCFA (uint32_t pfa) {
  ufoFatal("tried to execute an invalid CFA: IP=%u", ufoIP - 4u);
}


//==========================================================================
//
//  ufoInitCommon
//
//==========================================================================
UFO_DISABLE_INLINE void ufoInitCommon (void) {
  ufoVSP = 0;
  ufoForthVocId = 0; ufoCompilerVocId = 0;

  //ufoForthCFAs = calloc(UFO_MAX_NATIVE_CFAS, sizeof(ufoForthCFAs[0]));
  for (uint32_t f = 0; f < UFO_MAX_NATIVE_CFAS; f += 1) ufoForthCFAs[f] = &ufoBadCFA;

  // allocate default TIB handle
  //UfoHandle *tibh = ufoAllocHandle(0x69a029a6); // arbitrary number
  //ufoDefTIB = tibh->ufoHandle;

  /*ufoForthCFAs[0] = NULL;*/ ufoCFAsUsed = 1u;
  UFO_ADD_DO_CFA(Forth);
  UFO_ADD_DO_CFA(Variable);
  UFO_ADD_DO_CFA(Value);
  UFO_ADD_DO_CFA(Const);
  UFO_ADD_DO_CFA(Defer);
  UFO_ADD_DO_CFA(Does);
  UFO_ADD_DO_CFA(Redirect);
  UFO_ADD_DO_CFA(Voc);
  UFO_ADD_DO_CFA(Create);
  UFO_ADD_DO_CFA(UserVariable);

  //fprintf(stderr, "DO-FORTH-CFA: 0x%08x\n", ufoDoForthCFA);

  ufoInitBaseDict();

  // create "FORTH" vocabulary (it should be the first one)
  ufoForthVocId = ufoCreateVoc("FORTH", 0, UFW_FLAG_PROTECTED);
  ufoVocSetOnlyDefs(ufoForthVocId);

  // base low-level interpreter words
  ufoInitBasicWords();

  // more FORTH words
  ufoInitMoreWords();

  // some COMPILER words
  ufoInitBasicCompilerWords();

  // STRING vocabulary
  ufoInitStringWords();

  // DEBUG vocabulary
  ufoInitDebugWords();

  // MTASK vocabulary
  ufoInitMTWords();

  // HANDLE vocabulary
  ufoInitHandleWords();

  // TTY vocabulary
  ufoInitTTYWords();

  // FILES vocabulary
  ufoInitFilesWords();

  // some higher-level FORTH words (includes, etc.)
  ufoInitHigherWords();

  // very-very high-level FORTH words
  ufoInitVeryVeryHighWords();

  ufoFinalLabelCheck();

  #if 0
  ufoDecompileWord(ufoFindWordChecked("RUN-INTERPRET-LOOP"));
  #endif

  ufoReset();
}

#undef UFC


// ////////////////////////////////////////////////////////////////////////// //
// virtual machine executor
//


//==========================================================================
//
//  ufoRunVMxxx
//
//  address interpreter
//
//==========================================================================
static void ufoRunVMxxx (uint32_t cfa) {
  UFO_EXEC_CFA(cfa);
  // VM execution loop
  for (;;) {
    cfa = ufoImgGetU32(ufoIP); ufoIP += 4u;
    UFO_EXEC_CFA(cfa);
  }
}


//==========================================================================
//
//  ufoRunVMCFA
//
//==========================================================================
static void ufoRunVMCFA (uint32_t cfa) {
  if (ufoInRunWord) ufoFatal("cannot run VM recursively");
  ufoInRunWord = 1;
  if (setjmp(ufoStopVMJP) == 0) {
    ufoRunVMxxx(cfa);
  }
  ufoInRunWord = 0;
}


// ////////////////////////////////////////////////////////////////////////// //
// high-level API
//

//==========================================================================
//
//  ufoRegisterWord
//
//  register new word
//
//==========================================================================
uint32_t ufoRegisterWord (const char *wname, ufoNativeCFA cfa, uint32_t flags) {
  ufo_assert(cfa != NULL);
  ufo_assert(wname != NULL && wname[0] != 0);
  uint32_t cfaidx = ufoCFAsUsed;
  if (cfaidx >= UFO_MAX_NATIVE_CFAS) ufoFatal("too many native words");
  ufoForthCFAs[cfaidx] = cfa;
  ufoCFAsUsed += 1;
  //ufoDefineNative(wname, xcfa, 0);
  cfaidx |= UFO_ADDR_CFA_BIT;
  flags &= 0xffffff00u;
  ufoCreateWordHeader(wname, flags);
  const uint32_t res = UFO_GET_DP();
  ufoImgEmitCFA(cfaidx);
  return res;
}


//==========================================================================
//
//  ufoRegisterDataWord
//
//==========================================================================
static uint32_t ufoRegisterDataWord (const char *wname, uint32_t cfaidx, uint32_t value,
                                     uint32_t flags)
{
  ufo_assert(wname != NULL && wname[0] != 0);
  flags &= 0xffffff00u;
  ufoCreateWordHeader(wname, flags);
  ufoImgEmitCFA(cfaidx);
  const uint32_t res = UFO_GET_DP();
  ufoImgEmitU32(value);
  return res;
}


//==========================================================================
//
//  ufoRegisterConstant
//
//==========================================================================
void ufoRegisterConstant (const char *wname, uint32_t value, uint32_t flags) {
  (void)ufoRegisterDataWord(wname, ufoDoConstCFA, value, flags);
}


//==========================================================================
//
//  ufoRegisterVariable
//
//==========================================================================
uint32_t ufoRegisterVariable (const char *wname, uint32_t value, uint32_t flags) {
  return ufoRegisterDataWord(wname, ufoDoVariableCFA, value, flags);
}


//==========================================================================
//
//  ufoRegisterValue
//
//==========================================================================
uint32_t ufoRegisterValue (const char *wname, uint32_t value, uint32_t flags) {
  return ufoRegisterDataWord(wname, ufoDoValueCFA, value, flags);
}


//==========================================================================
//
//  ufoRegisterDefer
//
//==========================================================================
uint32_t ufoRegisterDefer (const char *wname, uint32_t value, uint32_t flags) {
  return ufoRegisterDataWord(wname, ufoDoDeferCFA, value, flags);
}


//==========================================================================
//
//  ufoFindWordInVocabulary
//
//  check if we have the corresponding word.
//  return CFA suitable for executing, or 0.
//
//==========================================================================
uint32_t ufoFindWordInVocabulary (const char *wname, uint32_t vocid) {
  if (wname == NULL || wname[0] == 0) return 0;
  size_t wlen = strlen(wname);
  if (wlen >= UFO_MAX_WORD_LENGTH) return 0;
  return ufoFindWordInVocAndParents(wname, (uint32_t)wlen, 0, vocid, 0);
}


//==========================================================================
//
//  ufoGetIP
//
//==========================================================================
uint32_t ufoGetIP (void) {
  return ufoIP;
}


//==========================================================================
//
//  ufoSetIP
//
//==========================================================================
void ufoSetIP (uint32_t newip) {
  ufoIP = newip;
}


//==========================================================================
//
//  ufoIsExecuting
//
//==========================================================================
int ufoIsExecuting (void) {
  return (ufoImgGetU32(ufoAddrSTATE) == 0);
}


//==========================================================================
//
//  ufoIsCompiling
//
//==========================================================================
int ufoIsCompiling (void) {
  return (ufoImgGetU32(ufoAddrSTATE) != 0);
}


//==========================================================================
//
//  ufoSetExecuting
//
//==========================================================================
void ufoSetExecuting (void) {
  ufoImgPutU32(ufoAddrSTATE, 0);
}


//==========================================================================
//
//  ufoSetCompiling
//
//==========================================================================
void ufoSetCompiling (void) {
  ufoImgPutU32(ufoAddrSTATE, 1);
}


//==========================================================================
//
//  ufoGetHere
//
//==========================================================================
uint32_t ufoGetHere () {
  return UFO_GET_DP();
}


//==========================================================================
//
//  ufoGetPad
//
//==========================================================================
uint32_t ufoGetPad () {
  return UFO_PAD_ADDR;
}


//==========================================================================
//
//  ufoTIBPeekCh
//
//==========================================================================
uint8_t ufoTIBPeekCh (uint32_t ofs) {
  return ufoTibPeekChOfs(ofs);
}


//==========================================================================
//
//  ufoTIBGetCh
//
//==========================================================================
uint8_t ufoTIBGetCh (void) {
  return ufoTibGetCh();
}


//==========================================================================
//
//  ufoTIBSkipCh
//
//==========================================================================
void ufoTIBSkipCh (void) {
  ufoTibSkipCh();
}


//==========================================================================
//
//  ufoTIBSRefill
//
//  returns 0 on EOF
//
//==========================================================================
int ufoTIBSRefill (int allowCrossIncludes) {
  return ufoLoadNextLine(allowCrossIncludes);
}


//==========================================================================
//
//  ufoPeekData
//
//==========================================================================
uint32_t ufoPeekData (void) {
  return ufoPeek();
}


//==========================================================================
//
//  ufoPopData
//
//==========================================================================
uint32_t ufoPopData (void) {
  return ufoPop();
}


//==========================================================================
//
//  ufoPushData
//
//==========================================================================
void ufoPushData (uint32_t value) {
  return ufoPush(value);
}


//==========================================================================
//
//  ufoPushBoolData
//
//==========================================================================
void ufoPushBoolData (int val) {
  ufoPushBool(val);
}


//==========================================================================
//
//  ufoPeekRet
//
//==========================================================================
uint32_t ufoPeekRet (void) {
  return ufoRPeek();
}


//==========================================================================
//
//  ufoPopRet
//
//==========================================================================
uint32_t ufoPopRet (void) {
  return ufoRPop();
}


//==========================================================================
//
//  ufoPushRet
//
//==========================================================================
void ufoPushRet (uint32_t value) {
  return ufoRPush(value);
}


//==========================================================================
//
//  ufoPushBoolRet
//
//==========================================================================
void ufoPushBoolRet (int val) {
  ufoRPush(val ? ufoTrueValue : 0);
}


//==========================================================================
//
//  ufoPeekByte
//
//==========================================================================
uint8_t ufoPeekByte (uint32_t addr) {
  return ufoImgGetU8(addr);
}


//==========================================================================
//
//  ufoPeekWord
//
//==========================================================================
uint16_t ufoPeekWord (uint32_t addr) {
  ufoPush(addr);
  UFCALL(WPEEK);
  return ufoPop();
}


//==========================================================================
//
//  ufoPeekCell
//
//==========================================================================
uint32_t ufoPeekCell (uint32_t addr) {
  ufoPush(addr);
  UFCALL(PEEK);
  return ufoPop();
}


//==========================================================================
//
//  ufoPokeByte
//
//==========================================================================
void ufoPokeByte (uint32_t addr, uint32_t value) {
  ufoImgPutU8(addr, value);
}


//==========================================================================
//
//  ufoPokeWord
//
//==========================================================================
void ufoPokeWord (uint32_t addr, uint32_t value) {
  ufoPush(value);
  ufoPush(addr);
  UFCALL(WPOKE);
}


//==========================================================================
//
//  ufoPokeCell
//
//==========================================================================
void ufoPokeCell (uint32_t addr, uint32_t value) {
  ufoPush(value);
  ufoPush(addr);
  UFCALL(POKE);
}


//==========================================================================
//
//  ufoGetPAD
//
//==========================================================================
uint32_t ufoGetPAD (void) {
  return UFO_PAD_ADDR;
}


//==========================================================================
//
//  ufoEmitByte
//
//==========================================================================
void ufoEmitByte (uint32_t value) {
  ufoImgEmitU8(value);
}


//==========================================================================
//
//  ufoEmitWord
//
//==========================================================================
void ufoEmitWord (uint32_t value) {
  ufoImgEmitU8(value & 0xff);
  ufoImgEmitU8((value >> 8) & 0xff);
}


//==========================================================================
//
//  ufoEmitCell
//
//==========================================================================
void ufoEmitCell (uint32_t value) {
  ufoImgEmitU32(value);
}


//==========================================================================
//
//  ufoIsInited
//
//==========================================================================
int ufoIsInited (void) {
  return (ufoMode != UFO_MODE_NONE);
}


//==========================================================================
//
//  ufoSetUserAbort
//
//==========================================================================
void ufoSetUserAbort (void) {
  //ufoVMAbort = 1;
  //HACK: push "(USER-ABORT)" word to RP
  ufoRPush(ufoUserAbortCFA);
}


static void (*ufoUserPostInitCB) (void);


//==========================================================================
//
//  ufoSetUserPostInit
//
//  called after main initialisation
//
//==========================================================================
void ufoSetUserPostInit (void (*cb) (void)) {
  ufoUserPostInitCB = cb;
}


//==========================================================================
//
//  ufoSStepAllowed
//
//==========================================================================
int ufoSStepAllowed (void) {
  #ifdef UFO_MTASK_ALLOWED
  return (ufoSingleStepAllowed != 0);
  #else
  return 0;
  #endif
}


//==========================================================================
//
//  ufoSetSStepAllowed
//
//==========================================================================
void ufoSetSStepAllowed (int enabled) {
  #ifdef UFO_MTASK_ALLOWED
  ufoSingleStepAllowed = (enabled ? 1 : 0);
  #else
  (void)enabled;
  #endif
}


//==========================================================================
//
//  ufoInit
//
//==========================================================================
void ufoInit (void) {
  if (ufoMode != UFO_MODE_NONE) return;
  ufoMode = UFO_MODE_NATIVE;

  #ifdef UFO_HUGE_IMAGES
  memset(ufoImage, 0, sizeof(ufoImage));
  #endif

  #ifdef UFO_MTASK_ALLOWED
  ufoSingleStepAllowed = 0;
  #endif

  ufoInFileLine = 0;
  ufoInFileName = NULL; ufoInFileNameLen = 0; ufoInFileNameHash = 0;
  ufoInFile = NULL;
  ufoLastIncPath = NULL; ufoLastSysIncPath = NULL;

  #ifdef UFO_MTASK_ALLOWED
  for (uint32_t f = 0; f < UFO_MAX_STATES; f += 1u) ufoStateMap[f] = NULL;
  memset(ufoStateUsedBitmap, 0, sizeof(ufoStateUsedBitmap));
  ufoCurrState = ufoNewState();
  strcpy(ufoCurrState->name, "MAIN");
  ufoInitStateUserVars(ufoCurrState);
  #else
  memset(&ufoCurrState, 0, sizeof(ufoCurrState));
  strcpy(ufoCurrState.name, "MAIN");
  ufoInitStateUserVars(&ufoCurrState);
  #endif

  ufoImgPutU32(ufoAddrDefTIB, 0); // create TIB handle
  ufoImgPutU32(ufoAddrTIBx, 0); // create TIB handle

  #ifdef UFO_MTASK_ALLOWED
  ufoYieldedState = NULL;
  ufoDebuggerState = NULL;
  ufoSingleStep = 0;
  #endif

  #ifdef UFO_DEBUG_STARTUP_TIMES
  uint32_t stt = ufo_get_msecs();
  ufoCondDefine("UFO-DEBUG-STARTUP-TIMES");
  #endif
  ufoInitCommon();
  #ifdef UFO_DEBUG_STARTUP_TIMES
  uint32_t ett = ufo_get_msecs();
  fprintf(stderr, "UrForth init time: %u msecs\n", (unsigned)(ett - stt));
  #endif

  ufoReset();

  if (ufoUserPostInitCB) {
    ufoUserPostInitCB();
    ufoReset();
  }

  // load ufo modules
  char *ufmname = ufoCreateIncludeName("init", 1, NULL);
  #ifdef WIN32
  FILE *ufl = fopen(ufmname, "rb");
  #else
  FILE *ufl = fopen(ufmname, "r");
  #endif
  if (ufl) {
    ufoPushInFile();
    ufoSetInFileNameReuse(ufmname);
    ufoInFile = ufl;
    ufoFileId = ufoLastUsedFileId;
    setLastIncPath(ufoInFileName, 1);
  } else {
    free(ufmname);
    ufoFatal("cannot load init code");
  }

  if (ufoInFile != NULL) {
    ufoRunInterpretLoop();
  }
}


//==========================================================================
//
//  ufoFinishVM
//
//==========================================================================
void ufoFinishVM (void) {
  if (ufoInRunWord) {
    longjmp(ufoStopVMJP, 669);
  } else {
    ufoFatal("VM is not running");
  }
}


//==========================================================================
//
//  ufoCallParseIntr
//
//  ( -- addr count TRUE / FALSE )
//  does base TIB parsing; never copies anything.
//  as our reader is line-based, returns FALSE on EOL.
//  EOL is detected after skipping leading delimiters.
//  passing -1 as delimiter skips the whole line, and always returns FALSE.
//  trailing delimiter is always skipped.
//  result is on the data stack.
//
//==========================================================================
void ufoCallParseIntr (uint32_t delim, int skipLeading) {
  ufoPush(delim); ufoPushBool(skipLeading);
  UFCALL(PAR_PARSE);
}


//==========================================================================
//
//  ufoCallParseName
//
//  ( -- addr count )
//  parse with leading blanks skipping. doesn't copy anything.
//  return empty string on EOL.
//
//==========================================================================
void ufoCallParseName (void) {
  UFCALL(PARSE_NAME);
}


//==========================================================================
//
//  ufoCallParse
//
//  ( -- addr count TRUE / FALSE )
//  parse without skipping delimiters; never copies anything.
//  as our reader is line-based, returns FALSE on EOL.
//  passing 0 as delimiter skips the whole line, and always returns FALSE.
//  trailing delimiter is always skipped.
//
//==========================================================================
void ufoCallParse (uint32_t delim) {
  ufoPush(delim);
  UFCALL(PARSE);
}


//==========================================================================
//
//  ufoCallParseSkipBlanks
//
//==========================================================================
void ufoCallParseSkipBlanks (void) {
  UFCALL(PARSE_SKIP_BLANKS);
}


//==========================================================================
//
//  ufoCallParseSkipComments
//
//==========================================================================
void ufoCallParseSkipComments (void) {
  ufoPushBool(1); UFCALL(PAR_PARSE_SKIP_COMMENTS);
}


//==========================================================================
//
//  ufoCallParseSkipLineComments
//
//==========================================================================
void ufoCallParseSkipLineComments (void) {
  ufoPushBool(0); UFCALL(PAR_PARSE_SKIP_COMMENTS);
}


//==========================================================================
//
//  ufoCallParseSkipLine
//
//  to the end of line; doesn't refill
//
//==========================================================================
void ufoCallParseSkipLine (void) {
  UFCALL(PARSE_SKIP_LINE);
}


//==========================================================================
//
//  ufoCallBasedNumber
//
//  convert number from addrl+1
//  returns address of the first inconvertible char
//  (BASED-NUMBER) ( addr count allowsign? base -- num TRUE / FALSE )
//
//==========================================================================
void ufoCallBasedNumber (uint32_t addr, uint32_t count, int allowSign, int base) {
  ufoPush(addr); ufoPush(count); ufoPushBool(allowSign);
  if (base < 0) ufoPush(0); else ufoPush((uint32_t)base);
  UFCALL(PAR_BASED_NUMBER);
}


//==========================================================================
//
//  ufoRunWord
//
//==========================================================================
void ufoRunWord (uint32_t cfa) {
  if (cfa != 0) {
    if (ufoMode == UFO_MODE_NONE) ufoFatal("UrForth is not properly inited");
    if (ufoInRunWord) ufoFatal("`ufoRunWord` cannot be called recursively");
    ufoMode = UFO_MODE_NATIVE;
    ufoRunVMCFA(cfa);
  }
}


//==========================================================================
//
//  ufoRunMacroWord
//
//==========================================================================
void ufoRunMacroWord (uint32_t cfa) {
  if (cfa != 0) {
    if (ufoMode == UFO_MODE_NONE) ufoFatal("UrForth is not properly inited");
    if (ufoInRunWord) ufoFatal("`ufoRunWord` cannot be called recursively");
    ufoMode = UFO_MODE_MACRO;
    const uint32_t oisp = ufoFileStackPos;
    ufoPushInFile();
    ufoFileId = 0;
    (void)ufoLoadNextUserLine();
    ufoRunVMCFA(cfa);
    ufoPopInFile();
    ufo_assert(ufoFileStackPos == oisp); // sanity check
  }
}


//==========================================================================
//
//  ufoIsInMacroMode
//
//  check if we are currently in "MACRO" mode.
//  should be called from registered words.
//
//==========================================================================
int ufoIsInMacroMode (void) {
  return (ufoMode == UFO_MODE_MACRO);
}


//==========================================================================
//
//  ufoRunInterpretLoop
//
//  run default interpret loop.
//
//==========================================================================
void ufoRunInterpretLoop (void) {
  if (ufoMode == UFO_MODE_NONE) {
    ufoInit();
  }
  const uint32_t cfa = ufoFindWord("RUN-INTERPRET-LOOP");
  if (cfa == 0) ufoFatal("'RUN-INTERPRET-LOOP' word not found");
  ufoReset();
  ufoMode = UFO_MODE_NATIVE;
  ufoRunVMCFA(cfa);
  while (ufoFileStackPos != 0) ufoPopInFile();
}


//==========================================================================
//
//  ufoRunFile
//
//==========================================================================
void ufoRunFile (const char *fname) {
  if (ufoMode == UFO_MODE_NONE) {
    ufoInit();
  }
  if (ufoInRunWord) ufoFatal("`ufoRunFile` cannot be called recursively");
  ufoMode = UFO_MODE_NATIVE;

  ufoReset();
  char *ufmname = ufoCreateIncludeName(fname, 0, ".");
  #ifdef WIN32
  FILE *ufl = fopen(ufmname, "rb");
  #else
  FILE *ufl = fopen(ufmname, "r");
  #endif
  if (ufl) {
    ufoPushInFile();
    ufoSetInFileNameReuse(ufmname);
    ufoInFile = ufl;
    ufoFileId = ufoLastUsedFileId;
    setLastIncPath(ufoInFileName, 0);
  } else {
    free(ufmname);
    ufoFatal("cannot load source file '%s'", fname);
  }
  ufoRunInterpretLoop();
}


//==========================================================================
//
//  ufoIsMTaskEnabled
//
//  check if the system was compiled with multitasking support
//
//==========================================================================
int ufoIsMTaskEnabled (void) {
  #ifdef UFO_MTASK_ALLOWED
  return 1;
  #else
  return 0;
  #endif
}

/*
 * WD1793/uPD765 emulator
 * Copyright (c) 2009-..., SAM style
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is furnished
 * to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
 * THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * changes by Ketmar // Invisible Vector
 *
 * Understanding is not required. Only obedience.
 * git commit: 84b4cb3f749ff3cb954a202b3c65d7b53ee08273
 */
#include "floppy.h"


//==========================================================================
//
//  flpCreate
//
//==========================================================================
Floppy *flpCreate (int id) {
  Floppy *flp = malloc(sizeof(Floppy));
  memset(flp, 0, sizeof(Floppy));
  flp->id = id;
  flp->trk80 = 1;
  flp->doubleSide = 1;
  flp->trk = 0;
  flp->pos = 0;
  return flp;
}


//==========================================================================
//
//  flpDestroy
//
//==========================================================================
void flpDestroy (Floppy *flp) {
  if (flp) {
    free(flp);
  }
}


//==========================================================================
//
//  flpEject
//
//==========================================================================
void flpEject (Floppy *flp) {
  if (flp) {
    flp->insert = 0;
    flp->changed = 0;
  }
}


//==========================================================================
//
//  flpWr
//
//==========================================================================
int flpWr (Floppy *flp, int hd, uint8_t val) {
  if (!flp) return FLPERR_SHIT;
  flp->wr = 1;
  hd &= 1;
  if (hd && !flp->doubleSide) return FLPERR_SHIT; // saving on HD1 for SS Floppy
  if (!flp->insert) return FLPERR_SHIT;
  flp->changed = 1;
  flp->data[(flp->trk<<1)|hd].byte[flp->pos] = val;
  return FLPERR_OK;
}


//==========================================================================
//
//  flpRd
//
//==========================================================================
uint8_t flpRd (Floppy *flp, int hd) {
  if (!flp) return 0xffU;
  flp->rd = 1;
  hd &= 1;
  if (!flp->insert) return 0xffU;
  if (hd && !flp->doubleSide) return 0xffU;
  return flp->data[(flp->trk<<1)|hd].byte[flp->pos];
}


//==========================================================================
//
//  flpStep
//
//==========================================================================
void flpStep (Floppy *flp, int dir) {
  if (!flp) return;
  switch (dir) {
    case FLP_FORWARD:
      if (flp->trk < (flp->trk80 ? 86 : 43)) ++flp->trk;
      break;
    case FLP_BACK:
      if (flp->trk > 0) --flp->trk;
      break;
  }
}


//==========================================================================
//
//  flpNext
//
//==========================================================================
int flpNext (Floppy *flp, int fdcSide) {
  if (!flp) return 0;
  int res = 0;
  int rtrk = (flp->trk<<1);
  if (flp->doubleSide && fdcSide) ++rtrk;
  if (flp->insert) {
    ++flp->pos;
    if (flp->pos >= FLP_MAX_TRACK_SIZE) {
      flp->pos = 0;
      res = 1;
    }
    flp->index = (flp->pos < 4 ? 1 : 0); // ~90ms index pulse
    flp->field = flp->data[rtrk].field[flp->pos]&0x0f;
  } else {
    flp->field = 0;
  }
  return res;
}


//==========================================================================
//
//  flpPrev
//
//==========================================================================
void flpPrev (Floppy *flp, int fdcSide) {
  if (!flp) return;
  int rtrk = (flp->trk<<1);
  if (flp->doubleSide && fdcSide) ++rtrk;
  if (flp->insert) {
    if (flp->pos > 0) {
      --flp->pos;
    } else {
      flp->pos = FLP_MAX_TRACK_SIZE-1;
    }
    flp->field = flp->data[rtrk].field[flp->pos]&0x0f;
  } else {
    flp->field = 0;
  }
}


//==========================================================================
//
//  flpFillFields
//
//==========================================================================
int flpFillFields (Floppy *flp, int tr, uint32_t flag) {
  if (!flp) return FLPERR_SHIT;
  if (tr < 0 || tr > 255) return FLPERR_SHIT;
  int res = FLPERR_OK;
  int bcnt = 0; // number of bytes in the current field
  uint8_t fld = 0;
  uint8_t *cpos = flp->data[tr].byte; // first byte of the current field
  uint8_t *bpos = cpos; // current byte
  uint8_t scn = 0; // sector number (1+)
  uint8_t sct = 1; // sector size code
  for (unsigned i = 0; i < 256; ++i) {
    flp->data[tr].map[i] = 0;
    flp->data[tr].mapaddr[i] = 0;
    flp->data[tr].mapraw[i] = 0;
    flp->data[tr].mapaddrraw[i] = 0;
  }
  uint8_t scraw = 0; // current "raw" sector index for address map
  int lastscraw = -1; // current "raw" sector index for data map
  for (unsigned i = 0; i < FLP_MAX_TRACK_SIZE; ++i, ++bpos) {
    flp->data[tr].field[i] = fld;
    fld &= 0x0f; // reset flags
    if (flag&FLP_FILL_FIELDS_SVF) {
      if (fld == 0) {
        if (*bpos == 0xf5) *bpos = 0xa1;
        if (*bpos == 0xf6) *bpos = 0xc2;
      }
    }
    // process field data
    if (bcnt > 0) {
      // we are in field yet
      --bcnt;
      if (bcnt == 0) {
        // field ends
        // fld should not be 0 here
        // other field types needs CRC, so calculate it
        if (fld < 4) {
          // always calculate CRC for address field
          if (fld == 1 || (flag&FLP_FILL_FIELDS_CRC)) {
            cpos -= 3; // including a1,a1,a1
            uint16_t crc = flpCalcWDCRC(cpos, bpos-cpos+1);
            // make "bad CRC" for data field, if requested
            if (fld != 1 && (flag&FLP_FILL_FIELDS_INV_CRC)) crc ^= 0xaa55u;
            if (i+1 < FLP_MAX_TRACK_SIZE) *(bpos+1) = ((crc&0xff00u)>>8); else res = FLPERR_NOSPACE;
            if (i+2 < FLP_MAX_TRACK_SIZE) *(bpos+2) = (crc&0xffu); else res = FLPERR_NOSPACE;
          }
          fld = 4; // "CRC field" type
          bcnt = 2; // two bytes
        } else {
          fld = 0; // "gap field" type
        }
      }
    } else {
      // out of the field, check type and setup new field
      const uint8_t bt = *bpos;
      if (bt == 0xfe) {
        // address mark last byte, CHRN follows
        cpos = bpos; // save field start
        fld = 1; // "address field" type
        bcnt = 4; // 4 bytes (CHRN)
        scn = (i+3 < FLP_MAX_TRACK_SIZE ? flp->data[tr].byte[i+3] : 0); // get logical sector number
        sct = (i+4 < FLP_MAX_TRACK_SIZE ? flp->data[tr].byte[i+4] : 0); // get sector size
        if (i+4 < FLP_MAX_TRACK_SIZE) {
          // remember address area for this logical sector
          // for sectors with the same logical number, assume that the first sector always wins
          if (scn > 0 && flp->data[tr].mapaddr[scn] == 0) flp->data[tr].mapaddr[scn] = i+1;
          // remember address area for this "raw" sector
          lastscraw = scraw;
          flp->data[tr].mapaddrraw[scraw++] = i+1;
        }
      } else if (bt == 0xfb || bt == 0xf8) {
        // 0xfb -- normal data
        // 0xf8 -- deleted data
        cpos = bpos; // save field start
        fld = (bt == 0xfb ? 2 : 3); // set field type
        bcnt = 128<<(sct&3); // bytes in field (k8: why `&3`?)
        if (i+1 < FLP_MAX_TRACK_SIZE) {
          // remember data area for this logical sector
          // for sectors with the same logical number, assume that the first sector always wins
          if (scn > 0 && flp->data[tr].map[scn] == 0) {
            flp->data[tr].map[scn] = i+1;
            scn = 0; // reset logical sector number, we don't need it anymore
          }
          // remember data area for this "raw" sector
          if (lastscraw >= 0) {
            flp->data[tr].mapraw[lastscraw] = i+1;
            lastscraw = -1;
          }
        }
      }
    }
  }
  return res;
}


//==========================================================================
//
//  flpClearTrack
//
//==========================================================================
int flpClearTrack (Floppy *flp, int tr) {
  if (!flp) return FLPERR_SHIT;
  if (tr < 0 || tr > 255) return FLPERR_SHIT;
  memset(flp->data[tr].byte, 0, FLP_MAX_TRACK_SIZE);
  memset(flp->data[tr].field, 0, FLP_MAX_TRACK_SIZE);
  memset(flp->data[tr].map, 0, sizeof(flp->data[tr].map));
  memset(flp->data[tr].mapaddr, 0, sizeof(flp->data[tr].mapaddr));
  memset(flp->data[tr].mapraw, 0, sizeof(flp->data[tr].mapraw));
  memset(flp->data[tr].mapaddrraw, 0, sizeof(flp->data[tr].mapaddrraw));
  return FLPERR_OK;
}


//==========================================================================
//
//  flpClearDisk
//
//==========================================================================
int flpClearDisk (Floppy *flp) {
  if (!flp) return FLPERR_SHIT;
  for (int i = 0; i < 256; ++i) flpClearTrack(flp, i);
  return FLPERR_OK;
}


//==========================================================================
//
//  flpGetTrack
//
//==========================================================================
int flpGetTrack (Floppy *flp, int tr, uint8_t *dst) {
  if (!flp || !dst) return FLPERR_SHIT;
  if (tr < 0 || tr > 255) return FLPERR_SHIT;
  memcpy(dst, flp->data[tr].byte, FLP_MAX_TRACK_SIZE);
  return FLPERR_OK;
}


//==========================================================================
//
//  flpGetTrackFields
//
//==========================================================================
int flpGetTrackFields (Floppy *flp, int tr, uint8_t *dst) {
  if (!flp || !dst) return FLPERR_SHIT;
  if (tr < 0 || tr > 255) return FLPERR_SHIT;
  memcpy(dst, flp->data[tr].field, FLP_MAX_TRACK_SIZE);
  return FLPERR_OK;
}


//==========================================================================
//
//  flpPutTrack
//
//==========================================================================
int flpPutTrack (Floppy *flp, int tr, const void *src, int len) {
  if (!flp || len < 0 || len > FLP_MAX_TRACK_SIZE) return FLPERR_SHIT;
  if (tr < 0 || tr > 255) return FLPERR_SHIT;
  if (len && !src) return FLPERR_SHIT;
  if (flpClearTrack(flp, tr) < 0) return FLPERR_SHIT;
  if (len > 0) memcpy(flp->data[tr].byte, src, len);
  return flpFillFields(flp, tr, FLP_FILL_FIELDS_NONE);
}


//==========================================================================
//
//  flpCalcWDCRC
//
//==========================================================================
uint16_t flpCalcWDCRC (const void *buf, size_t len) {
  uint32_t crc = 0xffffU;
  const uint8_t *ptr = (const uint8_t *)buf;
  while (len--) {
    crc ^= (*ptr++)<<8;
    for (unsigned i = 0; i < 8; ++i) {
      crc <<= 1;
      if (crc&0x10000U) crc ^= 0x1021U;
    }
  }
  return (crc&0xffffU);
}



//**************************************************************************
//
// some higher-level API
//
//**************************************************************************


#define FFMT_PUT_BYTE(bt_)  do { \
  if (tbleft == 0) return FLPERR_OK; \
  --tbleft; \
  *(ppos++) = (bt_); \
} while (0)

#define FFMT_FILL_BYTES(bt_,cnt_)  do { \
  if (tbleft == 0) return FLPERR_OK; \
  const size_t cc = (tbleft < (cnt_) ? tbleft : (size_t)(cnt_)); \
  tbleft -= cc; \
  memset(ppos, (bt_), cc); \
} while (0)


//==========================================================================
//
//  flpFormatTracks
//
//  format a single track from sectors list
//  <sdata> is list of <scount> sectors
//  TODO: calculate GAP3 len !!!
//
//==========================================================================
int flpFormatTracks (Floppy *flp, int tr, const FDCSector *sdata, int scount, int calccrc) {
  if (!flp || scount < 1 || scount > 255 || !sdata) return FLPERR_SHIT;
  if (tr < 0 || tr > 255) return FLPERR_SHIT;
  //fprintf(stderr, "flpFormatTracks:000: tr=%d; scount=%d; calccrc=%d\n", tr, scount, calccrc);
  uint8_t *ppos = flp->data[tr].byte;
  //const uint8_t *const stppos = ppos;
  int dsz = 0;
  for (int i = 0; i < scount; ++i) dsz += (128<<sdata[i].sz);
  dsz = (FLP_MAX_TRACK_SIZE-dsz)/scount-72;
  if (dsz < 10) return FLPERR_SHIT;
  size_t tbleft = (size_t)FLP_MAX_TRACK_SIZE;
  // 12 spaces
  FFMT_FILL_BYTES(0x00u, 12);
  ppos += 12;
  // track mark
  FFMT_PUT_BYTE(0xc2u);
  FFMT_PUT_BYTE(0xc2u);
  FFMT_PUT_BYTE(0xc2u);
  FFMT_PUT_BYTE(0xfcu);
  //FIXME: make this safer!
  for (unsigned sc = 0; sc < (unsigned)scount; ++sc) {
    //size_t tbleft = (ppos >= stppos+FLP_MAX_TRACK_SIZE ? (size_t)0u : (size_t)(ptrdiff_t)(stppos+FLP_MAX_TRACK_SIZE-ppos));
    // 10 sync (GAP1)
    FFMT_FILL_BYTES(0x4eu, 10);
    ppos += 10;
    // 12 spaces
    FFMT_FILL_BYTES(0x00u, 12);
    ppos += 12;
    // address mark
    FFMT_PUT_BYTE(0xa1u);
    FFMT_PUT_BYTE(0xa1u);
    FFMT_PUT_BYTE(0xa1u);
    FFMT_PUT_BYTE(0xfeu);
    // addr field
    FFMT_PUT_BYTE(sdata[sc].trk);
    FFMT_PUT_BYTE(sdata[sc].head);
    FFMT_PUT_BYTE(sdata[sc].sec);
    FFMT_PUT_BYTE(sdata[sc].sz);
    // address crc (will be fixed later)
    FFMT_PUT_BYTE(0xf7u);
    FFMT_PUT_BYTE(0xf7u);
    // 22 syncs (GAP2)
    FFMT_FILL_BYTES(0x4eu, 22);
    ppos += 22;
    // 12 spaces
    FFMT_FILL_BYTES(0x00u, 12);
    ppos += 12;
    // data mark
    FFMT_PUT_BYTE(0xa1u);
    FFMT_PUT_BYTE(0xa1u);
    FFMT_PUT_BYTE(0xa1u);
    FFMT_PUT_BYTE(sdata[sc].type); // 0xf8 (deleted) or 0xfb (normal)
    // data
    //fprintf(stderr, "tbleft=%u\n", (unsigned)tbleft);
    const size_t ln = 128u<<(sdata[sc].sz&0x03u);
    if (tbleft < ln) {
      if (tbleft) memcpy(ppos, sdata[sc].data, tbleft);
      tbleft = 0;
      return FLPERR_OK;
    } else {
      memcpy(ppos, sdata[sc].data, ln);
      tbleft -= ln;
    }
    ppos += ln;
    if (tbleft < 2) return FLPERR_SHIT;
    // data crc
    FFMT_PUT_BYTE(0xf7);
    FFMT_PUT_BYTE(0xf7);
    // 60 sync (GAP3)
    FFMT_FILL_BYTES(0x4eu, dsz);
    ppos += dsz;
  }
  // last sync (GAP4)
  while (ppos-flp->data[tr].byte < FLP_MAX_TRACK_SIZE) {
    FFMT_PUT_BYTE(0x4e);
  }
  flpFillFields(flp, tr, (calccrc ? FLP_FILL_FIELDS_ALL : FLP_FILL_FIELDS_NONE));
  return FLPERR_OK;
}


//==========================================================================
//
//  flpGetSectorDataPtr
//
//==========================================================================
uint8_t *flpGetSectorDataPtr (Floppy *flp, uint8_t tr, uint8_t sc) {
  if (!flp) return NULL;
  const uint32_t datapos = flp->data[tr].map[sc]; // input nr 1+, tab nr 0+
  if (datapos < 4 || datapos >= FLP_MAX_TRACK_SIZE) return NULL;
  return flp->data[tr].byte+datapos;
}


//==========================================================================
//
//  flpGetSectorSize
//
//==========================================================================
uint16_t flpGetSectorSize (Floppy *flp, uint8_t tr, uint8_t sc) {
  if (!flp) return 0;
  uint32_t addrpos = flp->data[tr].mapaddr[sc]; // input nr 1+, tab nr 0+
  if (addrpos == 0 || addrpos+3 >= FLP_MAX_TRACK_SIZE) return 0;
  return 128u<<flp->data[tr].byte[addrpos+3];
}


//==========================================================================
//
//  flpGetSectorSizeSafe
//
//==========================================================================
uint16_t flpGetSectorSizeSafe (Floppy *flp, uint8_t tr, uint8_t sc) {
  if (!flp) return 0;
  uint32_t addrpos = flp->data[tr].mapaddr[sc]; // input nr 1+, tab nr 0+
  if (addrpos == 0 || addrpos+4 > FLP_MAX_TRACK_SIZE) return 0;
  uint32_t datapos = flp->data[tr].map[sc]; // input nr 1+, tab nr 0+
  if (datapos < 4 || datapos >= FLP_MAX_TRACK_SIZE) return 0;
  const uint8_t n = flp->data[tr].byte[addrpos+3];
  if (n > 6) return 0; // >8192? wtf?! is it bigger than track size
  const uint16_t secsize = 128u<<n;
  // check if it fits the track
  if (datapos+secsize > FLP_MAX_TRACK_SIZE) return 0;
  // ok, it seems to be safe
  return secsize;
}


//==========================================================================
//
//  flpGetRawSectorC
//
//==========================================================================
int flpGetRawSectorC (Floppy *flp, uint8_t tr, uint8_t sc) {
  if (!flp) return FLPERR_SHIT;
  uint32_t addrpos = flp->data[tr].mapaddrraw[sc]; // input nr 1+, tab nr 0+
  if (addrpos == 0 || addrpos+0 >= FLP_MAX_TRACK_SIZE) return FLPERR_SHIT;
  return flp->data[tr].byte[addrpos+0];
}


//==========================================================================
//
//  flpGetRawSectorH
//
//==========================================================================
int flpGetRawSectorH (Floppy *flp, uint8_t tr, uint8_t sc) {
  if (!flp) return FLPERR_SHIT;
  uint32_t addrpos = flp->data[tr].mapaddrraw[sc]; // input nr 1+, tab nr 0+
  if (addrpos == 0 || addrpos+1 >= FLP_MAX_TRACK_SIZE) return FLPERR_SHIT;
  return flp->data[tr].byte[addrpos+1];
}


//==========================================================================
//
//  flpGetRawSectorR
//
//==========================================================================
int flpGetRawSectorR (Floppy *flp, uint8_t tr, uint8_t sc) {
  if (!flp) return FLPERR_SHIT;
  uint32_t addrpos = flp->data[tr].mapaddrraw[sc]; // input nr 1+, tab nr 0+
  if (addrpos == 0 || addrpos+2 >= FLP_MAX_TRACK_SIZE) return FLPERR_SHIT;
  return flp->data[tr].byte[addrpos+2];
}


//==========================================================================
//
//  flpGetRawSectorN
//
//==========================================================================
int flpGetRawSectorN (Floppy *flp, uint8_t tr, uint8_t sc) {
  if (!flp) return FLPERR_SHIT;
  uint32_t addrpos = flp->data[tr].mapaddrraw[sc]; // input nr 1+, tab nr 0+
  if (addrpos == 0 || addrpos+3 >= FLP_MAX_TRACK_SIZE) return FLPERR_SHIT;
  return flp->data[tr].byte[addrpos+3];
}


//==========================================================================
//
//  flp_get_sector_data_common
//
//==========================================================================
static void flp_get_sector_data_common (Floppy *flp, uint8_t tr, uint32_t datapos, uint32_t addrpos, FDCSectorInfo **sinf, int *crcok) {
  if (datapos < 4 || datapos >= FLP_MAX_TRACK_SIZE || addrpos+3 >= FLP_MAX_TRACK_SIZE) {
    if (sinf) *sinf = NULL;
    if (crcok) *crcok = 0;
    return;
  }
  if (sinf) *sinf = (FDCSectorInfo *)(flp->data[tr].byte+addrpos);
  /* check data crc */
  if (crcok) {
    *crcok = 0;
    const uint8_t ssz = flp->data[tr].byte[addrpos+3];
    if (ssz <= 5) {
      const uint32_t bytesz = (128u<<ssz);
      if (datapos+bytesz <= FLP_MAX_TRACK_SIZE) {
        const uint16_t crc = flpCalcWDCRC(flp->data[tr].byte+datapos-4, bytesz+4);
        const uint16_t xcrc = (((uint16_t)flp->data[tr].byte[datapos+bytesz])<<8)|((uint16_t)flp->data[tr].byte[datapos+bytesz+1]);
        *crcok = (crc == xcrc ? 1 : 0);
      }
    }
  }
}


//==========================================================================
//
//  flpGetSectorDataPtrEx
//
//==========================================================================
uint8_t *flpGetSectorDataPtrEx (Floppy *flp, uint8_t tr, uint8_t sc, FDCSectorInfo **sinf, int *crcok) {
  if (crcok) *crcok = 0;
  if (sinf) *sinf = NULL;

  if (!flp) return NULL;

  const uint32_t datapos = flp->data[tr].map[sc]; // input nr 1+, tab nr 0+
  if (datapos < 4 || datapos >= FLP_MAX_TRACK_SIZE) return NULL;

  const uint32_t addrpos = flp->data[tr].mapaddr[sc]; // input nr 1+, tab nr 0+
  if (addrpos == 0 || addrpos+4 > FLP_MAX_TRACK_SIZE) return NULL;

  flp_get_sector_data_common(flp, tr, datapos, addrpos, sinf, crcok);
  return flp->data[tr].byte+datapos;
}


//==========================================================================
//
//  flpGetRawSectorDataPtr
//
//==========================================================================
uint8_t *flpGetRawSectorDataPtr (Floppy *flp, uint8_t tr, int idx, FDCSectorInfo **sinf, int *crcok) {
  if (crcok) *crcok = 0;
  if (sinf) *sinf = NULL;

  if (!flp || idx < 0 || idx > 255) return NULL;

  uint32_t datapos = flp->data[tr].mapraw[idx]; // input nr 1+, tab nr 0+
  if (datapos < 4) return NULL;

  uint32_t addrpos = flp->data[tr].mapaddrraw[idx]; // input nr 1+, tab nr 0+
  if (addrpos == 0 || addrpos+4 > FLP_MAX_TRACK_SIZE) return NULL;

  flp_get_sector_data_common(flp, tr, datapos, addrpos, sinf, crcok);
  return flp->data[tr].byte+datapos;
}


//==========================================================================
//
//  flpFixSectorDataCRC
//
//==========================================================================
int flpFixSectorDataCRC (Floppy *flp, uint8_t tr, uint8_t sc) {
  if (!flp) return FLPERR_SHIT;
  uint8_t *ptr = flpGetSectorDataPtr(flp, tr, sc);
  if (ptr == NULL) return FLPERR_SHIT;
  const uint16_t secsize = flpGetSectorSizeSafe(flp, tr, sc);
  if (secsize == 0) return FLPERR_SHIT;
  // fix CRC
  const uint16_t crc = flpCalcWDCRC(ptr-4, secsize+4);
  ptr += secsize;
  ptr[0] = (crc>>8)&0xffu;
  ptr[1] = (crc&0xffu);
  return FLPERR_OK;
}


//==========================================================================
//
//  flpPutSectorData
//
//==========================================================================
int flpPutSectorData (Floppy *flp, uint8_t tr, uint8_t sc, const void *buf, size_t len) {
  if (!flp) return FLPERR_SHIT;
  if (len && !buf) return FLPERR_SHIT;
  if (len > FLP_MAX_TRACK_SIZE) return FLPERR_SHIT;
  if (len) {
    const uint16_t secsize = flpGetSectorSizeSafe(flp, tr, sc);
    if (secsize == 0) return FLPERR_SHIT;
    if (len > secsize) return FLPERR_NOSPACE;
    uint8_t *ptr = flpGetSectorDataPtr(flp, tr, sc);
    if (ptr == NULL) return FLPERR_SHIT;
    memcpy(ptr, buf, len);
  }
  return flpFixSectorDataCRC(flp, tr, sc);
}


//==========================================================================
//
//  flpGetSectorData
//
//==========================================================================
int flpGetSectorData (Floppy *flp, uint8_t tr, uint8_t sc, void *buf, size_t len) {
  if (!flp) return FLPERR_SHIT;
  if (len && !buf) return FLPERR_SHIT;
  const uint16_t secsize = flpGetSectorSizeSafe(flp, tr, sc);
  if (secsize == 0) return FLPERR_SHIT;
  int res = FLPERR_OK;
  if (len > secsize) {
    res = FLPERR_NOSPACE;
    len = secsize;
  }
  uint8_t *ptr = flpGetSectorDataPtr(flp, tr, sc);
  if (ptr == NULL) return FLPERR_SHIT;
  if (len) memcpy(buf, ptr, len);
  return res;
}

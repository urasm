/*
 * WD1793/uPD765 emulator
 * Copyright (c) 2009-..., SAM style
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is furnished
 * to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
 * THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * changes by Ketmar // Invisible Vector
 *
 * Understanding is not required. Only obedience.
 */
// TR-DOS FS operations (incomplete)
#ifndef LIBFDC_DSKFS_ALL_H
#define LIBFDC_DSKFS_ALL_H

#include "floppy.h"
#include "dskfs_trdos.h"
#include "dskfs_p3dos.h"

#ifdef __cplusplus
extern "C" {
#endif


#define FLP_TRD_TRACK_SIZE  (4096)

// maximum number of tracks for 80-cylinder drive
#define FLP_MAX_TRACKS_80  (86*2)


// disk type
typedef enum {
  FLP_DISK_TYPE_UNKNOWN = 0, // or error
  FLP_DISK_TYPE_TRDOS   = 1,
  FLP_DISK_TYPE_P3DOS   = 2,
} FloppyDiskType;


// ////////////////////////////////////////////////////////////////////////// //
FloppyDiskType flpDetectDiskType (Floppy *flp);


#ifdef __cplusplus
}
#endif

#endif

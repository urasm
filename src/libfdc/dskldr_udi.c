/*
 * disk images I/O
 * coded by Ketmar // Invisible Vector
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 */
#include "dskldr.h"
#include "dskldr_common.h"
#include "dskfs_all.h"


//==========================================================================
//
//  udiCRC32
//
//  crc32 for UDI, taken from Unreal 0.32.7
//
//==========================================================================
static void udiCRC32 (uint32_t *crcp, const uint8_t *buf, int len) {
  uint32_t crc = *crcp;
  while (len--) {
    crc ^= -1^*buf++;
    for (int k = 8; --k; ) { int temp = -(crc&1); crc >>= 1, crc ^= 0xEDB88320&temp; }
    crc ^= FLPERR_SHIT;
  }
  *crcp = crc;
}


//==========================================================================
//
//  loadUDITrack
//
//==========================================================================
static int loadUDITrack (Floppy *flp, FILE *fl, uint8_t tr, int sd) {
  int rt = (tr<<1)+(sd ? 1 : 0);
  uint8_t type;
  uint32_t len;
  uint8_t trkBuf[FLP_MAX_TRACK_SIZE];
  if (fread(&type, 1, 1, fl) != 1) {
    // only MFM disks are allowed
    libfdcMsg(LIBFDC_MSG_ERROR, "only MFM UDI disks are allowed");
    return FLPERR_SHIT;
  }
  if (type != 0x00) {
    // skip unknown field
    //fprintf(stderr, "UDI: TRK %u: unknown format 0x%02x\n", rt, type);
    libfdcMsg(LIBFDC_MSG_ERROR, "UDI track %u unknown format 0x%02x", rt, type);
    if (freaduint(fl, &len, 4) < 0 || len > 0x7fffffffU) return FLPERR_SHIT; // field len
    if (fseek(fl, len, SEEK_CUR) != 0) return FLPERR_SHIT;
  } else {
    // MFM
    // track size
    if (freaduint(fl, &len, 2) < 0) return FLPERR_SHIT;
    if (len > FLP_MAX_TRACK_SIZE) {
      //fprintf(stderr, "UDI: TRK %u: too long (%u)\n", rt, len);
      libfdcMsg(LIBFDC_MSG_ERROR, "UDI track %u too long (%u/%u)", rt, len, (unsigned)FLP_MAX_TRACK_SIZE);
      #if 0
      // skip track image
      if (fseek(fl, len, SEEK_CUR) != 0) return FLPERR_SHIT;
      // and bit field
      len = (len>>3)+((len&7) == 0 ? 0 : 1);
      if (fseek(fl, len, SEEK_CUR) != 0) return FLPERR_SHIT;
      #else
      memset(trkBuf, 0, sizeof(trkBuf));
      if (fread(trkBuf, FLP_MAX_TRACK_SIZE, 1, fl) != 1) return FLPERR_SHIT;
      flpPutTrack(flp, rt, trkBuf, FLP_MAX_TRACK_SIZE);
      // skip rest of the track image
      if (fseek(fl, len-FLP_MAX_TRACK_SIZE, SEEK_CUR) != 0) return FLPERR_SHIT;
      // process bit fields?
      len = (len>>3)+(((len&7) == 0) ? 0 : 1); // skip bit field
      if (fseek(fl, len, SEEK_CUR) != 0) return FLPERR_SHIT;
      #endif
    } else {
      memset(trkBuf, 0, sizeof(trkBuf));
      if (len > 0 && fread(trkBuf, len, 1, fl) != 1) return FLPERR_SHIT;
      flpPutTrack(flp, rt, trkBuf, len);
      // process bit fields?
      len = (len>>3)+(((len&7) == 0) ? 0 : 1); // skip bit field
      if (fseek(fl, len, SEEK_CUR) != 0) return FLPERR_SHIT;
    }
  }
  return FLPERR_OK;
}


//==========================================================================
//
//  dskLoadUDI
//
//==========================================================================
int dskLoadUDI (Floppy *flp, FILE *fl) {
  uint8_t buf[16];
  if (fl == NULL) return FLPERR_SHIT;
  if (fread(buf, 16, 1, fl) != 1) {
    libfdcMsg(LIBFDC_MSG_ERROR, "cannot load UDI header");
    return FLPERR_SHIT;
  }
  if (memcmp(buf, "UDI!", 4) != 0) {
    libfdcMsg(LIBFDC_MSG_ERROR, "not an UDI image");
    return FLPERR_SHIT;
  }
  if (buf[8] != 0) {
    libfdcMsg(LIBFDC_MSG_ERROR, "invalid UDI version (%u)", buf[8]);
    return FLPERR_SHIT;
  }
  if (buf[10] > 1) {
    libfdcMsg(LIBFDC_MSG_ERROR, "invalid UDI side count (%u)", buf[10]+1);
    return FLPERR_SHIT;
  }
  if (buf[12] != 0 || buf[13] != 0 || buf[14] != 0 || buf[15] != 0) {
    libfdcMsg(LIBFDC_MSG_ERROR, "found UDI extender header (%u bytes)", (unsigned)(buf[10]|(buf[11]<<8)|(buf[12]<<16)|(buf[13]<<24)));
    return FLPERR_SHIT;
  }
  const int cylCount = buf[9]+1; // max track
  const int sides = (buf[10] == 0x01); // true if double side
  libfdcMsg(LIBFDC_MSG_DEBUG, "UDI: %d sides, %d cylinders", sides+1, cylCount);
  for (int i = 0; i < cylCount; ++i) {
    if (loadUDITrack(flp, fl, (uint8_t)i, 0) < 0) goto error;
    if (sides) { if (loadUDITrack(flp, fl, (uint8_t)i, 1) < 0) goto error; }
  }
  flp->protect = 0;
  flp->doubleSide = (sides ? 1 : 0);
  flp->trk80 = (cylCount > 42 ? 1 : 0);
  flp->insert = 1;
  flp->changed = 0;
  return FLPERR_OK;
error:
  flp->insert = 0;
  flp->changed = 0;
  flpFormatTRD(flp);
  return FLPERR_SHIT;
}


//==========================================================================
//
//  getUDIBitField
//
//==========================================================================
static void getUDIBitField (Floppy *flp, uint8_t tr, uint8_t *buf) {
  unsigned msk = 0x01;
  uint8_t fieldBuf[FLP_MAX_TRACK_SIZE];
  uint8_t trkBuf[FLP_MAX_TRACK_SIZE];
  flpGetTrack(flp, tr, trkBuf);
  flpGetTrackFields(flp, tr, fieldBuf);
  for (int i = 0; i < FLP_MAX_TRACK_SIZE; ++i) {
    if (msk == 0x100) {
      msk = 0x01;
      *(++buf) = 0x00;
    }
    if (fieldBuf[i] == 0 && trkBuf[i] == 0xa1) *buf |= (uint8_t)msk;
    msk <<= 1;
  }
}


//==========================================================================
//
//  dskSaveUDI
//
//==========================================================================
int dskSaveUDI (Floppy *flp, FILE *fl) {
  static const char *sign = "UDI!";
  uint8_t *img; // 0x112cf4 for 160 tracks in UDI
  uint8_t *dptr, *bptr;
  if (fl == NULL) return FLPERR_SHIT;
  if ((dptr = img = calloc(1, 0x112cf4)) == NULL) return FLPERR_SHIT;
  memcpy(dptr, sign, 4);
  bptr = img+4;
  dptr += 8;
  *(dptr++) = 0x00; // version
  *(dptr++) = (flp->trk80 ? 79 : 39); // maximun track number
  *(dptr++) = (flp->doubleSide ? 1 : 0); // double side (due to floppy property)
  *(dptr++) = 0x00;
  *(dptr++) = 0x00;
  *(dptr++) = 0x00;
  *(dptr++) = 0x00;
  *(dptr++) = 0x00;
  for (int i = 0; i < (flp->trk80 ? 160 : 80); ++i) {
    *(dptr++) = 0x00; // MFM
    *(dptr++) = FLP_MAX_TRACK_SIZE&0xffU; // track len
    *(dptr++) = (FLP_MAX_TRACK_SIZE>>8)&0xffU;
    flpGetTrack(flp, i, dptr); // track image
    dptr += FLP_MAX_TRACK_SIZE;
    getUDIBitField(flp, i, dptr);
    dptr += 782; // 6250/8+1
    if (!flp->doubleSide) ++i; // if single-side skip
  }
  int i = (int)(ptrdiff_t)(dptr-img);
  writeui32(bptr, i);
  uint32_t j = 0xffffffffU;
  udiCRC32(&j, img, i);
  //fprintf(stderr, "UDI: crc = 0x%08x\n", j);
  writeui32(dptr, j);
  dptr += 4;
  if (fwrite(img, (ptrdiff_t)(dptr-img), 1, fl) != 1) {
    free(img);
    return FLPERR_SHIT;
  }
  free(img);
  flp->changed = 0;
  return FLPERR_OK;
}

/*
 * WD1793/uPD765 emulator
 * Copyright (c) 2009-..., SAM style
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is furnished
 * to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
 * THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * changes by Ketmar // Invisible Vector
 *
 * Understanding is not required. Only obedience.
 */
// TR-DOS FS operations (incomplete)
#ifndef LIBFDC_DSKFS_TRDOS_H
#define LIBFDC_DSKFS_TRDOS_H

#include "floppy.h"

#ifdef __cplusplus
extern "C" {
#endif


#define FLP_TRD_TRACK_SIZE           (4096)
#define FLP_TRDOS_SECTORS_PER_TRACK  (16)
#define FLP_TRDOS_SECTOR_SIZE        (256)

// maximum number of tracks for 80-cylinder drive
#define FLP_TRDOS_MAX_TRACKS_80  (86*2)
#define FLP_TRDOS_CATALOG_MAX    (128)



typedef struct LIBFDC_PACKED {
  uint8_t name[8];
  uint8_t ext;
  uint8_t lst, hst;
  uint8_t llen, hlen;
  uint8_t slen;
  uint8_t sec;
  uint8_t trk;
} TRFile;


// ////////////////////////////////////////////////////////////////////////// //
int flpIsDiskTRDOS (Floppy *flp);

// format whole disk as 2x84x16x256 and init as TRDOS
// <0: error
int flpFormatTRD (Floppy *flp);

// build a single track 16x256 (TRDOS), sector data @bpos (4K)
// <0: error
int flpFormatTRDTrack (Floppy *flp, int tr, const void *bpos, size_t bpossize);

//FLPERR_xxx
int flpCreateFile (Floppy *flp, TRFile *dsc);
int flpGetCatalogEntry (Floppy *flp, TRFile *dst, int num);

// `dst` must have room for at least `FLP_TRDOS_CATALOG_MAX` items
// returns number of items (0 on error)
// `dst` can be NULL if you only want to count files
int flpGetTRCatalog (Floppy *flp, TRFile *dst);

int flpHasBoot (Floppy *flp); // return boot index in directory or -1
int flpSetBoot (Floppy *flp, FILE *fl, int replace);
// if the disk is TR-DOS, and it has only one basic file, and that file is
// not boot, create a very simple boot
int flpSetBootSimple (Floppy *flp); // 0: set
int flpRemoveBoot (Floppy *flp);

// `dst` can be NULL
// returns index if found, -1 if no or error
// does some simple file name sanity check
int flpFindFirstBasic (Floppy *flp, TRFile *dst, int ffirst);

// if not, `dst` is undefined
// `dst` can be NULL
int flpHasAnyNonBootBasic (Floppy *flp, TRFile *dst);


#ifdef __cplusplus
}
#endif

#endif

/*
 * WD1793/uPD765 emulator
 * Copyright (c) 2009-..., SAM style
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is furnished
 * to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
 * THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * changes by Ketmar // Invisible Vector
 *
 * Understanding is not required. Only obedience.
 */
// FDC emulators, and high-level disk interface API
#ifndef LIBFDC_FDC_H
#define LIBFDC_FDC_H

#include "bdicommon.h"
#include "floppy.h"

#ifdef __cplusplus
extern "C" {
#endif


#define FDC_FAST  (0x01u)

enum {
  DIF_NONE = 0,
  DIF_BDI,
  DIF_P3DOS,
  DIF_END = -1
};

#define BDI_SYS   (0xff)
#define FDC_COM   (0x1f)
#define FDC_TRK   (0x3f)
#define FDC_SEC   (0x5f)
#define FDC_DATA  (0x7f)

typedef struct FDC FDC;
typedef void (*fdcCall) (FDC *);

struct FDC {
  const int id;
  unsigned irq:1;   // VG93:irq; uPD765:exec
  unsigned drq:1;   // 0:data request
  unsigned dir:1;   // drq dir: 0 - from CPU; 1 - to CPU
  unsigned mr:1;    // master reset
  unsigned block:1;
  unsigned side:1;
  unsigned step:1; // step dir
  unsigned mfm:1;
  unsigned idle:1;
  unsigned crchi:1;
  // options
  unsigned optionTurbo:1;
  // other things
  uint8_t trk; // registers
  uint8_t sec;
  uint8_t data;
  uint8_t com;
  uint8_t state; // vp1-128 : mode (seek/rd/wr)
  uint8_t tmp;
  uint16_t wdata;
  uint16_t tdata;
  Floppy *flop[4]; // four floppy drives, always inited by `difCreate()`
  Floppy *flp; // current floppy ptr
  uint16_t crc; // calculated crc
  uint16_t fcrc; // crc get from floppy
  uint8_t buf[6];
  int fmode;
  int cnt;
  int wait; // pause (ns)
  int tns;
  const fdcCall *plan; // current task
  unsigned pos; // pos in plan

  unsigned dma:1; // not implemented yet
  unsigned intr:1; // uPD765 interrupt pending. reset @ com08
  int hlt; // head load time (all ns)
  int hut; // head unload time
  int srt; // step rate time
  uint8_t comBuf[8]; // uPD765 command args
  int comCnt; // arg count for command
  int comPos; // pos in comBuf
  uint8_t resBuf[8]; // uPD765 response
  int resCnt;
  int resPos;
  uint8_t sr0, sr1, sr2, sr3; // status registers

  /* BDI: last out to sysreg */
  uint8_t sysreg;

  /* used only in uPD765 emulator */
  FDCSector slst[64];
  int scnt;
};

typedef struct DiskHW_t DiskHW;
typedef struct DiskIF_t DiskIF;


// create disk interface with the given type
//   DIF_NONE
//   DIF_BDI
//   DIF_P3DOS
// it alwasy creates four floppy drives
// invalid types will become "none"
// default active drive is 0
// default mode is non-turbo
DiskIF *difCreate (int type);

// destroy disk interface
void difDestroy (DiskIF *dif);

// set (force) hardware type for the given interface
//   DIF_NONE
//   DIF_BDI
//   DIF_P3DOS
// invalid types will become "none"
void difSetHW (DiskIF *dif, int type);

// returns hardware type
//   DIF_NONE
//   DIF_BDI
//   DIF_P3DOS
int difGetHW (const DiskIF *dif);

// returns pointer to FDC struct (or NULL)
FDC *difGetFDC (DiskIF *dif);

// returns pointer to Floppy struct (or NULL)
// `driveidx` is [0..3]
Floppy *difGetFloppy (DiskIF *dif, int driveidx);

// returns pointer to Floppy struct (or NULL)
Floppy *difGetCurrentFloppy (DiskIF *dif);

// set "turbo mode"
void difSetTurbo (DiskIF *dif, int v);

// get "turbo mode"
int difGetTurbo (const DiskIF *dif);

// reset disk interface (call on machine reset)
void difReset (DiskIF *dif);

// tstates: tstates passed
// cpufreq: 3500000 for ZX Spectrum
int difCalcNSFromTStates (int tstates, int cpufreq);

// update disk interface (up to the given `ns`)
// call this in emulation loop
// `ns` is time passed from the last call (in nanoseconds)
// it can be calculated with `difCalcNSFromTStates()`
// sync disk interface before in/out, and on each frame
void difSync (DiskIF *dif, int ns);

// write value into FDC port
// WARNING: call only when FDC ports are active!
// returns `-1` if not an FDC port
// note that `0` means success!
int difOut (DiskIF *dif, int port, int val);

// write value into FDC port
// WARNING: call only when FDC ports are active!
// returns `-1` if not an FDC port
int difIn (DiskIF *dif, int port);

// utility function used by both FDCs
//void fdcUpdateCRC16 (FDC *fdc, uint8_t val);

#ifdef __cplusplus
}
#endif
#endif

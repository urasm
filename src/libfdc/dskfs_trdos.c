/*
 * WD1793/uPD765 emulator
 * Copyright (c) 2009-..., SAM style
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is furnished
 * to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
 * THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * changes by Ketmar // Invisible Vector
 *
 * Understanding is not required. Only obedience.
 * git commit: 84b4cb3f749ff3cb954a202b3c65d7b53ee08273
 */
#include "dskfs_all.h"
#include "dskfs_trdos.h"
#include "dskldr.h"


//**************************************************************************
//
// TR-DOS low level API
//
//**************************************************************************

//==========================================================================
//
//  flpIsDiskTRDOS
//
//==========================================================================
int flpIsDiskTRDOS (Floppy *flp) {
  if (!flp || !flp->insert) return 0;
  // trdos
  uint8_t fbuf[0x100];
  if (flpGetSectorData(flp, 0, 15, fbuf, 0x100) != 0) return 0;
  // at least 16 sectors
  if (flpGetSectorData(flp, 0, 9, fbuf, 0x100) != 0) return 0;
  return (fbuf[0xe7] == 0x10u);
}


//==========================================================================
//
//  flpFormatTRDTrack
//
//  build a single track 16x256 (TRDOS), sector data @bpos (4K)
//
//==========================================================================
int flpFormatTRDTrack (Floppy *flp, int tr, const void *bpos, size_t bpossize) {
  if (!flp || bpossize < 0 || tr < 0 || tr > 255) return FLPERR_SHIT;
  FDCSector lst[FLP_TRDOS_SECTORS_PER_TRACK];
  memset(lst, 0, sizeof(lst));
  const uint8_t *ppos = (const uint8_t *)bpos;
  if (!ppos) bpossize = 0;
  for (unsigned f = 0; f < FLP_TRDOS_SECTORS_PER_TRACK; ++f) {
    FDCSector *sc = &lst[f];
    sc->sec = (uint8_t)(f+1u);
    sc->type = 0xfbU;
    sc->crc = 0xffffU;
    sc->trk = ((tr&0xfe)>>1);
    sc->head = (tr&0x01 ? 1 : 0);
    sc->sz = 1; // 256 bytes
    if (bpossize > 0) {
      if (bpossize >= 256) {
        memcpy(sc->data, ppos, 256);
        ppos += 256;
        bpossize -= 256;
      } else {
        memcpy(sc->data, ppos, bpossize);
        memset(sc->data, 0, 256-bpossize);
        bpossize = 0;
      }
    } else {
      memset(sc->data, 0, 256);
    }
  }
  return flpFormatTracks(flp, tr, lst, FLP_TRDOS_SECTORS_PER_TRACK, FLP_FORMAT_TRACK_CALC_CRC);
}


//==========================================================================
//
//  flpFormatTRD
//
//  format whole disk as 2x84x16x256 and init as TRDOS
//
//==========================================================================
int flpFormatTRD (Floppy *flp) {
  if (!flp) return FLPERR_SHIT;
  flp->protect = 0;
  flp->doubleSide = 1;
  flp->trk80 = 1;
  flp->insert = 0;
  flp->changed = 0;
  for (int i = 1; i < 168; ++i) {
    int res = flpFormatTRDTrack(flp, i, NULL, 0);
    if (res != 0) return res;
  }
  // first track
  const uint8_t trd_8e0[32] = {
    0x00,0x00,0x01,0x16,0x00,0xf0,0x09,0x10,0x00,0x00,0x20,0x20,0x20,0x20,0x20,0x20,
    0x20,0x20,0x20,0x00,0x00,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x00,0x00,0x00,
  };
  uint8_t buf[0x1000];
  memset(buf, 0, 0x1000);
  memcpy(buf+0x8e0, trd_8e0, 0x20);
  int res = flpFormatTRDTrack(flp, 0, buf, sizeof(buf));
  if (res == FLPERR_OK) flp->insert = 1;
  return res;
}



//**************************************************************************
//
// TR-DOS utilities
//
//**************************************************************************

//==========================================================================
//
//  flpCreateFile
//
//==========================================================================
int flpCreateFile (Floppy *flp, TRFile *dsc) {
  if (flpDetectDiskType(flp) != FLP_DISK_TYPE_TRDOS) return FLPERR_SHIT;
  uint8_t fbuf[0x100];
  uint8_t files;
  uint16_t freesec;
  if (flpGetSectorData(flp, 0, 9, fbuf, 256) != FLPERR_OK) return FLPERR_SHIT;
  dsc->sec = fbuf[0xe1];
  dsc->trk = fbuf[0xe2];
  files = fbuf[0xe4];
  if (files > 127) return FLPERR_MANYFILES;
  ++files;
  fbuf[0xe4] = files;
  freesec = fbuf[0xe5]+(fbuf[0xe6]<<8);
  if (freesec < dsc->slen) return FLPERR_NOSPACE;
  freesec -= dsc->slen;
  fbuf[0xe5] = (freesec&0xff);
  fbuf[0xe6] = ((freesec&0xff00)>>8);
  fbuf[0xe1] += (dsc->slen&0x0f);
  fbuf[0xe2] += ((dsc->slen&0xf0)>>4);
  if (fbuf[0xe1] > 0x0f) {
    fbuf[0xe1] -= 0x10;
    ++fbuf[0xe2];
  }
  if (flpPutSectorData(flp, 0, 9, fbuf, 256) != FLPERR_OK) return FLPERR_SHIT;
  freesec = ((files&0xf0)>>4)+1;
  if (flpGetSectorData(flp, 0, freesec, fbuf, 256) != FLPERR_OK) return FLPERR_SHIT;
  memmove(fbuf+(((files-1)&0x0f)<<4), dsc, 16);
  flpPutSectorData(flp, 0, freesec, fbuf, 256);
  flp->changed = 1;
  return FLPERR_OK;
}


//==========================================================================
//
//  flpGetCatalogEntry
//
//==========================================================================
int flpGetCatalogEntry (Floppy *flp, TRFile *dst, int num) {
  uint8_t fbuf[0x100];
  int sec, pos;
  if (flpDetectDiskType(flp) != FLP_DISK_TYPE_TRDOS) return FLPERR_SHIT;
  if (num < 0 || num > 127) return FLPERR_MANYFILES;
  sec = ((num&0xf0)>>4); // sector
  pos = ((num&0x0f)<<4); // file number inside sector
  if (flpGetSectorData(flp, 0, sec+1, fbuf, 256) != FLPERR_OK) return FLPERR_NOSPACE;
  if (dst != NULL) memmove(dst, fbuf+pos, 16);
  return FLPERR_OK;
}


//==========================================================================
//
//  flpGetTRCatalog
//
//==========================================================================
int flpGetTRCatalog (Floppy *flp, TRFile *dst) {
  int cnt = 0;
  if (flpDetectDiskType(flp) == FLP_DISK_TYPE_TRDOS) {
    uint8_t *dpt = (uint8_t *)dst;
    uint8_t fbuf[0x100];
    for (int sc = 1; sc < 9; ++sc) {
      if (flpGetSectorData(flp, 0, sc, fbuf, 256) != FLPERR_OK) break;
      const uint8_t *ptr = fbuf;
      unsigned fc;
      for (fc = 0; fc < 16; ++fc) {
        if (*ptr == 0) break;
        if (dpt) {
          memmove(dpt, ptr, 16);
          dpt += 16;
        }
        ptr += 16;
        ++cnt;
      }
      if (fc < 16) break;
    }
  }
  return cnt;
}



//**************************************************************************
//
// 'boot' utilities
//
//**************************************************************************

//==========================================================================
//
//  flpHasBoot
//
//==========================================================================
int flpHasBoot (Floppy *flp) {
  if (flp != NULL && flpDetectDiskType(flp) == FLP_DISK_TYPE_TRDOS) {
    TRFile cat[128];
    int catSize = flpGetTRCatalog(flp, cat);
    for (int i = 0; i < catSize; ++i) {
      if (memcmp(cat[i].name, "boot    B", 9) == 0) return i;
    }
  }
  return FLPERR_SHIT;
}


//==========================================================================
//
//  flpSetBoot
//
//==========================================================================
int flpSetBoot (Floppy *flp, FILE *fl, int replace) {
  if (flp != NULL && flpDetectDiskType(flp) == FLP_DISK_TYPE_TRDOS) {
    int idx = flpHasBoot(flp);
    if (idx >= 0) {
      if (replace) return FLPERR_SHIT; //TODO
      return FLPERR_OK;
    }
    return dskLoadHoBeta(flp, fl);
  }
  return FLPERR_SHIT;
}


//==========================================================================
//
//  flpFindFirstBasic
//
//==========================================================================
int flpFindFirstBasic (Floppy *flp, TRFile *dst, int ffirst) {
  if (flp == NULL || flpDetectDiskType(flp) != FLP_DISK_TYPE_TRDOS) return FLPERR_SHIT;
  if (!flp->insert) return FLPERR_SHIT;
  if (ffirst < 0) ffirst = 0;
  TRFile fcb;
  for (; ffirst < FLP_TRDOS_CATALOG_MAX; ++ffirst) {
    if (flpGetCatalogEntry(flp, &fcb, ffirst) != 0) break;
    if (fcb.name[0] == 0) break; // end of directory
    if (fcb.name[0] == 1) continue; // deleted
    if (fcb.ext != 'B') continue;
    int nameok = 1;
    int seennspc = 0;
    for (unsigned f = 0; f < 8; ++f) {
      const uint8_t ch = (unsigned)(fcb.name[f]&0xffU);
      if (ch < 32 || ch == '"') { nameok = 0; break; }
      if (ch != 32) seennspc = 1;
    }
    if (!nameok || !seennspc) continue;
    if (dst) memcpy(dst, &fcb, sizeof(TRFile));
    return ffirst;
  }
  return FLPERR_SHIT;
}


//==========================================================================
//
//  flpHasAnyNonBootBasic
//
//==========================================================================
int flpHasAnyNonBootBasic (Floppy *flp, TRFile *dst) {
  if (flp == NULL || flpDetectDiskType(flp) != FLP_DISK_TYPE_TRDOS) return 0;
  if (!flp->insert) return 0;
  // check if we have any non-boot basic file
  TRFile fcb;
  int fidx = 0;
  for (;;) {
    fidx = flpFindFirstBasic(flp, &fcb, fidx);
    if (fidx < 0) break;
    if (memcmp(fcb.name, "boot    ", 8) == 0) { ++fidx; continue; }
    if (dst) memcpy(dst, &fcb, sizeof(TRFile));
    return 1;
  }
  return 0;
}


//==========================================================================
//
//  flpSetBootSimple
//
//==========================================================================
int flpSetBootSimple (Floppy *flp) {
  if (flp == NULL || flpDetectDiskType(flp) != FLP_DISK_TYPE_TRDOS) return FLPERR_SHIT;
  if (!flp->insert) return FLPERR_SHIT;
  // check if we have any non-boot basic file
  TRFile fcb;
  int fidx = 0;
  int seenBasic = 0;
  for (;;) {
    fidx = flpFindFirstBasic(flp, &fcb, fidx);
    if (fidx < 0) break;
    if (memcmp(fcb.name, "boot    ", 8) == 0) return FLPERR_SHIT; // nothing to do
    if (seenBasic) return FLPERR_SHIT; // too many basic files
    seenBasic = 1;
    ++fidx;
  }
  if (!seenBasic) return FLPERR_SHIT;

  // create simple autorun boot:
  // 10 RANDOMIZE USR VAL "15619":REM LOAD "file"

  uint8_t secBuf[256];
  memset(secBuf, 0, sizeof(secBuf));
  size_t scpos = 0;
  // line number
  secBuf[scpos++] = 0;
  secBuf[scpos++] = 10;
  // line size (will be fixed later)
  secBuf[scpos++] = 0;
  secBuf[scpos++] = 0;
  // line data
  secBuf[scpos++] = 0xf9U; // RANDOMIZE
  secBuf[scpos++] = 0xc0U; // USR
  secBuf[scpos++] = 0xb0U; // VAL
  secBuf[scpos++] = '"';
  secBuf[scpos++] = '1';
  secBuf[scpos++] = '5';
  secBuf[scpos++] = '6';
  secBuf[scpos++] = '1';
  secBuf[scpos++] = '9';
  secBuf[scpos++] = '"';
  secBuf[scpos++] = ':';
  secBuf[scpos++] = 0xeaU; // REM
  secBuf[scpos++] = ':';
  secBuf[scpos++] = 0xefU; // LOAD
  secBuf[scpos++] = '"';
  // put name
  size_t nlen = 8;
  while (nlen > 0 && fcb.name[nlen-1] == ' ') --nlen;
  if (nlen == 0) return FLPERR_SHIT; // just in case
  memcpy(secBuf+scpos, fcb.name, nlen); scpos += nlen;
  secBuf[scpos++] = '"';
  // line end
  secBuf[scpos++] = 13;
  // fix line size
  secBuf[2] = (scpos-4)&0xffU;
  secBuf[3] = ((scpos-4)>>8)&0xffU;
  // TR-DOS signature
  secBuf[scpos++] = 0x80;
  secBuf[scpos++] = 0xaa;
  // start line
  secBuf[scpos++] = 10;
  secBuf[scpos++] = 0;

  memcpy(fcb.name, "boot    ", 8);
  fcb.ext = 'B';
  // last 4 bytes are not in length
  fcb.lst = (scpos-4)&0xff;
  fcb.hst = ((scpos-4)>>8)&0xff;
  fcb.llen = (scpos-4)&0xff;
  fcb.hlen = ((scpos-4)>>8)&0xff;
  fcb.slen = 1; // one sector
  fcb.sec = fcb.trk = 0; // will be set in `flpCreateFile()`

  if (flpCreateFile(flp, &fcb) != FLPERR_OK) return FLPERR_SHIT;
  if (flpPutSectorData(flp, fcb.trk, fcb.sec+1, secBuf, 256) != FLPERR_OK) return FLPERR_SHIT;

  flp->changed = 1;
  return FLPERR_OK;
}


//==========================================================================
//
//  flpRemoveBoot
//
//  TODO
//
//==========================================================================
int flpRemoveBoot (Floppy *flp) {
  return FLPERR_SHIT; //TODO
}

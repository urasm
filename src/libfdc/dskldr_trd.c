/*
 * disk images I/O
 * coded by Ketmar // Invisible Vector
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 */
#include "dskldr.h"
#include "dskfs_all.h"


//==========================================================================
//
//  dskLoadTRD
//
//==========================================================================
int dskLoadTRD (Floppy *flp, FILE *fl) {
  uint8_t trkBuf[FLP_TRD_TRACK_SIZE], b;
  if (fl == NULL) return FLPERR_SHIT;
  if (fseek(fl, 0x8e7, SEEK_CUR) != 0) {
    libfdcMsg(LIBFDC_MSG_ERROR, "cannot seek to TRD header");
    return FLPERR_SHIT;
  }
  if (fread(&b, 1, 1, fl) != 1) {
    libfdcMsg(LIBFDC_MSG_ERROR, "cannot read TRD header signature byte");
    return FLPERR_SHIT;
  }
  if (b != 0x10) {
    // TRD signature byte not found
    libfdcMsg(LIBFDC_MSG_ERROR, "TRD signature byte not found");
    return FLPERR_SHIT;
  }
  if (fseek(fl, -0x8e8, SEEK_CUR) != 0) {
    libfdcMsg(LIBFDC_MSG_ERROR, "cannot seek to TRD start");
    return FLPERR_SHIT;
  }
  if (flpFormatTRD(flp) != FLPERR_OK) {
    libfdcMsg(LIBFDC_MSG_ERROR, "cannot format emulated floppy");
    return FLPERR_SHIT;
  }
  //FIXME: TODO: single-sided, 43
  for (int i = 0; i < FLP_MAX_TRACKS_80; ++i) {
    size_t rd = fread(trkBuf, 1, FLP_TRD_TRACK_SIZE, fl);
    if (rd < 1) {
      if (i == 0) {
        flpFormatTRD(flp);
        flp->changed = 0;
        flp->insert = 0;
        return FLPERR_SHIT;
      }
      break;
    }
    if (rd < FLP_TRD_TRACK_SIZE) memset(trkBuf+rd, 0, FLP_TRD_TRACK_SIZE-rd);
    if (flpFormatTRDTrack(flp, i, trkBuf, sizeof(trkBuf)) != FLPERR_OK) return FLPERR_SHIT;
    if (rd < FLP_TRD_TRACK_SIZE) break; // no more bytes in file anyway
  }
  flp->insert = 1;
  flp->changed = 0;
  return FLPERR_OK;
}


//==========================================================================
//
//  dskSaveTRD
//
//==========================================================================
int dskSaveTRD (Floppy *flp, FILE *fl) {
  if (!flp || !flp->insert) return FLPERR_SHIT;
  if (fl == NULL) return FLPERR_SHIT;
  for (int trk = 0; trk < 80*2; ++trk) {
    for (int sec = 1; sec < 17; ++sec) {
      uint8_t sdata[256];
      if (flpGetSectorData(flp, trk, sec, sdata, 256) != FLPERR_OK) return FLPERR_SHIT;
      if (fwrite(sdata, 256, 1, fl) != 1) return FLPERR_SHIT;
    }
  }
  flp->changed = 0;
  return FLPERR_OK;
}

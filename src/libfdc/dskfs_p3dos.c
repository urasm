/*
 * +3DOS filesystem API
 * Copyright (c) 2020 Ketmar Dark
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is furnished
 * to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
 * THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * coded by Ketmar // Invisible Vector
 *
 * Understanding is not required. Only obedience.
 */
#include "dskfs_all.h"
#include "dskfs_p3dos.h"
#include "dskldr.h"

//#define P3DSK_DEBUG_FILE_READER
//#define P3DSK_DEBUG_FILE_GROW
//#define P3DSK_DEBUG_FILE_WRITER

#define P3DSK_SECTORS_PER_TRACK  (9)

#define P3DSK_ASSERT(cond_)  do { \
  if (!(cond_)) { fprintf(stderr, "ASSERTION FAILED at file %s, line %d\n", __FILE__, __LINE__); abort(); } \
} while (0)

static const char *p3doshdrsign = "PLUS3DOS";


//==========================================================================
//
//  flpIsDiskP3DOS
//
//==========================================================================
int flpIsDiskP3DOS (Floppy *flp) {
  if (!flp || !flp->insert) return 0;
  P3DskGeometry geom;
  if (p3dskDetectGeom(flp, &geom) != 0) return 0;
  return 1;
}


//==========================================================================
//
//  p3dskFormatTrack
//
//  build a single track 9x512 (+3DOS)
//  `dsktype` is `P3DSK_XXX`
//
//==========================================================================
static int p3dskFormatTrack (Floppy *flp, int dsktype, int tr) {
  if (!flp || tr < 0 || tr > 255 || dsktype < 0 || dsktype >= P3DSK_MAX) return FLPERR_SHIT;
  // don't format second side for single-sided disks (but this is not an error)
  if (dsktype != P3DSK_PCW_DS && (tr&1) != 0) return FLPERR_OK;
  // first sector number
  int currsec = (dsktype == P3DSK_SYSTEM ? 65 : dsktype == P3DSK_DATA ? 193 : 1);
  FDCSector lst[P3DSK_SECTORS_PER_TRACK];
  memset(lst, 0, sizeof(lst));
  for (unsigned f = 0; f < P3DSK_SECTORS_PER_TRACK; ++f) {
    FDCSector *sc = &lst[f];
    sc->type = 0xfbU;
    sc->crc = 0xffffU;
    sc->trk = ((tr&0xfe)>>1);
    sc->head = (tr&0x01 ? 1 : 0);
    sc->sz = 2; // 512 bytes
    sc->sec = currsec++;
    memset(sc->data, 0, 512);
  }
  return flpFormatTracks(flp, tr, lst, P3DSK_SECTORS_PER_TRACK, FLP_FORMAT_TRACK_CALC_CRC);
}


//==========================================================================
//
//  p3dskFormatDisk
//
//  create +3DOS disk with file system
//  `dsktype` is `P3DSK_XXX`
//
//==========================================================================
int p3dskFormatDisk (Floppy *flp, int dsktype) {
  if (!flp || dsktype < 0 || dsktype >= P3DSK_MAX) return FLPERR_SHIT;

  flp->protect = 0;
  flp->doubleSide = 1;
  flp->trk80 = (dsktype == P3DSK_PCW_DS ? 1 : 0);
  flp->insert = 0;
  flp->changed = 0;

  const int maxtrk = (dsktype == P3DSK_PCW_DS ? 80 : 40)*2;
  const int firstsec = (dsktype == P3DSK_SYSTEM ? 65 : dsktype == P3DSK_DATA ? 193 : 1);

  //fprintf(stderr, "formatting: firstsec=%d; maxtrk=%d\n", firstsec, maxtrk);

  // format all tracks
  if (flpClearDisk(flp) != FLPERR_OK) return FLPERR_SHIT;
  for (int tr = 0; tr < maxtrk; ++tr) {
    int res = p3dskFormatTrack(flp, dsktype, tr);
    if (res != FLPERR_OK) {
      //fprintf(stderr, "cannot format track #%d\n", tr);
      return res;
    }
  }

  // write dpb
  if (dsktype == P3DSK_PCW_SS || dsktype == P3DSK_PCW_DS) {
    //fprintf(stderr, "writing dpb...\n");
    const uint8_t defdbp[2][10] = {
      { 0x00u, /* Disc type */
        0x00u, /* Disc geometry */
        0x28u, /* Tracks */
        0x09u, /* Sectors */
        0x02u, /* Sector size */
        0x01u, /* Reserved tracks */
        0x03u, /* ?Sectors per block */
        0x02u, /* ?Directory blocks */
        0x2Au, /* Gap length (R/W) */
        0x52u, /* Gap length (format) */
      },
      { 0x03u, /* Disc type */
        0x81u, /* Disc geometry */
        0x50u, /* Tracks */
        0x09u, /* Sectors */
        0x02u, /* Sector size */
        0x02u, /* Reserved tracks */
        0x04u, /* ?Sectors per block */
        0x04u, /* ?Directory blocks */
        0x2Au, /* Gap length (R/W) */
        0x52u, /* Gap length (format) */
      },
    };
    uint8_t *dpb = flpGetSectorDataPtr(flp, 0, firstsec);
    if (!dpb) return FLPERR_SHIT;
    memcpy(dpb, defdbp[dsktype == P3DSK_PCW_SS ? 0 : 1], 10);
    memset(dpb+10, 0, 512-10);
    if (flpFixSectorDataCRC(flp, 0, firstsec) < 0) return FLPERR_SHIT;
  }

  // write directory
  P3DiskInfo p3d;
  p3d.flp = flp;
  if (p3dskDetectGeom(p3d.flp, &p3d.geom) < 0) return FLPERR_SHIT;

  //fprintf(stderr, "filling directory...\n");
  for (int f = 0; f < p3d.geom.maxdirentries; ++f) {
    uint8_t *de = p3dskGetDirEntryPtr(&p3d, f);
    if (!de) return FLPERR_SHIT;
    memset(de, 0xE5u, 32); // mark as unused
  }

  // recalc checksums for all modified sectors
  //fprintf(stderr, "fixing directory checksums...\n");
  //FIXME: optimise this!
  int lastls = -1;
  for (int f = 0; f < p3d.geom.dirblocks; ++f) {
    int lsidx = p3dskCalcLogicalSectorForBlock(&p3d.geom, f, 0);
    if (lsidx < 0) return FLPERR_SHIT;
    if (lsidx != lastls) {
      int trkn, secn;
      if (p3dskLogicalSectorToPhysTS(&p3d.geom, lsidx, &trkn, &secn) != FLPERR_OK) return FLPERR_SHIT;
      if (flpFixSectorDataCRC(flp, trkn, secn) != FLPERR_OK) return FLPERR_SHIT;
      lastls = lsidx;
    }
  }

  flp->insert = 1;
  return FLPERR_OK;
}


//==========================================================================
//
//  p3dskDetectGeom
//
//  returns 0 on success, or <0 on error
//  on error, `geom` is undefined, but sides guaranteed to hold zero
//
//==========================================================================
int p3dskDetectGeom (Floppy *flp, P3DskGeometry *geom) {
  if (!geom) return FLPERR_SHIT;
  memset(geom, 0, sizeof(P3DskGeometry));

  if (!flp /*|| !flp->insert*/) return FLPERR_SHIT;
  int minsnum = 65535;
  int maxsnum = -1;

  for (int f = 0; f < 256; ++f) {
    int r = flpGetRawSectorR(flp, 0, f);
    if (r > 0) {
      if (minsnum > r) minsnum = r;
      if (maxsnum < r) maxsnum = r;
    }
  }
  //printf("minsec=%d; maxsec=%d\n", minsnum, maxsnum);
  if (maxsnum < 1 || minsnum > 255) return FLPERR_SHIT;
  if (maxsnum-minsnum < 8) return FLPERR_SHIT;

  if (minsnum == 193) {
    // amstrad cpc data only format
    // 180 blocks
    // 64 directory entries
    // 2 directory blocks
    geom->sides = 1;
    geom->cyls = 40;
    geom->sectors = P3DSK_SECTORS_PER_TRACK;
    geom->firstsector = 193;
    geom->secsize = 512;
    geom->successive = 0;
    geom->restracks = 0;
    geom->blocksize = 128u<<3;
    geom->dirblocks = 2;
    geom->rwgap = 42;
    geom->fmtgap = 82;
    geom->checksum = 0;
    geom->disktype = P3DSK_DATA;
  } else if (minsnum == 65) {
    // amstrad cpc system format
    // 170 blocks
    // 64 directory entries
    // 2 directory blocks
    geom->sides = 1;
    geom->cyls = 40;
    geom->sectors = P3DSK_SECTORS_PER_TRACK;
    geom->firstsector = 65;
    geom->secsize = 512;
    geom->successive = 0;
    geom->restracks = 2;
    geom->blocksize = 128u<<3;
    geom->dirblocks = 2;
    geom->rwgap = 42;
    geom->fmtgap = 82;
    geom->checksum = 0;
    geom->disktype = P3DSK_SYSTEM;
  } else {
    const uint8_t bsec_pcw180[10] = { 0, 0, 40u, 9u, 2u, 1u, 3u, 2u, 0x2Au, 0x52u };

    const uint8_t *bootsec = flpGetSectorDataPtr(flp, 0, minsnum);
    if (!bootsec) return FLPERR_SHIT;
    int fssize = flpGetRawSectorN(flp, 0, minsnum);
    if (fssize > 2) return FLPERR_SHIT; // sector is too big
    if (fssize < bootsec[4]) return FLPERR_SHIT; // sector size doesn't match
    //fprintf(stderr, "000 (%u)\n", bootsec[0]);

    int calcchecksum = 1;

    // alot of 0xE5s is 180k
    unsigned e5count = 0;
    while (e5count < 10 && bootsec[e5count] == 0xE5u) ++e5count;
    if (e5count >= 10) {
      bootsec = bsec_pcw180;
      calcchecksum = 0;
    } else {
      //checksum = bootsec[15];
      // detect PCW16 boot+root
      if (bootsec[0] == 0xe9U || bootsec[0] == 0xeaU) {
        if (memcmp(bootsec+0x2bu, "CP/M", 4) != 0 ||
            memcmp(bootsec+0x33u, "DSK", 3) != 0 ||
            memcmp(bootsec+0x7cu, "CP/M", 4) != 0)
        {
          return FLPERR_SHIT;
        }
        // real DPB is at 0x80
        bootsec += 0x80u;
        calcchecksum = 0;
      }
    }

    // the only disk types +3DOS knows, we don't need the whole CP/M spectrum
    if (bootsec[0] != 0 && bootsec[0] != 3) return FLPERR_SHIT;
    // check sector size
    if (bootsec[4] > 2) return FLPERR_SHIT;
    // check directory size
    if (bootsec[7] == 0) return FLPERR_SHIT;
    // reserved bits must be zero
    if (bootsec[1]&0x7cu) return FLPERR_SHIT;

    // sides
    switch (bootsec[1]&0x03u) {
      case 0: geom->sides = 1; break;
      case 1: geom->sides = 2; break;
      case 2: geom->sides = 2; geom->successive = 1; break;
      default: return FLPERR_SHIT;
    }

    /*k8: it seems that this should be set for 80-track disks
    if (bootsec[1]&0x80u) {
      fprintf(stderr, "WTF is DoubleTrack?!\n");
      //return FLPERR_SHIT;
    }
    */

    geom->cyls = bootsec[2];
    geom->sectors = bootsec[3];
    geom->secsize = 128u<<bootsec[4];
    // check block size
    if (bootsec[6] > 8) goto error;
    // check directory blocks
    if (bootsec[7] == 0) goto error;
    if (!geom->cyls || !geom->sectors || geom->cyls > 83 || geom->sectors > 0x1800/geom->secsize) goto error;
    geom->restracks = bootsec[5];
    geom->blocksize = 128u<<bootsec[6];
    geom->dirblocks = bootsec[7];
    geom->rwgap = bootsec[8];
    geom->fmtgap = bootsec[9];
    geom->firstsector = 1;
    geom->disktype = bootsec[0];
    geom->checksum = 0;

    if (calcchecksum) {
      uint8_t checksum = 0;
      for (unsigned f = 0; f < geom->secsize; ++f) checksum += bootsec[f];
      geom->checksum = (checksum == 3);
    }
  }

  // more sanity checks
  // reserved tracks check
  if (geom->restracks >= geom->cyls*geom->sides) goto error;

  geom->maxdirentries = geom->dirblocks*geom->blocksize/32u; // one dir entry is 32 bytes
  geom->maxblocks = ((geom->cyls*geom->sides-geom->restracks)*geom->sectors*geom->secsize)/geom->blocksize;

  // is directory too big?
  if (geom->dirblocks >= geom->maxblocks) goto error;

  return FLPERR_OK;

error:
  memset(geom, 0, sizeof(P3DskGeometry));
  return FLPERR_SHIT;
}


//==========================================================================
//
//  p3dskLogicalSectorToPhysTS
//
//==========================================================================
int p3dskLogicalSectorToPhysTS (P3DskGeometry *geom, int lsidx, int *outtrk, int *outsec) {
  if (!geom || lsidx < 0 || !geom->sides || lsidx > 16383) return FLPERR_SHIT;
  // calculate sector number, it is independed of interleaving
  const int secn = lsidx%geom->sectors+geom->firstsector;
  // convert to logical sector number (and skip reserved tracks)
  const int lstrk = lsidx/geom->sectors+geom->restracks;
  int head, cyl;
  if (!geom->successive) {
    // side-interleaved
    const uint8_t hmaskshift = geom->sides-1; // `sides` is 1 or 2
    head = lstrk&hmaskshift;
    cyl = lstrk>>hmaskshift;
  } else {
    // side-successive
    // alas, we have to use real division here
    head = lstrk/geom->cyls;
    cyl = lstrk%geom->cyls;
  }
  if (cyl >= geom->cyls) return FLPERR_SHIT;
  const int trkn = (cyl<<1)+head; // libfdc always numbers tracks like this, even for single-sided disks
  if (trkn > 86*2) return FLPERR_SHIT;
  if (outtrk) *outtrk = trkn;
  if (outsec) *outsec = secn;
  return FLPERR_OK;
}


//==========================================================================
//
//  p3dskGetLogicalSector
//
//  get logical sector data, or NULL on error
//  it is not const, but don't write it!
//
//==========================================================================
uint8_t *p3dskGetLogicalSector (P3DiskInfo *p3d, int lsidx) {
  if (!p3d) return NULL;
  Floppy *flp = p3d->flp;
  P3DskGeometry *geom = &p3d->geom;
  if (!flp) return NULL;
  int trkn, secn;
  if (p3dskLogicalSectorToPhysTS(geom, lsidx, &trkn, &secn) != FLPERR_OK) return NULL;
  //fprintf(stderr, "p3dskGetLogicalSector: lsidx=%d; lstrk=%d; trkn=%d; secn=%d\n", lsidx, lstrk, trkn, secn);
  return flpGetSectorDataPtr(flp, trkn, secn);
}


//==========================================================================
//
//  p3dskUpdateLogicalSectorCRC
//
//==========================================================================
int p3dskUpdateLogicalSectorCRC (P3DiskInfo *p3d, int lsidx) {
  if (!p3d) return FLPERR_SHIT;
  Floppy *flp = p3d->flp;
  P3DskGeometry *geom = &p3d->geom;
  if (!flp) return FLPERR_SHIT;
  int trkn, secn;
  int res = p3dskLogicalSectorToPhysTS(geom, lsidx, &trkn, &secn);
  if (res != FLPERR_OK) return res;
  return flpFixSectorDataCRC(flp, trkn, secn);
}


//==========================================================================
//
//  p3dskCalcLogicalSectorForBlock
//
//  calculate logical sector number for the given disk block
//
//==========================================================================
int p3dskCalcLogicalSectorForBlock (P3DskGeometry *geom, int blknum, int bofs) {
  if (!geom || !geom->sides || blknum < 0 ||
      blknum >= geom->maxblocks || bofs < 0 || bofs >= geom->blocksize)
  {
    return FLPERR_SHIT;
  }
  return (blknum*geom->blocksize+bofs)/geom->secsize;
}


//==========================================================================
//
//  p3dskCalcPhysSectorOffsetForBlock
//
//  calculate offset in physical sector for the given block offset
//
//==========================================================================
int p3dskCalcPhysSectorOffsetForBlock (P3DskGeometry *geom, int blknum, int bofs) {
  if (!geom || !geom->sides || blknum < 0 || blknum >= geom->maxblocks ||
      bofs < 0 || bofs >= geom->blocksize)
  {
    return FLPERR_SHIT;
  }
  return bofs%geom->secsize;
}


//==========================================================================
//
//  p3dskGetDirEntryPtr
//
//==========================================================================
uint8_t *p3dskGetDirEntryPtr (P3DiskInfo *p3d, int idx) {
  if (!p3d) return NULL;
  Floppy *flp = p3d->flp;
  P3DskGeometry *geom = &p3d->geom;
  if (!flp || !geom || !geom->sides || idx < 0 || idx >= geom->maxdirentries) return NULL;
  const int deblk = idx/(geom->blocksize/32);
  const int deofs = idx*32%geom->blocksize;
  const int lsidx = p3dskCalcLogicalSectorForBlock(geom, deblk, deofs);
  if (lsidx < 0) return NULL;
  const int sofs = p3dskCalcPhysSectorOffsetForBlock(geom, deblk, deofs);
  if (sofs < 0) return NULL;
  //fprintf(stderr, "p3dskGetDirEntryPtr: idx=%d; lsidx=%d; deblk=%d; deofs=%d; sofs=%d\n", idx, lsidx, deblk, deofs, sofs);
  uint8_t *data = p3dskGetLogicalSector(p3d, lsidx);
  if (!data) return NULL;
  return data+sofs;
}


//==========================================================================
//
//  p3dskUpdateDirEntryCRC
//
//==========================================================================
int p3dskUpdateDirEntryCRC (P3DiskInfo *p3d, int idx) {
  if (!p3d) return FLPERR_SHIT;
  Floppy *flp = p3d->flp;
  P3DskGeometry *geom = &p3d->geom;
  if (!flp || !geom || !geom->sides || idx < 0 || idx >= geom->maxdirentries) return FLPERR_SHIT;
  const int deblk = idx/(geom->blocksize/32);
  const int deofs = idx*32%geom->blocksize;
  const int lsidx = p3dskCalcLogicalSectorForBlock(geom, deblk, deofs);
  if (lsidx < 0) return FLPERR_SHIT;
  const int sofs = p3dskCalcPhysSectorOffsetForBlock(geom, deblk, deofs);
  if (sofs < 0) return FLPERR_SHIT;
  return p3dskUpdateLogicalSectorCRC(p3d, lsidx);
}


//==========================================================================
//
//  p3dskIsValidDirEntry
//
//==========================================================================
static int p3dskIsValidDirEntry (const uint8_t *de) {
  if (!de) return 0;
  // 0-15: user #n file
  // 16-31: user #n for P2DOS, password extent for CP/M 3+
  // 32: disk label
  // 33: time stamp
  // 0xE5: unused
  // users are not supported
  if (de[0] && de[0] != 0xE5u) return 0;
  for (unsigned f = 1; f < 12; ++f) {
    const uint8_t ch = de[f]&0x7fU;
    if (ch < 32 || strchr("<>.,;:=?*[]", (char)ch)) return 0;
  }
  return 1;
}


//==========================================================================
//
//  p3dskReadBlock
//
//  returns 0 on success, <0 on error
//  make sure that `dest` is at least `geom->blocksize` bytes
//
//==========================================================================
int p3dskReadBlock (P3DiskInfo *p3d, int blkidx, void *dest) {
  if (!p3d) return FLPERR_SHIT;
  Floppy *flp = p3d->flp;
  P3DskGeometry *geom = &p3d->geom;
  if (!flp || !geom || !flp->insert || !geom->sides || !dest) return FLPERR_SHIT;
  if (blkidx < 0 || blkidx >= geom->maxblocks) return FLPERR_SHIT;
  uint8_t *dp = (uint8_t *)dest;
  const uint16_t secsize = geom->secsize; // cache it
  const uint16_t blksize = geom->blocksize; // cache it
  uint16_t bytesleft = blksize;
  for (unsigned bofs = 0; bofs < blksize; bofs += secsize) {
    // calculate logical sector
    const int lsidx = p3dskCalcLogicalSectorForBlock(geom, blkidx, (int)bofs);
    if (lsidx < 0) return FLPERR_SHIT;
    // calculate physical sector offset
    int psofs = p3dskCalcPhysSectorOffsetForBlock(geom, blkidx, (int)bofs);
    if (psofs < 0) return FLPERR_SHIT;
    // read logical sector
    const uint8_t *secdata = p3dskGetLogicalSector(p3d, lsidx);
    if (secdata == NULL) return FLPERR_SHIT;
    // copy data
    if (secsize >= bytesleft) {
      // last chunk
      memcpy(dp, secdata, bytesleft);
      return FLPERR_OK;
    }
    // not a last chuck
    memcpy(dp, secdata, secsize);
    dp += secsize;
    bytesleft -= secsize;
  }
  P3DSK_ASSERT(bytesleft == 0);
  return FLPERR_OK;
}



//**************************************************************************
//
// +3DOS filename operations
//
//**************************************************************************

//==========================================================================
//
//  cpmUpcase
//
//==========================================================================
static __attribute__((const)) __attribute__((always_inline))
inline char cpmUpcase (char ch) {
  return (ch >= 'a' && ch <= 'z' ? ch-32 : ch);
}


//==========================================================================
//
//  cpmNameEqu
//
//==========================================================================
static int cpmNameEqu (const void *n0, const void *n1) {
  const uint8_t *p0 = (const uint8_t *)n0;
  const uint8_t *p1 = (const uint8_t *)n1;
  for (unsigned f = 0; f < 11; ++f, ++p0, ++p1) {
    if ((p0[0]&0x7fU) != (p1[0]&0x7fU)) return 0;
  }
  return 1;
}


//==========================================================================
//
//  cpmNameEquCI
//
//==========================================================================
static int cpmNameEquCI (const void *n0, const void *n1) {
  const uint8_t *p0 = (const uint8_t *)n0;
  const uint8_t *p1 = (const uint8_t *)n1;
  for (unsigned f = 0; f < 11; ++f, ++p0, ++p1) {
    const char c0 = cpmUpcase((char)(p0[0]&0x7fU));
    const char c1 = cpmUpcase((char)(p1[0]&0x7fU));
    if (c0 != c1) return 0;
  }
  return 1;
}


//==========================================================================
//
//  p3dskCheckNamePart
//
//==========================================================================
static __attribute__((const)) const char *p3dskCheckNamePart (const char *name, size_t maxlen) {
  if (!name || !name[0]) return NULL;
  while (*name) {
    uint8_t ch = (uint8_t)(name[0]&0xffU);
    if (ch < 32 || ch > 127) return NULL;
    if (ch == '.' || ch == 32) break; // allow trailing spaces
    if (strchr("<>,;:=?*[]", (char)ch)) return NULL;
    if (maxlen == 0) return NULL;
    ++name;
    --maxlen;
  }
  while (*name == ' ') ++name;
  return name;
}


//==========================================================================
//
//  p3dskIsValidFileName
//
//==========================================================================
int p3dskIsValidFileName (const char *name) {
  if (!name || !name[0]) return 0;
  // check name
  //fprintf(stderr, "  000:<%s>\n", name);
  name = p3dskCheckNamePart(name, 8);
  //fprintf(stderr, "  001:<%s>\n", name);
  if (!name) return 0;
  if (!name[0]) return 1;
  if (name[0] != '.') return 0; // can happen due to space skipping
  ++name;
  if (!name[0]) return 1;
  //fprintf(stderr, "  002:<%s>\n", name);
  name = p3dskCheckNamePart(name, 3);
  //fprintf(stderr, "  003:<%s>\n", name);
  if (!name) return 0;
  return (name[0] == 0);
}


//==========================================================================
//
//  p3dskNormaliseFileName
//
//==========================================================================
int p3dskNormaliseFileName (char *dest, const char *src) {
  if (!dest) return FLPERR_SHIT;
  *dest = 0;
  if (!src) return FLPERR_SHIT;
  if (!p3dskIsValidFileName(src)) return FLPERR_SHIT;
  // `src` is guaranteed to fit
  char tmpbuf[13];
  size_t tpos = 0;
  while (*src) {
    uint8_t ch = (uint8_t)((*src++)&0xffU);
    P3DSK_ASSERT(ch >= 32 && ch <= 127);
    if (ch == '.') { --src; break; }
    if (ch == 32) break;
    P3DSK_ASSERT(tpos < 12);
    tmpbuf[tpos++] = (char)ch;
  }
  while (*src == ' ') ++src;
  if (*src == '.') {
    ++src;
    tmpbuf[tpos++] = '.';
    while (*src) {
      uint8_t ch = (uint8_t)((*src++)&0xffU);
      P3DSK_ASSERT(ch != '.' && ch >= 32 && ch <= 127);
      if (ch == 32) break;
      P3DSK_ASSERT(tpos < 12);
      tmpbuf[tpos++] = (char)ch;
    }
  }
  P3DSK_ASSERT(tpos <= 12);
  tmpbuf[tpos] = 0;
  strcpy(dest, tmpbuf);
  return FLPERR_OK;
}


//==========================================================================
//
//  p3dskBuildCPMName
//
//  `src` must be valid file name
//  `dest` is 11-byte buffer
//
//==========================================================================
static int p3dskBuildCPMName (void *dest, const char *src) {
  if (!dest) return FLPERR_SHIT;
  if (!src) return FLPERR_SHIT;
  if (!p3dskIsValidFileName(src)) return FLPERR_SHIT;
  // `src` is guaranteed to fit
  char tmpbuf[11];
  size_t tpos = 0;
  while (*src) {
    uint8_t ch = (uint8_t)((*src++)&0xffU);
    P3DSK_ASSERT(ch >= 32 && ch <= 127);
    if (ch == '.') { --src; break; }
    if (ch == 32) break;
    P3DSK_ASSERT(tpos < 11);
    tmpbuf[tpos++] = (char)ch;
  }
  //fprintf(stderr, "::<%s>\n", src);
  // pad with spaces
  while (tpos < 8) tmpbuf[tpos++] = ' ';
  // extension
  while (*src == ' ') ++src;
  if (*src == '.') {
    ++src;
    while (*src) {
      uint8_t ch = (uint8_t)((*src++)&0xffU);
      P3DSK_ASSERT(ch != '.' && ch >= 32 && ch <= 127);
      if (ch == 32) break;
      P3DSK_ASSERT(tpos < 11);
      tmpbuf[tpos++] = (char)ch;
    }
  }
  // pad with spaces
  while (tpos < 11) tmpbuf[tpos++] = ' ';
  P3DSK_ASSERT(tpos == 11);
  memcpy(dest, tmpbuf, 11);
  return FLPERR_OK;
}


//==========================================================================
//
//  p3dskIsGlobMask
//
//==========================================================================
int p3dskIsGlobMask (const char *str) {
  if (!str) return 0;
  for (; *str; ++str) {
    const char ch = *str;
    if (ch == '?' || ch == '*' || ch == '[') return 1;
  }
  return 0;
}


//==========================================================================
//
//  p3dskBuildNameDirEntry
//
//  `destbuf` must be at least 13 bytes
//  will never write more than 13 bytes
//
//==========================================================================
static int p3dskBuildNameDirEntry (char destbuf[], const uint8_t *de) {
  if (!de || !destbuf) return FLPERR_SHIT;
  if (!p3dskIsValidDirEntry(de)) return FLPERR_SHIT;
  // copy name
  memcpy(destbuf, de+1, 8);
  // normalize chars
  for (unsigned f = 0; f < 8; ++f) destbuf[f] &= 0x7fU;
  // remove trailing spaces
  size_t epos = 8;
  while (epos > 0 && destbuf[epos-1] == ' ') --epos;
  // put dot (it is always there)
  destbuf[epos++] = '.';
  // copy extension
  memcpy(destbuf+epos, de+9, 3);
  // normalize chars
  for (unsigned f = 0; f < 3; ++f) destbuf[epos+f] &= 0x7fU;
  // remove trailing spaces
  epos += 3;
  while (epos > 0 && destbuf[epos-1] == ' ') --epos;
  destbuf[epos] = 0;
  return FLPERR_OK;
}


//==========================================================================
//
//  p3dskCopyStrN
//
//==========================================================================
static int p3dskCopyStrN (char destbuf[], const void *src, size_t nlen) {
  if (!destbuf) return FLPERR_SHIT;
  if (nlen == 0) { destbuf[0] = 0; return FLPERR_OK; }
  if (!src) return FLPERR_SHIT;
  // copy it
  memcpy(destbuf, src, nlen);
  // normalize chars
  for (unsigned f = 0; f < nlen; ++f) destbuf[f] &= 0x7fU;
  // remove trailing spaces
  /*
  size_t epos = nlen;
  while (epos > 0 && destbuf[epos-1] == ' ') --epos;
  destbuf[epos] = 0;
  */
  destbuf[nlen] = 0;
  return FLPERR_OK;
}


//==========================================================================
//
//  p3dskGlobMatch
//
//==========================================================================
int p3dskGlobMatch (const char *str, const char *pat, unsigned flags) {
  int hasMatch, inverted;
  size_t patpos;
  int star = 0;
  if (!pat) pat = "";
  if (!str) str = "";
loopStart:
  patpos = 0;
  for (size_t i = 0; str[i]; ++i) {
    char sch = str[i];
    if ((flags&P3DSK_GLOB_CASE_SENSITIVE) == 0) sch = cpmUpcase(sch);
    switch (pat[patpos++]) {
      case 0: goto starCheck;
      case '?': // match anything (except '.' for file names)
        if (sch == '.' && (flags&P3DSK_GLOB_STRING) == 0) goto starCheck;
        break;
      case '*':
        star = 1;
        str += i;
        pat += patpos;
        while (*pat == '*') ++pat;
        if (*pat == 0) return 1;
        goto loopStart;
      case '[':
        hasMatch = inverted = 0;
        if (pat[patpos] == '^') { inverted = 1; ++patpos; }
        while (pat[patpos] && pat[patpos] != ']') {
          char c0 = pat[patpos++];
          if ((flags&P3DSK_GLOB_CASE_SENSITIVE) == 0) c0 = cpmUpcase(c0);
          char c1;
          if (pat[patpos] == '-') {
            // char range
            ++patpos; // skip '-'
            c1 = pat[patpos++];
            if (!c1) {
              --patpos;
              c1 = c0;
            }
            if ((flags&P3DSK_GLOB_CASE_SENSITIVE) == 0) c1 = cpmUpcase(c1);
          } else {
            c1 = c0;
          }
          // dot cannot be matched for filenames
          if ((flags&P3DSK_GLOB_STRING) == 0) {
            if (c0 == '.') ++c0;
            if (c1 == '.') --c1;
          }
          if (!hasMatch && (uint8_t)(sch&0xffU) >= (uint8_t)(c0&0xffU) && (uint8_t)(sch&0xffU) <= (uint8_t)(c1&0xffU)) hasMatch = 1;
        }
        if (pat[patpos] == ']') ++patpos;
        if (inverted) hasMatch = !hasMatch;
        if (!hasMatch) goto starCheck;
        break;
      default:
        if ((flags&P3DSK_GLOB_CASE_SENSITIVE) == 0) {
          if (sch != cpmUpcase(pat[patpos-1])) goto starCheck;
        } else {
          if (sch != pat[patpos-1]) goto starCheck;
        }
        break;
    }
  }
  pat += patpos;
  while (*pat == '*') ++pat;
  if (*pat) {
    // special handling for file names
    //if ((flags&P3DSK_GLOB_STRING) == 0 && pat[0] == '.' && !pat[1]) return 1;
    return 0;
  }
  // special handling for file names
  return 1;

starCheck:
   if (!star) return 0;
   if (*str) ++str;
   goto loopStart;
}



//**************************************************************************
//
// +3DOS directory searching
//
//**************************************************************************

//==========================================================================
//
//  p3dskFindInit
//
//==========================================================================
int p3dskFindInit (P3DiskInfo *p3d, P3DskFileInfo *nfo, const char *mask) {
  if (!p3d || !p3d->flp || !p3d->geom.sides || !p3d->flp->insert) return FLPERR_SHIT;
  if (!mask[0]) return FLPERR_BADMASK;
  const size_t masklen = strlen(mask);
  if (masklen >= sizeof(nfo->mask)-2u) return FLPERR_BADMASK;
  memset(nfo, 0, sizeof(P3DskFileInfo));
  strcpy(nfo->mask, mask);
  if (!strchr(nfo->mask, '.')) strcat(nfo->mask, ".");
  nfo->p3d = p3d;
  return FLPERR_OK;
}


//==========================================================================
//
//  p3dskCalcFileSize
//
//  <0: error
//
//==========================================================================
static int p3dskCalcFileSize (P3DiskInfo *p3d, const void *namebuf/*[11]*/, uint16_t currextidx) {
  if (!p3d) return FLPERR_SHIT;
  Floppy *flp = p3d->flp;
  P3DskGeometry *geom = &p3d->geom;
  if (!flp || !geom || !flp->insert || !geom->sides || !namebuf) return FLPERR_SHIT;
  int fsize = 0;
  uint16_t nenum = 0;
  for (;;) {
    int found = 0;
    for (unsigned f = 0; f < geom->maxdirentries; ++f) {
      const uint8_t *de = p3dskGetDirEntryPtr(p3d, currextidx);
      currextidx = (currextidx+1)%geom->maxdirentries;
      if (!de) return FLPERR_SHIT;
      if (de[0] != 0) continue; // allow only USER 0 files
      // get extent number
      // bit 5-7 of Xl are 0, bit 0-4 store the lower bits of the extent number.
      // bit 6 and 7 of Xh are 0, bit 0-5 store the higher bits of the extent number.
      if (de[12]&0x70u) continue; // check bits 5-7
      if (de[14]&0xc0u) continue; // check bits 6-7
      const uint16_t eidx = de[12]|(uint16_t)(de[14]<<5);
      //fprintf(stderr, "ce=%u; eidx=%u; nenum=%u; [%.*s]  [%.*s]\n", f, eidx, nenum, 11, (const char *)namebuf, 11, (const char *)(de+1));
      if (eidx != nenum) continue;
      // compare name
      if (cpmNameEqu(de+1, namebuf) == 0) continue;
      //fprintf(stderr, "  HIT!\n");
      // calc size
      uint16_t rn = de[15]*128;
      if (geom->maxblocks < 256) {
        // 8-bit block numbers
        if (rn/geom->blocksize > 16) return FLPERR_SHIT;
      } else {
        // 16-bit block numbers
        if (rn/geom->blocksize > 8) return FLPERR_SHIT;
      }
      // +3DOS is not using this, but why not?
      if (de[13] && de[13] <= 128 && de[13] <= rn) rn -= 128-de[13];
      fsize += rn; // Bc is not used by +3DOS
      ++nenum;
      found = 1;
    }
    if (!found) break;
  }
  return fsize;
}


//==========================================================================
//
//  p3dskFindNext
//
//==========================================================================
int p3dskFindNext (P3DskFileInfo *nfo) {
  if (!nfo || !nfo->p3d) return FLPERR_SHIT;
  Floppy *flp = nfo->p3d->flp;
  P3DskGeometry *geom = &nfo->p3d->geom;
  if (!flp || !geom || !flp->insert || !geom->sides) return FLPERR_SHIT;
  if (!nfo->mask[0]) return FLPERR_SHIT;
  for (; nfo->nextextent < geom->maxdirentries; ++nfo->nextextent) {
    const uint8_t *de = p3dskGetDirEntryPtr(nfo->p3d, nfo->nextextent);
    if (!de) return FLPERR_SHIT;
    // ignore non-first extents
    if (de[12]|de[14]) continue;
    //FIXME: return error?
    if (de[0] != 0) continue; // allow only USER 0 files
    //FIXME: return error?
    if (!p3dskIsValidDirEntry(de)) continue; // this file entry doesn't look valid, skip it
    memset(nfo->name, 0, sizeof(nfo->name));
    memset(nfo->nameonly, 0, sizeof(nfo->nameonly));
    memset(nfo->extonly, 0, sizeof(nfo->extonly));
    if (p3dskBuildNameDirEntry(nfo->name, de) != FLPERR_OK) return FLPERR_SHIT;
    if (p3dskGlobMatch(nfo->name, nfo->mask, P3DSK_GLOB_DEFAULT) == 0) continue;
    // remove trailing dot
    const size_t nlen = strlen(nfo->name);
    if (nlen && nfo->name[nlen-1] == '.') nfo->name[nlen-1] = 0;
    // save name as-is
    if (p3dskCopyStrN(nfo->nameonly, de+1, 8) != FLPERR_OK) return FLPERR_SHIT;
    if (p3dskCopyStrN(nfo->extonly, de+9, 3) != FLPERR_OK) return FLPERR_SHIT;
    // i found her!
    // get attrs
    nfo->readonly = (de[9+0]&0x80u ? 1 : 0);
    nfo->system = (de[9+1]&0x80u ? 1 : 0);
    nfo->archive = (de[9+2]&0x80u ? 1 : 0);
    // misc info
    nfo->firstextent = nfo->nextextent;
    // file size
    const int fsize = p3dskCalcFileSize(nfo->p3d, de+1, nfo->firstextent);
    if (fsize < 0) return FLPERR_SHIT;
    nfo->size = (unsigned)fsize;
    // continue from the next extent
    ++nfo->nextextent;
    // return success flag
    return 1;
  }
  return 0; // no more files
}



//**************************************************************************
//
// +3DOS high-level directory operations
//
//**************************************************************************

//==========================================================================
//
//  p3dskDeleteFile
//
//  delete file
//  returns error code (FLPERR_NOFILE means that file not found)
//
//==========================================================================
int p3dskDeleteFiles (P3DiskInfo *p3d, const char *name) {
  // find file
  if (!p3d) return FLPERR_SHIT;
  Floppy *flp = p3d->flp;
  P3DskGeometry *geom = &p3d->geom;
  if (!flp || !geom || !flp->insert || !geom->sides || flp->protect) return FLPERR_SHIT;
  if (!name || !name[0]) return FLPERR_NOFILE;
  // check for glob mask
  char pdfname[13];
  if (!p3dskIsGlobMask(name)) {
    if (!p3dskIsValidFileName(name)) return FLPERR_NOFILE;
    if (p3dskBuildCPMName(pdfname, name) != FLPERR_OK) return FLPERR_SHIT;
  } else {
    pdfname[0] = 0; // "not a file name" flag
  }
  int wasfound = 0;
  // walk directory, fill all file directory entries with 0xe5
  for (unsigned dirext = 0; dirext < p3d->geom.maxdirentries; ++dirext) {
    uint8_t *de = p3dskGetDirEntryPtr(p3d, dirext);
    if (!de) return FLPERR_SHIT;
    //FIXME: return error?
    if (de[0] != 0 && de[0] != 33) continue; // allow only USER 0 files and time stamps
    //FIXME: return error?
    if (!p3dskIsValidDirEntry(de)) continue; // this file entry doesn't look valid, skip it
    // compare with a glob mask, or with a name
    if (pdfname[0]) {
      // name
      #if 0
      char tmpbuf[11];
      memcpy(tmpbuf, de+1, 11);
      for (unsigned ff = 0; ff < 11; ++ff) tmpbuf[ff] &= 0x7f;
      fprintf(stderr, "  de:<%.11s>  n:<%.11s>\n", tmpbuf, pdfname);
      #endif
      if (cpmNameEquCI(pdfname, de+1) == 0) continue;
    } else {
      // compare with a glob mask (reuse `pdfname`)
      memset(pdfname, 0, sizeof(pdfname));
      if (p3dskBuildNameDirEntry(pdfname, de) != FLPERR_OK) return FLPERR_SHIT;
      const int equ = p3dskGlobMatch(pdfname, name, P3DSK_GLOB_DEFAULT);
      pdfname[0] = 0;
      if (!equ) continue;
    }
    // i found her!
    wasfound = 1;
    // erase directory entry
    memset(de, 0xE5u, 32);
    // update sector crc
    p3dskUpdateDirEntryCRC(p3d, dirext);
  }
  return (wasfound ? FLPERR_OK : FLPERR_NOFILE);
}



//**************************************************************************
//
// +3DOS high-level file i/o operations
//
//**************************************************************************

//==========================================================================
//
//  p3dskOpenFile
//
//  find file and open it for reading/writing
//  returns error code
//
//==========================================================================
int p3dskOpenFile (P3DiskInfo *p3d, P3DskFileInfo *nfo, const char *name) {
  if (!nfo || !p3d) return FLPERR_SHIT;
  if (!p3dskIsValidFileName(name)) return FLPERR_NOFILE;
  char pdfname[13];
  if (p3dskNormaliseFileName(pdfname, name) != FLPERR_OK) return FLPERR_NOFILE;
  //fprintf(stderr, "<%s> : <%s>\n", pdfname, name);
  if (p3dskFindInit(p3d, nfo, pdfname) != FLPERR_OK) return FLPERR_NOFILE;
  int res = p3dskFindNext(nfo);
  //fprintf(stderr, "<%s> : <%s>; res=%d\n", pdfname, name, res);
  if (res < 0) return res;
  if (res == 0) return FLPERR_NOFILE;
  return FLPERR_OK;
}


//==========================================================================
//
//  p3dskOpenFile
//
//  create empty +3DOS file
//  will fail if the given file already exists
//  returns error code
//
//==========================================================================
int p3dskCreateFile (P3DiskInfo *p3d, const char *name) {
  if (!p3d || !name || !name[0]) return FLPERR_SHIT;
  if (!p3d->flp || p3d->flp->protect) return FLPERR_SHIT;
  if (!p3dskIsValidFileName(name)) return FLPERR_NOFILE;
  char pdfname[13];
  if (p3dskNormaliseFileName(pdfname, name) != FLPERR_OK) return FLPERR_SHIT;
  P3DskFileInfo nfo;
  if (p3dskFindInit(p3d, &nfo, pdfname) != FLPERR_OK) return FLPERR_SHIT;
  int res = p3dskFindNext(&nfo);
  if (res < 0) return res;
  if (res != 0) return FLPERR_FILEEXIST;
  char cpmname[11];
  if (p3dskBuildCPMName(cpmname, pdfname) != FLPERR_OK) return FLPERR_SHIT;
  // find empty directory entry
  for (unsigned f = 0; f < p3d->geom.maxdirentries; ++f) {
    uint8_t *de = p3dskGetDirEntryPtr(p3d, (int)f);
    if (!de) return FLPERR_SHIT;
    if (de[0] <= 15) continue; // USER n files, preserve
    // this is either unused, or timestamp
    // build new file extent
    memset(de, 0, 32);
    de[0] = 0; // always USER 0
    // set name
    memcpy(de+1, cpmname, 11);
    return p3dskUpdateDirEntryCRC(p3d, (int)f);
  }
  return FLPERR_MANYFILES;
}


////////////////////////////////////////////////////////////////////////////////
// file block walker
typedef struct {
  P3DiskInfo *p3d;
  // wanted file extent index
  uint16_t nenum;
  // extent to start scanning from
  uint16_t nextextent;
  // file name, to detect file extents
  char namebuf[11];
  // read file blocks
  uint16_t blocks[16];
  // number of valid blocks in `blocks` array
  unsigned blocksused;
  uint32_t extbytes; // bytes in this extent
} P3DskExtentWalker;


//==========================================================================
//
//  p3dskInitExtentWalker
//
//  returns error code
//
//==========================================================================
static int p3dskInitExtentWalker (P3DskExtentWalker *wlk, const P3DskFileInfo *nfo) {
  if (wlk) memset(wlk, 0, sizeof(P3DskExtentWalker));
  if (!nfo || !nfo->p3d || !wlk) return FLPERR_SHIT;
  Floppy *flp = nfo->p3d->flp;
  P3DskGeometry *geom = &nfo->p3d->geom;
  if (!flp || !geom || !flp->insert || !geom->sides) return FLPERR_SHIT;
  if (!nfo->mask[0] || !nfo->name[0]) return FLPERR_SHIT;
  // init walker
  wlk->p3d = nfo->p3d;
  wlk->nenum = 0;
  wlk->nextextent = nfo->firstextent;
  wlk->blocksused = 0;
  // build name buffer, so we can look for extents faster
  memcpy(wlk->namebuf, nfo->nameonly, 8);
  memcpy(wlk->namebuf+8, nfo->extonly, 3);
  return FLPERR_OK;
}


//==========================================================================
//
//  p3dskNextExtent
//
//  returns error code, 0 if no more extents, >0 if next extent found
//
//==========================================================================
static int p3dskNextExtent (P3DskExtentWalker *wlk) {
  if (!wlk || !wlk->p3d) return FLPERR_SHIT;
  Floppy *flp = wlk->p3d->flp;
  P3DskGeometry *geom = &wlk->p3d->geom;
  if (!flp || !geom || !flp->insert || !geom->sides || !wlk->namebuf[0]) return FLPERR_SHIT;
  if (wlk->nextextent >= geom->maxdirentries) return 0; // no more
  // find extent
  for (unsigned f = 0; f < geom->maxdirentries; ++f) {
    const uint8_t *de = p3dskGetDirEntryPtr(wlk->p3d, wlk->nextextent);
    if (!de) return FLPERR_SHIT;
    wlk->nextextent = (wlk->nextextent+1)%geom->maxdirentries;
    if (de[0] != 0) continue; // allow only USER 0 files
    // get extent number
    // bit 5-7 of Xl are 0, bit 0-4 store the lower bits of the extent number.
    // bit 6 and 7 of Xh are 0, bit 0-5 store the higher bits of the extent number.
    if (de[12]&0x70u) continue; // check bits 5-7
    if (de[14]&0xc0u) continue; // check bits 6-7
    const uint16_t eidx = de[12]|(uint16_t)(de[14]<<5);
    if (eidx != wlk->nenum) continue;
    // compare name
    if (cpmNameEqu(de+1, wlk->namebuf) == 0) continue;
    // good extent; advance extent sequence number
    ++wlk->nenum;
    // get bytes in this extent
    uint16_t rn = de[15]*128;
    // get block list
    wlk->blocksused = 0;
    if (geom->maxblocks < 256) {
      // 8-bit block numbers
      if (rn/geom->blocksize > 16) return FLPERR_SHIT;
      const unsigned blkused = (rn+geom->blocksize-1u)/geom->blocksize;
      for (unsigned bc = 0; bc < blkused; ++bc) {
        uint16_t bnum = de[16+bc];
        if (bnum) {
          if (bnum < geom->dirblocks || bnum >= geom->maxblocks) return FLPERR_SHIT;
        }
        wlk->blocks[wlk->blocksused++] = bnum;
      }
    } else {
      // 16-bit block numbers
      if (rn/geom->blocksize > 8) return FLPERR_SHIT;
      const unsigned blkused = (rn+geom->blocksize-1u)/geom->blocksize;
      for (unsigned bc = 0; bc < blkused; ++bc) {
        uint16_t bnum = de[16+(bc<<1)]|((uint16_t)(de[17+(bc<<1)]));
        if (bnum) {
          if (bnum < geom->dirblocks || bnum >= geom->maxblocks) return FLPERR_SHIT;
        }
        wlk->blocks[wlk->blocksused++] = bnum;
      }
    }
    // +3DOS is not using this, but why not?
    if (de[13] && de[13] <= 128 && de[13] <= rn) rn -= 128-de[13];
    // clamp bytes in this extent
    if (rn > wlk->blocksused*geom->blocksize) rn = wlk->blocksused*geom->blocksize;
    wlk->extbytes = rn;
    // check for possible EOF
    if (wlk->blocksused == 0 || rn == 0) {
      wlk->nextextent = geom->maxdirentries;
      wlk->blocksused = 0;
      break;
    }
    // found something
    return 1;
  }
  return 0; // no more
}


//==========================================================================
//
//  p3dskFindCalcBlockbufLength
//
//  returns -1, or number of 16-bit items in file block buffer
//
//==========================================================================
int p3dskFindCalcBlockbufLength (P3DskFileInfo *nfo) {
  if (!nfo || !nfo->p3d) return FLPERR_SHIT;
  Floppy *flp = nfo->p3d->flp;
  P3DskGeometry *geom = &nfo->p3d->geom;
  if (!flp || !geom || !flp->insert || !geom->sides) return FLPERR_SHIT;
  if (!nfo->mask[0] || !nfo->name[0]) return FLPERR_SHIT;
  int buflen = 0;
  P3DskExtentWalker wlk;
  if (p3dskInitExtentWalker(&wlk, nfo) != FLPERR_OK) return FLPERR_SHIT;
  for (;;) {
    int neres = p3dskNextExtent(&wlk);
    if (neres < 0) return neres;
    if (neres == 0) break;
    buflen += (int)wlk.blocksused;
  }
  return buflen;
}


//==========================================================================
//
//  p3dskGetBlockbuf
//
//  get all file blocks; buffer size must be
//  calculated with `p3dskFindCalcBlockbufLength()`
//
//==========================================================================
int p3dskGetBlockbuf (P3DskFileInfo *nfo, uint16_t *dest) {
  if (!nfo || !nfo->p3d) return FLPERR_SHIT;
  Floppy *flp = nfo->p3d->flp;
  P3DskGeometry *geom = &nfo->p3d->geom;
  if (!flp || !geom || !flp->insert || !geom->sides || !dest) return FLPERR_SHIT;
  if (!nfo->mask[0] || !nfo->name[0]) return FLPERR_SHIT;
  P3DskExtentWalker wlk;
  if (p3dskInitExtentWalker(&wlk, nfo) != FLPERR_OK) return FLPERR_SHIT;
  for (;;) {
    int neres = p3dskNextExtent(&wlk);
    if (neres < 0) return neres;
    if (neres == 0) break;
    for (unsigned f = 0; f < wlk.blocksused; ++f) *dest++ = wlk.blocks[f];
  }
  return FLPERR_OK;
}


//==========================================================================
//
//  p3dskReadFile
//
//  read file data
//  returns <0 on error, or number of bytes read
//
//==========================================================================
int p3dskReadFile (const P3DskFileInfo *nfo, void *buf, int ofs, int bytecount) {
  if (!nfo || !nfo->p3d) return FLPERR_SHIT;
  Floppy *flp = nfo->p3d->flp;
  P3DskGeometry *geom = &nfo->p3d->geom;
  if (!flp || !geom || !flp->insert || !geom->sides) return FLPERR_SHIT;
  if (!nfo->mask[0] || !nfo->name[0]) return FLPERR_SHIT;
  if (ofs < 0 || bytecount < 0) return FLPERR_SHIT;
  if (bytecount == 0) return 0;
  if (!buf) return FLPERR_SHIT;
  if ((unsigned)ofs >= nfo->size) return 0; // past EOF
  const size_t wantedofs = (unsigned)ofs;
  size_t currfpos = 0;
  uint8_t *dest = (uint8_t *)buf;
  size_t dpos = 0;
  const uint16_t blksize = geom->blocksize; // cache it
  // walk extents
  P3DskExtentWalker wlk;
  if (p3dskInitExtentWalker(&wlk, nfo) != FLPERR_OK) return FLPERR_SHIT;
  do {
    int neres = p3dskNextExtent(&wlk);
    if (neres < 0) return neres;
    if (neres == 0) break;
    #ifdef P3DSK_DEBUG_FILE_READER
    fprintf(stderr, ".extent; currfpos=%u; wantedofs=%u; bcount=%u; ebytes=%u\n", (unsigned)currfpos, (unsigned)wantedofs, wlk.blocksused, wlk.extbytes);
    #endif
    // skip this extent as a whole if it is not interesting
    if (currfpos < wantedofs && currfpos+wlk.extbytes <= wantedofs) {
      // skip this extent
      #ifdef P3DSK_DEBUG_FILE_READER
      fprintf(stderr, "..skip extent; currfpos=%u; wantedofs=%u; ebytes=%u\n", (unsigned)currfpos, (unsigned)wantedofs, wlk.extbytes);
      #endif
      currfpos += wlk.extbytes;
      continue;
    }
    // universal code here, why not?
    #ifdef P3DSK_DEBUG_FILE_READER
    fprintf(stderr, "..processing extent; currfpos=%u; wantedofs=%u; ebytes=%u\n", (unsigned)currfpos, (unsigned)wantedofs, wlk.extbytes);
    #endif
    for (unsigned ebidx = 0; ebidx < wlk.blocksused; ++ebidx) {
      if (wlk.extbytes == 0) break;
      // process this block
      const uint16_t secsize = geom->secsize; // cache it
      const uint16_t blkidx = wlk.blocks[ebidx];
      for (unsigned bofs = 0; bofs < blksize; bofs += secsize) {
        // read block sector
        if (wlk.extbytes == 0) break;
        size_t seccopy = (secsize <= wlk.extbytes ? secsize : wlk.extbytes);
        #ifdef P3DSK_DEBUG_FILE_READER
        fprintf(stderr, "....processing block; bofs=%u; seccopy=%u/%u; currfpos=%u; wantedofs=%u; ebytes=%u; dpos=%u; bytecount=%d\n", bofs, seccopy, secsize, (unsigned)currfpos, (unsigned)wantedofs, wlk.extbytes, (unsigned)dpos, bytecount);
        #endif
        wlk.extbytes -= seccopy;
        // skip this sector if we don't need it
        if (currfpos+seccopy <= wantedofs) {
          currfpos += seccopy;
          continue;
        }
        // empty block (hole)?
        if (blkidx == 0) {
          // skip unneeded bytes
          if (currfpos < wantedofs) {
            P3DSK_ASSERT(currfpos+seccopy > wantedofs);
            const size_t skip = wantedofs-currfpos;
            seccopy -= skip;
            currfpos += skip;
          }
          // copy bytes
          P3DSK_ASSERT(seccopy != 0);
          P3DSK_ASSERT(currfpos >= wantedofs);
          if (seccopy >= bytecount) {
            memset(dest+dpos, 0, (unsigned)bytecount);
            dpos += (unsigned)bytecount;
            return (int)dpos; // done with reading
          }
          memset(dest+dpos, 0, seccopy);
          currfpos += seccopy;
          dpos += seccopy;
          bytecount -= (int)seccopy;
        } else {
          // sanity check
          if (blkidx < geom->dirblocks || blkidx >= geom->maxblocks) return FLPERR_SHIT;
          // calculate logical sector
          const int lsidx = p3dskCalcLogicalSectorForBlock(geom, (int)blkidx, (int)bofs);
          if (lsidx < 0) return FLPERR_SHIT;
          // calculate physical sector offset
          int psofs = p3dskCalcPhysSectorOffsetForBlock(geom, (int)blkidx, (int)bofs);
          if (psofs < 0) return FLPERR_SHIT;
          // read logical sector
          const uint8_t *secdata = p3dskGetLogicalSector(nfo->p3d, lsidx);
          if (secdata == NULL) return FLPERR_SHIT;
          // skip unneeded bytes
          if (currfpos < wantedofs) {
            P3DSK_ASSERT(currfpos+seccopy > wantedofs);
            const size_t skip = wantedofs-currfpos;
            seccopy -= skip;
            secdata += skip;
            currfpos += skip;
          }
          // copy bytes
          P3DSK_ASSERT(seccopy != 0);
          P3DSK_ASSERT(currfpos >= wantedofs);
          if (seccopy >= bytecount) {
            memcpy(dest+dpos, secdata, (unsigned)bytecount);
            dpos += (unsigned)bytecount;
            return (int)dpos; // done with reading
          }
          memcpy(dest+dpos, secdata, seccopy);
          currfpos += seccopy;
          dpos += seccopy;
          bytecount -= (int)seccopy;
        }
      }
    }
  } while (bytecount > 0);
  return (int)dpos;
}


//==========================================================================
//
//  p3dskReadFileHeader
//
//  read and validate +3DOS file header
//  returns error code
//
//==========================================================================
int p3dskReadFileHeader (const P3DskFileInfo *nfo, P3DskFileHeader *hdr) {
  if (!hdr) return FLPERR_SHIT;
  memset(hdr, 0, sizeof(P3DskFileHeader));
  if (!nfo) return FLPERR_SHIT;
  uint8_t buf[P3DSK_FILE_HEADER_SIZE];
  int rd = p3dskReadFile(nfo, buf, 0, P3DSK_FILE_HEADER_SIZE);
  if (rd < 0) return rd;
  if (rd != P3DSK_FILE_HEADER_SIZE) return FLPERR_NOHEADER;
  // check signature
  if (memcmp(buf, p3doshdrsign, 8) != 0) return FLPERR_NOHEADER;
  // check checksum
  uint8_t csum = 0;
  for (unsigned f = 0; f < (unsigned)(P3DSK_FILE_HEADER_SIZE-1); ++f) csum += buf[f];
  if (csum != buf[P3DSK_FILE_HEADER_SIZE-1]) return FLPERR_NOHEADER;
  // header seems to be ok, copy it to `hdr`
  hdr->issue = buf[9];
  hdr->version = buf[10];
  hdr->filesize = buf[11]|(((uint32_t)buf[12])<<8)|(((uint32_t)buf[13])<<16)|(((uint32_t)buf[14])<<24);
  hdr->bastype = buf[15+0];
  hdr->baslength = buf[15+1]|(((uint16_t)buf[15+2])<<8);
  hdr->basaddr = buf[15+3]|(((uint16_t)buf[15+4])<<8);
  hdr->basvarsofs = buf[15+5]|(((uint16_t)buf[15+6])<<8);
  hdr->valid = 1;
  return FLPERR_OK;
}


//==========================================================================
//
//  p3dskWriteFileHeader
//
//  write file header to the first 128 bytes of the file
//  returns error code
//
//==========================================================================
int p3dskWriteFileHeader (P3DskFileInfo *nfo, const P3DskFileHeader *hdr) {
  if (!nfo || !hdr || !hdr->valid) return FLPERR_SHIT;
  uint8_t hbuf[P3DSK_FILE_HEADER_SIZE];
  memset(hbuf, 0, sizeof(hbuf));
  // put signature
  memcpy(hbuf, p3doshdrsign, 8);
  hbuf[8] = 0x1Au;
  hbuf[9] = hdr->issue;
  hbuf[10] = hdr->version;
  hbuf[11] = hdr->filesize&0xFFu;
  hbuf[12] = (hdr->filesize>>8)&0xFFu;
  hbuf[13] = (hdr->filesize>>16)&0xFFu;
  hbuf[14] = (hdr->filesize>>24)&0xFFu;
  // copy basic header
  hbuf[15+0] = hdr->bastype;
  hbuf[15+1] = hdr->baslength&0xFFu;
  hbuf[15+2] = (hdr->baslength>>8)&0xFFu;
  hbuf[15+3] = hdr->basaddr&0xFFu;
  hbuf[15+4] = (hdr->basaddr>>8)&0xFFu;
  hbuf[15+5] = hdr->basvarsofs&0xFFu;
  hbuf[15+6] = (hdr->basvarsofs>>8)&0xFFu;
  // checksum
  uint8_t csum = 0;
  for (unsigned f = 0; f < (unsigned)(P3DSK_FILE_HEADER_SIZE-1); ++f) csum += hbuf[f];
  hbuf[P3DSK_FILE_HEADER_SIZE-1] = csum;
  // write it
  int res = p3dskWriteFile(nfo, hbuf, 0, P3DSK_FILE_HEADER_SIZE);
  if (res < 0) return res;
  return (res == P3DSK_FILE_HEADER_SIZE ? FLPERR_OK : FLPERR_SHIT);
}



//**************************************************************************
//
// +3DOS file grow/shrink
//
//**************************************************************************

//==========================================================================
//
//  p3dskBuildBlockBitmap
//
//  build bitmap of used disk blocks
//  returns dynamically allocated data or NULL on error
//
//==========================================================================
uint8_t *p3dskBuildBlockBitmap (Floppy *flp, P3DskGeometry *geom) {
  if (!flp || !geom || !geom->sides) return NULL;
  uint8_t *bmp = malloc(geom->maxblocks);
  memset(bmp, 0, geom->maxblocks);
  // mark directory as used
  memset(bmp, 1, geom->dirblocks);
  // scan directory, mark all used blocks
  P3DiskInfo p3d;
  p3d.flp = flp;
  memcpy(&p3d.geom, geom, sizeof(P3DskGeometry));
  for (unsigned dirext = 0; dirext < geom->maxdirentries; ++dirext) {
    uint8_t *de = p3dskGetDirEntryPtr(&p3d, dirext);
    if (!de) { free(bmp); return NULL; }
    // 0-15: user #n file
    // 16-31: user #n for P2DOS, password extent for CP/M 3+
    // 32: disk label
    // 33: time stamp
    // 0xE5: unused
    // users are not supported, but allow files for users 0..15
    if (de[0] == 0xE5u) continue; // unused
    if (de[0] > 33) { free(bmp); return NULL; } // oops
    // ignore allocated blocks for password extents and disk labels
    if (de[0] > 16) continue;
    // allow holes
    uint16_t rn = de[15]*128;
    // scan blocks
    if (geom->maxblocks < 256) {
      if (rn/geom->blocksize > 16) { free(bmp); return NULL; }
      const unsigned blkused = (rn+geom->blocksize-1u)/geom->blocksize;
      // 8-bit block numbers
      for (unsigned bc = 0; bc < blkused; ++bc) {
        uint16_t bnum = de[16+bc];
        if (!bnum) continue;
        if (bnum < geom->dirblocks || bnum >= geom->maxblocks) { free(bmp); return NULL; }
        //if (bmp[bnum]) fprintf(stderr, "DISK BLOCK #%d is doubly allocated! deidx=%u\n", bnum, dirext);
        bmp[bnum] = 1; // mask as used
      }
    } else {
      // 16-bit block numbers
      if (rn/geom->blocksize > 8) { free(bmp); return NULL; }
      const unsigned blkused = (rn+geom->blocksize-1u)/geom->blocksize;
      for (unsigned bc = 0; bc < blkused; ++bc) {
        uint16_t bnum = de[16+(bc<<1)]|((uint16_t)(de[17+(bc<<1)]));
        if (!bnum) continue;
        if (bnum < geom->dirblocks || bnum >= geom->maxblocks) { free(bmp); return NULL; }
        //if (bmp[bnum]) fprintf(stderr, "DISK BLOCK #%d is doubly allocated! deidx=%u\n", bnum, dirext);
        bmp[bnum] = 1; // mask as used
      }
    }
  }
  return bmp;
}


//==========================================================================
//
//  p3dskFindLastExtent
//
//  <0: error
//
//==========================================================================
static int p3dskFindLastExtent (P3DiskInfo *p3d, const void *namebuf/*[11]*/, uint16_t currextidx) {
  if (!p3d) return FLPERR_SHIT;
  Floppy *flp = p3d->flp;
  P3DskGeometry *geom = &p3d->geom;
  if (!flp || !geom || !flp->insert || !geom->sides || !namebuf) return FLPERR_SHIT;
  uint16_t res = currextidx;
  uint16_t nenum = 0;
  for (;;) {
    int found = 0;
    for (unsigned f = 0; f < geom->maxdirentries; ++f) {
      const uint16_t pex = currextidx;
      const uint8_t *de = p3dskGetDirEntryPtr(p3d, currextidx);
      currextidx = (currextidx+1)%geom->maxdirentries;
      if (!de) return FLPERR_SHIT;
      if (de[0] != 0) continue; // allow only USER 0 files
      // get extent number
      // bit 5-7 of Xl are 0, bit 0-4 store the lower bits of the extent number.
      // bit 6 and 7 of Xh are 0, bit 0-5 store the higher bits of the extent number.
      if (de[12]&0x70u) continue; // check bits 5-7
      if (de[14]&0xc0u) continue; // check bits 6-7
      const uint16_t eidx = de[12]|(uint16_t)(de[14]<<5);
      if (eidx != nenum) continue;
      // compare name
      if (cpmNameEqu(de+1, namebuf) == 0) continue;
      // good extent
      res = pex;
      ++nenum;
      found = 1;
    }
    if (!found) break;
  }
  return res;
}


typedef struct {
  unsigned maxblk;
  unsigned blkused;
  uint16_t rn;
  uint16_t blocks[16];
} P3DskExtBlocks;


//==========================================================================
//
//  p3dskParseExtentBlocks
//
//==========================================================================
static int p3dskParseExtentBlocks (P3DskGeometry *geom, const uint8_t *de, P3DskExtBlocks *blist) {
  if (!blist || !de || !geom || !geom->sides) return FLPERR_SHIT;
  blist->rn = de[15]*128;
  if (geom->maxblocks < 256) {
    blist->maxblk = 16;
    if (blist->rn/geom->blocksize > 16) return FLPERR_SHIT;
    blist->blkused = (blist->rn+geom->blocksize-1u)/geom->blocksize;
    // 8-bit block numbers
    for (unsigned bc = 0; bc < blist->blkused; ++bc) {
      const uint16_t bnum = de[16+bc];
      if (bnum) {
        if (bnum < geom->dirblocks || bnum >= geom->maxblocks) return FLPERR_SHIT;
      }
      blist->blocks[bc] = bnum;
    }
  } else {
    // 16-bit block numbers
    blist->maxblk = 8;
    if (blist->rn/geom->blocksize > 8) return FLPERR_SHIT;
    blist->blkused = (blist->rn+geom->blocksize-1u)/geom->blocksize;
    for (unsigned bc = 0; bc < blist->blkused; ++bc) {
      uint16_t bnum = de[16+(bc<<1)]|((uint16_t)(de[17+(bc<<1)]));
      if (bnum) {
        if (bnum < geom->dirblocks || bnum >= geom->maxblocks) return FLPERR_SHIT;
      }
      blist->blocks[bc] = bnum;
    }
  }
  // +3DOS is not using this, but why not?
  if (de[13] && de[13] <= 128 && de[13] <= blist->rn) blist->rn -= 128-de[13];
  return FLPERR_OK;
}


//==========================================================================
//
//  p3dskWriteExtentBlocks
//
//==========================================================================
static int p3dskWriteExtentBlocks (P3DiskInfo *p3d, uint8_t *de, const P3DskExtBlocks *blist, int lastext) {
  if (!p3d) return FLPERR_SHIT;
  Floppy *flp = p3d->flp;
  P3DskGeometry *geom = &p3d->geom;
  if (!flp || !flp->insert || flp->protect || !blist || !de || !geom || !geom->sides) return FLPERR_SHIT;
  if (geom->maxblocks < 256) {
    if (blist->rn/geom->blocksize > 16) return FLPERR_SHIT;
    //blist->blkused = (blist->rn+geom->blocksize-1u)/geom->blocksize;
    // 8-bit block numbers
    for (unsigned bc = 0; bc < blist->blkused; ++bc) {
      de[16+bc] = blist->blocks[bc];
    }
  } else {
    // 16-bit block numbers
    if (blist->rn/geom->blocksize > 8) return FLPERR_SHIT;
    //blist->blkused = (blist->rn+geom->blocksize-1u)/geom->blocksize;
    for (unsigned bc = 0; bc < blist->blkused; ++bc) {
      const uint16_t bnum = blist->blocks[bc];
      de[16+(bc<<1)] = bnum&0xffU;
      de[17+(bc<<1)] = (bnum>>8)&0xffU;
    }
  }
  uint16_t recs = (blist->rn+127u)/128u;
  de[15] = recs&0xffU;
  // +3DOS is not using this, but why not?
  if (blist->rn%128u) {
    de[13] = blist->rn%128u;
  } else {
    de[13] = 0;
  }
  return p3dskUpdateDirEntryCRC(p3d, lastext);
}


//==========================================================================
//
//  p3dskGrowFile
//
//==========================================================================
int p3dskGrowFile (P3DskFileInfo *nfo, int newsize) {
  if (!nfo || !nfo->p3d || newsize < 0) return FLPERR_SHIT;
  Floppy *flp = nfo->p3d->flp;
  P3DskGeometry *geom = &nfo->p3d->geom;
  if (!flp || !geom || !flp->insert || flp->protect || !geom->sides) return FLPERR_SHIT;
  if (!nfo->mask[0] || !nfo->name[0]) return FLPERR_SHIT;
  if ((unsigned)newsize <= nfo->size) return FLPERR_OK;
  // build name buffer
  char namebuf[11];
  memcpy(namebuf, nfo->nameonly, 8);
  memcpy(namebuf+8, nfo->extonly, 3);
  // block bitmap (will be requested if necessary)
  uint8_t *bmp = NULL;
  // find last file extent
  int lastext = p3dskFindLastExtent(nfo->p3d, namebuf, nfo->firstextent);
  if (lastext < 0) return FLPERR_SHIT;
  #ifdef P3DSK_DEBUG_FILE_GROW
  fprintf(stderr, "GF: name[%.11s]; firstext=%u; lastext=%u\n", namebuf, nfo->firstextent, lastext);
  #endif
  uint8_t *de = p3dskGetDirEntryPtr(nfo->p3d, lastext);
  if (!de) return FLPERR_SHIT;
  // first, check if we can simply use the last record
  size_t toadd = (unsigned)newsize-nfo->size;
  #ifdef P3DSK_DEBUG_FILE_GROW
  fprintf(stderr, "GF: toadd=%u\n", (unsigned)toadd);
  #endif
  // +3DOS doesn't support this, but meh, why not?
  if (de[13] > 0 && de[13] < 128) {
    int left = 128-de[13];
    #ifdef P3DSK_DEBUG_FILE_GROW
    fprintf(stderr, "GF: using Bc; left=%d\n", left);
    #endif
    if (toadd <= left) {
      // yay
      nfo->size += toadd;
      de[13] += toadd;
      if (de[13] >= 128) de[13] = 0; // for compatibility
      return p3dskUpdateDirEntryCRC(nfo->p3d, lastext);
    }
    // still can add a little
    nfo->size += left;
    toadd -= left;
    de[13] = 0; // for compatibility
    if (p3dskUpdateDirEntryCRC(nfo->p3d, lastext) != FLPERR_OK) return FLPERR_SHIT;
  }
  P3DSK_ASSERT(toadd != 0);
  // ok, partial last recrod is filled, go with more records
  P3DskExtBlocks blist;
  for (;;) {
    // get block list
    if (p3dskParseExtentBlocks(geom, de, &blist) != FLPERR_OK) {
      if (bmp) free(bmp);
      return FLPERR_SHIT;
    }
    #ifdef P3DSK_DEBUG_FILE_GROW
    fprintf(stderr, "GF: got block list for extent %d; rn=%u; blkused=%u; maxblk=%u\n", lastext, blist.rn, blist.blkused, blist.maxblk);
    #endif
    while (toadd != 0) {
      // see if we have any free records in the last block
      if (blist.rn && blist.rn%geom->blocksize != 0) {
        // sanity check
        if (blist.blkused >= blist.maxblk) {
          if (bmp) free(bmp);
          return FLPERR_SHIT;
        }
        // number of free bytes in the last block
        unsigned left = geom->blocksize-blist.rn%geom->blocksize;
        P3DSK_ASSERT(left != 0);
        #ifdef P3DSK_DEBUG_FILE_GROW
        fprintf(stderr, "GF:   filling last used extent block; left=%u; toadd=%u\n", left, (unsigned)toadd);
        #endif
        if (left >= toadd) {
          // fits in the block
          nfo->size += toadd;
          blist.rn += toadd;
          int res = p3dskWriteExtentBlocks(nfo->p3d, de, &blist, lastext);
          if (bmp) free(bmp);
          return res;
        }
        // fill last block up to the block boundary
        blist.rn += left;
        nfo->size += left;
        toadd -= left;
        P3DSK_ASSERT(left != 0);
        P3DSK_ASSERT(blist.rn%geom->blocksize == 0);
      }
      // we need a new block; do we have a room for it in the current extent?
      if (blist.blkused >= blist.maxblk) {
        #ifdef P3DSK_DEBUG_FILE_GROW
        fprintf(stderr, "GF: no more free blocks in this extent\n");
        #endif
        // nope; update extent and get out
        if (p3dskWriteExtentBlocks(nfo->p3d, de, &blist, lastext) != FLPERR_OK) {
          if (bmp) free(bmp);
          return FLPERR_SHIT;
        }
        break;
      }
      // we have a room for a new block, allocate it
      if (!bmp) {
        bmp = p3dskBuildBlockBitmap(flp, geom);
        if (!bmp) {
          // oops
          p3dskUpdateDirEntryCRC(nfo->p3d, lastext); // i don't care about errors here
          if (bmp) free(bmp);
          return FLPERR_SHIT;
        }
      }
      unsigned bidx = 0;
      for (; bidx < geom->maxblocks; ++bidx) if (!bmp[bidx]) break;
      if (bidx >= geom->maxblocks) {
        // no blocks available, alas
        p3dskUpdateDirEntryCRC(nfo->p3d, lastext); // i don't care about errors here
        if (bmp) free(bmp);
        return FLPERR_NOSPACE;
      }
      #ifdef P3DSK_DEBUG_FILE_GROW
      fprintf(stderr, "GF: allocated new block #%u\n", bidx);
      #endif
      // mark this block as allocated
      bmp[bidx] = 1;
      // put it into blocks list
      blist.blocks[blist.blkused++] = bidx;
      // allocate block (or its part)
      if (toadd <= geom->blocksize) {
        // completely fits
        blist.rn += toadd;
        nfo->size += toadd;
        int res = p3dskWriteExtentBlocks(nfo->p3d, de, &blist, lastext);
        if (bmp) free(bmp);
        return res;
      }
      // need more than one block, use this one completely
      blist.rn += geom->blocksize;
      nfo->size += geom->blocksize;
      toadd -= geom->blocksize;
      P3DSK_ASSERT(toadd != 0);
      if (p3dskWriteExtentBlocks(nfo->p3d, de, &blist, lastext) != FLPERR_OK) {
        if (bmp) free(bmp);
        return FLPERR_SHIT;
      }
    }
    // we will come here only when we need a new extent
    uint16_t nxnum = (de[12]|(uint16_t)(de[14]<<5))+1u;
    // look for a free extent (reuse time stamps and other crap too)
    int found = 0;
    for (unsigned f = 0; f < geom->maxdirentries; ++f) {
      lastext = (lastext+1)%geom->maxdirentries;
      de = p3dskGetDirEntryPtr(nfo->p3d, lastext);
      if (!de) {
        if (bmp) free(bmp);
        return FLPERR_SHIT;
      }
      if (de[0] <= 15) continue; // USER n files, preserve
      // this is either unused, or timestamp
      // build new file extent
      de[0] = 0; // always USER 0
      // set name
      memcpy(de+1, namebuf, 11);
      // set extent number
      de[12] = nxnum&0x1fu;
      de[14] = (nxnum>>5)&0x3fu;
      // set used records (this will be fixed on the next loop iteration)
      de[13] = de[15] = 0;
      // clear allocated blocks
      memset(de+16, 0, 16);
      if (p3dskUpdateDirEntryCRC(nfo->p3d, lastext) != FLPERR_OK) {
        if (bmp) free(bmp);
        return FLPERR_SHIT;
      }
      found = 1;
      break;
    }
    if (!found) {
      // alas, no more entries in a directory
      if (bmp) free(bmp);
      return FLPERR_MANYFILES;
    }
    // loop again
  }
  // the thing that should not be
  abort();
}



//==========================================================================
//
//  p3dskWriteFile
//
//==========================================================================
int p3dskWriteFile (P3DskFileInfo *nfo, const void *buf, int ofs, int bytecount) {
  if (!nfo || !nfo->p3d) return FLPERR_SHIT;
  Floppy *flp = nfo->p3d->flp;
  P3DskGeometry *geom = &nfo->p3d->geom;
  if (!flp || !geom || !flp->insert || flp->protect || !geom->sides) return FLPERR_SHIT;
  if (!nfo->mask[0] || !nfo->name[0]) return FLPERR_SHIT;
  if (ofs < 0 || bytecount < 0) return FLPERR_SHIT;
  if (bytecount == 0) return 0;
  if (!buf) return FLPERR_SHIT;
  const int disksize = geom->maxblocks*geom->blocksize;
  // some easy checks
  if (ofs >= disksize || bytecount > disksize) return FLPERR_NOSPACE; //FIXME: some other error code?
  if (ofs+bytecount > disksize) return FLPERR_NOSPACE;
  // grow file
  int opres = p3dskGrowFile(nfo, ofs+bytecount);
  if (opres != FLPERR_OK) return opres;
  // walk the file to the required extent, and write from there
  // if we will encounter a hole, the writer will fail
  const size_t wantedofs = (unsigned)ofs;
  size_t currfpos = 0;
  const uint8_t *src = (const uint8_t *)buf;
  size_t spos = 0;
  const uint16_t blksize = geom->blocksize; // cache it
  // walk extents
  P3DskExtentWalker wlk;
  if (p3dskInitExtentWalker(&wlk, nfo) != FLPERR_OK) return FLPERR_SHIT;
  do {
    int neres = p3dskNextExtent(&wlk);
    if (neres < 0) return neres;
    if (neres == 0) break;
    #ifdef P3DSK_DEBUG_FILE_WRITER
    fprintf(stderr, ".extent; currfpos=%u; wantedofs=%u; bcount=%u; ebytes=%u\n", (unsigned)currfpos, (unsigned)wantedofs, wlk.blocksused, wlk.extbytes);
    #endif
    // skip this extent as a whole if it is not interesting
    if (currfpos < wantedofs && currfpos+wlk.extbytes <= wantedofs) {
      // skip this extent
      #ifdef P3DSK_DEBUG_FILE_WRITER
      fprintf(stderr, "..skip extent; currfpos=%u; wantedofs=%u; ebytes=%u\n", (unsigned)currfpos, (unsigned)wantedofs, wlk.extbytes);
      #endif
      currfpos += wlk.extbytes;
      continue;
    }
    // universal code here, why not?
    #ifdef P3DSK_DEBUG_FILE_WRITER
    fprintf(stderr, "..processing extent; currfpos=%u; wantedofs=%u; ebytes=%u\n", (unsigned)currfpos, (unsigned)wantedofs, wlk.extbytes);
    #endif
    for (unsigned ebidx = 0; ebidx < wlk.blocksused; ++ebidx) {
      if (wlk.extbytes == 0) break;
      // process this block
      const uint16_t secsize = geom->secsize; // cache it
      const uint16_t blkidx = wlk.blocks[ebidx];
      uint16_t wlkbleft = wlk.extbytes;
      for (unsigned bofs = 0; bofs < blksize; bofs += secsize) {
        // read block sector
        if (wlkbleft == 0) break;
        size_t seccopy = (secsize <= wlkbleft ? secsize : wlkbleft);
        #ifdef P3DSK_DEBUG_FILE_WRITER
        fprintf(stderr, "...processing block; bofs=%u; bidx=%u; seccopy=%u/%u; currfpos=%u; wantedofs=%u; ebytes=%u; spos=%u; bytecount=%d\n", bofs, blkidx, seccopy, secsize, (unsigned)currfpos, (unsigned)wantedofs, wlkbleft, (unsigned)spos, bytecount);
        #endif
        wlkbleft -= seccopy;
        // skip this sector if we don't need it
        if (currfpos+seccopy <= wantedofs) {
          currfpos += seccopy;
          continue;
        }
        // empty block (hole)?
        if (blkidx == 0) return FLPERR_SHIT; // we cannot work with holes (yet)
        // sanity check
        if (blkidx < geom->dirblocks || blkidx >= geom->maxblocks) return FLPERR_SHIT;
        // calculate logical sector
        const int lsidx = p3dskCalcLogicalSectorForBlock(geom, (int)blkidx, (int)bofs);
        if (lsidx < 0) return FLPERR_SHIT;
        // calculate physical sector offset
        int psofs = p3dskCalcPhysSectorOffsetForBlock(geom, (int)blkidx, (int)bofs);
        if (psofs < 0) return FLPERR_SHIT;
        // read logical sector
        #ifdef P3DSK_DEBUG_FILE_WRITER
        fprintf(stderr, "....reading lsec #%d (ofs=%d)\n", lsidx, psofs);
        #endif
        uint8_t *secdata = p3dskGetLogicalSector(nfo->p3d, lsidx);
        if (secdata == NULL) return FLPERR_SHIT;
        // skip unneeded bytes
        if (currfpos < wantedofs) {
          P3DSK_ASSERT(currfpos+seccopy > wantedofs);
          const size_t skip = wantedofs-currfpos;
          seccopy -= skip;
          secdata += skip;
          currfpos += skip;
        }
        // copy bytes
        P3DSK_ASSERT(seccopy != 0);
        P3DSK_ASSERT(currfpos >= wantedofs);
        if (seccopy >= bytecount) {
          #ifdef P3DSK_DEBUG_FILE_WRITER
          fprintf(stderr, "....writing final #%d bytes\n", bytecount);
          #endif
          memcpy(secdata, src+spos, (unsigned)bytecount);
          spos += (unsigned)bytecount;
          // update data CRC
          opres = p3dskUpdateLogicalSectorCRC(nfo->p3d, lsidx);
          if (opres != FLPERR_OK) return opres;
          return (int)spos; // done with writing
        }
        #ifdef P3DSK_DEBUG_FILE_WRITER
        fprintf(stderr, "....writing #%d bytes\n", seccopy);
        #endif
        memcpy(secdata, src+spos, seccopy);
        currfpos += seccopy;
        spos += seccopy;
        bytecount -= (int)seccopy;
        // update data CRC
        opres = p3dskUpdateLogicalSectorCRC(nfo->p3d, lsidx);
        if (opres != FLPERR_OK) return opres;
      }
    }
  } while (bytecount > 0);
  return (int)spos;
}



//**************************************************************************
//
// CP/M bootable disks
//
//**************************************************************************

//==========================================================================
//
//  p3dskIsBootable
//
//  check if the given disk is CP/M bootable (ignores +3DOS boot methods)
//  returns error code or boolean
//
//==========================================================================
int p3dskIsBootable (const P3DiskInfo *p3d) {
  if (!p3d) return FLPERR_SHIT;
  Floppy *flp = p3d->flp;
  const P3DskGeometry *geom = &p3d->geom;
  if (!flp || !geom || !flp->insert || !geom->sides) return FLPERR_SHIT;
  //if (!geom->restracks) return FLPERR_SHIT; // this disk has no reserved sectors, so it cannot be booted
  // read first sector
  const uint8_t *bsec = flpGetSectorDataPtr(flp, 0, geom->firstsector);
  if (!bsec) return FLPERR_SHIT;
  // calc checksum
  uint8_t csum = 0;
  for (unsigned f = 0; f < geom->secsize; ++f) csum += bsec[f];
  return (csum == 3);
}


//==========================================================================
//
//  p3dskWriteBootableChecksum
//
//  modifies the first disk sector checksum to
//  indicate a "CP/M bootable" disk
//  returns error code
//
//==========================================================================
int p3dskWriteBootableChecksum (P3DiskInfo *p3d) {
  if (!p3d) return FLPERR_SHIT;
  Floppy *flp = p3d->flp;
  P3DskGeometry *geom = &p3d->geom;
  if (!flp || !geom || !flp->insert || !geom->sides) return FLPERR_SHIT;
  if (!geom->restracks) return FLPERR_SHIT; // this disk has no reserved sectors, so it cannot be booted
  // read first sector
  uint8_t *bsec = flpGetSectorDataPtr(flp, 0, geom->firstsector);
  if (!bsec) return FLPERR_SHIT;
  // reset csum byte
  //const uint8_t ob = bsec[15];
  //bsec[15] = 0;
  // calc checksum
  uint8_t csum = 0;
  for (unsigned f = 0; f < geom->secsize; ++f) if (f != 15) csum += bsec[f];
  // ensure that it is right
  csum = ((csum^0xFFu)+4u)&0xffU;
  if (bsec[15] == csum) return FLPERR_OK; // already valid
  if (flp->protect) return FLPERR_SHIT;
  bsec[15] = csum;
  return flpFixSectorDataCRC(flp, 0, geom->firstsector);
}


//==========================================================================
//
//  p3dskResetBootableChecksum
//
//  modifies the first disk sector checksum to
//  indicate a "CP/M non-bootable" disk
//  returns error code
//
//==========================================================================
int p3dskResetBootableChecksum (P3DiskInfo *p3d) {
  if (!p3d) return FLPERR_SHIT;
  Floppy *flp = p3d->flp;
  P3DskGeometry *geom = &p3d->geom;
  if (!flp || !geom || !flp->insert || !geom->sides) return FLPERR_SHIT;
  if (!geom->restracks) return FLPERR_SHIT; // this disk has no reserved sectors, so it cannot be booted
  // read first sector
  uint8_t *bsec = flpGetSectorDataPtr(flp, 0, geom->firstsector);
  if (!bsec) return FLPERR_SHIT;
  // reset csum byte
  //const uint8_t ob = bsec[15];
  //bsec[15] = 0;
  // calc checksum
  uint8_t csum = 0;
  for (unsigned f = 0; f < geom->secsize; ++f) if (f != 15) csum += bsec[f];
  // ensure that it is right
  csum = ((csum&0xFFu)+4)&0xffU;
  if (bsec[15] != csum) return FLPERR_OK; // already valid
  if (flp->protect) return FLPERR_SHIT;
  bsec[15] = csum^0xFFu;
  return flpFixSectorDataCRC(flp, 0, geom->firstsector);
}



/*
Directory entries - The directory is a sequence of directory entries
(also called extents), which contain 32 bytes of the following structure:

    St F0 F1 F2 F3 F4 F5 F6 F7 E0 E1 E2 Xl Bc Xh Rc
    Al Al Al Al Al Al Al Al Al Al Al Al Al Al Al Al

  St is the status; possible values are:
    0-15: used for file, status is the user number
    16-31: used for file, status is the user number (P2DOS) or used for password extent (CP/M 3 or higher)
    32: disc label
    33: time stamp (P2DOS)
    0xE5: unused

  F0-E2 are the file name and its extension. They may consist of any printable 7 bit ASCII character but:
    <>.,;:=?*[]

  The file name must not be empty, the extension may be empty. Both are
  padded with blanks. The highest bit of each character of the file name
  and extension is used as attribute. The attributes have the following
  meaning:

    F0: requires set wheel byte (Backgrounder II)
    F1: public file (P2DOS, ZSDOS), foreground-only command (Backgrounder II)
    F2: date stamp (ZSDOS), background-only commands (Backgrounder II)
    F7: wheel protect (ZSDOS)
    E0: read-only
    E1: system file
    E2: archived

  Public files (visible under each user number) are not supported by CP/M
  2.2, but there is a patch and some free CP/M clones support them
  without any patches.

  The wheel byte is (by default) the memory location at 0x4b. If it is
  zero, only non-privileged commands may be executed.

  Xl and Xh store the extent number. A file may use more than one
  directory entry, if it contains more blocks than an extent can hold. In
  this case, more extents are allocated and each of them is numbered
  sequentially with an extent number. If a physical extent stores more
  than 16k, it is considered to contain multiple logical extents, each
  pointing to 16k data, and the extent number of the last used logical
  extent is stored. Note: Some formats decided to always store only one
  logical extent in a physical extent, thus wasting extent space. CP/M
  2.2 allows 512 extents per file, CP/M 3 and higher allow up to 2048.
  Bit 5-7 of Xl are 0, bit 0-4 store the lower bits of the extent number.
  Bit 6 and 7 of Xh are 0, bit 0-5 store the higher bits of the extent
  number.

  Rc and Bc determine the length of the data used by this extent. The
  physical extent is divided into logical extents, each of them being 16k
  in size (a physical extent must hold at least one logical extent, e.g.
  a blocksize of 1024 byte with two-byte block pointers is not allowed).
  Rc stores the number of 128 byte records of the last used logical
  extent. Bc stores the number of bytes in the last used record. The
  value 0 means 128 for backward compatibility with CP/M 2.2, which did
  not support Bc. ISX records the number of unused instead of used bytes
  in Bc.

  Al stores block pointers. If the disk capacity minus boot tracks but
  including the directory area is less than 256 blocks, Al is interpreted
  as 8-bit values, otherwise as 16-bit little-endian values. Since the
  directory area is not subtracted, the directory area starts with block
  0 and files can never allocate block 0, which is why this value can be
  given a new meaning: A block pointer of 0 marks a hole in the file. If
  a hole covers the range of a full extent, the extent will not be allo-
  cated. In particular, the first extent of a file does not neccessarily
  have extent number 0. A file may not share blocks with other files, as
  its blocks would be freed if the other files were erased without a fol-
  lowing disk system reset. CP/M returns EOF when it reaches a hole,
  whereas UNIX returns zero-value bytes, which makes holes invisible.
*/

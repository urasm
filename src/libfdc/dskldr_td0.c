/*
 * disk images I/O
 * coded by Ketmar // Invisible Vector
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 */
// teledisk
#include "dskldr.h"
#include "dskldr_common.h"
#include "dskfs_all.h"


// ////////////////////////////////////////////////////////////////////////// //
// LHA unpacker
static const uint8_t *packed_ptr, *packed_end;


static inline int readChar (void) {
  return (packed_ptr < packed_end ? *packed_ptr++ : -1);
}


static const uint8_t d_code[256] = {
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,
  0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,
  0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,
  0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,
  0x03,0x03,0x03,0x03,0x03,0x03,0x03,0x03,
  0x03,0x03,0x03,0x03,0x03,0x03,0x03,0x03,
  0x04,0x04,0x04,0x04,0x04,0x04,0x04,0x04,
  0x05,0x05,0x05,0x05,0x05,0x05,0x05,0x05,
  0x06,0x06,0x06,0x06,0x06,0x06,0x06,0x06,
  0x07,0x07,0x07,0x07,0x07,0x07,0x07,0x07,
  0x08,0x08,0x08,0x08,0x08,0x08,0x08,0x08,
  0x09,0x09,0x09,0x09,0x09,0x09,0x09,0x09,
  0x0A,0x0A,0x0A,0x0A,0x0A,0x0A,0x0A,0x0A,
  0x0B,0x0B,0x0B,0x0B,0x0B,0x0B,0x0B,0x0B,
  0x0C,0x0C,0x0C,0x0C,0x0D,0x0D,0x0D,0x0D,
  0x0E,0x0E,0x0E,0x0E,0x0F,0x0F,0x0F,0x0F,
  0x10,0x10,0x10,0x10,0x11,0x11,0x11,0x11,
  0x12,0x12,0x12,0x12,0x13,0x13,0x13,0x13,
  0x14,0x14,0x14,0x14,0x15,0x15,0x15,0x15,
  0x16,0x16,0x16,0x16,0x17,0x17,0x17,0x17,
  0x18,0x18,0x19,0x19,0x1A,0x1A,0x1B,0x1B,
  0x1C,0x1C,0x1D,0x1D,0x1E,0x1E,0x1F,0x1F,
  0x20,0x20,0x21,0x21,0x22,0x22,0x23,0x23,
  0x24,0x24,0x25,0x25,0x26,0x26,0x27,0x27,
  0x28,0x28,0x29,0x29,0x2A,0x2A,0x2B,0x2B,
  0x2C,0x2C,0x2D,0x2D,0x2E,0x2E,0x2F,0x2F,
  0x30,0x31,0x32,0x33,0x34,0x35,0x36,0x37,
  0x38,0x39,0x3A,0x3B,0x3C,0x3D,0x3E,0x3F,
};


static const uint8_t d_len[256] = {
  0x03,0x03,0x03,0x03,0x03,0x03,0x03,0x03,
  0x03,0x03,0x03,0x03,0x03,0x03,0x03,0x03,
  0x03,0x03,0x03,0x03,0x03,0x03,0x03,0x03,
  0x03,0x03,0x03,0x03,0x03,0x03,0x03,0x03,
  0x04,0x04,0x04,0x04,0x04,0x04,0x04,0x04,
  0x04,0x04,0x04,0x04,0x04,0x04,0x04,0x04,
  0x04,0x04,0x04,0x04,0x04,0x04,0x04,0x04,
  0x04,0x04,0x04,0x04,0x04,0x04,0x04,0x04,
  0x04,0x04,0x04,0x04,0x04,0x04,0x04,0x04,
  0x04,0x04,0x04,0x04,0x04,0x04,0x04,0x04,
  0x05,0x05,0x05,0x05,0x05,0x05,0x05,0x05,
  0x05,0x05,0x05,0x05,0x05,0x05,0x05,0x05,
  0x05,0x05,0x05,0x05,0x05,0x05,0x05,0x05,
  0x05,0x05,0x05,0x05,0x05,0x05,0x05,0x05,
  0x05,0x05,0x05,0x05,0x05,0x05,0x05,0x05,
  0x05,0x05,0x05,0x05,0x05,0x05,0x05,0x05,
  0x05,0x05,0x05,0x05,0x05,0x05,0x05,0x05,
  0x05,0x05,0x05,0x05,0x05,0x05,0x05,0x05,
  0x06,0x06,0x06,0x06,0x06,0x06,0x06,0x06,
  0x06,0x06,0x06,0x06,0x06,0x06,0x06,0x06,
  0x06,0x06,0x06,0x06,0x06,0x06,0x06,0x06,
  0x06,0x06,0x06,0x06,0x06,0x06,0x06,0x06,
  0x06,0x06,0x06,0x06,0x06,0x06,0x06,0x06,
  0x06,0x06,0x06,0x06,0x06,0x06,0x06,0x06,
  0x07,0x07,0x07,0x07,0x07,0x07,0x07,0x07,
  0x07,0x07,0x07,0x07,0x07,0x07,0x07,0x07,
  0x07,0x07,0x07,0x07,0x07,0x07,0x07,0x07,
  0x07,0x07,0x07,0x07,0x07,0x07,0x07,0x07,
  0x07,0x07,0x07,0x07,0x07,0x07,0x07,0x07,
  0x07,0x07,0x07,0x07,0x07,0x07,0x07,0x07,
  0x08,0x08,0x08,0x08,0x08,0x08,0x08,0x08,
  0x08,0x08,0x08,0x08,0x08,0x08,0x08,0x08,
};


#define N          (4096) // buffer size
#define F          (60)   // lookahead buffer size
#define NIL        (N)    // leaf of tree
#define THRESHOLD  (2)

#define N_CHAR    (256-THRESHOLD+F) // kinds of characters (character code = 0..N_CHAR-1)
#define T         (N_CHAR*2-1)      // size of table
#define R         (T-1)             // position of root
#define MAX_FREQ  (0x8000)          // updates tree when the root frequency comes to this value

static uint8_t text_buf[N+F-1];
static uint16_t freq[T+1]; // frequency table

// pointers to parent nodes, except for the
// elements [T..T+N_CHAR-1] which are used to get
// the positions of leaves corresponding to the codes
static short prnt[T+N_CHAR];
static short son[T]; // pointers to child nodes (son[], son[]+1)

static int tbpos;

static uint32_t getbuf;
static uint8_t getlen;


/* get one bit */
static int GetBit (void) {
  int i;
  while (getlen <= 8) {
    if ((i = readChar()) == -1) i = 0;
    getbuf |= i<<(8-getlen);
    getlen += 8;
  }
  i = getbuf;
  getbuf <<= 1;
  --getlen;
  return ((i>>15)&0x01);
}


/* get one byte */
static uint8_t GetByte (void) {
  uint32_t r;
  while (getlen <= 8) {
    int i;
    if ((i = readChar()) == -1) i = 0;
    getbuf |= i<<(8-getlen);
    getlen += 8;
  }
  r = getbuf;
  getbuf <<= 8;
  getlen -= 8;
  return (r>>8)&0xff;
}


static void StartHuff (void) {
  int i, j;
  getbuf = 0;
  getlen = 0;
  for (i = 0; i < N_CHAR; ++i) {
    freq[i] = 1;
    son[i] = i+T;
    prnt[i+T] = i;
  }
  i = 0;
  j = N_CHAR;
  while (j <= R) {
    freq[j] = freq[i]+freq[i+1];
    son[j] = i;
    prnt[i] = prnt[i+1] = j;
    i += 2;
    ++j;
  }
  freq[T] = 0xffff;
  prnt[R] = 0;
  for (i = 0; i < N-F; ++i) text_buf[i] = ' ';
  tbpos = N-F;
}


/* reconstruction of tree */
static void reconst (void) {
  int i, j, k;
  int f, l;
  /* collect leaf nodes in the first half of the table and replace the freq by (freq+1)/2 */
  j = 0;
  for (i = 0; i < T; ++i) {
    if (son[i] >= T) {
      freq[j] = (freq[i]+1)/2;
      son[j] = son[i];
      ++j;
    }
  }
  /* begin constructing tree by connecting sons */
  for (i = 0, j = N_CHAR; j < T; i += 2, ++j) {
    k = i+1;
    f = freq[j] = freq[i]+freq[k];
    for (k = j-1; f < freq[k]; --k) ;
    ++k;
    l = (j-k)*sizeof(*freq);
    memmove(&freq[k+1], &freq[k], l);
    freq[k] = f;
    memmove(&son[k+1], &son[k], l);
    son[k] = i;
  }
  /* connect prnt */
  for (i = 0; i < T; ++i) if ((k = son[i]) >= T) prnt[k] = i; else prnt[k] = prnt[k+1] = i;
}


/* increment frequency of given code by one, and update tree */
static void update (int c) {
  int i, j, k, l;
  if (freq[R] == MAX_FREQ) reconst();
  c = prnt[c+T];
  do {
    k = ++freq[c];
    /* if the order is disturbed, exchange nodes */
    if (k > freq[l = c+1]) {
      while (k > freq[++l]);
      --l;
      freq[c] = freq[l];
      freq[l] = k;
      i = son[c];
      prnt[i] = l;
      if (i < T) prnt[i+1] = l;
      j = son[l];
      son[l] = i;
      prnt[j] = c;
      if (j < T) prnt[j+1] = c;
      son[c] = j;
      c = l;
    }
  } while ((c = prnt[c]) != 0); /* repeat up to root */
}


static int DecodeChar (void) {
  int c = son[R];
  /* travel from root to leaf, */
  /* choosing the smaller child node (son[]) if the read bit is 0, */
  /* the bigger (son[]+1} if 1 */
  while (c < T) c = son[c+GetBit()];
  c -= T;
  update(c);
  return c;
}


static int DecodePosition (void) {
  int i, j, c;
  /* recover upper 6 bits from table */
  i = GetByte();
  c = ((int)d_code[i])<<6;
  j = d_len[i];
  /* read lower 6 bits verbatim */
  j -= 2;
  while (j-- > 0) i = (i<<1)+GetBit();
  return c|(i&0x3f);
}


// returns full size, but fills only destSize bytes
static int lzhUnpack (void *dest, int destSize, const void *src, int pkSize) {
  int count = 0;
  uint8_t *buf = (uint8_t *)dest;
  int i, j, k, c;
  if (destSize < 0 || pkSize < 1) return -1;
  packed_ptr = (const uint8_t *)src;
  packed_end = packed_ptr+pkSize;
  StartHuff();
  while (packed_ptr < packed_end) {
    c = DecodeChar();
    if (c < 256) {
      if (destSize > 0) { *buf++ = c; --destSize; }
      text_buf[tbpos++] = c;
      tbpos &= (N-1);
      ++count;
    } else {
      i = (tbpos-DecodePosition()-1)&(N-1);
      j = c-255+THRESHOLD;
      for (k = 0; k < j; ++k) {
        c = text_buf[(i+k)&(N-1)];
        if (destSize > 0) { *buf++ = c; --destSize; }
        text_buf[tbpos++] = c;
        tbpos &= (N-1);
        ++count;
      }
    }
  }
  return count;
}


// ////////////////////////////////////////////////////////////////////////// //
//#define tdlogf(...)  fprintf(stderr, __VA_ARGS__)
//#define tdlogf(...)


enum {
  // No ID address field was present for this sector,
  // but there is a data field. The sector information in
  // the header represents fabricated information.
  TD0_SEC_NO_ID = 0x40,
  // This sector's data field is missing; no sector data follows this header.
  TD0_SEC_NO_DATA = 0x20,
  // A DOS sector copy was requested; this sector was not allocated.
  // In this case, no sector data follows this header.
  TD0_SEC_NO_DATA2 = 0x10
};


//==========================================================================
//
//  dskLoadTD0
//
//  TODO: CHECK CRCs!
//  TODO: add more diagnostic messages!
//
//==========================================================================
int dskLoadTD0 (Floppy *flp, FILE *fl) {
  char sign[4];
  uint8_t ver, data1, type, flags, data2, sides;
  uint32_t crc;
  uint8_t *tdimage;
  uint8_t *img;
  int tdimageSize, tdimageMax;
  FDCSector trkimg[256];

  if (fl == NULL) return FLPERR_SHIT;
  if (fread(sign, 4, 1, fl) != 1) {
    libfdcMsg(LIBFDC_MSG_ERROR, "cannot read TD0 signature");
    return FLPERR_SHIT;
  }
  if (sign[2] != 0 || (sign[0] != 't' && sign[0] != 'T') || (sign[1] != 'd' && sign[1] != 'D')) {
    libfdcMsg(LIBFDC_MSG_ERROR, "invalid TD0 signature");
    return FLPERR_SHIT;
  }
  if (fread(&ver, 1, 1, fl) != 1) {
    libfdcMsg(LIBFDC_MSG_ERROR, "cannot read TD0 version");
    return FLPERR_SHIT;
  }
  if (ver != 0x15u) {
    libfdcMsg(LIBFDC_MSG_ERROR, "invalid TD0 version (%u, must be %u)", ver, 0x15u);
    return FLPERR_SHIT;
  }

  if (fread(&data1, 1, 1, fl) != 1 ||
      fread(&type, 1, 1, fl) != 1 ||
      fread(&flags, 1, 1, fl) != 1 || // bit7: have comment?
      fread(&data2, 1, 1, fl) != 1 ||
      fread(&sides, 1, 1, fl) != 1)
  {
    libfdcMsg(LIBFDC_MSG_ERROR, "cannot read TD0 header");
    return FLPERR_SHIT;
  }
  if (sides < 1 || sides > 2) {
    libfdcMsg(LIBFDC_MSG_ERROR, "invalid number of TD0 sides (%u)", sides);
    return FLPERR_SHIT;
  }
  if (freaduint(fl, &crc, 2) != FLPERR_OK) {
    libfdcMsg(LIBFDC_MSG_ERROR, "cannot read TD0 CRC");
    return FLPERR_SHIT;
  }

  // now load the image
  tdimageSize = 0;
  tdimageMax = 1024*1024*8;
  if ((tdimage = malloc(tdimageMax)) == NULL) return FLPERR_SHIT;
  while (tdimageSize < tdimageMax) {
    size_t rd = fread(tdimage+tdimageSize, 1, tdimageMax-tdimageSize, fl);
    if (rd < 1) break;
    tdimageSize += rd;
  }
  libfdcMsg(LIBFDC_MSG_DEBUG, "TD0: %d bytes of data read", tdimageSize);

  if (sign[0] == 't') {
    // packed image
    int usz = lzhUnpack(NULL, 0, tdimage, tdimageSize);
    uint8_t *newimg;
    if (usz < 1 || usz > tdimageMax) { free(tdimage); return FLPERR_SHIT; }
    if ((newimg = malloc(usz)) == NULL) { free(tdimage); return FLPERR_SHIT; }
    tdimageSize = lzhUnpack(newimg, usz, tdimage, tdimageSize);
    free(tdimage);
    tdimage = newimg;
    libfdcMsg(LIBFDC_MSG_DEBUG, "TD0: %d bytes of data unpacked", tdimageSize);
  }

  img = tdimage;
  memset(trkimg, 0, sizeof(trkimg));

  if (flags&0x80) {
    // skip comment
    int len;
    if ((tdimageSize-= 10) < 0) goto fail;
    len = getui16(img+2);
    img += 10;
    if ((tdimageSize -= len) < 0) goto fail;
    img += len;
  }

  for (;;) {
    uint8_t secs, cyl, head/*, xcrc*/, sn;

    if (tdimageSize == 0) goto fail;
    if (img[0] == 0xff) {
      //???
      libfdcMsg(LIBFDC_MSG_DEBUG, "TD0: 0xFF! (%d left)", tdimageSize);
      break;
    }
    if ((tdimageSize -= 4) < 0) goto fail;

    secs = *img++;
    cyl = *img++;
    head = *img++;
    /*xcrc = **/img++; // low 8 bits of the previous 3 bytes

    libfdcMsg(LIBFDC_MSG_DEBUG, "TD0: cyl=%u; head=%u; secs=%u", cyl, head, secs);
    // read track
    memset(trkimg, 0, sizeof(trkimg));
    sn = 0;
    for (int sec = 0; sec < secs; ++sec) {
      FDCSector *csec = &trkimg[sn];
      uint8_t flags;
      if ((tdimageSize -= 5) < 0) goto fail;
      // standard CHRN
      csec->/*cyl*/trk = *img++;
      csec->/*side*/head = *img++;
      csec->sec = *img++;
      csec->/*len*/sz = *img++;
      flags = *img++;
      csec->type = 0xfb; // normal data marker
      if (csec->/*len*/sz > 5) {
        libfdcMsg(LIBFDC_MSG_ERROR, "invalid TD0 sector size byte (%u)", (unsigned)csec->sz);
        goto fail;
      }
      //if (csec->side == 0 && head == 1) csec->side = 1; // fix TR-DOS formatter bug
      if (flags&(TD0_SEC_NO_ID|TD0_SEC_NO_DATA|TD0_SEC_NO_DATA2)) {
        // sector without data or 'fabricated' (???)
        libfdcMsg(LIBFDC_MSG_DEBUG, "TD0: C:%u; H:%u; R:%u; N:%u; flags=0x%02x", csec->trk, csec->head, csec->sec, csec->sz, flags);
        libfdcMsg(LIBFDC_MSG_DEBUG, "TD0:   EMPTY SECTOR!");
        if (!(flags&(TD0_SEC_NO_DATA|TD0_SEC_NO_DATA2))) {
          // skip sector data
          int dsize;
          if ((tdimageSize -= 3) < 0) goto fail;
          ++img; // skip CRC
          dsize = getui16(img); img += 2;
          if ((tdimageSize -= dsize) < 0) goto fail;
          img += dsize;
          continue;
        }
      } else {
        int dsize, ssize = (128<<csec->/*len*/sz);
        if ((tdimageSize -= 3) < 0) goto fail;
        ++img; // skip CRC
        dsize = getui16(img); img += 2;
        libfdcMsg(LIBFDC_MSG_DEBUG, "TD0: C:%u; H:%u; R:%u; N:%u; flags=0x%02x; dsize=%d (%d left)", csec->trk, csec->head, csec->sec, csec->sz, flags, dsize, tdimageSize);
        if ((tdimageSize -= dsize) < 0) goto fail;
        if (--dsize <= 0) goto fail; // compression type
        // decompress sector data
        switch (*img++) {
          case 0: // raw data
            libfdcMsg(LIBFDC_MSG_DEBUG, "TD0:  raw data (%d)", dsize);
            if (dsize != ssize) goto fail;
            memcpy(csec->data, img, ssize);
            img += dsize;
            break;
          case 1: // repeated 2-byte pattern
            libfdcMsg(LIBFDC_MSG_DEBUG, "TD0:  repeated 2-byte pattern (%d)", dsize);
            if ((dsize -= 4) == 0) {
              int cnt = getui16(img);
              img += 2; // skip counter
              if (cnt*2 > ssize) cnt = ssize/2;
              libfdcMsg(LIBFDC_MSG_DEBUG, "TD0:   count=%d; data=0x%02x%02x", cnt, img[0], img[1]);
              for (int f = 0; f < cnt; ++f) {
                csec->data[f*2+0] = img[0];
                csec->data[f*2+1] = img[1];
              }
              img += 2; // skip data
            } else {
              goto fail;
            }
            break;
          case 2: // RLE compression
            libfdcMsg(LIBFDC_MSG_DEBUG, "TD0:  RLE (%d)", dsize);
            if (dsize > 0) {
              int dpos = 0;
              while (dsize-- > 0) {
                libfdcMsg(LIBFDC_MSG_DEBUG, "TD0:   type=%u", *img);
                switch (*img++) {
                  case 0: // literal block
                    libfdcMsg(LIBFDC_MSG_DEBUG, "TD0:  dsize=%d", dsize);
                    if (--dsize >= 0) {
                      int cnt = *img++;
                      //
                      libfdcMsg(LIBFDC_MSG_DEBUG, "TD0: count=%d", cnt);
                      if ((dsize -= cnt) < 0 || dpos+cnt > ssize) goto fail;
                      if (cnt > 0) memcpy(csec->data+dpos, img, cnt);
                      dpos += cnt;
                      img += cnt;
                    } else {
                      goto fail;
                    }
                    break;
                  case 1: // repeated fragment
                    libfdcMsg(LIBFDC_MSG_DEBUG, "TD0:  dsize=%d", dsize);
                    if ((dsize -= 3) >= 0) {
                      libfdcMsg(LIBFDC_MSG_DEBUG, "TD0: count=%u; data=0x%02x%02x", img[0], img[2], img[1]);
                      for (int cnt = *img++; cnt > 0; --cnt, dpos += 2) {
                        if (dpos+2 > ssize) goto fail;
                        csec->data[dpos+0] = img[0];
                        csec->data[dpos+1] = img[1];
                      }
                      img += 2;
                    } else {
                      goto fail;
                    }
                    break;
                  default: goto fail;
                }
              }
              if (dpos < ssize) libfdcMsg(LIBFDC_MSG_DEBUG, "TD0:  SECTOR NOT FILLED!");
            } else {
              goto fail;
            }
            break;
          default:
            libfdcMsg(LIBFDC_MSG_DEBUG, "TD0: TD0: invalid sector flags: %u", flags);
            goto fail;
        }
        ++sn;
      }
    }
    // cylinder read
    if (cyl < FLP_TRDOS_MAX_TRACKS_80/2 && head < 2 && sn > 0) {
      //fprintf(stderr, "C=%u; H=%u; SCNT=%u", cyl, head, secs);
      flpFormatTracks(flp, (cyl<<1)+head, trkimg, sn/*secs*/, FLP_FORMAT_TRACK_CALC_CRC);
    }
  }
  //fprintf(stderr, "OK!");
  free(tdimage);
  flp->protect = 0;
  flp->doubleSide = 1; //FIXME
  flp->trk80 = 1; //FIXME
  flp->insert = 1;
  flp->changed = 0;
  return 0;
fail:
  free(tdimage);
  flpFormatTRD(flp);
  flp->insert = 0;
  flp->changed = 0;
  return FLPERR_SHIT;
}

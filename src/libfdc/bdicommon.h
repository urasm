/*
 * coded by Ketmar // Invisible Vector
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 */
#ifndef LIBFDC_BDI_COMMON_H
#define LIBFDC_BDI_COMMON_H

#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


////////////////////////////////////////////////////////////////////////////////
#if defined(__GNUC__)
# ifndef LIBFDC_PACKED
#  define LIBFDC_PACKED  __attribute__((packed)) __attribute__((gcc_struct))
# endif
# ifndef LIBFDC_INLINE
#  define LIBFDC_INLINE  inline
# endif
# ifndef LIBFDC_MAYBE_UNUSED
#  define LIBFDC_MAYBE_UNUSED  __attribute__((unused))
# endif
#else
# ifndef LIBFDC_PACKED
#  define LIBFDC_PACKED
# endif
# ifndef LIBFDC_INLINE
#  define LIBFDC_INLINE
# endif
# ifndef LIBFDC_MAYBE_UNUSED
#  define LIBFDC_MAYBE_UNUSED
# endif
#endif

#ifdef __cplusplus
extern "C" {
#endif


enum {
  LIBFDC_MSG_DEBUG = 0,
  LIBFDC_MSG_WARNING = 1,
  LIBFDC_MSG_ERROR = 2,
};

// `type` is `LIBFDC_MSG_xxx`
// the buffer is static, and will be reused by the library!
// *NOT* terminated with '\n'!
extern void (*libfdcMessageCB) (int type, const char *msg);

void libfdcMsg (int type, const char *fmt, ...) __attribute__((format(printf,2,3)));


#ifdef __cplusplus
}
#endif
#endif

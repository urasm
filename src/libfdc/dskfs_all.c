/*
 * WD1793/uPD765 emulator
 * Copyright (c) 2009-..., SAM style
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is furnished
 * to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
 * THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * changes by Ketmar // Invisible Vector
 *
 * Understanding is not required. Only obedience.
 * git commit: 84b4cb3f749ff3cb954a202b3c65d7b53ee08273
 */
#include "dskfs_all.h"


//==========================================================================
//
//  flpDetectDiskType
//
//==========================================================================
FloppyDiskType flpDetectDiskType (Floppy *flp) {
  if (flp && flp->insert) {
    if (flpIsDiskTRDOS(flp)) return FLP_DISK_TYPE_TRDOS;
    if (flpIsDiskP3DOS(flp)) return FLP_DISK_TYPE_P3DOS;
  }
  return FLP_DISK_TYPE_UNKNOWN;
}

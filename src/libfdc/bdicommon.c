/*
 * coded by Ketmar // Invisible Vector
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 */
#include "bdicommon.h"

#include <stdarg.h>


void (*libfdcMessageCB) (int type, const char *msg) = NULL;


#define MSGBUF_SIZE  (1024)
static __thread char lfdcmsgbuf[MSGBUF_SIZE];


__attribute__((format(printf,2,3))) void libfdcMsg (int type, const char *fmt, ...) {
  if (!libfdcMessageCB) return;
  va_list va;
  va_start(va, fmt);
  vsnprintf(lfdcmsgbuf, sizeof(lfdcmsgbuf), fmt, va);
  lfdcmsgbuf[sizeof(lfdcmsgbuf)-1] = 0;
  va_end(va);
  libfdcMessageCB(type, lfdcmsgbuf);
}

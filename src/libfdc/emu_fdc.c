/*
 * WD1793/uPD765 emulator
 * Copyright (c) 2009-..., SAM style
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is furnished
 * to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
 * THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * changes by Ketmar // Invisible Vector
 *
 * Understanding is not required. Only obedience.
 * git commit: 84b4cb3f749ff3cb954a202b3c65d7b53ee08273
 */
#include "emu_fdc.h"


// ////////////////////////////////////////////////////////////////////////// //
struct DiskIF_t {
  int type; // DIF_xxx
  const DiskHW *hw;
  FDC *fdc;
  // currently, only bit 0 has a meaning
  // bit0: set to turn on "turbo mode" (see `FDC_FAST` constant)
  // WARNING! don't modify this directly, use `difSetTurbo()`
  // this is because the code has to do some housekeeping
  unsigned flags;
};


//==========================================================================
//
//  fdcUpdateCRC16
//
//  add byte to CRC
//
//==========================================================================
static void fdcUpdateCRC16 (FDC *fdc, uint8_t val) {
  uint32_t tkk = fdc->crc;
  tkk ^= val<<8;
  for (unsigned i = 8; i--; ) {
    if ((tkk *= 2)&0x10000) tkk ^= 0x1021;
  }
  fdc->crc = tkk&0xffff;
}


// ////////////////////////////////////////////////////////////////////////// //
#define LIBFDC_FDC_EMU_API  static

/* flag for fdcFlag: "turbo mode" */
#define FDC_IS_TURBO_MODE  (fdc->optionTurbo)

#include "emu_vg93.c"
#include "emu_upd765.c"


// ////////////////////////////////////////////////////////////////////////// //
struct DiskHW_t {
  const int id; /* hardware type */
  void (*reset) (DiskIF *dif);
  int (*in) (DiskIF *dif, int port);
  int (*out) (DiskIF *dif, int port, int val);
  void (*sync) (DiskIF *dif, int ns);
};


//**************************************************************************
//
// dummy (none)
//
//**************************************************************************
static void dumReset (DiskIF* dif) {}
static int dumIn (DiskIF *dif, int port) { return -1; }
static int dumOut (DiskIF *dif, int port, int val) { return -1; }
static void dumSync (DiskIF *dif, int ns) {}



//**************************************************************************
//
// common
//
//**************************************************************************

//==========================================================================
//
//  fdcSync
//
//==========================================================================
static void fdcSync (FDC *fdc, int ns) {
  if (fdc->plan == NULL) return; // fdc does nothing
  //fprintf(stderr, "fdcSync:000: wait=%d; tns=%d; ns=%d; plan=%p\n", fdc->wait, fdc->tns, ns, fdc->plan);
  fdc->wait -= ns;
  fdc->tns += ns;
  //fprintf(stderr, "fdcSync:001: wait=%d; tns=%d; ns=%d; plan=%p\n", fdc->wait, fdc->tns, ns, fdc->plan);
  while (fdc->wait < 0 && fdc->plan != NULL) {
    //fprintf(stderr, "  fdcSync:002: wait=%d; tns=%d; ns=%d; plan=%p\n", fdc->wait, fdc->tns, ns, fdc->plan);
    if (fdc->plan[fdc->pos] != NULL) {
      fdc->plan[fdc->pos](fdc);
    } else {
      fdc->plan = NULL;
    }
  }
}


//==========================================================================
//
//  dhwSync
//
//==========================================================================
static void dhwSync (DiskIF *dif, int ns) {
  fdcSync(dif->fdc, ns);
}



//**************************************************************************
//
// BDI (VG93)
//
//**************************************************************************

//==========================================================================
//
//  bdiGetPort
//
//==========================================================================
static int bdiGetPort (int port) {
  int res = 0;
  if ((port&0x9f) == 0x9f) {
    // 1xxxxx11 : bdi system port
    res = BDI_SYS;
  } else {
    // 0xxxxx11 : vg93 registers
    switch (port&0xff) {
      case 0x1f: res = FDC_COM; break;  // 000xxx11
      case 0x3f: res = FDC_TRK; break;  // 001xxx11
      case 0x5f: res = FDC_SEC; break;  // 010xxx11
      case 0x7f: res = FDC_DATA; break; // 011xxx11
    }
  }
  return res;
}


//==========================================================================
//
//  bdiIn
//
//==========================================================================
static int bdiIn (DiskIF *dif, int port) {
  //const int origport = port;
  port = bdiGetPort(port);
  if (port == 0) return -1; /* not FDC */
  int res;
  if (port == BDI_SYS) {
    res = ((dif->fdc->irq ? 0x80 : 0x00)|(dif->fdc->drq ? 0x40 : 0x00))&0xff;
  } else {
    res = vgRead(dif->fdc, port)&0xff;
  }
  //fprintf(stderr, "in BDI port #%04X (%02X); res=#%02X\n", (unsigned)origport, (unsigned)bdiGetPort(port), res&0xffU);
  return res&0xffU;
}


//==========================================================================
//
//  bdiOut
//
//==========================================================================
static int bdiOut (DiskIF *dif, int port, int val) {
  //fprintf(stderr, "out BDI port #%04X (%02X) <- #%02X\n", (unsigned)port, (unsigned)bdiGetPort(port), val&0xffU);
  port = bdiGetPort(port);
  if (port == 0) return -1;
  if (port == BDI_SYS) {
    dif->fdc->sysreg = val&0xffU;
    dif->fdc->flp = dif->fdc->flop[val&3]; // select floppy
    vgSetMR(dif->fdc, (val&0x04 ? 1 : 0)); // master reset
    dif->fdc->block = (val&0x08 ? 1 : 0);
    dif->fdc->side = (val&0x10 ? 0 : 1); // side
    dif->fdc->mfm = (val&0x40 ? 1 : 0);
  } else {
    vgWrite(dif->fdc, port, val);
  }
  return 0;
}


//==========================================================================
//
//  bdiReset
//
//==========================================================================
static void bdiReset (DiskIF *dif) {
  vgReset(dif->fdc);
  dif->fdc->sysreg = 0x20|0x08; // MFM
  bdiOut(dif, 0xff, 0x00);
}


//==========================================================================
//
//  bdiSync
//
//==========================================================================
/*
static void bdiSync (DiskIF *dif, int ns) {
  fdcSync(dif->fdc, ns);
}
*/



//**************************************************************************
//
// +3DOS (uPD765)
//
//**************************************************************************

//==========================================================================
//
//  pdosGetPort
//
//==========================================================================
static int pdosGetPort (int p) {
  int port = -1;
  if ((p&0xf002) == 0x2000) port = 0; // A0 input of upd765
  if ((p&0xf002) == 0x3000) port = 1; // 0:status(r), 1:data(rw)
  return port;
}


//==========================================================================
//
//  pdosIn
//
//==========================================================================
static int pdosIn (DiskIF *dif, int port) {
  port = pdosGetPort(port);
  if (port < 0) return -1;
  //fprintf(stderr, "in %.4X\n",port);
  return uRead(dif->fdc, port)&0xff;
}


//==========================================================================
//
//  pdosOut
//
//==========================================================================
static int pdosOut (DiskIF *dif, int port, int val) {
  port = pdosGetPort(port);
  if (port < 0) return -1;
  uWrite(dif->fdc, port, val);
  return 0;
}


//==========================================================================
//
//  pdosReset
//
//==========================================================================
static void pdosReset (DiskIF *dif) {
  uReset(dif->fdc);
}



//**************************************************************************
//
// common
//
//**************************************************************************
static const DiskHW dhwTab[4] = {
  {DIF_NONE,  &dumReset,  &dumIn,  &dumOut,  &dumSync},
  {DIF_BDI,   &bdiReset,  &bdiIn,  &bdiOut,  &dhwSync},
  {DIF_P3DOS, &pdosReset, &pdosIn, &pdosOut, &dhwSync},
  {DIF_END,   NULL,       NULL,    NULL,     NULL}
};


//==========================================================================
//
//  findDHW
//
//==========================================================================
static const DiskHW *findDHW (int id) {
  for (unsigned idx = 0; dhwTab[idx].id != DIF_END; ++idx) {
    if (dhwTab[idx].id == id) return &dhwTab[idx];
  }
  return NULL;
}


//==========================================================================
//
//  difSetHW
//
//==========================================================================
void difSetHW (DiskIF *dif, int type) {
  if (!dif) return;
  dif->hw = findDHW(type);
  if (!dif->hw) {
    //fprintf(stderr, "difSetHW: unknown interface type %d!\n", type);
    dif->hw = findDHW(DIF_NONE);
  }
  dif->type = dif->hw->id;
}


//==========================================================================
//
//  difGetHW
//
//==========================================================================
int difGetHW (const DiskIF *dif) {
  return (dif ? dif->type : DIF_NONE);
}


//==========================================================================
//
//  difGetFDC
//
//==========================================================================
FDC *difGetFDC (DiskIF *dif) {
  return (dif ? dif->fdc : NULL);
}


//==========================================================================
//
//  difGetFDC
//
//==========================================================================
Floppy *difGetFloppy (DiskIF *dif, int driveidx) {
  if (!dif || !dif->fdc || driveidx < 0 || driveidx > 3) return NULL;
  return dif->fdc->flop[driveidx];
}


//==========================================================================
//
//  difGetFDC
//
//==========================================================================
Floppy *difGetCurrentFloppy (DiskIF *dif) {
  if (!dif || !dif->fdc) return NULL;
  return dif->fdc->flp;
}


//==========================================================================
//
//  difSetTurbo
//
//==========================================================================
void difSetTurbo (DiskIF *dif, int v) {
  if (!dif) return;
  v = (v ? FDC_FAST : 0);
  if ((dif->flags&FDC_FAST) != v) {
    if (v) dif->flags |= FDC_FAST; else dif->flags &= ~((unsigned)FDC_FAST);
    if (dif->fdc) dif->fdc->optionTurbo = (v ? 1 : 0);
  }
}


//==========================================================================
//
//  difGetTurbo
//
//==========================================================================
int difGetTurbo (const DiskIF *dif) {
  return (dif && (dif->flags&FDC_FAST) ? 1 : 0);
}


//==========================================================================
//
//  difCreate
//
//==========================================================================
DiskIF *difCreate (int type) {
  DiskIF *dif = malloc(sizeof(DiskIF));
  memset(dif, 0, sizeof(DiskIF));
  dif->fdc = malloc(sizeof(FDC));
  memset(dif->fdc, 0x00, sizeof(FDC));
  dif->fdc->wait = -1;
  dif->fdc->plan = NULL;
  dif->fdc->flop[0] = flpCreate(0);
  dif->fdc->flop[1] = flpCreate(1);
  dif->fdc->flop[2] = flpCreate(2);
  dif->fdc->flop[3] = flpCreate(3);
  dif->fdc->flp = dif->fdc->flop[0];
  difSetHW(dif, type);
  return dif;
}


//==========================================================================
//
//  difDestroy
//
//==========================================================================
void difDestroy (DiskIF *dif) {
  if (!dif) return;
  flpDestroy(dif->fdc->flop[0]);
  flpDestroy(dif->fdc->flop[1]);
  flpDestroy(dif->fdc->flop[2]);
  flpDestroy(dif->fdc->flop[3]);
  dif->fdc->flp = NULL;
  free(dif->fdc);
  free(dif);
}


//==========================================================================
//
//  difReset
//
//==========================================================================
void difReset (DiskIF *dif) {
  if (!dif) return;
  dif->hw->reset(dif);
}


//==========================================================================
//
//  difCalcNSFromTStates
//
//  tstates: tstates passed
//  cpufreq: 3500000 for ZX Spectrum
//
//==========================================================================
int difCalcNSFromTStates (int tstates, int cpufreq) {
  // 3500000 must be converted to 3.5
  #if 0
  const double nspt = ((double)tstates*(double)1e3)/((double)cpufreq/(double)1e6);
  return (int)(nspt);
  #else
  // sadly, this can overflow, so use 64-bit integers
  return (int)((int64_t)tstates*1000LL*1000000LL/(int64_t)cpufreq);
  #endif
}


//==========================================================================
//
//  difSync
//
//  update disk interface (up to the given `ns`)
//  call this in emulation loop
//  `ns` is time passed from the last call (in nanoseconds)
//  it can be calculated with `difCalcNSFromTStates()`
//
//==========================================================================
void difSync (DiskIF *dif, int ns) {
  if (!dif || ns <= 0) return;
  dif->hw->sync(dif, ns);
}


//==========================================================================
//
//  difOut
//
//==========================================================================
int difOut (DiskIF *dif, int port, int val) {
  if (!dif) return -1;
  return dif->hw->out(dif, port, val);
}


//==========================================================================
//
//  difIn
//
//==========================================================================
int difIn (DiskIF *dif, int port) {
  if (!dif) return -1;
  return dif->hw->in(dif, port);
}

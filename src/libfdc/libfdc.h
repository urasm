/*
 * coded by Ketmar // Invisible Vector
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 */
#ifndef LIBFDC_MAIN_H
#define LIBFDC_MAIN_H


#include "bdicommon.h"
#include "floppy.h"
#include "dskfs_all.h"
#include "emu_fdc.h"
#include "dskldr.h"


#endif

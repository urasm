/*
 * disk images I/O
 * coded by Ketmar // Invisible Vector
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 */
// HoBeta
#include "dskldr.h"
#include "dskfs_all.h"


//==========================================================================
//
//  calcHobSum
//
//==========================================================================
static uint16_t calcHobSum (const void *hdr) {
  const uint8_t *buf = (const uint8_t *)hdr;
  uint16_t res = 0;
  for (unsigned int f = 0; f < 15; ++f) res += ((uint16_t)buf[f])*257+f;
  return res;
}


//==========================================================================
//
//  dskIsHoBetaBuf
//
//==========================================================================
int dskIsHoBetaBuf (const void *buf, int size) {
  const uint8_t *b = (const uint8_t *)buf;
  if (size < 17 || ((size-17)&0xff)) return 0;
  uint16_t csum = calcHobSum(buf);
  if ((csum&0xff) != b[15] || ((csum&0xff00)>>8) != b[16]) return 0;
  return 1;
}


//==========================================================================
//
//  dskLoadHoBeta
//
//==========================================================================
int dskLoadHoBeta (Floppy *flp, FILE *fl) {
  uint8_t secBuf[256];
  TRFile nfle;
  uint16_t csum;
  if (fl == NULL) return FLPERR_SHIT;
  if (!flp->insert) {
    if (flpFormatTRD(flp) != FLPERR_OK) {
      libfdcMsg(LIBFDC_MSG_ERROR, "cannot format emulated floppy");
      return FLPERR_SHIT;
    }
    flp->insert = 1;
  }
  if (flpDetectDiskType(flp) != FLP_DISK_TYPE_TRDOS) {
    libfdcMsg(LIBFDC_MSG_ERROR, "emulate floppy not a TR-DOS disk");
    return FLPERR_SHIT;
  }
  if (fread(secBuf, 17, 1, fl) != 1) {
    libfdcMsg(LIBFDC_MSG_ERROR, "cannot read HoBeta header");
    return FLPERR_SHIT;
  }
  csum = calcHobSum(secBuf);
  if ((csum&0xff) != secBuf[15] || ((csum&0xff00)>>8) != secBuf[16]) {
    libfdcMsg(LIBFDC_MSG_ERROR, "invalid HoBeta header checksum");
    fseek(fl, -17, SEEK_CUR);
    return FLPERR_SHIT;
  }
  memcpy(&nfle, secBuf, 13);
  nfle.slen = secBuf[14];
  if (flpCreateFile(flp, &nfle) != FLPERR_OK) {
    libfdcMsg(LIBFDC_MSG_ERROR, "cannot create file on TR-DOS disk");
    return FLPERR_SHIT;
  }
  for (int i = 0; i < nfle.slen; ++i) {
    if (fread(secBuf, 256, 1, fl) != 1) {
      libfdcMsg(LIBFDC_MSG_ERROR, "cannot read HoBeta data");
      return FLPERR_SHIT;
    }
    if (flpPutSectorData(flp, nfle.trk, nfle.sec+1, secBuf, 256) != FLPERR_OK) {
      libfdcMsg(LIBFDC_MSG_ERROR, "cannot write HoBeta data to TR-DOS disk");
      return FLPERR_SHIT;
    }
    ++nfle.sec;
    if (nfle.sec > 15) {
      ++nfle.trk;
      nfle.sec -= 16;
    }
  }
  flp->changed = 1;
  return FLPERR_OK;
}


//==========================================================================
//
//  dskSaveHoBeta
//
//==========================================================================
int dskSaveHoBeta (Floppy *flp, FILE *fl, int catidx) {
  uint8_t buf[256]; // header/sector
  uint16_t csum;
  TRFile tfl;
  uint8_t tr, sc;
  if (fl == NULL) return FLPERR_SHIT;
  if (flpGetCatalogEntry(flp, &tfl, catidx) != FLPERR_OK) return FLPERR_SHIT;
  memcpy(buf, tfl.name, 13);
  buf[13] = 0x00;
  buf[14] = tfl.slen;
  csum = calcHobSum(buf);
  buf[15] = (csum&0xff);
  buf[16] = ((csum&0xff00)>>8);
  if (fwrite(buf, 17, 1, fl) != 1) return FLPERR_SHIT;
  tr = tfl.trk;
  sc = tfl.sec;
  for (unsigned int f = 0; f < tfl.slen; ++f) {
    if (flpGetSectorData(flp, tr, sc+1, buf, 256) != FLPERR_OK) return FLPERR_SHIT;
    if (fwrite(buf, 256, 1, fl) != 1) return FLPERR_SHIT;
    if (++sc > 15) { ++tr; sc = 0; }
  }
  return FLPERR_OK;
}

/*
 * WD1793/uPD765 emulator
 * Copyright (c) 2009-..., SAM style
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is furnished
 * to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
 * THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * changes by Ketmar // Invisible Vector
 *
 * Understanding is not required. Only obedience.
 * git commit: 84b4cb3f749ff3cb954a202b3c65d7b53ee08273
 */
#define FDC_TIME_BYTEDELAY  (32000) /* 300 turns/min = 5 turns/sec = 31250 bytes/sec = 32mks/byte */
#define FDC_TIME_TURBOBYTE  (500) /* same for FDC_IS_TURBO_MODE */

// NOTE: cdb4 = crc for A1,A1,A1
// init value of crc must be FFFF, accumulation starting from 1st A1

static const int pauses[4] = {6000, 12000, 20000, 30000}; // pause in ns for 1st type commands

// 1818vg93

//==========================================================================
//
//  rdFCRC
//
//  read FCRC from disk (hi-low)
//
//==========================================================================
static void rdFCRC (FDC *fdc) {
  fdc->fcrc = (flpRd(fdc->flp, fdc->side)<<8);
  flpNext(fdc->flp, fdc->side);
  fdc->fcrc |= flpRd(fdc->flp, fdc->side);
  flpNext(fdc->flp, fdc->side);
}


//==========================================================================
//
//  wrCRC
//
//  save CRC to disk (hi-low)
//
//==========================================================================
static void wrCRC (FDC *fdc) {
  flpWr(fdc->flp, fdc->side, (fdc->crc>>8)&0xff);
  flpNext(fdc->flp, fdc->side);
  flpWr(fdc->flp, fdc->side, fdc->crc&0xff);
  flpNext(fdc->flp, fdc->side);
}


//==========================================================================
//
//  waitADR
//
//  wait ADR
//  return: 0 : not ADR, 1 : ADR, 2 : IDX
//
//==========================================================================
static int waitADR (FDC *fdc) {
  fdc->wait += (FDC_IS_TURBO_MODE ? FDC_TIME_TURBOBYTE : FDC_TIME_BYTEDELAY);
  if (flpNext(fdc->flp, fdc->side)) return 2; // 2:IDX
  if (fdc->flp->field != 1) return 0; // 0:not ADR
  return 1;
}


//==========================================================================
//
//  vg_seekADR
//
//  wait ADR mark & read to BUF[4] and FCRC
//  return: 0 : not ADR, 1 : ADR readed in buf + fcrc, 2 : IDX
//
//  HACK: public, because uPD765 needs it too
//
//==========================================================================
LIBFDC_FDC_EMU_API int vg_seekADR (FDC *fdc) {
  int res = waitADR(fdc);
  if (res != 1) return res;
  fdc->crc = 0xcdb4;
  fdcUpdateCRC16(fdc, 0xfe);
  for (unsigned i = 0; i < 4; ++i) {
    fdc->buf[i] = flpRd(fdc->flp, fdc->side);
    fdcUpdateCRC16(fdc, fdc->buf[i]);
    flpNext(fdc->flp, fdc->side);
  }
  rdFCRC(fdc);
  fdc->wait += (FDC_IS_TURBO_MODE ? 1 : (6*FDC_TIME_BYTEDELAY)); // delay : 6 byte reading
  return 1; // 1:ADR
}


//==========================================================================
//
//  vgSendByte
//
//  send byte FDC->Floppy (wr commands)
//
//==========================================================================
static int vgSendByte (FDC *fdc) {
  int res = 0;
  if (FDC_IS_TURBO_MODE) {
    if (fdc->drq) {
      if (fdc->tns > FDC_TIME_BYTEDELAY) {
        fdc->state |= 0x04;
        flpNext(fdc->flp, fdc->side);
        fdc->tns = 0;
        res = 1;
      }
    } else {
      fdcUpdateCRC16(fdc, fdc->data);
      flpWr(fdc->flp, fdc->side, fdc->data);
      flpNext(fdc->flp, fdc->side);
      fdc->drq = 1;
      fdc->tns = 1;
      res = 1;
    }
    fdc->wait = 1;
  } else {
    if (fdc->drq) fdc->state |= 0x04; // time to write the byte, but we didn't got it from the CPU
    fdcUpdateCRC16(fdc, fdc->data);
    flpWr(fdc->flp, fdc->side, fdc->data);
    flpNext(fdc->flp, fdc->side);
    fdc->drq = 1;
    fdc->wait += FDC_TIME_BYTEDELAY;
    fdc->tns = 0;
    res = 1;
  }
  return res;
}


//==========================================================================
//
//  vgGetByte
//
//  get byte Floppy->FDC (rd commands)
//
//==========================================================================
static int vgGetByte (FDC *fdc) {
  int res = 0;
  if (FDC_IS_TURBO_MODE) {
    if (fdc->drq) {
      if (fdc->tns > FDC_TIME_BYTEDELAY) {
        fdc->state |= 0x04;
        flpNext(fdc->flp, fdc->side);
        fdc->tns = 0;
        res = 1;
      }
    } else {
      fdc->data = flpRd(fdc->flp, fdc->side);
      flpNext(fdc->flp, fdc->side);
      fdc->drq = 1;
      fdc->tns = 0;
      res = 1;
    }
    fdc->wait = 1; // check every time (no delay)
  } else {
    if (fdc->drq) fdc->state |= 0x04; // data lost - time to read the next byte, but previous one wasn't transfered
    fdc->data = flpRd(fdc->flp, fdc->side);
    flpNext(fdc->flp, fdc->side);
    fdc->drq = 1;
    fdc->wait += FDC_TIME_BYTEDELAY;
    fdc->tns = 0;
    res = 1;
  }
  return res;
}


//==========================================================================
//
//  vgwait
//
//  wait fdc->cnt ms & spin flop if motor is on
//
//==========================================================================
static void vgwait (FDC *fdc) {
  fdc->cnt -= FDC_TIME_BYTEDELAY;
  if (fdc->cnt < 0) {
    ++fdc->pos;
  } else {
    fdc->wait += FDC_TIME_BYTEDELAY;
    if (fdc->flp->motor) flpNext(fdc->flp, fdc->side);
  }
}


//**************************************************************************
//
// idle : wait 15 IDX pulses & stop motor
//
//**************************************************************************

//==========================================================================
//
//  vgstp00
//
//==========================================================================
static void vgstp00 (FDC *fdc) {
  //printf("stop\n");
  fdc->irq = 1;
  fdc->idle = 1;
  if (!fdc->flp->motor || !fdc->flp->insert) {
    fdc->plan = NULL;
  } else {
    fdc->cnt = 15;
    ++fdc->pos;
  }
}


//==========================================================================
//
//  vgstp01
//
//==========================================================================
static void vgstp01 (FDC *fdc) {
  fdc->wait = (FDC_IS_TURBO_MODE ? FDC_TIME_TURBOBYTE : FDC_TIME_BYTEDELAY);
  if (flpNext(fdc->flp, fdc->side)) {
    // turn floppy to next byte, check IDX
    --fdc->cnt;
    if (fdc->cnt < 1) {
      // check 15 IDX pulses
      fdc->flp->motor = 0; // and off
      fdc->plan = NULL;
    }
  }
}


static const fdcCall vgStop[3] = {&vgstp00, &vgstp01, NULL};

//==========================================================================
//
//  vgstp
//
//==========================================================================
static void vgstp (FDC *fdc) {
  fdc->plan = vgStop;
  fdc->pos = 0;
}



//**************************************************************************
//
// check head pos (h=1, v=1, motor)
//
//**************************************************************************

//==========================================================================
//
//  vgchk00
//
//  prepare : check if check is on, if disk inserted, set IDX count to 9
//
//==========================================================================
static void vgchk00 (FDC *fdc) {
  if ((fdc->com&0x0c) != 0x0c) {
    // if (h=0 || v=0), stops
    vgstp(fdc);
  } else if (!fdc->flp->insert) {
    // if no disk, seek error, stop
    fdc->state |= 0x10;
    vgstp(fdc);
  } else {
    fdc->cnt = 9; // try seek ADR in 9 spins
    ++fdc->pos;
  }
}


//==========================================================================
//
//  vgchk01
//
//  read ADR mark in cnt spins, change side every IDX, read ADR to buf
//
//==========================================================================
static void vgchk01 (FDC *fdc) {
  int res = vg_seekADR(fdc);
  if (res == 2) {
    fdc->side ^= 1;
    --fdc->cnt;
    if (fdc->cnt < 1) {
      fdc->state |= 0x10; // SEEK error
      vgstp(fdc);
    }
  } else if (res == 1 && fdc->buf[0] == fdc->trk) {
    ++fdc->pos;
  }
}


//==========================================================================
//
//  vgchk02
//
//  check CRC
//
//==========================================================================
static void vgchk02 (FDC *fdc) {
  if (fdc->crc != fdc->fcrc) fdc->state |= 0x08; // crc error
  ++fdc->pos;
}


static const fdcCall vgCheck[5] = {&vgchk00, &vgchk01, &vgchk02, &vgstp, NULL}; // prepare, seek/read ADR, check CRC, stop


//==========================================================================
//
//  vgchk
//
//==========================================================================
static void vgchk (FDC *fdc) {
  fdc->plan = vgCheck;
  fdc->pos = 0;
}


//**************************************************************************
//
// restore
//
//**************************************************************************

//==========================================================================
//
//  vgres00
//
//  prepare, do start delay (h=1)
//
//==========================================================================
static void vgres00 (FDC *fdc) {
  fdc->fmode = 0;
  fdc->trk = 0xff;
  fdc->cnt = 1000000; // delay for BV
  if (fdc->com&8) {
    // if h=1 : start motor, pause 15 ms
    fdc->wait += (FDC_IS_TURBO_MODE ? 5000 : 15000);
    fdc->flp->motor = 1;
  }
  ++fdc->pos;
}


//==========================================================================
//
//  vgres01
//
//  do step in until TRK0 or Rtrk=0
//
//==========================================================================
static void vgres01 (FDC *fdc) {
  if (fdc->flp->trk == 0 || fdc->trk == 0) {
    if (fdc->flp->trk != 0) fdc->state |= 0x10;
    fdc->trk = 0;
    ++fdc->pos;
  } else {
    fdc->wait += (FDC_IS_TURBO_MODE ? 1 : pauses[fdc->com&3]);
    --fdc->trk;
    flpStep(fdc->flp, FLP_BACK);
  }
}


static const fdcCall vgRest[5] = {&vgres00, &vgwait, &vgres01, &vgchk, NULL};



//**************************************************************************
//
// seek
//
//**************************************************************************

//==========================================================================
//
//  vgseek00
//
//==========================================================================
static void vgseek00 (FDC *fdc) {
  fdc->fmode = 0;
  fdc->cnt = 1000000; // 1ms delay for BV :)
  if (fdc->com&8) {
    fdc->wait += (FDC_IS_TURBO_MODE ? 1 : 15000);
    fdc->flp->motor = 1;
  }
  ++fdc->pos;
}


//==========================================================================
//
//  vgseek01
//
//==========================================================================
static void vgseek01 (FDC *fdc) {
  if (fdc->trk == fdc->data) {
    ++fdc->pos;
  } else if (fdc->trk < fdc->data) {
    flpStep(fdc->flp, FLP_FORWARD);
    ++fdc->trk;
    fdc->wait += (FDC_IS_TURBO_MODE ? FDC_TIME_TURBOBYTE : FDC_TIME_BYTEDELAY);
  } else {
    flpStep(fdc->flp, FLP_BACK);
    fdc->trk--;
    fdc->wait += (FDC_IS_TURBO_MODE ? FDC_TIME_TURBOBYTE : FDC_TIME_BYTEDELAY);
  }
}


static const fdcCall vgSeek[5] = {&vgseek00, &vgwait, &vgseek01, &vgchk, NULL};



//**************************************************************************
//
// step/step forward/step back
//
//**************************************************************************

//==========================================================================
//
//  vgstpf
//
//==========================================================================
static void vgstpf (FDC *fdc) {
  fdc->step = 1;
  ++fdc->pos;
}


//==========================================================================
//
//  vgstpb
//
//==========================================================================
static void vgstpb (FDC *fdc) {
  fdc->step = 0;
  ++fdc->pos;
}


static void vgstep (FDC *fdc) {
  flpStep(fdc->flp, (fdc->step ? FLP_FORWARD : FLP_BACK));
  fdc->wait += (FDC_IS_TURBO_MODE ? 1 : pauses[fdc->com&3]);
  if (fdc->com&0x10) {
    if (fdc->step) ++fdc->trk; else --fdc->trk;
  }
  ++fdc->pos;
}


static const fdcCall vgStepF[5] = {&vgseek00, &vgstpf, &vgstep, &vgchk, NULL};
static const fdcCall vgStepB[5] = {&vgseek00, &vgstpb, &vgstep, &vgchk, NULL};
static const fdcCall vgStep[4] = {&vgseek00, &vgstep, &vgchk, NULL};



//**************************************************************************
//
// read sectors
//
//**************************************************************************

//==========================================================================
//
//  vgrdsDBG
//
//==========================================================================
/*
static void vgrdsDBG (FDC *fdc) {
  ++fdc->pos;
}
*/


//==========================================================================
//
//  vgrds00
//
//  prepare
//
//==========================================================================
static void vgrds00 (FDC *fdc) {
  //if ((fdc->com&0xe1) == 0x80) fprintf(stderr, "RDSec(%.2X)...T:%.2X S:%.2X H:%i (FT:%.2X)\n", fdc->com, fdc->trk, fdc->sec, fdc->side, fdc->flp->trk);
  //fprintf(stderr, "fdc com %.2X\n", fdc->com);
  fdc->fmode = 1;
  if (!fdc->flp->insert) {
    vgstp(fdc);
  } else {
    fdc->flp->motor = 1;
    if (fdc->com&4) fdc->wait += 15000; // if (e=0) pause 15ms
    fdc->cnt = 5; // seek sector in 5 spins
    ++fdc->pos;
  }
}


//==========================================================================
//
//  vgrds01
//
//  seek right sector ADR
//
//==========================================================================
static void vgrds01 (FDC *fdc) {
  int res = vg_seekADR(fdc);
  if (res == 2) {
    --fdc->cnt;
    if (fdc->cnt < 1) {
      fdc->state |= 0x10; // sector not found
      vgstp(fdc);
    }
  } else if (res == 1) {
    if (fdc->buf[0] == fdc->trk && fdc->buf[2] == fdc->sec) {
      // check TRK,SEC
      if ((~fdc->com&2) || fdc->buf[1] == (fdc->com&8 ? 1 : 0)) {
        // check HEAD (s, if c=1)
        if (fdc->crc != fdc->fcrc) {
          // check CRC
          fdc->state |= 0x08; // ADR crc error
          vgstp(fdc);
        } else {
          fdc->cnt = 52; // DATA must be in next (22 + 30) bytes
          ++fdc->pos; // ADR found, next step
        }
      }
    }
  }
}


//==========================================================================
//
//  vgrds02
//
//  seek sector DATA in next CNT bytes, else - array not found
//
//==========================================================================
static void vgrds02 (FDC *fdc) {
  if (fdc->cnt > 0) {
    fdc->tmp = flpRd(fdc->flp, fdc->side);
    flpNext(fdc->flp, fdc->side);
    fdc->wait += (FDC_IS_TURBO_MODE ? FDC_TIME_TURBOBYTE : FDC_TIME_BYTEDELAY);
    if (fdc->flp->field == 2 || fdc->flp->field == 3) {
      fdc->buf[4] = fdc->tmp;
      ++fdc->pos;
      fdc->wait = 0;
    }
    --fdc->cnt;
  } else {
    fdc->state |= 0x10; // sector not found (array not found)
    vgstp(fdc);
  }
}


//==========================================================================
//
//  vgrds03
//
//  init crc, set sector size
//
//==========================================================================
static void vgrds03 (FDC *fdc) {
  if (fdc->buf[4] == 0xf8) fdc->state |= 0x20;
  fdc->crc = 0xcdb4;
  fdcUpdateCRC16(fdc, fdc->buf[4]); // add DATA mark (F8 | FB)
  fdc->cnt = (0x80<<(fdc->buf[3]&3)); // sector size (128,256,512,1024)
  fdc->drq = 0;
  fdc->dir = 1; // dir: FDC to CPU
  fdc->wait = FDC_TIME_BYTEDELAY;
  ++fdc->pos;
  fdc->tns = 0;
  //fprintf(stderr, "vgrds03: %i.%i.%i\n", fdc->trk, fdc->sec, fdc->cnt);
}


// transfer CNT bytes flp->cpu
static void vgrds04 (FDC *fdc) {
  if (!vgGetByte(fdc)) return;
  fdcUpdateCRC16(fdc, fdc->data);
  --fdc->cnt;
  if (fdc->cnt < 1) {
    fdc->wait = FDC_TIME_BYTEDELAY;
    ++fdc->pos;
  }
}


//==========================================================================
//
//  vgrds05
//
//  end: read, compare CRC, check multisector, stop
//
//==========================================================================
static void vgrds05 (FDC *fdc) {
  rdFCRC(fdc);
  fdc->wait += (FDC_IS_TURBO_MODE ? 1 : (2*FDC_TIME_BYTEDELAY));
  if (fdc->crc != fdc->fcrc) {
    //fprintf(stderr, "crc error\n");
    fdc->state |= 0x08;
    ++fdc->pos;
  } else if (fdc->com&0x10) {
    ++fdc->sec;
    //fprintf(stderr, "sec %i\n", fdc->sec);
    fdc->pos = 1;
    fdc->cnt = 5;
  } else {
    ++fdc->pos;
  }
}


static const fdcCall vgRdSec[8] = {&vgrds00, &vgrds01, &vgrds02, &vgrds03, &vgrds04, &vgrds05, &vgstp, NULL};



//**************************************************************************
//
// write sectors
//
//**************************************************************************

//==========================================================================
//
//  vgwrs00
//
//  check write protect
//
//==========================================================================
static void vgwrs00 (FDC *fdc) {
  if (fdc->flp->protect) {
    fdc->state |= 0x40;
    vgstp(fdc);
  } else {
    fdc->drq = 1;
    fdc->dir = 0;
    ++fdc->pos;
  }
}


//==========================================================================
//
//  vgwrs01
//
//  init crc, set F8/F9 (need roll flp back), prepare to write data
//
//==========================================================================
static void vgwrs01 (FDC *fdc) {
  flpPrev(fdc->flp, fdc->side); // back to DATA mark
  fdc->crc = 0xcdb4;
  fdc->tmp = (fdc->com&1 ? 0xf8 : 0xfb);
  fdcUpdateCRC16(fdc, fdc->tmp);
  flpWr(fdc->flp, fdc->side, fdc->tmp);
  flpNext(fdc->flp, fdc->side);
  fdc->cnt = 0x80<<(fdc->buf[3]&3); // sector size
  fdc->wait = FDC_TIME_BYTEDELAY;
  fdc->tns = 0;
  ++fdc->pos;
}


//==========================================================================
//
//  vgwrs02
//
//  write CNT bytes
//
//==========================================================================
static void vgwrs02 (FDC *fdc) {
  if (!vgSendByte(fdc)) return;
  --fdc->cnt;
  if (fdc->cnt < 1) {
    fdc->drq = 0;
    ++fdc->pos;
  }
}


//==========================================================================
//
//  vgwrs03
//
//  write crc, check multisector, end
//
//==========================================================================
static void vgwrs03 (FDC *fdc) {
  wrCRC(fdc);
  fdc->wait += (FDC_IS_TURBO_MODE ? 1 : (2*FDC_TIME_BYTEDELAY));
  if (fdc->com&0x10) {
    ++fdc->sec;
    fdc->pos = 2;
    fdc->cnt = 5;
  } else {
    ++fdc->pos;
  }
}


static const fdcCall vgWrSec[9] = {&vgrds00, &vgwrs00, &vgrds01, &vgrds02, &vgwrs01, &vgwrs02, &vgwrs03, &vgstp, NULL};



//**************************************************************************
//
// read address
//
//**************************************************************************

//==========================================================================
//
//  vgrda00
//
//==========================================================================
static void vgrda00 (FDC *fdc) {
  int res = waitADR(fdc);
  if (res == 2) {
    --fdc->cnt;
    if (fdc->cnt < 1) {
      fdc->state |= 0x10; // sector not found
      vgstp(fdc);
    }
  } else if (res == 1) {
    fdc->fcrc = 0;
    fdc->crc = 0xcdb4;
    fdcUpdateCRC16(fdc, 0xfe); // add ADR mark
    fdc->cnt = 6; // send 6 bytes FDC->CPU
    fdc->drq = 0;
    fdc->dir = 1;
    fdc->wait = FDC_TIME_BYTEDELAY;
    ++fdc->pos;
  }
}


//==========================================================================
//
//  vgrda01
//
//  read CNT bytes, last 2 bytes is CRC
//
//==========================================================================
static void vgrda01 (FDC *fdc) {
  if (!vgGetByte(fdc)) return;
  --fdc->cnt;
  switch (fdc->cnt) {
    case 0:
      fdc->fcrc |= (fdc->data&0xff);
      fdc->wait = FDC_TIME_BYTEDELAY;
      --fdc->pos;
      break;
    case 1:
      fdc->fcrc = (fdc->data<<8)&0xff00;
      break;
    case 5:
      fdc->sec = fdc->data;
    default:
      fdcUpdateCRC16(fdc, fdc->data);
      break;
  }
}


static const fdcCall vgRdAdr[6] = {&vgrds00, &vgrda00, &vgrda01, &vgchk02, &vgstp, NULL};



//**************************************************************************
//
// read track
//
//**************************************************************************

//==========================================================================
//
//  vgrdt00
//
//  wait IDX
//
//==========================================================================
static void vgrdt00 (FDC *fdc) {
  fdc->wait += (FDC_IS_TURBO_MODE ? FDC_TIME_TURBOBYTE : FDC_TIME_BYTEDELAY);
  if (flpNext(fdc->flp, fdc->side)) {
    fdc->drq = 0;
    fdc->dir = 1;
    ++fdc->pos;
  }
}


//==========================================================================
//
//  vgrdt01
//
//==========================================================================
static void vgrdt01 (FDC *fdc) {
  if (!vgGetByte(fdc)) return;
  if (fdc->flp->pos == 0) ++fdc->pos; // end of trk (same as IDX)
}


static const fdcCall vgRdTrk[5] = {&vgrds00, &vgrdt00, &vgrdt01, &vgstp, NULL};



//**************************************************************************
//
// write track
//
//**************************************************************************

//==========================================================================
//
//  vgwrt00
//
//==========================================================================
static void vgwrt00 (FDC *fdc) {
  fdc->wait += (FDC_IS_TURBO_MODE ? FDC_TIME_TURBOBYTE : FDC_TIME_BYTEDELAY);
  if (flpNext(fdc->flp, fdc->side)) {
    ++fdc->pos;
    fdc->wait = FDC_TIME_BYTEDELAY;
  }
}


//==========================================================================
//
//  vgwrt01
//
//==========================================================================
static void vgwrt01 (FDC *fdc) {
  if (!vgSendByte(fdc)) return;
  if (fdc->flp->pos == 0) {
    fdc->drq = 0;
    ++fdc->pos;
  }
}


//==========================================================================
//
//  vgwrt02
//
//==========================================================================
static void vgwrt02 (FDC *fdc) {
  flpFillFields(fdc->flp, (fdc->flp->trk<<1)|fdc->side, FLP_FILL_FIELDS_ALL);
  ++fdc->pos;
}


static const fdcCall vgWrTrk[7] = {&vgrds00, &vgwrs00, &vgwrt00, &vgwrt01, &vgwrt02, &vgstp, NULL};



//**************************************************************************
//
// plans for various commands
//
//**************************************************************************

typedef struct {
  int mask;
  int val;
  const fdcCall *plan;
} vgComItem;

static const vgComItem vgComTab[11] = {
  {0xf0, 0x00, vgRest},  // 0000xxxx - restore
  {0xf0, 0x10, vgSeek},  // 0001xxxx - seek
  {0xe0, 0x20, vgStep},  // 001xxxxx - step
  {0xe0, 0x40, vgStepF}, // 010xxxxx - step forward
  {0xe0, 0x60, vgStepB}, // 011xxxxx - step back
  {0xe1, 0x80, vgRdSec}, // 100xxxx0 - read sectors
  {0xe0, 0xa0, vgWrSec}, // 101xxxxx - write sectors
  {0xfb, 0xc0, vgRdAdr}, // 11000x00 - read address
  {0xfb, 0xe0, vgRdTrk}, // 11100x00 - read track
  {0xfb, 0xf0, vgWrTrk}, // 11110x00 - write track
  {0x00, 0x00, vgStop}   // othercom - do nothing
};


//==========================================================================
//
//  vgExec
//
//==========================================================================
static void vgExec (FDC *fdc, uint8_t com) {
  int idx;
  if ((com&0xf0) == 0xd0) {
    // interrupt (doesn't mind about FDC is idle)
    //fprintf(stderr, "INT:%.2X\n", com);
    fdc->wait = 0;
    vgstp(fdc);
  } else if (fdc->idle) {
    // if FDC is idle
    fdc->com = com;
    idx = 0;
    for (;;) { // vgComTab[idx].mask != 0
      if ((com&vgComTab[idx].mask) == vgComTab[idx].val) {
        fdc->plan = vgComTab[idx].plan;
        fdc->wait = 0;
        fdc->pos = 0;
        fdc->idle = 0;
        fdc->state = 0;
        fdc->irq = 0;
        break;
      }
      ++idx;
    }
  }
}



//**************************************************************************
//
// public API
//
//**************************************************************************

//==========================================================================
//
//  vgWrite
//
//==========================================================================
LIBFDC_FDC_EMU_API void vgWrite (FDC *fdc, int addr, uint8_t value) {
  switch (addr) {
    case FDC_COM:
      if (!fdc->mr) break; // no commands during master reset
      vgExec(fdc, value);
      break;
    case FDC_TRK:
      fdc->trk = value;
      break;
    case FDC_SEC:
      fdc->sec = value;
      break;
    case FDC_DATA:
      fdc->data = value;
      fdc->drq = 0;
      break;
  }
}


//==========================================================================
//
//  vgRead
//
//==========================================================================
LIBFDC_FDC_EMU_API uint8_t vgRead (FDC *fdc, int addr) {
  uint8_t res = 0xff;
  switch (addr) {
    case FDC_COM:
      //fdc->state &= ~0x08; // debug: reset crc error
      fdc->state &= 0x7e;
      if (!fdc->flp->insert) fdc->state |= 0x80;
      if (!fdc->idle) fdc->state |= 0x01;
      if (fdc->fmode == 0) {
        fdc->state &= 0x99;
        if (fdc->flp->protect) fdc->state |= 0x40;
        if (fdc->flp->motor) fdc->state |= 0x20;
        if (fdc->flp->trk == 0) fdc->state |= 0x04;
        if (fdc->flp->insert && fdc->flp->motor && fdc->flp->index) fdc->state |= 0x02;
      } else if (fdc->fmode == 1) {
        fdc->state &= 0xfd;
        if (fdc->drq) fdc->state |= 0x02;
      }
      res = fdc->state;
      //fprintf(stderr, "in 1F = %.2X\n", res);
      break;
    case FDC_TRK:
      res = fdc->trk;
      break;
    case FDC_SEC:
      res = fdc->sec;
      break;
    case FDC_DATA:
      res = fdc->data;
      fdc->drq = 0;
      //fprintf(stderr, "%.2X ", res);
      break;
  }
  return res;
}


//==========================================================================
//
//  vgReset
//
//==========================================================================
LIBFDC_FDC_EMU_API void vgReset (FDC *fdc) {
  fdc->trk = 0;
  fdc->sec = 0;
  fdc->data = 0;
  fdc->state = 0;
  fdc->idle = 0;
  fdc->irq = 1;
  fdc->drq = 0;
  fdc->side = 0;
  fdc->plan = NULL;
  fdc->pos = 0;
  fdc->wait = -1;
}


//==========================================================================
//
//  vgSetMR
//
//==========================================================================
LIBFDC_FDC_EMU_API void vgSetMR (FDC *fdc, int z) {
  fdc->mr = z;
  if (z == 0) {
    fdc->idle = 1;
    vgExec(fdc, 0x03); // restore
    fdc->sec = 1;
  }
}

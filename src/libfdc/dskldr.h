/*
 * WD1793/uPD765 emulator
 * Copyright (c) 2009-..., SAM style
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is furnished
 * to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
 * THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * changes by Ketmar // Invisible Vector
 *
 * Understanding is not required. Only obedience.
 */
#ifndef LIBFDC_DSKLDR_H
#define LIBFDC_DSKLDR_H

#include "bdicommon.h"
#include "floppy.h"

#ifdef __cplusplus
extern "C" {
#endif


////////////////////////////////////////////////////////////////////////////////
int dskLoadTRD (Floppy *flp, FILE *fl);
int dskSaveTRD (Floppy *flp, FILE *fl);

int dskLoadSCL (Floppy *flp, FILE *fl);
int dskSaveSCL (Floppy *flp, FILE *fl);

int dskLoadHoBeta (Floppy *flp, FILE *fl);
int dskSaveHoBeta (Floppy *flp, FILE *fl, int catidx);
int dskIsHoBetaBuf (const void *buf, int size); // returns bool


////////////////////////////////////////////////////////////////////////////////
int dskLoadUDI (Floppy *flp, FILE *fl);
int dskSaveUDI (Floppy *flp, FILE *fl);

int dskLoadFDI (Floppy *flp, FILE *fl);
int dskSaveFDI (Floppy *flp, FILE *fl);

int dskLoadTD0 (Floppy *flp, FILE *fl);


////////////////////////////////////////////////////////////////////////////////
int dskLoadDSK (Floppy *flp, FILE *fl);
int dskSaveDSK (Floppy *flp, FILE *fl);


#ifdef __cplusplus
}
#endif
#endif

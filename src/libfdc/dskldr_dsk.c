/*
 * disk images I/O
 * based on the code by SAM style
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 */
#include "dskldr.h"

//#include "filetypes.h"

// +0x00 signature (23)
// +0x17 DiskInfo\r\n
// +0x22 disk info block

typedef struct LIBFDC_PACKED {
  uint8_t byte0d0f[4]; // unused
  uint8_t track;
  uint8_t side;
  uint8_t dataRate; // unknown, SD, HD, ED
  uint8_t recMode;  // unknown, FM, MFM
  uint8_t secSize;
  uint8_t secCount;
  uint8_t gap3Size;
  uint8_t filler;
  uint8_t sectorInfo[0x100-0x18];  // max 29 (x 8 bytes)
} TrackInfBlock;


typedef struct LIBFDC_PACKED {
  uint8_t track;
  uint8_t side;
  uint8_t sector;
  uint8_t size;
  uint8_t sr1;
  uint8_t sr2;
  uint8_t bslo;
  uint8_t bshi;
} SectorInfBlock; // 8 bytes


static const char edsksgn[8] = "EXTENDED";
static const char dsksgn[8] =  "MV - CPC";


int dskLoadDSK (Floppy *flp, FILE *fl) {
  //fprintf(stderr, "*** loading DSK (000) (fpl=%p; ins=%d) ***\n", flp, (flp ? flp->insert : -1));
  if (!flp) return FLPERR_SHIT;
  if (fl == NULL) return FLPERR_SHIT;

  FDCSector secs[256];
  char sigBuf[256];

  //fprintf(stderr, "*** loading DSK ***\n");
  // disk info block
  if (fread(sigBuf, 256, 1, fl) != 1) {
    libfdcMsg(LIBFDC_MSG_ERROR, "cannot load DSK header");
    return FLPERR_SHIT;
  }

  // check disk format
  int ext = -1;
  if (!strncmp(sigBuf, edsksgn, 8)) {
    // extend dsk format
    ext = 1;
  } else if (!strncmp(sigBuf, dsksgn, 8)) {
    // common dsk format
    ext = 0;
  }
  if (ext < 0) {
    libfdcMsg(LIBFDC_MSG_ERROR, "invalid DSK signature");
    return FLPERR_SHIT;
  }

  // clear disk
  for (int i = 0; i < 168; ++i) {
    memset(flp->data[i].byte, 0, FLP_MAX_TRACK_SIZE);
    flpFillFields(flp, i, 0);
  }

  int trkcnt = sigBuf[0x30]&0xff;
  int sidcnt = sigBuf[0x31]&0xff;

  if (sidcnt == 0 || sidcnt > 2) {
    libfdcMsg(LIBFDC_MSG_ERROR, "invalid number of DSK sides (%d)", sidcnt);
    return FLPERR_SHIT;
  }

  if (trkcnt == 0) {
    libfdcMsg(LIBFDC_MSG_ERROR, "DSK contains no tracks");
    return FLPERR_SHIT;
  }

  if (trkcnt > 86) {
    libfdcMsg(LIBFDC_MSG_WARNING, "too many cylinders in DSK (%d), truncated to %d", trkcnt, 86);
    trkcnt = 86;
  }

  libfdcMsg(LIBFDC_MSG_DEBUG, "DSK: %d head%s, %d cylinders", sidcnt, (sidcnt == 2 ? "s" :""), trkcnt);
  int tlen = (sigBuf[0x32]&0xff)|((sigBuf[0x33]&0xff)<<8);
  int tr = 0;
  int pos = 0x100; // 1st track allways @ 0x100
  int err = 0;
  for (int i = 0; i < trkcnt*sidcnt && !err; ++i) {
    int rtlen = (ext ? sigBuf[0x34+i]<<8 : tlen);
    if (rtlen > 0) {
      // move to track info block
      if (fseek(fl, pos, SEEK_SET) < 0) {
        libfdcMsg(LIBFDC_MSG_ERROR, "cannot seek to DSK track info block for track %d", i);
        return FLPERR_SHIT;
      }
      if (fread(sigBuf, 12, 1, fl) != 1) {
        libfdcMsg(LIBFDC_MSG_ERROR, "cannot read DSK track info block for track %d", i);
        return FLPERR_SHIT;
      }
      // track-info?
      if (strncmp(sigBuf, "Track-Info\r\n", 12) == 0) {
        TrackInfBlock tib;
        if (fread((char *)&tib, sizeof(TrackInfBlock), 1, fl) != 1) return FLPERR_SHIT;
        SectorInfBlock *sib = (SectorInfBlock *)&tib.sectorInfo;
        for (int sc = 0; sc < tib.secCount; ++sc) {
          int slen;
          if (!ext) {
            if (sib[sc].size > 5) {
              slen = 0x1800;
            } else {
              slen = (0x80<<sib[sc].size)&0xffff;
            }
          } else {
            slen = sib[sc].bslo|(sib[sc].bshi<<8);
          }
          secs[sc].trk = sib[sc].track;
          secs[sc].head = sib[sc].side;
          secs[sc].sec = sib[sc].sector;
          secs[sc].sz = sib[sc].size;
          secs[sc].type = 0xfb;
          //fprintf(stderr, "  sector(tr=%d;sc=%d): C=%d; H=%d; R=%d; N=%d; slen=%d\n", tr, sc, sib[sc].track, sib[sc].side, sib[sc].sector, sib[sc].size, slen);
          if (fread((char *)secs[sc].data, slen, 1, fl) != 1) return FLPERR_SHIT;
        }
        // calculate good CRCs
        flpFormatTracks(flp, tr, secs, tib.secCount, FLP_FORMAT_TRACK_CALC_CRC);
        ++tr;
        if (sidcnt == 1) ++tr;
        pos += rtlen;
      } else {
        libfdcMsg(LIBFDC_MSG_ERROR, "invalid DSK track info block signature for track %d", i);
        err = 1; //ERR_DSK_SIGN;
      }
    } else {
      //flpFormatTracks(flp, tr, secs, tib.secCount, FLP_FORMAT_TRACK_CALC_CRC);
      // flp_format_trk(flp, tr, 10, 512, NULL);
      ++tr;
      if (sidcnt == 1) ++tr;
    }
  }
  flp->protect = 0;
  flp->doubleSide = (sidcnt > 1 ? 1 : 0);
  flp->trk80 = (trkcnt > 42 ? 1 : 0);
  flp->insert = 1;
  flp->changed = 0;
  return (err ? FLPERR_SHIT : FLPERR_OK);
}


static long saveTrackDSK (Floppy *flp, int trk, int side, FILE *fl) {
  TrackInfBlock tinf;
  FDCSector sdata[29];
  long fpos = ftell(fl);
  if (fpos < 0) return -1;
  int sec = 0;
  int pos = 0;
  int dsz, dps;
  int mdsz = 0;
  int rtrk = (trk<<1)|side;
  side &= 1;
  memset(&tinf, 0, sizeof(TrackInfBlock));
  // scan track & collect sectors info & data
  for (pos = 0; pos < FLP_MAX_TRACK_SIZE && sec < 29; ++pos) {
    if (flp->data[rtrk].field[pos] == 1) {
      // sector info
      sdata[sec].trk = flp->data[rtrk].byte[pos++]; // C
      sdata[sec].head = flp->data[rtrk].byte[pos++];// H
      sdata[sec].sec = flp->data[rtrk].byte[pos++]; // R
      sdata[sec].sz = flp->data[rtrk].byte[pos++]&7;// N
      pos += 2; // skip crc
      if (sdata[sec].sz > mdsz) mdsz = sdata[sec].sz; // max N
      dsz = 128<<sdata[sec].sz; // sector data size
      if (dsz > 0x1800) dsz = 0x1800;
      dps = flp->data[rtrk].map[sdata[sec].sec]; // position of sector data
      if (dps > 0) sdata[sec].type = flp->data[rtrk].byte[dps-1]; // sector data type (FB | F8)
      memcpy(sdata[sec].data, flp->data[rtrk].byte+dps, dsz); // copy sector data
      ++sec;
    }
  }
  if (sec < 1) return 0; // no sectors found, no data written, size = 0
  // fill track info
  tinf.track = trk&0xff;
  tinf.side = side&1;
  tinf.secSize = mdsz&7; // 0 @ extend dsk ?
  tinf.secCount = sec&0xff;
  tinf.gap3Size = 0x1a;
  tinf.filler = 0xe5;
  pos = 0;
  for (int i = 0; i < sec; ++i) {
    tinf.sectorInfo[pos] = sdata[i].trk;
    tinf.sectorInfo[pos+1] = sdata[i].head;
    tinf.sectorInfo[pos+2] = sdata[i].sec;
    tinf.sectorInfo[pos+3] = sdata[i].sz;
    dsz = 128<<sdata[i].sz;
    if (i == sec-1) tinf.sectorInfo[pos+4] = 0x80; // end of cylinder
    tinf.sectorInfo[pos+6] = dsz&0xff;
    tinf.sectorInfo[pos+7] = (dsz>>8)&0xff;
    pos += 8;
  }
  // save track info block
  if (fwrite("Track-Info\r\n", 12, 1, fl) != 1) return -1;
  if (fwrite(&tinf, sizeof(TrackInfBlock), 1, fl) != 1) return -1;
  // save sectors data
  for (int i = 0; i < sec; ++i) {
    dsz = 128<<sdata[i].sz;
    if (dsz > 0x1800) dsz = 0x1800;
    if (fwrite(sdata[i].data, dsz, 1, fl) != 1) return -1;
  }
  long epos = ftell(fl);
  if (epos < 0) return -1;
  return epos-fpos;
}


static const char *sign = "EXTENDED CPC DSK File\r\nDisk-Info\r\nZXEmuT        ";

int dskSaveDSK (Floppy *flp, FILE *fl) {
  if (!flp || fl == NULL || !flp->insert) return FLPERR_SHIT;
  uint8_t buf[256];
  memset(buf, 0, 0x100);
  memcpy(buf, sign, 0x22+0x0e);
  // 30 = track count, will be updated later
  // 31: 1 | 2 sides
  // 32,33 = 0 @ extend format
  // 34+N = size of track N (from Track-Info to Track-Info)
  // fwrite(buf, 256, 1, fl);   // signature + disk info : buf will be saved later
  if (fseek(fl, 256, SEEK_SET) < 0) return FLPERR_SHIT;
  int cpos = 0x34;
  long isize;
  int ds = 0;
  int tcount = 0;
  for (int i = 0; i < 86; ++i) {
    isize = saveTrackDSK(flp, i, 0, fl);
    buf[cpos] = (isize>>8)&0xff;
    ++cpos;
    if (isize > 0) tcount = i+1;
    if (flp->doubleSide) {
      isize = saveTrackDSK(flp, i, 1, fl);
      if (isize < 0) return FLPERR_SHIT;
      if (isize > 0) ds = 1;
      buf[cpos] = (isize>>8)&0xff;
      ++cpos;
      if (isize > 0) tcount = i+1;
    }
  }
  buf[0x30] = (tcount < 43 ? 42 : 84); // = tcount & 0xff;
  buf[0x31] = (ds ? 2 : 1);
  if (!ds) {
    cpos = 0x36; // 0x34 doesn't changed
    tcount = 0x35;
    while (cpos < 256) {
      buf[tcount] = buf[cpos];
      buf[cpos] = 0;
      ++tcount;
      cpos += 2;
    }
  }
  // save disk info block
  if (fseek(fl, 0, SEEK_SET) < 0) return FLPERR_SHIT;
  if (fwrite(buf, 256, 1, fl) != 1) return FLPERR_SHIT;
  // update name
  flp->changed = 0;
  return FLPERR_OK;
}

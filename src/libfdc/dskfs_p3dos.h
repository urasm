/*
 * WD1793/uPD765 emulator
 * Copyright (c) 2009-..., SAM style
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is furnished
 * to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
 * THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * changes by Ketmar // Invisible Vector
 *
 * Understanding is not required. Only obedience.
 */
// TR-DOS FS operations (incomplete)
#ifndef LIBFDC_DSKFS_P3DOS_H
#define LIBFDC_DSKFS_P3DOS_H

#include "floppy.h"
#include "dskfs_p3dos.h"

#ifdef __cplusplus
extern "C" {
#endif


// ////////////////////////////////////////////////////////////////////////// //
enum {
  P3DSK_PCW_SS = 0, // standard PCW range DD SS ST (and +3) (180)
  P3DSK_SYSTEM = 1, // standard CPC range DD SS ST system format
  P3DSK_DATA   = 2, // standard CPC range DD SS ST data only format
  P3DSK_PCW_DS = 3, // Standard PCW range DD DS DT (720)
  // not a type! ;-)
  P3DSK_MAX    = 4,
};


typedef struct {
  uint8_t sides;
  uint8_t cyls;
  uint8_t sectors;
  uint8_t firstsector;
  uint16_t secsize;
  uint8_t restracks; // number of restracks tracks
  uint16_t blocksize;
  uint8_t dirblocks; // number of directory blocks
  uint8_t rwgap;
  uint8_t fmtgap;
  unsigned checksum; // bool; is valid CP/M bootable disk?
  unsigned successive; // 0: interleaved
  // calculated
  uint8_t disktype;
  uint16_t maxdirentries;
  uint16_t maxblocks;
} P3DskGeometry;

// this is used to make things easier,
// so you don't have to pass both `flp` and `geom`
// there is no need to "finalize" this struct
typedef struct {
  Floppy *flp;
  P3DskGeometry geom;
} P3DiskInfo;


// ////////////////////////////////////////////////////////////////////////// //
int flpIsDiskP3DOS (Floppy *flp);

// create +3DOS disk with file system
// `dsktype` is `P3DSK_XXX`
// returns 0 on success, or <0 on error
int p3dskFormatDisk (Floppy *flp, int dsktype);

// returns 0 on success, or <0 on error
// on error, `geom` is undefined, but sides guaranteed to hold zero
int p3dskDetectGeom (Floppy *flp, P3DskGeometry *geom);

// get logical sector data, or NULL on error
// it is not const, but don't write it!
// returns 0 on success, or <0 on error
int p3dskLogicalSectorToPhysTS (P3DskGeometry *geom, int lsidx, int *outtrk, int *outsec);

// get logical sector data, or NULL on error
// it is not const, but don't write it!
uint8_t *p3dskGetLogicalSector (P3DiskInfo *p3d, int lsidx);

// call this if you changed the result of `p3dskGetLogicalSector()`
// returns error code
int p3dskUpdateLogicalSectorCRC (P3DiskInfo *p3d, int lsidx);

// calculate logical sector number for the given disk block
int p3dskCalcLogicalSectorForBlock (P3DskGeometry *geom, int blknum, int bofs);

// calculate offset in physical sector for the given block offset
int p3dskCalcPhysSectorOffsetForBlock (P3DskGeometry *geom, int blknum, int bofs);

// return pointer to the directory entry with the given index, or `NULL` on error
// it is not const, but don't write it!
uint8_t *p3dskGetDirEntryPtr (P3DiskInfo *p3d, int idx);

// call this if you changed the result of `p3dskGetDirEntryPtr()`
// returns error code
int p3dskUpdateDirEntryCRC (P3DiskInfo *p3d, int idx);

// returns 0 on success, <0 on error
// make sure that `dest` is at least `geom->blocksize` bytes
int p3dskReadBlock (P3DiskInfo *p3d, int blkidx, void *dest);


// ////////////////////////////////////////////////////////////////////////// //
// +3DOS directory API
// this api can be used without mounting the volume
// it is slow (because mounting can cache some data),
// but it is still much faster than CP/M on the original
// Z80 CPUs. ;-)

// default is "match filenames, case-insensitive"
#define P3DSK_GLOB_DEFAULT  (0u)
// perform case-senitive matching
#define P3DSK_GLOB_CASE_SENSITIVE  (1u<<0)
// match string, not a file name
#define P3DSK_GLOB_STRING  (1u<<1)

// wildcard match
//   note that "*" finds only files without an extension, while
//   "*.*" will find all files (even with empty extension or name)
//   "[a-b]" char ranges are supported
//   "[^a-b]" char ranges are supported
// returns boolean
int p3dskGlobMatch (const char *str, const char *pat, unsigned flags);

// this ignores case
int p3dskIsValidFileName (const char *name);

// normalise +3DOS file name, so it will be suitable for `p3dskFindInit()`
// `dest` must be at least 13 bytes
// the function will never write more than 13 bytes
// `dest` and `src` can be the same
// returns error code
int p3dskNormaliseFileName (char *dest, const char *src);

// this ignores case
int p3dskIsGlobMask (const char *str);


// this works both as "file find" info, and as simple FCB
typedef struct {
  // "name.ext"; for files without an extension, the dot will be removed
  char name[13];
  char nameonly[9];
  char extonly[4];
  // file size
  uint32_t size;
  // first file extent in a directory
  uint16_t firstextent;
  // attributes
  unsigned readonly:1;
  unsigned system:1;
  unsigned archive:1;
  // private
  // used by `p3dskFindFile()`
  // stored here, so we don't have to pass it around
  P3DiskInfo *p3d;
  // next extent we should examine
  uint16_t nextextent;
  // more than enough; we cannot simply rebuild it due to ranges
  char mask[128];
} P3DskFileInfo;


// start directory iteration
// `mask` is a normal CP/M wildcard (see `p3dskGlobMatch()`)
// file names are case-insensitive
// returns <0 on error, 0 if no files found, >0 if file was found
// actually, can return:
//   FLPERR_OK
//   FLPERR_SHIT
//   FLPERR_BADMASK
int p3dskFindInit (P3DiskInfo *p3d, P3DskFileInfo *nfo, const char *mask);

// start/continue file searching
// `nfo` must be initialised with `p3dskFindInit()`
// returns <0 on error; 0 if no more files; 1 if next was found (`nfo` is filled)
int p3dskFindNext (P3DskFileInfo *nfo);

// get buffer size to hold all file blocks
// returns -1, or number of 16-bit items in file block buffer
int p3dskFindCalcBlockbufLength (P3DskFileInfo *nfo);

// get all file blocks; buffer size must be calculated with `p3dskFindCalcBlockbufLength()`
int p3dskGetBlockbuf (P3DskFileInfo *nfo, uint16_t *dest);


// ////////////////////////////////////////////////////////////////////////// //
// high-level +3DOS file i/o API
//
// WARNING! you can read several files simultaneously, but writing is the
//          whole different story. DO NOT assume that you can read opened
//          files after you did writing/deletion (except the file you're
//          writing into).
//
//          there are no sanity checks there, so doing something unexpected
//          may turn your +3DOS disk into garbage.
//
//          you can call `p3dskRefreshFile()` to refresh `nfo`, though. (NOT YET)
//
//          also, never ever use two `nfo`s to write to the same file!
//          note that you can use several `nfo`s to read files, tho.
//
//          ah, directory traversing is not possible after writing or
//          deletion too.
//
//          files with holes are not supported (they will be shrink at the first hole).
//
//          files without the first logical extent are not supported (never found, and
//          will be erased if a new file with the same name created).
//
//          time stamps and password entries will be removed on directory modification.

// delete file (glob masks are supported)
// will happily delete read-only files
// returns error code (FLPERR_NOFILE means that file not found)
int p3dskDeleteFiles (P3DiskInfo *p3d, const char *name);

// create empty +3DOS file
// will fail if the given file already exists
// if you want to perform writes, use `p3dskOpenFile()` after this
// returns error code
int p3dskCreateFile (P3DiskInfo *p3d, const char *name);

// find file and open it for reading/writing
// returns error code
int p3dskOpenFile (P3DiskInfo *p3d, P3DskFileInfo *nfo, const char *name);

// read file data
// returns <0 on error, or number of bytes read
int p3dskReadFile (const P3DskFileInfo *nfo, void *buf, int ofs, int bytecount);

// write file data
// note that this will not transparently process +3DOS file header.
// if you will write past the end of the file, the file will be made bigger.
// if there's no room in a directory or on a disk, partial data may be written,
// but you will still get error returned (FLPERR_MANYFILES or FLPERR_NOSPACE).
// returns <0 on error, or number of bytes written
int p3dskWriteFile (P3DskFileInfo *nfo, const void *buf, int ofs, int bytecount);

// grow file to the given size
// if the size is already equal or bigger, do nothing.
// new area will not be filled with anything.
// if there's no room in a directory or on a disk, partial data may be written,
// but you will still get error returned (FLPERR_MANYFILES or FLPERR_NOSPACE).
// returns <0 on error, or number of bytes written
int p3dskGrowFile (P3DskFileInfo *nfo, int newsize);

// grow file to the given size
// if the size is already equal or bigger, do nothing.
// new area will be filled with zeroes.
// if there's no room in a directory or on a disk, partial data may be written,
// but you will still get error returned (FLPERR_MANYFILES or FLPERR_NOSPACE).
// returns <0 on error, or number of bytes written
//int p3dskShrinkFile (P3DskFileInfo *nfo, int newsize);


// this is here only for debugging
uint8_t *p3dskBuildBlockBitmap (Floppy *flp, P3DskGeometry *geom);


// ////////////////////////////////////////////////////////////////////////// //
// +3DOS file header utilities

/*
  Bytes 0...7     - +3DOS signature - 'PLUS3DOS'
  Byte 8          - 1Ah (26) Soft-EOF (end of file)
  Byte 9          - Issue number
  Byte 10         - Version number
  Bytes 11...14   - Length of the file in bytes, 32 bit number,
                      least significant byte in lowest address
  Bytes 15...22   - +3 BASIC header data
  Bytes 23...126  - Reserved (set to 0)
  Byte 127        - Checksum (sum of bytes 0...126 modulo 256)

  +3DOS BASIC header:
+---------------+-------+-------+-------+-------+-------+-------+-------+
| BYTE          |   0   |   1   |   2   |   3   |   4   |   5   |   6   |
+---------------+-------+-------+-------+-------+-------+-------+-------+
| Program           0   file length     8000h or LINE   offset to prog  |
| Numeric array     1   file length     xxx     name    xxx     xxx     |
| Character array   2   file length     xxx     name    xxx     xxx     |
| CODE or SCREEN$   3   file length     load address    xxx     xxx     |
+-----------------------------------------------------------------------+
*/

typedef struct {
  uint8_t valid;
  uint8_t issue;
  uint8_t version;
  uint32_t filesize; // with the header
  // basic header
  uint8_t bastype;
  uint16_t baslength; // without the header, obviously
  uint16_t basaddr; // or line start
  uint16_t basvarsofs; // offset to variables
} P3DskFileHeader;

#define P3DSK_FILE_HEADER_SIZE  (128)

// read and validate +3DOS file header
// returns error code
int p3dskReadFileHeader (const P3DskFileInfo *nfo, P3DskFileHeader *hdr);

// write file header to the first 128 bytes of the file
// returns error code
int p3dskWriteFileHeader (P3DskFileInfo *nfo, const P3DskFileHeader *hdr);


// ////////////////////////////////////////////////////////////////////////// //
// some info about +3DOS bootable disks
// +3DOS will load and run sector 1 of track 0 if its checksum is ok
// P3DSK_DATA cannot be used as bootable disk (no room for boot sector)
// boot sector will be loaded at #FE00, and execution will start from #FE10
// memory layout will be set to: RAM4, RAM7, RAM6, RAM3
// RAM3 contains bootstrap code
// on entry, the stack is at #FE00
//
// checksum is simply the sum of all sector bytes (with initial checksum
// byte as 0), and then modified, so the final checksum will be equal to 3.
// i.e.
//   finalsum = ((sum^0xFFu)+4u)&0xFFu;
//
// there are two other boot methods for +3 loader:
// first, the code file named "*" will be loaded, if it is present (no details yet).
// second, the "DISK" basic file will be loaded (and possibly run).
//

// check if the given disk is CP/M bootable (ignores +3DOS boot methods)
// not really needed, as geometry detector will do this for you
// returns error code or boolean
int p3dskIsBootable (const P3DiskInfo *p3d);

// modifies the first disk sector checksum to
// indicate a "CP/M bootable" disk
// returns error code
int p3dskWriteBootableChecksum (P3DiskInfo *p3d);

// modifies the first disk sector checksum to
// indicate a "CP/M non-bootable" disk
// returns error code
int p3dskResetBootableChecksum (P3DiskInfo *p3d);


#ifdef __cplusplus
}
#endif

#endif

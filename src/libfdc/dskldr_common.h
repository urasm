/*
 * disk images I/O
 * coded by Ketmar // Invisible Vector
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 */
#ifndef LIBFDC_DSKLDR_INTERNAL_COMMON_H
#define LIBFDC_DSKLDR_INTERNAL_COMMON_H


//==========================================================================
//
//  writeui16
//
//==========================================================================
static LIBFDC_MAYBE_UNUSED LIBFDC_INLINE void writeui16 (uint8_t *d, uint16_t v) {
  d[0] = (v&0xff);
  d[1] = ((v>>8)&0xff);
}


//==========================================================================
//
//  writeui32
//
//==========================================================================
static LIBFDC_MAYBE_UNUSED LIBFDC_INLINE void writeui32 (uint8_t *d, uint32_t v) {
  d[0] = (v&0xff);
  d[1] = ((v>>8)&0xff);
  d[2] = ((v>>16)&0xff);
  d[3] = ((v>>24)&0xff);
}


//==========================================================================
//
//  getui16
//
//==========================================================================
static LIBFDC_MAYBE_UNUSED LIBFDC_INLINE uint16_t getui16 (const void *buf) {
  return ((((const uint8_t *)buf)[1])<<8)|(((const uint8_t *)buf)[0]);
}


//==========================================================================
//
//  freaduint
//
//==========================================================================
static LIBFDC_MAYBE_UNUSED int freaduint (FILE *fl, uint32_t *valp, int n) {
  uint32_t val = 0;
  int shift = 0;
  *valp = 0;
  while (n-- > 0) {
    uint8_t b;
    if (fread(&b, 1, 1, fl) != 1) return FLPERR_SHIT;
    val |= ((uint32_t)b)<<shift;
    shift += 8;
  }
  *valp = val;
  return FLPERR_OK;
}


//==========================================================================
//
//  fwriteuint
//
//==========================================================================
static LIBFDC_MAYBE_UNUSED int fwriteuint (FILE *fl, uint32_t val, int n) {
  while (n-- > 0) {
    uint8_t b = (val&0xff);
    if (fwrite(&b, 1, 1, fl) != 1) return FLPERR_SHIT;
    val >>= 8;
  }
  return FLPERR_OK;
}


#endif

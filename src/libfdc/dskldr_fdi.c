/*
 * disk images I/O
 * coded by Ketmar // Invisible Vector
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 */
#include "dskldr.h"
#include "dskldr_common.h"
#include "dskfs_all.h"


//==========================================================================
//
//  dskLoadFDI
//
//==========================================================================
int dskLoadFDI (Floppy *flp, FILE *fl) {
  uint8_t hdr[14];
  FDCSector trkimg[256];
  uint16_t cylCount, headCount;
  size_t dataOfs, shdrsOfs;
  int wpFlag;

  if (fl == NULL) return FLPERR_SHIT;
  if (fread(hdr, 14, 1, fl) != 1) {
    libfdcMsg(LIBFDC_MSG_ERROR, "cannot read FDI header");
    return FLPERR_SHIT;
  }
  if (memcmp(hdr, "FDI", 3) != 0) {
    libfdcMsg(LIBFDC_MSG_ERROR, "invalid FDI signature");
    return FLPERR_SHIT;
  }
  wpFlag = (hdr[3] != 0); // write protection flag
  cylCount = hdr[4]+(hdr[5]<<8); // cylinders
  headCount = hdr[6]+(hdr[7]<<8); // heads
  if (headCount < 1 || headCount > 2) {
    // invalid number of heads
    libfdcMsg(LIBFDC_MSG_ERROR, "invalid FDI number of sides (%u)", headCount);
    return FLPERR_SHIT;
  }
  dataOfs = hdr[10]+(hdr[11]<<8); // sectors data pos
  shdrsOfs = hdr[12]+(hdr[13]<<8)+14; // track headers data pos
  if (fseek(fl, shdrsOfs, SEEK_SET) != 0) {
    libfdcMsg(LIBFDC_MSG_ERROR, "cannot seek to FDI track data");
    return FLPERR_SHIT;
  }

  libfdcMsg(LIBFDC_MSG_DEBUG, "FDI: %u heads, %u cylinders%s", headCount, cylCount, (wpFlag ? ", write-protected" : ""));

  // read tracks data
  for (int cyl = 0; cyl < cylCount; ++cyl) {
    for (int head = 0; head < headCount; ++head) {
      uint32_t trkOfs;
      uint8_t seccount;
      long tofs;
      uint8_t secGoodCRC[256];

      memset(trkimg, 0, sizeof(trkimg));
      if (freaduint(fl, &trkOfs, 4) < 0) goto error;
      tofs = dataOfs+trkOfs; // track data offset in file
      //fprintf(stderr, "dataOfs=0x%08x; trkOfs=0x%08x\n", dataOfs, trkOfs);
      // skip 2 bytes
      if (fseek(fl, 2, SEEK_CUR) != 0) goto error;
      // sectors in track
      if (fread(&seccount, 1, 1, fl) != 1) goto error;

      for (int sec = 0; sec < seccount; ++sec) {
        uint8_t flags;
        uint32_t secOfs;
        uint16_t slen;
        long oldPos;
        FDCSector *csec = &trkimg[sec];
        //fprintf(stderr, " sec=%d\n", sec);
        if (fread(&csec->/*cyl*/trk, 1, 1, fl) != 1) goto error;
        if (fread(&csec->/*side*/head, 1, 1, fl) != 1) goto error;
        if (fread(&csec->sec, 1, 1, fl) != 1) goto error;
        if (fread(&csec->/*len*/sz, 1, 1, fl) != 1) goto error;
        if (fread(&flags, 1, 1, fl) != 1) goto error; // flags
        if (freaduint(fl, &secOfs, 2) < 0) goto error;
        //fprintf(stderr, " C=%u; H=%u; R=%u; N=%u; flags=0x%02x; secOfs=0x%04x\n", csec->cyl, csec->side, csec->sec, csec->len, flags, secOfs);
        if (csec->/*len*/sz > 4) goto error;
        secGoodCRC[sec] = ((flags&(1<<csec->/*len*/sz)) != 0);
        if (csec->/*len*/sz == 4) ++csec->/*len*/sz;
        // bits0-5: 'good CRC' flags for various sector sizes (128, 256, 1024, 4096)
        // bit6: should be 0
        // bit7!=0: deleted data marker
        csec->type = (flags&0x80 ? 0xf8 : 0xfb);
        secOfs += tofs; // sector data offset
        oldPos = ftell(fl); // remember current pos
        if (fseek(fl, secOfs, SEEK_SET) != 0) goto error;
        slen = (128<<csec->/*len*/sz); // sector len
        // read sector data
        if (fread(csec->data, slen, 1, fl) != 1) goto error;
        if (fseek(fl, oldPos, SEEK_SET) != 0) goto error;
      }

      // calculate good CRCs
      flpFormatTracks(flp, (cyl<<1)+head, trkimg, seccount, FLP_FORMAT_TRACK_CALC_CRC);

      // fix invalid CRCs (for now: ignore CRC for different sizes)
      for (int sec = 0; sec < seccount; ++sec) {
        if (!secGoodCRC[sec]) {
          FDCSectorInfo *si;
          uint8_t *dta = flpGetRawSectorDataPtr(flp, (cyl<<1)+head, sec, &si, NULL);
          if (dta != NULL) {
            //FIXME: potential out-of-bounds access
            // make bad crc
            dta[(128<<si->len)+0] ^= 0xaa;
            dta[(128<<si->len)+1] ^= 0x55;
          }
        }
      }
    }
  }

  //fprintf(stderr, "OK\n");
  flp->protect = (wpFlag ? 1 : 0);
  flp->doubleSide = (headCount ? 1 : 0);
  flp->trk80 = (cylCount > 42 ? 1 : 0);
  flp->insert = 1;
  flp->changed = 0;
  return FLPERR_OK;

error:
  //fprintf(stderr, "ERROR\n");
  libfdcMsg(LIBFDC_MSG_ERROR, "error reading FDI data");
  flpFormatTRD(flp);
  flp->insert = 0;
  flp->changed = 0;
  return FLPERR_SHIT;
}


//==========================================================================
//
//  dskSaveFDI
//
//==========================================================================
int dskSaveFDI (Floppy *flp, FILE *fl) {
  uint16_t cylCount, headCount;
  long trkDataOfs, dataOfs;
  long hdrPos, cpos;

  if (fl == NULL) return FLPERR_SHIT;
  hdrPos = ftell(fl);
  if (fwrite("FDI", 3, 1, fl) != 1) return FLPERR_SHIT;
  // write protection flag
  if (fwriteuint(fl, (flp->protect ? 1 : 0), 1) != 0) return FLPERR_SHIT;

  cylCount = (flp->trk80 ? 80 : 40);
  headCount = (flp->doubleSide ? 2 : 1);
  if (fwriteuint(fl, cylCount, 2) != 0) return FLPERR_SHIT;
  if (fwriteuint(fl, headCount, 2) != 0) return FLPERR_SHIT;
  if (fwriteuint(fl, 0, 2*3) != 0) return FLPERR_SHIT;

  // write tracks info
  trkDataOfs = 0;
  for (int cyl = 0; cyl < cylCount; ++cyl) {
    for (int head = 0; head < headCount; ++head) {
      uint8_t tr = (cyl<<1)+head;
      int scnt = 0;
      //FIXME: this is wrong for sectors with address marks, but no data
      for (unsigned f = 0; f < 256; ++f) {
        if (!flp->data[tr].mapaddrraw[f] || !flp->data[tr].mapraw[f]) {
          scnt = (int)f+1;
          break;
        }
      }
      if (scnt < 0 || scnt > 255) {
        if (cyl == 0 || head != headCount-1) return FLPERR_SHIT;
        cylCount = cyl+1;
        goto shdrdone;
      }

      if (fwriteuint(fl, trkDataOfs, 4) != 0) return FLPERR_SHIT;
      if (fwriteuint(fl, 0, 2) != 0) return FLPERR_SHIT;
      if (fwriteuint(fl, scnt, 1) != 0) return FLPERR_SHIT;

      uint32_t sofs = 0;
      for (int sec = 0; sec < scnt; ++sec) {
        FDCSectorInfo *si;
        int crcok = 0;
        uint8_t *ptr = flpGetRawSectorDataPtr(flp, (cyl<<1)+head, sec, &si, &crcok);
        if (ptr == NULL) return FLPERR_SHIT;
        uint8_t b = si->len;
        if (b > 5 || b == 4) return FLPERR_SHIT;
        if (b == 5) --b;
        if (fwriteuint(fl, si->cyl, 1) != 0) return FLPERR_SHIT;
        if (fwriteuint(fl, si->side, 1) != 0) return FLPERR_SHIT;
        if (fwriteuint(fl, si->sec, 1) != 0) return FLPERR_SHIT;
        if (fwriteuint(fl, b, 1) != 0) return FLPERR_SHIT;
        b = (crcok ? 1<<b : 0)|(ptr[-1] == 0xf8 ? 0x80 : 0x00);
        if (fwriteuint(fl, b, 1) != 0) return FLPERR_SHIT;
        if (fwriteuint(fl, sofs, 2) != 0) return FLPERR_SHIT;
        sofs += (128<<si->len);
        trkDataOfs += (128<<si->len);
      }
    }
  }

shdrdone:
  // disk comment
  if (fwriteuint(fl, 0, 1) != 0) return FLPERR_SHIT;

  // write tracks data
  dataOfs = ftell(fl)-hdrPos;
  for (int cyl = 0; cyl < cylCount; ++cyl) {
    for (int head = 0; head < headCount; ++head) {
      uint8_t tr = (cyl<<1)+head;
      int scnt = 0;
      //FIXME: this is wrong for sectors with address marks, but no data
      for (unsigned f = 0; f < 256; ++f) {
        if (!flp->data[tr].mapaddrraw[f] || !flp->data[tr].mapraw[f]) {
          scnt = (int)f+1;
          break;
        }
      }
      if (scnt < 0) return FLPERR_SHIT;
      for (int sec = 0; sec < scnt; ++sec) {
        FDCSectorInfo *si;
        uint8_t *ptr = flpGetRawSectorDataPtr(flp, (cyl<<1)+head, sec, &si, NULL);
        if (ptr == NULL) return FLPERR_SHIT;
        if (fwrite(ptr, (128<<si->len), 1, fl) != 1) return FLPERR_SHIT;
      }
    }
  }

  // fix header
  cpos = ftell(fl);
  if (fseek(fl, hdrPos+4, SEEK_SET) != 0) return FLPERR_SHIT;
  if (fwriteuint(fl, cylCount, 2) != 0) return FLPERR_SHIT;
  if (fwriteuint(fl, headCount, 2) != 0) return FLPERR_SHIT;
  if (fwriteuint(fl, dataOfs-1, 2) != 0) return FLPERR_SHIT;
  if (fwriteuint(fl, dataOfs, 2) != 0) return FLPERR_SHIT;
  if (fseek(fl, cpos, SEEK_SET) != 0) return FLPERR_SHIT;

  return FLPERR_OK;
}

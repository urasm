/*
 * WD1793/uPD765 emulator
 * Copyright (c) 2009-..., SAM style
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is furnished
 * to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
 * THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * changes by Ketmar // Invisible Vector
 *
 * Understanding is not required. Only obedience.
 */
// floppy disk representation
#ifndef LIBFDC_FLOPPY_H
#define LIBFDC_FLOPPY_H

#include "bdicommon.h"

#ifdef __cplusplus
extern "C" {
#endif

#define FLP_MAX_TRACK_SIZE  (6250)

// error
// WARNING! DO NOT CHANGE! this is used in alot of places, sometimes as magic numbers (sorry!)
enum {
  FLPERR_OK        = 0,
  FLPERR_SHIT      = -1, // general error
  FLPERR_MANYFILES = -2, // too many files in FS
  FLPERR_NOSPACE   = -3, // no room in directory/disk
  FLPERR_BADMASK   = -4, // bad file glob wildcard
  FLPERR_NOFILE    = -5, // no such file
  FLPERR_NOHEADER  = -6, // +3DOS file contains no valid header
  FLPERR_FILEEXIST = -7, // for +3DOS file creation
};


// step direction
enum {
  FLP_BACK = 0,
  FLP_FORWARD,
};

typedef struct FDCSector_t {
  uint8_t trk;
  uint8_t head;
  uint8_t sec;
  uint8_t sz; // 0..3 = 128..1024
  uint8_t type;
  uint16_t crc;
  uint8_t data[0x1800]; // for sz [0..5]
} FDCSector;

typedef struct LIBFDC_PACKED FDCSectorInfo_t {
  uint8_t cyl;
  uint8_t side;
  uint8_t sec;
  uint8_t len;
  uint8_t crch;
  uint8_t crcl;
} FDCSectorInfo;

typedef struct Floppy_t {
  unsigned trk80:1; // fdd is 80T
  unsigned doubleSide:1;  // fdd is DS
  unsigned motor:1; // fdd motor is on
  unsigned insert:1;  // disk inserted
  unsigned protect:1; // disk is write protected
  unsigned changed:1; // disk is changed
  unsigned index:1; // disk index impulse

  unsigned rd:1;
  unsigned wr:1;

  // drive id; not used anywhere
  int id;
  // used in FDC emulators (Rd/Wr/Step)
  uint8_t trk;
  uint8_t field;
  int pos;

  // data for 256 tracks
  // for DS disks, tracks are interleaved: cyl(0),head(0)==0, cyl(0),head(1)==1, etc.
  // for SS disks, odd tracks are unused, but the numbering scheme is not changed
  struct {
    uint8_t byte[FLP_MAX_TRACK_SIZE]; // actual track data, including all special/service bytes
    uint8_t field[FLP_MAX_TRACK_SIZE]; // this is used in FDC emulators
    uint16_t map[256]; // position of logical sector data n = 1+ (0: no sector data)
    uint16_t mapaddr[256]; // position of logical sector address mark n = 1+ (0: no sector data)
    // sectors by "raw" index, not by logical one; "raw" indicies starts from zero
    uint16_t mapraw[256]; // position of sector data n = 1+ (0: no sector data)
    uint16_t mapaddrraw[256]; // position of sector address mark n = 1+ (0: no sector data)
  } data[256];
  // field values:
  // bits 0..3 is type:
  //   0: sector gaps
  //   1: CHRN address area
  //   2: normal data
  //   3: deleted data
  //   4: CRC (for address or data)
  // `field` array simply tells FDC emulators what kind of data is
  // stored at the corresponding `byte` position.
} Floppy;


// create new floppy disk structure
// default is double-sided, 80 tracks
// `id` is just a drive number: [0..4]
// it is not really used anywhere, just stored in `Floppy->id`
// you can use this function to create floppy
// without a disk interface if you only need to
// manipulate disk images
Floppy *flpCreate (int id);

// destroy floppy disk structure
void flpDestroy (Floppy *flp);

// clears "inserted" and "changed" flags
void flpEject (Floppy *flp);

// write byte to floppy sector
// doesn't advance position (see `flpNext()`/`flpPrev()`)
// `hd` is head number (only bit 0 matters)
// returns 0 on success, -1 on error
// not-inserted floppy means "error" too
// used by FDC emulators
// <0: error
int flpWr (Floppy *flp, int hd, uint8_t val);

// read byte from floppy sector
// doesn't advance position (see `flpNext()`/`flpPrev()`)
// `hd` is head number (only bit 0 matters)
// no error indication (oops); returns 0xff on error
// used by FDC emulators
uint8_t flpRd (Floppy *flp, int hd);

// move head to next/previous track
// `dir` is: `FLP_FORWARD`/`FLP_BACK` (other values are ignored)
// used by FDC emulators
void flpStep (Floppy *flp, int dir);

// advance to next reading position inside current sector
// returns 1 if index strobe
// `fdcSide` is head number
// (for some reason, side 1 is any non-zero value, instead of checking bit 0, as in most other APIs)
// does no sanity checks!
// used by FDC emulators
int flpNext (Floppy *flp, int fdcSide);

// "deadvance" to previous reading position inside current sector
// `fdcSide` is head number
// (for some reason, side 1 is any non-zero value, instead of checking bit 0, as in most other APIs)
// does no sanity checks!
// used by FDC emulators
void flpPrev (Floppy *flp, int fdcSide);


// note that tracks are always side-interleaved, even for one-sided disks
// i.e. for one-sided disks, odd tracks are simply missing

#define FLP_FILL_FIELDS_NONE  (0x00u)
#define FLP_FILL_FIELDS_CRC   (0x01u)
#define FLP_FILL_FIELDS_SVF   (0x02u)
/* for future expansions */
#define FLP_FILL_FIELDS_ALL   (0xffu)
/* options */
/* this makes bad CRC */
#define FLP_FILL_FIELDS_INV_CRC  (0x1000u)

// flag: see FLP_FILL_FIELDS_XXX
// this fills `mapXXX` for the given track, and calculates data/addr CRCs
// also, fills `field` array
// i.e. track data must be correctly set via some format function
//   0: ok
//  -1: general error
//  -3: some CRC is out of track data (but otherwise the track is ok) (FLPERR_NOSPACE)
int flpFillFields (Floppy *flp, int tr, uint32_t flag);

// clear track data and fields
// i.e. fills track data and all metadata with zeroes
// <0: error
int flpClearTrack (Floppy *flp, int tr);

// clear all 256 disk tracks
// <0: error
int flpClearDisk (Floppy *flp);

// get track data
// WARNING! make sure that `dst` is at least of `FLP_MAX_TRACK_SIZE` bytes!
// <0: error
int flpGetTrack (Floppy *flp, int tr, uint8_t *dst);

// get track fields
// WARNING! make sure that `dst` is at least of `FLP_MAX_TRACK_SIZE` bytes!
// <0: error
int flpGetTrackFields (Floppy *flp, int tr, uint8_t *dst);

// put track data, and fill track fields
// if len<FLP_MAX_TRACK_SIZE, the rest will be filled with zeroes
// <0: error
int flpPutTrack (Floppy *flp, int tr, const void *src, int len);

// calculate WD crc for data
uint16_t flpCalcWDCRC (const void *buf, size_t len);


// ////////////////////////////////////////////////////////////////////////// //
// some higher-level API

#define FLP_FORMAT_TRACK_NO_CRC   (0)
#define FLP_FORMAT_TRACK_CALC_CRC (1)
// format a single track from sectors list
// <sdata> is list of <scount> sectors
// TODO: calculate GAP3 len !!!
// scount: sector count
// calccrc: one of `FLP_FORMAT_TRACK_XXX`
// <0: error
int flpFormatTracks (Floppy *flp, int tr, const FDCSector *sdata, int scount, int calccrc);

// returns `NULL` on error
// WARNING! unsafe!
uint8_t *flpGetSectorDataPtr (Floppy *flp, uint8_t tr, uint8_t sc);

// 'sinf' can be NULL
// returns `NULL` on error
// WARNING! unsafe!
uint8_t *flpGetSectorDataPtrEx (Floppy *flp, uint8_t tr, uint8_t sc, FDCSectorInfo **sinf, int *crcok);

// this is for "raw sector indexes" (not logical)
// 'sinf' can be NULL
// returns `NULL` on error
// WARNING! unsafe!
uint8_t *flpGetRawSectorDataPtr (Floppy *flp, uint8_t tr, int idx, FDCSectorInfo **sinf, int *crcok);

// 0: unknown/no sector
// returns sector size in bytes
uint16_t flpGetSectorSize (Floppy *flp, uint8_t tr, uint8_t sc);

// 0: unknown/error/no sector
// returns sector size in bytes
// ensures that sector size is valid
// i.e. that `memcpy()` will not read/write outside of *track* data buffer
uint16_t flpGetSectorSizeSafe (Floppy *flp, uint8_t tr, uint8_t sc);

// -1: not found
int flpGetRawSectorC (Floppy *flp, uint8_t tr, uint8_t sc);
int flpGetRawSectorH (Floppy *flp, uint8_t tr, uint8_t sc);
int flpGetRawSectorR (Floppy *flp, uint8_t tr, uint8_t sc);
int flpGetRawSectorN (Floppy *flp, uint8_t tr, uint8_t sc);

// <0: error
// performs some sanity check
// will completely reject too long data (no changes will be made, will return -3 aka FLPERR_NOSPACE)
// cannot be used to put data to more than one sector
// you can use zero `len` (`buf` doesn't matter in this case) to fix data CRC
// but it is better to call `flpFixSectorDataCRC()` instead
int flpPutSectorData (Floppy *flp, uint8_t tr, uint8_t sc, const void *buf, size_t len);

// <0: error
// performs some sanity check
// will copy partial sector data (and return -3 aka FLPERR_NOSPACE)
// cannot be used to put data to more than one sector
int flpGetSectorData (Floppy *flp, uint8_t tr, uint8_t sc, void *buf, size_t len);

// <0: error
// performs some sanity check
// cannot be used to put data to more than one sector
// call this to recalc sector data CRC if you manually modified sector data
// `flpPutSectorData` does this automatically
int flpFixSectorDataCRC (Floppy *flp, uint8_t tr, uint8_t sc);


#ifdef __cplusplus
}
#endif

#endif

/*
 * disk images I/O
 * coded by Ketmar // Invisible Vector
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 */
#include "dskldr.h"
#include "dskldr_common.h"
#include "dskfs_all.h"


typedef struct {
  uint8_t trk;
  uint8_t sec;
  uint8_t slen;
} FilePos;


//==========================================================================
//
//  dskLoadSCL
//
//==========================================================================
int dskLoadSCL (Floppy *flp, FILE *fl) {
  uint8_t trkBuf[FLP_TRD_TRACK_SIZE], *bptr = trkBuf, fcnt;
  int secFree, scnt;
  if (fl == NULL) return FLPERR_SHIT;
  if (fread(trkBuf, 9, 1, fl) != 1) {
    libfdcMsg(LIBFDC_MSG_ERROR, "cannot read SCL header");
    return FLPERR_SHIT;
  }
  if (memcmp(trkBuf, "SINCLAIR", 8) != 0) {
    libfdcMsg(LIBFDC_MSG_ERROR, "invalid SCL signature");
    return FLPERR_SHIT;
  }
  if (trkBuf[8] > 0x80) {
    libfdcMsg(LIBFDC_MSG_ERROR, "too many files in SCL (%u)", trkBuf[8]);
    return FLPERR_SHIT;
  }
  if (flpFormatTRD(flp) != FLPERR_OK) {
    libfdcMsg(LIBFDC_MSG_ERROR, "cannot format emulated floppy");
    return FLPERR_SHIT;
  }
  fcnt = trkBuf[8]; // files total
  memset(trkBuf, 0, sizeof(trkBuf)); // clear TRK0 in buf
  scnt = 16;
  // build catalog
  for (int i = 0; i < fcnt; ++i) {
    // file dsc
    if (fread(bptr, 14, 1, fl) != 1) {
      libfdcMsg(LIBFDC_MSG_ERROR, "error reading SCL directory");
      flpFormatTRD(flp);
      flp->insert = 0;
      flp->changed = 0;
      return FLPERR_SHIT;
    }
    bptr[14] = (scnt&0x0f); // sector
    bptr[15] = ((scnt&0x3f0)>>4); // track
    scnt += bptr[13]; // +sectors size
    bptr += 16; // next file
  }
  if (fcnt < 128) bptr[0] = 0; // mark last file
  trkBuf[0x800] = 0;
  trkBuf[0x8e1] = (scnt&0x0f); // free sector
  trkBuf[0x8e2] = ((scnt&0x3f0)>>4); // free track
  trkBuf[0x8e3] = 0x16; // 80DS
  trkBuf[0x8e4] = fcnt; // files total
  secFree = 0x9f0-scnt; // sectors free (0x9f0) // FIXED: not 0xa00!
  trkBuf[0x8e5] = (secFree&0xff);
  trkBuf[0x8e6] = ((secFree&0xff00)>>8);
  trkBuf[0x8e7] = 0x10; // trdos code
  if (flpFormatTRDTrack(flp, 0, trkBuf, sizeof(trkBuf)) != FLPERR_OK) {
    libfdcMsg(LIBFDC_MSG_ERROR, "cannot format emulated floppy track (dir)");
    return FLPERR_SHIT;
  }
  // read file data
  for (int i = 1; i < FLP_TRDOS_MAX_TRACKS_80; ++i) {
    size_t rd = fread(trkBuf, 1, FLP_TRD_TRACK_SIZE, fl);
    if (rd < 1) break;
    if (rd < FLP_TRD_TRACK_SIZE) memset(trkBuf+rd, 0, FLP_TRD_TRACK_SIZE-rd);
    if (flpFormatTRDTrack(flp, i, trkBuf, sizeof(trkBuf)) != FLPERR_OK) {
      libfdcMsg(LIBFDC_MSG_ERROR, "cannot format emulated floppy track (data)");
      return FLPERR_SHIT;
    }
    if (rd < FLP_TRD_TRACK_SIZE) break; // no more bytes in file anyway
  }
  flp->insert = 1;
  flp->changed = 0;
  return FLPERR_OK;
}


//==========================================================================
//
//  dskSaveSCL
//
//==========================================================================
int dskSaveSCL (Floppy *flp, FILE *fl) {
  if (!flp || !flp->insert) return FLPERR_SHIT;
  static const char *sign = "SINCLAIR";
  uint8_t img[9*FLP_TRD_TRACK_SIZE], *dptr;
  uint8_t secBuf[256], *bptr;
  FilePos newfp, dir[128];
  int fileCnt = 0;
  uint64_t sum = 0;
  uint8_t tr, sc;
  if (fl == NULL) return FLPERR_SHIT;
  memcpy(img, sign, 9);
  // build directory
  dptr = img+9;
  for (int i = 1; i < 9; ++i) {
    if (flpGetSectorData(flp, 0, i, secBuf, 256) != FLPERR_OK) return FLPERR_SHIT;
    bptr = secBuf;
    for (int j = 0; j < 16; ++j) {
      if (*bptr == 0) {
        i = 20;
        break;
      } else {
        if (*bptr != 1) {
          memcpy(dptr, bptr, 14);
          newfp.trk = *(bptr+15);
          newfp.sec = *(bptr+14);
          newfp.slen = *(bptr+13);
          dir[fileCnt++] = newfp;
          dptr += 14;
          ++img[8];
        }
        bptr += 16;
      }
    }
  }
  if (fileCnt == 0) return FLPERR_SHIT;
  img[8] = fileCnt;
  for (int f = 0; f < dptr-img; ++f) sum = (sum+img[f])&0xffffffffU;
  if (fwrite(img, dptr-img, 1, fl) != 1) return FLPERR_SHIT;
  for (int i = 0; i < fileCnt; ++i) {
    tr = dir[i].trk;
    sc = dir[i].sec;
    for (int j = 0; j < dir[i].slen; ++j) {
      if (flpGetSectorData(flp, tr, sc+1, img, 256) != FLPERR_OK) return FLPERR_SHIT;
      for (int f = 0; f < 256; ++f) sum = (sum+img[f])&0xffffffffU;
      if (fwrite(img, 256, 1, fl) != 1) return FLPERR_SHIT;
      if (++sc > 15) { ++tr; sc = 0; }
    }
  }
  writeui32(img, sum);
  if (fwrite(img, 4, 1, fl) != 1) return FLPERR_SHIT;
  flp->changed = 0;
  return FLPERR_OK;
}

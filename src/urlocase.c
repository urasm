// convert UrAsm source code to lower case (preserving strings and labels)
// coded by Ketmar // Invisible Vector
// GPLv3 ONLY
//
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include "liburasm/liburasm.h"


//**************************************************************************
//
// very simple string library
//
//**************************************************************************

typedef struct {
  char *data;
  size_t length;
  int *rc; /* refcounter; NULL means "statically allocated" */
} VStr;


// initialise VStr structure
static __attribute__((unused)) inline void vstr_init (VStr *str) { if (str) { str->data = NULL; str->length = 0; str->rc = NULL; } }


// free VStr
static __attribute__((unused)) inline void vstr_free (VStr *str) {
  if (str) {
    if (str->rc) {
      if (--(*str->rc) == 0) {
        free(str->rc);
        if (str->data) free(str->data);
      }
      str->data = NULL;
      str->length = 0;
      str->rc = NULL;
    }
  }
}


// copy VStr (both strings should be initialised)
static __attribute__((unused)) inline void vstr_copy (VStr *dest, const VStr *src) {
  if (!dest) return;
  if (!src) { vstr_free(dest); return; }
  if (dest->data != src->data) {
    /* different strings */
    vstr_free(dest);
    dest->data = src->data;
    dest->length = src->length;
    dest->rc = src->rc;
  }
  if (dest->rc) ++(*dest->rc);
}


// ensure that the given string is uniqie and modifiable
static __attribute__((unused)) inline void vstr_make_unique (VStr *str) {
  if (!str) return;
  if (str->length == 0) {
    vstr_free(str); /* just in case */
    return;
  }
  if (str->rc && *str->rc == 1) return; /* nothing to do */
  /* create new string */
  char *ns = malloc(str->length+1);
  memcpy(ns, str->data, str->length);
  ns[str->length] = 0;
  int *nrc = malloc(sizeof(int));
  *nrc = 1;
  if (str->rc) --(*str->rc);
  str->data = ns;
  str->rc = nrc;
}


// replace string with static contents
static __attribute__((unused)) inline void vstr_set_static (VStr *str, const char *s) {
  if (!str) return;
  vstr_free(str);
  if (s && s[0]) {
    str->data = (char *)s;
    str->length = strlen(s);
  }
}


// create string with static contents
static __attribute__((unused)) inline void vstr_init_static (VStr *str, const char *s) {
  vstr_init(str);
  vstr_set_static(str, s);
}


// replace string with dynamic contents
static __attribute__((unused)) inline void vstr_set_dynamic (VStr *str, const char *s) {
  if (!str) return;
  vstr_free(str);
  if (s && s[0]) {
    str->length = strlen(s);
    str->data = malloc(str->length+1);
    memcpy(str->data, s, str->length);
    str->data[str->length] = 0;
  }
}


// create string with dynamic contents
static __attribute__((unused)) inline void vstr_init_dynamic (VStr *str, const char *s) {
  vstr_init(str);
  vstr_set_dynamic(str, s);
}


// replace string with dynamic contents and the given length
static __attribute__((unused)) inline void vstr_set_dynamic_ex (VStr *str, const char *s, size_t len) {
  if (!str) return;
  vstr_free(str);
  if (len) {
    str->length = len;
    str->data = malloc(len+1);
    if (s) {
      memcpy(str->data, s, len);
      str->data[len] = 0;
    } else {
      memset(str->data, 0, len+1);
    }
  }
}


// create string with dynamic contents and the given length
static __attribute__((unused)) inline void vstr_init_dynamic_ex (VStr *str, const char *s, size_t len) {
  vstr_init(str);
  vstr_set_dynamic_ex(str, s, len);
}


// append source to destination
static __attribute__((unused)) inline void vstr_cat_vstr (VStr *dest, const VStr *src) {
  if (!dest || !src || src->length == 0) return;
  vstr_make_unique(dest);
  // make sure that we have `rc` allocated
  if (!dest->rc) { dest->rc = malloc(sizeof(int)); *dest->rc = 1; }
  dest->data = realloc(dest->data, src->length+dest->length+1);
  memmove(dest->data+dest->length, src->data, src->length);
  dest->length += src->length;
  dest->data[dest->length] = 0;
}


// append source to destination
static __attribute__((unused)) inline void vstr_cat_cstr (VStr *dest, const char *s) {
  if (!dest || !s || !s[0]) return;
  VStr tmp;
  if (dest->data && s >= dest->data && s < dest->data+dest->length) {
    /* oops; inside */
    vstr_init_dynamic(&tmp, s);
  } else {
    vstr_init_static(&tmp, s);
  }
  vstr_cat_vstr(dest, &tmp);
  vstr_free(&tmp);
}


//==========================================================================
//
//  load_file
//
//==========================================================================
static void load_file (const VStr *fname, VStr *text) {
  if (!text) {
    fprintf(stderr, "FATAL: cannot load file into nowhere!\n");
    exit(1);
  }
  if (!fname || fname->length == 0) {
    fprintf(stderr, "FATAL: cannot open nameless file for reading!\n");
    exit(1);
  }
  FILE *fl = fopen(fname->data, "rb");
  if (!fl) {
    fprintf(stderr, "FATAL: cannot open file '%s' for reading!\n", fname->data);
    exit(1);
  }
  fseek(fl, 0, SEEK_END);
  long size = ftell(fl);
  if (size <= 0 || size > 1024*1024*32) {
    fclose(fl);
    fprintf(stderr, "FATAL: file '%s' is either too big, or too small!\n", fname->data);
    exit(1);
  }
  char *res = malloc((size_t)size+1);
  fseek(fl, 0, SEEK_SET);
  if (fread(res, (size_t)size, 1, fl) != 1) {
    fclose(fl);
    fprintf(stderr, "FATAL: error reading file '%s'!\n", fname->data);
    exit(1);
  }
  res[(size_t)size] = 0;
  vstr_free(text);
  text->data = res;
  text->length = (size_t)size;
  text->rc = malloc(sizeof(int));
  *text->rc = 1;
}


//==========================================================================
//
//  save_file
//
//==========================================================================
static void save_file (const VStr *fname, const VStr *text) {
  if (!fname || fname->length == 0) {
    fprintf(stderr, "FATAL: cannot create nameless file!\n");
    exit(1);
  }
  FILE *fl = fopen(fname->data, "wb");
  if (!fl) {
    fprintf(stderr, "FATAL: cannot create file '%s'!\n", fname->data);
    exit(1);
  }
  if (text && text->length) {
    if (fwrite(text->data, text->length, 1, fl) != 1) {
      fclose(fl);
      unlink(fname->data);
      fprintf(stderr, "FATAL: error writing file '%s'!\n", fname->data);
      exit(1);
    }
  }
  fclose(fl);
}


//==========================================================================
//
//  isAlpha
//
//==========================================================================
static inline int isAlpha (const char ch) {
  return
    (ch >= 'A' && ch <= 'Z') ||
    (ch >= 'a' && ch <= 'z');
}


//==========================================================================
//
//  isIdChar
//
//==========================================================================
static inline int isIdChar (const char ch) {
  return
    (ch >= 'A' && ch <= 'Z') ||
    (ch >= 'a' && ch <= 'z') ||
    (ch >= '0' && ch <= '9') ||
    ch == '@' || ch == '_' || ch == '.';
}


//==========================================================================
//
//  toLower
//
//==========================================================================
static inline char toLower (const char ch) { return (ch >= 'A' && ch <= 'Z' ? ch-'A'+'a' : ch); }


//==========================================================================
//
//  toUpper
//
//==========================================================================
static inline char toUpper (const char ch) { return (ch >= 'a' && ch <= 'z' ? ch-'a'+'A' : ch); }


//==========================================================================
//
//  vstr_part_strEquCI_cstr
//
//==========================================================================
static int vstr_part_strEquCI_cstr (const VStr *str, size_t start, size_t len, const char *cstr) {
  if (!str) abort();
  if (start >= str->length) return (!cstr || !cstr[0]);
  if (len > str->length-start) len = str->length-start;
  if (!cstr || !cstr[0]) return (len == 0);
  const size_t cstrlen = strlen(cstr);
  if (cstrlen != len) return 0;
  const char *s = str->data+start;
  for (; *cstr; ++s, ++cstr) {
    if (toLower(*s) != toLower(*cstr)) return 0;
  }
  return 1;
}


//==========================================================================
//
//  vstr_part_check_token_list
//
//==========================================================================
static int vstr_part_check_token_list (const VStr *str, size_t start, size_t len, const char *list[], size_t count) {
  if (!count) return 0;
  for (size_t f = count; f--; ++list) {
    if (vstr_part_strEquCI_cstr(str, start, len, list[0])) return 1;
  }
  return 0;
}


// ////////////////////////////////////////////////////////////////////////// //
#define URASM_COMMAND_TOKENS  (18)
static const char *UrAsmCommands[URASM_COMMAND_TOKENS] = {
  "DISPLAY",
  "DISPLAY0",
  "DISPLAYA",
  "DISPHEX",
  "DISPHEX0",
  "DISPHEXA",
  //
  "DEFFMT",
  "MODEL",
  //
  "MACRO",
  "ENDM",
  //
  //
  "MODULE",
  "ENDMODULE",
  //
  "IF",
  "IFX",
  "ELSE",
  "ELSEIF",
  "ELSEIFX",
  "ENDIF",
};


#define ASM_COMMAND_TOKENS  (34)
static const char *asmCommands[ASM_COMMAND_TOKENS] = {
  "EQU",
  "ORG",
  "DISP",
  "ENDDISP",
  "PHASE",
  "DEPHASE",
  "UNPHASE",
  "ALIGN",
  "DISPALIGN",
  "PHASEALIGN",
  "ENT",
  "CLR",
  "RESERVE",
  //
  "INCLUDE",
  "INCBIN",
  //
  "DUP",
  "EDUP",
  //
  "DEFINCR",
  "DEFB",
  "DB",
  "DEFW",
  "DW",
  "DEFR",
  "DR",
  "DEFS",
  "DS",
  "DEFM",
  "DM",
  "DEFZ",
  "DZ",
  "DEFX",
  "DX",
  "DEFC",
  "DC",
};


#define URA_REGSX_COUNT  (8)
static const char *URA_REGSX[URA_REGSX_COUNT] = {
  "IX",
  "IY",
  "IXH", "IXL",
  "IYH", "IYL",
  "I", "R",
};


//==========================================================================
//
//  translate
//
//==========================================================================
static void translate (VStr *text, int mode_lower) {
  if (!text || text->length == 0) return;
  vstr_make_unique(text);

  size_t pos = 0;
  while (pos < text->length) {
    /* comment? */
    if (text->data[pos] == ';') {
      while (pos < text->length && text->data[pos] != '\n') ++pos;
      continue;
    }
    if ((unsigned)(text->data[pos]&0xff) <= 32) { ++pos; continue; }
    /* af' */
    if (pos >= 2 && text->data[pos] == '\'' &&
        toLower(text->data[pos-2]) == 'a' &&
        toLower(text->data[pos-1]) == 'f')
    {
      ++pos;
      continue;
    }
    /* string? */
    if (text->data[pos] == '"' || text->data[pos] == '\'') {
      const char ech = text->data[pos++];
      while (pos < text->length) {
        const char ch = text->data[pos++];
        if (ch == '\n' || ch == '\r') break;
        if (ch == '\\') { ++pos; continue; }
        if (ch == ech) break;
      }
      continue;
    }
    /* token should start with alpha */
    if (isAlpha(text->data[pos])) {
      /* find token end */
      size_t epos = pos+1;
      while (epos < text->length) {
        const char ch = text->data[epos];
        if ((unsigned)(ch&0xff) <= 32 || ch == ';' || ch == '"' || ch == '\'') break;
        if (!isIdChar(ch)) break;
        ++epos;
      }
      const int found =
        vstr_part_check_token_list(text, pos, epos-pos, URASM_TOKENS, URASM_MAX_TOKEN) ||
        vstr_part_check_token_list(text, pos, epos-pos, URA_REGS8, 8) ||
        vstr_part_check_token_list(text, pos, epos-pos, URA_REGS16, 4) ||
        vstr_part_check_token_list(text, pos, epos-pos, URA_REGS16A, 4) ||
        vstr_part_check_token_list(text, pos, epos-pos, URA_REGSX, URA_REGSX_COUNT) ||
        vstr_part_check_token_list(text, pos, epos-pos, URA_COND, 8) ||
        vstr_part_check_token_list(text, pos, epos-pos, asmCommands, ASM_COMMAND_TOKENS) ||
        (!mode_lower && vstr_part_check_token_list(text, pos, epos-pos, UrAsmCommands, URASM_COMMAND_TOKENS));
      /* fix case if found */
      if (found) {
        for (; pos < epos; ++pos) {
          text->data[pos] = (mode_lower ? toLower(text->data[pos]) : toUpper(text->data[pos]));
        }
      } else {
        pos = epos;
      }
      continue;
    }
    /* skip token */
    if (isIdChar(text->data[pos])) {
      while (pos < text->length) {
        const char ch = text->data[pos];
        if ((unsigned)(ch&0xff) <= 32 || ch == ';' || ch == '"' || ch == '\'') break;
        if (!isIdChar(ch)) break;
        ++pos;
      }
    } else {
      /* skip delimiter */
      ++pos;
    }
  }
}


//==========================================================================
//
//  main
//
//==========================================================================
int main (int argc, char *argv[]) {
  VStr infile;
  VStr outfile;
  vstr_init(&infile);
  vstr_init(&outfile);

  int mode_lower = 1;
  int nomoreargs = 0;

  for (int f = 1; f < argc; ) {
    const char *s = argv[f++];
    if (!s || !s[0]) continue;
    if (!nomoreargs) {
      if (strcmp(s, "--") == 0) { nomoreargs = 1; continue; }
      if (s[0] == '-') {
        if (strcmp(s, "--lo") == 0) { mode_lower = 1; continue; }
        if (strcmp(s, "--hi") == 0 || strcmp(s, "--up") == 0) { mode_lower = 0; continue; }
        if (strcmp(s, "--help") == 0 || strcmp(s, "--h") == 0 ||
            strcmp(s, "-help") == 0 || strcmp(s, "-h") == 0 ||
            strcmp(s, "--?") == 0 || strcmp(s, "-?") == 0)
        {
          printf("urlocase: convert UrAsm Z80 assembler source case\n"
                 "options:\n"
                 "  --lo  convert to lower case (default)\n"
                 "  --up  convert to upper case\n");
          return 0;
        }
        if (strcmp(s, "-o") == 0) {
          if (outfile.length) {
            fprintf(stderr, "FATAL: too many output files!\n");
            return 1;
          }
          if (f >= argc || !argv[f] || !argv[f][0]) {
            fprintf(stderr, "FATAL: output file name expected for '-o'!\n");
            return 1;
          }
          vstr_set_static(&outfile, argv[f++]);
          continue;
        }
        fprintf(stderr, "FATAL: unknown argument '%s'!\n", s);
        return 1;
      }
    }
    if (!infile.length) {
      vstr_set_static(&infile, s);
      continue;
    }
    /*
    if (!outfile.length) {
      vstr_set_static(&outfile, s);
      continue;
    }
    */
    fprintf(stderr, "FATAL: too many arguments!\n");
    return 1;
  }

  if (!infile.length) {
    fprintf(stderr, "FATAL: no input file!\n");
    return 1;
  }

  if (!outfile.length) vstr_copy(&outfile, &infile);

  VStr text;
  vstr_init(&text);
  load_file(&infile, &text);

  translate(&text, mode_lower);

  save_file(&outfile, &text);

  printf("%s -> %s (%s): done\n", infile.data, outfile.data, (mode_lower ? "lo" : "UP"));

  vstr_free(&infile);
  vstr_free(&outfile);
  vstr_free(&text);

  return 0;
}

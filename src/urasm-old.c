// URASM Z80 assembler
// coded by Ketmar // Invisible Vector
// GPLv3 ONLY
//
#ifndef _GNU_SOURCE
# define _GNU_SOURCE
#endif

#include <getopt.h>
#include <setjmp.h>
#include <stdarg.h>
#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#ifdef WIN32
# include <windows.h>
#endif

#include "liburasm/liburasm.h"
#include "libfdc/libfdc.h"
#include "liburforth/urforth.h"

#include "ursna48.c"


#define UR_FORCE_INLINE static inline __attribute__((always_inline))
#define UR_INLINE static inline


#define VERSION_HI  0
#define VERSION_MID 2
#define VERSION_LO  2


#define MAYBE_UNUSED __attribute__((unused))

#define lambda(return_type, body_and_args)  ({ \
  return_type __fn__ body_and_args \
  __fn__; \
})


// ////////////////////////////////////////////////////////////////////////// //
static const char *ur_assert_failure (const char *cond, const char *fname, int fline,
                                      const char *func)
{
  for (const char *t = fname; *t; ++t) {
    #ifdef WIN32
    if (*t == '/' || *t == '\\') fname = t+1;
    #else
    if (*t == '/') fname = t+1;
    #endif
  }
  fflush(stdout);
  fprintf(stderr, "\n%s:%d: Assertion in `%s` failed: %s\n", fname, fline, func, cond);
  fflush(stderr);
  abort();
}

#define ur_assert(cond_)  do { if (__builtin_expect((!(cond_)), 0)) { ur_assert_failure(#cond_, __FILE__, __LINE__, __PRETTY_FUNCTION__); } } while (0)


//==========================================================================
//
//  joaatHashStr
//
//==========================================================================
static uint32_t joaatHashStr (const void *buf) {
  uint32_t hash = 0x29a;
  const uint8_t *s = (const uint8_t *)buf;
  size_t len = (s != NULL ? strlen((const char *)s) : 0);
  while (len--) {
    hash += (*s++);
    hash += hash<<10;
    hash ^= hash>>6;
  }
  // finalize
  hash += hash<<3;
  hash ^= hash>>11;
  hash += hash<<15;
  return hash;
}


//==========================================================================
//
//  joaatHashStrCI
//
//==========================================================================
static uint32_t joaatHashStrCI (const void *buf) {
  uint32_t hash = 0x29a;
  const uint8_t *s = (const uint8_t *)buf;
  size_t len = (s != NULL ? strlen((const char *)s) : 0);
  while (len--) {
    hash += (*s++)|0x20; // this converts ASCII capitals to locase (and destroys other, but who cares)
    hash += hash<<10;
    hash ^= hash>>6;
  }
  // finalize
  hash += hash<<3;
  hash ^= hash>>11;
  hash += hash<<15;
  return hash;
}


////////////////////////////////////////////////////////////////////////////////
UR_FORCE_INLINE int isSpace (char ch) { return (ch && ((unsigned)(ch&0xff) <= 32 || ch == 127)); }
UR_FORCE_INLINE int isAlpha (char ch) { return ((ch >= 'A' && ch <= 'Z') || (ch >= 'a' && ch <= 'z')); }
UR_FORCE_INLINE int isDigit (char ch) { return (ch >= '0' && ch <= '9'); }
UR_FORCE_INLINE int isHexDigit (char ch) { return ((ch >= '0' && ch <= '9') || (ch >= 'A' && ch <= 'F') || (ch >= 'a' && ch <= 'f')); }
UR_FORCE_INLINE int isAlphaDigit (char ch) { return (isAlpha(ch) || isDigit(ch)); }

UR_FORCE_INLINE char toUpper (char ch) { return (ch >= 'a' && ch <= 'z' ? ch-'a'+'A' : ch); }
UR_FORCE_INLINE char toLower (char ch) { return (ch >= 'A' && ch <= 'Z' ? ch-'A'+'a' : ch); }

static int digitInBase (char ch, int base) {
  if (ch < '0') return -1;
  if (base <= 10) {
    if (ch >= '0'+base) return -1;
    return ch-'0';
  }
  ch = toUpper(ch);
  if (ch <= '9') return ch-'0';
  if (ch < 'A' || ch > 'A'+base-10) return -1;
  ch -= 'A'-10;
  return (ch < base ? ch : -1);
}


//==========================================================================
//
//  strEquCI
//
//==========================================================================
static int strEquCI (const char *s0, const char *s1) {
  int res = 1;
  while (res && *s0 && *s1) {
    char c0 = *s0++; if (c0 >= 'A' && c0 <= 'Z') c0 = c0 - 'A' + 'a';
    char c1 = *s1++; if (c1 >= 'A' && c1 <= 'Z') c1 = c1 - 'A' + 'a';
    res = (c0 == c1);
  }
  return (res && s0[0] == 0 && s1[0] == 0);
}


////////////////////////////////////////////////////////////////////////////////
static char *strprintfVA (const char *fmt, va_list vaorig) {
  char *buf = NULL;
  int olen, len = 128;
  //
  buf = malloc(len);
  if (buf == NULL) { fprintf(stderr, "\nFATAL: out of memory!\n"); abort(); }
  for (;;) {
    char *nb;
    va_list va;
    //
    va_copy(va, vaorig);
    olen = vsnprintf(buf, len, fmt, va);
    va_end(va);
    if (olen >= 0 && olen < len) return buf;
    if (olen < 0) olen = len*2-1;
    nb = realloc(buf, olen+1);
    if (nb == NULL) { fprintf(stderr, "\nFATAL: out of memory!\n"); abort(); }
    buf = nb;
    len = olen+1;
  }
}


static __attribute__((format(printf,1,2))) char *strprintf (const char *fmt, ...) {
  char *buf = NULL;
  va_list va;
  //
  va_start(va, fmt);
  buf = strprintfVA(fmt, va);
  va_end(va);
  return buf;
}


///////////////////////////////////////////////////////////////////////////////
// global variables
//
enum {
  AMODE_NORMAL = 0,
  AMODE_UFO = 1, // scanning UrForth code
};

static int asmMode = AMODE_NORMAL;

static char *ufoIncludeDir = NULL;
static char *sysIncludeDir = NULL;
static char *refFileName = NULL;
static char *lastIncludePath = NULL;
static char *lastSysIncludePath = NULL;
static int urasm_dump_all_labels = 1; // 0: do not write all-uppercase labels

#define MAX_LINE_SIZE  (1024*1024)
static char currLine[MAX_LINE_SIZE]; /* current processing line; can be freely modified */

// set by `processLabel()`
static char currSeenLabel[1024];

static uint32_t ufoMacroVocId = 0;


///////////////////////////////////////////////////////////////////////////////
// init dirs
//
static void initInclideDir (void) {
  const char *id = getenv("URASM_INCLUDE_DIR");
  if (id && id[0]) {
    sysIncludeDir = strdup(id);
  } else {
    char myDir[4096];
    memset(myDir, 0, sizeof(myDir));
    #ifndef WIN32
    if (readlink("/proc/self/exe", myDir, sizeof(myDir)-1) < 0) {
      strcpy(myDir, ".");
    } else {
      char *p = (char *)strrchr(myDir, '/');
      if (!p) strcpy(myDir, "."); else *p = '\0';
    }
    #else
    GetModuleFileName(GetModuleHandle(NULL), myDir, sizeof(myDir)-1);
    char *p = strrchr(myDir, '\\');
    if (!p) strcpy(myDir, "."); else *p = '\0';
    #endif
    strcat(myDir, "/libs");
    sysIncludeDir = strdup(myDir);
  }
  while (sysIncludeDir[0] && sysIncludeDir[strlen(sysIncludeDir)-1] == '/') sysIncludeDir[strlen(sysIncludeDir)-1] = '\0';
  if (!sysIncludeDir[0]) strcpy(sysIncludeDir, ".");
}


static void initUFEInclideDir (void) {
  const char *id = getenv("URASM_URFORTH_INCLUDE_DIR");
  if (id && id[0]) {
    ufoIncludeDir = strdup(id);
  } else {
    char myDir[4096];
    memset(myDir, 0, sizeof(myDir));
    #ifndef WIN32
    if (readlink("/proc/self/exe", myDir, sizeof(myDir)-1) < 0) {
      strcpy(myDir, ".");
    } else {
      char *p = (char *)strrchr(myDir, '/');
      if (!p) strcpy(myDir, "."); else *p = '\0';
    }
    #else
    GetModuleFileName(GetModuleHandle(NULL), myDir, sizeof(myDir)-1);
    char *p = strrchr(myDir, '\\');
    if (!p) strcpy(myDir, "."); else *p = '\0';
    #endif
    strcat(myDir, "/urflibs");
    ufoIncludeDir = strdup(myDir);
  }
  while (ufoIncludeDir[0] && ufoIncludeDir[strlen(ufoIncludeDir)-1] == '/') ufoIncludeDir[strlen(ufoIncludeDir)-1] = '\0';
  if (!ufoIncludeDir[0]) strcpy(ufoIncludeDir, ".");
}


///////////////////////////////////////////////////////////////////////////////
// string utilities
//
/* trim trailing spaces and comments; normalize colons */
static void normalizeStr (char *s) {
  if (asmMode == AMODE_UFO) return; // do nothing
  char *p = s;
  /* now skip all shit */
  while (*p) {
    const char ch = *p++;
    /* check for "af'" */
    if (ch == '\'' && p-s >= 2 && toLower(p[-2] == 'a') && toLower(p[-1] == 'f')) continue;
    /* comment */
    if (ch == ';') { p[-1] = 0; break; }
    /* string */
    if (ch == '"' || ch == '\'') {
      const char qch = ch;
      while (*p) {
        const char c1 = *p++;
        if (c1 == qch) break;
        if (c1 == '\\' && *p) ++p;
      }
      continue;
    }
    /* reduce and normalise colons */
    if (ch == ':') {
      --p; /* back to colon */
      /* remove spaces before colon */
      char *t = p;
      while (t != s && isSpace(t[-1])) --t;
      if (t != p) memmove(t, p, strlen(p)+1);
      p = t;
      ur_assert(p[0] == ':');
      ++p; /* skip colon */
      /* remove following spaces and colons */
      t = p;
      while (*t == ':' || isSpace(*t)) ++t;
      if (t != p) memmove(p, t, strlen(t)+1);
      continue;
    }
  }
  /* done; trim trailing spaces and colons */
  size_t slen = strlen(s);
  while (slen > 0 && (isSpace(s[slen-1]) || s[slen-1] == ':')) --slen;
  s[slen] = 0;
}


/* check if string starts with the given command (case-insensitive) */
/* returns NULL or pointer to args */
/* skips spaces after command if any */
static char *strIsCommand (const char *command, char *str) {
  for (int cnt = 1; cnt > 0; --cnt) {
    while (*str && isSpace(*str)) ++str; // skip spaces
    if (*str == '$') ++str;
    for (; *command && *str; ++command, ++str) {
      if (toUpper(*command) != toUpper(*str)) return NULL; // alas
    }
    if (*command) return NULL; // alas
    if (*str && isAlphaDigit(*str)) return NULL; // alas
    while (*str && isSpace(*str)) ++str; // skip spaces
    if (asmMode != AMODE_UFO) {
      if (*str && *str == ':') break; // try again if we have a colon
    }
    return str; // found
  }
  return NULL;
}


/* parse string literal */
/* don't free() result */
/* skips trailing spaces */
static char *parseStr (char **str, char endQ, int *lenp) {
  static char buf[MAX_LINE_SIZE];
  int len = 0, n, f, base;
  char *a = *str;
  memset(buf, 0, sizeof(buf));
  if (lenp) *lenp = 0;
  for (; *a; ++a) {
    if (*a == '\\') {
      if (!a[1]) break;
      switch (*(++a)) {
        case 'a': buf[len++] = '\a'; break;
        case 'b': buf[len++] = '\b'; break;
        case 'e': buf[len++] = '\x1b'; break;
        case 'f': buf[len++] = '\f'; break;
        case 'n': buf[len++] = '\n'; break;
        case 'r': buf[len++] = '\r'; break;
        case 't': buf[len++] = '\t'; break;
        case 'v': buf[len++] = '\v'; break;
        case 'z': buf[len++] = '\0'; break;
        case 'x': case 'X': // hex
          ++a; // skip 'x'
          base = 16; f = 2;
donum:    for (n = 0; f > 0; --f) {
            char ch = digitInBase(*a++, base);
            //
            if (ch < 0) { --a; break; }
            n *= base;
            n += ch;
          }
          buf[len++] = n;
          --a; // return to the last digit, 'for' will skip it
          break;
        case '0': // octal
          base = 8; f = 4;
          goto donum;
        case '1' ... '9': // decimal
          base = 10; f = 3;
          goto donum;
        default: buf[len++] = a[0]; break; // others
      }
    } else {
      if (*a == endQ) { ++a; break; }
      buf[len++] = *a;
    }
  }
  while (*a && isSpace(*a)) ++a; // skip trailing spaces
  *str = a;
  buf[len] = '\0';
  if (lenp) *lenp = len;
  return buf;
}


///////////////////////////////////////////////////////////////////////////////
// source file stack, reader, etc
//
typedef struct SourceLine {
  struct SourceLine *next;
  char *line;
  const char *fname;
  int lineNo;
  int system;
} SourceLine;

static SourceLine *asmText = NULL;
static SourceLine *currSrcLine = NULL;

typedef struct FileInfo_s {
  struct FileInfo_s *next;
  char *name;
} FileInfo;

static FileInfo *knownFiles = NULL;


static void clearKnownFiles (void) {
  while (knownFiles) {
    FileInfo *fi = knownFiles;
    knownFiles = fi->next;
    if (fi->name) free(fi->name);
    free(fi);
  }
}


static const char *appendKnownFileName (const char *fn) {
  if (!fn || !fn[0]) return "<unknown>";
  for (FileInfo *fi = knownFiles; fi; fi = fi->next) {
    if (strcmp(fi->name, fn) == 0) return fi->name;
  }
  FileInfo *fi = calloc(1, sizeof(FileInfo));
  ur_assert((fi->name = strdup(fn)) != NULL);
  fi->next = knownFiles;
  knownFiles = fi;
  return fi->name;
}


// ////////////////////////////////////////////////////////////////////////// //
#define MAX_MACRO_ARGS  (32)

typedef struct MacroDef {
  struct MacroDef *next;
  char *name;
  SourceLine *lines;
  int argc;
  char *argdefaults[MAX_MACRO_ARGS]; // default values
  char *argnames[MAX_MACRO_ARGS]; // argument names
} MacroDef;

typedef struct {
  MacroDef *mac;
  char *argvals[MAX_MACRO_ARGS]; // argument values
} CurMacroDef;

static MacroDef *maclist = NULL;
static CurMacroDef *curmacro = NULL; // !NULL if we are not inserting macro
static int curmacronum = 0;


static void freeLineList (SourceLine *list) {
  while (list) {
    SourceLine *l = list;
    list = l->next;
    free(l->line);
    //free(l->fname);
    free(l);
  }
}


static void clearMacros (void) {
  while (maclist) {
    MacroDef *md = maclist;
    maclist = md->next;
    for (unsigned f = 0; f < MAX_MACRO_ARGS; ++f) {
      if (md->argdefaults[f]) free(md->argdefaults[f]);
      if (md->argnames[f]) free(md->argnames[f]);
    }
    free(md->name);
    freeLineList(md->lines);
    free(md);
  }
}


static MacroDef *findMacro (const char *name) {
  for (MacroDef *mc = maclist; mc != NULL; mc = mc->next) if (strEquCI(name, mc->name)) return mc;
  return NULL;
}


static void asmTextClear (void) {
  freeLineList(asmText);
  asmText = NULL;
  currSrcLine = NULL;
}


#define currLineTrimBlanks()  do { \
  while (slen && *(uint8_t *)(currLine+slen-1) <= 32) --slen; \
  currLine[slen] = 0; \
} while (0)

#define currLineRemoveComment()  do { \
  size_t pos = 0; \
  char instr = 0; \
  while (pos < slen) { \
    if (instr) { \
      if (currLine[pos] == '\\' && pos+1 < slen) { \
        ++pos; \
      } else if (currLine[pos] == instr) { \
        instr = 0; \
      } \
    } else { \
      if (currLine[pos] == ';') { slen = pos; break; } \
      if (currLine[pos] == '"') { \
        instr = '"'; \
      } else if (currLine[pos] == '\'') { \
        if (pos == 0 || \
            (currLine[pos-1] != '_' && !isAlphaDigit(currLine[pos-1]))) \
        { \
          instr = currLine[pos]; \
        } \
      } \
    } \
    ++pos; \
  } \
  while (slen && *(uint8_t *)(currLine+slen-1) <= 32) --slen; \
  currLine[slen] = 0; \
} while (0)


static SourceLine *loadTextFile (FILE *fl, int system, const char *fname, int *error) {
  if (error) *error = 0;

  fname = appendKnownFileName(fname);
  int continuation = 0;

  SourceLine *first = NULL;
  SourceLine *last = NULL;
  int lineNo = 0;

  // read file
  while (fgets(currLine, sizeof(currLine)-1, fl)) {
    ++lineNo;
    currLine[sizeof(currLine)-1] = '\0';
    size_t slen = strlen(currLine);

    if (slen == 0 || (currLine[slen-1] != '\n' && currLine[slen-1] != '\r')) {
      if (!feof(fl)) {
        if (error) *error = 1;
        fprintf(stderr, "ERROR: file %s, line %d: line too long\n", fname, lineNo);
        freeLineList(first);
        return NULL;
      }
    }

    currLineTrimBlanks();

    int new_cont = 0;
    if (slen && currLine[slen-1] == '\\') {
      // this is a possible continuation line
      if (slen > 1 && currLine[slen-2] == '\\') {
        // nope, fix it
        slen -= 1;
        currLine[slen] = 0;
      } else {
        new_cont = 1;
        --slen;
        currLineTrimBlanks();
      }
    }

    //!normalizeStr(currLine);
    if (!currLine[0]) {
      // don't store empty lines
      continuation = new_cont;
      continue;
    }

    // remove comments from continuations
    if (continuation || new_cont) {
      currLineRemoveComment();
    }

    // continuation?
    if (continuation) {
      // join continuation with the last line
      ur_assert(last);
      size_t newlen = slen+strlen(last->line)+4;
      if (newlen > MAX_LINE_SIZE-8) {
        if (error) *error = 1;
        fprintf(stderr, "ERROR: too long continuation at line %d in file '%s'\n", lineNo, fname);
        freeLineList(first);
        return NULL;
      }
      char *newbuf = malloc(newlen);
      ur_assert(newbuf); // who cares
      snprintf(newbuf, newlen, "%s%s", last->line, currLine);
      free(last->line);
      last->line = newbuf;
    } else {
      // add current line
      SourceLine *s = calloc(1, sizeof(SourceLine));
      ur_assert(s);
      s->system = system;
      s->lineNo = lineNo;
      ur_assert((s->line = strdup(currLine)) != NULL);
      s->fname = fname;
      if (last) last->next = s; else first = s;
      last = s;
    }

    continuation = new_cont;
  }

  return first;
}


static int asmTextLoad (const char *fname, int system) {
  FILE *fl;
  int error;

  if (!(fl = fopen(fname, "r"))) {
    fprintf(stderr, "ERROR: can't open file: %s\n", fname);
    return -1;
  }
  printf("loading: %s\n", fname);

  SourceLine *first = loadTextFile(fl, 0, fname, &error);
  fclose(fl);

  if (error) return -1;

  SourceLine *last = asmText;
  if (last) {
    while (last->next) last = last->next;
    last->next = first;
  } else {
    asmText = first;
  }
  return 0;
}


static char *extractFileDir (const char *s) {
  if (!s || !s[0]) return strdup(".");
  const char *slash;
  #ifdef WIN32
  slash = NULL;
  for (const char *ts = s; *ts; ++ts) {
    if (*ts == '/' || *ts == '\\') slash = ts;
  }
  if (slash == s && (s[0] == '/' || s[0] == '\\') && (s[1] == '/' || s[1] == '\\')) slash = NULL;
  #else
  slash = strrchr(s, '/');
  #endif
  if (!slash) return strdup(".");
  ptrdiff_t len = (ptrdiff_t)(slash-s)+1;
  char *res = malloc(len+1);
  memcpy(res, s, len);
  res[len] = 0;
  #ifdef WIN32
  while (len > 0 && (res[len-1] == '\\' || res[len-1] == '/')) --len;
  #else
  while (len > 0 && res[len-1] == '/') --len;
  #endif
  if (len == 0) { free(res); return strdup("."); }
  res[len] = 0;
  return res;
}


static void loadCurSrcLine (void) {
  if (currSrcLine) {
    strcpy(currLine, currSrcLine->line);
    /* macros will not change include dirs */
    if (!curmacro) {
      char **incpp = (!currSrcLine->system ? &lastIncludePath : &lastSysIncludePath);
      if (*incpp) free(*incpp);
      *incpp = extractFileDir(currSrcLine->fname);
    }
    normalizeStr(currLine);
  } else {
    currLine[0] = 0;
  }
}


UR_FORCE_INLINE SourceLine *setCurSrcLine (SourceLine *l) {
  currSrcLine = l;
  loadCurSrcLine();
  return l;
}


UR_FORCE_INLINE SourceLine *setUFOCurSrcLine (SourceLine *l) {
  currSrcLine = l;
  if (currSrcLine) {
    strcpy(currLine, currSrcLine->line);
  } else {
    currLine[0] = 0;
  }
  return l;
}


UR_FORCE_INLINE SourceLine *nextSrcLine (void) {
  return (currSrcLine != NULL ? setCurSrcLine(currSrcLine->next) : NULL);
}


UR_FORCE_INLINE SourceLine *nextUFOSrcLine (void) {
  return (currSrcLine != NULL ? setUFOCurSrcLine(currSrcLine->next) : NULL);
}


UR_FORCE_INLINE int strHasPathDelim (const char *s) {
  if (!s || !s[0]) return 0;
  #ifdef WIN32
  return (strchr(s, '/') || strchr(s, '\\') ? 1 : 0);
  #else
  return (strchr(s, '/') ? 1 : 0);
  #endif
}


/* returns malloced string */
static char *createIncludeName (const char *fname, int assystem, const char *defaultmain,
                                const char *lastIncPathUFO) {
  if (!fname || !fname[0]) return NULL;
  char *res;
  if (fname[0] != '/') {
    const char *incdir;
    if (!assystem) {
      incdir = lastIncludePath;
    } else if (assystem == -669) { // ufo normal
      incdir = lastIncPathUFO;
    } else if (assystem == -666) { // ufo system
      incdir = lastIncPathUFO;
      if (!incdir || !incdir[0]) incdir = ufoIncludeDir;
    } else {
      incdir = lastSysIncludePath;
      if (!incdir || !incdir[0]) incdir = sysIncludeDir;
    }
    if (incdir == NULL || incdir[0] == 0) incdir = ".";
    res = strprintf("%s/%s", (incdir && incdir[0] ? incdir : "."), fname);
  } else {
    res = strprintf("%s", fname);
  }
  struct stat st;
  if (defaultmain && defaultmain[0]) {
    if (stat(res, &st) == 0) {
      if (S_ISDIR(st.st_mode)) {
        char *rs = strprintf("%s/%s", res, defaultmain);
        free(res);
        res = rs;
      }
    }
  }
  /* check if there is the disk file */
  if (strHasPathDelim(fname) && stat(res, &st) != 0) {
    /* no file, try "root include" */
    const char *incdir = (!assystem || assystem == -669 ? NULL : assystem == -666 ? ufoIncludeDir : sysIncludeDir);
    char *rs = strprintf("%s/%s", (incdir && incdir[0] ? incdir : "."), fname);
    free(res);
    res = rs;
    /* check for dir again */
    if (defaultmain && defaultmain[0]) {
      if (stat(res, &st) == 0) {
        if (S_ISDIR(st.st_mode)) {
          char *rs = strprintf("%s/%s", res, defaultmain);
          free(res);
          res = rs;
        }
      }
    }
  }
  //fprintf(stderr, "inc: fname=<%s>; sys=%d; def=<%s>; res=<%s>\n", fname, assystem, defaultmain, res);
  return res;
}


static int includeCount = 0;

// process 'INCLUDE'
// include file instead of the current line
// returns 0 on ok, <0 on error
static int asmTextInclude (const char *fname, int system, int softinclude) {
  char *fn = createIncludeName(fname, system, "zzmain.zas", NULL);

  FILE *fl = fopen(fn, "r");
  if (!fl) {
    if (softinclude) return 0;
    fprintf(stderr, "ERROR: file %s, line %d: can't open INCLUDE file: '%s'\n", currSrcLine->fname, currSrcLine->lineNo, currLine);
    free(fn);
    return -1;
  }

  if (includeCount > 256) {
    fclose(fl);
    free(fn);
    fprintf(stderr, "ERROR: file %s, line %d: too many nested INCLUDEs!\n", currSrcLine->fname, currSrcLine->lineNo);
    return -1;
  }

  ++includeCount;
  printf("loading: %s\n", fn);

  int error;
  SourceLine *first = loadTextFile(fl, system, fn, &error);
  fclose(fl);
  free(fn);

  if (error) return -1;

  --includeCount;
  currSrcLine->line[0] = 0; // remove `include` directive

  SourceLine *last = first;
  if (last) {
    while (last->next) last = last->next;
    last->next = currSrcLine->next;
    currSrcLine->next = first;
  }
  return 0;
}


///////////////////////////////////////////////////////////////////////////////
// prototypes
//
static void processCurrentLine (void); // only one, will skip to next one


///////////////////////////////////////////////////////////////////////////////
// error raisers, etc
//
static jmp_buf errJP;


static void errorWriteFile (FILE *fo) {
  if (currSrcLine) {
    size_t slen = strlen(currSrcLine->line);
    fprintf(fo, "at file %s, line %d\n%.70s%s\n*", currSrcLine->fname,
            currSrcLine->lineNo, currSrcLine->line, (slen > 70 ? " ..." : ""));
  } else {
    fprintf(fo, "somewhere in time: ");
  }
}


static void errorMsgV (const char *fmt, va_list ap) {
  errorWriteFile(stderr);
  vfprintf(stderr, fmt, ap);
  va_end(ap);
  fputc('\n', stderr);
  fflush(stderr);
}


__attribute__((format(printf, 1, 2)))
static void warningMsg (const char *fmt, ...) {
  va_list ap;
  fprintf(stderr, "WARNING ");
  va_start(ap, fmt);
  errorMsgV(fmt, ap);
}


__attribute__((format(printf, 1, 2)))
static void errorMsg (const char *fmt, ...) {
  va_list ap;
  fprintf(stderr, "FATAL ");
  va_start(ap, fmt);
  errorMsgV(fmt, ap);
}


__attribute__((noreturn)) __attribute__((format(printf, 1, 2)))
static void fatal (const char *fmt, ...) {
  va_list ap;
  va_start(ap, fmt);
  errorMsgV(fmt, ap);
  longjmp(errJP, 666);
}


__attribute__((noreturn))
static void fatalUrLib (int errcode) {
  errorMsg("%s", urasm_errormsg(errcode));
  longjmp(errJP, 666);
}


///////////////////////////////////////////////////////////////////////////////
// writer options
static int optWriteType = 't';
static int optWTChanged = 0;
static int optWriteFixups = 0;
static int optFixupType = 0; // 0: text file; 1: zas file; 2: difzas; 3: zas with zero endmarker
static int optRunTape = 1;
static int optRunDMB = 1;
static int optRunSCL = -1; /* -1: save cargador, but no boot; 1: boot and cargador */
static int optTapExt = 0; /* set '.tap' extension (but don't write tape; this for inline tape blocks) */
static int optSNA48 = 1;
static char *optOutputDir = NULL;
static char *optOutputFile = NULL;
static int optOutputFileFixed = 0;


static char *buildOutputFileName (const char *basename, const char *ext) {
  ur_assert(ext);
  ur_assert(basename);
  if (ext[0] == '.') ++ext;
  //fprintf(stderr, "********** <%s> <%s> <%s> %d <%s>\n", basename, ext, optOutputFile, optOutputFileFixed, optOutputDir);
  if (optOutputFile) {
    if (!optOutputFileFixed) {
      optOutputFileFixed = 1;
      char *spp = strrchr(optOutputFile, '/');
      #ifdef WIN32
      char *spp1 = strrchr(optOutputFile, '\\');
      if (!spp || (spp < spp1)) spp = spp1;
      #endif
      if (!spp) spp = optOutputFile; else ++spp;
      char *dotpos = strrchr(spp, '.');
      if (dotpos) *dotpos = 0;
    }
    if (optOutputFile && optOutputFile[0]) return strprintf("%s/%s.%s", optOutputDir, optOutputFile, ext);
  }
  return strprintf("%s/%s.%s", optOutputDir, basename, ext);
}


typedef struct OutDataFile_t {
  char *name;
  char type; // 'C', 'B'
  int hasstart;
  uint16_t start; // start address, or basic start line
  uint16_t size; // data size (never 0)
  uint8_t *data;
  struct OutDataFile_t *next;
} OutDataFile;

static OutDataFile *datafiles = NULL;


static OutDataFile *appendDataData (const void *bytes, uint16_t start, uint16_t count) {
  if (!count) return NULL;

  OutDataFile *off = malloc(sizeof(OutDataFile));
  off->name = NULL;
  off->type = 'C';
  off->hasstart = 0;
  off->start = start;
  off->size = count;
  off->data = malloc(count);
  if (bytes) memcpy(off->data, bytes, count); else memset(off->data, 0, count);
  off->next = NULL;

  OutDataFile *last = datafiles;
  if (!last) {
    datafiles = off;
  } else {
    while (last->next) last = last->next;
    last->next = off;
  }

  return off;
}


static OutDataFile *appendDataFile (const char *fname, long ofs, int len, int allowskip) {
  if (!fname || !fname[0]) return NULL;

  FILE *fl = fopen(fname, "rb");
  if (!fl) {
    if (allowskip) return NULL;
    fatal("cannot open data file '%s'", fname);
  }
  if (fseek(fl, 0, SEEK_END) < 0) fatal("error reading data file '%s'", fname);
  long fsize = ftell(fl);
  if (fsize < 0) fatal("error reading data file '%s'", fname);
  if (ofs < 0) {
    // from file end
    ofs = -ofs;
    if (ofs < 0) fatal("invalid offset");
    if (ofs >= fsize) ofs = 0; else ofs = fsize-ofs;
  }
  if (ofs >= fsize) { fclose(fl); return NULL; }
  if (fseek(fl, ofs, SEEK_SET) < 0) fatal("error reading data file '%s'", fname);
  fsize -= ofs;
  if (len < 0) {
    if (fsize > 65535) fatal("data file '%s' too big", fname);
    len = (int)fsize;
  }
  if (len > 65535) fatal("data file '%s' too big", fname);
  if (len == 0) { fclose(fl); return NULL; }

  OutDataFile *off = malloc(sizeof(OutDataFile));
  off->name = NULL;
  off->type = 'C';
  off->hasstart = 0;
  off->start = 0;
  off->size = (uint16_t)len;
  off->data = malloc(len);
  off->next = NULL;
  if (fread(off->data, (unsigned)len, 1, fl) != 1) fatal("error reading data file '%s'", fname);
  fclose(fl);

  OutDataFile *last = datafiles;
  if (!last) {
    datafiles = off;
  } else {
    while (last->next) last = last->next;
    last->next = off;
  }

  return off;
}


//////////////////////////////////////////////////////////////////////////////
// operator management
//
// return !0 to skip current line
typedef int (*UrAsmOpFn) (void);

enum {
  PI_CONT_LINE = 0,
  PI_SKIP_LINE = 1
};

typedef struct UrAsmOp_t {
  uint32_t hash;
  struct UrAsmOp_t *hlink;
  // other
  char *name;
  UrAsmOpFn fn;
  void *udata; // user data (struct info for structs, for example)
  struct UrAsmOp_t *next;
} UrAsmOp;

#define UR_OPER_BUCKETS  (256)
static UrAsmOp *oplist = NULL;
static UrAsmOp *urCurrentOp = NULL;
static UrAsmOp *operBuckets[UR_OPER_BUCKETS];


//==========================================================================
//
//  urAddOpEx
//
//==========================================================================
static UrAsmOp *urAddOpEx (const char *name, UrAsmOpFn fn, char optpfx) {
  ur_assert(name != NULL && name[0] != 0);
  if (oplist == NULL) {
    //fprintf(stderr, "CLEAR OPBUCKETS!\n");
    for (int f = 0; f < UR_OPER_BUCKETS; f += 1) operBuckets[f] = NULL;
  }
  UrAsmOp *res = calloc(1, sizeof(UrAsmOp));
  ur_assert(res);
  if (optpfx != 0) {
    res->name = calloc(1, strlen(name) + 2);
    ur_assert(res->name);
    res->name[0] = optpfx;
    strcpy(res->name + 1, name);
  } else {
    res->name = strdup(name);
    ur_assert(res->name);
  }
  res->fn = fn;
  res->udata = NULL;
  res->next = oplist;
  oplist = res;
  // insert into hash table
  res->hash = joaatHashStrCI(res->name);
  const uint32_t bkt = res->hash%UR_OPER_BUCKETS;
  //fprintf(stderr, "NEWOP: 0x%08x (%u) : <%s>\n", res->hash, bkt, res->name);
  res->hlink = operBuckets[bkt];
  operBuckets[bkt] = res;
  return res;
}


//==========================================================================
//
//  urAddOp
//
//==========================================================================
static UrAsmOp *urAddOp (const char *name, UrAsmOpFn fn) {
  return urAddOpEx(name, fn, 0);
}


//==========================================================================
//
//  urAddOpAndDollar
//
//==========================================================================
static void urAddOpAndDollar (const char *name, UrAsmOpFn fn) {
  (void)urAddOpEx(name, fn, 0);
  (void)urAddOpEx(name, fn, '$');
}


//==========================================================================
//
//  urFindOp
//
//==========================================================================
static UrAsmOp *urFindOp (const char *name) {
  if (name == NULL || name[0] == 0) return NULL;
  #if 0
  UrAsmOp *res;
  if (name[0] == '$' && name[1]) {
    for (res = oplist; res; res = res->next) {
      if (res->name[0] == '$') {
        if (strEquCI(name, res->name)) break;
      } else {
        if (strEquCI(name+1, res->name)) break;
      }
    }
  } else {
    for (res = oplist; res; res = res->next) if (strEquCI(name, res->name)) break;
  }
  return res;
  #else
  const uint32_t hash = joaatHashStrCI(name);
  //fprintf(stderr, "LOOK: 0x%08x (%u) : <%s>\n", hash, hash%UR_OPER_BUCKETS, name);
  UrAsmOp *res = operBuckets[hash%UR_OPER_BUCKETS];
  while (res != NULL && (res->hash != hash || !strEquCI(name, res->name))) {
    //fprintf(stderr, "  REJECT: 0x%08x: <%s>\n", res->hash, res->name);
    res = res->hlink;
  }
  return res;
  #endif
}


//==========================================================================
//
//  urClearOps
//
//==========================================================================
static void urClearOps (void) {
  while (oplist != NULL) {
    UrAsmOp *c = oplist;
    oplist = oplist->next;
    free(c->name);
    free(c);
  }
  for (int f = 0; f < UR_OPER_BUCKETS; f += 1) operBuckets[f] = NULL;
}


///////////////////////////////////////////////////////////////////////////////
// label management
//
enum {
  LBL_TYPE_VERY_SPECIAL = -42,
  LBL_TYPE_UNKNOWN = -1,
  LBL_TYPE_ASS = 0, // `=`
  LBL_TYPE_EQU = 1, // equ
  LBL_TYPE_CODE = 2, // ':', or "lbl opcode"
  LBL_TYPE_STOFS = 3, // struct offset
  LBL_TYPE_DATA = 4,
};

typedef struct UrLabelInfo_t {
  uint32_t hash;
  struct UrLabelInfo_t *hlink;
  // other
  char *name;
  int32_t value;
  int type; /* -1: uknown (forward reference); 0: =; 1: equ; 2: code */
  int known; /* !0: label value already known */
  int refLine; /* first referenced line */
  int fixuptype; /* UR_FIXUP_XXX */
  int modHidden; /* hidden module label? hack for PLUS3BOOT */
  char savedFirstChar; /* for hidden labels */
  char *refFile;
  struct UrLabelInfo_t *next;
  // previous temp label
  struct UrLabelInfo_t *prevTemp;
  int tempIdx;
  int tempFwd;
} UrLabelInfo;


#define UR_LABEL_BUCKETS  (256)
static UrLabelInfo *labels = NULL;
static UrLabelInfo *labelsTail = NULL;
static UrLabelInfo *labelBuckets[UR_LABEL_BUCKETS];
// list of all @@, from the last defined
static UrLabelInfo *tempLabels = NULL;
static int tempLabelIdx = 0;


//==========================================================================
//
//  initLabelBuckets
//
//==========================================================================
static void initLabelBuckets (void) {
  for (int f = 0; f < UR_LABEL_BUCKETS; f += 1) labelBuckets[f] = NULL;
}


//==========================================================================
//
//  urClearLabels
//
//==========================================================================
static void urClearLabels (void) {
  UrLabelInfo *c;
  while ((c = labels) != NULL) {
    labels = c->next;
    if (c->name) free(c->name);
    if (c->refFile) free(c->refFile);
    free(c);
  }
  labelsTail = NULL;
  initLabelBuckets();
}


//==========================================================================
//
//  urFindLabel
//
//==========================================================================
static UrLabelInfo *urFindLabel (const char *name) {
  if (name != NULL && name[0] != 0) {
    #if 0
    for (UrLabelInfo *c = labels; c; c = c->next) {
      if (strcmp(name, c->name) == 0) return c;
    }
    #else
    const uint32_t hash = joaatHashStr(name);
    UrLabelInfo *c = labelBuckets[hash%UR_LABEL_BUCKETS];
    while (c != NULL) {
      if (c->hash == hash && strcmp(c->name, name) == 0) return c;
      c = c->hlink;
    }
    #endif
  }
  return NULL;
}


//==========================================================================
//
//  urAddLabel
//
//==========================================================================
static UrLabelInfo *urAddLabel (const char *name) {
  ur_assert(name != NULL && name[0] != 0);
  UrLabelInfo *c = urFindLabel(name);
  if (c == NULL) {
    c = calloc(1, sizeof(UrLabelInfo));
    ur_assert(c);
    c->name = strdup(name);
    ur_assert(c->name);
    c->type = LBL_TYPE_UNKNOWN;
    c->fixuptype = UR_FIXUP_NONE;
    c->modHidden = 0;
    c->savedFirstChar = 0;
    c->next = NULL;
    if (labelsTail) labelsTail->next = c; else labels = c;
    labelsTail = c;
    c->hash = joaatHashStr(c->name);
    const uint32_t bkt = c->hash%UR_LABEL_BUCKETS;
    c->hlink = labelBuckets[bkt];
    labelBuckets[bkt] = c;
  }
  return c;
}


//==========================================================================
//
//  urAddTempLabel
//
//==========================================================================
static UrLabelInfo *urAddTempLabel (void) {
  char name[32];
  tempLabelIdx += 1;
  snprintf(name, sizeof(name), "@@_%05d", tempLabelIdx);
  UrLabelInfo *lbl = urAddLabel(name);
  lbl->tempIdx = tempLabelIdx;
  lbl->prevTemp = tempLabels;
  tempLabels = lbl;
  return lbl;
}


//==========================================================================
//
//  urNextTempLabel
//
//==========================================================================
static UrLabelInfo *urNextTempLabel (void) {
  char name[32];
  tempLabelIdx += 1;
  snprintf(name, sizeof(name), "@@_%05d", tempLabelIdx);
  UrLabelInfo *lbl = urFindLabel(name);
  ur_assert(lbl->tempIdx == tempLabelIdx);
  ur_assert(lbl->prevTemp == NULL);
  lbl->prevTemp = tempLabels;
  tempLabels = lbl;
  return lbl;
}


//==========================================================================
//
//  urResetTempLabels
//
//==========================================================================
static void urResetTempLabels (void) {
  while (tempLabels != NULL) {
    UrLabelInfo *lbl = tempLabels;
    tempLabels = lbl->prevTemp;
    lbl->prevTemp = NULL;
  }
  tempLabels = NULL;
  tempLabelIdx = 0;
}


//==========================================================================
//
//  urAddTempLocal
//
//  add local label; it may shadow others
//
//==========================================================================
static UrLabelInfo *urAddTempLocal (const char *name) {
  UrLabelInfo *c = calloc(1, sizeof(UrLabelInfo));
  ur_assert(c);
  c->name = strdup(name);
  ur_assert(c->name);
  c->type = LBL_TYPE_EQU;
  c->known = 1;
  c->fixuptype = UR_FIXUP_NONE;
  c->modHidden = 0;
  c->savedFirstChar = 0;
  c->next = labels;
  labels = c;
  if (!labelsTail) labelsTail = c;
  c->hash = joaatHashStr(c->name);
  const uint32_t bkt = c->hash%UR_LABEL_BUCKETS;
  c->hlink = labelBuckets[bkt];
  labelBuckets[bkt] = c;
  return c;
}


static void UFOZXLabelRemoved (UrLabelInfo *lbl);


//==========================================================================
//
//  urRemoveTempLocal
//
//==========================================================================
static void urRemoveTempLocal (UrLabelInfo *tmplbl) {
  if (!tmplbl) return;
  ur_assert(tmplbl == labels);
  UFOZXLabelRemoved(tmplbl);
  labels = tmplbl->next;
  if (!labels) labelsTail = NULL;
  const uint32_t bkt = tmplbl->hash%UR_LABEL_BUCKETS;
  ur_assert(labelBuckets[bkt] == tmplbl);
  labelBuckets[bkt] = tmplbl->hlink;
  free(tmplbl->name);
  free(tmplbl);
}


///////////////////////////////////////////////////////////////////////////////
// structures
typedef struct StructField_s {
  struct StructField_s *next; // next field
  char *name; // field name
  uint16_t ofs; // field offset in struct
  uint16_t size; // field size in bytes (can be 0 for aliases)
  int32_t initValue;
  int hasInit;
} StructField;


typedef struct StructInfo_s {
  struct StructInfo_s *next; // next structure
  char *name; // structure name
  StructField *fields;
  uint16_t size; // total structure size in bytes
  int zerofill;
} StructInfo;


// list of all structures
static StructInfo *structList = NULL;


static void freeStructs (void) {
  while (structList != NULL) {
    StructInfo *sth = structList;
    structList = sth->next;
    StructField *fld = sth->fields;
    while (fld) {
      StructField *fc = fld;
      fld = fld->next;
      if (fc->name) free(fc->name);
      free(fc);
    }
    if (sth->name) free(sth->name);
    free(sth);
  }
}


///////////////////////////////////////////////////////////////////////////////
// fixup management
typedef struct FixupItem_s {
  struct FixupItem_s *next;
  uint16_t opdestaddr;
  uint16_t opaddr;
  int fixuptype;
  int size;
} FixupItem;
static FixupItem *fixlisthead = NULL, *fixlisttail = NULL;


static void clearFixups (void) {
  FixupItem *c;
  while ((c = fixlisthead) != NULL) {
    fixlisthead = c->next;
    free(c);
  }
  fixlisthead = fixlisttail = NULL;
}


static void addFixup (uint16_t opdestaddr, uint16_t opaddr, int fixuptype, int size) {
  FixupItem *fx = calloc(1, sizeof(FixupItem));
  //
  fx->opdestaddr = opdestaddr;
  fx->opaddr = opaddr;
  fx->fixuptype = fixuptype;
  fx->size = size;
  //
  if (fixlisttail != NULL) fixlisttail->next = fx; else fixlisthead = fx;
  fx->next = NULL;
  fixlisttail = fx;
}


///////////////////////////////////////////////////////////////////////////////
// destination memory management
//
static uint8_t memory[65536];
static uint8_t memused[65536];
static uint8_t memresv[65536];
static uint16_t start_pc = 0x100; // viva CP/M!
static uint16_t start_disp = 0x100; // viva CP/M!
static uint16_t start_ent = 0x100; // viva CP/M!
static uint16_t pc = 0; /* current position to write */
static uint16_t disp = 0; /* current 'virtual PC' */
static uint16_t ent = 0; /* starting address */
static uint16_t clrAddr = 24999; /* address for CLEAR for cargador */
static int pass = 0; /* 0: collect labels; 1: compiling; 2: compiling and fixing labels */
static int inTapeBlock = 0;
static uint8_t tapeXorB = 0;

static int findChunkFrom (int addr, int *start, int *len);


// find next free area starting from `addr`
// returns address or -1
static int reserveFindFreeFrom (uint16_t addr) {
  for (uint32_t freeaddr = addr; freeaddr <= 0xffffU; ++freeaddr) {
    if (!memresv[freeaddr]) return freeaddr;
  }
  return -1;
}


// find next reserved area starting from `addr`
// returns address or -1
static int reserveFindReservedFrom (uint16_t addr) {
  for (uint32_t freeaddr = addr; freeaddr <= 0xffffU; ++freeaddr) {
    if (memresv[freeaddr]) return freeaddr;
  }
  return -1;
}


static void reserveReleaseUsed (uint16_t addr, int len) {
  while (len--) memused[addr++] = 0;
}


//TODO: wrapping
static int reserveHit (uint16_t addr, int len) {
  if (len <= 0) return 0;
  while (len--) {
    if (memresv[addr++]) return 1;
  }
  return 0;
}


// start from `addr`, look for free areas, and check
// if we can insert jump from a pcaddr there
// returns next free area or -1
static int reservedFindNextJumpableFreeFrom (uint16_t pcaddr, uint16_t addr) {
  // check if there is enough room at least for a jr
  if (memresv[pcaddr] || memresv[pcaddr+1]) return -1;
  uint16_t caddr = addr;
  // skip reserved
  while (caddr <= 0xffffU && memresv[caddr]) ++caddr;
  if (caddr > 0xffffU) return -1; // alas
  // check distance
  uint32_t jrdist = caddr-(pcaddr+2U);
  if (jrdist < 128u) return (int)caddr; // yay, we can JR there, this free area is good
  // cannot JR, check if we have room for JP
  if (memresv[pcaddr+2]) return -1; // alas
  // ok, can JP
  return (int)caddr;
}


// start from `addr`, look for free areas, and check
// if we can insert jump from a pcaddr there, and if
// that area is big enough to hold a next jump and at least one more byte
// returns next free area or -1
static int reservedFindNextSuitableFreeFrom (uint16_t pcaddr, uint16_t addr) {
  int caddr = addr;
  for (;;) {
    caddr = reservedFindNextJumpableFreeFrom(pcaddr, (uint16_t)caddr);
    if (caddr < 0) return -1; // alas
    // we can jump to naddr, now check if it is big enough
    // if it has less than 3 bytes, it is too small
    if (memresv[(uint16_t)(caddr+1)] || memresv[(uint16_t)(caddr+2)]) {
      // too small
      caddr += 3;
      continue;
    }
    // if we have more than 3 bytes, it is definitely ok
    if (!memresv[(uint16_t)(caddr+3)]) return caddr; // ok
    // it has exactly 3 bytes, check if it is enough
    int xaddr = reservedFindNextJumpableFreeFrom(caddr+1, (uint16_t)caddr+3);
    if (xaddr >= 0) return caddr; // ok
    // alas
    caddr = reserveFindReservedFrom(caddr);
    if (caddr < 0) return -1;
    caddr = reserveFindFreeFrom(caddr);
    if (caddr < 0) return -1;
  }
}

UR_FORCE_INLINE uint8_t getByte (uint16_t addr) {
  return memory[addr];
}


UR_FORCE_INLINE uint16_t getWord (uint16_t addr) {
  return ((uint16_t)memory[addr])|(((uint16_t)memory[addr+1])<<8);
}


UR_FORCE_INLINE void putByte (uint16_t addr, uint8_t b) {
  if (inTapeBlock) tapeXorB ^= b;
  memory[addr] = b;
  memused[addr] = 1;
}


UR_FORCE_INLINE void putWord (uint16_t addr, uint16_t w) {
  putByte(addr, w&0xFFU);
  putByte(addr+1, (w>>8)&0xFFU);
}


UR_FORCE_INLINE void emitByte (uint8_t b) {
  putByte(pc, b);
  ++pc;
  ++disp;
}


UR_FORCE_INLINE void emitWord (uint16_t w) {
  emitByte(w&0xFFU);
  emitByte((w>>8)&0xFFU);
}


UR_FORCE_INLINE void emitRWord (uint16_t w) {
  emitByte((w>>8)&0xFFU);
  emitByte(w&0xFFU);
}


static void prepareMemory (void) {
  memset(memory, 0, sizeof(memory));
  memset(memused, 0, sizeof(memused));
  memset(memresv, 0, sizeof(memresv));
}


///////////////////////////////////////////////////////////////////////////////
// module list management
//
typedef struct ModuleInfo_t {
  char *name;
  char *fname; // opened in this file
  int seen; // !0: module already seen, skip other definitions from the same file
  struct ModuleInfo_t *next;
  uint32_t hash;
  struct ModuleInfo_t *hlink;
} ModuleInfo;

#define UR_MODULE_BUCKETS  (1024)
static ModuleInfo *modules = NULL;
static ModuleInfo *currModule = NULL;
static ModuleInfo *moduleBuckets[UR_MODULE_BUCKETS];

static uint8_t mod_memory_saved[65536];
static uint8_t mod_memused_saved[65536];
static uint8_t mod_memresv_saved[65536];


static void modulesClear (void) {
  currModule = NULL;
  while (modules) {
    ModuleInfo *c = modules;
    modules = modules->next;
    free(c->name);
    free(c->fname);
    free(c);
  }
  for (int f = 0; f < UR_MODULE_BUCKETS; f +=1) moduleBuckets[f] = NULL;
}


//==========================================================================
//
//  modulesResetSeen
//
//==========================================================================
static void modulesResetSeen (void) {
  for (ModuleInfo *c = modules; c; c = c->next) c->seen = 0;
}


//==========================================================================
//
//  moduleFind
//
//==========================================================================
static ModuleInfo *moduleFind (const char *name) {
  if (!name || !name[0]) return NULL;
  #if 0
  for (ModuleInfo *c = modules; c; c = c->next) if (!strcmp(c->name, name)) return c;
  #else
  const uint32_t hash = joaatHashStr(name);
  ModuleInfo *c = moduleBuckets[hash%UR_MODULE_BUCKETS];
  while (c != NULL) {
    if (c->hash == hash && strcmp(c->name, name) == 0) return c;
    c = c->hlink;
  }
  #endif
  return NULL;
}


//==========================================================================
//
//  moduleAdd
//
//  special module name: PLUS3BOOT
//
//==========================================================================
static ModuleInfo *moduleAdd (const char *name, const char *fname) {
  ModuleInfo *c;
  ur_assert(name && fname && name[0] && fname[0]);
  c = calloc(1, sizeof(ModuleInfo));
  ur_assert(c);
  c->name = strdup(name);
  ur_assert(c->name);
  c->fname = strdup(fname);
  ur_assert(c->fname);
  c->next = modules;
  modules = c;
  c->hash = joaatHashStr(name);
  const uint32_t bkt = c->hash%UR_MODULE_BUCKETS;
  c->hlink = moduleBuckets[bkt];
  moduleBuckets[bkt] = c;
  // +3DOS bootsector?
  if (strEquCI(c->name, "PLUS3BOOT")) {
    // save current memory state, because bootsector will be written as a separate independent file
    memcpy(mod_memory_saved, memory, sizeof(mod_memory_saved));
    memcpy(mod_memused_saved, memused, sizeof(mod_memused_saved));
    memcpy(mod_memresv_saved, memresv, sizeof(mod_memresv_saved));
  }
  return c;
}


//==========================================================================
//
//  moduleOpen
//
//==========================================================================
static void moduleOpen (void) {
  //fprintf(stderr, "::: OPENING MODULE <%s> :::\n", currModule->name);
  // unhide +3DOS boot labels
  if (strEquCI(currModule->name, "PLUS3BOOT")) {
    for (UrLabelInfo *lbl = labels; lbl; lbl = lbl->next) {
      if (lbl->modHidden == 1) {
        if (!lbl->savedFirstChar) fatal("internal module manager error");
        lbl->modHidden = 0;
        lbl->name[0] = lbl->savedFirstChar; // unhide it
        //fprintf(stderr, "***UN-HIDDEN: <%s>\n", lbl->name);
      }
    }
  }
}


//==========================================================================
//
//  isModuleLabel
//
//==========================================================================
static int isModuleLabel (const ModuleInfo *mi, const char *name) {
  if (!mi || !name || !name[0]) return 0;
  if (name[0] == '.') ++name;
  const size_t mnlen = strlen(mi->name);
  if (strlen(name) <= mnlen || name[mnlen] != '.') return 0;
  return (memcmp(name, mi->name, mnlen) == 0);
}


//==========================================================================
//
//  moduleClose
//
//  close current module
//
//==========================================================================
static void moduleClose (const char *mname) {
  if (mname && !mname[0]) mname = NULL;
  if (!currModule) {
    if (!mname) fatal("trying to close unopened module");
    fatal("trying to close unopened module '%s'", mname);
  }
  if (mname && strcmp(mname, currModule->name)) fatal("invalid module name in ENDMODULE; got '%s', expected '%s'", mname, currModule->name);

  // +3DOS bootsector?
  if (strEquCI(currModule->name, "PLUS3BOOT")) {
    // append bootsector chunk
    int start = 0, len;

    if (!findChunkFrom(start, &start, &len)) fatal("no +3DOS bootsector chunk");
    if (start != 0xFE10) fatal("+3DOS bootsector chunk must be 'org'ed and #FE10");
    if (len > 512-16) fatal("+3DOS bootsector chunk is too big");

    if (pass == 1) {
      OutDataFile *off = appendDataData(memory+(unsigned)start, (unsigned)start, (unsigned)len);
      if (!off) fatal("cannot append +3DOS bootsector");
      off->name = strdup("");
      off->type = 1; // special type

      start += len;
      if (findChunkFrom(start, &start, &len)) fatal("only +3DOS bootsector chunk is allowed (found #%04X, %d)", (unsigned)start, len);
    }

    // restore memory
    memcpy(memory, mod_memory_saved, sizeof(memory));
    memcpy(memused, mod_memused_saved, sizeof(memused));
    memcpy(memresv, mod_memresv_saved, sizeof(memresv));

    // remove module labels
    for (UrLabelInfo *lbl = labels; lbl; lbl = lbl->next) {
      if (!isModuleLabel(currModule, lbl->name)) continue;
      //fprintf(stderr, "***HIDDEN: <%s>\n", lbl->name);
      lbl->modHidden = 1;
      lbl->savedFirstChar = lbl->name[0];
      lbl->name[0] = '{'; // hide it
    }
  }

  currModule = NULL;
}


///////////////////////////////////////////////////////////////////////////////
// label getter and utilities
//
static char *lastSeenGlobalLabel = NULL; /* global; malloced */


static char *mangleLabelName (const char *name) {
  static char newname[MAX_LINE_SIZE*2+1024];
  if (!name || !name[0]) {
    newname[0] = '\0';
    return newname;
  }
  // local label or macro label?
  if (name[0] == '.') {
    // two dots is a macro label
    if (name[1] == '.') {
      if (!name[2]) fatal("invalid label '%s'", name);
      // this is macro label
      if (curmacro == NULL) fatal("macro label outside of macro: '%s'", name);
      snprintf(newname, sizeof(newname), ".%s.MACLBL%d%s", curmacro->mac->name, curmacronum, name+1);
      return newname;
    }
    if (!name[1]) fatal("invalid label '%s'", name);
    // simple local
    if (!lastSeenGlobalLabel) fatal("local label '%s' without a global", name);
    snprintf(newname, sizeof(newname), ".%s%s", lastSeenGlobalLabel, name);
    return newname;
  }
  // global label
  // if global is prefixed with '@', it should not be mangled
  // this is to allow access to other labels from modules
  if (name[0] == '@') {
    if (name[1] == '@') fatal("double-prefixed label '%s'", name);
    if (!name[1]) fatal("invalid label '%s'", name);
    snprintf(newname, sizeof(newname), "%s", name+1);
    return newname;
  }
  // if no module, nothing to do
  // also, do not mangle labels with dots inside:
  // those are either already mangled, or call to other modules
  if (!currModule || strchr(name, '.')) {
    snprintf(newname, sizeof(newname), "%s", name);
    return newname;
  }
  // this is global unqualified label and we have a module; let's rename it
  snprintf(newname, sizeof(newname), "%s.%s", currModule->name, name);
  return newname;
}


static UrLabelInfo *addTempLabel (void) {
  UrLabelInfo *lbl = tempLabels;
  if (lbl == NULL || !lbl->tempFwd) {
    if (pass == 0) {
      lbl = urAddTempLabel();
    } else {
      lbl = urNextTempLabel();
    }
    lbl->tempFwd = 1;
    if (!lbl->refFile) {
      lbl->refLine = currSrcLine->lineNo;
      lbl->refFile = strdup(currSrcLine->fname);
    }
  }
  return lbl;
}


static UrLabelInfo *addFwdTempLabel (void) {
  return addTempLabel();
}


// WARNING! this will be invalidated
static char lastFindLabelName[MAX_LINE_SIZE*2+1024];


static UrLabelInfo *findLabel (const char *name) {
  UrLabelInfo *lbl;
  if (!name || !name[0]) fatal("UrAsm internal error: empty name in `findLabel()`");
  // temp label?
  if (name[0] == '@') {
    if ((name[1] == '@' && name[2] == 'b' && name[3] == 0) ||
        (name[1] == 'b' && name[2] == 0))
    {
      // last defined temp
      lbl = tempLabels;
      while (lbl != NULL && lbl->tempFwd) lbl = lbl->prevTemp;
      if (lbl == NULL) fatal("no temp labels defined yet");
      return lbl;
    }
    if ((name[1] == '@' && name[2] == 'f' && name[3] == 0) ||
        (name[1] == 'f' && name[2] == 0))
    {
      // next defined temp
      return addFwdTempLabel();
    }
  }
  // try temp label
  if (name[0] == '@' && name[1] == '@' && name[2] == '_') {
    //fprintf(stderr, "TEMP: <%s>\n", name);
    lbl = urFindLabel(name);
    if (lbl) return lbl;
  }
  char *nn = mangleLabelName(name);
  strcpy(lastFindLabelName, nn);
  lbl = urFindLabel(nn);
  /*
  if (!lbl) {
    // try non-module label
    lbl = urFindLabel(ln);
  }
  */
  return lbl;
}


static UrLabelInfo *findAddLabel (const char *name) {
  if (!name || !name[0]) fatal("UrAsm internal error: empty name in `findAddLabel()`");
  char *nn = mangleLabelName(name);
  strcpy(lastFindLabelName, nn);
  UrLabelInfo *lbl = urAddLabel(nn);
  if (!lbl->refFile) {
    lbl->refLine = currSrcLine->lineNo;
    lbl->refFile = strdup(currSrcLine->fname);
  }
  return lbl;
}


static int lblOptMakeU2 = 0;

static int32_t findLabelCB (const char *name, uint16_t addr, int *defined, int *found, int *fixuptype) {
  // predefined
  if (strEquCI(name, "__URASM_DEFFMT_ANY_DISK")) {
    *found = 1;
    *defined = 1;
    *fixuptype = UR_FIXUP_NONE;
    return (optWriteType == 'S' || optWriteType == '3'); // SCL or +3DOS
  }
  if (strEquCI(name, "__URASM_DEFFMT_SCL")) {
    *found = 1;
    *defined = 1;
    *fixuptype = UR_FIXUP_NONE;
    return (optWriteType == 'S');
  }
  if (strEquCI(name, "__URASM_DEFFMT_SCL_MONO")) {
    *found = 1;
    *defined = 1;
    *fixuptype = UR_FIXUP_NONE;
    return (optWriteType == 'M');
  }
  if (strEquCI(name, "__URASM_DEFFMT_P3DOS")) {
    *found = 1;
    *defined = 1;
    *fixuptype = UR_FIXUP_NONE;
    return (optWriteType == '3');
  }
  if (strEquCI(name, "__URASM_DEFFMT_HOBETA")) {
    *found = 1;
    *defined = 1;
    *fixuptype = UR_FIXUP_NONE;
    return (optWriteType == 'H');
  }
  if (strEquCI(name, "__URASM_DEFFMT_TAPE")) {
    *found = 1;
    *defined = 1;
    *fixuptype = UR_FIXUP_NONE;
    return (optWriteType == 't');
  }
  if (strEquCI(name, "__URASM_DEFFMT_ANY_SNA")) {
    *found = 1;
    *defined = 1;
    *fixuptype = UR_FIXUP_NONE;
    return (optWriteType == 's');
  }
  if (strEquCI(name, "__URASM_DEFFMT_SNA48")) {
    *found = 1;
    *defined = 1;
    *fixuptype = UR_FIXUP_NONE;
    return (optWriteType == 's' && optSNA48);
  }
  if (strEquCI(name, "__URASM_DEFFMT_SNA128")) {
    *found = 1;
    *defined = 1;
    *fixuptype = UR_FIXUP_NONE;
    return (optWriteType == 's' && !optSNA48);
  }
  if (strEquCI(name, "__URASM_DEFFMT_RAW")) {
    *found = 1;
    *defined = 1;
    *fixuptype = UR_FIXUP_NONE;
    return (optWriteType == 'r');
  }
  if (strEquCI(name, "__URASM_DEFFMT_DMB")) {
    *found = 1;
    *defined = 1;
    *fixuptype = UR_FIXUP_NONE;
    return (optWriteType == 'd');
  }

  UrLabelInfo *lbl = findLabel(name);
  if (!lbl) {
    if (pass != 0) {
      //fprintf(stderr, "====\n"); for (UrLabelInfo *l = labels; l; l = l->next) fprintf(stderr, "  <%s>=%d (known=%d; type=%d)\n", l->name, l->value, l->known, l->type);
      errorMsg("using undefined label '%s'", name);
      *found = 0;
      *defined = 0;
      return 0;
    }
    lbl = urAddLabel(lastFindLabelName);
    lbl->type = (lblOptMakeU2 ? LBL_TYPE_VERY_SPECIAL : LBL_TYPE_UNKNOWN);
    lbl->known = 0;
    lbl->refLine = currSrcLine->lineNo;
    lbl->refFile = strdup(currSrcLine->fname);
    //fprintf(stderr, "new label: [%s]\n", lbl->name);
    //fprintf(stderr, "new label: [%s] (%d : #%04X); disp=#%04X; pc=#%04X\n", lbl->name, lbl->value, lbl->value&0xffffU, disp, pc);
  } else {
    //fprintf(stderr, "label reference: [%s] (%d : #%04X); disp=#%04X; pc=#%04X\n", lbl->name, lbl->value, lbl->value&0xffffU, disp, pc);
  }
  if (lbl) {
    *found = 1;
    *defined = (lbl->known != 0);
    *fixuptype = lbl->fixuptype;
    return lbl->value;
  }
  *found = 0;
  *defined = 0;
  return 0;
}


// qtypes
enum {
  UR_QTYPE_DEFINED,
  UR_QTYPE_KNOWN
};

static int isLabelDefinedOrKnown (const char *name, uint16_t addr, int qtype) {
  UrLabelInfo *lbl = findLabel(name);
  switch (qtype) {
    case UR_QTYPE_DEFINED: return (lbl ? lbl->known != 0 : 0);
    case UR_QTYPE_KNOWN: return (lbl != NULL);
    default: ;
  }
  return 0;
}


static void fixupOperandCB (const urasm_operand_t *op, uint16_t opdestaddr, uint16_t opaddr, int fixuptype, int size) {
  if (pass == 1) {
    //static const char *n[4] = {"none", "word", "low", "high"};
    //fprintf(stderr, "%d: fixupOperandCB: destaddr=#%04x; addr=#%04x; pc=#%04X; fixuptype=%s\n", size, opdestaddr, opaddr, pc, n[fixuptype]);
    addFixup(opdestaddr, opaddr, fixuptype, size);
  }
}


static int checkLabels (void) {
  int wasError = 0;
  for (UrLabelInfo *c = labels; c; c = c->next) {
    //fprintf(stderr, ":LBL: <%s> (type=%d)\n", c->name, c->type);
    if (c->type == LBL_TYPE_UNKNOWN) {
      fprintf(stderr, "ERROR at file %s, line %d: referencing undefined label: %s\n", c->refFile, c->refLine, c->name);
      wasError = 1;
    }
    if (c->type == LBL_TYPE_ASS) c->known = -1;
  }
  //if (wasError) longjmp(errJP, 667);
  return wasError;
}


///////////////////////////////////////////////////////////////////////////////
// expression utils (aka string parsing)
//

/* skip leading spaces */
/* returns string with spaces skipped */
UR_FORCE_INLINE char *strSkipSpaces (const char *s) {
  while (*s && isSpace(*s)) ++s;
  return (char *)s;
}


/* skip leading spaces */
/* returns string with spaces skipped */
UR_FORCE_INLINE const char *strSkipSpacesConst (const char *s) {
  while (*s && isSpace(*s)) ++s;
  return s;
}


/* remove trailing spaces from string */
static void strTrimRight (char *s) {
  if (!s || !s[0]) return;
  size_t len = strlen(s);
  while (len > 0 && isSpace(s[len-1])) --len;
  s[len] = 0;
}


/* skip leading spaces and colons */
/* returns string with spaces skipped */
UR_FORCE_INLINE char *strSkipSpacesColons (char *s) {
  while (*s && (isSpace(*s) || *s == ':')) ++s;
  return s;
}


/* remove leading spaces from the current line */
UR_FORCE_INLINE void removeSpaces (void) {
  char *e = strSkipSpaces(currLine);
  if (e != currLine) memmove(currLine, e, strlen(e)+1);
}


/* skip leading spaces, and argument (up to, but not including comma or colon) */
/* correctly skip strings */
/* returns string after skipped argument (with trailing spaces skipped) */
static char *skipMacroArg (char *str) {
  int parens = 0;
  char *strstart = str;
  for (;;) {
    str = strSkipSpaces(str);
    if (!str[0]) return str;
    if (parens == 0 && (str[0] == ',' || str[0] == ':')) return str;
    /* check for "af'" */
    if (str[0] == '\'' && str-strstart >= 2 && toLower(str[-2] == 'a') && toLower(str[-1] == 'f')) {
      ++str;
      continue;
    }
    if (str[0] == '(') { ++parens; str += 1; continue; }
    if (str[0] == ')') { --parens; if (parens < 0) parens = 0; str += 1; continue; }
    /* check for string */
    if (str[0] == '"' || str[0] == '\'') {
      const char qch = *str++;
      while (*str) {
        const char ch = *str++;
        if (ch == qch) break;
        if (ch == '\\' && *str) ++str;
      }
      continue;
    }
    ++str;
  }
}


/* evaluate next numeric expression in input string */
/* returns expression value */
static int32_t getExprArg (int *defined, int *addr) {
  int error = 0;
  char *a = strSkipSpaces(currLine);
  int32_t res;
  if (!a[0]) fatal("expression expected");
  const char *ee = urasm_expr(&res, a, disp, defined, addr, &error);
  if (error) fatalUrLib(error);
  if (*ee) {
    if (ee[0] != ',' && ee[0] != ':') fatal("bad expression");
    memmove(currLine, ee, strlen(ee)+1);
  } else {
    currLine[0] = '\0';
  }
  return res;
}


/* evaluate next string expression in input string */
/* returns expression value */
static char *getStrExprArgFmt (void) {
  int error = 0;
  int donteval = 0, defined = 0;
  static char resbuf[256];
  char *a = strSkipSpaces(currLine);
  if (!a[0]) fatal("expression expected");
  urasm_exprval_t res;
  urasm_exprval_init(&res);
  const char *ee = urasm_expr_ex(&res, a, disp, &donteval, &defined, &error);
  if (error) fatalUrLib(error);
  if (*ee) {
    if (ee[0] != ',' && ee[0] != ':') fatal("bad expression");
    memmove(currLine, ee, strlen(ee)+1);
  } else {
    currLine[0] = '\0';
  }
  if (res.str) {
    snprintf(resbuf, sizeof(resbuf), "%s", res.str);
  } else {
    snprintf(resbuf, sizeof(resbuf), "%d", res.val);
  }
  urasm_exprval_clear(&res);
  return resbuf;
}


/* evaluate next numeric expression in input string */
/* there shoild be no other expressions in the string */
/* returns expression value */
static int32_t getOneExprArg (int *defined, int *addr) {
  int32_t res = getExprArg(defined, addr);
  if (currLine[0] && currLine[0] != ':') fatal("too many expressions");
  return res;
}


/* is next expression a string literal? */
UR_FORCE_INLINE int isStrArg (void) {
  const char *s = strSkipSpaces(currLine);
  return (s[0] == '"' || s[0] == '\'');
}


/* check of we reached end of operator */
UR_FORCE_INLINE int isOperatorEnd (void) {
  const char *s = strSkipSpaces(currLine);
  return (s[0] == 0 || s[0] == ':');
}


/* check of we reached end of operator */
UR_FORCE_INLINE int isLineEnd (void) {
  const char *s = strSkipSpaces(currLine);
  return (s[0] == 0 || s[0] == ';');
}


/* parse string argument from input string */
/* returns parsed string */
static char *getStrArg (int *lenp) {
  char *res, qCh;
  char *a = strSkipSpaces(currLine);
  qCh = *a++;
  if (qCh != '"' && qCh != '\'') fatal("string expected");
  res = parseStr(&a, qCh, lenp);
  if (*a) {
    if (a[0] != ',' && a[0] != ':') fatal("bad string expression");
    memmove(currLine, a, strlen(a)+1);
  } else {
    currLine[0] = '\0';
  }
  return res;
}


/* parse string argument from input string; allows math after the string */
/* returns parsed string */
/* returns NULL and math expr in `lenp` */
static char *getStrExprArg (int *lenp, int isWord) {
  char *a = strSkipSpaces(currLine);
  const char qCh = *a++;
  if (qCh != '"' && qCh != '\'') fatal("string expected");
  char *res = parseStr(&a, qCh, lenp);
  if (!res[0]) fatal("invalid empty string");
  if (*a) {
    if (a[0] != ',' && a[0] != ':' && strlen(res) <= 2) {
      memmove(currLine, a, strlen(a)+1);
      // "ab" -- 'a' will be in low (first) byte
      // 'ab' -- 'a' will be in high (second) byte
      int32_t sval;
      if (res[1]) {
        // two-byte
        if (qCh == '"') {
          sval = (uint8_t)(res[0]&0xffU)|(((uint8_t)(res[1]&0xffU))<<8);
        } else {
          sval = (uint8_t)(res[1]&0xffU)|(((uint8_t)(res[0]&0xffU))<<8);
        }
      } else {
        // one-byte
        sval = (uint8_t)(res[0]&0xffU);
      }
      //fprintf(stderr, "SMATH:000: str=<%s> (sval=%d); <%s>\n", res, sval, currLine);
      // parse math
      int defined = 0, fixuptype = UR_FIXUP_NONE;
      int32_t xadd = getExprArg(&defined, &fixuptype);
      //fprintf(stderr, "SMATH:001: str=<%s> (sval=%d; xadd=%d); <%s>\n", res, sval, xadd, currLine);
      if (pass > 0 && !defined) fatal("undefined operand");
      sval += xadd;
      if (currLine[0] && currLine[0] != ',' && currLine[0] != ':') fatal("bad string expression");
      res = NULL;
      if (lenp) *lenp = sval;
    } else {
      if (a[0] != ',' && a[0] != ':') fatal("bad string expression");
      memmove(currLine, a, strlen(a)+1);
    }
  } else {
    currLine[0] = '\0';
  }
  return res;
}


/* get identifier (and lowercase it) */
static char *getOneIdArgLo (void) {
  static char res[MAX_LINE_SIZE+128];
  char *p;
  char *a = strSkipSpaces(currLine);
  memset(res, 0, sizeof(res));
  if (!a[0]) fatal("identifier expected");
  for (p = res; *a && *a != ',' && *a != ':' && *a != '=' && !isSpace(*a); ++a, ++p) *p = *a;
  for (; p > res && isSpace(p[-1]); --p) {}
  *p = '\0';
  if (p-res > 120) fatal("identifier too long: %s", res);
  while (*a && isSpace(*a)) ++a;
  if (*a) {
    memmove(currLine, a, strlen(a)+1);
    if (currLine[0] == ';') currLine[0] = 0;
    if (currLine[0]) fatal("extra arguments");
  } else {
    currLine[0] = '\0';
  }
  if (!res[0]) fatal("identifier expected");
  for (char *t = res; *t; ++t) *t = toLower(*t);
  return res;
}


/* get label argument */
static char *getLabelArg (int checkdelim) {
  static char res[MAX_LINE_SIZE+128];
  char *p;
  char *a = strSkipSpaces(currLine);
  memset(res, 0, sizeof(res));
  if (!a[0]) fatal("label expected");
  for (p = res; *a && *a != ',' && *a != ':' && *a != '=' && !isSpace(*a); ++a, ++p) *p = *a;
  for (; p > res && isSpace(p[-1]); --p) {}
  *p = '\0';
  if (p-res > 120) fatal("label name too long: %s", res);
  while (*a && isSpace(*a)) ++a;
  if (*a) {
    if (checkdelim && a[0] != ',' && a[0] != ':') fatal("bad label expression");
    memmove(currLine, a, strlen(a)+1);
  } else {
    currLine[0] = '\0';
  }
  if (!res[0]) fatal("label expected");
  return res;
}


/* get label argument, and ensure that it is the last one */
static char *getOneLabelArg (void) {
  char *res = getLabelArg(1);
  if (currLine[0] && currLine[0] != ':') fatal("too many expressions");
  return res;
}


/* returns ',' or 0 */
static char eatComma (void) {
  char *a = strSkipSpaces(currLine);
  if (!a[0]) { currLine[0] = '\0'; return 0; }
  if (a[0] == ':') return 0;
  if (a[0] != ',') fatal("invalid expression: ',' expected");
  for (++a; *a && isSpace(*a); ++a) {}
  if (!a[0]) { currLine[0] = '\0'; return 0; }
  memmove(currLine, a, strlen(a)+1);
  return ',';
}


static int checkDelim (char dm) {
  char *a = strSkipSpaces(currLine);
  if (a[0] != dm) return 0;
  for (++a; *a && isSpace(*a); ++a) {}
  if (!a[0]) { currLine[0] = '\0'; return 1; }
  memmove(currLine, a, strlen(a)+1);
  return 1;
}


static void matchDelim (char dm) {
  if (!checkDelim(dm)) fatal("invalid expression: '%c' expected", dm);
}


static int checkIdentCI (const char *ident) {
  ur_assert(ident && ident[0]);
  char *a = strSkipSpaces(currLine);
  while (*a && *ident) {
    char c0 = *a++; if (c0 >= 'A' && c0 <= 'Z') c0 = c0 - 'A' + 'a';
    char c1 = *ident++; if (c1 >= 'A' && c1 <= 'Z') c1 = c1 - 'A' + 'a';
    if (c0 != c1) return 0;
  }
  if (ident[0] != 0) return 0;
  if (*a == '_' || isAlphaDigit(*a)) return 0;
  while (*a && isSpace(*a)) a += 1;
  if (*a) {
    memmove(currLine, a, strlen(a)+1);
  } else {
    currLine[0] = '\0';
  }
  return 1;
}


///////////////////////////////////////////////////////////////////////////////
// label processor
//
static MAYBE_UNUSED void removeSpacesAndColons (void) {
  char *ep = strSkipSpacesColons(currLine);
  memmove(currLine, ep, strlen(ep)+1);
}


static void checkExprEnd (void) {
  char *ep = strSkipSpaces(currLine);
  memmove(currLine, ep, strlen(ep)+1);
  if (currLine[0] && currLine[0] != ':') fatal("end of expression expected");
}


static void checkOperatorEnd (void) {
  char *ep = strSkipSpaces(currLine);
  memmove(currLine, ep, strlen(ep)+1);
  if (currLine[0]) fatal("end of operator expected");
}


/* remove label from curLine */
static void removeLabel (void) {
  char *ep = currLine;
  if (ep[0] && !isSpace(ep[0])) for (ep = currLine; *ep && !isSpace(*ep) && *ep != ':'; ++ep) {} // skip text
  // skip spaces and colons
  ep = strSkipSpacesColons(ep);
  memmove(currLine, ep, strlen(ep)+1);
}


static int labelDoEQU (const char *lblname, const char *value) {
  static char n2[256];
  UrLabelInfo *lbl;
  //
  if (value == NULL || lblname == NULL || !lblname[0] || strlen(lblname) > 255 || strlen(value) >= MAX_LINE_SIZE) return -1;
  memset(n2, 0, sizeof(n2));
  strcpy(n2, lblname);
  if (!urasm_is_valid_name(n2)) return -1; // this is not a valid label, get out of here
  // check if this can be an instruction
  lbl = urAddLabel(lblname);
  if (!lbl->refFile) {
    lbl->refLine = 0;
    lbl->refFile = strdup("artificially-defined-label");
  }
  //fprintf(stderr, "labelDoEQU: <%s>=<%s>\n", lblname, value);
  strcpy(currLine, value);
  {
    int defined = 1, addr = UR_FIXUP_NONE;
    int32_t res = getOneExprArg(&defined, &addr);
    lbl->type = LBL_TYPE_EQU;
    if (addr != UR_FIXUP_NONE) lbl->fixuptype = addr;
    if (defined) {
      lbl->value = res;
      lbl->known = 1;
    } else {
      return -1; //fatal("can't calculate label %s", lbl->name);
    }
  }
  return 0;
}


static void processLabel (void) {
  char *argstart;
  char *ep, *nn;
  static char n2[256];
  UrLabelInfo *lbl;
  int noLocAff = 0, doEQU = 0, isTemp = 0;
  //fprintf(stderr, "LINE %5d: <%s> from <%s>\n", (currSrcLine ? currSrcLine->lineNo : 0), currLine, (currSrcLine ? currSrcLine->fname : "<nowhere>"));
  memset(n2, 0, sizeof(n2));

  currSeenLabel[0] = 0;

  if (!currLine[0] || isSpace(currLine[0]) || currLine[0] == ':') {
    // this may be " id = smth" or " id equ smth"
    ep = currLine;
    // skip spaces
    while (isSpace(*ep)) ++ep;
    if (ep[0] == ':' || !ep[0] || (!isAlpha(ep[0]) && ep[0] != '_' && ep[0] != '.' && ep[0] != '@')) {
      removeLabel(); // removeLabel() removes any spaces, etc
      return;
    }
    // this looks like a label; check for '=' or 'equ'
    while (isAlphaDigit(ep[0]) || ep[0] == '_' || ep[0] == '.' || ep[0] == '@') ++ep;
    nn = ep;
    // skip trailing spaces
    while (isSpace(*ep)) ++ep;
    if (ep[0] == '=') {
      doEQU = 0;
      argstart = ++ep;
    } else if (isSpace(*nn)) {
      doEQU = 1;
      argstart = strIsCommand("EQU", ep);
    } else {
      argstart = NULL;
    }
    if (!argstart) {
      removeLabel(); // removeLabel() removes any spaces, etc
      return;
    }
    // remove leading spaces from name
    // copy label
    ep = currLine;
    // skip spaces
    while (isSpace(*ep)) ++ep;
    if (ep >= nn) fatal("internal compiler error");
    if (nn-ep > 120) fatal("label too long");
    memset(n2, 0, sizeof(n2));
    memmove(n2, ep, nn-ep);
    if (urFindOp(n2) || !urasm_is_valid_name(n2)) {
      //fatal("invalid label name");
      removeLabel(); // removeLabel() removes any spaces, etc
      return;
    }
    // remove label name
    memmove(currLine, argstart, strlen(argstart)+1);
    // append label
    if (n2[0] == '@' && n2[1] == '@' && !n2[2]) {
      // unnamed back/forward
      fatal("temporary label should end with ':'");
      /*
      noLocAff = 1;
      lbl = addTempLabel();
      */
    } else {
      if (n2[0] == '@' && n2[1]) { noLocAff = 1; memmove(n2, n2+1, strlen(n2)); }
      lbl = findAddLabel(n2);
    }
    //fprintf(stderr, "**: LBL: <%s> (doEQU=%d)\n", lbl->name, doEQU);
    if (doEQU && pass == 0 && lbl->type != LBL_TYPE_UNKNOWN) {
      fatal("duplicate label '%s'", lbl->name);
    }
    int defined = 1, addr = UR_FIXUP_NONE;
    int32_t res = getOneExprArg(&defined, &addr);
    lbl->type = (doEQU ? LBL_TYPE_EQU : LBL_TYPE_ASS);
    if (addr != UR_FIXUP_NONE) lbl->fixuptype = addr;
    if (defined) {
      lbl->value = res;
      lbl->known = 1;
    } else {
      if (pass != 0) fatal("can't calculate label '%s'", lbl->name);
    }
    currLine[0] = '\0';
    return;
  }

  // collect label
  for (ep = currLine; *ep && !isSpace(*ep) && *ep != ':'; ++ep) {}
  if (ep-currLine > 120) fatal("label too long");

  // copy label
  memset(n2, 0, sizeof(n2));
  memmove(n2, currLine, ep-currLine);
  if (urFindOp(n2)) {
    ep = strSkipSpaces(ep);
    if (*ep != ':') return; // this must be an instruction, process it
  }
  if (!urasm_is_valid_name(n2)) return; // this is not a valid label, get out of here

  // check for macro
  if (findMacro(n2)) { /*fprintf(stderr, "***M:<%s>\n", n2);*/ return; } // macro call

  // check if this can be instruction
  //ep = strSkipSpaces(ep);
  //if (*ep != ':' && urFindOp(n2)) return; // this must be and instruction, process it
  // ok, we got a good label
  removeLabel();
  if (n2[0] == '@' && n2[1] == '@' && !n2[2]) {
    // unnamed back/forward
    noLocAff = 1; isTemp = 1;
    lbl = addTempLabel();
  } else {
    if (n2[0] == '@' && n2[1]) { noLocAff = 1; memmove(n2, n2+1, strlen(n2)); }
    /*!fprintf(stderr, "GOT LABEL <%s> (%d)\n", n2, (currSrcLine ? currSrcLine->lineNo : 0));*/
    lbl = findAddLabel(n2);
    //printf("new: [%s]\n", lbl->name);
  }

  // get command name
  if (currLine[0] == '=') {
    if (isTemp) fatal("cannot assign values to temp labels");
    doEQU = 0;
    argstart = currLine+1;
  } else {
    doEQU = 1;
    argstart = strIsCommand("EQU", currLine);
  }
  if (!argstart || doEQU) {
    if (pass == 0 && !isTemp && lbl->type != LBL_TYPE_UNKNOWN) {
      fatal("duplicate label '%s'", lbl->name);
    }
  }

  //fprintf(stderr, "**: LBL: <%s> (doEQU=%d)\n", lbl->name, doEQU);
  if (argstart) {
    if (isTemp) fatal("cannot assign values to temp labels");
    // do '=' or 'EQU'
    memmove(currLine, argstart, strlen(argstart)+1);
    if (!doEQU) {
      if (lbl->type != LBL_TYPE_UNKNOWN && lbl->type != LBL_TYPE_ASS) {
        fatal("duplicate label '%s'", lbl->name);
      }
    }
    int defined = 1, addr = UR_FIXUP_NONE;
    int32_t res = getOneExprArg(&defined, &addr);
    lbl->type = (doEQU ? LBL_TYPE_EQU : LBL_TYPE_ASS);
    if (addr != UR_FIXUP_NONE) lbl->fixuptype = addr;
    if (defined) {
      lbl->value = res;
      lbl->known = 1;
    } else {
      if (pass != 0) fatal("can't calculate label '%s'", lbl->name);
    }
    currLine[0] = '\0';
    return;
  }

  //fprintf(stderr, "**: LBL: <%s> (doEQU=%d); disp=#%04X; pc=#%04X\n", lbl->name, doEQU, disp, pc);
  // code label
  // update last seen global
  if (!isTemp) {
    if (lbl->name[0] != '{' && lbl->name[0] != '.' && !noLocAff) {
      //fprintf(stderr, "**: LASTGLOB: <%s>\n", lbl->name);
      if (lastSeenGlobalLabel) free(lastSeenGlobalLabel);
      lastSeenGlobalLabel = strdup(lbl->name);
    }
    lbl->type = LBL_TYPE_CODE;
    lbl->value = disp;
    lbl->known = 1;
    lbl->fixuptype = UR_FIXUP_WORD;

    snprintf(currSeenLabel, sizeof(currSeenLabel), "%s", n2);
  } else if (lbl->type != LBL_TYPE_UNKNOWN) {
    ur_assert(lbl->type == LBL_TYPE_CODE);
    ur_assert(lbl->value == disp);
    lbl->tempFwd = 0;
  } else {
    //fprintf(stderr, "FIXTEMP: %s\n", lbl->name);
    lbl->type = LBL_TYPE_CODE;
    lbl->value = disp;
    lbl->known = 1;
    lbl->fixuptype = UR_FIXUP_WORD;
    ur_assert(lbl->tempIdx != 0);
    lbl->tempFwd = 0;
  }
}


///////////////////////////////////////////////////////////////////////////////
// instruction finder (in source)
//
/* array ends with NULL */
/* returns line or NULL */
/* iidx will be set to found instruction number */
static __attribute__((sentinel)) SourceLine *findNextInstructionFromList (int *iidx, ...) {
  if (iidx) *iidx = -1;
  for (SourceLine *cur = currSrcLine; cur; cur = cur->next) {
    va_list ap;
    //if (!isSpace(cur->line[0])) continue; // fuck labeled strings
    va_start(ap, iidx);
    for (int f = 0; ;++f) {
      const char *name = va_arg(ap, const char *);
      //
      if (!name) break;
      if (strIsCommand(name, cur->line)) {
        va_end(ap);
        if (iidx) *iidx = f;
        return cur;
      }
    }
    va_end(ap);
  }
  return NULL;
}


static MAYBE_UNUSED SourceLine *findNextInstruction (const char *name) {
  return findNextInstructionFromList(NULL, name, NULL);
}


///////////////////////////////////////////////////////////////////////////////
// writers
//
///////////////////////////////////////////////////////////////////////////////
static void writeFixups (void) {
  void writeFixupList (FILE *fo, int cnt, const char *lbl, int (*chk)(const FixupItem *)) {
    if (cnt > 0) {
      int prevaddr = 0;
      fprintf(fo, "%s:\n", lbl);
      if (optFixupType == 1) fprintf(fo, "  dw %d ; count\n", cnt);
      for (const FixupItem *fx = fixlisthead; fx != NULL; fx = fx->next) {
        if (chk(fx)) {
          fprintf(fo, "  dw #%04X\n", fx->opaddr-prevaddr);
          if (optFixupType == 2) {
            prevaddr = fx->opaddr;
          }
        }
      }
      if (optFixupType == 2 || optFixupType == 3) fprintf(fo, "  dw 0 ; end marker\n");
    }
  }

  if (optFixupType == 0) {
    /* simple text file */
    char *fname = strprintf("%s/%s", optOutputDir, "zfixuptable.txt");
    printf("writing fixups to '%s'...\n", fname);
    FILE *fo = fopen(fname, "w");
    free(fname);
    if (fo == NULL) fatal("can't write fixup file");
    fprintf(fo, "; addr dadr sz\n");
    for (const FixupItem *fx = fixlisthead; fx != NULL; fx = fx->next) {
      static const char type[4] = "NWLH";
      fprintf(fo, "%c #%04x #%04x %d\n", type[fx->fixuptype], fx->opaddr, fx->opdestaddr, fx->size);
    }
    fclose(fo);
  } else {
    /* various asm formats */
    char *fname = strprintf("%s/%s", optOutputDir, "zfixuptable.zas");
    printf("writing fixups to '%s'...\n", fname);
    FILE *fo = fopen(fname, "w");
    int cntw = 0, cntwl = 0, cntwh = 0, cntbl = 0, cntbh = 0;
    free(fname);
    if (fo == NULL) fatal("can't write fixup file");
    for (const FixupItem *fx = fixlisthead; fx != NULL; fx = fx->next) {
      if (fx->fixuptype == UR_FIXUP_WORD) { ++cntw; continue; }
      switch (fx->fixuptype) {
        case UR_FIXUP_LOBYTE: if (fx->size == 2) ++cntwl; else ++cntbl; break;
        case UR_FIXUP_HIBYTE: if (fx->size == 2) ++cntwh; else ++cntbh; break;
      }
    }
    writeFixupList(fo, cntw, "fixup_table_w", lambda(int, (const FixupItem *fx) {
      return (fx->fixuptype == UR_FIXUP_WORD);
    }));
    writeFixupList(fo, cntwl, "fixup_table_wl", lambda(int, (const FixupItem *fx) {
      return (fx->fixuptype == UR_FIXUP_LOBYTE && fx->size == 2);
    }));
    writeFixupList(fo, cntwh, "fixup_table_wh", lambda(int, (const FixupItem *fx) {
      return (fx->fixuptype == UR_FIXUP_HIBYTE && fx->size == 2);
    }));
    writeFixupList(fo, cntbl, "fixup_table_bl", lambda(int, (const FixupItem *fx) {
      return (fx->fixuptype == UR_FIXUP_LOBYTE && fx->size == 1);
    }));
    writeFixupList(fo, cntbh, "fixup_table_bh", lambda(int, (const FixupItem *fx) {
      return (fx->fixuptype == UR_FIXUP_HIBYTE && fx->size == 1);
    }));
    fclose(fo);
  }
}


///////////////////////////////////////////////////////////////////////////////
/* return 'found' flag */
static int findChunkFrom (int addr, int *start, int *len) {
  if (addr < 0) addr = 0;
  for (; addr <= 65535 && !memused[addr]; ++addr) {}
  if (addr > 65535) return 0;
  *start = addr;
  for (; addr <= 65535 && memused[addr]; ++addr) {}
  *len = addr-(*start);
  return 1;
}


static void rleUnpack (uint16_t addr, const uint8_t *buf, int buflen) {
  for (; buflen >= 2; buflen -= 2) {
    int cnt = *buf++;
    uint8_t byte = *buf++;
    //
    if (cnt == 0) {
      // copy
      for (cnt = byte; cnt >= 0; --cnt, ++addr, --buflen, ++buf) {
        if (!memused[addr]) putByte(addr, *buf);
        if (addr == 0xffff) return;
      }
    } else {
      // fill
      for (; cnt > 0; --cnt, ++addr) {
        if (!memused[addr]) putByte(addr, byte);
        if (addr == 0xffff) return;
      }
    }
  }
}


static int fWriteByte (FILE *fo, uint8_t b, uint8_t *bxor) {
  if (fwrite(&b, 1, 1, fo) != 1) return -1;
  if (bxor) *bxor = (*bxor)^b;
  return 0;
}


static int fWriteWord (FILE *fo, uint16_t w, uint8_t *bxor) {
  if (fWriteByte(fo, w&0xFFU, bxor)) return -1;
  if (fWriteByte(fo, (w>>8)&0xFFU, bxor)) return -1;
  return 0;
}


///////////////////////////////////////////////////////////////////////////////
// .sna
//
static int saveSna (const char *fname, int as48) {
  char *fn;// = malloc(strlen(fname)+16);
  uint8_t regs[27];
  FILE *fo;
  char abuf[32];
  //
  //fn = strprintf("%s/%s.sna", optOutputDir, fname);
  fn = buildOutputFileName(fname, "sna");
  fo = fopen(fn, "wb");
  free(fn);
  if (!fo) { fprintf(stderr, "ERROR: can't write SNA file: %s.sna\n", fname); return -1; }
  printf("out: %s.sna\n", fname);
  memmove(regs, ursna48, 27);
#if 0
  if (optRunSNA) {
    /* push new address */
    uint16_t sp = (uint16_t)regs[23]+(((uint16_t)regs[24])<<8);
    sp -= 2;
    putByte(sp, ent&0xFFU);
    putByte(sp+1, (ent>>8)&0xFFU);
    regs[23] = sp&0xFFU;
    regs[24] = (sp>>8)&0xFFU;
  }
  fwrite(regs, 27, 1, fo);
  rleUnpack(16384, ursna48+27, sizeof(ursna48)-27);
  fwrite(memory+16384, 49152, 1, fo);
#endif
  //
  uint16_t sp = (uint16_t)regs[23]+(((uint16_t)regs[24])<<8);
  //
  //fprintf(stderr, "SP: #%04X #%02X%02X\n", sp, memory[sp+1], memory[sp]);
  //fprintf(stderr, "%d\n", memused[sp]);
  if (memused[sp] || memused[(sp+1)&0xffff]) {
    fprintf(stderr, "FATAL: can't save snapshot!\n");
    goto error;
  }
  //
  if (fwrite(ursna48, 27, 1, fo) != 1) goto error;
  rleUnpack(16384, (const uint8_t *)ursna48+27, sizeof(ursna48)-27);
  //for (int f = 0; f < 49152; ++f) if (!memused[f+16384]) memory[f+16384] = ursna48[f+27];
  //
  sprintf(abuf, "%05d", clrAddr);
  memcpy(memory+23762, abuf, 5);
  sprintf(abuf, "%05d", ent);
  memcpy(memory+23773, abuf, 5);
  //
  if (fwrite(memory+16384, 49152, 1, fo) != 1) goto error;
  //
  if (!as48) {
    int addr = memory[sp]+256*memory[(sp+1)&0xffff];
    //
    if (fWriteWord(fo, addr, NULL) != 0) goto error; // PC
    if (fWriteByte(fo, 0x10, NULL) != 0) goto error; // 7FFD
    if (fWriteByte(fo, 0, NULL) != 0) goto error; // TR-DOS inactive
    // write pages
    memset(memory, 0, 16384);
    for (int f = 1; f < 8; ++f) {
      if (f != 2 && f != 5) {
        if (fwrite(memory, 16384, 1, fo) != 1) goto error;
      }
    }
  }
  //
  fclose(fo);
  return 0;
error:
  fclose(fo);
  unlink(fname);
  return -1;
}


///////////////////////////////////////////////////////////////////////////////
// bin chunks
//
static void saveRaw (const char *basename) {
  char *fname = NULL;// = malloc(strlen(basename)+16);
  int start = 0, len;
  FILE *fo;
  //
  while (findChunkFrom(start, &start, &len)) {
    if (fname != NULL) free(fname);
    fname = strprintf("%s/%s_%04X.%s", optOutputDir, basename, start, (optTapExt ? "tap" : "bin"));
    fo = fopen(fname, "wb");
    if (!fo) {
      fprintf(stderr, "ERROR: can't write file %s!\n", fname);
    } else {
      printf("out: %s\n", fname);
      if (fwrite(memory+start, len, 1, fo) != 1) {
        fprintf(stderr, "ERROR: error writing file %s!\n", fname);
        fclose(fo);
        unlink(fname);
      } else {
        fclose(fo);
      }
    }
    start += len;
  }
  if (fname != NULL) free(fname);
}


///////////////////////////////////////////////////////////////////////////////
// HoBeta files
//
typedef struct __attribute__((packed)) __attribute__((gcc_struct)) {
  char name[8];
  char type;
  uint16_t start;
  uint16_t len;
  uint8_t zero; // always zero
  uint8_t secLen; // length in sectors
  uint16_t checksum;
} HOBHeader;


static uint16_t calcHobSum (const void *hdr) {
  const uint8_t *buf = (const uint8_t *)hdr;
  uint16_t res = 0;
  //
  for (unsigned int f = 0; f < 15; ++f) res += ((uint16_t)buf[f])*257+f;
  return res;
}


static void saveHob (const char *basename) {
  char *fname = NULL;//malloc(strlen(basename)+16);
  int start = 0, len;
  HOBHeader hdr;
  FILE *fo;
  //
  while (findChunkFrom(start, &start, &len)) {
    if (fname != NULL) free(fname);
    fname = strprintf("%s/%s_%04X.%s", optOutputDir, basename, start, "$C");
    fo = fopen(fname, "wb");
    if (!fo) {
      fprintf(stderr, "ERROR: can't write file %s!\n", fname);
      start += len;
    } else {
      char tmpbuf[sizeof(hdr.name)*2];
      printf("out: %s\n", fname);
      memset(&hdr, 0, sizeof(hdr));
      memset(&hdr.name, 32, 8);
      snprintf(tmpbuf, sizeof(tmpbuf), "%c%04X", basename[0], start);
      while (strlen(tmpbuf) < sizeof(hdr.name)) strcat(tmpbuf, " "); /* sorry! */
      memcpy(hdr.name, tmpbuf, sizeof(hdr.name));
      hdr.type = 'C';
      hdr.start = start;
      hdr.len = len;
      hdr.secLen = (len+255)/256;
      hdr.checksum = calcHobSum(&hdr);
      if (fwrite(&hdr, sizeof(hdr), 1, fo) != 1) {
        fprintf(stderr, "ERROR: error writing file %s!\n", fname);
        fclose(fo);
        unlink(fname);
        goto quit;
      }
      if (fwrite(memory+start, len, 1, fo) != 1) {
        fprintf(stderr, "ERROR: error writing file %s!\n", fname);
        fclose(fo);
        unlink(fname);
        goto quit;
      }
      start += len;
      while (len%256) {
        if (fwrite(&hdr.zero, 1, 1, fo) != 1) {
          fprintf(stderr, "ERROR: error writing file %s!\n", fname);
          fclose(fo);
          unlink(fname);
          goto quit;
        }
        ++len;
        //fprintf(stderr, ":%d\n", len%256);
      }
      fclose(fo);
    }
  }
quit:
  if (fname != NULL) free(fname);
}


// ////////////////////////////////////////////////////////////////////////// //
typedef struct {
  uint8_t fcount;
  uint8_t dir[14*256];
  uint32_t dirpos;
  uint32_t currfstartpos;
  uint8_t *data;
  size_t datapos;
  size_t datasize;
} SCLFile;


static const uint8_t monoldr_code[169] = {
  0x00,0x00,0xff,0xf0,0xff,0xf0,0xff,0xf0,0xff,0xf3,0x01,0xfd,0x7f,0x3e,0x10,0xed,
  0x79,0xaf,0x21,0x00,0x58,0x11,0x01,0x58,0x01,0xff,0x02,0xfb,0x76,0xf3,0xd3,0xfe,
  0x36,0x00,0xed,0xb0,0x21,0x00,0x40,0x11,0x01,0x40,0x01,0x00,0x18,0x77,0xed,0xb0,
  0xfb,0x76,0xf3,0x3b,0x3b,0xe1,0xe5,0x11,0xce,0xff,0x19,0x7e,0x23,0x5e,0x23,0x56,
  0x23,0xc1,0xe5,0xd5,0x69,0x60,0x01,0x35,0x00,0x09,0x4f,0x87,0x81,0x01,0x43,0x00,
  0x81,0x4f,0x3e,0x00,0x88,0x47,0xed,0xb0,0xd1,0xe1,0xd5,0x01,0xff,0x03,0x13,0xed,
  0xa0,0xed,0xa0,0x13,0x10,0xf8,0xc9,0x31,0xa5,0xa5,0xfb,0x21,0x5a,0x5a,0xe5,0x21,
  0x9a,0x02,0xe5,0x76,0x3b,0x3b,0xe1,0x01,0x35,0x00,0x09,0x0e,0x00,0x7e,0xb7,0x28,
  0x23,0x23,0x5e,0x23,0x56,0x23,0xe5,0xeb,0x06,0x08,0xb8,0x30,0x01,0x47,0x90,0xf5,
  0xc5,0xe5,0xed,0x5b,0xf4,0x5c,0x0e,0x05,0xcd,0x13,0x3d,0xe1,0xc1,0x09,0xf1,0x20,
  0xe7,0xe1,0x18,0xd9,0xe1,0xd1,0xf9,0xeb,0xe9,
};


static void sclInit (SCLFile *scl) {
  memset(scl, 0, sizeof(*scl));
  scl->datasize = 2*80*16*256; /* maximum disk size */
  scl->data = malloc(scl->datasize);
  memset(scl->data, 0, scl->datasize);
  scl->currfstartpos = 0xffffffffu;
}


static void sclClear (SCLFile *scl) {
  if (!scl) return;
  if (scl->data) free(scl->data);
  memset(scl, 0, sizeof(*scl));
  scl->currfstartpos = 0xffffffffu;
}


static void sclStartFile (SCLFile *scl, const char *name, char type, uint16_t v0, uint16_t v1) {
  if (scl->fcount == 255) {
    fprintf(stderr, "FATAL: too many files in SCL!\n");
    exit(1);
  }
  if (scl->currfstartpos != 0xffffffffu) {
    fprintf(stderr, "FATAL: last SCL file is not finished!\n");
    exit(1);
  }
  int nlen = 0;
  while (nlen < 8 && name && name[nlen]) scl->dir[scl->dirpos++] = name[nlen++];
  while (nlen < 8) { scl->dir[scl->dirpos++] = ' '; ++nlen; }
  scl->dir[scl->dirpos++] = type;
  scl->dir[scl->dirpos++] = v0&0xff;
  scl->dir[scl->dirpos++] = (v0>>8)&0xff;
  scl->dir[scl->dirpos++] = v1&0xff;
  scl->dir[scl->dirpos++] = (v1>>8)&0xff;
  //scl->dir[scl->dirpos++] = 0; /* size in sectors, unknown yet */
  scl->currfstartpos = scl->datapos;
}


static void sclEndFile (SCLFile *scl) {
  if (scl->currfstartpos == 0xffffffffu) {
    fprintf(stderr, "FATAL: last SCL file already finished!\n");
    exit(1);
  }
  /* align to sector */
  while (scl->datapos%256) scl->data[scl->datapos++] = 0;
  const uint32_t fsz = scl->datapos-scl->currfstartpos;
  if (fsz > 255*256) {
    fprintf(stderr, "FATAL: SCL file too big!\n");
    exit(1);
  }
  ur_assert((fsz&255) == 0);
  scl->dir[scl->dirpos++] = fsz/256; /* size in sectors */
  ur_assert(scl->dirpos%14 == 0);
  ++scl->fcount;
  scl->currfstartpos = 0xffffffffu;
}


static void sclWriteDataForce (SCLFile *scl, const void *buf, size_t len) {
  if (!len) return;
  if (len > 255*256) {
    fprintf(stderr, "FATAL: SCL file too big!\n");
    exit(1);
  }
  memcpy(scl->data+scl->datapos, buf, len);
  scl->datapos += len;
}


static void sclWriteData (SCLFile *scl, const void *buf, size_t len) {
  if (scl->currfstartpos == 0xffffffffu) {
    fprintf(stderr, "FATAL: no open SCL file!\n");
    exit(1);
  }
  sclWriteDataForce(scl, buf, len);
}


static void sclWriteByte (SCLFile *scl, uint8_t b) {
  sclWriteData(scl, &b, 1);
}


static void sclWriteWord (SCLFile *scl, uint16_t w) {
  uint8_t b = w&0xff;
  sclWriteData(scl, &b, 1);
  b = (w>>8)&0xff;
  sclWriteData(scl, &b, 1);
}


static int sclFileWrite (FILE *fo, const void *buf, size_t count, uint32_t *checksum) {
  if (!count) return 0;
  const uint8_t *p = (const uint8_t *)buf;
  if (checksum) for (size_t n = count; n--; ++p) *checksum += (uint32_t)(*p);
  if (fwrite(buf, count, 1, fo) != 1) return -1;
  return 0;
}


// <0: error; 0: ok
static int sclSaveToFile (FILE *fo, SCLFile *scl) {
  if (scl->currfstartpos != 0xffffffffu) {
    fprintf(stderr, "FATAL: last SCL file is not finished!\n");
    exit(1);
  }
  const char *sign = "SINCLAIR";
  uint32_t checksum = 0;
  // header
  if (sclFileWrite(fo, sign, 8, &checksum) != 0) return -1;
  if (sclFileWrite(fo, &scl->fcount, 1, &checksum) != 0) return -1;
  // directory
  if (sclFileWrite(fo, scl->dir, scl->dirpos, &checksum) != 0) return -1;
  // data
  if (sclFileWrite(fo, scl->data, scl->datapos, &checksum) != 0) return -1;
  // write checksum
  for (unsigned f = 0; f < 4; ++f) {
    const uint8_t b = (checksum>>(f*8))&0xff;
    if (fwrite(&b, 1, 1, fo) != 1) return -1;
  }
  // done
  return 0;
}


// ////////////////////////////////////////////////////////////////////////// //
static void saveSCLCargador (SCLFile *scl) {
  static uint8_t cargador[16384]; // should be enough for everyone
  int linestart = -1;
  int linenum = 0;
  int start = 0, len, pos;

  void putStr (const char *s) {
    for (; *s; ++s) cargador[pos++] = *s;
  }

  void putNum (int num) {
    char buf[64];
    sprintf(buf, "%d", num);
    putStr(buf);
  }

  void startLine () {
    ++linenum;
    linestart = pos;
    // number
    cargador[pos++] = (linenum>>8)&0xff;
    cargador[pos++] = linenum&0xff;
    // size (will be fixed later)
    cargador[pos++] = 0;
    cargador[pos++] = 0;
  }

  void flushLine () {
    if (linestart >= 0) {
      const int size = pos-linestart-4;
      cargador[linestart+2] = size&0xff;
      cargador[linestart+3] = (size>>8)&0xff;
    }
    linestart = -1;
  }

  char cname[16];

  pos = 0;
  startLine();
  // CLEAR VAL "xxx"
  putStr("\xfd\xb0\""); putNum(clrAddr); putStr("\"");

  // load blocks
  int cont = 1;
  while (findChunkFrom(start, &start, &len)) {
    // :RANDOMIZE USR VAL "15619":REM:LOAD "
    if (cont) { putStr(":"); cont = 0; }
    putStr("\xf9\xc0\xb0\""); putNum(15619); putStr("\":\xea:\xef\"");
    // generate chunk name
    snprintf(cname, sizeof(cname), "c%04X", (unsigned)start);
    putStr(cname);
    //" CODE
    putStr("\"\xaf\r");
    flushLine();
    startLine();
    start += len;
  }

  // load code files
  for (OutDataFile *off = datafiles; off; off = off->next) {
    if (off->type == 1) continue; // skip +3DOS bootsector
    if (off->type != 'C') continue;
    if (!off->hasstart) continue;
    // :RANDOMIZE USR VAL "15619":REM:LOAD "
    if (cont) { putStr(":"); cont = 0; }
    putStr("\xf9\xc0\xb0\""); putNum(15619); putStr("\":\xea:\xef\"");
    // generate chunk name
    snprintf(cname, sizeof(cname), "%s", off->name);
    char *dotpos = strchr(cname, '.');
    char type = 'C';
    if (dotpos) { type = toUpper(dotpos[1]); *dotpos = 0; }
    if (type < 'A' || type > 'Z') type = 'C';
    for (char *s = cname; *s; ++s) *s = toUpper(*s);
    putStr(cname);
    //" CODE
    putStr("\"\xaf\r");
    flushLine();
    startLine();
  }

  // and run
  if (cont) { putStr(":"); cont = 0; }
  // RANDOMIZE USR VAL "xxx"
  putStr("\xf9\xc0\xb0\""); putNum(ent); putStr("\"");
  putStr("\r");
  flushLine();

  //putWord(0xaa80);
  //putWord(1); // autostart line

  // write to SCL
  sclStartFile(scl, "CARGADOR", 'B', (unsigned)pos, (unsigned)pos);
  sclWriteData(scl, cargador, (size_t)pos);
  sclWriteByte(scl, 0x80);
  sclWriteByte(scl, 0xaa);
  sclWriteWord(scl, 1);
  sclEndFile(scl);
}


static void saveSCLCargadorMono (SCLFile *scl) {
  static uint8_t cargador[16384]; // should be enough for everyone
  int linestart = -1;
  int linenum = 0;
  int start = 0, len, pos;

  void putDB (uint8_t v) {
    cargador[pos++] = v;
  }

  void putDW (uint16_t v) {
    cargador[pos++] = v&0xff;
    cargador[pos++] = (v>>8)&0xff;
  }

  void putStr (const char *s) {
    for (; *s; ++s) cargador[pos++] = *s;
  }

  void putNum (int num) {
    char buf[64];
    sprintf(buf, "%d", num);
    putStr(buf);
  }

  void startLine () {
    linestart = pos;
    // number
    cargador[pos++] = (linenum>>8)&0xff;
    cargador[pos++] = linenum&0xff;
    // size (will be fixed later)
    cargador[pos++] = 0;
    cargador[pos++] = 0;
    linenum++;
  }

  void flushLine () {
    if (linestart >= 0) {
      const int size = pos-linestart-4;
      cargador[linestart+2] = size&0xff;
      cargador[linestart+3] = (size>>8)&0xff;
    }
    linestart = -1;
  }

  pos = 0;
  startLine();

  // 0 REM code
  putStr("\xea");
  // monoloader parameters:
  //   dw ldr_copy_addr
  //   dw ldr_sp
  //   dw prg_start_addr
  //   dw prg_sp
  // block count
  const int bcountpos = pos;
  putDB(0);
  putDW(0x4000); // to screen$
  putDW(0x4200); // stack
  putDW(ent); // program start address
  putDW(0x0000); // stack
  // copy monoloader
  for (uint32_t f = 9; f < (uint32_t)sizeof(monoldr_code); ++f) {
    putDB(monoldr_code[f]);
  }
  // put block data
  while (findChunkFrom(start, &start, &len)) {
    if (len > 255*256) fatal("code chunk too big");
    // sector count
    putDB(len/256+(len%256 ? 1 : 0));
    // address
    putDW(start);
    start += len;
    // one more block
    ++cargador[bcountpos];
  }
  // load code files
  for (OutDataFile *off = datafiles; off; off = off->next) {
    if (off->type == 1) continue; // skip +3DOS bootsector
    if (off->type != 'C') continue;
    if (!off->hasstart) continue;
    if (off->size > 255*256) fatal("data chunk too big");
    // sector count
    putDB(off->size/256+(off->size%256 ? 1 : 0));
    // address
    putDW(off->start);
    // one more block
    ++cargador[bcountpos];
  }
  // no more blocks
  putDB(0);

  flushLine();
  startLine();

  // CLEAR VAL "xxx":RANDOMIZE USR PEEK VAL "23635"+VAL "256"*PEEK VAL "23636"+VAL "14"
  putStr("\xfd\xb0\""); putNum(clrAddr); putStr("\":");
  putStr("\xf9\xc0(\xbe\xb0\"23635\"+\xb0\"256\"*\xbe\xb0\"23636\"+\xb0\"14\")\r");
  flushLine();

  // write to SCL
  sclStartFile(scl, "CARGADOR", 'B', (unsigned)pos, (unsigned)pos);
  sclWriteData(scl, cargador, (size_t)pos);
  sclWriteByte(scl, 0x80);
  sclWriteByte(scl, 0xaa);
  sclWriteWord(scl, 1);
  sclEndFile(scl);

  // put block data
  uint8_t ssz = 0;
  start = 0;
  while (findChunkFrom(start, &start, &len)) {
    ssz += len/256+(len%256 ? 1 : 0);
    sclWriteDataForce(scl, memory+start, len);
    /* align to sector */
    while (scl->datapos%256) scl->data[scl->datapos++] = 0;
    start += len;
  }
  for (unsigned dphase = 0; dphase < 2; ++dphase) {
    // put code files
    for (OutDataFile *off = datafiles; off; off = off->next) {
      if (off->type == 1) continue; // skip +3DOS bootsector
      if (off->type != 'C') continue;
      if (dphase == 0) {
        if (!off->hasstart) continue;
      } else {
        if (off->hasstart) continue;
      }
      if (off->size > 255*256) fatal("data chunk too big");
      ssz += off->size/256+(off->size%256 ? 1 : 0);
      sclWriteDataForce(scl, off->data, off->size);
      /* align to sector */
      while (scl->datapos%256) scl->data[scl->datapos++] = 0;
    }
  }

  // fix basic file sector size
  scl->dir[scl->dirpos-1] += ssz;
}


static void saveSCL (const char *basename) {
  SCLFile scl;
  int fcount = 0;
  int start, len;
  //char *fname = strprintf("%s/%s.scl", optOutputDir, basename);
  char *fname = buildOutputFileName(basename, "scl");

  // count files
  start = 0;
  while (findChunkFrom(start, &start, &len)) {
    ++fcount;
    start += len;
  }
  // count data files
  for (OutDataFile *off = datafiles; off; off = off->next) {
    if (off->type == 1) continue; // skip +3DOS bootsector
    if (off->type == 'C') ++fcount;
  }

  if (fcount && optRunSCL) fcount += (optRunSCL > 0 ? 2 : 1); // +loader and boot
  if (fcount > 255) {
    fprintf(stderr, "ERROR: can't write file '%s' (too many chunks: %d)!\n", fname, fcount);
    exit(1);
  }

  // create output file
  FILE *fo = fopen(fname, "wb");
  if (!fo) {
    fprintf(stderr, "ERROR: can't write file '%s'!\n", fname);
    exit(1);
  }

  // initialise SCL writer
  sclInit(&scl);
  // write loader
  if (fcount && optRunSCL) {
    // create simple boot
    if (optRunSCL > 0) {
      const uint8_t dasboot[] = {
        0xf9,0xc0,0xb0,'"','1','5','6','1','9','"',':',0xea,':',0xef,'"','C','A','R','G','A','D','O','R','"','\r', //RANDOMIZE USR VAL "15619":REM:LOAD "CARGADOR"
      };
      sclStartFile(&scl, "boot", 'B', sizeof(dasboot)+4, sizeof(dasboot)+4);
      sclWriteWord(&scl, 1); // line number
      sclWriteWord(&scl, (uint16_t)sizeof(dasboot)); // line size
      sclWriteData(&scl, dasboot, sizeof(dasboot)); // line data
      // TR-DOS signature
      sclWriteByte(&scl, 0x80);
      sclWriteByte(&scl, 0xaa);
      // start line
      sclWriteWord(&scl, 0);
      sclEndFile(&scl);
    }
    saveSCLCargador(&scl);
  }

  // write chunks
  char cname[16];
  start = 0;
  while (findChunkFrom(start, &start, &len)) {
    snprintf(cname, sizeof(cname), "c%04X", (unsigned)start);
    sclStartFile(&scl, cname, 'C', (unsigned)start, (unsigned)len);
    sclWriteData(&scl, memory+(unsigned)start, (unsigned)len);
    sclEndFile(&scl);
    start += len;
  }

  // write data files
  for (OutDataFile *off = datafiles; off; off = off->next) {
    if (off->type == 1) continue; // skip +3DOS bootsector
    if (off->type != 'C') {
      fprintf(stderr, "skipping data file with invalid type '%C'\n", off->type);
      continue;
    }
    snprintf(cname, sizeof(cname), "%s", off->name);
    char *dotpos = strchr(cname, '.');
    char type = 'C';
    if (dotpos) { type = toUpper(dotpos[1]); *dotpos = 0; }
    if (type < 'A' || type > 'Z') type = 'C';
    for (char *s = cname; *s; ++s) *s = toUpper(*s);
    sclStartFile(&scl, cname, type, (unsigned)off->start, (unsigned)off->size);
    sclWriteData(&scl, off->data, (unsigned)off->size);
    sclEndFile(&scl);
  }

  if (sclSaveToFile(fo, &scl) < 0) {
    fprintf(stderr, "ERROR: error writing file '%s'!\n", fname);
    fclose(fo);
    unlink(fname);
    exit(1);
  }
  sclClear(&scl);
  fclose(fo);
  printf("out: %s\n", fname);
  if (fname != NULL) free(fname);
}


static void saveSCLMono (const char *basename) {
  SCLFile scl;
  int start, len;
  //char *fname = strprintf("%s/%s.scl", optOutputDir, basename);
  char *fname = buildOutputFileName(basename, "scl");

  // count total size in sectors
  int secsize = 1; // for loader
  start = 0;
  while (findChunkFrom(start, &start, &len)) {
    secsize += len/256+(len%256 ? 1 : 0);
    start += len;
  }
  // count data files
  for (OutDataFile *off = datafiles; off; off = off->next) {
    if (off->type == 1) continue; // skip +3DOS bootsector
    if (off->type == 'C') {
      secsize += off->size/256+(off->size%256 ? 1 : 0);
    }
  }

  if (secsize > 254) {
    fprintf(stderr, "ERROR: can't write file '%s' (too many sectors: %d)!\n", fname, secsize);
    exit(1);
  }

  // create output file
  FILE *fo = fopen(fname, "wb");
  if (!fo) {
    fprintf(stderr, "ERROR: can't write file '%s'!\n", fname);
    exit(1);
  }

  // initialise SCL writer
  sclInit(&scl);
  // write loader
  if (optRunSCL) {
    // create simple boot
    if (optRunSCL > 0) {
      const uint8_t dasboot[] = {
        0xf9,0xc0,0xb0,'"','1','5','6','1','9','"',':',0xea,':',0xef,'"','C','A','R','G','A','D','O','R','"','\r', //RANDOMIZE USR VAL "15619":REM:LOAD "CARGADOR"
      };
      sclStartFile(&scl, "boot", 'B', sizeof(dasboot)+4, sizeof(dasboot)+4);
      sclWriteWord(&scl, 1); // line number
      sclWriteWord(&scl, (uint16_t)sizeof(dasboot)); // line size
      sclWriteData(&scl, dasboot, sizeof(dasboot)); // line data
      // TR-DOS signature
      sclWriteByte(&scl, 0x80);
      sclWriteByte(&scl, 0xaa);
      // start line
      sclWriteWord(&scl, 0);
      sclEndFile(&scl);
    }
  }
  saveSCLCargadorMono(&scl);

  if (sclSaveToFile(fo, &scl) < 0) {
    fprintf(stderr, "ERROR: error writing file '%s'!\n", fname);
    fclose(fo);
    unlink(fname);
    exit(1);
  }
  sclClear(&scl);
  fclose(fo);
  printf("out: %s\n", fname);
  if (fname != NULL) free(fname);
}


// ////////////////////////////////////////////////////////////////////////// //
int optRunDSK = 0;
int optDskType = P3DSK_PCW_SS;


static int saveDSKCargador (P3DiskInfo *p3d, int autorun) {
  static uint8_t cargador[16384]; // should be enough for everyone
  int linestart = -1;
  int linenum = 0;
  int start = 0, len, pos;

  void putStr (const char *s) {
    for (; *s; ++s) cargador[pos++] = *s;
  }

  void putNum (int num) {
    char buf[64];
    sprintf(buf, "%d", num);
    putStr(buf);
  }

  void startLine () {
    ++linenum;
    linestart = pos;
    // number
    cargador[pos++] = (linenum>>8)&0xff;
    cargador[pos++] = linenum&0xff;
    // size (will be fixed later)
    cargador[pos++] = 0;
    cargador[pos++] = 0;
  }

  void flushLine () {
    if (linestart >= 0) {
      const int size = pos-linestart-4;
      cargador[linestart+2] = size&0xff;
      cargador[linestart+3] = (size>>8)&0xff;
    }
    linestart = -1;
  }

  char cname[16];

  pos = 0;
  startLine();
  // CLEAR VAL "xxx"
  putStr("\xfd\xb0\""); putNum(clrAddr); putStr("\"");
  // load blocks
  while (findChunkFrom(start, &start, &len)) {
    // :LOAD "
    putStr(":\xef\"");
    // generate chunk name
    snprintf(cname, sizeof(cname), "C%04X.BIN", (unsigned)start);
    putStr(cname);
    // " CODE
    putStr("\"\xaf");
    start += len;
  }
  putStr("\r");
  flushLine();
  startLine();
  // and run
  // :RANDOMIZE USR VAL "xxx"
  putStr("\xf9\xc0\xb0\""); putNum(ent); putStr("\"\r");
  flushLine();

  // write to DSK
  // build cp/m 8.3 name
  char pdfname[16];
  if (autorun) strcpy(pdfname, "DISK"); else strcpy(pdfname, "CARGADOR.BAS");

  int crerr = p3dskCreateFile(p3d, pdfname);
  if (crerr < 0) {
    if (crerr == FLPERR_MANYFILES) { fprintf(stderr, "NO FREE DIRECTORY ENTRIES (creating)!\n"); return -1; }
    if (crerr != FLPERR_FILEEXIST) { fprintf(stderr, "+3DOS disk error (creating)\n"); return -1; }
    p3dskDeleteFiles(p3d, pdfname);
    crerr = p3dskCreateFile(p3d, pdfname);
    if (crerr < 0) { fprintf(stderr, "+3DOS disk error (creating1)\n"); return -1; }
  }

  P3DskFileInfo nfo;
  if (p3dskOpenFile(p3d, &nfo, pdfname) != FLPERR_OK) { fprintf(stderr, "cannot open created +3DOS file '%s'\n", pdfname); return -1; }

  int wrres = p3dskWriteFile(&nfo, cargador, 128, (int)pos);
  if (wrres == FLPERR_MANYFILES) { fprintf(stderr, "NO FREE DIRECTORY ENTRIES (writing)!\n"); return -1; }
  if (wrres == FLPERR_NOSPACE) { fprintf(stderr, "NO FREE SPACE (writing)!\n"); return -1; }
  if (wrres < 0) { fprintf(stderr, "+3DOS disk writing error (writing)\n"); return -1; }

  // write +3DOS header
  P3DskFileHeader hdr;
  memset(&hdr, 0, sizeof(P3DskFileHeader));
  hdr.valid = 1;
  hdr.issue = 1;
  hdr.version = 0;
  hdr.filesize = (uint32_t)(pos+128);
  hdr.bastype = 0; // basic
  hdr.baslength = (uint16_t)pos;
  hdr.basaddr = 1;
  hdr.basvarsofs = (uint16_t)pos;
  if (p3dskWriteFileHeader(&nfo, &hdr) != FLPERR_OK) { fprintf(stderr, "error writing +3DOS file header\n"); return -1; }

  return 0;
}


static int saveDSKChunk (P3DiskInfo *p3d, const char *name, const void *data, int start, int len, int p3header) {
  // write to DSK
  // build cp/m 8.3 name
  char pdfname[13];
  if (p3dskNormaliseFileName(pdfname, name) != FLPERR_OK) { fprintf(stderr, "bad +3DOS file name '%s'\n", name); return -1; }

  for (char *s = pdfname; *s; ++s) if (*s >= 'a' && *s <= 'z') s[0] -= 32;

  int crerr = p3dskCreateFile(p3d, pdfname);
  if (crerr < 0) {
    if (crerr == FLPERR_MANYFILES) { fprintf(stderr, "NO FREE DIRECTORY ENTRIES (creating)!\n"); return -1; }
    if (crerr != FLPERR_FILEEXIST) { fprintf(stderr, "+3DOS disk error (creating)\n"); return -1; }
    p3dskDeleteFiles(p3d, pdfname);
    crerr = p3dskCreateFile(p3d, pdfname);
    if (crerr < 0) { fprintf(stderr, "+3DOS disk error (creating1)\n"); return -1; }
  }

  P3DskFileInfo nfo;
  if (p3dskOpenFile(p3d, &nfo, pdfname) != FLPERR_OK) { fprintf(stderr, "cannot open created +3DOS file '%s'\n", pdfname); return -1; }

  int wrres = p3dskWriteFile(&nfo, data, (p3header ? 128 : 0), (int)len);
  if (wrres == FLPERR_MANYFILES) { fprintf(stderr, "NO FREE DIRECTORY ENTRIES (writing)!\n"); return -1; }
  if (wrres == FLPERR_NOSPACE) { fprintf(stderr, "NO FREE SPACE (writing)!\n"); return -1; }
  if (wrres < 0) { fprintf(stderr, "+3DOS disk writing error (writing)\n"); return -1; }

  // write +3DOS header
  if (p3header) {
    P3DskFileHeader hdr;
    memset(&hdr, 0, sizeof(P3DskFileHeader));
    hdr.valid = 1;
    hdr.issue = 1;
    hdr.version = 0;
    hdr.filesize = (uint32_t)(len+128);
    hdr.bastype = 3; // code
    hdr.baslength = (uint16_t)len;
    hdr.basaddr = (uint16_t)start;
    hdr.basvarsofs = 0;
    if (p3dskWriteFileHeader(&nfo, &hdr) != FLPERR_OK) { fprintf(stderr, "error writing +3DOS file header\n"); return -1; }
  }

  return 0;
}


static void saveDSK (const char *basename) {
  // create and format the floppy
  Floppy *flp = flpCreate(0);
  if (p3dskFormatDisk(flp, optDskType) != FLPERR_OK) {
    flpDestroy(flp);
    fprintf(stderr, "ERROR: cannot format +3DOS disk\n");
    return;
  }

  P3DiskInfo p3d;
  p3d.flp = flp;
  if (p3dskDetectGeom(p3d.flp, &p3d.geom) < 0) {
    flpDestroy(flp);
    fprintf(stderr, "ERROR: not a +3DOS disk!\n");
    return;
  }

  int hasbootsector = 0;
  for (OutDataFile *off = datafiles; off; off = off->next) {
    if (off->type == 1) { hasbootsector = 1; break; }
  }

  int start = 0, len;
  int haschunks = findChunkFrom(start, &start, &len);

  if (!hasbootsector && haschunks) {
    if (saveDSKCargador(&p3d, optRunDSK) != 0) {
      flpDestroy(flp);
      return;
    }
  }

  // write chunks
  start = 0;
  while (findChunkFrom(start, &start, &len)) {
    char cname[16];
    snprintf(cname, sizeof(cname), "C%04X.BIN", (unsigned)start);
    if (saveDSKChunk(&p3d, cname, memory+(unsigned)start, start, len, 1) != 0) {
      flpDestroy(flp);
      return;
    }
    start += len;
  }

  // write data files
  for (OutDataFile *off = datafiles; off; off = off->next) {
    if (off->type == 1) {
      // +3DOS bootsector
      if (!p3d.geom.restracks) {
        fprintf(stderr, "no room for +3DOS bootsector\n");
        continue;
      }
      uint8_t *bootsec = malloc(512);
      if (flpGetSectorData(p3d.flp, 0, p3d.geom.firstsector, bootsec, 512) != FLPERR_OK) {
        fprintf(stderr, "cannot read +3DOS bootsector\n");
        continue;
      }
      size_t clen = off->size;
      if (clen > 512-16) clen = 512-16;
      if (clen) memcpy(bootsec+16, off->data, clen);
      if (flpPutSectorData(p3d.flp, 0, p3d.geom.firstsector, bootsec, 512) != FLPERR_OK) fatal("cannot write +3DOS bootsector");
      if (p3dskWriteBootableChecksum(&p3d) != FLPERR_OK) fatal("cannot make +3DOS disk bootable");
      printf("written +3DOS bootsector (%u bytes of code)\n", off->size);
      continue;
    }

    if (off->type != 'C') {
      fprintf(stderr, "skipping data file with invalid type '%C'\n", off->type);
      continue;
    }
    char pdfname[13];
    if (p3dskNormaliseFileName(pdfname, off->name) != FLPERR_OK) {
      fprintf(stderr, "skipping data file with invalid name '%s'\n", off->name);
      continue;
    }
    saveDSKChunk(&p3d, pdfname, off->data, off->start, off->size, off->hasstart);
  }

  // write disk
  //char *fname = strprintf("%s/%s.dsk", optOutputDir, basename);
  char *fname = buildOutputFileName(basename, "dsk");
  FILE *fo = fopen(fname, "wb");
  if (!fo) { fprintf(stderr, "cannot create disk file '%s'\n", fname); free(fname); flpDestroy(flp); return; }
  if (dskSaveDSK(flp, fo) != FLPERR_OK) {
    unlink(fname);
    fprintf(stderr, "error writing disk file '%s'\n", fname);
    free(fname);
    flpDestroy(flp);
    return;
  }
  fclose(fo);
  printf("out: %s\n", fname);
  free(fname);
  flpDestroy(flp);
}


///////////////////////////////////////////////////////////////////////////////
// .dmb
//
static int saveDMB (const char *fname) {
  char *fn;// = malloc(strlen(fname)+16);
  int start = 0, len;
  uint16_t pcnt = 0;
  FILE *fo;
  //
  //fn = strprintf("%s/%s.dmb", optOutputDir, fname);
  fn = buildOutputFileName(fname, "dmb");
  fo = fopen(fn, "wb");
  free(fn);
  if (!fo) { fprintf(stderr, "ERROR: can't write DMB file: %s.dmb\n", fname); return -1; }
  //
  while (findChunkFrom(start, &start, &len)) { ++pcnt; start += len; }
  //
  printf("out: %s.dmb\n", fname);
  if (fwrite("DMB1", 4, 1, fo) != 1) goto error;
  if (fWriteWord(fo, (optRunDMB ? ent : 0), NULL) != 0) goto error;
  if (fWriteWord(fo, clrAddr, NULL) != 0) goto error;
  if (fWriteWord(fo, pcnt, NULL) != 0) goto error;
  //
  start = 0;
  while (findChunkFrom(start, &start, &len)) {
    if (fWriteWord(fo, len, NULL) != 0) goto error;
    if (fWriteWord(fo, start, NULL) != 0) goto error;
    if (fwrite(memory+start, len, 1, fo) != 1) goto error;
    start += len;
  }
  fclose(fo);
  return 0;
error:
  fclose(fo);
  unlink(fname);
  fprintf(stderr, "ERROR: error writing file %s.dmb!\n", fname);
  return -1;
}


///////////////////////////////////////////////////////////////////////////////
// .tap
//
static char tapeLoaderName[16];


static void saveTapCargador (FILE *fo) {
  // count blocks
  static uint8_t cargador[16384]; // should be enough for everyone
  int start = 0, len, pos, f;
  uint8_t bxor;
  //

  void putStr (const char *s) {
    for (; *s; ++s) cargador[pos++] = *s;
  }

  void putNum (int num) {
    char buf[64];
    sprintf(buf, "%d", num);
    putStr(buf);
  }

  //
  pos = 4;
  // number
  cargador[0] = 0; cargador[1] = 10;
  // size (will be fixed later)
  putStr("\xfd\xb0\""); putNum(clrAddr); putStr("\""); // CLEAR VAL "xxx"
  // load blocks
  while (findChunkFrom(start, &start, &len)) {
    // :LOAD "" CODE
    putStr(":\xef\"\"\xaf");
    start += len;
  }
  // additional code files
  for (OutDataFile *off = datafiles; off; off = off->next) {
    if (off->type == 1) continue; // skip +3DOS bootsector
    if (off->type != 'C' || off->size > 65533 || !off->hasstart) {
      continue;
    }
    // :LOAD "" CODE
    putStr(":\xef\"\"\xaf");
    start += len;
  }
  // and run
  // :RANDOMIZE USR VAL "xxx"
  putStr(":\xf9\xc0\xb0\""); putNum(ent); putStr("\"\r");
  // patch len
  cargador[2] = (pos-4)&0xff;
  cargador[3] = ((pos-4)>>8)&0xff;
  // write header
  fWriteWord(fo, 19, NULL); // length of header
  bxor = 0;
  fWriteByte(fo, 0, &bxor); // header block
  fWriteByte(fo, 0, &bxor); // 'basic' flag
  if (tapeLoaderName[0]) {
    for (int f = 0; f < 10; ++f) fWriteByte(fo, tapeLoaderName[f], &bxor);
  } else {
    fWriteByte(fo, 'c', &bxor);
    fWriteByte(fo, 'a', &bxor);
    fWriteByte(fo, 'r', &bxor);
    fWriteByte(fo, 'g', &bxor);
    fWriteByte(fo, 'a', &bxor);
    fWriteByte(fo, 'd', &bxor);
    fWriteByte(fo, 'o', &bxor);
    fWriteByte(fo, 'r', &bxor);
    fWriteByte(fo, ' ', &bxor);
    fWriteByte(fo, ' ', &bxor);
  }
  fWriteWord(fo, pos, &bxor); // length
  fWriteWord(fo, 10, &bxor); // start
  fWriteWord(fo, pos, &bxor); // length2
  fWriteByte(fo, bxor, NULL);
  // write data
  fWriteWord(fo, pos+2, NULL); // plus type and checkbyte
  bxor = 0;
  fWriteByte(fo, 0xFFU, &bxor); // data block
  for (f = 0; f < pos; ++f) fWriteByte(fo, cargador[f], &bxor);
  fWriteByte(fo, bxor, NULL);
}


static void saveTap (const char *basename) {
  char *fname;// = malloc(strlen(basename)+16);
  char blkname[128];
  int start = 0, len, f;
  uint8_t bxor;
  FILE *fo;

  //fname = strprintf("%s/%s.tap", optOutputDir, basename);
  fname = buildOutputFileName(basename, "tap");
  fo = fopen(fname, "wb");
  free(fname);
  if (!fo) { fprintf(stderr, "ERROR: can't write file %s.tap!\n", basename); return; }
  printf("out: %s.tap\n", basename);
  if (optRunTape) saveTapCargador(fo);
  while (findChunkFrom(start, &start, &len)) {
    // write header
    if (tapeLoaderName[0]) {
      //for (int f = 0; f < 10; ++f) fWriteByte(fo, tapeLoaderName[f], &bxor);
      memcpy(blkname, tapeLoaderName, 10);
    } else {
      sprintf(blkname, "c%04X:%04X", start, len);
    }
    //printf(" block: %s\n", blkname);
    fWriteWord(fo, 19, NULL); // length of header
    bxor = 0;
    fWriteByte(fo, 0, &bxor); // header block
    fWriteByte(fo, 3, &bxor); // 'code' flag
    for (f = 0; f < 10; ++f) fWriteByte(fo, blkname[f], &bxor);
    fWriteWord(fo, len, &bxor);
    fWriteWord(fo, start, &bxor);
    fWriteWord(fo, 32768, &bxor);
    fWriteByte(fo, bxor, NULL);
    // write data
    fWriteWord(fo, len+2, NULL); // plus type and checkbyte
    bxor = 0;
    fWriteByte(fo, 0xFFU, &bxor); // data block
    for (f = 0; f < len; ++f) fWriteByte(fo, memory[start+f], &bxor);
    fWriteByte(fo, bxor, NULL);
    start += len;
  }

  for (int dfpass = 0; dfpass < 2; ++dfpass) {
    // write data files
    for (OutDataFile *off = datafiles; off; off = off->next) {
      if (off->type == 1) continue; // skip +3DOS bootsector
      if (off->type != 'C' || off->size > 65533) {
        fprintf(stderr, "skipping data file with invalid type '%C'\n", off->type);
        continue;
      }
      if (dfpass == 0) {
        if (!off->hasstart) continue;
      } else {
        if (off->hasstart) continue;
      }
      snprintf(blkname, 10, "%s", off->name);
      blkname[10] = 0; // just in case
      while (strlen(blkname) < 10) strcat(blkname, " ");
      // write data block
      fWriteWord(fo, 19, NULL); // length of header
      bxor = 0;
      fWriteByte(fo, 0, &bxor); // header block
      fWriteByte(fo, 3, &bxor); // 'code' flag
      for (f = 0; f < 10; ++f) fWriteByte(fo, blkname[f], &bxor);
      fWriteWord(fo, off->size, &bxor);
      fWriteWord(fo, off->start, &bxor);
      fWriteWord(fo, 32768, &bxor);
      fWriteByte(fo, bxor, NULL);
      // write data
      fWriteWord(fo, off->size+2, NULL); // plus type and checkbyte
      bxor = 0;
      fWriteByte(fo, 0xFFU, &bxor); // data block
      for (f = 0; f < off->size; ++f) fWriteByte(fo, off->data[f], &bxor);
      fWriteByte(fo, bxor, NULL);
    }
  }

  fclose(fo);
}


///////////////////////////////////////////////////////////////////////////////
// pseudoinstructions
//
// note that processCurrentLine() will NOT skip to the next line before
// calling pseudoinstruction handler!
// note that processCurrentLine() will skip current line after calling
// pseudoinstruction handler!
//
static int wasOrg = 0;
static int wasClr = 0;


// ////////////////////////////////////////////////////////////////////////// //
// print message using printf-like syntax
// doesn't print new line
static void processPrintf (FILE *fo, const char *fmt) {
  if (!fmt || !fmt[0]) return;
  /* it may be passed from argument parser, and destroyed by other arguments, so copy it */
  char *tempstr = NULL;
  size_t tempsize = 0;
  char *fmtcopy = malloc(strlen(fmt)+1);
  strcpy(fmtcopy, fmt);
  char *currfmt = fmtcopy;
  char tempbuf[512];
  int stlen;
  char *strarg;
  int defined;
  int32_t exprval;
  while (*currfmt) {
    /* find '%' */
    char *prcs = strchr(currfmt, '%');
    if (!prcs || !prcs[1]) {
      /* no more formatting; print the tail and exit the loop */
      fprintf(fo, "%s", currfmt);
      break;
    }
    /* is this "%%"? */
    int docheck = 1;
    if (prcs[1] == '%') {
      ++prcs;
      docheck = 0;
    }
    /* print up to `prcs` */
    if (prcs > currfmt) {
      size_t partlen = (ptrdiff_t)(prcs-currfmt);
      if (partlen+1 > tempsize) {
        tempsize = ((partlen+8)|0xff)+1;
        tempstr = realloc(tempstr, tempsize);
      }
      memcpy(tempstr, currfmt, partlen);
      tempstr[partlen] = 0;
      fprintf(fo, "%s", tempstr);
    }
    currfmt = ++prcs; /* skip percent */
    if (!docheck) continue;
    /* parse format */
    char sign = ' ';
    int width = 0;
    int zerofill = 0;
    char ftype = ' ';
    if (*currfmt == '+' || *currfmt == '-') sign = *currfmt++;
    if (sign != '-' && *currfmt == '0') zerofill = 1;
    while (isDigit(*currfmt)) { width = width*10+((*currfmt)-'0'); ++currfmt; }
    if (width > 256) width = 256;
    ftype = *currfmt++;
    if (!ftype) break; /* oops */
    if (!eatComma()) fatal("out of arguments for string format");
    switch (ftype) {
      case 's': /* string */
        stlen = 0;
        switch (strSkipSpaces(currLine)[0]) {
          case '"': case '\'': /* string literal? */
            strarg = getStrArg(&stlen);
            break;
          default: /* expression */
            strarg = getStrExprArgFmt();
            stlen = (int)strlen(strarg);
            break;
        }
        /* left pad */
        if (sign != '-' && stlen < width) {
          int padlen = width-stlen;
          memset(tempbuf, ' ', padlen);
          tempbuf[padlen+1] = 0;
          fprintf(fo, "%s", tempbuf);
        }
        fprintf(fo, "%s", strarg);
        /* right pad */
        if (sign == '-' && stlen < width) {
          int padlen = width-stlen;
          memset(tempbuf, ' ', padlen);
          tempbuf[padlen+1] = 0;
          fprintf(fo, "%s", tempbuf);
        }
        break;
      case 'd': /* decimal */
        defined = 1;
        exprval = getExprArg(&defined, NULL);
        if (width && zerofill) {
          fprintf(fo, "%0*d", width, exprval);
        } else if (width) {
          fprintf(fo, "%*d", (sign != '-' ? width : -width), exprval);
        } else {
          fprintf(fo, "%d", exprval);
        }
        break;
      case 'c': /* char */
        defined = 1;
        exprval = getExprArg(&defined, NULL);
        if (exprval <= 0 || exprval == '?' || exprval > 255) exprval = '?';
        if (width) {
          fprintf(fo, "%*c", (sign != '-' ? width : -width), exprval);
        } else {
          fprintf(fo, "%c", exprval);
        }
        break;
      case 'u': /* unsigned */
        defined = 1;
        exprval = getExprArg(&defined, NULL);
        if (width && zerofill) {
          fprintf(fo, "%0*u", width, (unsigned)(exprval&0xffff));
        } else if (width) {
          fprintf(fo, "%*u", (sign != '-' ? width : -width), (unsigned)(exprval&0xffff));
        } else {
          fprintf(fo, "%u", (unsigned)(exprval&0xffff));
        }
        break;
      case 'x': case 'X': /* hex */
        defined = 1;
        exprval = getExprArg(&defined, NULL);
        if (width && zerofill) {
          fprintf(fo, (ftype == 'x' ? "%0*x" : "%0*X"), width, (unsigned)(exprval&0xffff));
        } else if (width) {
          fprintf(fo, (ftype == 'x' ? "%*x" : "%*X"), (sign != '-' ? width : -width), (unsigned)(exprval&0xffff));
        } else {
          fprintf(fo, (ftype == 'x' ? "%x" : "%X"), (unsigned)(exprval&0xffff));
        }
        break;
      default:
        if (ftype <= 0 || ftype == 127) ftype = '?';
        fatal("invalid format specifier: '%c'", ftype);
        break;
    }
  }
  if (tempstr) free(tempstr);
  free(fmtcopy);
}


///////////////////////////////////////////////////////////////////////////////
// user error
//
static int piERROR (void) {
  int len = 0;
  char *res = getStrArg(&len);
  fprintf(stdout, "*** USER ERROR: ");
  processPrintf(stdout, res);
  fputc('\n', stdout);
  fatal("user error abort");
  return PI_SKIP_LINE;
}


static int piWARNING (void) {
  int len = 0;
  char *res = getStrArg(&len);
  fprintf(stdout, "*** USER WARNING ");
  if (currSrcLine) {
    fprintf(stdout, "at file %s, line %d: ", currSrcLine->fname, currSrcLine->lineNo);
  } else {
    fprintf(stdout, "somewhere in time: ");
  }
  processPrintf(stdout, res);
  fputc('\n', stdout);
  return PI_SKIP_LINE;
}


///////////////////////////////////////////////////////////////////////////////
// user warnings
//
static int piPrintfCommon (int passNo) {
  if (passNo < 0 || pass == passNo) {
    int len = 0;
    char *res = getStrArg(&len);
    processPrintf(stdout, res);
    fputc('\n', stdout);
  }
  return PI_SKIP_LINE;
}


static int piDISPLAYX (int passNo, int asHex) {
  for (;;) {
    if (isStrArg()) {
      int len = 0;
      char *res = getStrArg(&len);
      //
      if (passNo < 0 || pass == passNo) printf("%s", res);
    } else {
      int defined = 1;
      int32_t v = getExprArg(&defined, NULL);
      //
      if (passNo < 0 || pass == passNo) {
        if (asHex) printf("%04X", (unsigned int)v);
        else printf("%d", v);
      }
    }
    if (!eatComma()) break;
  }
  return PI_SKIP_LINE;
}


static int piDISPLAY (void) { return piDISPLAYX(1, 0); }
static int piDISPLAY0 (void) { return piDISPLAYX(0, 0); }
static int piDISPLAYA (void) { return piDISPLAYX(-1, 0); }
static int piDISPHEX (void) { return piDISPLAYX(1, 1); }
static int piDISPHEX0 (void) { return piDISPLAYX(0, 1); }
static int piDISPHEXA (void) { return piDISPLAYX(-1, 1); }

static int piPRINTF (void) { return piPrintfCommon(1); } /* 2nd pass */
static int piPRINTF0 (void) { return piPrintfCommon(0); } /* 1st pass */
static int piPRINTFA (void) { return piPrintfCommon(-1); } /* all passes */


///////////////////////////////////////////////////////////////////////////////
// ORG, DISP, etc.
//
static int piORG (void) {
  int defined = 1;
  int32_t res = getOneExprArg(&defined, NULL);
  //
  if (!defined) fatal("sorry, ORG operand value must be known here");
  if (res < 0 || res > 65535) fatal("invalid ORG operand value: %d", res);
  if (inTapeBlock) fatal("sorry, no ORGs in TAPEDATA allowed");
  pc = disp = res;
  if (!wasOrg) {
    wasOrg = 1;
    ent = res;
    if (!wasClr && res > 0) clrAddr = res-1;
  }
  return PI_CONT_LINE;
}


static int piDISP (void) {
  int defined = 1;
  int32_t res = getOneExprArg(&defined, NULL);
  //
  if (!defined) fatal("sorry, DISP operand value must be known here");
  if (res < 0 || res > 65535) fatal("invalid DISP operand value: %d", res);
  //printf("DISP=%d\n", res);
  disp = res;
  return PI_CONT_LINE;
}


static int piENDDISP (void) {
  if (inTapeBlock) fatal("sorry, no ENDDISPs in TAPEDATA allowed");
  checkExprEnd();
  disp = pc;
  return PI_CONT_LINE;
}


static int piENT (void) {
  int defined = 1;
  int32_t res = getOneExprArg(&defined, NULL);
  //
  //if (!defined) fatal("sorry, ENT operand value must be known here");
  if (res < 0 || res > 65535) fatal("invalid ENT operand value: %d", res);
  ent = res;
  return PI_CONT_LINE;
}


static int piCLR (void) {
  int defined = 1;
  int32_t res = getOneExprArg(&defined, NULL);
  //
  //if (!defined) fatal("sorry, CLR operand value must be known here");
  if (res < 0 || res > 65535) fatal("invalid CLR operand value: %d", res);
  clrAddr = res;
  wasClr = 1;
  return PI_CONT_LINE;
}


// RESERVE start, count
static int piRESERVE (void) {
  int defined = 1, start;
  int32_t res = getExprArg(&defined, NULL);
  if (!defined) fatal("sorry, RESERVE operand values must be known here");
  if (res < 0 || res > 65535) fatal("invalid RESERVE address value: %d", res);
  start = res;
  if (!eatComma()) fatal("RESERVE needs 2 args");
  res = getOneExprArg(&defined, NULL);
  if (!defined) fatal("sorry, RESERVE operand values must be known here");
  if (res < 0 || res > 65535) fatal("invalid RESERVE length value: %d", res);
  while (res--) {
    if (memused[start]) fatal("trying to reserve already used memory at #%04X", start);
    memresv[start++] = 1;
  }
  return PI_CONT_LINE;
}


static int piALIGN (void) {
  int defined = 1;
  int32_t res;
  //
  if (inTapeBlock) fatal("sorry, no ALIGNs in TAPEDATA allowed");
  res = getOneExprArg(&defined, NULL);
  if (!defined) fatal("sorry, ALIGN operand value must be known here");
  if (res < 0 || res > 65535) fatal("invalid ALIGN operand value: %d", res);
  if (pc != disp) fatal("sorry, no ALIGNs in desynced blocks allowed");
  if (res > 0 && pc%res != 0) {
    pc /= res;
    pc *= res;
    pc += res;
    disp = pc;
  }
  return PI_CONT_LINE;
}


static int piDISPALIGN (void) {
  int defined = 1;
  int32_t res = getOneExprArg(&defined, NULL);
  //
  if (!defined) fatal("sorry, DISPALIGN operand value must be known here");
  if (res < 0 || res > 65535) fatal("invalid DISPALIGN operand value: %d", res);
  if (res > 0 && disp%res != 0) {
    disp /= res;
    disp *= res;
    disp += res;
  }
  return PI_CONT_LINE;
}


///////////////////////////////////////////////////////////////////////////////
// DEFx
//
// DEFINCR operations
enum {
  DFI_SHR,
  DFI_SHL,
  DFI_SAR, // arith shift, extend bit 7
  DFI_SAL, // arith shift, extend bit 0
  DFI_ROR,
  DFI_ROL,
  DFI_AND,
  DFI_XOR,
  DFI_OR,
  DFI_ADD,
  DFI_SUB,
  DFI_MUL,
  DFI_DIV,
  DFI_MOD,
};

typedef struct DefIncrOp_s DefIncrOp;
struct DefIncrOp_s {
  DefIncrOp *next;
  uint16_t operator; // DFI_xxx
  uint16_t operand;
};

static DefIncrOp *dfi_list = NULL;
//static int defIncr = 0;


static void dfi_free (void) {
  while (dfi_list) {
    DefIncrOp *c = dfi_list;
    dfi_list = c->next;
    free(c);
  }
}


//FIXME: make this faster
#define DFI_XSHIFT(sop_)  do { \
  for (uint16_t f = 0; f < opnd; ++f) { \
    val = ((val&xmask) sop_ 1)|(val&andmask ? ormask : 0); \
  } \
} while (0)


static int32_t dfi_apply (int32_t val, int isbyte) {
  const uint16_t xmask = (isbyte ? 0xff : 0xffff);
  val &= xmask;
  uint16_t andmask, ormask;
  for (DefIncrOp *dop = dfi_list; dop; dop = dop->next) {
    uint16_t opnd = dop->operand;
    switch (dop->operator) {
      case DFI_SHR:
        if (!opnd) break;
        if (opnd > (isbyte ? 7 : 15)) { val = 0; break; }
        val = (val&xmask)>>opnd;
        break;
      case DFI_SHL:
        if (!opnd) break;
        if (opnd > (isbyte ? 7 : 15)) { val = 0; break; }
        val = (val&xmask)<<opnd;
        break;
      case DFI_SAR:
        if (!opnd) break;
        if (isbyte) {
          if (opnd > 7) { val = (val&0x80 ? 0xff : 0x00); break; }
          andmask = ormask = 0x80;
        } else {
          if (opnd > 15) { val = (val&0x8000 ? 0xffff : 0x0000); break; }
          andmask = ormask = 0x8000;
        }
        DFI_XSHIFT(>>);
        break;
      case DFI_SAL:
        if (!opnd) break;
        if (opnd > (isbyte ? 7 : 15)) { val = (val&0x01 ? 0xff : 0x00); break; }
        andmask = ormask = 0x01;
        DFI_XSHIFT(<<);
        break;
      case DFI_ROR:
        opnd &= (isbyte ? 7 : 15);
        if (!opnd) break;
        andmask = 0x01;
        ormask = (isbyte ? 0x80 : 0x8000);
        DFI_XSHIFT(>>);
        break;
      case DFI_ROL:
        opnd &= (isbyte ? 7 : 15);
        if (!opnd) break;
        andmask = (isbyte ? 0x80 : 0x8000);
        ormask = 0x01;
        DFI_XSHIFT(<<);
        break;
      case DFI_AND:
        val &= opnd;
        break;
      case DFI_XOR:
        val ^= opnd;
        break;
      case DFI_OR:
        val |= opnd;
        break;
      case DFI_ADD:
        val += opnd;
        break;
      case DFI_SUB:
        val -= opnd;
        break;
      case DFI_MUL:
        val *= opnd;
        break;
      case DFI_DIV:
        if (opnd) val /= opnd; else val = 0;
        break;
      case DFI_MOD:
        if (opnd) val %= opnd; else val = 0;
        break;
    }
    val &= xmask;
  }
  return val;
}


static int piDEFINCR (void) {
  #if 0
  int defined = 1;
  int32_t cnt = getOneExprArg(&defined, NULL);
  if (!defined) fatal("DEFINCR: increment must be defined");
  defIncr = cnt;
  return PI_CONT_LINE;
  #else
  dfi_free();
  if (isStrArg()) {
    int len = 0;
    char *rstr = getStrArg(&len);
    if (!rstr || !strEquCI(rstr, "expr")) fatal("invalid DEFINCR command (\"expr\" expected)");
    DefIncrOp *last = NULL;
    while (eatComma()) {
      len = 0;
      rstr = getStrArg(&len);
      if (!rstr || len != 3) fatal("invalid DEFINCR operator");
      DefIncrOp *dop = malloc(sizeof(DefIncrOp));
           if (strEquCI(rstr, "SHR")) dop->operator = DFI_SHR;
      else if (strEquCI(rstr, "SHL")) dop->operator = DFI_SHL;
      else if (strEquCI(rstr, "SAR")) dop->operator = DFI_SAR;
      else if (strEquCI(rstr, "SAL")) dop->operator = DFI_SAL;
      else if (strEquCI(rstr, "ROR")) dop->operator = DFI_ROR;
      else if (strEquCI(rstr, "ROL")) dop->operator = DFI_ROL;
      else if (strEquCI(rstr, "AND")) dop->operator = DFI_AND;
      else if (strEquCI(rstr, "XOR")) dop->operator = DFI_XOR;
      else if (strEquCI(rstr, "OR")) dop->operator = DFI_OR;
      else if (strEquCI(rstr, "ADD")) dop->operator = DFI_ADD;
      else if (strEquCI(rstr, "SUB")) dop->operator = DFI_SUB;
      else if (strEquCI(rstr, "MUL")) dop->operator = DFI_MUL;
      else if (strEquCI(rstr, "DIV")) dop->operator = DFI_DIV;
      else if (strEquCI(rstr, "MOD")) dop->operator = DFI_MOD;
      else fatal("invalid DEFINCR operator '%s'", rstr);
      dop->next = NULL;
      if (!eatComma()) fatal("DEFINCR: operand expected");
      int defined = 1;
      int32_t res = getExprArg(&defined, NULL);
      if (!defined) fatal("DEFINCR: operand must be defined");
      if (res < 0) fatal("DEFINCR: operand must be positive");
      res &= 0xffff;
      dop->operand = (uint16_t)res;
      if (last) last->next = dop; else dfi_list = dop;
      last = dop;
    }
  } else {
    int defined = 1;
    int32_t cnt = getOneExprArg(&defined, NULL);
    if (!defined) fatal("DEFINCR: increment must be defined");
    //defIncr = cnt;
    if (cnt > 0) {
      cnt &= 0xffff;
      if (cnt) {
        DefIncrOp *dop = malloc(sizeof(DefIncrOp));
        dop->next = NULL;
        dop->operator = DFI_ADD;
        dop->operand = cnt;
        dfi_list = dop;
      }
    } else {
      cnt &= 0xffff; //UB, i don't care
      if (cnt) {
        DefIncrOp *dop = malloc(sizeof(DefIncrOp));
        dop->next = NULL;
        dop->operator = DFI_SUB;
        dop->operand = cnt;
        dfi_list = dop;
      }
    }
  }
  #endif
  return PI_CONT_LINE;
}


static int piDEFBW (int isWord) {
  int32_t res;
  for (;;) {
    int defined = 0, fixuptype = UR_FIXUP_NONE;

    if (isStrArg()) {
      int f, len = 0;
      char *rstr = getStrExprArg(&len, isWord);
      if (rstr) {
        for (f = 0; f < len; ++f) {
          int32_t b = (uint8_t)rstr[f];
          //b += defIncr;
          b = dfi_apply(b, 1/*isbyte*/);
          if (b < -128 || b > 255) fatal("invalid operand value: %d", b);
          if (b < 0) b += 256;
          emitByte(b);
        }
        if (!eatComma()) break;
        continue;
      } else {
        defined = 1;
        res = len;
      }
    } else {
      res = getExprArg(&defined, &fixuptype);
    }

    {
      //int32_t res = getExprArg(&defined, &fixuptype);
      //
      if (pass > 0 && !defined) fatal("undefined operand");
      //res += defIncr;
      res = dfi_apply(res, !isWord/*isbyte*/);
      if (isWord) {
        if (res < -32768 || res > 65535) fatal("invalid operand value: %d", res);
        if (res < 0) res += 65536;
        if (isWord == 1) {
          if (fixuptype != UR_FIXUP_NONE) urasm_fixup_operand(NULL, pc, disp, fixuptype, 2);
          emitWord(res);
        } else {
          /* reversed word */
          switch (fixuptype) {
            case UR_FIXUP_WORD: /* swapped bytes */
              urasm_fixup_operand(NULL, pc, disp, UR_FIXUP_HIBYTE, 1);
              urasm_fixup_operand(NULL, pc, disp+1, UR_FIXUP_LOBYTE, 1);
              break;
            case UR_FIXUP_LOBYTE:
            case UR_FIXUP_HIBYTE:
              warningMsg("non-word fixup for reversed word; wtf?!");
              break;
            default: break;
          }
          emitRWord(res);
        }
      } else {
        if (res < -128 || res > 255) fatal("invalid operand value: %d", res);
        if (fixuptype != UR_FIXUP_NONE) {
          if (fixuptype == UR_FIXUP_WORD) warningMsg("invalid fixup for operand");
          urasm_fixup_operand(NULL, pc, disp, fixuptype, 1);
        }
        if (res < 0) res += 256;
        emitByte(res);
      }
    }
    if (!eatComma()) break;
  }
  return PI_CONT_LINE;
}

static int piDEFB (void) { return piDEFBW(0); }
static int piDEFW (void) { return piDEFBW(1); }
static int piDEFR (void) { return piDEFBW(2); }


static int piDEFS (void) {
  for (;;) {
    int32_t bt, f;
    int defined = 0, fixuptype = UR_FIXUP_NONE;
    int32_t res = getExprArg(&defined, &fixuptype);
    //
    if (pass > 0 && !defined) fatal("undefined operand");
    if (res < 1 || res > 65535) fatal("invalid number of repetitions: %d", res);
    if (*currLine && currLine[0] == ',') {
      (void)eatComma();
      bt = getExprArg(&defined, NULL);
      if (pass > 0 && !defined) fatal("undefined operand");
      //bt += defIncr;
      bt = dfi_apply(bt, 1/*isbyte*/);
      if (bt < -128 || bt > 255) fatal("invalid operand value: %d", res);
      if (bt < 0) bt += 256;
      if (fixuptype != UR_FIXUP_NONE) {
        if (fixuptype == UR_FIXUP_WORD) warningMsg("invalid fixup for operand");
      }
      for (f = 0; f < res; ++f) {
        if (fixuptype != UR_FIXUP_NONE) urasm_fixup_operand(NULL, pc, disp, fixuptype, 1);
        emitByte(bt);
      }
    } else {
      pc += res; disp += res;
    }
    if (!eatComma()) break;
  }
  return PI_CONT_LINE;
}


/* bit 0: put '\0' */
/* bit 1: set bit 7 of last byte */
/* bit 2: put length */
static int piDEFSTR (int type) {
  for (;;) {
    if (isStrArg()) {
      int f, len = 0;
      char *res = getStrArg(&len);
      //
      if (type&0x04) {
        if (len > 255) fatal("string too long");
        emitByte(len);
      }
      for (f = 0; f < len; ++f) {
        uint8_t b = (uint8_t)res[f];
        //
        if ((type&0x02) && f == len-1) b |= 0x80;
        emitByte(b);
      }
      if (type&0x01) emitByte(0);
    } else {
      int defined = 1;
      int32_t v = getExprArg(&defined, NULL);
      //
      if (pass > 0 && !defined) fatal("undefined expression");
      if (!defined) v = 0; else v = dfi_apply(v, 1/*isbyte*/);
      if (v < -128 || v > 255) fatal("invalid operand value: %d", v);
      if (v < 0) v += 256;
      emitByte(v);
    }
    if (!eatComma()) break;
  }
  return PI_CONT_LINE;
}


static int piDEFM (void) { return piDEFSTR(0x00); }
static int piDEFZ (void) { return piDEFSTR(0x01); }
static int piDEFX (void) { return piDEFSTR(0x02); }
static int piDEFC (void) { return piDEFSTR(0x04); }


///////////////////////////////////////////////////////////////////////////////
// INCBIN
//
/* INCBIN "name"[,maxlen[,offset]] */
static int piINCBIN (void) {
  int system = 0;
  char *fn, qCh;
  uint8_t bt;
  FILE *fl;
  int maxlen = 65536;
  int offset = 0;
  char *args = currLine;

  if (!currLine[0]) fatal("INCBIN without file name");

  if (isStrArg()) {
    qCh = *args++;
    system = 0;
  } else if (currLine[0] == '<') {
    qCh = '>'; ++args;
    system = 1;
  } else {
    qCh = 0;
    system = 0;
  }

  fn = parseStr(&args, qCh, NULL); // i don't care about string length here
  int softinclude = 0;
  if (fn[0] == '?') { softinclude = 1; ++fn; while (isSpace(*fn)) ++fn; }
  if (!fn[0]) fatal("INCBIN: empty file name");
  // now fix the name
  char *fname = createIncludeName(fn, system, NULL, NULL);
  memmove(currLine, args, strlen(args)+1);

  // maxlen
  if (currLine[0] == ',') {
    int defined = 1;
    (void)eatComma();
    maxlen = getExprArg(&defined, NULL);
    if (!defined) fatal("INCBIN: undefined maxlen");
    if (maxlen < 1) { free(fname); return 1; } // nothing to do
  }

  // offset (negative: from the end)
  if (currLine[0] == ',') {
    int defined = 1;
    (void)eatComma();
    offset = getOneExprArg(&defined, NULL);
    if (!defined) fatal("INCBIN: undefined offset");
  } else {
    if (currLine[0] && currLine[0] != ':') fatal("too many expressions");
  }

  fl = fopen(fname, "rb");
  if (!fl) {
    if (!softinclude) fatal("INCBIN: file not found: '%s'", fname);
  } else {
    if (offset) {
      if (fseek(fl, offset, (offset < 0 ? SEEK_END : SEEK_SET)) < 0) {
        fclose(fl);
        fatal("INCBIN: error seeking to %d in file '%s'", offset, fname);
      }
    }
    while (maxlen-- > 0) {
      int res = fread(&bt, 1, 1, fl);
      if (!res) break;
      if (res != 1) { fclose(fl); fatal("INCBIN: error reading file: '%s'", fname); }
      emitByte(bt);
    }
    fclose(fl);
  }

  free(fname);
  return PI_SKIP_LINE;
}


/* DATABIN "name"[,len[,offset[,codeaddr]]] */
/* DATABIN "name|dfname"[,len[,offset[,codeaddr]]] */
static int piDATABINcommon (int ascode) {
  if (pass != 1) return PI_SKIP_LINE;

  int system = 0;
  char *fn, qCh;
  int maxlen = -1;
  int offset = 0;
  int codeaddr = 32768;
  int hasstart = 0;
  char *args = currLine;

  if (!currLine[0]) fatal("DATABIN without file name");

  if (isStrArg()) {
    qCh = *args++;
    system = 0;
  } else if (currLine[0] == '<') {
    qCh = '>'; ++args;
    system = 1;
  } else {
    qCh = 0;
    system = 0;
  }

  fn = parseStr(&args, qCh, NULL); // i don't care about string length here
  int allowskip = 0;
  if (fn[0] == '?') { allowskip = 1; ++fn; while (isSpace(*fn)) ++fn; }
  if (!fn[0]) fatal("DATABIN: empty file name");
  char *fnameup = strdup(fn);
  memmove(currLine, args, strlen(args)+1);

  // maxlen
  if (currLine[0] == ',') {
    int defined = 1;
    (void)eatComma();
    maxlen = getExprArg(&defined, NULL);
    if (!defined) fatal("DATABIN: undefined length");
    if (maxlen < 1) { free(fnameup); return 1; } // nothing to do
  }

  // offset (negative: from the end)
  if (currLine[0] == ',') {
    int defined = 1;
    (void)eatComma();
    offset = getExprArg(&defined, NULL);
    if (!defined) fatal("DATABIN: undefined offset");
  }

  // code address
  if (currLine[0] == ',') {
    int defined = 1;
    (void)eatComma();
    codeaddr = getOneExprArg(&defined, NULL);
    if (!defined) fatal("DATABIN: undefined codeaddr");
    hasstart = 1;
  } else {
    if (currLine[0] && currLine[0] != ':') fatal("too many expressions");
  }

  // find and extract "disk name"
  char *diskname;

  char *pipeptr = strchr(fnameup, '|');
  if (pipeptr) {
    if (!pipeptr[1]) fatal("empty data file output name");
    diskname = strdup(pipeptr+1);
    *pipeptr = 0;
  } else {
    // build output name from disk name
    char *slp = strrchr(fnameup, '/');
    #ifdef WIN32
    char *slp1 = strrchr(fnameup, '\\');
    if (slp1 && (!slp || slp1 > slp)) slp = slp1;
    #endif
    if (slp) {
      ++slp;
      if (!slp[0]) fatal("empty data file output name");
      diskname = strdup(slp);
    } else {
      diskname = strdup(fnameup);
    }
  }

  // now fix the name
  char *fname = createIncludeName(fnameup, system, NULL, NULL);
  free(fnameup);

  OutDataFile *off = appendDataFile(fname, offset, maxlen, allowskip);

  if (off) {
    off->type = 'C';
    off->name = diskname;
    off->start = codeaddr&0xffffU;
    off->hasstart = (hasstart || ascode);
    //for (char *s = off->name; *s; ++s) *s = toUpper(*s);
    printf("data file: %s (start=%u; size=%u; std=%d)\n", off->name, off->start, off->size, off->hasstart);
  }

  free(fname);
  return PI_SKIP_LINE;
}


static int piDATABIN (void) { return piDATABINcommon(0); }
static int piCODEBIN (void) { return piDATABINcommon(1); }


///////////////////////////////////////////////////////////////////////////////
// INCLUDE
//
/* INCLUDE "name" */
static int piINCLUDE (void) {
  int system = 0;
  char *fn, qCh;
  char *args = currLine;
  if (!currLine[0]) fatal("INCLUDE without file name");
  if (isStrArg()) {
    qCh = *args++;
    system = 0;
  } else if (currLine[0] == '<') {
    qCh = '>'; ++args;
    system = 1;
  } else {
    qCh = 0;
    system = 0;
  }
  fn = parseStr(&args, qCh, NULL); // i don't care about string length here
  // flags
  int softinclude = 0;
  if (fn[0] == '?') { softinclude = 1; ++fn; while (isSpace(*fn)) ++fn; }
  if (!fn[0]) fatal("INCLUDE: empty file name");
  // load
  if (asmTextInclude(fn, system, softinclude) != 0) {
    if (!softinclude) fatal("INCLUDE: some shit happens!");
  }
  return PI_SKIP_LINE;
}


///////////////////////////////////////////////////////////////////////////////
// MODULE, ENDMODULE
//
static int piENDMODULE (void) {
  if (!currModule) fatal("ENDMODULE without MODULE");
  const char *mn = NULL;
  if (currLine[0]) mn = getOneLabelArg();
  moduleClose(mn);
  return PI_SKIP_LINE;
}


static int piMODULE (void) {
  ModuleInfo *mi;
  char *mn;
  SourceLine *ol = currSrcLine;
  int inum;
  if (currModule) fatal("no nested modules allowed");
  mn = getOneLabelArg();
  if (!urasm_is_valid_name(mn)) fatal("invalid module name: %s", mn);
  mi = moduleFind(mn);
  //fprintf(stderr, "+++MODULE: [%s] %p (seen=%d)\n", mn, mi, (mi ? mi->seen : -1));
  if (mi) {
    if (mi->seen) {
      if (strcmp(mi->fname, currSrcLine->fname)) {
        fatal("duplicate module definition; previous was in %s", mi->fname);
      }
      /* skip module */
      nextSrcLine(); /* skip "MODULE" line */
      if (!setCurSrcLine(findNextInstructionFromList(&inum, "MODULE", "ENDMODULE", NULL))) {
        setCurSrcLine(ol);
        fatal("no ENDMODULE");
      }
      if (inum == 0) fatal("no nested modules allowed");
      currModule = mi;
      moduleOpen();
      //skipInstruction(); //k8:wtf?!
      return piENDMODULE();
    }
  } else {
    mi = moduleAdd(mn, currSrcLine->fname);
  }
  mi->seen = 1;
  currModule = mi;
  moduleOpen();
  return PI_SKIP_LINE;
}


/* Z80, Z80N */
static int piMODEL (void) {
  char *mn = getOneIdArgLo();
  if (strSkipSpaces(currLine)[0]) fatal("only one model name expected");
       if (strcmp(mn, "z80") == 0) urasm_allow_zxnext = 0;
  else if (strcmp(mn, "z80a") == 0) urasm_allow_zxnext = 0;
  else if (strcmp(mn, "z80n") == 0) urasm_allow_zxnext = 1;
  else if (strcmp(mn, "z80next") == 0) urasm_allow_zxnext = 1;
  else if (strcmp(mn, "zxnext") == 0) urasm_allow_zxnext = 1;
  else fatal("invalid model name: %s", mn);
  return PI_SKIP_LINE;
}


////////////////////////////////////////////////////////////////////////////////
// $SAVECODE "filename",addr,size [,ldaddr [,options...]]
//
// options:
//   wipe -- wipe saved area (mark it as unused)
//
static int piSAVECODE (void) {
  //if (pass != 1) return PI_SKIP_LINE;
  //char *fn = parseStr(&args, qCh, NULL); // i don't care about string length here
  //matchDelim(',');

  int optWipe = 0, optData = 0;
  char *fn, qCh;
  char *args = currLine;

  if (!currLine[0]) fatal("SAVECODE without file name");

  if (isStrArg()) {
    qCh = *args++;
  } else if (currLine[0] == '<') {
    qCh = '>'; ++args;
  } else {
    qCh = 0;
  }

  fn = parseStr(&args, qCh, NULL); // i don't care about string length here
  if (!fn[0]) fatal("DATABIN: empty file name");

  char *fname = strdup(fn);
  memmove(currLine, args, strlen(args)+1);

  // addr
  matchDelim(',');
  int defined = 1;
  int staddr = getExprArg(&defined, NULL);
  if (!defined) fatal("SAVECODE: undefined start address");
  if (staddr < 0 || staddr > 65535) fatal("SAVECODE: invalid start address (%d)", staddr);

  // size
  matchDelim(',');
  defined = 1;
  int size = getExprArg(&defined, NULL);
  if (!defined) fatal("SAVECODE: undefined size");
  if (size < 0 || size > 65535 || staddr+size > 65536) fatal("SAVECODE: invalid size");

  // load address
  int ldaddr = staddr;
  if (checkDelim(',')) {
    defined = 1;
    ldaddr = getExprArg(&defined, NULL);
    if (!defined) fatal("SAVECODE: undefined load address");
    if (ldaddr < 0 || ldaddr > 65535) fatal("SAVECODE: invalid load address");
  }

  // parse options
  while (checkDelim(',')) {
    char *nstr = getLabelArg(0/*checkdelim*/);
    if (strEquCI(nstr, "wipe")) { optWipe = 1; continue; }
    if (strEquCI(nstr, "data")) { optData = 1; continue; }
    fatal("SAVECODE: unknown option '%s'", nstr);
  }
  if (!isLineEnd()) fatal("SAVECODE: unknown extra args");

  // save, if we are on the second pass
  if (pass == 1 && size > 0) {
    OutDataFile *off = appendDataData(memory+(unsigned)staddr, (unsigned)staddr, (unsigned)size);
    if (!off) fatal("SAVECODE: out of memory");
    off->name = fname;
    off->hasstart = !optData;
    fname = NULL;
  }

  if (optWipe) memset(memused+staddr, 0, size);

  if (fname) free(fname);
  return PI_SKIP_LINE;
}


////////////////////////////////////////////////////////////////////////////////
// STRUCT
//
static int piENDS (void) {
  fatal("ENDS without STRUCT");
  checkOperatorEnd();
  return PI_SKIP_LINE;
}


// this will be registered for each new struct
// use `urCurrentOp` to check what structure we are creating
// `currSeenLabel` contains current seen label
static int piSTRUCT_Internal (void) {
  StructInfo *sth = urCurrentOp->udata;

  // reserve room, set default values, add labels
  uint16_t origPC = pc;
  uint16_t origDisp = disp;

  pc += sth->size;
  disp += sth->size;

  if (sth->zerofill) {
    for (uint16_t f = 0; f < sth->size; ++f) {
      putByte(origPC+f, 0);
    }
  }

  for (StructField *fld = sth->fields; fld; fld = fld->next) {
    if (fld->hasInit) {
      if (fld->size == 1) {
        const uint8_t v = (fld->initValue < 0 ? 0x100+fld->initValue : fld->initValue)&0xff;
        putByte(origPC+fld->ofs, v);
      } else if (fld->size == 2) {
        const uint16_t v = (fld->initValue < 0 ? 0x10000+fld->initValue : fld->initValue)&0xffff;
        putWord(origPC+fld->ofs, v);
      } else if (fld->size == 4) {
        uint32_t v = 0;
        memcpy(&v, &fld->initValue, 4);
        putWord(origPC+fld->ofs, v&0xffff);
        putWord(origPC+fld->ofs+2, (v>>16)&0xffff);
      } else {
        fatal("STRUCT `%s`: trying to init some strangely-sized field", sth->name);
      }
    }

    if (pass) continue;

    //fprintf(stderr, "LBL: <%s>\n", currSeenLabel);
    // need to create a new label?
    if (!currSeenLabel[0] || !fld->name || !fld->name[0]) continue;

    char *nn = mangleLabelName(currSeenLabel);
    char *lbn = malloc(strlen(nn)+strlen(fld->name)+64);
    sprintf(lbn, "%s.%s", nn, fld->name);

    UrLabelInfo *c = urFindLabel(lbn);
    if (!c) c = urAddLabel(lbn);
    free(lbn);

    c->type = LBL_TYPE_STOFS;
    c->value = origDisp+fld->ofs;
    c->known = 1;
  }

  // create sizeof?
  if (currSeenLabel[0]) {
    char *nn = mangleLabelName(currSeenLabel);
    char *lbn = malloc(strlen(nn)+strlen("_sizeof")+64);
    sprintf(lbn, "%s.%s", nn, "_sizeof");

    UrLabelInfo *c = urFindLabel(lbn);
    if (!c) c = urAddLabel(lbn);
    free(lbn);

    c->type = LBL_TYPE_STOFS;
    c->value = sth->size;
    c->known = 1;
  }

  // now parse initial values, if there are any
  if (!isLineEnd()) {
    const int isCurly = checkDelim('{');
    SourceLine *stline = currSrcLine;

    int wasNewLine = 1; // don't require a comma
    for (;;) {
      if (!currSrcLine) {
        if (isCurly) {
          setCurSrcLine(stline);
          fatal("STRUCT: no closing curly bracket");
        }
        break;
      }
      if (isLineEnd()) {
        if (!isCurly) break;
        // load next line
        nextSrcLine();
        wasNewLine = 1;
        continue;
      }

      if (isCurly && checkDelim('}')) {
        // we're done
        if (!isLineEnd()) fatal("STRUCT: closing curly bracket must be alone on the line");
        break;
      }

      if (wasNewLine) {
        wasNewLine = 0;
      } else {
        matchDelim(',');
      }

      char *nstr = getLabelArg(0/*checkdelim*/);

      // find field
      StructField *fld = sth->fields;
      for (; fld; fld = fld->next) if (fld->name && strcmp(fld->name, nstr) == 0) break;
      if (!fld) fatal("unknown field `%s` in struct `%s`", nstr, sth->name);

      // only `=` is allowed
      matchDelim('=');

      int defined = 1;
      int32_t ival = getExprArg(&defined, NULL);

      if (fld->size == 1) {
        if (defined && (ival < -128 || ival > 255)) {
          fatal("STRUCT: field `%s` has inivalid initial value: %d",
                (fld->name ? fld->name : "<anonymous>"), ival);
        }
        if (ival < -128) ival = -128;
        const uint8_t v = (ival < 0 ? 0x100+ival : ival)&0xff;
        putByte(origPC+fld->ofs, v);
      } else if (fld->size == 2) {
        if (defined && (ival < -32768 || ival > 65535)) {
          fatal("STRUCT: field `%s` has inivalid initial value: %d",
                (fld->name ? fld->name : "<anonymous>"), ival);
        }
        if (ival < -32768) ival = -32768;
        const uint16_t v = (ival < 0 ? 0x10000+ival : ival)&0xffff;
        putWord(origPC+fld->ofs, v);
      } else if (fld->size == 4) {
        uint32_t v = 0;
        memcpy(&v, &ival, 4);
        putWord(origPC+fld->ofs, v&0xffff);
        putWord(origPC+fld->ofs+2, (v>>16)&0xffff);
      } else {
        fatal("STRUCT `%s`: trying to init some strangely-sized field", sth->name);
      }
    }
  }

  return PI_SKIP_LINE;
}


static int structCheckSizeDecl (const char *s) {
  if (strEquCI(s, "byte")) return 1;
  if (strEquCI(s, "word")) return 2;
  if (strEquCI(s, "address")) return 2;
  if (strEquCI(s, "dword")) return 4;
  if (strEquCI(s, "label")) return 0;
  if (strEquCI(s, "defs")) return -666;
  if (strEquCI(s, "ds")) return -666;
  return -1;
}


// STRUCT name [,init_offset]
static int piSTRUCT (void) {
  int zerofill = 0, extend = 0;
  int defined = 1, inum;
  SourceLine *stline;

  //fprintf(stderr, "PASS=%d\n", pass);
  if (pass) {
    stline = currSrcLine;
    nextSrcLine(); // skip ourself
    if (!setCurSrcLine(findNextInstructionFromList(&inum, "ENDS", NULL))) {
      setCurSrcLine(stline);
      fatal("STRUCT: no ENDS");
    }
    return PI_SKIP_LINE;
  }

  /*
  if (checkDelim('*')) extend = 1;
  else if (checkDelim('!')) extend = 2; // compatibility with the old code
  else if (checkDelim('+')) extend = 2;
  */

  // flags: `[flag, flag...]`
  // currently, only `zero_fill` flag is defined
  if (checkDelim('[')) {
    while (!checkDelim(']')) {
      if (zerofill == 0 && checkIdentCI("zero_fill")) zerofill = 1;
      else fatal("invalid structure flag");
      if (!checkDelim(',')) {
        if (!checkDelim(']')) fatal("invalid structure flag list");
        break;
      }
    }
  }

  // if `extend` is before a struct name, we are extending it without inheriting
  if (checkIdentCI("extend") || checkIdentCI("extends")) extend = 1;

  // struct name
  char stname[128], estname[128];
  stname[0] = 0;
  char *nstr = getLabelArg(0/*checkdelim*/);
  if (!nstr || !nstr[0]) fatal("structure name expected");
  if (strlen(nstr) > 127) fatal("structure name too long");
  if (/*urFindOp(nstr) ||*/ !urasm_is_valid_name(nstr)) {
    fatal("structure name `%s` is invalid", nstr);
  }
  strcpy(stname, nstr);

  StructInfo *sth = NULL;
  if (extend == 0) {
    for (sth = structList; sth; sth = sth->next) {
      if (strcmp(sth->name, stname) == 0) {
        fatal("duplicate struct name `%s`!", stname);
      }
    }
    if (urFindOp(stname)) fatal("invalid struct name `%s`!", stname);
  }

  if (extend || checkIdentCI("extend") || checkIdentCI("extends")) {
    if (extend == 0) {
      // inherit
      extend = 2;
      nstr = getLabelArg(0/*checkdelim*/);
      if (!nstr || !nstr[0]) fatal("parent structure name expected");
      if (strlen(nstr) > 127) fatal("parent structure name too long");
      if (/*urFindOp(nstr) ||*/ !urasm_is_valid_name(nstr)) {
        fatal("structure name `%s` is invalid", nstr);
      }
      strcpy(estname, nstr);
      for (sth = structList; sth; sth = sth->next) {
        if (strcmp(sth->name, estname) == 0) break;
      }
      if (!sth) fatal("STRUCT: cannot extend unknown structure `%s`", estname);
    } else {
      // extend
      for (sth = structList; sth; sth = sth->next) {
        if (strcmp(sth->name, stname) == 0) break;
      }
      if (!sth) fatal("STRUCT: cannot extend unknown structure `%s`", stname);
    }
    // inherit?
    if (extend == 2) {
      StructInfo *stnew = calloc(1, sizeof(StructInfo));
      stnew->name = strdup(stname);
      stnew->size = sth->size;
      stnew->zerofill = (sth->zerofill || zerofill);

      // copy fields
      StructField *xlast = NULL;
      for (StructField *fld = sth->fields; fld; fld = fld->next) {
        StructField *nf = calloc(1, sizeof(StructField));
        nf->name = (fld->name ? strdup(fld->name) : NULL);
        nf->ofs = fld->ofs;
        nf->size = fld->size;
        nf->initValue = fld->initValue;
        nf->hasInit = fld->hasInit;
        if (xlast) xlast->next = nf; else stnew->fields = nf;
        xlast = nf;
      }

      sth = stnew;
    }
  } else {
    // allocate struct header
    sth = calloc(1, sizeof(StructInfo));
    sth->name = strdup(stname);
    sth->zerofill = zerofill;
  }

  int32_t currofs = (sth ? sth->size : 0);
  // parse initial offset
  if (checkDelim(',')) {
    defined = 1;
    currofs = getExprArg(&defined, NULL);
    if (!defined) fatal("STRUCT: initial offset must be defined");
    if (currofs < 0 || currofs > 65535) fatal("STRUCT: invalid initial offset");
  }

  // done with args
  if (!isLineEnd()) fatal("too many arguments for STRUCT");

  // for fast appending
  StructField *fldlast = (sth ? sth->fields : NULL);
  StructField *fldnew = NULL;

  if (fldlast) {
    while (fldlast->next) fldlast = fldlast->next;
  }

  if (extend == 2) fldnew = sth->fields;

  // parse until ENDS
  stline = currSrcLine;
  nextSrcLine(); // skip ourself
  //fprintf(stderr, "001: <%s>\n", currLine);
  inum = -1;
  while (currSrcLine) {
    size_t slen = strlen(currLine);
    currLineRemoveComment();
    currLineTrimBlanks();
    if (!currLine[0]) { nextSrcLine(); continue; }

    if (strIsCommand("ENDS", currSrcLine->line)) break;
    //fprintf(stderr, "112: <%s> (%d)\n", currLine, inum);

    // parse new field
    int skipInit = 0;
    char *fldname = getLabelArg(0/*checkdelim*/);
    int fldsize = structCheckSizeDecl(fldname);
    if (fldsize == -666) {
      // defs/ds
      defined = 1;
      fldsize = getExprArg(&defined, NULL);
      if (!defined) fatal("STRUCT: DEFS size must be defined");
      if (fldsize < 0 || fldsize > 65535) fatal("STRUCT: DEFS size is invalid");
      skipInit = 1;
      fldname = NULL;
    } else if (fldsize < 0) {
      // this must be a label
      int found = 0;
      for (StructField *cf = sth->fields; cf; cf = cf->next) {
        if (cf->name && cf->name[0] && strcmp(cf->name, fldname) == 0) {
          found = 1;
          break;
        }
      }
      if (found) fatal("duplicate field name '%s'", fldname);
      fldname = strdup(fldname);
      char *szdecl = getLabelArg(0/*checkdelim*/);
      fldsize = structCheckSizeDecl(szdecl);
      if (fldsize == -666) {
        // defs/ds
        defined = 1;
        fldsize = getExprArg(&defined, NULL);
        if (!defined) fatal("STRUCT: DEFS size must be defined");
        if (fldsize < 0 || fldsize > 65535) fatal("STRUCT: DEFS size is invalid");
        skipInit = 1;
      } else {
        if (fldsize < 0) fatal("field size definition expected");
      }
    } else {
      fldname = NULL; // no name
    }

    // check for init value
    int hasInit = 0;
    int32_t initval = 0;

    if (!skipInit && checkDelim('=')) {
      defined = 1;
      initval = getExprArg(&defined, NULL);
      if (!defined) fatal("STRUCT: initial value must be defined");
      hasInit = 1;
      if (fldsize == 0 ||
          (fldsize == 1 && (initval < -128 || initval > 255)) ||
          (fldsize == 2 && (initval < -32768 || initval > 65535)))
      {
        fatal("STRUCT: initial value is out of range");
      }
    }

    if (!isLineEnd()) fatal("STRUCT: extra field data");

    if (currofs+fldsize > 65535) fatal("STRUCT: too big");

    // create field
    StructField *fld = calloc(1, sizeof(StructField));
    fld->name = fldname;
    fld->ofs = currofs;
    fld->size = fldsize;
    fld->hasInit = hasInit;
    fld->initValue = initval;
    if (fldlast) fldlast->next = fld; else sth->fields = fld;
    fldlast = fld;
    currofs += fldsize;

    if (!fldnew) fldnew = fld;

    #if 0
    fprintf(stderr, "FLD <%s>: ofs=%u; size=%u; hasinit=%d; init=%d\n", fld->name, fld->ofs,
            fld->size, fld->hasInit, fld->initValue);
    #endif

    nextSrcLine();
  }
  if (!currSrcLine) { setCurSrcLine(stline); fatal("no ENDS"); }
  if (currofs > 65535) fatal("STRUCT: too big");
  sth->size = currofs;

  //FIXME: structs must be local to module!

  // register new struct
  if (extend == 0 || extend == 2) {
    StructInfo *slast = structList;
    if (slast) {
      while (slast->next) slast = slast->next;
      slast->next = sth;
    } else {
      structList = sth;
    }
    UrAsmOp *op = urAddOp(sth->name, &piSTRUCT_Internal);
    op->udata = sth;
  }

  // add struct labels
  for (StructField *fld = fldnew; fld; fld = fld->next) {
    if (!fld->name || !fld->name[0]) continue;

    char *nn = mangleLabelName(sth->name);
    char *lbn = malloc(strlen(nn)+strlen(fld->name)+64);
    sprintf(lbn, "%s.%s", nn, fld->name);

    //fprintf(stderr, "%d: <%s>\n", extend, lbn);

    UrLabelInfo *c = urFindLabel(lbn);
    if (c) fatal("STRUCT: field name '%s' conflict!", fld->name);

    c = urAddLabel(lbn);
    free(lbn);
    c->type = LBL_TYPE_STOFS;
    c->value = fld->ofs;
    c->known = 1;
  }

  // add `structname.sizeof`
  {
    char *nn = mangleLabelName(sth->name);
    char *lbn = malloc(strlen(nn)+64);
    sprintf(lbn, "%s.%s", nn, "_sizeof");

    UrLabelInfo *c = urFindLabel(lbn);
    if (c) {
      if (!extend) fatal("STRUCT: field name '%s' conflict!", "_sizeof");
    } else {
      c = urAddLabel(lbn);
    }
    free(lbn);

    c->type = LBL_TYPE_STOFS;
    c->value = sth->size;
    c->known = 1;
  }

  return PI_SKIP_LINE;
}


// ////////////////////////////////////////////////////////////////////////// //
static int piEND_PASMO (void) {
  int defined = 1;
  int32_t res = getOneExprArg(&defined, NULL);
  //if (!defined) fatal("sorry, ENT operand value must be known here");
  if (res < 0 || res > 65535) fatal("invalid END operand value: %d", res);
  ent = res;
  return PI_CONT_LINE;
}


///////////////////////////////////////////////////////////////////////////////
// DUP, EDUP
//
static int piEDUP (void) {
  fatal("EDUP without DUP");
  checkOperatorEnd();
  return PI_SKIP_LINE;
}


static int piENDR (void) {
  fatal("ENDR without REPT");
  checkOperatorEnd();
  return PI_SKIP_LINE;
}


// DUP count [,var [,initval [,incr]]]
static int piDUPEx (int isrept) {
  int defined = 1, inum;
  SourceLine *stline, *eline = NULL;

  //int32_t cnt = getOneExprArg(&defined, NULL);
  int32_t cnt = getExprArg(&defined, NULL);
  if (!defined) fatal("DUP: counter must be defined");
  if (cnt > 65535) fatal("DUP: counter too big: %d", cnt);
  if (cnt < 0) warningMsg("DUP: counter too small: %d", cnt);

  int32_t initval = 0;
  int32_t incr = 1;

  // variable name
  char vname[128];
  vname[0] = 0;
  if (currLine[0] == ',') {
    (void)eatComma();
    char *lbl = getLabelArg(0/*checkdelim*/);
    if (lbl[0]) {
      if (strlen(lbl) > 127) fatal("DUP counter variable name too long");
      if (urFindOp(lbl) || !urasm_is_valid_name(lbl)) fatal("DUP counter variable name is invalid");
      strcpy(vname, lbl);
    }
  }

  // initial variable value
  if (currLine[0] == ',') {
    (void)eatComma();
    defined = 1;
    initval = getExprArg(&defined, NULL);
    if (!defined) fatal("DUP: initial value must be defined");
  }

  // increment
  if (currLine[0] == ',') {
    (void)eatComma();
    defined = 1;
    incr = getExprArg(&defined, NULL);
    if (!defined) fatal("DUP: increment value must be defined");
  }

  if (currLine[0]) fatal("too many arguments for DUP");

  int dupstack[128];
  int dupsp = 0;

  // now find corresponding EDUP
  // note that we should skip nested DUPs
  nextSrcLine(); // skip ourself
  stline = currSrcLine;
  while (currSrcLine) {
    if (!setCurSrcLine(findNextInstructionFromList(&inum, "DUP", "REPT", "EDUP", "ENDR", "ENDM", NULL))) break;
    if (inum == 0 || inum == 1) {
      if (dupsp >= 128) fatal("too many nested DUPs");
      dupstack[dupsp++] = isrept;
      isrept = (inum == 1);
      nextSrcLine(); // skip DUP
    } else {
      // EDUP
      if (dupsp == 0) { eline = currSrcLine; break; }
      if (isrept) {
        if (inum < 3) fatal("invalid REPT end directive");
      } else {
        if (inum >= 3) fatal("invalid DUP end directive");
      }
      isrept = dupstack[--dupsp];
      nextSrcLine(); // skip EDUP
    }
  }
  if (!eline) { setCurSrcLine(stline); fatal("no EDUP"); }

  // create counter local, if necessary
  UrLabelInfo *lcnt = (vname[0] ? urAddTempLocal(vname) : NULL);
  if (lcnt) lcnt->value = initval;

  // repeat lines
  while (cnt-- > 0) {
    setCurSrcLine(stline);
    while (currSrcLine != eline) processCurrentLine();
    // increment counter
    if (lcnt) lcnt->value += incr;
  }

  // remove counter
  urRemoveTempLocal(lcnt);

  return PI_SKIP_LINE;
}


static int piDUP (void) { return piDUPEx(0); }
static int piREPT (void) { return piDUPEx(1); }


///////////////////////////////////////////////////////////////////////////////
// IF, ENDIF
//
static int ifCount = 0;


// results:
//  -1: error (should not happen)
//   0: successfully complete
//   1: stopped *AT* "ELSE"
//   2: stopped *AT* "ELSIF"
static int ifSkipToEndIfOrElse (int wholeBody) {
  int inum, wasElse = 0;
  SourceLine *oline;
  //
  while (currSrcLine) {
    if (!setCurSrcLine(findNextInstructionFromList(&inum, "IF", "ELSE", "ENDIF",
                                                   "ELSEIF", "MACRO", "ENDM", "IFX",
                                                   "ELSEIFX",
                                                   "ELSIF", "ELSIFX",
                                                   NULL))) break;
    switch (inum) {
      case 0: /* if */
      case 6: /* ifx */
        nextSrcLine(); // skip IF
        ifSkipToEndIfOrElse(1); // and recurse
        nextSrcLine(); // skip ENDIF
        break;
      case 1: /* else */
        if (wasElse) fatal("duplicate ELSE");
        if (!wholeBody) return 1;
        wasElse = 1;
        nextSrcLine(); // skip ELSE
        break; // and continue
      case 2: /* endif */
        return 0; // and exit
      case 3: /* elseif */
      case 7: /* elseifx */
      case 8: /* elsif */
      case 9: /* elsifx */
        if (wasElse) fatal("ELSIF in ELSE");
        if (!wholeBody) return 2;
        nextSrcLine(); // skip ELSIF
        break; // and continue
      case 4: /* macro */
        // skip it as a whole
        nextSrcLine(); // skip MACRO
        for (;;) {
          oline = currSrcLine;
          if (!setCurSrcLine(findNextInstructionFromList(&inum, "MACRO", "ENDM", NULL))) { setCurSrcLine(oline); fatal("invalid MACRO"); }
          if (inum == 1) break;
          fatal("invalid nested MACRO");
        }
        nextSrcLine(); // skip ENDM
        break;
      case 5: /* endm */
        fatal("unexpected ENDM");
    }
  }
  fatal("IF without ENDIF");
  return -1;
}


static int piENDIF (void) {
  /*!fprintf(stderr, "ENDIF(%d): LINE <%s> (%d)\n", ifCount, curLine, (currSrcLine ? currSrcLine->lineNo : 0));*/
  if (--ifCount < 0) fatal("ENDIF without IF");
  checkOperatorEnd();
  return PI_SKIP_LINE;
}


static int piELSE (void) {
  if (--ifCount < 0) fatal("ELSE without IF");
  nextSrcLine(); // skip ELSE
  ifSkipToEndIfOrElse(1);
  return PI_SKIP_LINE;
}

static int piELSIF (void) {
  if (--ifCount < 0) fatal("ELSIF without IF");
  nextSrcLine(); // skip ELSIF
  ifSkipToEndIfOrElse(1);
  return PI_SKIP_LINE;
}


static int piELSIFX (void) {
  if (--ifCount < 0) fatal("ELSIFX without IF");
  nextSrcLine(); // skip ELSIFX
  ifSkipToEndIfOrElse(1);
  return PI_SKIP_LINE;
}


static int piIFAll (int isIfX) {
  int defined = 1;
  int ooo = lblOptMakeU2;

  //fprintf(stderr, "piIFALL: <%s>\n", currLine);
  lblOptMakeU2 = (isIfX ? 1 : 0);
  int32_t cond = getOneExprArg(&defined, NULL);
  lblOptMakeU2 = ooo;
  //fprintf(stderr, "piIFALL COND: <%d>\n", cond);

  if (!defined) {
    if (!isIfX) fatal("IF: condition must be defined");
    cond = 0; // for IFX: 0 if there is any undefined label
  }
  if (cond) {
    // ok, do it until ELSE/ELSIF/ENDIF
    ++ifCount;
    return PI_SKIP_LINE;
  }
  for (;;) {
    int r;
    char *args;
    //
    nextSrcLine(); // skip last instruction
    // skip until ELSE/ELSIF/ENDIF
    r = ifSkipToEndIfOrElse(0);
    /*!fprintf(stderr, "ELSIF(%d): 000: LINE <%s> (%d)\n", r, curLine, (currSrcLine ? currSrcLine->lineNo : 0));*/
    if (r == 0) break; // ENDIF
    if (r == 1) { ++ifCount; break; } // ELSE
    // ELSIF, do condition
    args = strIsCommand("ELSIF", currLine);
    if (args == NULL) args = strIsCommand("ELSEIF", currLine);
    if (args != NULL) {
      isIfX = 0;
    } else {
      args = strIsCommand("ELSIFX", currLine);
      if (args == NULL) args = strIsCommand("ELSEIFX", currLine);
      if (args == NULL) fatal("internal error in conditionals"); // the thing that should not be
      isIfX = 1;
    }
    memmove(currLine, args, strlen(args)+1);
    /*!fprintf(stderr, "ELSIF(%d): 001: LINE <%s> (%d)\n", r, curLine, (currSrcLine ? currSrcLine->lineNo : 0));*/
    cond = getOneExprArg(&defined, NULL);
    /*!fprintf(stderr, "ELSIF(%d): 002: LINE <%s> (%d); cond=%d\n", r, curLine, (currSrcLine ? currSrcLine->lineNo : 0), cond);*/
    if (!defined) {
      if (!isIfX) fatal("ELSIF: condition must be defined");
      cond = 0; // for IFX: 0 if there is any undefined label
    }
    if (cond) { ++ifCount; break; } // condition is true
  }
  return PI_SKIP_LINE;
}


static int piIF (void) { return piIFAll(0); }
static int piIFX (void) { return piIFAll(1); }


///////////////////////////////////////////////////////////////////////////////
// macro processor
///////////////////////////////////////////////////////////////////////////////
/*
 what i did with MACRO is the brain-damaged cheating all the way.

 first, i will collect the MACRO body and remember it (removing it
 from the main source code, so second pass will not see it).

 second, when the macro is used, i will:
   * insert the whole macro body in place (label resolver will
     fix "..lbl" labels for us)
   * let the asm play with it

 this is not the best scheme, but it is fairly simple and it works.
*/

// will be called when parser encounters term starting with '=' or '*'
// first term char will not be skipped
// must return pointer to the first char after expression end
static const char *getValueCB (urasm_exprval_t *res, const char *expr, uint16_t addr, int donteval, int *defined, int *error) {
  char name[257];
  char *p = name;

  if (curmacro == NULL) fatal("'=' outside of macro");
  if (*expr++ != '=') fatal("'=' expected!");

  expr = strSkipSpaces(expr);
  while (*expr) {
    if (isAlphaDigit(*expr) || *expr == '_') {
      if (p-name > 250) fatal("id too long");
      *p++ = *expr++;
      continue;
    }
    break;
  }
  *p = 0;

  expr = strSkipSpaces(expr);
  for (int f = 0; f < curmacro->mac->argc; ++f) {
    if (strEquCI(name, curmacro->mac->argnames[f])) {
      if (*expr == '[') {
        urasm_exprval_t v;
        int l = (int)strlen(curmacro->argvals[f]);
        ++expr; // skip "["
        urasm_exprval_init(&v);
        expr = urasm_expr_ex(&v, expr, addr, &donteval, defined, error);
        if (*error) return expr;
        if (expr == NULL || *expr != ']' || v.str != NULL) { *error = UR_EXPRERR_FUNC; return expr; }
        ++expr;
        if (v.val < 0) v.val += l;
        if (v.val < 0 || v.val >= l) {
          res->val = '?';
        } else {
          res->val = (unsigned char)(curmacro->argvals[f][v.val]);
        }
        return expr;
      } else {
        urasm_expr_ex(res, curmacro->argvals[f], addr, &donteval, defined, error);
        return expr;
      }
    }
  }

  fatal("unknown macro variable: '%s'", name);
}


//!0: error; oprlen is opr size (w/o ending 0), it's 255 for now
// opr starts with '=' (invariant)
static int expandCB (char *opr, int oprlen) {
  char name[257], *p = name, *op = opr;

  if (curmacro == NULL) fatal("'=' outside of macro");
  if (*op++ != '=') fatal("'=' expected!"); // just in case
  //fprintf(stderr, "expand: [%s]\n", opr);

  if (!isAlpha(op[0])) return 0; // nothing to do

  // copy argument name
  while (*op) {
    if (isAlphaDigit(*op) || *op == '_') {
      if (p-name > 250) fatal("id too long");
      *p++ = *op++;
      continue;
    }
    break;
  }
  *p = 0;

  // expand argument? we only need to expand `=arg[n]`
  op = strSkipSpaces(op);
  if (op[0] != '[') return 0;

  for (int f = 0; f < curmacro->mac->argc; ++f) {
    if (strEquCI(name, curmacro->mac->argnames[f])) {
      ur_assert(*op == '[');
      // replace argument with character, or with literal value
      // used for `=regpair[0]` or `=regpair[]`
      if (op[1] == ']') {
        op += 2;
        const int l = (int)strlen(curmacro->argvals[f]);
        const int lleft = (int)strlen(op);
        char *tmp = malloc(l+lleft+8);
        snprintf(tmp, l+lleft+8, "%s%s", curmacro->argvals[f], op);
        if ((int)strlen(tmp) > oprlen) {
          free(tmp);
          return -1;
        }
        strcpy(opr, tmp);
        free(tmp);
        return 0;
      } else {
        urasm_exprval_t v;
        int error = 0, defined = 1, donteval = 0;
        ++op; // skip '['
        urasm_exprval_init(&v);
        op = (char *)urasm_expr_ex(&v, op, pc, &donteval, &defined, &error);
        if (error) return -1;
        // result should be a number
        if (op == NULL || *op != ']' || v.str != NULL) return -1;
        ++op; // skip ']'
        // it is guaranteed to have more than one char in opr
        // so we can simply put char from argument value, and remove other expression chars
        const int l = (int)strlen(curmacro->argvals[f]);
        if (v.val < 0) v.val += l; // negative: indexing from the end
        if (v.val < 0 || v.val >= l) fatal("index %d is out of bounds for macro argument '%s'", v.val, curmacro->mac->argnames[f]);
        // copy char
        opr[0] = curmacro->argvals[f][v.val];
        // remove other chars
        memmove(opr+1, op, strlen(op)+1);
      }
      return 0;
    }
  }

  fatal("unknown macro variable: '%s'", name);
}


// main macro expander
static void processMacro (MacroDef *mc) {
  SourceLine *oldcurline = currSrcLine;
  CurMacroDef cm;
  memset(&cm, 0, sizeof(cm));
  //
  //fprintf(stderr, "processMacro: [%s] (%d)\n", mc->name, curmacronum);
  //for (SourceLine *l = mc->lines; l != NULL; l = l->next) fprintf(stderr, "*[%s]\n", l->line);
  //
  // parse macro arguments
  cm.mac = mc;
  for (unsigned f = 0; f < MAX_MACRO_ARGS; ++f) cm.argvals[f] = NULL;
  int currArg = 0;
  for (;;) {
    removeSpaces();
    // do we have more arguments?
    if (!currLine[0]) break;
    // check for argument name (this is purely cosmetic thing)
    // use like this: "mcall :argname=[value]" (value may be omited for default)
    if (currLine[0] == ':' && isAlpha(currLine[1])) {
      int pos = 2;
      while (isAlphaDigit(currLine[pos]) || currLine[pos] == '_') ++pos;
      //hack!
      const char svch = currLine[pos];
      currLine[pos] = 0;
      if (!strEquCI(currLine+1, mc->argnames[currArg])) {
        // out-of-order, find proper argument and rewind to it
        currArg = -1;
        for (int c = 0; c < mc->argc; ++c) {
          if (strEquCI(currLine+1, mc->argnames[c])) {
            currArg = c;
            break;
          }
        }
        if (currArg < 0) fatal("macro '%s' has no argument named '%s'", mc->name, currLine+1);
      }
      currLine[pos] = svch;
      // remove argument name
      memmove(currLine, currLine+pos, strlen(currLine+pos)+1);
      removeSpaces();
      // check for '='
      if (currLine[0] != '=') fatal("expected '=' after argument name");
      // remove '='
      memmove(currLine, currLine+1, strlen(currLine));
      removeSpaces();
    }
    // too many args?
    if (currArg >= mc->argc) fatal("too many arguments to macro '%s'", mc->name);
    // check for default value
    if (currLine[0] == ',') {
      if (mc->argdefaults[currArg] == NULL) fatal("macro '%s' has no default value for argument '%s'", mc->name, mc->argnames[currArg]);
      cm.argvals[currArg] = strdup(mc->argdefaults[currArg]);
      memmove(currLine, currLine+1, strlen(currLine));
    } else {
      // skip argument (so we will know its length, and will be able to copy it)
      char *e = skipMacroArg(currLine);
      //fprintf(stderr, "*[%s]\n*[%s]\n", curLine, e);
      const char ech = *e;
      if (ech != 0) {
        if (ech == ':') fatal("invalid invocation of macro '%s' (only one macro per line allowed)", mc->name);
        if (ech != ',') fatal("invalid invocation of macro '%s'", mc->name);
      }
      *e = 0;
      cm.argvals[currArg] = strdup(currLine);
      // strip trailing spaces
      strTrimRight(cm.argvals[currArg]);
      if (ech) {
        *e = ech;
        memmove(currLine, e+1, strlen(e));
      } else {
        currLine[0] = 0;
      }
    }
    ++currArg;
  }
  // check for line end
  removeSpaces();
  if (currLine[0]) fatal("invalid macro invocation");

  // setup default argument values
  for (int f = 0; f < mc->argc; ++f) {
    //fprintf(stderr, "MAC: %s; arg #%d (%s): <%s>\n", mc->name, f, mc->argnames[f], cm.argvals[f]);
    if (cm.argvals[f]) continue;
    if (mc->argdefaults[f] == NULL) fatal("macro '%s' has no default value for argument '%s'", mc->name, mc->argnames[f]);
    cm.argvals[f] = strdup(mc->argdefaults[f]);
    //fprintf(stderr, "  DEF arg #%d (%s): <%s>\n", f, mc->argnames[f], cm.argvals[f]);
  }

  //for (int f = 0; f < mc->argc; ++f) fprintf(stderr, "%d: [%s]\n", f, cm.argvals[f]);

  // insert macro code into the source
  setCurSrcLine(mc->lines);
  curmacro = &cm;
  ++curmacronum;
  while (currSrcLine != NULL) {
    //fprintf(stderr, "*[%s] (%d)\n", currSrcLine->line, currSrcLine->lineNo);
    processCurrentLine();
  }

  for (unsigned f = 0; f < MAX_MACRO_ARGS; ++f) if (cm.argvals[f]) free(cm.argvals[f]);
  setCurSrcLine(oldcurline);
  curmacro = NULL;
  nextSrcLine();
}


static int piMACRO (void) {
  char *name;
  int argc = 0;
  char *argdefaults[MAX_MACRO_ARGS];
  char *argnames[MAX_MACRO_ARGS];
  SourceLine *stline, *eline = NULL;
  MacroDef *mc;
  //
  name = strdup(getLabelArg(0));
  //fprintf(stderr, "[%s]\n", name);
  if (findMacro(name) != NULL) fatal("duplicate MACRO definition: '%s'", name);
  if (currLine[0] == ',') memmove(currLine, currLine+1, strlen(currLine));
  removeSpaces();
  //
  while (currLine[0]) {
    if (argc >= MAX_MACRO_ARGS) fatal("too many arguments in MACRO");
    if (!isAlpha(currLine[0])) fatal("invalid MACRO definition");
    argnames[argc] = strdup(getLabelArg(0));
    if (currLine[0] == '=') {
      // default value
      char *e = strchr(currLine, ','), tch;
      //
      if (e == NULL) e = currLine+strlen(currLine);
      tch = *e;
      *e = 0;
      argdefaults[argc] = strdup(currLine+1);
      *e = tch;
      memmove(currLine, e, strlen(e)+1);
    } else {
      argdefaults[argc] = NULL;
    }
    removeSpaces();
    if (currLine[0] == ',') {
      memmove(currLine, currLine+1, strlen(currLine));
      removeSpaces();
    }
    //fprintf(stderr, "%d: [%s] [%s]\n", argc, argnames[argc], argdefaults[argc]);
    ++argc;
  }
  //
  // now find corresponding ENDM
  // note that we should skip nested DUPs
  stline = currSrcLine;
  nextSrcLine(); // skip ourself
  while (currSrcLine) {
    int inum;
    //
    if (!setCurSrcLine(findNextInstructionFromList(&inum, "MACRO", "ENDM", NULL))) break;
    // ok, we found something; what is it?
    if (inum == 0) {
      // new MACRO
      fatal("no nested MACROs yet");
    } else {
      // ENDM, gotcha!
      eline = currSrcLine;
      // kill ENDM
      eline->line[0] = 0;
      nextSrcLine(); // skip ENDM
      break;
    }
  }
  if (!eline) { setCurSrcLine(stline); fatal("no ENDM"); }
  //
  if ((mc = calloc(1, sizeof(*mc))) == NULL) fatal("out of memory");
  mc->name = name;
  mc->argc = argc;
  for (int f = 0; f < argc; ++f) { mc->argdefaults[f] = argdefaults[f]; mc->argnames[f] = argnames[f]; }
  //
  eline->next = NULL;
  mc->lines = stline->next;
  stline->next = currSrcLine;
  stline->line[0] = 0;
  //
  mc->next = maclist;
  maclist = mc;
  setCurSrcLine(stline);
  return PI_SKIP_LINE;
}


static int piENDM (void) {
  fatal("ENDM without MACRO");
  return PI_SKIP_LINE;
}


///////////////////////////////////////////////////////////////////////////////
// line processor
static void piTapParseLoaderName (void) {
  if (eatComma()) {
    int len;
    if (!isStrArg()) fatal("loader name expected");
    char *fn = getStrArg(&len);
    if (len > 10) fatal("loader name too long");
    memset(tapeLoaderName, ' ', 10);
    for (int f = 0; f < len; ++f) tapeLoaderName[f] = fn[f];
  }
}


static int piMATHMODE (void) {
  char *name = getOneLabelArg();
       if (strEquCI(name, "OLD")) urasm_use_old_priorities = 1;
  else if (strEquCI(name, "NEW")) urasm_use_old_priorities = 0;
  else fatal("invalid math mode; NEW or OLD expected");
  return PI_SKIP_LINE;
}


static int piCASTRATES (void) {
  char *name = getOneLabelArg();
       if (strEquCI(name, "YES")) urasm_allow_hex_castrates = 1;
  else if (strEquCI(name, "TAN")) urasm_allow_hex_castrates = 1;
  else if (strEquCI(name, "NO")) urasm_allow_hex_castrates = 0;
  else if (strEquCI(name, "ONA")) urasm_allow_hex_castrates = 0;
  else fatal("yes/no value expected");
  return PI_SKIP_LINE;
}


static int piREFOPT (void) {
  char *name = getOneLabelArg();
       if (strEquCI(name, "ALLLABELS")) urasm_dump_all_labels = 1;
  else if (strEquCI(name, "ALL_LABELS")) urasm_dump_all_labels = 1;
  else if (strEquCI(name, "NOUPCASE")) urasm_dump_all_labels = 0;
  else if (strEquCI(name, "NO_UPCASE")) urasm_dump_all_labels = 0;
  else if (strEquCI(name, "DEFAULT")) urasm_dump_all_labels = 1;
  else fatal("invalid refopt mode");
  return PI_SKIP_LINE;
}


static int piDEFFMT (void) {
  char *name;

  if (isStrArg()) {
    int len = 0;
    name = getStrArg(&len);
  } else {
    name = getLabelArg(1);
  }
  if (optWTChanged) return 1;

  optRunDMB = optRunTape = optRunSCL = optRunDSK = 0;
  //optRunSNA = 0;

  if (strEquCI(name, "SNA") || strEquCI(name, "RUNSNA") || strEquCI(name, "SNARUN")) {
    optWriteType = 's';
    if (currLine[0]) fatal("too many expressions");
    return PI_SKIP_LINE;
  }
  if (strEquCI(name, "TAP") || strEquCI(name, "TAPE")) {
    optWriteType = 't';
    piTapParseLoaderName();
    return PI_SKIP_LINE;
  }
  if (strEquCI(name, "RUNTAP") || strEquCI(name, "RUNTAPE") || strEquCI(name, "TAPERUN")) {
    optRunTape = 1;
    optWriteType = 't';
    piTapParseLoaderName();
    return PI_SKIP_LINE;
  }
  if (strEquCI(name, "BIN") || strEquCI(name, "RAW")) {
    optWriteType = 'r';
    if (currLine[0]) fatal("too many expressions");
    return PI_SKIP_LINE;
  }
  if (strEquCI(name, "DMB") || strEquCI(name, "RUNDMB") || strEquCI(name, "DMBRUN")) {
    optRunDMB = (name[3] != 0);
    optWriteType = 'd';
    if (currLine[0]) fatal("too many expressions");
    return PI_SKIP_LINE;
  }
  if (strEquCI(name, "NONE") || strEquCI(name, "NOTHING")) {
    optWriteType = 'n';
    if (currLine[0]) fatal("too many expressions");
    return PI_SKIP_LINE;
  }
  if (strEquCI(name, "SCL") || strEquCI(name, "RUNSCL") ||  strEquCI(name, "SCLRUN")) {
    optWriteType = 'S';
    optRunSCL = (name[3] != 0 ? -1 : 0); /* no boot */
    piTapParseLoaderName();
    return PI_SKIP_LINE;
  }
  if (strEquCI(name, "SCLMONO") || strEquCI(name, "RUNSCLMONO") ||  strEquCI(name, "SCLMONORUN")) {
    optWriteType = 'M';
    optRunSCL = (name[7] != 0 ? -1 : 0); /* no boot */
    piTapParseLoaderName();
    return PI_SKIP_LINE;
  }
  if (strEquCI(name, "SCLBOOT") || strEquCI(name, "BOOTSCL")) {
    optWriteType = 'S';
    optRunSCL = 1; /* with boot */
    piTapParseLoaderName();
    return PI_SKIP_LINE;
  }
  if (strEquCI(name, "SCLMONOBOOT") || strEquCI(name, "BOOTSCLMONO")) {
    optWriteType = 'M';
    optRunSCL = 1; /* with boot */
    piTapParseLoaderName();
    return PI_SKIP_LINE;
  }

  if (strEquCI(name, "DSKBOOT") || strEquCI(name, "BOOTDSK") ||
      strEquCI(name, "DSK180BOOT") || strEquCI(name, "BOOTDSK180"))
  {
    optWriteType = '3';
    optRunDSK = 1; /* with boot */
    optDskType = P3DSK_PCW_SS; // +3DOS 180K disk
    piTapParseLoaderName();
    return PI_SKIP_LINE;
  }

  if (strEquCI(name, "DSK720BOOT") || strEquCI(name, "BOOTDSK720")) {
    optWriteType = '3';
    optRunDSK = 1; /* with boot */
    optDskType = P3DSK_PCW_DS; // +3DOS 720K disk
    piTapParseLoaderName();
    return PI_SKIP_LINE;
  }

  if (strEquCI(name, "DSK") || strEquCI(name, "DSK180")) {
    optWriteType = '3';
    optRunDSK = 0; /* without boot */
    optDskType = P3DSK_PCW_SS; // +3DOS 180K disk
    piTapParseLoaderName();
    return PI_SKIP_LINE;
  }

  if (strEquCI(name, "DSK720")) {
    optWriteType = '3';
    optRunDSK = 0; /* without boot */
    optDskType = P3DSK_PCW_DS; // +3DOS 720K disk
    piTapParseLoaderName();
    return PI_SKIP_LINE;
  }

  fatal("invalid default output type: %s", name);
}


///////////////////////////////////////////////////////////////////////////////
// line processor
//
static void processCurrentLine (void) {
  if (!currSrcLine) return; // do nothing
  loadCurSrcLine();
  ur_assert(asmMode != AMODE_UFO); // the thing that should not be
  //fprintf(stderr, "<%s>\n", currLine);
  processLabel();
  for (;;) {
    char *str, *ee, name[66];
    UrAsmOp *op;
    int len;
    const char *errpos;
    removeSpacesAndColons();
    // skip spaces and ':'
    if (!currLine[0]) { nextSrcLine(); break; }
    // try to find and process command
    str = currLine; //while (*str && isSpace(*str)) ++str; // skip spaces
    // find command end
    for (ee = str; *ee && !isSpace(*ee) && *ee != '"' && *ee != '\'' && *ee != ',' && *ee != ':'; ++ee) {}
    // get command, if any
    if (ee != str && ee-str <= 64) {
      MacroDef *mc;
      memset(name, 0, sizeof(name));
      memmove(name, str, ee-str);
      /* UrForth macro? */
      uint32_t fwmacro = ufoFindWordInVocabulary(name, ufoMacroVocId);
      if (fwmacro != 0) {
        // ok, do it
        str = ee; while (*str && isSpace(*str)) ++str; // skip spaces
        memmove(currLine, str, strlen(str)+1);
        ufoRunMacroWord(fwmacro);
        /* only one macro per line! */
        nextSrcLine(); // skip it
        break;
      }
      /* known command? */
      op = urFindOp(name);
      if (op) {
        // ok, do it
        str = ee; while (*str && isSpace(*str)) ++str; // skip spaces
        memmove(currLine, str, strlen(str)+1);
        urCurrentOp = op;
        if (op->fn()) {
          urCurrentOp = NULL;
          nextSrcLine(); // skip it
          break;
        }
        urCurrentOp = NULL;
        continue;
      }
      /* known macro? */
      if ((mc = findMacro(name)) != NULL) {
        // ok, do it
        str = ee; while (*str && isSpace(*str)) ++str; // skip spaces
        memmove(currLine, str, strlen(str)+1);
        processMacro(mc);
        /* only one macro per line! */
        break;
      }
    }
    // with reservation management
   again_from_reserve:
    len = urasm_opasm(currLine, pc, disp, &errpos);
    if (len < 0) fatalUrLib(len);
    // check for reservation
    if (len > 0) {
      if (reserveHit(pc, len)) {
       insert_jump:
        if (pc != disp) fatal("trying to use reserved areas with phased code");
        reserveReleaseUsed(pc, len);
        int nextaddr = reservedFindNextSuitableFreeFrom(pc, pc+len);
        if (nextaddr < 0) fatal("codegen started in reserved area #%04X", pc);
        // insert jump to the new area, and retry
        fprintf(stderr, "...inserting jump from #%04X to #%04X\n", (unsigned)pc, (unsigned)nextaddr);
        // can we insert a JP? it is faster than JR
        if (memresv[pc] || memresv[pc+1]) fatal("internal reserve area manager error at #%04X", pc);
        if (!memresv[pc+2]) {
          // use JP
          emitByte(0xc3U); // JP
          emitWord((uint16_t)nextaddr);
        } else {
          // use JR
          int jrdest = nextaddr-(pc+2U);
          if (jrdest >= 128) fatal("internal reserve area manager error at #%04X (jr dist)", pc);
          emitByte(0x18U); // JR
          emitByte((uint8_t)jrdest);
        }
        disp = pc = (uint16_t)nextaddr;
        goto again_from_reserve;
      } else {
        // did we compiled JP/JR?
        if (getByte(pc) == 0xc3U || getByte(pc) == 0x18U) {
          // no room for jump?
          if (memresv[(pc+len)&0xffffU] || memresv[(pc+len+1)&0xffffU] || memresv[(pc+len+2)&0xffffU]) {
            if (pc != disp) fatal("trying to use reserved areas with phased code");
            // find next free area
            int next = pc+len;
            while (next >= 0 && next < 65536) {
              next = reserveFindFreeFrom(next);
              if (next < 0) break;
              // has room for JP there?
              if (!memresv[next] && !memresv[(next+1)&0xffffU] && !memresv[(next+2)&0xffffU]) break;
              // no room for JP, has room for JP?
              if (!memresv[next] && !memresv[(next+1)&0xffffU]) {
                // check if jr can fit
                const int nj = reservedFindNextJumpableFreeFrom(next, next+2);
                if (nj >= 0) break;
              }
              next = reserveFindReservedFrom(next);
            }
            if (next >= 0) disp = pc = (next-len)&0xffffU; // so "+len" will make PC right
          }
        } else {
          // check if we still have enough room for a possible jp/jr
          if (memresv[(pc+len)&0xffffU] || memresv[(pc+len+1)&0xffffU]) goto insert_jump;
          // we have at least a room for JR, check if we have a room for JP
          if (!memresv[(pc+len+2)&0xffffU]) {
            // no room for JP, check if jr will be enough
            int next = reservedFindNextSuitableFreeFrom(pc, pc+len+2);
            if (next >= 0 && next-(pc+len+2u) >= 128) goto insert_jump;
          }
        }
      }
    }
    pc += len;
    disp += len;
    if (len >= 0 && errpos) {
      memmove(currLine, errpos+1, strlen(errpos));
    } else {
      nextSrcLine(); // skip it
      break;
    }
  }
}


///////////////////////////////////////////////////////////////////////////////
// start inline UrForth code
//
static int piSTARTFORTH (void) {
  checkOperatorEnd();
  asmMode = AMODE_UFO;
  ufoRunInterpretLoop();
  asmMode = AMODE_NORMAL;
  return PI_SKIP_LINE;
}


///////////////////////////////////////////////////////////////////////////////
// setup instructions
//
static void registerInstructions (void) {
  urAddOpAndDollar("DISPLAY", piDISPLAY);
  urAddOpAndDollar("DISPLAY0", piDISPLAY0);
  urAddOpAndDollar("DISPLAYA", piDISPLAYA);
  urAddOpAndDollar("DISPHEX", piDISPHEX);
  urAddOpAndDollar("DISPHEX0", piDISPHEX0);
  urAddOpAndDollar("DISPHEXA", piDISPHEXA);
  //
  urAddOpAndDollar("DEFFMT", piDEFFMT);
  urAddOp("$DEFFMT", piDEFFMT);
  urAddOp("$MODEL", piMODEL);
  urAddOp("$SAVECODE", piSAVECODE);
  //
  urAddOp("MACRO", piMACRO);
  urAddOp("ENDM", piENDM);
  //
  urAddOp("ORG", piORG);
  urAddOp("DISP", piDISP);
  urAddOp("ENDDISP", piENDDISP);
  urAddOp("PHASE", piDISP);
  urAddOp("DEPHASE", piENDDISP);
  urAddOp("UNPHASE", piENDDISP);
  urAddOp("ALIGN", piALIGN);
  urAddOp("DISPALIGN", piDISPALIGN);
  urAddOp("PHASEALIGN", piDISPALIGN);
  urAddOp("ENT", piENT);
  urAddOp("CLR", piCLR);
  urAddOp("RESERVE", piRESERVE);
  //
  urAddOpAndDollar("INCLUDE", piINCLUDE);
  urAddOpAndDollar("INCBIN", piINCBIN);
  urAddOpAndDollar("DATABIN", piDATABIN);
  urAddOpAndDollar("CODEBIN", piCODEBIN);
  //
  urAddOp("MODULE", piMODULE);
  urAddOp("ENDMODULE", piENDMODULE);
  //
  urAddOp("DUP", piDUP);
  urAddOp("EDUP", piEDUP);
  urAddOp("REPT", piREPT);
  urAddOp("ENDR", piENDR);
  // structures
  urAddOp("STRUCT", piSTRUCT);
  urAddOp("ENDS", piENDS);
  // pasmo support
  urAddOp("END", piEND_PASMO);
  //
  urAddOpAndDollar("IF", piIF);
  urAddOpAndDollar("IFX", piIFX);
  urAddOpAndDollar("ELSE", piELSE);
  urAddOpAndDollar("ELSIF", piELSIF);
  urAddOpAndDollar("ELSIFX", piELSIFX);
  urAddOpAndDollar("ELSEIF", piELSIF);
  urAddOpAndDollar("ELSEIFX", piELSIFX);
  urAddOpAndDollar("ENDIF", piENDIF);
  //
  urAddOp("DEFINCR", piDEFINCR);
  urAddOp("DEFB", piDEFB); urAddOp("DB", piDEFB);
  urAddOp("DEFW", piDEFW); urAddOp("DW", piDEFW);
  urAddOp("DEFR", piDEFR); urAddOp("DR", piDEFR);
  urAddOp("DEFS", piDEFS); urAddOp("DS", piDEFS);
  urAddOp("DEFM", piDEFM); urAddOp("DM", piDEFM);
  urAddOp("DEFZ", piDEFZ); urAddOp("DZ", piDEFZ);
  urAddOp("DEFX", piDEFX); urAddOp("DX", piDEFX);
  urAddOp("DEFC", piDEFC); urAddOp("DC", piDEFC);

  urAddOp("$ERROR", piERROR);
  urAddOp("$WARNING", piWARNING);

  urAddOp("$PRINTF", piPRINTF);
  urAddOp("$PRINTF0", piPRINTF0);
  urAddOp("$PRINTFA", piPRINTFA);

  urAddOp("$MATHMODE", piMATHMODE);
  urAddOp("$REFOPT", piREFOPT);
  urAddOp("$CASTRATES", piCASTRATES);

  urAddOp("$START_FORTH", piSTARTFORTH);
}


///////////////////////////////////////////////////////////////////////////////
// !0: invalid label
/*
static inline void fnSkipSpaces (const char *expr) {
  while (*expr && isSpace(*expr)) ++expr;
  return expr;
}
*/


UR_FORCE_INLINE char fnNextChar (const char *expr) {
  while (*expr && isSpace(*expr)) ++expr;
  return *expr;
}


#define FN_SKIP_BLANKS  do { \
  while (*expr && isSpace(*expr)) ++expr; \
} while (0)

#define FN_CHECK_END  do { \
  while (*expr && isSpace(*expr)) ++expr; \
  if (expr[0] != ')') { *error = UR_EXPRERR_FUNC; return expr; } \
  ++expr; \
} while (0)


#define FN_CHECK_COMMA  do { \
  while (*expr && isSpace(*expr)) ++expr; \
  if (expr[0] != ',') { *error = UR_EXPRERR_FUNC; return expr; } \
  ++expr; \
} while (0)


static const char *readLabelName (char *buf, const char *expr) {
  int pos = 0;
  //
  while (*expr && isSpace(*expr)) ++expr;
  for (;;) {
    char ch = *expr++;
    //
    if (pos >= 128) return NULL;
    if (ch == ')') { --expr; break; }
    if (!ch) break;
    if (isAlphaDigit(ch) || ch == '$' || ch == '.' || ch == '_' || ch == '@') {
      buf[pos++] = ch;
    } else {
      break;
    }
  }
  if (pos < 1) return NULL;
  buf[pos] = '\0';
  if (!urasm_is_valid_name(buf)) return NULL;
  return expr;
}


static const char *fnDefKn (urasm_exprval_t *res, const char *expr, uint16_t addr, int donteval, int *defined, int *error, int qtype) {
  char lbl[130];
  //
  if ((expr = readLabelName(lbl, expr)) == NULL) { *error = UR_EXPRERR_LABEL; return NULL; }
  FN_CHECK_END;
  if (!donteval) res->val = (isLabelDefinedOrKnown(lbl, addr, qtype) != 0);
  return expr;
}

static const char *fnDefined (urasm_exprval_t *res, const char *expr, uint16_t addr, int donteval, int *defined, int *error) { return fnDefKn(res, expr, addr, donteval, defined, error, UR_QTYPE_DEFINED); }
static const char *fnKnown (urasm_exprval_t *res, const char *expr, uint16_t addr, int donteval, int *defined, int *error) { return fnDefKn(res, expr, addr, donteval, defined, error, UR_QTYPE_KNOWN); }


static const char *fnAligned256 (urasm_exprval_t *res, const char *expr, uint16_t addr, int donteval, int *defined, int *error) {
  expr = urasm_expr_ex(res, expr, addr, &donteval, defined, error);
  FN_CHECK_END;
  if (!donteval) res->val = (res->val%256 ? 0 : 1);
  return expr;
}


static const char *fnSameSeg (urasm_exprval_t *res, const char *expr, uint16_t addr, int donteval, int *defined, int *error) {
  urasm_exprval_t v0, v1;
  //
  urasm_exprval_init(&v0);
  urasm_exprval_init(&v1);
  expr = urasm_expr_ex(&v0, expr, addr, &donteval, defined, error);
  if (*error) return expr;
  FN_CHECK_COMMA;
  expr = urasm_expr_ex(&v1, expr, addr, &donteval, defined, error);
  if (*error) return expr;
  FN_CHECK_END;
  if (!donteval) res->val = (v0.val/256 == v1.val/256);
  return expr;
}


static const char *fnAlign (urasm_exprval_t *res, const char *expr, uint16_t addr, int donteval, int *defined, int *error) {
  urasm_exprval_t v0, v1;
  urasm_exprval_init(&v0);
  urasm_exprval_init(&v1);
  expr = urasm_expr_ex(&v0, expr, addr, &donteval, defined, error);
  if (*error) return expr;
  FN_SKIP_BLANKS;
  if (fnNextChar(expr) == ',') {
    FN_CHECK_COMMA;
    expr = urasm_expr_ex(&v1, expr, addr, &donteval, defined, error);
    if (*error) return expr;
  } else {
    v1.val = 256;
  }
  FN_CHECK_END;
  if (!donteval) {
    if (v1.val < 1) { *error = UR_EXPRERR_FUNC; return NULL; }
    res->val = (v0.val+v1.val-1)/v1.val*v1.val;
  }
  return expr;
}


static const char *fnLow (urasm_exprval_t *res, const char *expr, uint16_t addr, int donteval, int *defined, int *error) {
  const char *ee = expr;
  expr = urasm_expr_ex(res, expr, addr, &donteval, defined, error);
  FN_CHECK_END;
  if (!donteval) {
    if (res->fixuptype == UR_FIXUP_HIBYTE) {
      *error = UR_EXPRERR_FUNC; return ee;
    }
    res->fixuptype = UR_FIXUP_LOBYTE;
    res->val &= 0xff;
  }
  return expr;
}


static const char *fnHigh (urasm_exprval_t *res, const char *expr, uint16_t addr, int donteval, int *defined, int *error) {
  const char *ee = expr;
  expr = urasm_expr_ex(res, expr, addr, &donteval, defined, error);
  FN_CHECK_END;
  if (!donteval) {
    if (res->fixuptype == UR_FIXUP_LOBYTE) {
      *error = UR_EXPRERR_FUNC; return ee;
    }
    res->fixuptype = UR_FIXUP_HIBYTE;
    res->val = (res->val>>8)&0xff;
  }
  return expr;
}


static const char *fnAbs (urasm_exprval_t *res, const char *expr, uint16_t addr, int donteval, int *defined, int *error) {
  const char *ee = expr;
  expr = urasm_expr_ex(res, expr, addr, &donteval, defined, error);
  FN_CHECK_END;
  if (!donteval) {
    if (res->fixuptype != UR_FIXUP_NONE) {
      *error = UR_EXPRERR_FUNC; return ee;
    }
    res->val = abs(res->val);
  }
  return expr;
}


static const char *fnBSwap (urasm_exprval_t *res, const char *expr, uint16_t addr, int donteval, int *defined, int *error) {
  const char *ee = expr;
  expr = urasm_expr_ex(res, expr, addr, &donteval, defined, error);
  FN_CHECK_END;
  if (!donteval) {
    if (res->fixuptype != UR_FIXUP_NONE) {
      *error = UR_EXPRERR_FUNC; return ee;
    }
    res->val = ((res->val>>8)&0xff)|((res->val&0xff)<<8);
  }
  return expr;
}


static const char *fnScrAddr8xn (int nmul, urasm_exprval_t *res, const char *expr, uint16_t addr, int donteval, int *defined, int *error) {
  int32_t scrbase = 0x4000;
  int32_t x, y;
  const char *ee = expr;
  //fprintf(stderr, "fnScrAddr8xn: n=%d; expr=<%s>\n", nmul, expr);
  // first argument is `x`
  expr = urasm_expr_ex(res, expr, addr, &donteval, defined, error);
  x = res->val;
  // second argument is `y`
  FN_CHECK_COMMA;
  expr = urasm_expr_ex(res, expr, addr, &donteval, defined, error);
  y = res->val*nmul;
  // optional third arg is screen base
  FN_SKIP_BLANKS;
  if (expr[0] == ',') {
    FN_CHECK_COMMA;
    expr = urasm_expr_ex(res, expr, addr, &donteval, defined, error);
    scrbase = res->val&0xffff;
  }
  FN_CHECK_END;
  if (!donteval) {
    //urasm_exprval_clear(res);
    if (res->fixuptype != UR_FIXUP_NONE) { *error = UR_EXPRERR_FUNC; return ee; }
    if (x < 0 || x > 31) fatal("invalid x coordinate: %d", x);
    if (y < 0 || y > 191) fatal("invalid y coordinate: %d", y/nmul);
    res->val = scrbase+
      (y/64)*2048+
      (y%8)*256+
      ((y%64)/8)*32+
      x;
    //fprintf(stderr, "x=%d; y=%d; addr=#%04X\n", x, y, res->val);
  }
  return expr;
}


static const char *fnScrAddr8x1 (urasm_exprval_t *res, const char *expr, uint16_t addr, int donteval, int *defined, int *error) {
  return fnScrAddr8xn(1, res, expr, addr, donteval, defined, error);
}

static const char *fnScrAddr8x8 (urasm_exprval_t *res, const char *expr, uint16_t addr, int donteval, int *defined, int *error) {
  return fnScrAddr8xn(8, res, expr, addr, donteval, defined, error);
}


static const char *fnScrAttr8x8 (urasm_exprval_t *res, const char *expr, uint16_t addr, int donteval, int *defined, int *error) {
  int32_t scrbase = 0x4000;
  int32_t x, y;
  const char *ee = expr;
  // first argument is `x`
  expr = urasm_expr_ex(res, expr, addr, &donteval, defined, error);
  x = res->val;
  // second argument is `y`
  FN_CHECK_COMMA;
  urasm_exprval_clear(res);
  expr = urasm_expr_ex(res, expr, addr, &donteval, defined, error);
  y = res->val;
  // optional third arg is screen base
  FN_SKIP_BLANKS;
  if (expr[0] == ',') {
    FN_CHECK_COMMA;
    urasm_exprval_clear(res);
    expr = urasm_expr_ex(res, expr, addr, &donteval, defined, error);
    scrbase = res->val&0xffff;
  }
  urasm_exprval_clear(res);
  FN_CHECK_END;
  if (!donteval) {
    if (res->fixuptype != UR_FIXUP_NONE) { *error = UR_EXPRERR_FUNC; return ee; }
    if (x < 0 || x > 31) fatal("invalid x coordinate: %d", x);
    if (y < 0 || y > 23) fatal("invalid y coordinate: %d", y);
    res->val = scrbase+6144+y*32+x;
  }
  return expr;
}


static const char *fnMArgToStr (urasm_exprval_t *res, const char *expr, uint16_t addr, int donteval, int *defined, int *error) {
  const char *ee = expr;
  // argument is macro argument name
  expr = strSkipSpacesConst(expr);
  if (expr[0] != '=') { *error = UR_EXPRERR_FUNC; return ee; }
  ++expr;
  if (!isAlpha(expr[0]) && expr[0] != '_') { *error = UR_EXPRERR_FUNC; return ee; }
  const char *nend = expr+1;
  while (isAlphaDigit(*nend) || *nend == '_') ++nend;
  if (nend-expr > 127) { *error = UR_EXPRERR_FUNC; return ee; }
  char name[128];
  memset(name, 0, sizeof(name));
  memcpy(name, expr, nend-expr);
  expr = nend;
  // parse index
  int index = 0;
  int has_index = 0;
  expr = strSkipSpacesConst(expr);
  if (expr[0] == '[') {
    expr = strSkipSpacesConst(expr+1);
    if (expr[0] != ']') {
      has_index = 1;
      int doneg = 0;
           if (expr[0] == '+') ++expr;
      else if (expr[0] == '-') { doneg = 1; ++expr; }
      index = 0;
      while (isDigit(expr[0])) {
        index = index*10+(expr[0]-'0');
        if (index >= 0x1fffffff) fatal("`margtostr` index too high");
        ++expr;
      }
      expr = strSkipSpacesConst(expr);
      if (doneg) index = -index;
    }
    if (expr[0] != ']') fatal("`margtostr` invalid index syntax");
    ++expr;
  }
  // done
  FN_CHECK_END;
  if (curmacro == NULL) fatal("`margtostr` outside of macro");
  if (!donteval) {
    for (int f = 0; f < curmacro->mac->argc; ++f) {
      if (strEquCI(name, curmacro->mac->argnames[f])) {
        // found argument, convert it to string
        if (!has_index) {
          res->str = realloc(res->str, strlen(curmacro->argvals[f])+1);
          strcpy(res->str, curmacro->argvals[f]);
        } else {
          const size_t slen = strlen(curmacro->argvals[f]);
          if (index < 0) {
            index = -index;
            if (index > slen) fatal("index out of string");
            index = (int)slen-index;
          }
          if (index >= slen) fatal("index out of string");
          res->str = realloc(res->str, 2);
          res->str[0] = curmacro->argvals[f][index];
          res->str[1] = 0;
        }
        // lowercase it
        for (char *s = res->str; *s; ++s) *s = toLower(*s);
        //fprintf(stderr, "<%s>\n", res->str);
        if (res->str[0]) {
          if (res->str[1]) {
            res->val = ((unsigned char)res->str[0]);
            res->val |= ((unsigned char)res->str[1])<<8;
          } else {
            res->val = (unsigned char)res->str[0];
          }
        } else {
          res->val = 0;
        }
        return expr;
      }
    }
    fatal("unknown macro argument '%s'", name);
  }
  return expr;
}


static const char *fnStrLen (urasm_exprval_t *res, const char *expr, uint16_t addr, int donteval, int *defined, int *error) {
  expr = strSkipSpacesConst(expr);
  int stlen = 0;
  switch (expr[0]) {
    case '"': case '\'': /* string literal? */
      {
        char *a = (char *)expr+1;
        (void)parseStr(&a, expr[0], &stlen);
        expr = (const char *)a;
      }
      break;
    default: /* expression */
      {
        urasm_exprval_t r;
        urasm_exprval_init(&r);
        expr = urasm_expr_ex(&r, expr, disp, &donteval, defined, error);
        if (*error) fatalUrLib(*error);
        if (!r.str) fatal("string expected for `strlen()`");
        stlen = (int)strlen(r.str);
        urasm_exprval_clear(&r);
      }
      break;
  }
  FN_CHECK_END;
  if (!donteval) {
    urasm_exprval_setint(res, stlen);
  }
  return expr;
}


static void registerFunctions (void) {
  urasm_expr_register_func("defined", fnDefined);
  urasm_expr_register_func("known", fnKnown);
  urasm_expr_register_func("aligned256", fnAligned256);
  urasm_expr_register_func("align", fnAlign);
  urasm_expr_register_func("sameseg", fnSameSeg);
  urasm_expr_register_func("low", fnLow);
  urasm_expr_register_func("high", fnHigh);
  urasm_expr_register_func("abs", fnAbs);
  urasm_expr_register_func("bswap", fnBSwap);

  urasm_expr_register_func("scraddr8x8", fnScrAddr8x8);
  urasm_expr_register_func("scraddr8x1", fnScrAddr8x1);
  urasm_expr_register_func("scrattr8x8", fnScrAttr8x8);

  urasm_expr_register_func("marg2str", fnMArgToStr);
  urasm_expr_register_func("strlen", fnStrLen);
}


///////////////////////////////////////////////////////////////////////////////
// preparing another pass
//
static void initPass (void) {
  asmMode = AMODE_NORMAL;
  currSrcLine = asmText;
  currModule = NULL;
  pc = start_pc;
  disp = start_disp;
  ent = start_ent;
  inTapeBlock = 0;
  tapeXorB = 0;
  wasOrg = 0;
  wasClr = 0;
  ifCount = 0;
  dfi_free();
  //defIncr = 0;
  lblOptMakeU2 = 0;
  curmacronum = 0;
  if (lastSeenGlobalLabel) free(lastSeenGlobalLabel);
  lastSeenGlobalLabel = NULL;
  //lastSeenGlobalLabel = strdup(" [MAIN] ");
  modulesResetSeen();
  prepareMemory();
  urasm_use_old_priorities = 0;
  strcpy(lastFindLabelName, "");
  strcpy(currSeenLabel, "");
  urResetTempLabels();
  ufoClearIncludePath();
}


static int postPass (void) {
  if (lastSeenGlobalLabel) free(lastSeenGlobalLabel);
  lastSeenGlobalLabel = NULL;
  if (checkLabels()) return -1;
  if (ifCount != 0) fatal("unbalanced IFs");
  return 0;
}


///////////////////////////////////////////////////////////////////////////////
static int labelCmp (const void *aa, const void *bb) {
  if (aa == bb) return 0;
  const UrLabelInfo *a = *(const UrLabelInfo **)aa;
  const UrLabelInfo *b = *(const UrLabelInfo **)bb;
  return
    a->value < b->value ? -1 :
    a->value > b->value ? 1 :
    0;
}


static __attribute__((unused)) int strStartsWith (const char *s, const char *pat) {
  if (!pat || !pat[0]) return 0;
  if (!s || !s[0]) return 0;
  while (*s && *pat) {
    if (*s != *pat) return 0;
    ++s;
    ++pat;
  }
  return (*pat == 0);
}


static int isGoodLabelForRef (const UrLabelInfo *ll) {
  if (!ll) return 0;

  /*
  // do not output `=` labels
  if (ll->type == LBL_TYPE_ASS) return 0;
  // do not output struct labels
  if (ll->type == LBL_TYPE_STOFFS) return 0;
  */

  if (urasm_dump_all_labels) {
    if (strStartsWith(ll->name, "UFO_") || strStartsWith(ll->name, "USE_")) return 0;
    return 1;
  }

  for (const char *s = ll->name; *s; ++s) {
    const char ch = *s;
    if (ch >= 'A' && ch <= 'Z') continue;
    if (ch >= '0' && ch <= '9') continue;
    if (ch == '_') continue;
    return 1;
  }

  return 0;
}


static void writeLabelsFile (const char *fname) {
  if (!fname || !fname[0]) return;

  // count labels
  int lcount = 0;
  for (const UrLabelInfo *ll = labels; ll; ll = ll->next) {
    if (isGoodLabelForRef(ll)) ++lcount;
  }

  UrLabelInfo **larr = NULL;
  if (lcount > 0) {
    // create labels array
    larr = (UrLabelInfo **)malloc(sizeof(UrLabelInfo *)*lcount);
    lcount = 0;
    for (UrLabelInfo *ll = labels; ll; ll = ll->next) {
      if (isGoodLabelForRef(ll)) larr[lcount++] = ll;
    }
    // sort labels
    qsort(larr, lcount, sizeof(UrLabelInfo *), &labelCmp);
  }

  // write labels
  FILE *fo = fopen(fname, "w");
  if (lcount > 0) {
    for (int f = 0; f < lcount; ++f) {
      UrLabelInfo *ll = larr[f];
      //if (!ll->name || (!isAlpha(ll->name[0]) && ll->name[0] != '@')) continue;
      if (!ll->name) continue; // just in case
      if (ll->name[0] == '{') continue; // we don't have these, but it is still reserved
      if (ll->value < 0) {
        fprintf(fo, "%d %s", ll->value, ll->name);
      } else {
        fprintf(fo, "#%04X %s", (unsigned)ll->value, ll->name);
      }
      fprintf(fo, "   ");
      switch (ll->type) {
        case LBL_TYPE_VERY_SPECIAL: fprintf(fo, "WTF"); break;
        case LBL_TYPE_UNKNOWN: fprintf(fo, "UNKNOWN"); break;
        case LBL_TYPE_ASS: fprintf(fo, "assign"); break;
        case LBL_TYPE_EQU: fprintf(fo, "equ"); break;
        case LBL_TYPE_CODE: fprintf(fo, "code"); break;
        case LBL_TYPE_STOFS: fprintf(fo, "stofs"); break;
        case LBL_TYPE_DATA: fprintf(fo, "data"); break;
        default: fprintf(fo, "WTF"); break;
      }
      fputc('\n', fo);
    }
    free(larr);
  }

  fclose(fo);
}


///////////////////////////////////////////////////////////////////////////////
// options
//
static struct option longOpts[] = {
  {"org", required_argument, NULL, 600},
  {"define", required_argument, NULL, 601},
  {"defzero", required_argument, NULL, 602},
  {"outdir", required_argument, NULL, 660},
  {"reffile", optional_argument, NULL, 669},
  {"outfile", required_argument, NULL, 665},
  {"castrates", optional_argument, NULL, 656},
  //
  {"sna", 0, NULL, 's'},
  {"sna128", 0, NULL, 'S'},
  {"tap", 0, NULL, 't'},
  {"autotap", 0, NULL, 'T'},
  {"raw", 0, NULL, 'r'},
  {"autodmb", 0, NULL, 'B'},
  {"dmb", 0, NULL, 'b'},
  {"none", 0, NULL, 'n'},
  {"help", 0, NULL, 'h'},
  {"hob", 0, NULL, 'H'},
  {"scl", 0, NULL, 'l'},
  {"autoscl", 0, NULL, 'L'},
  {"monoscl", 0, NULL, 'm'},
  {"autosclmono", 0, NULL, 'M'},
  {"dsk", 0, NULL, 690},
  {"dsk720", 0, NULL, 691},
  {"adsk", 0, NULL, 692},
  {"adsk720", 0, NULL, 693},
  {"fixups", optional_argument, NULL, 'F'},
  //
  {NULL, 0, NULL, 0}
};


static const char *defInFiles[] = {"main.zas", "main.a80", "main.asm", NULL};


static void usage (const char *pname) {
  printf(
    "usage: %s [options] infile\n"
    "default infiles:", pname);
  for (int f = 0; defInFiles[f]; ++f) printf("  %s", defInFiles[f]);
  printf("\n"
    "options:\n"
    "  -s --sna      write 48K .SNA file with autostart\n"
    "  -S --sna128   write 148K .SNA file with autostart\n"
    "  -t --tap      write .tap file\n"
    "  -T --autotap  write .tap file with basic loader (CLR to set CLEAR)\n"
    "  -r --raw      write raw file(s)\n"
    "  -b --dmb      write DMB file\n"
    "  -B --autodmb  write DMB file with autostart\n"
    "  -H --hob      write HoBeta code file(s)\n"
    "  -l --scl      write SCL TR-DOS archive\n"
    "  -L --autoscl  write autostarting SCL TR-DOS archive\n"
    "  -m --monoscl  write SCL with monoloader\n"
    /*"  -M --monosclauto  write autorun SCL with monoloader\n"*/
    "     --dsk      write +3DOS 180KB disk\n"
    "     --dsk720   write +3DOS 720KB disk\n"
    "     --adsk     write +3DOS 180KB disk with autostart\n"
    "     --adsk720  write +3DOS 720KB disk with autostart\n"
    "  -n --none     write nothing\n"
    "  -F --fixups   write fixup file 'zfixuptable.EXT'\n"
    "                optional arg: text (default), asm/zas, asmdiff/zasdiff, asmz/zasz\n"
    "                  text: .txt file\n"
    "                  asm: address lists with counters\n"
    "                  asmdiff: 2nd and other addresses are relative, with counters\n"
    "                  asmz: address lists, with 0 end markers\n"
    "  -h --help     this help\n"
    "specials:\n"
    "  --reffile[=name]  write labels to reffile, formatted as \"#nnnn NAME\"\n"
    "  --org xxx         set ORG\n"
    "  --define  val     perform 'val EQU 1'\n"
    "  --defzero val     perform 'val EQU 0'\n"
    "  --outdir dir      output dir for resulting files (default: current)\n"
    "  --outfile fname   output file name for image formats (default: main asm file name)\n"
    "  --castrates       allow '&abc' hex numbers (and disable '&b' prefix)\n"
  );
}


///////////////////////////////////////////////////////////////////////////////
//#include "urforth.c"

enum {
  UFO_ZX_LABEL_UNDEFINED = -1,
  UFO_ZX_LABEL_UNKNOWN = 0, // referenced, but not defined yet
  UFO_ZX_LABEL_VAR = 1, // a = value
  UFO_ZX_LABEL_EQU = 2, // a equ value
  UFO_ZX_LABEL_CODE = 3,
  UFO_ZX_LABEL_DATA = 4,
  UFO_ZX_LABEL_STOFS = 5, // structure offset
};

__attribute__((noreturn))
void ufoFatalError (void) {
  longjmp(errJP, 666);
}

static void ufoZXGetU8 (uint32_t mypfa) {
  uint32_t addr = ufoPopData();
  addr &= 0xffffU;
  ufoPushData(getByte(addr));
}

static void ufoZXPutU8 (uint32_t mypfa) {
  uint32_t addr = ufoPopData();
  uint32_t v = ufoPopData();
  addr &= 0xffffU;
  v &= 0xffU;
  putByte(addr, v);
}

static void ufoZXGetU16 (uint32_t mypfa) {
  uint32_t addr = ufoPopData();
  addr &= 0xffffU;
  ufoPushData(getWord(addr));
}

static void ufoZXPutU16 (uint32_t mypfa) {
  uint32_t addr = ufoPopData();
  uint32_t v = ufoPopData();
  addr &= 0xffffU;
  v &= 0xffffU;
  putWord(addr, v);
}

static void ufoZXEmitU8 (uint32_t mypfa) {
  emitByte(ufoPopData()&0xffU);
}

static void ufoZXEmitU16 (uint32_t mypfa) {
  emitWord(ufoPopData()&0xffffU);
}

static void ufoZXGetReserved (uint32_t mypfa) {
  uint32_t addr = ufoPopData();
  ufoPushData(memresv[addr&0xffffU]);
}

static void ufoZXSetReserved (uint32_t mypfa) {
  uint32_t addr = ufoPopData();
  uint32_t resvflag = ufoPopData();
  memresv[addr&0xffffU] = resvflag;
}

static void ufoZXGetPass (uint32_t mypfa) {
  ufoPushData(pass);
}

static void ufoZXGetOrg (uint32_t mypfa) {
  ufoPushData(pc);
}

static void ufoZXSetOrg (uint32_t mypfa) {
  uint32_t addr = ufoPopData();
  if (addr > 0xffff) ufoFatal("invalid ORG address: %u", addr);
  pc = disp = (uint16_t)addr;
  if (!wasOrg) {
    wasOrg = 1; // so next `ORG` will not reset it
    ent = (uint16_t)addr;
  }
}

static void ufoZXGetDisp (uint32_t mypfa) {
  ufoPushData(disp);
}

static void ufoZXSetDisp (uint32_t mypfa) {
  uint32_t addr = ufoPopData();
  if (addr > 0xffff) ufoFatal("invalid DISP address: %u", addr);
  disp = (uint16_t)addr;
}

static void ufoZXGetEnt (uint32_t mypfa) {
  ufoPushData(wasOrg ? (uint32_t)ent : ~0u);
}

static void ufoZXSetEnt (uint32_t mypfa) {
  uint32_t addr = ufoPopData();
  if (addr > 0xffff) ufoFatal("invalid ENT address: %u", addr);
  wasOrg = 1; // so next `ORG` will not reset it
  ent = (uint16_t)addr;
}

static int ufoZXLType2UFO (int type) {
  switch (type) {
    case LBL_TYPE_VERY_SPECIAL: return UFO_ZX_LABEL_UNDEFINED;
    case LBL_TYPE_UNKNOWN: return UFO_ZX_LABEL_UNKNOWN;
    case LBL_TYPE_ASS: return UFO_ZX_LABEL_VAR;
    case LBL_TYPE_EQU: return UFO_ZX_LABEL_EQU;
    case LBL_TYPE_CODE: return UFO_ZX_LABEL_CODE;
    case LBL_TYPE_DATA: return UFO_ZX_LABEL_DATA;
    case LBL_TYPE_STOFS: return UFO_ZX_LABEL_STOFS;
  }
  ufoFatal("WTF?! unknown label type %d", type);
}

static char ufoStrLit[256];

static void ufoZXPopStrLit (void) {
  int32_t count = (int32_t)ufoPopData();
  uint32_t addr = ufoPopData();
  if (count < 0) ufoFatal("invalid string");
  uint32_t pos = 0;
  while (pos < (uint32_t)sizeof(ufoStrLit)) {
    uint8_t ch = ufoPeekByte(addr);
    ufoStrLit[pos] = (char)ch;
    if (ch == 0) break;
    pos += 1u; addr += 1u;
  }
  if (pos == (uint32_t)sizeof(ufoStrLit)) ufoFatal("string too long");
}

static void ufoZXHasLabel (uint32_t mypfa) {
  ufoZXPopStrLit();
  if (ufoStrLit[0] == 0) {
    ufoPushBoolData(0);
  } else {
    UrLabelInfo *lbl = findAddLabel(ufoStrLit);
    if (lbl == NULL || ufoZXLType2UFO(lbl->type) > UFO_ZX_LABEL_UNKNOWN) {
      ufoPushBoolData(0);
    } else {
      ufoPushBoolData(1);
    }
  }
}

static void ufoZXGetLabelType (uint32_t mypfa) {
  ufoZXPopStrLit();
  if (ufoStrLit[0] == 0) {
    ufoPushData(UFO_ZX_LABEL_UNDEFINED);
  } else {
    UrLabelInfo *lbl = findAddLabel(ufoStrLit);
    if (lbl == NULL) ufoPushData(UFO_ZX_LABEL_UNDEFINED);
    else ufoPushData(ufoZXLType2UFO(lbl->type));
  }
}

static void ufoZXGetLabelValue (uint32_t mypfa) {
  ufoZXPopStrLit();
  if (ufoStrLit[0] == 0) ufoPushData(0);
  else {
    UrLabelInfo *lbl = findAddLabel(ufoStrLit);
    ufoPushData(lbl ? lbl->value : 0);
  }
}

// this also creates labels
// ( value addr count -- )
static void urw_set_typed_label (int type) {
  ufoZXPopStrLit();
  if (ufoStrLit[0] == 0) ufoFatal("cannot set empty label");
  int32_t value = (int32_t)ufoPopData();

  // convert label type
  switch (type) {
    case UFO_ZX_LABEL_UNDEFINED: ufoFatal("cannot set label with undefined type");
    case UFO_ZX_LABEL_UNKNOWN: ufoFatal("cannot set label with unknown type");
    case UFO_ZX_LABEL_VAR: type = LBL_TYPE_ASS; break;
    case UFO_ZX_LABEL_EQU: type = LBL_TYPE_EQU; break;
    case UFO_ZX_LABEL_CODE: type = LBL_TYPE_CODE; break;
    case UFO_ZX_LABEL_DATA: type = LBL_TYPE_DATA; break;
    case UFO_ZX_LABEL_STOFS: type = LBL_TYPE_STOFS; break;
    default: ufoFatal("invalid label type %d", type);
  }

  UrLabelInfo *lbl = findAddLabel(ufoStrLit);

  if (lbl->type != LBL_TYPE_UNKNOWN && lbl->type != type) {
    ufoFatal("invalid label '%s' type", ufoStrLit);
  }
  if (type != LBL_TYPE_ASS) {
    if (lbl->type >= 0 && lbl->value != value) {
      ufoFatal("invalid label '%s' value", ufoStrLit);
    }
  }

  lbl->value = value;
  if (lbl->type == LBL_TYPE_UNKNOWN) lbl->type = type;
}

static void ufoZXSetLabelVar (uint32_t mypfa) { urw_set_typed_label(UFO_ZX_LABEL_VAR); }
static void ufoZXSetLabelEqu (uint32_t mypfa) { urw_set_typed_label(UFO_ZX_LABEL_EQU); }
static void ufoZXSetLabelCode (uint32_t mypfa) { urw_set_typed_label(UFO_ZX_LABEL_CODE); }
static void ufoZXSetLabelData (uint32_t mypfa) { urw_set_typed_label(UFO_ZX_LABEL_DATA); }
static void ufoZXSetLabelStOfs (uint32_t mypfa) { urw_set_typed_label(UFO_ZX_LABEL_STOFS); }

#define UFO_ZX_MAX_ITERS  (16)

typedef uint32_t UFOZXLabelIterator;

typedef struct {
  UFOZXLabelIterator id;
  UrLabelInfo *lbl;
} UZXLabelIter;

static UZXLabelIter ufoZXLblIters[UFO_ZX_MAX_ITERS];
static uint32_t ufoZXLblIdLast;

static int UFOZXFindIter (UFOZXLabelIterator it) {
  int f = 0;
  while (f != UFO_ZX_MAX_ITERS && ufoZXLblIters[f].id != it) f += 1;
  if (f == UFO_ZX_MAX_ITERS) f = -1;
  return f;
}

static UrLabelInfo *UFOZXNormLabel (UrLabelInfo *c) {
  while (c != NULL && (c->type == LBL_TYPE_VERY_SPECIAL || c->type == LBL_TYPE_UNKNOWN)) {
    c = c->next;
  }
  return c;
}

static void UFOZXLabelRemoved (UrLabelInfo *lbl) {
  ur_assert(lbl != NULL);
  UrLabelInfo *c = UFOZXNormLabel(lbl->next);
  for (int f = 0; f < UFO_ZX_MAX_ITERS; f += 1) {
    if (ufoZXLblIters[f].lbl == lbl) {
      ufoZXLblIters[f].lbl = c;
    }
  }
}

static UFOZXLabelIterator ufoZXNewLabelIter (void) {
  UFOZXLabelIterator res = 0;
  UrLabelInfo *c = UFOZXNormLabel(labels);
  if (c != NULL) {
    int f = 0;
    while (res == 0 && f != UFO_ZX_MAX_ITERS) {
      if (ufoZXLblIters[f].id == 0) {
        ufoZXLblIdLast += 1;
        if (ufoZXLblIdLast == 0) ufoZXLblIdLast = 1;
        ufoZXLblIters[f].id = ufoZXLblIdLast;
        ufoZXLblIters[f].lbl = c;
        res = ufoZXLblIdLast;
      }
    }
  }
  return res;
}

static int ufoZXLabelIterNext (UFOZXLabelIterator it) {
  int res = 0;
  int f = UFOZXFindIter(it);
  if (f != -1) {
    UrLabelInfo *c = ufoZXLblIters[f].lbl;
    if (c != NULL) c = UFOZXNormLabel(c->next);
    if (c != NULL) {
      ufoZXLblIters[f].lbl = c;
      res = 1;
    } else {
      ufoZXLblIters[f].id = 0;
      ufoZXLblIters[f].lbl = NULL;
    }
  }
  return res;
}

static void ufoZXLabelIterClose (UFOZXLabelIterator it) {
  int f = UFOZXFindIter(it);
  if (f != -1) {
    ufoZXLblIters[f].id = 0;
    ufoZXLblIters[f].lbl = NULL;
  }
}

static const char *ufoZXLabelIterGetName (UFOZXLabelIterator it) {
  int f = UFOZXFindIter(it);
  if (f != -1) {
    UrLabelInfo *c = ufoZXLblIters[f].lbl;
    if (c != NULL) return c->name;
  }
  return NULL;
}

static int ufoZXIterGetValue (UFOZXLabelIterator it) {
  int f = UFOZXFindIter(it);
  if (f != -1) {
    UrLabelInfo *c = ufoZXLblIters[f].lbl;
    if (c != NULL) return c->value;
  }
  return 0;
}

static int ufoZXIterGetType (UFOZXLabelIterator it) {
  int f = UFOZXFindIter(it);
  if (f != -1) {
    UrLabelInfo *c = ufoZXLblIters[f].lbl;
    if (c != NULL) ufoZXLType2UFO(c->type);
  }
  return UFO_ZX_LABEL_UNDEFINED;
}

// UR-NEW-LABEL-ITER
// ( -- iterid | 0 )
static void ufoZXNewLabelIterWord (uint32_t mypfa) {
  ufoPushData(ufoZXNewLabelIter());
}

// UR-CLOSE-LABEL-ITER
// ( iterid -- )
static void ufoZXCloseLabelIterWord (uint32_t mypfa) {
  uint32_t id = ufoPopData();
  ufoZXLabelIterClose(id);
}

// UR-LABEL-ITER-NEXT
// ( iterid -- not-done? )
static void ufoZXNextLabelIterWord (uint32_t mypfa) {
  uint32_t id = ufoPopData();
  ufoPushBoolData(ufoZXLabelIterNext(id));
}

// UR-LABEL-ITER-GET-NAME
// ( iterid -- addr count )
// to PAD
static void ufoZXGetLabelNameIterWord (uint32_t mypfa) {
  uint32_t id = ufoPopData();
  const char *name = ufoZXLabelIterGetName(id);
  if (name == NULL) name = "";
  uint32_t count = 0;
  uint32_t pad = ufoGetPad() + 4u;
  while (count != 1024 && *name != 0) {
    ufoPokeByte(pad + count, ((const unsigned char *)name)[count]);
    count += 1u; name += 1u;
  }
  if (count == 1024) ufoFatal("label name too long");
  ufoPokeByte(pad + count, 0); // just in case
  ufoPushData(pad); ufoPushData(count);
}

// UR-LABEL-ITER-GET-VALUE
// ( iterid -- value )
static void ufoZXGetLabelValueIterWord (uint32_t mypfa) {
  uint32_t id = ufoPopData();
  ufoPushData((uint32_t)ufoZXIterGetValue(id));
}

// UR-LABEL-ITER-GET-TYPE
// ( iterid -- type )
static void ufoZXGetLabelTypeIterWord (uint32_t mypfa) {
  uint32_t id = ufoPopData();
  ufoPushData((uint32_t)ufoZXIterGetType(id));
}


static int ufoGetSrcLine (void *buf, size_t bufsize, const char **fname, int *lnum) {
  char *dest = (char *)buf;
  if (ufoIsInMacroMode()) {
    //fprintf(stderr, "!<%s>\n", currLine);
    if (strlen(currLine) >= bufsize) ufoFatal("user line too long");
    strcpy(buf, currLine);
    if (currSrcLine != NULL) {
      *lnum = currSrcLine->lineNo;
      *fname = currSrcLine->fname;
    } else {
      *lnum = -1;
      *fname = "wtf";
    }
  } else {
    SourceLine *sl = nextUFOSrcLine();
    //fprintf(stderr, "!NL:%p:<%s>\n", sl, (sl && sl->line ? sl->line : "fuck!"));
    if (sl == NULL) return 0;
    if (sl->line == NULL) {
      *dest = 0;
    } else if (strlen(sl->line) >= bufsize) {
      ufoFatal("user line too long");
    } else {
      memcpy(buf, sl->line, strlen(sl->line) + 1);
    }
    *lnum = sl->lineNo;
    *fname = sl->fname;
  }
  return 1;
}

char *ufoCreateIncludeName (const char *fname, int assystem, const char *lastIncPath) {
  if (lastIncPath == NULL) {
    if (assystem) {
      lastIncPath = ufoIncludeDir;
    } else {
      lastIncPath = ".";
    }
  }
  char *res = createIncludeName(fname, (assystem ? -666 : -669), NULL, lastIncPath);
  struct stat st;
  char *tmp;
  if (res == NULL || res[0] == 0 || stat(res, &st) != 0) return res;
  if (S_ISDIR(st.st_mode)) {
    tmp = strprintf("%s/%s", res, "00-main-loader.f");
    free(res);
    res = tmp;
  }
  return res;
  //return createIncludeName(fname, (assystem ? -666 : -669), NULL, lastIncPath);
}


// $END_FORTH
static void ufoZXEndForth (uint32_t mypfa) {
  if (ufoIsInMacroMode()) ufoFatal("$END_FORTH in non-native mode");
  if (ufoIsCompiling()) ufoFatal("$END_FORTH: still compiling something");
  ufoFinishVM();
}

static void urw_declare_typed_label (int type) {
  if (ufoIsCompiling()) ufoFatal("execution mode expected");
  ufoCallParseName();
  ufoZXPopStrLit();
  if (ufoStrLit[0] == 0) ufoFatal("label name expected");
  UrLabelInfo *lbl = findAddLabel(ufoStrLit);
  if (lbl->type != LBL_TYPE_UNKNOWN && lbl->type != type) {
    ufoFatal("invalid label '%s' type", ufoStrLit);
  }
  if (type != LBL_TYPE_ASS) {
    if (lbl->type >= 0 && lbl->value != pc) {
      ufoFatal("invalid label '%s' value", ufoStrLit);
    }
  }

  lbl->value = pc;
  if (lbl->type == LBL_TYPE_UNKNOWN) lbl->type = type;
}

// $LABEL-DATA: name
static void ufoZXDefLabelData (uint32_t mypfa) { urw_declare_typed_label(LBL_TYPE_DATA); }
// $LABEL-CODE: name
static void ufoZXDefLabelCode (uint32_t mypfa) { urw_declare_typed_label(LBL_TYPE_CODE); }


//==========================================================================
//
//  initUrForth
//
//==========================================================================
static void initUrForth (void) {
  for (int f = 0; f < UFO_ZX_MAX_ITERS; f += 1) {
    ufoZXLblIters[f].id = 0;
    ufoZXLblIters[f].lbl = NULL;
  }
  ufoZXLblIdLast = 0;

  ufoMacroVocId = ufoCreateVoc("URASM-MACROS", 0, UFW_FLAG_PROTECTED);

  ufoVocSetOnlyDefs(ufoCreateVoc("URASM", 0, UFW_FLAG_PROTECTED));

  // UrAsm label types
  ufoRegisterConstant("LBL-TYPE-UNKNOWN", UFO_ZX_LABEL_UNKNOWN, UFW_FLAG_PROTECTED);
  ufoRegisterConstant("LBL-TYPE-VAR", UFO_ZX_LABEL_VAR, UFW_FLAG_PROTECTED);
  ufoRegisterConstant("LBL-TYPE-EQU", UFO_ZX_LABEL_EQU, UFW_FLAG_PROTECTED);
  ufoRegisterConstant("LBL-TYPE-CODE", UFO_ZX_LABEL_CODE, UFW_FLAG_PROTECTED);
  ufoRegisterConstant("LBL-TYPE-STOFS", UFO_ZX_LABEL_STOFS, UFW_FLAG_PROTECTED);
  ufoRegisterConstant("LBL-TYPE-DATA", UFO_ZX_LABEL_DATA, UFW_FLAG_PROTECTED);

  // ZX-C,  ( val8 -- )
  // puts byte to zx dictionary
  ufoRegisterWord("C,", &ufoZXEmitU8, UFW_FLAG_PROTECTED);

  // ZX-W,  ( val -- )
  // puts word to zx dictionary
  ufoRegisterWord("W,", &ufoZXEmitU16, UFW_FLAG_PROTECTED);

  // ZX-C@  ( addr -- value8 )
  ufoRegisterWord("C@", &ufoZXGetU8, UFW_FLAG_PROTECTED);

  // ZX-C!  ( val8 addr -- )
  ufoRegisterWord("C!", &ufoZXPutU8, UFW_FLAG_PROTECTED);

  // ZX-W@  ( addr -- value16 )
  ufoRegisterWord("W@", &ufoZXGetU16, UFW_FLAG_PROTECTED);

  // ZX-W!  ( val16 addr -- )
  ufoRegisterWord("W!", &ufoZXPutU16, UFW_FLAG_PROTECTED);

  // ZX-RESERVED?  ( addr -- bool )
  ufoRegisterWord("RESERVED?", &ufoZXGetReserved, UFW_FLAG_PROTECTED);

  // ZX-RESERVED!  ( bool addr -- )
  ufoRegisterWord("RESERVED!", &ufoZXSetReserved, UFW_FLAG_PROTECTED);

  ufoRegisterWord("HAS-LABEL?", &ufoZXHasLabel, UFW_FLAG_PROTECTED);
  ufoRegisterWord("LABEL-TYPE?", &ufoZXGetLabelType, UFW_FLAG_PROTECTED);

  ufoRegisterWord("GET-LABEL", &ufoZXGetLabelValue, UFW_FLAG_PROTECTED);

  ufoRegisterWord("SET-LABEL-VAR", &ufoZXSetLabelVar, UFW_FLAG_PROTECTED);
  ufoRegisterWord("SET-LABEL-EQU", &ufoZXSetLabelEqu, UFW_FLAG_PROTECTED);
  ufoRegisterWord("SET-LABEL-CODE", &ufoZXSetLabelCode, UFW_FLAG_PROTECTED);
  ufoRegisterWord("SET-LABEL-STOFS", &ufoZXSetLabelStOfs, UFW_FLAG_PROTECTED);
  ufoRegisterWord("SET-LABEL-DATA", &ufoZXSetLabelData, UFW_FLAG_PROTECTED);

  ufoRegisterWord("NEW-LABEL-ITER", &ufoZXNewLabelIterWord, UFW_FLAG_PROTECTED);
  ufoRegisterWord("CLOSE-LABEL-ITER", &ufoZXCloseLabelIterWord, UFW_FLAG_PROTECTED);
  ufoRegisterWord("LABEL-ITER-NEXT", &ufoZXNextLabelIterWord, UFW_FLAG_PROTECTED);
  ufoRegisterWord("LABEL-ITER-GET-NAME", &ufoZXGetLabelNameIterWord, UFW_FLAG_PROTECTED);
  ufoRegisterWord("LABEL-ITER-GET-VALUE", &ufoZXGetLabelValueIterWord, UFW_FLAG_PROTECTED);
  ufoRegisterWord("LABEL-ITER-GET-TYPE", &ufoZXGetLabelTypeIterWord, UFW_FLAG_PROTECTED);

  ufoRegisterWord("PASS@", &ufoZXGetPass, UFW_FLAG_PROTECTED);

  //ufoRegisterWord("LOAD-DATA-FILE", ZX_LOAD_DATA_FILE);

  ufoRegisterWord("ORG@", &ufoZXGetOrg, UFW_FLAG_PROTECTED);
  ufoRegisterWord("DISP@", &ufoZXGetDisp, UFW_FLAG_PROTECTED);
  ufoRegisterWord("ENT@", &ufoZXGetEnt, UFW_FLAG_PROTECTED);
  ufoRegisterWord("ORG!", &ufoZXSetOrg, UFW_FLAG_PROTECTED);
  ufoRegisterWord("DISP!", &ufoZXSetEnt, UFW_FLAG_PROTECTED);
  ufoRegisterWord("ENT!", &ufoZXSetDisp, UFW_FLAG_PROTECTED);

  ufoVocSetOnlyDefs(ufoGetForthVocId());

  ufoRegisterWord("$LABEL-CODE:", &ufoZXDefLabelData, UFW_FLAG_PROTECTED | UFW_FLAG_IMMEDIATE);
  ufoRegisterWord("$LABEL-DATA:", &ufoZXDefLabelCode, UFW_FLAG_PROTECTED | UFW_FLAG_IMMEDIATE);

  ufoRegisterWord("$END-FORTH", &ufoZXEndForth, UFW_FLAG_PROTECTED | UFW_FLAG_IMMEDIATE);
  ufoRegisterWord("$END_FORTH", &ufoZXEndForth, UFW_FLAG_PROTECTED | UFW_FLAG_IMMEDIATE);
}


///////////////////////////////////////////////////////////////////////////////
// main
//
int main (int argc, char *argv[]) {
  int res = 0, c;
  const char *pname = argv[0];
  char *inFile = NULL;
  char **defines = NULL, **values = NULL;
  int defcount = 0;
  int wantref = 0;
  //
  ufoSetUserPostInit(&initUrForth);
  initLabelBuckets();
  modulesClear();
  initInclideDir();
  initUFEInclideDir();
  //
  urasm_getbyte = getByte;
  urasm_putbyte = putByte;
  urasm_label_by_name = findLabelCB;
  urasm_getval = getValueCB;
  urasm_expand = expandCB;
  urasm_fixup_operand = fixupOperandCB;
  //
  //strcpy(tapeLoaderName, "cargador  ");
  tapeLoaderName[0] = 0;
  //
  printf("urasm v%d.%d.%d, compile date: %s %s\n", VERSION_HI, VERSION_MID, VERSION_LO, __DATE__, __TIME__);
  while ((c = getopt_long(argc, argv, "sStTrBbnhHFLlMm", longOpts, NULL)) >= 0) {
    switch (c) {
      case '?': return 1;
      case 'S': /*optRunSNA = 1;*/ optSNA48 = 0; optWriteType = 's'; optWTChanged = 1; break;
      case 's': /*optRunSNA = 0;*/ optSNA48 = 1; optWriteType = 's'; optWTChanged = 1; break;
      case 'T': optRunTape = 1; optWriteType = 't'; optWTChanged = 1; break;
      case 't': optRunTape = 0; optWriteType = 't'; optWTChanged = 1; break;
      case 'L': optRunSCL = 1; optWriteType = 'S'; optWTChanged = 1; break; /* with boot */
      case 'l': optRunSCL = -1; optWriteType = 'S'; optWTChanged = 1; break; /* no boot */
      case 'm': optRunSCL = -1; optWriteType = 'M'; optWTChanged = 1; break; /* no boot */
      case 'M': optRunSCL = 1; optWriteType = 'M'; optWTChanged = 1; break; /* with boot */
      case 'r': case 'n': optWriteType = c; optWTChanged = 1; break;
      case 'b': optRunTape = 0; optRunDMB = 0; optWriteType = 'd'; optWTChanged = 1; break;
      case 'B': optRunTape = 0; optRunDMB = 1; optWriteType = 'd'; optWTChanged = 1; break;
      case 'h': usage(pname); res = 0; goto earlyerrquit;
      case 'H': optWriteType = 'H'; optWTChanged = 1; break;
      case 'F':
        optWriteFixups = 1;
        if (optarg != NULL) {
          if (strcmp(optarg, "asm") == 0 || strcmp(optarg, "zas") == 0) {
            optFixupType = 1;
          } else if (strcmp(optarg, "asmdiff") == 0 || strcmp(optarg, "zasdiff") == 0) {
            optFixupType = 2;
          } else if (strcmp(optarg, "asmz") == 0 || strcmp(optarg, "zasz") == 0) {
            optFixupType = 3;
          } else if (strcmp(optarg, "text") == 0) {
            optFixupType = 0;
          } else {
            fprintf(stderr, "FATAL: invalid fixup type: '%s'\n", optarg);
            return 1;
          }
        }
        break;
      case 600: // org
        c = atoi(optarg);
        //fprintf(stderr, "ORG: %d\n", c);
        if (c < 0 || c > 65535) {
          fprintf(stderr, "FATAL: invalid ORG: %d\n", c);
          return 1;
        }
        start_pc = start_disp = start_ent = c;
        break;
      case 601: // define
        //fprintf(stderr, "define: [%s]\n", optarg);
        defines = realloc(defines, sizeof(char *)*(defcount+1));
        values = realloc(values, sizeof(char *)*(defcount+1));
        defines[defcount] = strdup(optarg);
        values[defcount] = strdup("1");
        ++defcount;
        break;
      case 602: // defzero
        //fprintf(stderr, "defzero: [%s]\n", optarg);
        defines = realloc(defines, sizeof(char *)*(defcount+1));
        values = realloc(values, sizeof(char *)*(defcount+1));
        defines[defcount] = strdup(optarg);
        values[defcount] = strdup("0");
        ++defcount;
        break;
      case 660: // outdir
        if (optOutputDir != NULL) free(optOutputDir);
        optOutputDir = strdup(optarg);
        break;
      case 665: // outdir
        if (optOutputFile != NULL) free(optOutputFile);
        optOutputFile = strdup(optarg);
        break;
      case 669: // reffile
        if (refFileName) free(refFileName);
        refFileName = (optarg ? strdup(optarg) : NULL);
        wantref = 1;
        break;
      case 690: case 692: // +3DOS 180K disk
        optRunDSK = (c == 692);
        optDskType = P3DSK_PCW_SS;
        optWriteType = '3';
        optWTChanged = 1;
        break;
      case 691: case 693: // +3DOS 720K disk
        optRunDSK = (c == 693);
        optDskType = P3DSK_PCW_DS;
        optWriteType = '3';
        optWTChanged = 1;
        break;
      case 656: // hex castrates
        if (optarg != NULL) {
          if (strcmp(optarg, "tan") == 0 || strcmp(optarg, "yes") == 0) {
            urasm_allow_hex_castrates = 1;
          } else if (strcmp(optarg, "ona") == 0 || strcmp(optarg, "no") == 0) {
            urasm_allow_hex_castrates = 0;
          } else {
            fprintf(stderr, "FATAL: i don't know what '%s' means.\n", optarg);
            return 1;
          }
        } else {
          urasm_allow_hex_castrates = 1;
        }
        break;
    }
  }

  if (optind >= argc) {
    // try to find default input file
    for (int f = 0; defInFiles[f]; ++f) {
      if (!access(defInFiles[f], R_OK)) {
        inFile = strdup(defInFiles[f]);
        break;
      }
    }
  } else {
    inFile = strdup(argv[optind]);
  }

  if (!inFile || !inFile[0]) {
    res = 1;
    fprintf(stderr, "ERROR: no input file!\n");
    goto earlyerrquit;
  }

  if (optOutputDir == NULL) optOutputDir = strdup(".");
  #ifdef WIN32
  for (char *ss = optOutputDir; *ss; ++ss) if (*ss == '\\') *ss = '/';
  #endif
  {
    char *ee = strrchr(optOutputDir, '/');
    if (ee && ee != optOutputDir) *ee = 0;
  }
  if (!optOutputDir[0]) { free(optOutputDir); optOutputDir = strdup("."); }

  registerInstructions();
  registerFunctions();

  res = asmTextLoad(inFile, 0);
  if (res == 0) {
    // init UrForth here, because why not?
    ufoInit();
    ufoFileReadLine = &ufoGetSrcLine;

    for (int f = 0; f < defcount; ++f) {
      if (labelDoEQU(defines[f], values[f]) != 0) {
        fprintf(stderr, "FATAL: can't define label: '%s'\n", defines[f]);
        goto errquit;
      }
      //fprintf(stderr, "****SET**** <%s> to <%s>\n", defines[f], values[f]);
    }

    for (pass = 0; pass <= 1; ++pass) {
      initPass();
      printf("pass %d\n", pass);
      //fprintf(stderr, "===========pass %d===========\n", pass);
      setCurSrcLine(asmText);
      if (setjmp(errJP)) { res = 1; break; }
      while (currSrcLine) processCurrentLine();
      if (postPass()) { res = 1; break; }
    }

    // write result
    if (res == 0) {
      char *oc = strdup(inFile);
      char *pd = strrchr(oc, '.');
      #ifdef WIN32
      if (pd && !strchr(oc, '/') && !strchr(oc, '\\')) *pd = '\0';
      #else
      if (pd && !strchr(oc, '/')) *pd = '\0';
      #endif
      switch (optWriteType) {
        case 's': saveSna(oc, optSNA48); break;
        case 't': saveTap(oc); break;
        case 'r': saveRaw(oc); break;
        case 'd': saveDMB(oc); break;
        case 'H': saveHob(oc); break;
        case 'S': saveSCL(oc); break;
        case 'M': saveSCLMono(oc); break;
        case '3': saveDSK(oc); break;
      }
      free(oc);
      if (optWriteFixups) writeFixups();
      if (wantref) {
        /* build ref file name */
        if (!refFileName || !refFileName[0]) {
          if (refFileName) free(refFileName);
          refFileName = malloc(strlen(inFile)+128);
          strcpy(refFileName, inFile);
          char *ext = strrchr(refFileName, '.');
          if (!ext) {
            strcat(refFileName, ".ref");
          } else {
            #ifdef WIN32
            char *slash = NULL;
            for (char *ts = refFileName; *ts; ++ts) {
              if (*ts == '/' || *ts == '\\') slash = ts;
            }
            #else
            char *slash = strrchr(refFileName, '/');
            #endif
            if (!slash || slash < ext) {
              strcpy(ext, ".ref");
            } else {
              strcat(refFileName, ".ref");
            }
          }
        }
        writeLabelsFile(refFileName);
        printf("refs written to '%s'\n", refFileName);
        free(refFileName);
      }
    }
  } else {
    fprintf(stderr, "ERROR: loading error!\n");
  }

errquit:
  ufoDeinit();
  if (lastSeenGlobalLabel) free(lastSeenGlobalLabel);
  clearFixups();
  urClearLabels();
  modulesClear();
  asmTextClear();
  urClearOps();
  clearMacros();
  freeStructs();
  clearKnownFiles();

earlyerrquit:
  if (inFile) free(inFile);
  if (sysIncludeDir) free(sysIncludeDir);
  if (ufoIncludeDir) free(ufoIncludeDir);
  if (lastIncludePath) free(lastIncludePath);
  if (lastSysIncludePath) free(lastSysIncludePath);
  for (int f = defcount-1; f >= 0; --f) {
    free(values[f]);
    free(defines[f]);
  }
  if (defines != NULL) { free(values); free(defines); }
  if (optOutputFile) free(optOutputFile);
  if (optOutputDir) free(optOutputDir);

  return (res ? 1 : 0);
}

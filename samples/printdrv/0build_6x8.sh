#!/bin/sh

odir=`pwd`
mdir=`dirname "$0"`
cd "$mdir"

FLAGS="--define DRIVER_6x8"

mkdir _out 2>/dev/null
cp -f static/cargador.\$B _out/cargador.\$B
urasm -n $FLAGS --fixups=zasz --org=0 printdrv.zas
urasm --outdir=_out -H $FLAGS printdrv.zas
urasm --outdir=_out -T $FLAGS printdrv.zas
trdx -c -s _out/print6x8.scl _out/cargador.\$B _out/printdrv_F000.\$C
mv _out/printdrv.tap _out/print6x8.tap
mv _out/printdrv_F000.\$C _out/print6x8_F000.\$C

cd "$odir"

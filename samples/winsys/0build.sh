#!/bin/sh

odir=`pwd`
mdir=`dirname "$0"`
cd "$mdir"

mkdir _out 2>/dev/null
cp -f static/cargador.\$B _out/cargador.\$B
urasm --define=USE_FIXUPS -n --fixups=zasz --org=0
urasm --define=USE_FIXUPS --outdir=_out
urasm --define=USE_FIXUPS --outdir=_out -H
trdx -c -s _out/winsys.scl _out/cargador.\$B _out/main_6000.\$C

cd "$odir"

#!/bin/sh

( cd .. ; urasm -r )
mv ../main_8000.bin .
exoraw -c main_8000.bin -o main_8000.exo
urasm maincode.zas
urasm main.zas
rm main_8000.bin main_8000.exo
cat main.tap maincode.tap >voxel.tap
rm main.tap maincode.tap
